<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
class Bismillah_Model extends CI_Model{
    public function __construct(){
        $this->load->database() ;
    }

    public function insert_id(){
        return $this->db->insert_id() ;
    }

    public function escape($str){
        return $this->db->escape($str) ;
    }

    public function escape_like_str($str){
        return $this->db->escape_like_str($str) ;
    }

    public function sql_exec($query, $save_log=TRUE, $simple=TRUE){
        if($this->db->save_log === TRUE && $save_log === TRUE){//save to log

        }

        if($simple){
            if(!$this->db->simple_query($query)){
                if(ENVIRONMENT <> 'live'){
                    print_r($this->db->error()) ;
                    print_r($query) ;
                }
            }
        }else{
            if($return = $this->db->query($query)){
                return $return ;
            }else{
                if(ENVIRONMENT <> 'live'){
                    print_r($this->db->error()) ;
                }
            }
        }
    }

    public function select($table, $field, $where='', $join='', $group='', $order='', $limit=''){
        if(trim($where) !== "") $where = 'WHERE ' . $where ;
        if(trim($group) !== "") $group = 'GROUP BY ' . $group ;
        if(trim($order) !== "") $order = 'ORDER BY ' . $order ;
        if(trim($limit) !== "") $limit = 'LIMIT ' . $limit ;

        $query = "SELECT {$field} FROM {$table} {$join} {$where} {$group} {$order} {$limit}" ;
        return $this->sql_exec($query, FALSE, FALSE) ;
    }

    public function getrow($o){
        return $o->unbuffered_row('array') ;
    }

    public function getrow_for($o){
        return $o->result_array() ; 
    }

    public function rows($o){
        return $o->num_rows() ;
    }
    
    public function fields($o){
		return $o->num_fields() ;
	}

    public function insert($table, $data, $save_log=TRUE){
        $field 	= array() ;
        $val 		= array() ;
        foreach ($data as $key => $value) {
            $field[] 	= $key ;
            $val[]		= $this->escape($value) ;
        }
        $field	= "(" . implode(",", $field) . ")" ;
        $val 		= "(" . implode(",", $val) . ")" ;
        $query	= "INSERT INTO {$table} {$field} VALUES {$val}" ;
        $this->sql_exec($query, $save_log) ;
    }

    public function edit($table, $data, $where='',$save_log=TRUE){
        if(trim($where) !== "") $where = 'WHERE ' . $where ;

        $udata 			= array() ;
        foreach ($data as $key => $value) {
            $udata[] 	= " {$key} = ".$this->escape($value)." " ;
        }
        $udata 	= implode(", ", $udata) ;
        $query	= "UPDATE {$table} SET {$udata} {$where}" ;
        $this->sql_exec($query, $save_log) ;
    }

    public function update($table, $data, $where='', $field_id='', $save_log=TRUE){
        if($field_id == '') $field_id = 'id';
        $dbdata = $this->select($table, $field_id, $where) ;
        if($this->rows($dbdata) > 0){
            $this->edit($table, $data, $where, $save_log) ;
        }else{
            $this->insert($table, $data, $save_log) ;
        }
    }

    public function delete($table, $where, $save_log=TRUE){
        if(trim($where) !== "") $where = 'WHERE ' . $where ;
        $query 	= "DELETE FROM {$table} {$where}" ;
        $this->sql_exec($query, $save_log) ;
    }

    public function delete_all($table){
        $query 	= "TRUNCATE TABLE {$table}" ;
        $this->sql_exec($query, FALSE) ;
    }

    public function AddIndex($table,$namaindex,$field){
        $dbd = $this->sql_exec("SHOW INDEXES FROM $table WHERE Key_name = '$namaindex'",FALSE, FALSE) ;
        if($this->rows($dbd) == 0){
            $this->sql_exec("ALTER TABLE $table ADD Index $namaindex ($field)") ;
        }
    }

    public function getsql(){
        return $this->db->last_query() ;
    }

    public function getval($field, $where, $table,$join="",$group="",$order=""){
        $rerow 		= '' ;
        $dbdata 	= $this->select($table, $field, $where, $join, $group, $order, "0,1") ;
        $row 		= $this->getrow($dbdata) ;
        if(strpos($field, ",") === FALSE && trim($field) !== "*" ){
            if(!empty($row)){
                $rerow 	= $row[$field] ;
            }
        }else{
            $rerow 	= $row ;
        }
        return $rerow ;
    }

    public function saveconfig($key, $val=''){
        $this->update("sys_config", array("title"=>$key, "val"=>$val), "title = " . $this->escape($key), "id") ;
    }

    public function getconfig($key,$default = ""){
        $return = $this->getval('val', "title = ". $this->escape($key), "sys_config") ;
        if($return == ""){
            $return = $default;
        }
        return $return;
    }

    public function getincrement($k,$l=true,$n=0 ){
        /*
			$k = Key about increment
			$l = Update increment?
			$n = Length (pad)
		*/
        // $inc 	= 1 ;
        // $k 	= "inc_" . $k ;
        // $val 	= intval($this->getconfig($k)) ;
        // $inc 	= ($val > 0) ? $val+1 : $inc ;
        // if($l){
        //     $this->saveconfig($k, $inc) ;
        // }
        // return str_pad($inc, $n, "0", STR_PAD_LEFT) ;

        $k 	= "inc_" . $k ;
        if($l){
            $this->sql_exec("insert into sys_config set title = '".$k."', val = (select (ifnull(max(CAST(a.val AS UNSIGNED ) ),0) + 1) from sys_config a where a.title = '".$k."')");
            $val = $this->getval('max(CAST(val AS UNSIGNED ))', "title = ". $this->escape($k), "sys_config") ;
            $this->delete("sys_config","title = '".$k."' and CAST(val AS UNSIGNED ) < $val");
            $inc = $val;
        }else{
            $val 	= intval($this->getconfig($k)) ;
            $inc 	= ($val > 0) ? $val+1 : 1 ;
        }

        return str_pad($inc, $n, "0", STR_PAD_LEFT) ;
    }

    public function getlastfaktur($key,$cabang,$tgl,$l=true,$length=0){
        $tgl = date_2t($tgl);
        $k       = $key .$cabang. date("ymd",$tgl) ;  
        $sisalength = $length - strlen($k);
        return $k . $this->getincrement($k, $l, $sisalength) ; 
    }

    // func DB
    public function AddTable($cTableName,$cSQL,$cDatabase=''){
        if(!empty($cDatabase)){
            $cDatabase = " From " . $cDatabase ;
        }

        //$cTableName = strtolower(trim($cTableName)) ;
        $dbData = $this->db->query("Show Tables $cDatabase") ;
        $lAdd = true ;
        foreach($dbData->result_array() as $dbRow){
            foreach($dbRow as $key){
                if($key == $cTableName){
                    $lAdd = false ; 
                }
            }
        }
        //if($this->rows($dbData) > 0)$lAdd = false ; 
        if($lAdd){
            $this->db->query($cSQL) ;
        }
    }

    function AddField($cTableName,$cFieldName,$cFieldType,$cDefault,$cFieldAfter=''){
        $dbData =  $this->db->query("Show Fields From $cTableName") ;
        $lModif = true ;
        foreach($dbData->result_array() as $dbRow){
            foreach($dbRow as $key){
                if($key == $cFieldName){
                    $lModif = false ; 
                }
            }
        }
        if($lModif){
            if(!empty($cFieldAfter)){
                $cFieldAfter = " AFTER " . $cFieldAfter ;
            }

            if($cDefault !== 'NULL') $cDefault = "'".$cDefault."'";  
            // echo('alert('."ALTER TABLE $cTableName ADD $cFieldName $cFieldType DEFAULT $cDefault $cFieldAfter").');' ;
            $this->db->query("ALTER TABLE $cTableName ADD $cFieldName $cFieldType DEFAULT $cDefault $cFieldAfter") ;
        }
    }
    
    public function CheckDatabase(){
     /*   $this->AddField("persekot_mutasi_total","keterangan","varchar(100)","","kodetransaksi");
        $this->AddField("hutang_pelunasan_total","keterangan","varchar(100)","","supplier");

        $cSQL = "CREATE TABLE `hj_jenis` (
            `id` double NOT NULL AUTO_INCREMENT,
            `kode` varchar(20) DEFAULT '',
            `keterangan` varchar(20) DEFAULT '',
            `status` varchar(1) DEFAULT '1',
            `username` varchar(50) NOT NULL DEFAULT '',
            `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            PRIMARY KEY (`id`),
            KEY `kode` (`kode`)
            ) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=latin1";
        $this->AddTable("hj_jenis",$cSQL);

        $this->AddField("customer","jenishj","varchar(100)","","golongan");

        $cSQL = "CREATE TABLE `stock_hj_jenis` (
            `id` double NOT NULL AUTO_INCREMENT,
            `hjjenis` varchar(20) DEFAULT '',
            `stock` varchar(20) DEFAULT '',
            `hj` double(16,2) DEFAULT '0.00',
            `status` varchar(1) DEFAULT '1',
            `username` varchar(50) NOT NULL DEFAULT '',
            `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
            PRIMARY KEY (`id`),
            KEY `hjjenisstockstatus` (`hjjenis`,`stock`,`status`)
            ) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=latin1";
        $this->AddTable("stock_hj_jenis",$cSQL); */

       
        // $cSQL = "CREATE TABLE `aset_kelompok` (
        //     `id` double NOT NULL AUTO_INCREMENT,
        //     `kode` varchar(20) DEFAULT '',
        //     `keterangan` varchar(20) DEFAULT '',
        //     `lama` double(10,0) DEFAULT 0,
        //     `username` varchar(50) NOT NULL DEFAULT '',
        //     `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
        //     PRIMARY KEY (`id`),
        //     KEY `kode` (`kode`),
        //     KEY `keterangan` (`keterangan`),
        //     KEY `kdket` (`kode`,`keterangan`)
        //     ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci";
        // $this->AddTable("aset_kelompok",$cSQL);

        // $cSQL = "CREATE TABLE `dati_1` (
        //     `id` double NOT NULL AUTO_INCREMENT,
        //     `kode` varchar(20) DEFAULT '',
        //     `keterangan` varchar(20) DEFAULT '',
        //     `username` varchar(50) NOT NULL DEFAULT '',
        //     `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
        //     PRIMARY KEY (`id`),
        //     KEY `kode` (`kode`),
        //     KEY `keterangan` (`keterangan`),
        //     KEY `kdket` (`kode`,`keterangan`)
        //     ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci";
        // $this->AddTable("dati_1",$cSQL);

        // $cSQL = "CREATE TABLE `dati_2` (
        //     `id` double NOT NULL AUTO_INCREMENT,
        //     `kode` varchar(20) DEFAULT '',
        //     `keterangan` varchar(20) DEFAULT '',
        //     `dati_1` varchar(20) DEFAULT '',
        //     `username` varchar(50) NOT NULL DEFAULT '',
        //     `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
        //     PRIMARY KEY (`id`),
        //     KEY `kode` (`kode`),
        //     KEY `dati_1` (`dati_1`),
        //     KEY `keterangan` (`keterangan`),
        //     KEY `kdket` (`kode`,`keterangan`)
        //     ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci";
        // $this->AddTable("dati_2",$cSQL);

        // $cSQL = "CREATE TABLE `stock_kelompok` (
        //     `id` double NOT NULL AUTO_INCREMENT,
        //     `kode` varchar(20) DEFAULT '',
        //     `keterangan` varchar(20) DEFAULT '',
        //     `jenis` varchar(1) DEFAULT '',
        //     `username` varchar(50) NOT NULL DEFAULT '',
        //     `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
        //     PRIMARY KEY (`id`),
        //     KEY `kode` (`kode`),
        //     KEY `keterangan` (`keterangan`),
        //     KEY `kdket` (`kode`,`keterangan`)
        //     ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci";
        // $this->AddTable("stock_kelompok",$cSQL);

        // $this->AddField("customer","dati_2","varchar(10)","","alamat");
        // $this->AddField("customer","dati_1","varchar(10)","","dati_2");
        // $this->AddField("stock","stock_kelompok","varchar(10)","","stock_group");

        // $this->AddField("penjualan_detail","stock_kelompok","varchar(10)","","tothpkeluar");
        // $this->AddField("penjualan_detail","jenis_kelompok","char(1)","","stock_kelompok");

        // $this->AddField("penjualan_retur_detail","stock_kelompok","varchar(10)","","hp");
        // $this->AddField("penjualan_retur_detail","jenis_kelompok","char(1)","","stock_kelompok");

        // //$this->sql_exec("ALTER TABLE `stock` CHANGE `jenis` `tampil` CHAR(1) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT 'B'");

        // $this->AddField("aset","vendors","json","","vendor");

        // $this->AddField("aset","kelompok","varchar(10)","","golongan");

        // $cSQL = " CREATE TABLE `aset_perubahan` (
        //     `id` bigint NOT NULL AUTO_INCREMENT,
        //     `kode` varchar(10) NOT NULL,
        //     `faktur` varchar(30) DEFAULT '',
        //     `vendors` json DEFAULT NULL,
        //     `cabang` varchar(3) NOT NULL,
        //     `tgl` date DEFAULT NULL,
        //     `kelompok` varchar(10) NOT NULL,
        //     `dataawal` json DEFAULT NULL,
        //     `dataakhir` json DEFAULT NULL,
        //     `status` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '1',
        //     `username` varchar(50) DEFAULT '',
        //     `datetime` datetime DEFAULT '0000-00-00 00:00:00',
        //     PRIMARY KEY (`id`),
        //     KEY `cabtglstt` (`cabang`,`tgl`,`status`),
        //     KEY `kodetglstt` (`kode`,`tgl`,`status`) USING BTREE
        //    ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
        // $this->AddTable("aset_perubahan",$cSQL);

        //kpi
        //$this->AddField("kpi_indikator","periode","char(1)","","satuan");
        // $this->AddField("penjualan_total","tgllunas","date","NULL","status");
        // $this->AddField("penjualan_total","fktlunas","varchar(50)","NULL","tgllunas");
        // $this->AddIndex("penjualan_total","tgllunas","tgllunas");
        // $this->AddIndex("penjualan_total","custtgllunas","customer,tgllunas");
        // $this->AddIndex("penjualan_total","custtgllunasstt","customer,tgllunas,status");
        
        // $this->AddField("penjualan_retur_total","tgllunas","date","NULL","status");
        // $this->AddField("penjualan_retur_total","fktlunas","varchar(50)","NULL","tgllunas");
        // $this->AddIndex("penjualan_retur_total","tgllunas","tgllunas");
        // $this->AddIndex("penjualan_retur_total","custtgllunas","customer,tgllunas");
        // $this->AddIndex("penjualan_retur_total","custtgllunasstt","customer,tgllunas,status");


        // $this->AddField("aset","tgllunaspiut","date","NULL","");
        // $this->AddField("aset","fktlunaspiut","varchar(50)","NULL","tgllunaspiut");
        // $this->AddIndex("aset","tgllunaspiut","tgllunaspiut");
        // $this->AddIndex("aset","custtgllunaspiut","customer,tgllunaspiut");
        // $this->AddIndex("aset","custtgllunaspiutstt","customer,tgllunaspiut,status");

        // $this->AddField("pembelian_total","tgllunas","date","NULL","status");
        // $this->AddField("pembelian_total","fktlunas","varchar(50)","NULL","tgllunas");
        // $this->AddIndex("pembelian_total","tgllunas","tgllunas");
        // $this->AddIndex("pembelian_total","supptgllunas","supplier,tgllunas");
        // $this->AddIndex("pembelian_total","supptgllunasstt","supplier,tgllunas,status");

        
        // $this->AddField("pembelian_retur_total","tgllunas","date","NULL","status");
        // $this->AddField("pembelian_retur_total","fktlunas","varchar(50)","NULL","tgllunas");
        // $this->AddIndex("pembelian_retur_total","tgllunas","tgllunas");
        // $this->AddIndex("pembelian_retur_total","supptgllunas","supplier,tgllunas");
        // $this->AddIndex("pembelian_retur_total","supptgllunasstt","supplier,tgllunas,status");

        
        //kpi
        $this->AddField("kpi_indikator","periode","char(1)","","satuan");
        $this->AddField("kpi_indikator","penilaian","char(1)","","periode");
        $this->AddField("kpi_indikator","jabatan","varchar(20)","","penilaian");
        $this->AddIndex("kpi_indikator","sttpenjbper","status,penilaian,jabatan,periode");
        $this->AddIndex("kpi_indikator","sttpenper","status,penilaian,periode");

        $cSQL = " CREATE TABLE `kpi_peringkat` (
            `id` bigint NOT NULL AUTO_INCREMENT,
            `kode` varchar(3) NOT NULL,
            `keterangan` varchar(30) DEFAULT '',
            `peringkat` int(2) DEFAULT '0',
            `nilai_min` double(5,2) DEFAULT '0.00',
            `nilai_max` double(5,2) DEFAULT '0.00',
            `status` char(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '1',
            `username` varchar(50) DEFAULT '',
            `datetime` datetime DEFAULT '0000-00-00 00:00:00',
            PRIMARY KEY (`id`),
            KEY `kode` (`kode`),
            KEY `peringkat` (`peringkat`),
            KEY `minmax` (`nilai_min`,`nilai_max`),
            KEY `status` (`status`) USING BTREE
           ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
        $this->AddTable("kpi_peringkat",$cSQL);
        $this->AddField("kpi_peringkat","nominal","double(16,2)","0.00","nilai_max");


        $cSQL = " CREATE TABLE `kpi_nilai_total` (
            `id` bigint NOT NULL AUTO_INCREMENT,
            `faktur` varchar(30) DEFAULT '',
            `nip` varchar(20) DEFAULT '',
            `jabatan` varchar(20) DEFAULT '',
            `periode` varchar(6) NOT NULL,
            `tgl` date DEFAULT NULL,
            `peringkat` varchar(3) NOT NULL,
            `jml_umum` double(5,2) DEFAULT '0.00',
            `kpi_umum` double(5,0) DEFAULT '0',
            `nilai_umum` double(5,2) DEFAULT '0.00',
            `jml_khusus` double(5,2) DEFAULT '0.00',
            `kpi_khusus` double(5,0) DEFAULT '0',
            `nilai_khusus` double(5,2) DEFAULT '0.00',
            `nilai` double(5,2) DEFAULT '0.00',
            `status` char(1) DEFAULT '1',
            `posting` char(1) DEFAULT '0',
            `mydata` json DEFAULT NULL,
            `username` varchar(50) DEFAULT '',
            `datetime` datetime DEFAULT '0000-00-00 00:00:00',
            PRIMARY KEY (`id`),
            KEY `faktur` (`faktur`),
            KEY `peringkatstt` (`peringkat`,`status`),
            KEY `periodestt` (`periode`,`status`),
            KEY `periodepostingstt` (`periode`,`posting`,`status`),
            KEY `tglstt` (`tgl`,`status`),
            KEY `tglpostingstt` (`tgl`,`posting`,`status`),
            KEY `nilaistt` (`nilai`,`status`),
            KEY `status` (`status`) USING BTREE
           ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
        $this->AddTable("kpi_nilai_total",$cSQL);
        
        $this->AddField("kpi_nilai_total","nominal","double(16,2)","0.00","posting");
        $this->AddField("kpi_nilai_total","cabang","char(4)","","faktur");
        $this->AddIndex("kpi_nilai_total","nipperstt","nip,periode,status");
        $this->AddIndex("kpi_nilai_total","nippercabstt","nip,periode,cabang,status");
        $this->AddIndex("kpi_nilai_total","percabsttpos","periode,cabang,status,posting");
        $this->AddIndex("kpi_nilai_total","nippersttpos","nip,periode,status,posting");


        $cSQL = " CREATE TABLE `kpi_nilai_detil` (
            `id` bigint NOT NULL AUTO_INCREMENT,
            `faktur` varchar(30) DEFAULT '',
            `kpi` varchar(20) DEFAULT '',
            `kpi_penilaian` char(1) DEFAULT '',
            `kpi_periode` char(2) DEFAULT '',
            `nilai` double(5,2) DEFAULT '0.00',
            PRIMARY KEY (`id`),
            KEY `faktur` (`faktur`),
            KEY `kpi` (`kpi`)
           ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
        $this->AddTable("kpi_nilai_detil",$cSQL);

        $this->AddField("gudang","cabang","varchar(10)","","keterangan");

        $cSQL = " CREATE TABLE `produksi_regu` (
            `id` bigint NOT NULL AUTO_INCREMENT,
            `kode` varchar(10) DEFAULT '',
            `keterangan` varchar(20) DEFAULT '',
            `karu` varchar(20) DEFAULT '',
            `anggota` JSON DEFAULT NULL,
            `cabang` varchar(3) DEFAULT '',
            `status` varchar(1) DEFAULT '',
            PRIMARY KEY (`id`),
            KEY `kode` (`kode`),
            KEY `kodeket` (`kode`,`keterangan`),
            KEY `kodeketstt` (`kode`,`keterangan`,`status`),
            KEY `cabkodeket` (`cabang`,`kode`,`keterangan`),
            KEY `cabkodeketstt` (`cabang`,`kode`,`keterangan`,`status`),
            KEY `cabang` (`cabang`,`status`),
            KEY `karu` (`karu`)
           ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
        $this->AddTable("produksi_regu",$cSQL);

        $this->AddField("produksi_total","regu","json","NULL","petugas");
        $this->AddField("produksi_hasil","regu","json","NULL","gudang");
        $this->AddField("produksi_hasil","qty_target","double(16,2)","0.00","stock");
        $this->AddField("produksi_hasil","qty","double(5,2)","0.00","pers_capaian");
        
        $cSQL = " CREATE TABLE `produksi_pencapaian` (
            `id` bigint NOT NULL AUTO_INCREMENT,
            `fakturproduksi` varchar(30) DEFAULT '',
            `fakturproduksihasil` varchar(30) DEFAULT '',
            `tgl` date DEFAULT NULL,
            `nip` varchar(20) DEFAULT '',
            `stock` varchar(15) DEFAULT '',
            `regu` varchar(10) DEFAULT '',
            `karu_status` char(1) DEFAULT '0',
            `target` double(16,2) DEFAULT '0.00',
            `pencapaian` double(16,2) DEFAULT '0.00',
            `pencapaian_pers` double(5,2) DEFAULT '0.00',
            `cabang` varchar(3) DEFAULT '',
            PRIMARY KEY (`id`),
            KEY `fakturproduksi` (`fakturproduksi`),
            KEY `fakturproduksihasil` (`fakturproduksihasil`),
            KEY `tgl` (`tgl`),
            KEY `tglcab` (`tgl`,`cabang`),
            KEY `tglnip` (`tgl`,`nip`),
            KEY `tglregu` (`tgl`,`regu`)
           ) ENGINE=InnoDB DEFAULT CHARSET=latin1";
        $this->AddTable("produksi_pencapaian",$cSQL);

        $this->AddField("kpi_peringkat","jabatan","varchar(10)","","nilai_max");

        $this->db->query("ALTER TABLE `kpi_peringkat` CHANGE `kode` `kode` VARCHAR(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL");
        $this->db->query("ALTER TABLE `kpi_nilai_total` CHANGE `peringkat` `peringkat` VARCHAR(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL");
        
        $this->AddField("pembelian_retur_total","fktpb","varchar(30)","","tgl");
        $this->AddField("pembelian_retur_total","diskon","double(16,2)","0.00","subtotal");
        $this->AddField("pembelian_retur_total","pembulatan","double(16,2)","0.00","diskon");
        $this->AddField("pembelian_retur_total","persppn","double(16,2)","0.00","pembulatan");
        $this->AddField("pembelian_retur_total","ppn","double(16,2)","0.00","persppn");
        $this->AddField("pembelian_retur_total","hutang","double(16,2)","0.00","kas");

    }
}
?>
