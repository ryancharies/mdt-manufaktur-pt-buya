<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Bismillah_Controller extends CI_Controller{
	public $aruser;
	public function __construct(){
		parent::__construct() ;
		$system_by 	= isset($this->input->request_headers()['sistem_by']) ? true : false;
		if(getsession($this,"username") == ""){
	        if($system_by){
	            echo('window.location.href = "'.base_url('admin/login').'" ;') ;
	        }else{
	            echo('<script>window.location.href = "'.base_url('admin/login').'" ;</script>') ;
	        }
	    }
		//jika direquest dari url masih bisa diakses
	}

	public function duser(){
		
		$this->db->where('username',getsession( $this, 'username' ));
		$dbd = $this->db->select("*")
			->from("sys_username")
			->get();
        $r  = $dbd->row_array();
		// print_r($r);
		if(!empty($r)){
			$r['data_var'] = json_decode($r['data_var'],true);
            $this->aruser = $r;
			$this->aruser['level'] = substr($r['password'],-4);
        }
	}
}
