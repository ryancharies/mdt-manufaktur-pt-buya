<?php
class Config_m extends Bismillah_Model{
    public function seekrekening($search){
        $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("keuangan_rekening", "*", $where, "", "", "kode ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    public function seekgudang($search){
        $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("gudang", "*", $where, "", "", "kode ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    public function seekkoga($search){
        $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%') and status = '1'" ;
        $dbd      = $this->select("payroll_komponen", "*", $where, "", "", "kode ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    public function seekkoabs($search){
        $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("absensi_kode", "*", $where, "", "", "kode ASC", '50') ;
        return array("db"=>$dbd) ;
    }

}
?>
