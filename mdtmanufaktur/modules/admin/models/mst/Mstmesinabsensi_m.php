<?php
class Mstmesinabsensi_m extends Bismillah_Model{

   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(u.keterangan LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $join     = "left join cabang c on c.kode = u.cabang";
      $f        = "u.kode,u.keterangan,u.snmesin,u.ipmesin,u.portmesin,u.macaddrmesin,u.jenis,c.keterangan cabang" ; 
      $dbd      = $this->select("absensi_mesin u", $f, $where, $join, "", "u.kode ASC", $limit) ;

      $row      = 0 ;
      $dba      = $this->select("absensi_mesin u", "u.id", $where, $join, "", "") ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba)) ;
   } 

   public function saving($va, $id){ 
      $data    = array("kode"=>$va['kode'],"keterangan"=>$va['keterangan'],"snmesin"=>$va['snmesin'],
               "ipmesin"=>$va['ipmesin'],"portmesin"=>$va['portmesin'],"macaddrmesin"=>$va['macaddrmesin'],
               "jenis"=>$va['jenis'],"cabang"=>$va['cabang'],
               "username"=> getsession($this, "username"),"datetime"=>date_now()) ;
      $where   = "kode = " . $this->escape($va['kode']) ;
      $this->update("absensi_mesin", $data, $where, "") ;
   }

   public function getdata($kode=''){
      $where 	 = "u.kode = " . $this->escape($kode);
      $join     = "left join cabang c on c.kode = u.cabang";
      $field    = "u.kode,u.keterangan,u.snmesin,u.ipmesin,u.portmesin,u.macaddrmesin,u.jenis,u.cabang,c.keterangan as ketcabang" ;
      $dbd      = $this->select("absensi_mesin u", $field, $where, $join, "", "","1") ;
	  return $dbd;
   }

   public function seekcabang($search)
    {
        $where = "kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%'";
        $dbd = $this->select("cabang", "*", $where, "", "", "keterangan ASC", '50');
        return array("db" => $dbd);
    }
}
?>
