<?php
class Mstmaterialproduk_m extends Bismillah_Model{
   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ;
      if($search !== "") $where[]	= "(s.kode LIKE '{$search}%' OR s.keterangan LIKE '%{$search}%')" ;
      $where[] = "tampil = 'P'";
	   $where 	 = implode(" AND ", $where) ;
      $join     = "left join stock_group g on g.Kode = s.stock_group";
      $field    = "s.*,g.Keterangan as KetStockGroup";
      $dbd      = $this->select("stock s", $field, $where, $join, "", "s.kode ASC", $limit) ;
      $dba      = $this->select("stock s", "s.id", $where) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }
    
    public function loadgrid2($kode){
      $where 	 = "tampil = 'B' or tampil = 'S'" ;
      $dbd      = $this->select("stock", "*", $where, "", "", "keterangan ASC") ;
      $dba      = $this->select("stock", "id", $where) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }
    
   public function checkkomponen($komponen,$kode){
      $va = array("status"=>0,"qty"=>0,"hp"=>0);
      $where 	 = "stock = '$kode' and komponen = '$komponen'" ;
      $dbd      = $this->select("stock_komponen", "*", $where, "", "", "stock ASC","1") ;
      if($dbr = $this->getrow($dbd)){
        $va['status'] = 1;
        $va['qty'] = $dbr['qty'];
		  $va['hp'] = $dbr['hp'];
      }

      return $va;
   }

   public function loadmaterial($kode){
      $where 	= "k.stock = '$kode'" ;
      $field   = "k.qty,k.hp,s.kode,s.keterangan,s.satuan";
      $join    = "left join stock s on s.kode = k.komponen";
      $dbd     = $this->select("stock_komponen k", $field, $where,$join, "", "k.stock ASC","") ;
      return $dbd;
   }

   public function saving($kode, $va){
      $kode = getsession($this, "ssstock_kode", "");
      $va['kode'] = $kode ;
      // save data stock
      $data    = array("btkl"=>string_2n($va['btkl']),"bop"=>string_2n($va['bop'])) ;
      $where   = "kode = " . $this->escape($va['kode'] ) ;
      $this->edit("stock", $data, $where, "") ;
       
      //save komponen
      $this->delete("stock_komponen", "stock = '{$va['kode']}'") ;
      $vaGrid = json_decode($va['grid2']);
      foreach($vaGrid as $key => $val){
        if($val->ck and $va['kelompok'] == "P"){
            $vapaket = array("stock"=>$va['kode'],"komponen"=>$val->kode,"qty"=>$val->qty,"hp"=>$val->hp);
            $this->insert("stock_komponen",$vapaket);
        }
      }
   }
   public function getdata($kode){
      $data = array() ;
      $where 	 = "s.kode = " . $this->escape($kode);
      $join     = "left join stock_group g on g.Kode = s.stock_group left join satuan t on t.kode = s.satuan
                  left join stock_komponen k on k.stock = s.kode";
      $field    = "s.*,g.Keterangan as KetStockGroup,t.Keterangan as KetSatuan,ifnull(sum(qty*hp),0) as bbb";
      $dbd      = $this->select("stock s", $field, $where, $join, "s.kode", "s.kode ASC","") ;
	  return $dbd;
   }
}
?>
