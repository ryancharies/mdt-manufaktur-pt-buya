<?php
class Mstptkpkaryawan_m extends Bismillah_Model{
   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(c.kode LIKE '{$search}%' OR c.nama LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $field = "c.kode,c.nama,c.telepon,c.tgl,c.alamat,g.keterangan as bank,c.rekening,c.anrekening,c.ktp";
      $join = "left join bank g on g.kode = c.bank";
      $dbd      = $this->select("karyawan c", $field, $where, $join, "", "c.kode ASC", $limit) ;
      $dba      = $this->select("karyawan c", "c.id", $where) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function loadgrid2($va){
      $where 	 = array() ; 
      $where[]	= "c.nip LIKE '{$va['kode']}'" ;
      $where 	 = implode(" AND ", $where) ;
      $field = "c.nip,c.tgl,j.keterangan as ptkp";
      $join = "left join pajak_ptkp j on j.kode = c.ptkp";
      $dbd      = $this->select("karyawan_ptkp c", $field, $where, $join, "", "c.tgl desc") ;
      $dba      = $this->select("karyawan_ptkp c", "c.id", $where) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function saving($va){
      $error = "";

      $va['tgl'] = date_2d($va['tgl']);
      if(!isset($va['ptkp']))$error .= "PTKP tidak boleh kosong.. \\n";
      if($va['kode'] == "")$error .= "Kode belum diisi.. \\n";
      if($va['tgl'] == "")$error .= "Tgl belum diisi.. \\n";
      if($error == ""){
         $data    = array("nip"=>$va['kode'], "ptkp"=>$va['ptkp'],"tgl"=>date_2s($va['tgl']),"username"=> getsession($this, "username"),
                  "datetime"=>date_now()) ;
         $where   = "nip = '{$va['kode']}' and tgl = '{$va['tgl']}'";
         $this->update("karyawan_ptkp", $data, $where) ;
         $error = "ok";
      }      
      return $error;
   }

   public function getdata($kode){
      $data = array() ;
		if($d = $this->getval("*", "kode = " . $this->escape($kode), "karyawan")){
         $data = $d;
		}
		return $data ;
   }

   public function deleting($va){
      $return = "ok";
      $va['tgl'] = date_2s($va['tgl']);
      $this->delete("karyawan_ptkp", "nip = '{$va['nip']}' and tgl = '{$va['tgl']}'") ;
      
      return $return;
      
   }

   public function seekptkp($search){
      $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
      $dbd      = $this->select("pajak_ptkp", "*", $where, "", "", "keterangan ASC", '50') ;
      return array("db"=>$dbd) ;
   }

}
?>
