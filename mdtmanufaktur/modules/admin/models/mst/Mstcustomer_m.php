<?php
class Mstcustomer_m extends Bismillah_Model{
   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(c.kode LIKE '{$search}%' OR c.nama LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $field = "c.kode,c.nama,c.notelepon,c.email,c.alamat,g.keterangan as ketgol,j.keterangan as ketjenishj,k.keterangan as dati_2,k.dati_1";
      $join = "left join customer_golongan g on g.kode = c.golongan left join hj_jenis j on j.kode = c.jenishj left join dati_2 k on k.kode = c.dati_2";
      $dbd      = $this->select("customer c", $field, $where, $join, "", "c.kode ASC", $limit) ;
      $dba      = $this->select("customer c", "c.id", $where,$join) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function saving($kode, $va){
      $kode = getsession($this, "sscustomer_kode", "");
      $va['kode'] = $kode !== "" ? $va['kode'] : $this->getkode() ;
      $dati1 = $this->mstcustomer_m->getval("dati_1", "kode = '{$va['dati_2']}'", "dati_2");
      $data    = array("kode"=>$va['kode'], "nama"=>$va['nama'],"email"=>$va['email'],"notelepon"=>$va['notelepon'],"alamat"=>$va['alamat'],
                      "golongan"=>$va['golongan'],"jenishj"=>$va['jenishj'],"dati_2"=>$va['dati_2'],"dati_1"=>$dati1) ;
      $where   = "kode = " . $this->escape($kode) ;
      $this->update("customer", $data, $where, "") ;
   }

   public function getdata($kode){
      $data = array() ;
      $join = "left join customer_golongan g on g.kode = c.golongan
               left join hj_jenis j on j.kode = c.jenishj
               left join dati_2 k on k.kode = c.dati_2
               left join dati_1 p on p.kode = k.dati_1";
      $field = "c.kode,c.nama,c.notelepon,c.email,c.alamat,c.golongan,c.jenishj,g.keterangan as ketgol,
            j.keterangan as ketjenishj,c.dati_2,k.keterangan as ketdati_2,p.keterangan as ketdati_1";
      $where =  "c.kode = " . $this->escape($kode);
      $dbd      = $this->select("customer c", $field, $where, $join, "") ;

		if($d = $this->getrow($dbd)){
         $data = $d;
		}
		return $data ;
   }

   public function deleting($kode){
      $return = "ok";
      $dbd = $this->select("piutang_kartu", "id", "customer = '$kode'");
      if ($dbr = $this->getrow($dbd)) {
          $return = "Data tidak bisa dihapus karena sudah digunakan!!";
      } else {
         $this->delete("customer", "kode = " . $this->escape($kode)) ;
      }
      return $return;
      
   }

   public function seekgolongan($search){
      $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
      $dbd      = $this->select("customer_golongan", "*", $where, "", "", "keterangan ASC", '50') ;
      return array("db"=>$dbd) ;
   }

   public function seekjenishj($search){
      $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
      $dbd      = $this->select("hj_jenis", "*", $where, "", "", "keterangan ASC", '50') ;
      return array("db"=>$dbd) ;
   }

   public function getkode($l=true){
      $y    = date("ym");
      $n    = $this->getincrement("customerkode" . $y, $l, 3);
      $n    = $y.$n ;
      return $n ;
   }
}
?>
