<?php
class Mstkaryawan_m extends Bismillah_Model{
   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(c.kode LIKE '{$search}%' OR c.nama LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $field = "c.kode,c.nama,c.telepon,c.tgl,c.alamat,g.keterangan as bank,c.rekening,c.anrekening,c.ktp,c.tgllahir,p.keterangan as pendidikan_tingkat";
      $join = "left join bank g on g.kode = c.bank left join pendidikan_tingkat p on p.kode = c.pendidikan_tingkat";
      $dbd      = $this->select("karyawan c", $field, $where, $join, "", "c.kode ASC", $limit) ;
      $dba      = $this->select("karyawan c", "c.id", $where, $join) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function saving($kode, $va){
      if(!isset($va['bank']))$va['bank'] = "";
      $kode = getsession($this, "sskaryawan_kode", "");
      $va['kode'] = $kode !== "" ? $va['kode'] : $this->getkode() ;
      $data    = array("kode"=>$va['kode'], "nama"=>$va['nama'],"ktp"=>$va['ktp'],"telepon"=>$va['notelepon'],"alamat"=>$va['alamat'],
                      "tgl"=>date_2s($va['tgl']),"tgllahir"=>date_2s($va['tgllahir']),"rekening"=>$va['rekening'],
                      "anrekening"=>$va['anrekening'],"bank"=>$va['bank'],"pendidikan_tingkat"=>$va['pendidikan_tingkat']) ;
      $where   = "kode = " . $this->escape($va['kode']) ;
      $this->update("karyawan", $data, $where, "") ;

      //save image
      $arrimg = explode("~",$va ['imgupload']) ;
      foreach($arrimg as $key=>$value){
         if(!empty($value)){
            $vaP = explode("`",$value) ;
            $kodepict = "ftkaryawan~".$va['kode'];
            $filehasil = saveimage($kodepict,$vaP[0],$vaP[1]) ;     

            $arr = array("kode"=>$kodepict,"urlfile"=>$filehasil,"fotoutama"=>$vaP[1]) ;
            $this->update("sys_upload",$arr,"kode = '$kode' and urlfile = '$filehasil'") ;
         }
      }
   }

   public function getdata($kode){
      $data = array() ;
		if($d = $this->getval("*", "kode = " . $this->escape($kode), "karyawan")){
         $data = $d;
		}
		return $data ;
   }

   public function deleting($kode){
      $return = "ok";
      $dbd = $this->select("piutang_kartu", "id", "supplier = '$kode'");
      if ($dbr = $this->getrow($dbd)) {
          $return = "Data tidak bisa dihapus karena sudah digunakan!!";
      } else {
         $this->delete("karyawan", "kode = " . $this->escape($kode)) ;
      }
      return $return;
      
   }

   public function getkode($l=true){
      $y    = date("Y");
      $n    = $this->getincrement("karyawankode" . $y, $l, 6);
      $n    = $y.$n ;
      return $n ;
   }

   public function getfoto($kode){
      $where = "kode LIKE '$kode'" ;
      $dbd      = $this->select("sys_upload", "*", $where, "", "", "id") ;
      return $dbd;
   }
}
?>
