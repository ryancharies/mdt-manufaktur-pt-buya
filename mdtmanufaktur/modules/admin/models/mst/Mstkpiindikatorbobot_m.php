<?php

class Mstkpiindikatorbobot_m extends Bismillah_Model {

    

    public function saving( $va ) {

        $potongan = '0';
        $error = '';
        $va['tgl'] = date_2s($va['tgl']);
        //cek valid
        if ( trim( $va['kode'] ) == '' )$error .= 'Kode indikator tidak valid !! \\n';
        if ( trim( $va['jabatan'] ) == '' )$error .= 'Jabatan tidak valid !! \\n';
        if ( trim( $va['bagian'] ) == '' )$error .= 'Bagian tidak valid !! \\n';

        if ( $error == '' ) {
            $data    = array( 'indikator'=>$va['kode'], 'tgl'=>$va['tgl'], 'jabatan'=>$va['jabatan'], 'bagian'=>$va['bagian'],'bobot'=>string_2n($va['bobot']),
             'username'=> getsession( $this, 'username' ), 'datetime'=>date_now()) ;
            $where   = "indikator = '{$va['kode']}' and tgl = '{$va['tgl']}' and jabatan = '{$va['jabatan']}' and bagian = '{$va['bagian']}'" ;
            $this->update( 'kpi_indikator_bobot', $data, $where, '' ) ;

        }
        //simpan detail
        return $error;
    }

    public function seekjabatan($search){
        $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("jabatan", "*", $where, "", "", "keterangan ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    public function seekbagian($search){
        $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("bagian", "*", $where, "", "", "keterangan ASC", '50') ;
        return array("db"=>$dbd) ;
    }
}
?>
