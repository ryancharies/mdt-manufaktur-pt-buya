<?php
class Mstsurat_m extends Bismillah_Model{
   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(kode LIKE '{$search}%' OR judul LIKE '%{$search}%')" ;
      $where[] = "status = '1'";
      $where 	 = implode(" AND ", $where) ;
      $field = "kode,judul as keterangan,path";
      $join = "";
      $dbd      = $this->select("surat", $field, $where, $join, "", "kode ASC", $limit) ;
      $dba      = $this->select("surat", "id", $where, $join, "", "kode ASC", $limit) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function saving($kode, $va){
      $kode = getsession($this, "sssurat_kode", "");
      $va['kode'] = $kode !== "" ? $va['kode'] : $this->getkode() ;
      $data    = array("kode"=>$va['kode'], "judul"=>$va['keterangan'],
               'username'=> getsession( $this, 'username' ), 'datetime'=>date_now()) ;
      $where   = "kode = " . $this->escape($va['kode']) ;
      $this->update("surat", $data, $where, "") ;

      //upload file
      if(!empty($va['namafile'])){ // jika terisi maka baru saja upload file
         $dir = "./uploads" ;
         if(!is_dir($dir)) mkdir($dir,0777) ;

         $dirdest = "./uploads/surat" ;
         if(!is_dir($dirdest)) mkdir($dirdest,0777) ;

         $destinasi = $dirdest."/surat_".$va['kode'].".docx";
         $fsource = "./tmp/".$va['namafile'];
         copy($fsource,$destinasi) ;
         //if(is_file($filename))unlink($filename) ;  

         $data = array("path"=>"surat_".$va['kode'].".docx");
         $where   = "kode = " . $this->escape($va['kode']) ;
         $this->edit("surat", $data, $where, "") ;
      }
   }

   public function getdata($kode){
      $data = array() ;
		if($d = $this->getval("*", "kode = " . $this->escape($kode), "surat")){
         $data = $d;
		}
		return $data ;
   }

   public function deleting($kode){
      $return = "ok";
      
      $this->edit("surat",array("status"=>'2'), "kode = " . $this->escape($kode)) ;
      
      return $return;
      
   }

   public function getkode($l=true){
      $y    = date("Y");
      $n    = $this->getincrement("suratkode" . $y, $l, 6);
      $n    = $y.$n ;
      return $n ;
   }
   
}
?>
