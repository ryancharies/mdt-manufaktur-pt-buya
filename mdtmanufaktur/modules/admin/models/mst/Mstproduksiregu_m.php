<?php
class Mstproduksiregu_m extends Bismillah_Model{

    public function saving($va, $id){ 
        $kode = getsession($this, "ssmstproduksiregu_id", "");
        $va['kode'] = $kode !== "" ? $kode : $this->getkode() ;
		$va['faktur'] = $kode !== "" ? $va['faktur'] : $this->func_m->getfaktur("AS",$va['tglperolehan'],$va['cabang'],true) ;
        $grid2 = json_decode($va['grid2'],true);
        $vendors = array();
        foreach($grid2 as $key => $val) {
            $vendors[$val['vendor']] = array("vendor"=>$val['vendor'],"namavendor"=>$val['namavendor'],"hutang"=>$val['hutang']);
        }

        $data    = array("kode"=>$va['kode'],"keterangan"=>$va['keterangan'],"golongan"=>$va['golaset'],"kelompok"=>$va['kelaset'],"cabang"=>$va['cabang'],"lama"=>$va['lama'],
                         "tglperolehan"=>date_2s($va['tglperolehan']),"unit"=>string_2n($va['unit']),"hargaperolehan"=>string_2n($va['hp']),
                         "jenispenyusutan"=>$va['jenis'],"tarifpenyusutan"=>string_2n($va['tarifpeny']),"residu"=>string_2n($va['residu']),
						 "faktur"=>$va['faktur'],"vendors"=>json_encode($vendors),"username"=> getsession($this, "username"),"datetime"=>date_now()) ;
        $where   = "kode = " . $this->escape($va['kode']) ;
        $this->update("aset", $data, $where, "") ;
		
		$this->updtransaksi_m->updkartuhutangpembelianaset($va['faktur']);
		$this->updtransaksi_m->updrekpembelianaset($va['faktur']);
    }

    public function getdata($kode=''){
        $where 	 = "a.kode = " . $this->escape($kode);
        $join     = "left join aset_golongan g on g.kode = a.golongan 
                    left join aset_kelompok k on k.kode = a.kelompok
                    left join cabang c on c.kode = a.cabang
					left join supplier s on s.kode = a.vendor";
        $field    = "a.kode,a.keterangan,a.golongan,g.keterangan as ketgolongan,a.kelompok,k.keterangan as ketkelompok,a.cabang, c.keterangan as ketcabang,
                    a.lama,a.tglperolehan,a.hargaperolehan,a.unit,
                    a.jenispenyusutan,a.tarifpenyusutan,a.residu,a.faktur,s.nama as ketvendor,a.vendor,a.vendors";
        $dbd      = $this->select("aset a", $field, $where, $join, "", "a.kode ASC","1") ;
        return $dbd;
    }
    
    public function getkode($cabang,$l=true){
      $y    = date("y").$cabang;
      $n    = $this->getincrement("prodregukode" . $y, $l, 3);
      $n    = $y.$n ;
      return $n ;
	}
}
?>
