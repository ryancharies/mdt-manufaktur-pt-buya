<?php
class Mstgolcustomer_m extends Bismillah_Model{

   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $dbd      = $this->select("customer_golongan", "*", $where, "", "", "kode ASC", $limit) ;
      $dba      = $this->select("customer_golongan", "id", $where) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function saving($kode, $va){
      $data    = array("kode"=>$va['kode'], "keterangan"=>$va['keterangan'],"rekrj"=>$va['rekrj'],"rekpj"=>$va['rekpj'],
                       "rekdisc"=>$va['rekdisc']) ;
      $where   = "kode = " . $this->escape($kode) ;
      $this->update("customer_golongan", $data, $where, "") ;
   }

   public function getdata($kode){
      $data = array() ;
		if($d = $this->getval("*", "kode = " . $this->escape($kode), "customer_golongan")){
         $data = $d;
		}
		return $data ;
   }

   public function deleting($kode){
      $this->delete("customer_golongan", "kode = " . $this->escape($kode)) ;
   }
    
   public function seekrekening($search){
      $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%') and Jenis = 'D'" ;
      $dbd      = $this->select("keuangan_rekening", "*", $where, "", "", "keterangan ASC", '50') ;
      return array("db"=>$dbd) ;
   }

}
?>
