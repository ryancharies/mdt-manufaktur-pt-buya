<?php

class Mstpayrollskalaperiode_m extends Bismillah_Model {

    public function saving( $va ) {

        $potongan = '0';
        $error = '';
        $va['tgl'] = date_2s($va['tgl']);
        //cek valid
        if ( trim( $va['payroll'] ) == '' )$error .= 'Payroll komponen gaji tidak valid !! \\n';
        if ( trim( $va['lamakerja'] ) == '' )$error .= 'Lamakerja tidak valid !! \\n';
        if ( trim( $va['nominal'] ) == '' )$error .= 'Nominal tidak valid !! \\n';

        if ( $error == '' ) {
            $data    = array( 'payroll'=>$va['payroll'], 'tgl'=>$va['tgl'], 'lama'=>$va['lamakerja'], 'nominal'=>$va['nominal'],
             'username'=> getsession( $this, 'username' ), 'datetime'=>date_now() ) ;
            $where   = "payroll = '{$va['payroll']}' and lama = '{$va['lamakerja']}' and tgl = '{$va['tgl']}'" ;
            $this->update( 'payroll_komponen_skala_periode', $data, $where, '' ) ;

        }

        return $error;
    }

}
?>
