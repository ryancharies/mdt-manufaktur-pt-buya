<?php

class Mstkpiindikator_m extends Bismillah_Model {

    public function loadgrid( $va ) {
        $limit    = $va['offset'].','.$va['limit'] ;
        $search	 = isset( $va['search'][0]['value'] ) ? $va['search'][0]['value'] : '' ;
        $search   = $this->escape_like_str( $search ) ;
        $where 	 = array() ;

        if ( $search !== '' ) $where[]	 = "(i.keterangan LIKE '%{$search}%')" ;
        $where 	 = implode( ' AND ', $where ) ;
        $join = 'left join jabatan j on j.kode = i.jabatan';
        $f        = 'i.kode,i.keterangan,i.periode,j.keterangan as jabatan,i.status,i.perhitungan,i.penilaian' ;

        $dbd      = $this->select( 'kpi_indikator i', $f, $where, $join, '', 'i.kode ASC', $limit ) ;

        $row      = 0 ;
        $dba      = $this->select( 'kpi_indikator i', 'i.id', $where, $join, '', '' ) ;

        return array( 'db'=>$dbd, 'rows'=> $this->rows( $dba ) ) ;
    }

    public function saving( $va, $id ) {
        $error = "";
        //cek valid
        $kode = getsession($this, "ssmstkpiindikator_kode", "");
        $va['kode'] = $kode !== "" ? $va['kode'] : $this->getkode() ;
        if ( trim( $va['kode'] ) == '' )$error .= 'Kode tidak boleh kosong !! \\n';
        if ( trim( $va['keterangan'] ) == '' )$error .= 'Keterangan tidak boleh kosong !! \\n';

        if ( $error == '' ) {
            if(!isset($va['jabatan'])) $va['jabatan'] = "";
            $data    = array( 'kode'=>$va['kode'], 'keterangan'=>$va['keterangan'],
                    'perhitungan'=>$va['perhitungan'],
                    'periode'=>$va['periode'],'jabatan'=>$va['jabatan'],'penilaian'=>$va['penilaian'],
                    'username'=> getsession( $this, 'username' ), 'datetime'=>date_now() ) ;
            $where   = 'kode = ' . $this->escape( $va['kode'] ) ;
            $this->update( 'kpi_indikator', $data, $where, '' ) ;          
        }
        //simpan detail
        return $error;
    }

    public function getdata( $kode = '' ) {
        $where 	 = 'i.kode = ' . $this->escape( $kode );
        $join = '';
        $field    = 'i.kode,i.keterangan,i.periode,i.jabatan,i.status,i.perhitungan,i.penilaian';
        $dbd      = $this->select( 'kpi_indikator i', $field, $where, $join, '', '', '1' ) ;
        return $dbd;
    }

    public function getkode($l=true){
        $n    = $this->getincrement("kpiindikatorkode", $l, 4);
        $n    = $n ;
        return $n ;
     }
}
?>
