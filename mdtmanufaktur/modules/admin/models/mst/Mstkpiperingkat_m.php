<?php
class Mstkpiperingkat_m extends Bismillah_Model
{
    public function loadgrid($va)
    {
        $limit = $va['offset'] . "," . $va['limit'];
        $search = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "";
        $search = $this->escape_like_str($search);
        $where = array();
        $where[] = "s.status = '1'";
        if ($search !== "") {
            $where[] = "(s.kode LIKE '{$search}%' OR s.keterangan LIKE '%{$search}%')";
        }

        $where = implode(" AND ", $where);
        $join = "";
        $field = "s.*";
        $dbd = $this->select("kpi_peringkat s", $field, $where, $join, "", "s.peringkat ASC", $limit);
        $dba = $this->select("kpi_peringkat s", "s.id", $where, $join);

        return array("db" => $dbd, "rows" => $this->rows($dba));
    }

    public function saving($kode, $va)
    {
        $kode = getsession($this, "sskpiper_kode", "");
        $va['kode'] = $kode !== "" ? $va['kode'] : $this->getkode();
        // save data stock
        $data = array("kode" => $va['kode'], "keterangan" => $va['keterangan'],"jabatan" => $va['jabatan'], "peringkat" => string_2n($va['peringkat']),
            "nilai_min" => string_2n($va['nilai_min']), "nilai_max" => string_2n($va['nilai_max']),"nominal" => string_2n($va['nominal']),
            "username" => getsession($this, "username"),"datetime"=>date_now());
        $where = "kode = " . $this->escape($va['kode']);
        $this->update("kpi_peringkat", $data, $where, "");

    }
    public function getdata($kode)
    {
        $data = array();
        $where = "s.kode = " . $this->escape($kode);
        $join = "";
        $field = "s.*";
        $dbd = $this->select("kpi_peringkat s", $field, $where, $join, "", "s.kode ASC", "1");
        return $dbd;
    }

    public function deleting($kode)
    {
      $return = "ok";
        $this->edit("kpi_peringkat",array("status"=>'2'), "kode = " . $this->escape($kode));
        
        return $return;
    }

    public function getkode($l = true)
    {
        $n = $this->getincrement("kpiperingkat", $l, 4);
        $n = $n;
        return $n;
    }
}
