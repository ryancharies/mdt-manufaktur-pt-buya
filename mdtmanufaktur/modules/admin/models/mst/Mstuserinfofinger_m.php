<?php
class Mstuserinfofinger_m extends Bismillah_Model{
    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "i.name LIKE '%{$search}%'" ;
        if(isset($va['mesin'])) $where[] = "i.mesin = '{$va['mesin']}'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "i.pin,i.name,i.password,i.groupfp,i.privilege,i.card,i.pin2,i.tz1,i.tz2,i.tz3,i.mesin,m.keterangan as ketmesin";
        $join     = "left join absensi_mesin m on m.kode = i.mesin";
        $dbd      = $this->select("absensi_mesin_userinfo i", $field, $where, $join, "", "i.id ASC") ;
        
        return array("db"=>$dbd) ;
    }

    public function loadgrid2($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        $where 	 = implode(" AND ", $where) ;
        $field    = "kode,keterangan";
        $join     = "";
        $dbd      = $this->select("absensi_mesin", $field, $where, $join, "", "") ;
        
        return array("db"=>$dbd) ;
    }

    public function proses($va){
        $error = "";
        
        if($error == ""){
            $grid = json_decode($va['grid']);
            foreach($grid as $key => $val){
                if($val->status == "Akan diproses"){
                    $arr = array("datetime"=>date_now(),"user"=>getsession($this, "username"),"status"=>0,"jenis"=>0,
                            "cabangentry"=>getsession($this, "cabang"),"pin"=>"","mesin"=>$val->kode);
                    $this->insert("absensi_mesin_dolist",$arr);
                }
            }
        
            $error = "ok";
        }

        return $error;
        
    }


    public function seekmesin($search){
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("absensi_mesin", "*", $where, "", "", "kode ASC", '100') ;
        return array("db"=>$dbd) ;
    }
    

    

}
?>
