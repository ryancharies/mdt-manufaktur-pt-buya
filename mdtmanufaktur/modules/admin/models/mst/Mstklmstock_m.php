<?php
class Mstklmstock_m extends Bismillah_Model{

   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(keterangan LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $f        = "id,kode,keterangan,jenis" ; 
      $dbd      = $this->select("stock_kelompok", $f, $where, "", "", "kode ASC", $limit) ;

      $row      = 0 ;
      $dba      = $this->select("stock_kelompok", "COUNT(id) id", $where) ;
      if($dbra  = $this->getrow($dba)){
         $row   = $dbra['id'] ;
      }
      return array("db"=>$dbd, "rows"=> $row ) ;
   } 

   public function saving($va, $kode){ 

      $kode = getsession($this, "ssmstklmstock_id", "");
      $va['kode'] = $kode !== "" ? $va['kode'] : $this->getkode();
      
      $data    = array("kode"=>$va['kode'],"keterangan"=>$va['keterangan'],"jenis"=>$va['jenis']) ;
      $where   = "kode = " . $this->escape($va['kode']) ;
      $this->update("stock_kelompok", $data, $where, "") ;
   }

   public function getdata($kode=''){
      
      $where 	 = "kode = " . $this->escape($kode);
      $join     = "";
      $field    = "kode,keterangan,jenis";
      $dbd      = $this->select("stock_kelompok", $field, $where, $join, "", "kode ASC","1") ;
	  return $dbd;
   }

   public function getkode($l = true)
    {
        $n = $this->getincrement("klmstock", $l, 3);
        return $n;
    }

}
?>
