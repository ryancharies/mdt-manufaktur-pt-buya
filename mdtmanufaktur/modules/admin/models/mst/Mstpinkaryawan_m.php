<?php

class Mstpinkaryawan_m extends Bismillah_Model {

    public function loadgrid( $va ) {
        $limit    = $va['offset'].','.$va['limit'] ;
        $search	 = isset( $va['search'][0]['value'] ) ? $va['search'][0]['value'] : '' ;
        $search   = $this->escape_like_str( $search ) ;
        $where 	 = array() ;

        if ( $search !== '' ) $where[]	 = "(u.nama LIKE '%{$search}%') or (u.kode LIKE '%{$search}%')" ;        
       // $where = u        

        $where 	 = implode( ' AND ', $where ) ;
        $join = '';
        $f        = 'u.kode,u.nama,u.alamat,u.telepon' ;

        $dbd      = $this->select( 'karyawan u', $f, $where, $join, '', 'u.kode ASC', $limit ) ;

        $row      = 0 ;
        $dba      = $this->select( 'karyawan u', 'u.id', $where, $join, '', '' ) ;

        return array( 'db'=>$dbd, 'rows'=> $this->rows( $dba ) ) ;
    }

    public function saving( $va ) {

        $potongan = '0';
        $error = '';
        $va['tgl'] = date_2s($va['tgl']);
        //cek valid
        if ( trim( $va['mesin'] ) == '' )$error .= 'Mesin absensi tidak valid !! \\n';
        if ( trim( $va['nip'] ) == '' )$error .= 'NIP tidak valid !! \\n';
        if ( trim( $va['pin'] ) == '' )$error .= 'PIN tidak valid !! \\n';

        if ( $error == '' ) {
            $data    = array( 'mesin'=>$va['mesin'], 'tgl'=>date("Y-m-d"), 'nip'=>$va['nip'], 'pin'=>$va['pin'],
             'username'=> getsession( $this, 'username' ), 'datetime'=>date_now() ) ;
            $where   = "mesin = '{$va['mesin']}' and nip = '{$va['nip']}'" ;
            $this->update( 'absensi_karyawan_pin', $data, $where, '' ) ;

        }
        //simpan detail
        return $error;
    }

    public function getdata( $kode = '' ) {
        $where 	 = 'u.kode = ' . $this->escape( $kode );
        $join     = '';
        $field    = 'u.kode,u.nama,u.alamat,u.telepon';
        $dbd      = $this->select( 'karyawan u', $field, $where, $join, '', '', '1' ) ;
        return $dbd;
    }

    public function loadmesin( $va ){
        $where = "";
        $field = "m.kode,m.keterangan,p.pin";
        $join = "left join absensi_karyawan_pin p on p.mesin = m.kode and p.nip = '{$va['nip']}'";
        $dbd = $this->select( 'absensi_mesin m', $field, $where, $join, '', 'kode ASC' );
        return array( 'db' => $dbd );
    }
}
?>
