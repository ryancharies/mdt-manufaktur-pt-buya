<?php
class Mstkodeabsensi_m extends Bismillah_Model{

   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(u.keterangan LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $join = "";
      $f        = "u.kode,u.keterangan,u.jenis" ; 
      $dbd      = $this->select("absensi_kode u", $f, $where, $join, "", "u.kode ASC", $limit) ;

      $row      = 0 ;
      $dba      = $this->select("absensi_kode u", "u.id", $where, $join, "", "") ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba)) ;
   } 

   public function saving($va, $id){ 
      $data    = array("kode"=>$va['kode'],"keterangan"=>$va['keterangan'],"jenis"=>$va['jenis'],"username"=> getsession($this, "username"),
               "datetime"=>date_now()) ;
      $where   = "kode = " . $this->escape($va['kode']) ;
      $this->update("absensi_kode", $data, $where, "") ;
   }

   public function getdata($kode=''){
      $where 	 = "u.kode = " . $this->escape($kode);
      $join     = "";
      $field    = "u.kode,u.keterangan,u. jenis";
      $dbd      = $this->select("absensi_kode u", $field, $where, $join, "", "","1") ;
	  return $dbd;
   }
}
?>
