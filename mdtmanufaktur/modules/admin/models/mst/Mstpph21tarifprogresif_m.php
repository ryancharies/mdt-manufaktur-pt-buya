<?php

class Mstpph21tarifprogresif_m extends Bismillah_Model {

    public function loadgrid( $va ) {
        $limit    = $va['offset'].','.$va['limit'] ;
        $search	 = isset( $va['search'][0]['value'] ) ? $va['search'][0]['value'] : '' ;
        $search   = $this->escape_like_str( $search ) ;
        $where 	 = array() ;

        if ( $search !== '' ) $where[]	 = "(p.tgl LIKE '%{$search}%')" ;  

        $where 	 = implode( ' AND ', $where ) ;
        $join = '';
        $f        = 'p.tgl,p.minpenghasilanthn penghasilanthn,p.npwp,p.perstarif' ;

        $dbd      = $this->select( 'pajak_pph21_tarifprogresif p', $f, $where, $join, '', 'p.tgl desc,p.npwp desc,p.minpenghasilanthn asc', $limit ) ;
        $arrreturn = array();
        while($dbr = $this->getrow($dbd)){
            if(!isset($arrreturn[$dbr['tgl']])) $arrreturn[$dbr['tgl']] = array("tgl"=>$dbr['tgl'],"npwp"=>"","nonnpwp"=>"");
            if($dbr['npwp'] == "1")$arrreturn[$dbr['tgl']]['npwp'] .= "penghasilan Rp. ".string_2s($dbr['minpenghasilanthn']) ." tarif ". $dbr['perstarif'] . " % <br/>";
            if($dbr['npwp'] == "0")$arrreturn[$dbr['tgl']]['nonnpwp'] .= "penghasilan Rp. ".string_2s($dbr['minpenghasilanthn']) ." tambahan tarif ". $dbr['perstarif'] . " %<br/>";
        }

        return $arrreturn;
    }

    public function loadgrid2( $va ) {
        $where 	 = array() ;
        $va['tgl'] = date_2s($va['tgl']);
        //get tgl terakhir setting
        $data = array();
        $dbd      = $this->select( 'pajak_pph21_tarifprogresif p', "p.tgl", "p.tgl <= '{$va['tgl']}'", '', '', 'p.tgl desc', "1" ) ;
        if($dbr = $this->getrow($dbd)){
            $tgl = $dbr['tgl'];
            
            $dba      = $this->select( 'pajak_pph21_tarifprogresif p', 'p.perstarif,p.minpenghasilanthn', "p.tgl = '$tgl' and p.npwp = '1'", "", '', 'p.tgl desc' ) ;
            while($dbra = $this->getrow($dba)){
                $data[] = array("perstarif"=>$dbra['perstarif'],"penghasilan"=>$dbra['minpenghasilanthn']);
            }
        }
        
        return $data;
    }

    public function loadgrid3( $va ) {
        $where 	 = array() ;
        $va['tgl'] = date_2s($va['tgl']);
        //get tgl terakhir setting
        $data = array();
        $dbd      = $this->select( 'pajak_pph21_tarifprogresif p', "p.tgl", "p.tgl <= '{$va['tgl']}'", '', '', 'p.tgl desc', "1" ) ;
        if($dbr = $this->getrow($dbd)){
            $tgl = $dbr['tgl'];
            
            $dba      = $this->select( 'pajak_pph21_tarifprogresif p', 'p.perstarif,p.minpenghasilanthn', "p.tgl = '$tgl' and p.npwp = '0'", "", '', 'p.tgl desc' ) ;
            while($dbra = $this->getrow($dba)){
                $data[] = array("perstarif"=>$dbra['perstarif'],"penghasilan"=>$dbra['minpenghasilanthn']);
            }
        }
        
        return $data;
    }

    public function saving( $va ) {
        //print_r($va);
        $potongan = '0';
        $error = '';
        $va['tgl'] = date_2s($va['tgl']);

        $this->delete("pajak_pph21_tarifprogresif","tgl = '{$va['tgl']}'");        
        $arrgrid2 = json_decode($va['grid2']);
        foreach($arrgrid2 as $key => $val){
            if($val->penghasilan > 0 || $val->perstarif > 0){
                $arrdata = array("tgl"=>$va['tgl'],"npwp"=>"1","minpenghasilanthn"=>string_2n($val->penghasilan),
                        "perstarif"=>string_2n($val->perstarif),"username"=> getsession($this, "username"),
                        "datetime"=>date_now());
                $this->insert("pajak_pph21_tarifprogresif",$arrdata);
            }            
        }

        $arrgrid3 = json_decode($va['grid3']);
        foreach($arrgrid3 as $key => $val){
            if($val->penghasilan > 0 || $val->perstarif > 0){
                $arrdata = array("tgl"=>$va['tgl'],"npwp"=>"0","minpenghasilanthn"=>string_2n($val->penghasilan),
                        "perstarif"=>string_2n($val->perstarif),"username"=> getsession($this, "username"),
                        "datetime"=>date_now());
                $this->insert("pajak_pph21_tarifprogresif",$arrdata);
            }            
        }

        return $error;
    }

    public function getdata( $kode = '' ) {
        $where 	 = 'u.kode = ' . $this->escape( $kode );
        $join     = '';
        $field    = 'u.kode,u.keterangan,u.status,u.perhitungan,u.periode,u.potongan,u.rekening';
        $dbd      = $this->select( 'payroll_komponen u', $field, $where, $join, '', '', '1' ) ;
        return $dbd;
    }

    public function getdataskala($payroll) {
        $where 	 = 'u.payroll = ' . $this->escape( $payroll );
        $join     = 'left join bagian j on j.kode = u.bagian';
        $field    = 'u.payroll,j.keterangan bagian,u.tgl,u.nominal';
        $dbd      = $this->select( 'payroll_komponen_skala_bagian u', $field, $where, $join, '', 'u.tgl asc', '' ) ;
        return $dbd;
    }

    public function loadbagian( $va ){
        $where = '';
        $field = 'kode,keterangan';
        $join = "";
        $dbd = $this->select( 'bagian', $field, $where, $join, '', 'kode ASC' );
        return array( 'db' => $dbd );
    }
}
?>
