<?php
class Mstdati2_m extends Bismillah_Model{

   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(k.keterangan LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $join     = "left join dati_1 p on p.kode = k.dati_1";
      $f        = "k.id,k.kode,k.keterangan,p.keterangan dati_1" ; 
      $dbd      = $this->select("dati_2 k", $f, $where, $join, "", "k.kode ASC", $limit) ;

      $row      = 0 ;
      $dba      = $this->select("dati_2 k", "COUNT(k.id) id", $where, $join) ;
      if($dbra  = $this->getrow($dba)){
         $row   = $dbra['id'] ;
      }
      return array("db"=>$dbd, "rows"=> $row ) ;
   } 

   public function saving($va, $kode){ 

      $kode = getsession($this, "ssmstdati2_id", "");
      $va['kode'] = $kode !== "" ? $va['kode'] : $this->getkode();
      
      $data    = array("kode"=>$va['kode'],"keterangan"=>$va['keterangan'],"dati_1"=>$va['dati_1']) ;
      $where   = "kode = " . $this->escape($va['kode']) ;
      $this->update("dati_2", $data, $where, "") ;
   }

   public function getdata($kode=''){
      
      $where 	 = "k.kode = " . $this->escape($kode);
      $join     = "left join dati_1 p on p.kode = k.dati_1";
      $field    = "k.kode,k.keterangan,k.dati_1,p.keterangan as ketdati1";
      $dbd      = $this->select("dati_2 k", $field, $where, $join, "", "k.kode ASC","1") ;
	  return $dbd;
   }

   public function getkode($l = true)
    {
        $n = $this->getincrement("dati2", $l, 5);
        return $n;
    }

}
?>
