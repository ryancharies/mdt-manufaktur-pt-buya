<?php

class Mstpayrollkomponen_m extends Bismillah_Model {

    public function loadgrid( $va ) {
        $limit    = $va['offset'].','.$va['limit'] ;
        $search	 = isset( $va['search'][0]['value'] ) ? $va['search'][0]['value'] : '' ;
        $search   = $this->escape_like_str( $search ) ;
        $where 	 = array() ;

        if ( $search !== '' ) $where[]	 = "(u.keterangan LIKE '%{$search}%')" ;
        $where 	 = implode( ' AND ', $where ) ;
        $join = '';
        $f        = 'u.kode,u.keterangan,u.periode,u.perhitungan,u.potongan,u.status,
                    u.diluarjadwal,u.stepskala,u.potterlambat,u.potplgcepat,u.kalkulasi,u.perhitunganpph21,
                    u.periodemulai,u.resetperiode' ;

        $dbd      = $this->select( 'payroll_komponen u', $f, $where, $join, '', 'u.kode ASC', $limit ) ;

        $row      = 0 ;
        $dba      = $this->select( 'payroll_komponen u', 'u.id', $where, $join, '', '' ) ;

        return array( 'db'=>$dbd, 'rows'=> $this->rows( $dba ) ) ;
    }

    public function saving( $va, $id ) {

        $potongan = '0';
        $potterlambat = '0';
        $potplgcepat = '0';
        $diluarjadwal = '0';
        $kalkulasi = '0';
        $perhitunganpph21 = '0';
        $resetperiode = '0';
        $error = '';

        //cek valid
        if ( empty( $va['grid2select'] ) && $va['periode'] == 'H' )$error .= 'Absensi kode belum dipilih !! \\n';
        if ( !empty( $va['grid2select'] ) && $va['periode'] <> 'H' )$error .= 'Absensi kode hanya untuk periode hari !! \\n';
        if ( !isset( $va['periode'] ) )$error .= 'Periode belum dipilih !! \\n';
        if ( trim( $va['kode'] ) == '' )$error .= 'Kode tidak boleh kosong !! \\n';
        if ( trim( $va['keterangan'] ) == '' )$error .= 'Kode tidak boleh kosong !! \\n';

        if ( $error == '' ) {
            if ( isset( $va['potongan'] ) )$potongan = '1';
            if ( isset( $va['potterlambat'] ) )$potterlambat = '1';
            if ( isset( $va['potplgcepat'] ) )$potplgcepat = '1';
            if ( isset( $va['diluarjadwal'] ) )$diluarjadwal = '1';
            if ( isset( $va['kalkulasi'] ) )$kalkulasi = '1';
            if ( isset( $va['perhitunganpph21'] ) )$perhitunganpph21 = '1';
            if ( isset( $va['resetperiode'] ) )$resetperiode = '1';
            $data    = array( 'kode'=>$va['kode'], 'keterangan'=>$va['keterangan'], 'periode'=>$va['periode'], 'perhitungan'=>$va['perhitungan'],
                    'potongan'=>$potongan,'potterlambat'=>$potterlambat,'potplgcepat'=>$potplgcepat,'kalkulasi'=>$kalkulasi,
                    'diluarjadwal'=>$diluarjadwal,'stepskala'=>$va['stepskala'],'periodemulai'=>$va['periodemulai'],'perhitunganpph21'=>$perhitunganpph21,
                    'resetperiode'=>$resetperiode,
                    'username'=> getsession( $this, 'username' ), 'datetime'=>date_now() ) ;
            $where   = 'kode = ' . $this->escape( $va['kode'] ) ;
            $this->update( 'payroll_komponen', $data, $where, '' ) ;

            //save kode absensi
            $grid2 = json_decode( $va['grid2'] );
            $absensikodeselect = 'array('.$va['grid2select'].');';
            eval( '$absensikodeselect ='.$absensikodeselect );
            $this->delete( 'payroll_komponen_absensi', "payroll = '{$va['kode']}'" );
            $n = 0 ;
            foreach ( $grid2 as $key => $val ) {
                $n++;

                //dicek apakah data tsb di select
                $select = false;
                foreach ( $absensikodeselect as $keyslct => $valslct ) {
                    if ( $valslct == $n ) {
                        $select = true;
                    }
                }

                if ( $select ) {
                    $vadetail = array( 'payroll' => $va['kode'],
                    'absensi' => $val->kode );
                    $this->insert( 'payroll_komponen_absensi', $vadetail );
                }
            }

            //save rekening akt
            $grid3 = json_decode( $va['grid3'] );
            $this->delete( 'payroll_komponen_rekening_bagian', "payroll = '{$va['kode']}'" );
            $n = 0 ;
            foreach ( $grid3 as $key => $val ) {
                $rekening = "";
                if($val->rekening <> ""){
                    $arrrek = explode("|",$val->rekening);
                    $rekening = trim($arrrek[1]);
                }
                $vadetail = array( 'tgl'=>date("Y-m-d"),'payroll' => $va['kode'],'bagian' =>  $val->kode,
                            "rekening"=>$rekening  );
                $this->insert( 'payroll_komponen_rekening_bagian', $vadetail );
                
            }

        }
        //simpan detail
        return $error;
    }

    public function getdata( $kode = '' ) {
        $where 	 = 'u.kode = ' . $this->escape( $kode );
        $join     = '';
        $field    = 'u.kode,u.keterangan,u.status,u.perhitungan,u.periode,u.kalkulasi,
                    u.potongan,u.potterlambat,u.potplgcepat,u.diluarjadwal,u.stepskala,u.perhitunganpph21,
                    u.periodemulai,u.resetperiode';
        $dbd      = $this->select( 'payroll_komponen u', $field, $where, $join, '', '', '1' ) ;
        return $dbd;
    }

    public function loadabsensikode( $va ){
        $where = '';
        $field = 'a.kode,a.keterangan,a.jenis,k.absensi';
        $join = "left join payroll_komponen_absensi k on k.absensi = a.kode and k.payroll = '{$va['kode']}'";
        $dbd = $this->select( 'absensi_kode a', $field, $where, $join, '', 'a.kode ASC' );
        return array( 'db' => $dbd );
    }

    public function loadrekeningbagian( $va ){
        $where = '';
        $field = 'b.kode,b.keterangan,a.rekening,r.keterangan as ketrekening';
        $join = "left join payroll_komponen_rekening_bagian a on a.bagian = b.kode and a.payroll = '{$va['kode']}'
                 left join keuangan_rekening r on r.kode = a.rekening";
        $dbd = $this->select( 'bagian b', $field, $where, $join, '', 'b.kode ASC' );
        return array( 'db' => $dbd );
    }

    public function seekrekening( $search ){
        $where = "jenis = 'D' and (kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')";
        $field = 'kode,keterangan';
        $join = "";
        $dbd = $this->select( 'keuangan_rekening', $field, $where, $join, '', 'kode ASC' );
        return array( 'db' => $dbd );
    }
}
?>
