<?php

class Mstpayrollskalakaryawan_m extends Bismillah_Model {

    public function loadgrid( $va ) {
        $limit    = $va['offset'].','.$va['limit'] ;
        $search	 = isset( $va['search'][0]['value'] ) ? $va['search'][0]['value'] : '' ;
        $search   = $this->escape_like_str( $search ) ;
        $where 	 = array() ;

        if ( $search !== '' ) $where[]	 = "(u.nama LIKE '%{$search}%') or (u.kode LIKE '%{$search}%')" ;        
       // $where = u        

        $where 	 = implode( ' AND ', $where ) ;
        $join = '';
        $f        = 'u.kode,u.nama,u.alamat,u.telepon' ;

        $dbd      = $this->select( 'karyawan u', $f, $where, $join, '', 'u.kode ASC', $limit ) ;

        $row      = 0 ;
        $dba      = $this->select( 'karyawan u', 'u.id', $where, $join, '', '' ) ;

        return array( 'db'=>$dbd, 'rows'=> $this->rows( $dba ) ) ;
    }

    public function saving( $va ) {

        $potongan = '0';
        $error = '';
        $va['tgl'] = date_2s($va['tgl']);
        //cek valid
        if ( trim( $va['payroll'] ) == '' )$error .= 'Payroll komponen gaji tidak valid !! \\n';
        if ( trim( $va['nip'] ) == '' )$error .= 'NIP tidak valid !! \\n';
        if ( trim( $va['nominal'] ) == '' )$error .= 'Nominal tidak valid !! \\n';

        if ( $error == '' ) {
            $data    = array( 'payroll'=>$va['payroll'], 'tgl'=>$va['tgl'], 'nip'=>$va['nip'], 'nominal'=>$va['nominal'],
             'username'=> getsession( $this, 'username' ), 'datetime'=>date_now() ) ;
            $where   = "payroll = '{$va['payroll']}' and nip = '{$va['nip']}' and tgl = '{$va['tgl']}'" ;
            $this->update( 'payroll_komponen_skala_karyawan', $data, $where, '' ) ;

        }
        //simpan detail
        return $error;
    }

    public function getdata( $kode = '' ) {
        $where 	 = 'u.kode = ' . $this->escape( $kode );
        $join     = '';
        $field    = 'u.kode,u.nama,u.alamat,u.telepon';
        $dbd      = $this->select( 'karyawan u', $field, $where, $join, '', '', '1' ) ;
        return $dbd;
    }

    public function getdataskala($payroll) {
        $where 	 = 'u.payroll = ' . $this->escape( $payroll );
        $join     = 'left join jabatan j on j.kode = u.jabatan';
        $field    = 'u.payroll,j.keterangan jabatan,u.tgl,u.nominal';
        $dbd      = $this->select( 'payroll_komponen_skala_jabatan u', $field, $where, $join, '', 'u.tgl asc', '' ) ;
        return $dbd;
    }

    public function loadkomponen( $va ){
        $where = "perhitungan = 'C' and status = '1'" ;

        $field = 'kode,keterangan,periode,resetperiode';
        $join = "";
        $dbd = $this->select( 'payroll_komponen', $field, $where, $join, '', 'kode ASC' );
        return array( 'db' => $dbd );
    }
}
?>
