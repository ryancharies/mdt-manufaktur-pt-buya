<?php
class Mstsurathr_m extends Bismillah_Model{
	public function loadgrid($va){
	    $limit    	= $va['offset'].",".$va['limit'] ;
	    $search	 	= isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
		$search   	= $this->escape_like_str($search) ;
	    $where 	 	= array() ; 
		if($search !== "") $where[]	= "(Title LIKE '%{$search}%')" ;
		$where[]	= "status = '1'";
    	$where 	 	= implode(" AND ", $where) ;
      	$f        	= "*" ; 
      	$dbd      	= $this->select("surat",$f,$where,"","","Kode ASC",$limit) ;

      	$row      	= 0 ;
      	$dba      	= $this->select("surat", "COUNT(ID) id", $where) ;
      	if($dbra  = $this->getrow($dba)){
         	$row   = $dbra['id'] ;
      	}
      	return  array("db"=>$dbd, "rows"=> $row ) ;
   	} 

   	public function SaveDraft($va)
   	{
   		$vaData    = array("kode" 	=> $va['cKode'] ,
						"judul" 	=> $va['cJudul'] ,
						"tgl" 		=> date_2s($va['dTgl']) ,
						"username" 	=> getsession($this, "username"),
						"datetime" 	=> date("Y-m-d H:i:s"));
		$cWhere   = "kode = " . $this->escape($va['cKode']) ;		
		$this->update("surat", $vaData, $cWhere) ;			
	}
	   
	public function GetDataSurat($cKode)
	{
		$vaRetval 	= array() ;
		$cField 	= "*" ;
		$cTable		= "surat" ; 
		$cWhere 	= "kode = '$cKode'" ;
		$dbData 	= $this->select($cTable,$cField,$cWhere); 
		if($dbRow = $this->getrow($dbData)){
			$vaRetval = $dbRow ; 
		}
		return $vaRetval ;
	}

	public function deleting($cKode)
	{
		$cTable = "surat" ;
		$vaUpdate = array("status"=>'0');
		$cWhere = "kode = '$cKode'" ;
		$this->update($cTable,$vaUpdate,$cWhere);
	}
}
?>