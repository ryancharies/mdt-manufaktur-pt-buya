<?php
class Mstptkpjmltanggungan_m extends Bismillah_Model{
   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $field = "kode,keterangan";
      $join = "";
      $dbd      = $this->select("pajak_ptkp", $field, $where, $join, "", "kode ASC", $limit) ;
      $dba      = $this->select("pajak_ptkp", "id", $where) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function loadgrid2($va){
      $where 	 = array() ; 
      $where[]	= "j.kode = '{$va['kode']}'" ;
      $where 	 = implode(" AND ", $where) ;
      $field = "j.kode,j.tgl,j.jumlahtanggungan jmltanggungan";
      $join = "left join pajak_ptkp p on p.kode = j.kode";
      $dbd      = $this->select("pajak_ptkp_jumlahtanggungan j", $field, $where, $join, "", "j.tgl desc") ;
      $dba      = $this->select("pajak_ptkp_jumlahtanggungan j", "j.id", $where) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function saving($va){
      $error = "";

      $va['tgl'] = date_2s($va['tgl']);
      if(string_2n($va['jmltanggungan']) < 0)$error .= "Jumlah tanggungan tidak valid.. \\n";
      if($va['kode'] == "")$error .= "Kode belum diisi.. \\n";
      if($va['tgl'] == "")$error .= "Tgl belum diisi.. \\n";
      if($error == ""){
         $data    = array("kode"=>$va['kode'], "jumlahtanggungan"=>string_2n($va['jmltanggungan']),
                  "tgl"=>date_2s($va['tgl']),"username"=> getsession($this, "username"),
                  "datetime"=>date_now()) ;
         $where   = "kode = '{$va['kode']}' and tgl = '{$va['tgl']}'";
         $this->update("pajak_ptkp_jumlahtanggungan", $data, $where) ;
         $error = "ok";
      }      
      return $error;
   }

   public function getdata($kode){
      $data = array() ;
		if($d = $this->getval("*", "kode = " . $this->escape($kode), "pajak_ptkp")){
         $data = $d;
		}
		return $data ;
   }

   public function deleting($va){
      $return = "ok";
      $va['tgl'] = date_2s($va['tgl']);
      $this->delete("pajak_ptkp_jumlahtanggungan", "kode = '{$va['kode']}' and tgl = '{$va['tgl']}'") ;
      
      return $return;
      
   }

}
?>
