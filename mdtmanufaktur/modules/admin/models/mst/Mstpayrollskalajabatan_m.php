<?php

class Mstpayrollskalajabatan_m extends Bismillah_Model {

    public function loadgrid( $va ) {
        $limit    = $va['offset'].','.$va['limit'] ;
        $search	 = isset( $va['search'][0]['value'] ) ? $va['search'][0]['value'] : '' ;
        $search   = $this->escape_like_str( $search ) ;
        $where 	 = array() ;

        if ( $search !== '' ) $where[]	 = "(u.keterangan LIKE '%{$search}%')" ;        
        $where[]	 = "(u.perhitungan = 'J')" ;
        $where[]	 = "u.status = '1'" ;

        $where 	 = implode( ' AND ', $where ) ;
        $join = '';
        $f        = 'u.kode,u.keterangan,u.periode,u.perhitungan,u.potongan,u.status' ;

        $dbd      = $this->select( 'payroll_komponen u', $f, $where, $join, '', 'u.kode ASC', $limit ) ;

        $row      = 0 ;
        $dba      = $this->select( 'payroll_komponen u', 'u.id', $where, $join, '', '' ) ;

        return array( 'db'=>$dbd, 'rows'=> $this->rows( $dba ) ) ;
    }

    public function saving( $va ) {

        $potongan = '0';
        $error = '';
        $va['tgl'] = date_2s($va['tgl']);
        //cek valid
        if ( trim( $va['payroll'] ) == '' )$error .= 'Payroll komponen gaji tidak valid !! \\n';
        if ( trim( $va['jabatan'] ) == '' )$error .= 'Jabatan tidak valid !! \\n';
        if ( trim( $va['nominal'] ) == '' )$error .= 'Nominal tidak valid !! \\n';

        if ( $error == '' ) {
            $data    = array( 'payroll'=>$va['payroll'], 'tgl'=>$va['tgl'], 'jabatan'=>$va['jabatan'], 'nominal'=>$va['nominal'],
             'username'=> getsession( $this, 'username' ), 'datetime'=>date_now() ) ;
            $where   = "payroll = '{$va['payroll']}' and jabatan = '{$va['jabatan']}' and tgl = '{$va['tgl']}'" ;
            $this->update( 'payroll_komponen_skala_jabatan', $data, $where, '' ) ;

        }
        //simpan detail
        return $error;
    }

    public function getdata( $kode = '' ) {
        $where 	 = 'u.kode = ' . $this->escape( $kode );
        $join     = '';
        $field    = 'u.kode,u.keterangan,u.status,u.perhitungan,u.periode,u.potongan';
        $dbd      = $this->select( 'payroll_komponen u', $field, $where, $join, '', '', '1' ) ;
        return $dbd;
    }

    public function getdataskala($payroll) {
        $where 	 = 'u.payroll = ' . $this->escape( $payroll );
        $join     = 'left join jabatan j on j.kode = u.jabatan';
        $field    = 'u.payroll,j.keterangan jabatan,u.tgl,u.nominal';
        $dbd      = $this->select( 'payroll_komponen_skala_jabatan u', $field, $where, $join, '', 'u.tgl asc', '' ) ;
        return $dbd;
    }

    public function loadjabatan( $va ){
        $where = '';
        $field = 'kode,keterangan';
        $join = "";
        $dbd = $this->select( 'jabatan', $field, $where, $join, '', 'kode ASC' );
        return array( 'db' => $dbd );
    }
}
?>
