<?php
class Mstdatastock_m extends Bismillah_Model
{
    public function loadgrid($va)
    {
        $limit = $va['offset'] . "," . $va['limit'];
        $search = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "";
        $search = $this->escape_like_str($search);
        $where = array();
        if ($search !== "") {
            $where[] = "(s.kode LIKE '{$search}%' OR s.keterangan LIKE '%{$search}%')";
        }

        $where = implode(" AND ", $where);
        $join = "left join stock_group g on g.Kode = s.stock_group 
            left join stock_kelompok k on k.kode = s.stock_kelompok";
        $field = "s.*,g.Keterangan as KetStockGroup,k.keterangan as ketkelompok";
        $dbd = $this->select("stock s", $field, $where, $join, "", "s.kode ASC", $limit);
        $dba = $this->select("stock s", "s.id", $where, $join);

        return array("db" => $dbd, "rows" => $this->rows($dba));
    }

    public function saving($kode, $va)
    {
        $kode = getsession($this, "ssstock_kode", "");
        $va['kode'] = $kode !== "" ? $va['kode'] : $this->getkode();
        // save data stock
        $data = array("kode" => $va['kode'], "barcode" => $va['barcode'], "satuan" => $va['satuan'], "stock_group" => $va['group'],
            "stock_kelompok" => $va['kelompok'], "hargajual" => $va['hargajual'],
            "tampil" => $va['tampil'], "keterangan" => $va['keterangan'], "username" => getsession($this, "username"));
        $where = "kode = " . $this->escape($va['kode']);
        $this->update("stock", $data, $where, "");

    }
    public function getdata($kode)
    {
        $data = array();
        $where = "s.kode = " . $this->escape($kode);
        $join = "left join stock_group g on g.Kode = s.stock_group
                left join stock_kelompok k on k.kode = s.stock_kelompok 
                left join satuan t on t.kode = s.satuan";
        $field = "s.*,g.Keterangan as KetStockGroup,t.Keterangan as KetSatuan,k.keterangan as ketkelompok";
        $dbd = $this->select("stock s", $field, $where, $join, "", "s.kode ASC", "1");
        return $dbd;
    }

    public function deleting($kode)
    {
      $return = "ok";
        $dbd = $this->select("stock_kartu", "id", "stock = '$kode'");
        if ($dbr = $this->getrow($dbd)) {
            $return = "Data tidak bisa dihapus karena sudah digunakan!!";
        } else {
            $this->delete("stock", "kode = " . $this->escape($kode));
        }
        return $return;
    }

    public function getkode($l = true)
    {
        $y = date("ym");
        $n = $this->getincrement("stockkode" . $y, $l, 6);
        $n = $y . $n;
        return $n;
    }
}
