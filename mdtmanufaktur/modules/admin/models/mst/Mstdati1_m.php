<?php
class Mstdati1_m extends Bismillah_Model{

   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(keterangan LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $f        = "id,kode,keterangan" ; 
      $dbd      = $this->select("dati_1", $f, $where, "", "", "kode ASC", $limit) ;

      $row      = 0 ;
      $dba      = $this->select("dati_1", "COUNT(id) id", $where) ;
      if($dbra  = $this->getrow($dba)){
         $row   = $dbra['id'] ;
      }
      return array("db"=>$dbd, "rows"=> $row ) ;
   } 

   public function saving($va, $kode){ 

      $kode = getsession($this, "ssmstdati1_id", "");
      $va['kode'] = $kode !== "" ? $va['kode'] : $this->getkode();
      
      $data    = array("kode"=>$va['kode'],"keterangan"=>$va['keterangan']) ;
      $where   = "kode = " . $this->escape($va['kode']) ;
      $this->update("dati_1", $data, $where, "") ;
   }

   public function getdata($kode=''){
      
      $where 	 = "kode = " . $this->escape($kode);
      $join     = "";
      $field    = "kode,keterangan";
      $dbd      = $this->select("dati_1", $field, $where, $join, "", "kode ASC","1") ;
	  return $dbd;
   }

   public function getkode($l = true)
    {
        $n = $this->getincrement("dati1", $l, 3);
        return $n;
    }

}
?>
