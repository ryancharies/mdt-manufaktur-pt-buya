<?php
class Mstasuransi_m extends Bismillah_Model{

   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(b.keterangan LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $join = "left join payroll_komponen r on r.kode = b.payroll";
      $f        = "b.id,b.kode,b.keterangan,b.payroll,r.keterangan as ketpayroll" ; 
      $dbd      = $this->select("asuransi b", $f, $where, $join, "", "b.kode ASC", $limit) ;

      $row      = 0 ;
      $dba      = $this->select("asuransi b", "COUNT(b.id) id", $where) ;
      if($dbra  = $this->getrow($dba)){
         $row   = $dbra['id'] ;
      }
      return array("db"=>$dbd, "rows"=> $row ) ;
   } 

   public function saving($va, $id){ 
      $data    = array("kode"=>$va['kode'],"keterangan"=>$va['keterangan'],"payroll"=>$va['payroll']) ;
      $where   = "kode = " . $this->escape($va['kode']) ;
      $this->update("asuransi", $data, $where, "") ;
   }

   public function getdata($kode=''){
      $where 	 = "b.kode = " . $this->escape($kode);
      $join     = "left join payroll_komponen r on r.kode = b.payroll";
      $field    = "b.id,b.kode,b.keterangan,b.payroll,r.keterangan as ketpayroll";
      $dbd      = $this->select("asuransi b", $field, $where, $join, "", "b.kode ASC","1") ;
	  return $dbd;
   }

   public function seekpayroll($search){
      $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%') and perhitungan = 'A'" ;
      $dbd      = $this->select("payroll_komponen", "*", $where, "", "", "keterangan ASC", '50') ;
      return array("db"=>$dbd) ;
   }
}
?>
