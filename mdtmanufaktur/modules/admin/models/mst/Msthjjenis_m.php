<?php
class Msthjjenis_m extends Bismillah_Model{

   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(keterangan LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $f        = "id,kode,keterangan" ; 
      $dbd      = $this->select("hj_jenis", $f, $where, "", "", "kode ASC", $limit) ;

      $row      = 0 ;
      $dba      = $this->select("hj_jenis", "COUNT(id) id", $where) ;
      if($dbra  = $this->getrow($dba)){
         $row   = $dbra['id'] ;
      }
      return array("db"=>$dbd, "rows"=> $row ) ;
   } 

   public function saving($va, $id){ 
      $data    = array("kode"=>$va['kode'],"keterangan"=>$va['keterangan']) ;
      $where   = "kode = " . $this->escape($va['kode']) ;
      $this->update("hj_jenis", $data, $where, "") ;

      //insert detail hj
      $vaGrid = json_decode($va['grid2']);
      foreach ($vaGrid as $key => $val) {
          $vadetail = array("hjjenis" => $va['kode'],
              "stock" => $val->kode,
              "hj" => $val->hj,
              "status" => "1",
              "username" => getsession($this, "username"),
              "datetime"=>date_now());
          $this->update("stock_hj_jenis", $vadetail,"hjjenis='{$va['kode']}' and stock = '{$val->kode}'");
      }
   }

   public function getdata($kode=''){
      /*$w    = "kode = " . $this->escape($kode) ;  
      $d    = $this->getval("id,kode,keterangan", $w, "aset_golongan") ;
      return !empty($d) ? $d : false ;*/
      $where 	 = "kode = " . $this->escape($kode);
      $join     = "";
      $field    = "kode,keterangan";
      $dbd      = $this->select("hj_jenis", $field, $where, $join, "", "kode ASC","1") ;
	  return $dbd;
   }

   public function loadstockhj($va){
      //print_r($va);
      $field = "s.kode,s.keterangan,s.satuan,ifnull(h.hj,0) hj";
      $where = "s.tampil = 'P'";
      $join = "left join stock_hj_jenis h on h.stock = s.kode and h.hjjenis = '{$va['kode']}' and h.status = '1'";
      $dbd      = $this->select("stock s", $field, $where, $join, "", "s.kode ASC") ;
      return $dbd;
   }

}
?>
