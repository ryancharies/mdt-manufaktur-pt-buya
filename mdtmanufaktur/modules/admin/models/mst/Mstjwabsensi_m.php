<?php
class Mstjwabsensi_m extends Bismillah_Model
{
    public function loadgrid($va)
    {
        $limit = $va['offset'] . "," . $va['limit'];
        $search = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "";
        $search = $this->escape_like_str($search);
        $where = array();
        if ($search !== "") {
            $where[] = "j.kode LIKE '%{$search}%'";
        }

        $where[] = "j.status = '1'";
        $where = implode(" AND ", $where);
        $field = "j.kode,j.keterangan,j.siklus,j.sistem,j.toleransi,j.mulailembur,j.maxlembur,j.bukaabsenmasuk,j.tutupabsenmasuk,
            j.bukaabsenpulang,j.tutupabsenpulang";
        $join = "";
        $dbd = $this->select("absensi_jadwal j", $field, $where, $join, "", "j. kode ASC", $limit);
        $dba = $this->select("absensi_jadwal j", "j.id", $where, $join, "","j. kode ASC");

        return array("db" => $dbd, "rows" => $this->rows($dba));
    }

    public function saving($kode, $va)
    {
        $error = "";
        $vaGrid = json_decode($va['grid2']);
        foreach ($vaGrid as $key => $val) {
            if($val->status == ""){
                $error .= "Status tidak valid ... !!! \\n";
            }

            if($val->status == "0" && $val->masuk == "00:00:00" && $val->pulang == "00:00:00"){
                $error .= "Absensi jadwal tidak valid ... !!! \\n";
            }
            if($error !== ""){
                break;
            }
        }

        if($error == ""){
            $kode = getsession($this, "ssjwabsensi_kode", "");
            $va['kode'] = $kode !== "" ? $kode : $this->getkode();

            //cek no bg
            $data = array("kode" => $va['kode'],
                "keterangan" => $va['keterangan'],
                "sistem" => $va['sistem'],
                "siklus" => string_2n($va['siklus']),
                "toleransi" => string_2n($va['toleransi']),
                "mulailembur" => string_2n($va['mulailembur']),
                "maxlembur" => string_2n($va['maxlembur']),
                "bukaabsenmasuk" => string_2n($va['bukaabsenmasuk']),
                "tutupabsenmasuk" => string_2n($va['tutupabsenmasuk']),
                "bukaabsenpulang" => string_2n($va['bukaabsenpulang']),
                "tutupabsenpulang" => string_2n($va['tutupabsenpulang']),
                "username" => getsession($this, "username"),
                "datetime" => date_now());
            $where = "kode = '{$va['kode']}'";
            $this->update("absensi_jadwal", $data, $where, "");

            //insert detail
            
            $this->delete("absensi_jadwal_detail", "kode = '{$va['kode']}'");
            $nohari = 0 ;
            foreach ($vaGrid as $key => $val) {
                $nohari ++;
                $vadetail = array("kode" => $va['kode'],
                    "nohari" => $nohari,
                    "masuk" => $val->masuk,
                    "pulang" => $val->pulang,
                    "libur" => $val->status
                );
                $this->update("absensi_jadwal_detail", $vadetail, "kode = '{$va['kode']}' and nohari = '{$nohari}'");
            }
        }

        return $error;

    }

    public function getdatatotal($kode)
    {
        $data = array();
        $field = "kode,keterangan,sistem,siklus,toleransi,bukaabsenmasuk,tutupabsenmasuk,bukaabsenpulang,tutupabsenpulang,mulailembur,maxlembur";
        $where = "kode = '$kode'";
        $join  = "";
        $dbd = $this->select("absensi_jadwal ", $field, $where, $join);
        if ($dbr = $this->getrow($dbd)) {
            $data = $dbr;
        }
        return $data;
    }

    public function getdatadetail($kode)
    {
        $field = "kode,masuk,pulang,libur as status";
        $where = "kode = '$kode'";
        $join = "";
        $dbd = $this->select("absensi_jadwal_detail", $field, $where, $join);
        return $dbd;
    }

    public function deleting($kode)
    {
        $error = "";
        $this->edit("absensi_jadwal", array("status" => 2), "kode = " . $this->escape($kode));
        
        return $error;

    }

    public function seekbank($search)
    {
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%')";
        $dbd = $this->select("bank", "*", $where, "", "", "kode ASC", '');
        return array("db" => $dbd);
    }

    public function seekrekening($search)
    {
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%') and jenis = 'D'";
        $dbd = $this->select("keuangan_rekening", "*", $where, "", "", "kode ASC", '');
        return array("db" => $dbd);
    }

    public function seekdiberiterima($search)
    {
        $where = "(kode LIKE '{$search}%' OR nama LIKE '%{$search}%')";
        $dbd = $this->select("supplier", "*", $where, "", "", "nama ASC", '');
        return array("db" => $dbd);
    }

    public function getkode($l = true)
    {
        $cabang = getsession($this, "cabang");
        $key = "JW" . $cabang . date("y");
        $n = $this->getincrement($key, $l, 5);
        $faktur = $key . $n;
        return $faktur;
    }

    public function cekvalidasidel($faktur)
    {
        $error = "";
        $dbd = $this->select("bg_list", "nobgcek", "faktur = '$faktur' and fakturcair <> ''");
        if ($dbr = $this->getrow($dbd)) {
            $error .= "No bg " . $dbr['nobgcek'] . " sudah cair";
        }
        return $error;
    }
}
