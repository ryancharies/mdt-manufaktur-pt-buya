<?php
class Mstbagian_m extends Bismillah_Model{

   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(keterangan LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $f        = "id,kode,keterangan,rekbyasuransi" ; 
      $dbd      = $this->select("bagian", $f, $where, "", "", "kode ASC", $limit) ;

      $row      = 0 ;
      $dba      = $this->select("bagian", "COUNT(id) id", $where) ;
      if($dbra  = $this->getrow($dba)){
         $row   = $dbra['id'] ;
      }
      return array("db"=>$dbd, "rows"=> $row ) ;
   } 

   public function saving($va, $id){ 
      if(!isset($va['rekbyasuransi']))$va['rekbyasuransi'] = "";
      $data    = array("kode"=>$va['kode'],"keterangan"=>$va['keterangan'],"rekbyasuransi"=>$va['rekbyasuransi']) ;
      $where   = "kode = " . $this->escape($va['kode']) ;
      $this->update("bagian", $data, $where, "") ;
   }

   public function getdata($kode=''){
      /*$w    = "kode = " . $this->escape($kode) ;  
      $d    = $this->getval("id,kode,keterangan", $w, "aset_golongan") ;
      return !empty($d) ? $d : false ;*/
      $where 	 = "kode = " . $this->escape($kode);
      $join     = "";
      $field    = "kode,keterangan,rekbyasuransi";
      $dbd      = $this->select("bagian", $field, $where, $join, "", "kode ASC","1") ;
	  return $dbd;
   }

   public function seekrekening($search){
      $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%') and Jenis = 'D'" ;
      $dbd      = $this->select("keuangan_rekening", "*", $where, "", "", "keterangan ASC", '50') ;
      return array("db"=>$dbd) ;
   }       
}
?>
