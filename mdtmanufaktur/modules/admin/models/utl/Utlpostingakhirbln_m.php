<?php
class  Utlpostingakhirbln_m extends Bismillah_Model{

    public function seekcabang($search){
        $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("cabang", "*", $where, "", "", "keterangan ASC", '50') ;
        return array("db"=>$dbd) ;
    }


    public function postingpenyaset($periode,$cabang){
        $arrtgl = explode("-",$periode);
        $tgl = date('Y-m-d',mktime(0,0,0,$arrtgl[0]+1,0,$arrtgl[1]));

        $fakturlike = "PA".$cabang.date("ym",date_2t($tgl));

        $this->delete("keuangan_jurnal","faktur like '$fakturlike%'");
        $this->delete("keuangan_bukubesar","faktur like '$fakturlike%'");

        $faktur = $this->getlastfaktur("PA",$cabang,$tgl,true,15);
        
        $arrdata = array();

        $where    = "a.cabang = '$cabang' and a.tglperolehan <= '$tgl'";
        $join     = "left join aset_golongan g on g.kode = a.golongan left join cabang c on c.kode = a.cabang";
        $field    = "a.kode,a.keterangan,a.golongan,g.keterangan as ketgolongan,a.cabang, c.keterangan as ketcabang,a.lama,a.tglperolehan,a.hargaperolehan,a.unit,
                    a.jenispenyusutan,a.tarifpenyusutan,a.residu,g.rekakmpeny,g.rekbypeny";
        $dbd      = $this->select("aset a", $field, $where, $join, "", "a.golongan asc,a.kode ASC") ;
        while( $dbr = $this->getrow($dbd) ){
            if(!isset($arrdata[$dbr['golongan']]))$arrdata[$dbr['golongan']] = array("keterangan"=>$dbr['ketgolongan'],"penyusutan"=>0,"rekdebet"=>$dbr['rekbypeny'],"rekkredit"=>$dbr['rekakmpeny']);
            $arrpeny = $this->perhitungan_m->getpenyusutan($dbr['kode'],$tgl);
            $arrdata[$dbr['golongan']]['penyusutan'] += $arrpeny['bulan ini'];
        }
        
        $datetime = date("Y-m-d H:i:s");
        $username = getsession($this,"username") ;
        foreach($arrdata as $key => $val){
            $keterangan = "Penyusutan ".$val['keterangan']." [".$periode."]";

            $this->updtransaksi_m->updjurnal($faktur,$cabang,$tgl,$val['rekdebet'],$keterangan,$val['penyusutan'],0,$datetime,$username);
            $this->updtransaksi_m->updjurnal($faktur,$cabang,$tgl,$val['rekkredit'],$keterangan,0,$val['penyusutan'],$datetime,$username);
        }
        $this->updtransaksi_m->updrekjurnal($faktur);
    }

    public function penjualanlunas($periode,$cabang){
        $arrtgl = explode("-",$periode);
        $tgl = date('Y-m-d',mktime(0,0,0,$arrtgl[0]+1,0,$arrtgl[1]));
        $tglawal = date_bom($tgl);
        $tglakhir = date_eom($tgl);

        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);
        

        //reset tgl lunas peridoe ini
        $this->db->where('tgllunas >=',$tglawal);
        $this->db->where('cabang',$cabang);
        $this->db->where('status','1');
        $this->db->set("tgllunas",NULL);
        $this->db->set("fktlunas",NULL);
        $this->db->update("penjualan_total");

        
        //upidate tgl lunas penjualan
        $this->db->where('t.tgllunas is NULL');
        $this->db->where('t.cabang',$cabang);
        $this->db->where('t.status','1');
        $field = "t.faktur,t.tgl,t.customer,t.total,sum(k.debet-k.kredit) as saldo,max(k.tgl) tgllunas";
        $dbd = $this->db->select($field)->from("penjualan_total t")
        ->join("piutang_kartu k","k.fkt = t.faktur","left")
        ->group_by("t.faktur")
        ->having("saldo = 0")
        ->order_by("t.tgl,t.faktur ASC")
        ->get();
        foreach($dbd->result_array() as $r){
            $this->db->where('fkt',$r['faktur']);
            $this->db->where('tgl',$r['tgllunas']);
            $this->db->where('kredit >',0);
            $dbd2 = $this->db->select("faktur fktlunas")->from("piutang_kartu")
                ->order_by("id desc")
                ->limit(1)
                ->get();
            $r2  = $dbd2->row_array();
            if(!empty($r2)){
                $this->edit("penjualan_total",array("tgllunas"=>$r['tgllunas'],"fktlunas"=>$r2['fktlunas']),"faktur = '{$r['faktur']}'");
            }
        }
        

        //reset tgl lunas peridoe ini
        $this->db->where('tgllunas >=',$tglawal);
        $this->db->where('cabang',$cabang);
        $this->db->where('status','1');      
        $this->db->set("tgllunas",NULL);
        $this->db->set("fktlunas",NULL);
        $this->db->update("penjualan_retur_total");

        //update tgl lunas retur penjualan
        $this->db->where('t.tgllunas is NULL');
        $this->db->where('t.status','1');      
        $this->db->where('t.cabang',$cabang);
        $field = "t.faktur,t.tgl,t.customer,t.total,sum(k.debet-k.kredit) as saldo,d.jumlah as pelunasan,max(k.tgl) tgllunas";
        $dbd = $this->db->select($field)->from("penjualan_retur_total t")
        ->join("piutang_kartu k","k.fkt = t.faktur","left")
        ->join("piutang_pelunasan_detail d","d.fkt = t.faktur","left")
        ->group_by("t.faktur")
        ->having("saldo = 0")
        ->order_by("t.tgl,t.faktur ASC")
        ->get();
        foreach($dbd->result_array() as $r){
            $this->db->where('fkt',$r['faktur']);
            $this->db->where('tgl',$r['tgllunas']);
            $this->db->where('debet >',0);
            $dbd2 = $this->db->select("faktur fktlunas")->from("piutang_kartu")
                ->order_by("id desc")
                ->limit(1)
                ->get();
            $r2  = $dbd2->row_array();
            if(!empty($r2)){
                $this->edit("penjualan_retur_total",array("tgllunas"=>$r['tgllunas'],"fktlunas"=>$r2['fktlunas']),"faktur = '{$r['faktur']}'");
            }

        }


        //reset tgl lunas peridoe ini
        $this->db->where('tgllunaspiut >=',$tglawal);
        $this->db->where('status','0');
        $this->db->where('cabang',$cabang);
        $this->db->set("tgllunaspiut",NULL);
        $this->db->set("fktlunaspiut",NULL);
        $this->db->update("aset");

        //update tgl lunas aset
        $this->db->where('t.tgllunaspiut is NULL');
        $this->db->where('t.status','0');
        $this->db->where('t.cabang',$cabang);
        $field = "t.fakturpj faktur,t.tglpj,t.customer,t.hargajual total,sum(k.debet-k.kredit) as saldo,d.jumlah as pelunasan,max(k.tgl) tgllunas";
        $dbd = $this->db->select($field)->from("aset t")
        ->join("piutang_kartu k","k.fkt = t.fakturpj","left")
        ->join("piutang_pelunasan_detail d","d.fkt = t.fakturpj","left")
        ->group_by("t.fakturpj")
        ->having("saldo = 0")
        ->order_by("t.tglpj,t.fakturpj ASC")
        ->get();
        foreach($dbd->result_array() as $r){

            $this->db->where('fkt',$r['faktur']);
            $this->db->where('tgl',$r['tgllunas']);
            $this->db->where('kredit >',0);
            $dbd2 = $this->db->select("faktur fktlunas")->from("piutang_kartu")
                ->order_by("id desc")
                ->limit(1)
                ->get();
            $r2  = $dbd2->row_array();
            if(!empty($r2)){
                $this->edit("aset",array("tgllunaspiut"=>$r['tgllunas'],"fktlunaspiut"=>$r2['fktlunas']),"fakturpj = '{$r['faktur']}'");
            }

            
        }


        //reset tgl lunas peridoe ini
        $this->db->where('tgllunas >=',$tglawal);
        $this->db->where('status','1');      
        $this->db->where('cabang',$cabang);
        $this->db->set("tgllunas",NULL);
        $this->db->set("fktlunas",NULL);
        $this->db->update("pembelian_total");

        //update tgl lunas pembelian
        $this->db->where('t.tgllunas is NULL');
        $this->db->where('t.status','1');      
        $this->db->where('t.cabang',$cabang);
        $field = "t.faktur,t.tgl,t.supplier,t.total,sum(k.debet-k.kredit) as saldo,d.jumlah as pelunasan,max(k.tgl) tgllunas";
        $dbd = $this->db->select($field)->from("pembelian_total t")
        ->join("hutang_kartu k","k.fkt = t.faktur","left")
        ->join("hutang_pelunasan_detail d","d.fkt = t.faktur","left")
        ->group_by("t.faktur")
        ->having("saldo = 0")
        ->order_by("t.tgl,t.faktur ASC")
        ->get();
        foreach($dbd->result_array() as $r){

            $this->db->where('fkt',$r['faktur']);
            $this->db->where('tgl',$r['tgllunas']);
            $this->db->where('kredit >',0);
            $dbd2 = $this->db->select("faktur fktlunas")->from("hutang_kartu")
                ->order_by("id desc")
                ->limit(1)
                ->get();
            $r2  = $dbd2->row_array();
            if(!empty($r2)){
                $this->edit("pembelian_total",array("tgllunas"=>$r['tgllunas'],"fktlunas"=>$r2['fktlunas']),"faktur = '{$r['faktur']}'");
            }
        }


        //reset tgl lunas peridoe ini
        $this->db->where('tgllunas >=',$tglawal);
        $this->db->where('status','1');      
        $this->db->where('cabang',$cabang);
        $this->db->set("tgllunas",NULL);
        $this->db->set("fktlunas",NULL);
        $this->db->update("pembelian_retur_total");

        //update tgl lunas retur pembelian
        $this->db->where('t.tgllunas is NULL');
        $this->db->where('t.status','1');      
        $this->db->where('t.cabang',$cabang);
        $field = "t.faktur,t.tgl,t.supplier,t.total,sum(k.debet-k.kredit) as saldo,d.jumlah as pelunasan,max(k.tgl) tgllunas";
        $dbd = $this->db->select($field)->from("pembelian_retur_total t")
        ->join("hutang_kartu k","k.fkt = t.faktur","left")
        ->join("hutang_pelunasan_detail d","d.fkt = t.faktur","left")
        ->group_by("t.faktur")
        ->having("saldo = 0")
        ->order_by("t.tgl,t.faktur ASC")
        ->get();
        foreach($dbd->result_array() as $r){
            $this->db->where('fkt',$r['faktur']);
            $this->db->where('tgl',$r['tgllunas']);
            $this->db->where('debet >',0);
            $dbd2 = $this->db->select("faktur fktlunas")->from("hutang_kartu")
                ->order_by("id desc")
                ->limit(1)
                ->get();
            $r2  = $dbd2->row_array();
            if(!empty($r2)){
                $this->edit("pembelian_retur_total",array("tgllunas"=>$r['tgllunas'],"fktlunas"=>$r2['fktlunas']),"faktur = '{$r['faktur']}'");
            }
        }
    }
}
?>