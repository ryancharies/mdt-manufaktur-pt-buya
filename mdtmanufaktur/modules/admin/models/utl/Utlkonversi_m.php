<?php
class  Utlkonversi_m extends Bismillah_Model{
    public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "KV".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }
    public function resetdata($tgl){
        $this->delete("keuangan_bukubesar","tgl <= '$tgl'");
        $this->delete("keuangan_jurnal","tgl <= '$tgl'");
        $this->delete("stock_kartu","tgl <= '$tgl'");
        $this->delete("stock_hp","tgl <= '$tgl'");
        $this->delete("hutang_kartu","tgl <= '$tgl'");
        $this->delete("piutang_kartu","tgl <= '$tgl'");
        $this->delete("pembelian_total","tgl <= '$tgl'");
        $this->delete("penjualan_total","tgl <= '$tgl'");
    }

    public function saldostock($tgl){
        $this->delete("stock_kartu","tgl <= '$tgl'");
        $this->delete("stock_hp","tgl <= '$tgl'");
        $cabang = getsession($this, "cabang") ;
        $username = getsession($this, "username");
        $datetime = date("Y-m-d H:i:s");

        $dbd      = $this->select("konversi_saldo_persediaan","*") ;
        while($dbr = $this->getrow($dbd)){
            $keterangan = "Konversi Saldo Awal";
            if($dbr['Stock_Group'] == '1')$dbr['Stock_Group'] = "001";
            if($dbr['Stock_Group'] == '2')$dbr['Stock_Group'] = "002";
            if($dbr['Stock_Group'] == '4')$dbr['Stock_Group'] = "004";
            $arrstock = array("kode"=>$dbr['Kode'],"barcode"=>$dbr['Kode'],"satuan"=>$dbr['Satuan'],"stock_group"=>$dbr['Stock_Group'],
                        "hargajual"=>0,"jenis"=>$dbr['Jenis'],"keterangan"=>$dbr['Nama'], "username"=>$username) ;
            $this->update("stock",$arrstock,"kode = '{$dbr['Kode']}'");
            $faktur = $this->getfaktur();
            $this->updtransaksi_m->updkartustock($faktur,$dbr['Kode'],$tgl,'01',$cabang,$keterangan,$dbr['Qty'],0,
                                                 $dbr['Harga_Pokok'],$username,$datetime);
            $cfghpp = $this->perhitungan_m->getcfghpp($tgl);
            $arrhp = $this->perhitungan_m->gethpstock($dbr['Kode'],$tgl,$tgl,$dbr['Qty'],0,$cabang,
                                                      $cfghpp['caraperhitungan'],$cfghpp['periode']);
        }
    }

    public function saldohutang($tgl){
        $this->delete("hutang_kartu","tgl <= '$tgl'");
        $this->delete("pembelian_total","tgl <= '$tgl'");
        $this->delete("persekot_mutasi_total","tgl <= '$tgl'");

        $cabang = getsession($this, "cabang") ;
        $username = getsession($this, "username");
        $datetime = date("Y-m-d H:i:s");
        $dbd      = $this->select("konversi_saldo_hutang","*") ;
        while($dbr = $this->getrow($dbd)){

            //entry mst supplier
            $data    = array("kode"=>$dbr['Kode'], "nama"=>$dbr['Nama_Supplier'],"email"=>"-",
                       "notelepon"=>"-","alamat"=>"-","namabank"=>"-",
                       "rekening"=>"-","atasnamarekening"=>"-") ;
            $where   = "kode = '{$dbr['Kode']}'";
            $this->update("supplier", $data, $where, "") ;

            //tr pembelian
            if(string_2n($dbr['Saldo_Hutang_Dagang']) > 0){
                $keterangan = "Konversi Saldo Awal";
                $faktur = $this->getfaktur();
                $data           = array("faktur"=>$faktur,
                                        "tgl"=>$tgl,
                                        "fktpo"=>"",
                                        "subtotal"=>string_2n($dbr['Saldo_Hutang_Dagang']),
                                        "total"=>string_2n($dbr['Saldo_Hutang_Dagang']),
                                        "hutang"=>string_2n($dbr['Saldo_Hutang_Dagang']),
                                        "status"=>"1",
                                        "gudang"=>"01",
                                        "supplier"=>$dbr['Kode'],
                                        "cabang"=> $cabang,
                                        "username"=> $username) ;
                $where          = "faktur = " . $this->escape($faktur) ;
                $this->update("pembelian_total", $data, $where, "") ;

                $this->updtransaksi_m->updkartuhutangpembelian($faktur);
            }
            
            if(string_2n($dbr['Saldo_Persekot'])){
                $faktur = $this->getfaktur();
                $data          = array("faktur"=>$faktur,
                                        "tgl"=>$tgl,
                                        "status"=>"1",
                                        "kodetransaksi"=>"0001",
                                        "supplier"=>$dbr['Kode'],
                                        "jumlah"=>string_2n($dbr['Saldo_Persekot']),
                                        "cabang"=> getsession($this, "cabang"),
                                        "username"=> getsession($this, "username"),
                                        "datetime"=>date("Y-m-d H:i:s")) ;
                 $where          = "faktur = " . $this->escape($faktur) ;
                 $this->update("persekot_mutasi_total", $data, $where, "") ;
                 $this->updtransaksi_m->updkartuhutangpersekot($faktur);
            }
          
        }

        //konversi mutasi persekot selain pembelian
        $dbd      = $this->select("konversi_saldo_porsekotlainpb","*") ;
        while($dbr = $this->getrow($dbd)){
            //entry mst supplier
            $data    = array("kode"=>$dbr['Kode'], "nama"=>$dbr['Nama_Supplier'],"email"=>"-",
                       "notelepon"=>"-","alamat"=>"-","namabank"=>"-",
                       "rekening"=>"-","atasnamarekening"=>"-") ;
            $where   = "kode = '{$dbr['Kode']}'";
            $this->update("supplier", $data, $where, "") ;

            if(string_2n($dbr['Saldo_Porsekot'])){
                $dbr['KT'] = str_pad($dbr['KT'],4,"0",STR_PAD_LEFT);
                $faktur = $this->getfaktur();
                $data          = array("faktur"=>$faktur,
                                        "tgl"=>$tgl,
                                        "status"=>"1",
                                        "kodetransaksi"=>$dbr['KT'],
                                        "supplier"=>$dbr['Kode'],
                                        "jumlah"=>string_2n($dbr['Saldo_Porsekot']),
                                        "cabang"=> getsession($this, "cabang"),
                                        "username"=> getsession($this, "username"),
                                        "datetime"=>date("Y-m-d H:i:s")) ;
                 $where          = "faktur = " . $this->escape($faktur) ;
                 $this->update("persekot_mutasi_total", $data, $where, "") ;
                 $this->updtransaksi_m->updkartuhutangpersekot($faktur);
            }
        }
    }

    public function saldopiutang($tgl){
        $this->delete("piutang_kartu","tgl <= '$tgl'");
        $this->delete("penjualan_total","tgl <= '$tgl'");
        $this->delete("uangmuka_mutasi_total","tgl <= '$tgl'");

        $cabang = getsession($this, "cabang") ;
        $username = getsession($this, "username");
        $datetime = date("Y-m-d H:i:s");
        $dbd      = $this->select("konversi_saldo_piutang","*") ;
        while($dbr = $this->getrow($dbd)){

            //entry customer
            if($dbr['Golongan'] == '1')$dbr['Golongan'] = "001";
            if($dbr['Golongan'] == '2')$dbr['Golongan'] = "002";
            if($dbr['Golongan'] == '3')$dbr['Golongan'] = "003";
            if($dbr['Golongan'] == '4')$dbr['Golongan'] = "004";
            $data    = array("kode"=>$dbr['Kode'], "nama"=>$dbr['Nama_Customer'],"email"=>"-","notelepon"=>"-","alamat"=>$dbr['Alamat'],
                      "golongan"=>$dbr['Golongan']) ;
            $where   = "kode = '{$dbr['Kode']}'";
            $this->update("customer", $data, $where, "") ;

            //tr penjualan
            if(string_2n($dbr['Saldo_Piutang']) > 0){
                $keterangan = "Konversi Saldo Awal";           
                $faktur = $this->getfaktur();
                $data    = array("faktur"=>$faktur,
                                "tgl"=>$tgl,
                                "subtotal"=>string_2n($dbr['Saldo_Piutang']), 
                                "total"=>string_2n($dbr['Saldo_Piutang']),
                                "kas"=>0,
                                "piutang"=>string_2n($dbr['Saldo_Piutang']),
                                "status"=>"1",
                                "gudang"=>"01",
                                "customer"=>$dbr['Kode'],
                                "cabang"=> $cabang,
                                "username"=> $username,
                                "datetime_insert"=>$datetime) ;
                $where   = "faktur = " . $this->escape($faktur) ;
                $this->update("penjualan_total", $data, $where, "") ;

                $this->updtransaksi_m->updkartupiutangpenjualan($faktur);
            }

            //tr uangmuka
            if(string_2n($dbr['Saldo_Uangmuka']) > 0){
                $faktur = $this->getfaktur();
                $data           = array("faktur"=>$faktur,
                                "tgl"=>$tgl,
                                "status"=>"1",
                                "kodetransaksi"=>"0007",
                                "customer"=>$dbr['Kode'],
                                "jumlah"=>string_2n($dbr['Saldo_Uangmuka']),
                                "cabang"=> getsession($this, "cabang"),
                                "username"=> getsession($this, "username"),
                                "datetime"=>date("Y-m-d H:i:s")) ;
                $where          = "faktur = " . $this->escape($faktur) ;
                $this->update("uangmuka_mutasi_total", $data, $where, "") ;

                $this->updtransaksi_m->updkartuhutanguangmuka($faktur);
            }
        }
    }

    public function saldonlr($tgl){
        $this->delete("keuangan_bukubesar","tgl <= '$tgl'");

        $cabang = getsession($this, "cabang") ;
        $username = getsession($this, "username");
        $datetime = date("Y-m-d H:i:s");

        $dbd      = $this->select("konversi_neraca","*") ;
        while($dbr = $this->getrow($dbd)){
            $keterangan = "Konversi Saldo Awal";
            $faktur = $this->getfaktur();
            $data    = array("faktur"=>$faktur,"cabang"=>"001","tgl"=>$tgl,"rekening"=>$dbr['Kode'],"keterangan"=>"Konversi Saldo Awal",
                    "debet"=>string_2n($dbr['Debet']),"kredit"=>string_2n($dbr['Kredit']),"datetime"=>date("Y-m-d H:i:s"),"username"=> getsession($this, "username")) ;
            //$data['faktur'] = $faktur;
            if(string_2n($dbr['Debet']) > 0 || string_2n($dbr['Kredit']) > 0)$this->insert("keuangan_bukubesar", $data) ;
        }
    }

    public function saldobg($tgl){
        $this->delete("bg_list","tgl <= '2020-01-20'");

        $cabang = getsession($this, "cabang") ;
        $username = getsession($this, "username");
        $datetime = date("Y-m-d H:i:s");

        $dbd      = $this->select("konversi_bg","*") ;
        while($dbr = $this->getrow($dbd)){
            $keterangan = "Konversi Saldo Awal";
            $faktur = $this->getfaktur();
            $data    = array("faktur"=>$faktur,"cabang"=>"001","kpddari"=>$dbr['kepada'],
                    "sc"=>"S","bank"=>$dbr['bank'],"bgcek"=>"BG","norekening"=>$dbr['rekening'],"nobgcek"=>$dbr['nobg'],
                    "tgl"=>$dbr['tgl'],"tgljthtmp"=>$dbr['tgljthtmp'],"nominal"=>string_2n($dbr['nominal']),
                    "datetime"=>date("Y-m-d H:i:s"),"username"=> getsession($this, "username")) ;
            //$data['faktur'] = $faktur;
            $this->insert("bg_list", $data) ;
        }
    }

    public function fixkodetrporsekot(){
        $dbd      = $this->select("hutang_kartu","id,faktur","jenis = 'P'") ;
        while($dbr = $this->getrow($dbd)){
            $kodetr = "";
            $dbd1 = $this->select("persekot_mutasi_total","kodetransaksi","faktur = '{$dbr['faktur']}'") ;
            if($dbr1 = $this->getrow($dbd1)){
                $kodetr = $dbr1['kodetransaksi'];
            }

            if($kodetr == ""){
                $dbd2 = $this->select("hutang_pelunasan_total","kdtrpersekot","faktur = '{$dbr['faktur']}'") ;
                if($dbr2 = $this->getrow($dbd2)){
                    $kodetr = $dbr2['kdtrpersekot'];
                }
            }

            $this->edit("hutang_kartu",array("kodetransaksi"=>$kodetr),"faktur = '{$dbr['faktur']}' and jenis = 'P'");
        }
    }

    public function fixkodetruangmuka(){
        $dbd      = $this->select("piutang_kartu","id,faktur","jenis = 'U'") ;
        while($dbr = $this->getrow($dbd)){
            $kodetr = "";
            $dbd1 = $this->select("uangmuka_mutasi_total","kodetransaksi","faktur = '{$dbr['faktur']}'") ;
            if($dbr1 = $this->getrow($dbd1)){
                $kodetr = $dbr1['kodetransaksi'];
            }

            if($kodetr == ""){
                $dbd2 = $this->select("piutang_pelunasan_total","kdtruangmuka","faktur = '{$dbr['faktur']}'") ;
                if($dbr2 = $this->getrow($dbd2)){
                    $kodetr = $dbr2['kdtruangmuka'];
                }
            }

            $this->edit("piutang_kartu",array("kodetransaksi"=>$kodetr),"faktur = '{$dbr['faktur']}' and jenis = 'U'");
        }
    }

    public function datakaryawan(){
        $arrtable = array("karyawan","jabatan","bagian","karyawan_bagian","karyawan_jabatan");
        foreach($arrtable as $table){
            $this->delete_all($table);
        }
        $dbd      = $this->select("konvers_karyawan","*") ;
        while($dbr = $this->getrow($dbd)){            
            if($dbr['MULAIKERJA'] == "")$dbr['MULAIKERJA'] = date("Y-m-d");
            $arrtgl = explode("-",$dbr['MULAIKERJA']);

            //insert karyawan
            $kode = $this->getkode($arrtgl[0]);
            $arrins = array("kode"=>$kode,"nama"=>$dbr['NAMA'],"alamat"=>$dbr['ALAMAT'],"tgl"=>$dbr['MULAIKERJA'],
                            "rekening"=>$dbr['NO.REK']);
            $this->insert("karyawan",$arrins);

            //cari bagian
            $kodebag = "";
            $dbdbag = $this->select("bagian","kode","keterangan = '{$dbr['BAGIAN']}'");
            if($dbrbag = $this->getrow($dbdbag)){
                $kodebag = $dbrbag['kode'];
            }else{
                $maxkodebag = "";
                $dbdkdbag= $this->select("bagian","ifnull(max(kode),'') as kode");
                if($dbrkdbag = $this->getrow($dbdkdbag)){
                    $maxkodebag = $dbrkdbag['kode'];
                }

                $kodebag = intval($maxkodebag) + 1;
                $kodebag = str_pad($kodebag,3,0,STR_PAD_LEFT);
                $arrbag = array("kode"=>$kodebag,"keterangan"=>$dbr['BAGIAN']);
                $this->insert("bagian",$arrbag);    
            }

            $arrkrbag = array("tgl"=>$dbr['MULAIKERJA'],"nip"=>$kode,"bagian"=>$kodebag,"username"=>"iniad","datetime"=>date_now());
            $this->insert("karyawan_bagian",$arrkrbag);

            //cari jabatan
            $kodejab = "";
            $dbdjab = $this->select("jabatan","kode","keterangan = '{$dbr['JABATAN']}'");
            if($dbrjab = $this->getrow($dbdjab)){
                $kodejab = $dbrjab['kode'];
            }else{
                $maxkodejab = "";
                $dbdkdjab= $this->select("jabatan","ifnull(max(kode),'') as kode");
                if($dbrkdjab = $this->getrow($dbdkdjab)){
                    $maxkodejab = $dbrkdjab['kode'];
                }

                $kodejab = intval($maxkodejab) + 1;
                $kodejab = str_pad($kodejab,3,0,STR_PAD_LEFT);
                $arrjab = array("kode"=>$kodejab,"keterangan"=>$dbr['JABATAN']);
                $this->insert("jabatan",$arrjab);    
            }

            $arrkrjab = array("tgl"=>$dbr['MULAIKERJA'],"nip"=>$kode,"jabatan"=>$kodejab,"username"=>"iniad","datetime"=>date_now());
            $this->insert("karyawan_jabatan",$arrkrjab);

            
        }
    }

    public function getkode($y){
        $n    = $this->getincrement("karyawankode" . $y, true, 6);
        $n    = $y.$n ;
        return $n ;
    }

    public function penjualanlunas(){
        $this->db->where('t.status','1');
        $field = "t.faktur,t.tgl,t.customer,t.total,sum(k.debet-k.kredit) as saldo,d.jumlah as pelunasan,max(k.tgl) tgllunas";
        $dbd = $this->db->select($field)->from("penjualan_total t")
        ->join("piutang_kartu k","k.fkt = t.faktur","left")
        ->join("piutang_pelunasan_detail d","d.fkt = t.faktur","left")
        ->group_by("t.faktur")
        ->having("saldo = 0")
        ->order_by("t.tgl,t.faktur ASC")
        ->get();
        foreach($dbd->result_array() as $r){
            $this->db->where('fkt',$r['faktur']);
            $this->db->where('tgl',$r['tgllunas']);
            $this->db->where('kredit >',0);
            $dbd2 = $this->db->select("faktur fktlunas")->from("piutang_kartu")
                ->order_by("id desc")
                ->limit(1)
                ->get();
            $r2  = $dbd2->row_array();
            if(!empty($r2)){
                $this->edit("penjualan_total",array("tgllunas"=>$r['tgllunas'],"fktlunas"=>$r2['fktlunas']),"faktur = '{$r['faktur']}'");
            }
        }

        $this->db->where('t.status','1');
        $field = "t.faktur,t.tgl,t.customer,t.total,sum(k.debet-k.kredit) as saldo,d.jumlah as pelunasan,max(k.tgl) tgllunas";
        $dbd = $this->db->select($field)->from("penjualan_retur_total t")
        ->join("piutang_kartu k","k.fkt = t.faktur","left")
        ->join("piutang_pelunasan_detail d","d.fkt = t.faktur","left")
        ->group_by("t.faktur")
        ->having("saldo = 0")
        ->order_by("t.tgl,t.faktur ASC")
        ->get();
        foreach($dbd->result_array() as $r){
            $this->db->where('fkt',$r['faktur']);
            $this->db->where('tgl',$r['tgllunas']);
            $this->db->where('debet >',0);
            $dbd2 = $this->db->select("faktur fktlunas")->from("piutang_kartu")
                ->order_by("id desc")
                ->limit(1)
                ->get();
            $r2  = $dbd2->row_array();
            if(!empty($r2)){
                $this->edit("penjualan_retur_total",array("tgllunas"=>$r['tgllunas'],"fktlunas"=>$r2['fktlunas']),"faktur = '{$r['faktur']}'");
            }

        }

        $this->db->where('t.status','0');
        $field = "t.fakturpj faktur,t.tglpj,t.customer,t.hargajual total,sum(k.debet-k.kredit) as saldo,d.jumlah as pelunasan,max(k.tgl) tgllunas";
        $dbd = $this->db->select($field)->from("aset t")
        ->join("piutang_kartu k","k.fkt = t.fakturpj","left")
        ->join("piutang_pelunasan_detail d","d.fkt = t.fakturpj","left")
        ->group_by("t.fakturpj")
        ->having("saldo = 0")
        ->order_by("t.tglpj,t.fakturpj ASC")
        ->get();
        foreach($dbd->result_array() as $r){

            $this->db->where('fkt',$r['faktur']);
            $this->db->where('tgl',$r['tgllunas']);
            $this->db->where('kredit >',0);
            $dbd2 = $this->db->select("faktur fktlunas")->from("piutang_kartu")
                ->order_by("id desc")
                ->limit(1)
                ->get();
            $r2  = $dbd2->row_array();
            if(!empty($r2)){
                $this->edit("aset",array("tgllunaspiut"=>$r['tgllunas'],"fktlunaspiut"=>$r2['fktlunas']),"fakturpj = '{$r['faktur']}'");
            }

            
        }


        $this->db->where('t.status','1');
        $field = "t.faktur,t.tgl,t.supplier,t.total,sum(k.debet-k.kredit) as saldo,d.jumlah as pelunasan,max(k.tgl) tgllunas";
        $dbd = $this->db->select($field)->from("pembelian_total t")
        ->join("hutang_kartu k","k.fkt = t.faktur","left")
        ->join("hutang_pelunasan_detail d","d.fkt = t.faktur","left")
        ->group_by("t.faktur")
        ->having("saldo = 0")
        ->order_by("t.tgl,t.faktur ASC")
        ->get();
        foreach($dbd->result_array() as $r){

            $this->db->where('fkt',$r['faktur']);
            $this->db->where('tgl',$r['tgllunas']);
            $this->db->where('kredit >',0);
            $dbd2 = $this->db->select("faktur fktlunas")->from("hutang_kartu")
                ->order_by("id desc")
                ->limit(1)
                ->get();
            $r2  = $dbd2->row_array();
            if(!empty($r2)){
                $this->edit("pembelian_total",array("tgllunas"=>$r['tgllunas'],"fktlunas"=>$r2['fktlunas']),"faktur = '{$r['faktur']}'");
            }
        }

        $this->db->where('t.status','1');
        $field = "t.faktur,t.tgl,t.supplier,t.total,sum(k.debet-k.kredit) as saldo,d.jumlah as pelunasan,max(k.tgl) tgllunas";
        $dbd = $this->db->select($field)->from("pembelian_retur_total t")
        ->join("hutang_kartu k","k.fkt = t.faktur","left")
        ->join("hutang_pelunasan_detail d","d.fkt = t.faktur","left")
        ->group_by("t.faktur")
        ->having("saldo = 0")
        ->order_by("t.tgl,t.faktur ASC")
        ->get();
        foreach($dbd->result_array() as $r){
            $this->db->where('fkt',$r['faktur']);
            $this->db->where('tgl',$r['tgllunas']);
            $this->db->where('debet >',0);
            $dbd2 = $this->db->select("faktur fktlunas")->from("hutang_kartu")
                ->order_by("id desc")
                ->limit(1)
                ->get();
            $r2  = $dbd2->row_array();
            if(!empty($r2)){
                $this->edit("pembelian_retur_total",array("tgllunas"=>$r['tgllunas'],"fktlunas"=>$r2['fktlunas']),"faktur = '{$r['faktur']}'");
            }
        }
    }

    public function penjualanfree2023(){
        $field = "faktur,tgl,customer_seharusnya,";
        $dbd = $this->db->select($field)->from("penjualan_free_2023")->get();
        foreach($dbd->result_array() as $r){

            //penjualan total
            $this->db->where('faktur',$r['faktur']);
            $this->db->set('customer',$r['customer_seharusnya']);
            $this->db->update("penjualan_total");

            //piutang kartu
            $this->db->where('fkt',$r['faktur']);
            $this->db->set('customer',$r['customer_seharusnya']);
            $this->db->update("piutang_kartu");

            //cari data di piutang kartu untuk faktur pelunasan
            $this->db->like('faktur','PP','after');
            $this->db->where('fkt',$r['faktur']);
            $f = "faktur,fkt";
            $dbd2 = $this->db->select($f)->from("piutang_kartu")->get();
            foreach($dbd2->result_array() as $r2){
                //penjualan total
                $this->db->where('faktur',$r2['faktur']);
                $this->db->set('customer',$r['customer_seharusnya']);
                $this->db->update("piutang_pelunasan_total");
            }
        }
    }
}
?>