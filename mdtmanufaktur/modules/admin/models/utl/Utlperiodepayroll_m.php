<?php
class Utlperiodepayroll_m extends Bismillah_Model{
   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $field = "kode,keterangan,tglawal,tglakhir,status";
      $join = "";
      $dbd      = $this->select("sys_periode_payroll", $field, $where, $join, "", "kode desc", $limit) ;
      $dba      = $this->select("sys_periode_payroll", "kode",$where, $join, "", "kode desc") ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function saving($kode, $va){
      $va['kode'] = $kode !== "" ? $va['kode'] : $this->getkode() ;
      $data    = array("kode"=>$va['kode'], "keterangan"=>$va['keterangan'],
                      "tglawal"=>date_2s($va['tglawal']),"tglakhir"=>date_2s($va['tglakhir'])) ;
      $where   = "kode = " . $this->escape($va['kode']) ;
      $this->update("sys_periode_payroll", $data, $where, "") ;
   }

   public function getdata($kode){
      $data = array() ;
		if($d = $this->getval("*", "kode = " . $this->escape($kode), "sys_periode_payroll")){
         $data = $d;
		}
		return $data ;
   }

   public function nonaktif($kode){
      $return = "ok";
      $this->edit("sys_periode_payroll",array("status"=>"2"), "kode = " . $this->escape($kode)) ;
      
      return $return;
      
   }

   public function aktif($kode){
      $return = "ok";
      $this->edit("sys_periode_payroll",array("status"=>"1"), "kode = " . $this->escape($kode)) ;
      
      return $return;
      
   }

   public function getkode(){
      $return    = date("Ym");
      $dbd = $this->select("sys_periode_payroll","*","","","","kode desc");
      if($dbr = $this->getrow($dbd)){
         $y = substr($dbr['kode'],0,4);
         $b = substr($dbr['kode'],4,2);
         if($b == 12){
            $b = 1;
            $y += 1;
         }else{
            $b += 1;
         }
         $b = str_pad($b,2,0,STR_PAD_LEFT);
         $return = $y.$b;
      }
      return $return ;
   }

  
}
?>
