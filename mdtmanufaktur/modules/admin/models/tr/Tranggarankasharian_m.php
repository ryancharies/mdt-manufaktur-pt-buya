<?php
class Tranggarankasharian_m extends Bismillah_Model{
    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "t.faktur LIKE '%{$search}%'" ;
        $where[] = "t.status = '1' and t.periode >= '{$va['tglawal']}' and t.periode <= '{$va['tglakhir']}'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "t.faktur,t.periode as tgl,t.cabang,t.rekening,t.keterangan,if(t.jenis='KK',t.nominal,0) kredit,
                    if(t.jenis='KM',t.nominal,0) debet,t.jenis,t.status,t.username,t.datetime,r.keterangan as ketrekening";
        $join     = "left join keuangan_rekening r on r.Kode = t.rekening";
        $dbd      = $this->select("anggaran_kas t", $field, $where, $join, "", "t.periode,t. faktur ASC", $limit) ;
        $dba      = $this->select("anggaran_kas t", "t.id", $where) ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function saving($faktur, $va){
        $nominal = string_2n($va['jumlah']);
        $faktur         = getsession($this, "ssanggarankasharian_faktur", "");
        $va['faktur']   = $faktur !== "" ? $faktur : $this->getfaktur() ;
        $data           = array("faktur"=>$va['faktur'],
                                "periode"=>date_2s($va['tgl']),
                                "status"=>"1",
                                "rekening"=>$va['rekening'],
                                "keterangan"=>$va['keterangan'],
                                "nominal"=>$nominal,
                                "jenis"=>$va['jenis'],
                                "cabang"=> getsession($this, "cabang"),
                                "username"=> getsession($this, "username"),
                                "datetime"=>date("Y-m-d H:i:s")) ;
        $where          = "faktur = " . $this->escape($faktur) ;
        $this->update("anggaran_kas", $data, $where, "") ;

    }

    public function getdatatotal($faktur){
        $data  = array() ;
        $field = "t.faktur,t.periode,t.cabang,t.rekening,t.keterangan,
                t.nominal,t.jenis,t.status,t.username,t.datetime,r.keterangan as ketrekening";
        $where = "t.faktur = '$faktur'";
        $join  = "left join keuangan_rekening r on r.kode = t.rekening";
        $dbd   = $this->select("anggaran_kas t", $field, $where, $join) ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }

   

    public function deleting($faktur){
        $this->edit("anggaran_kas",array("status"=>2),"faktur = " . $this->escape($faktur));
    }

    public function seekrekening($search){
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%') and jenis = 'D'" ;
        $dbd      = $this->select("keuangan_rekening", "*", $where, "", "", "kode ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "AK".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }

    public function getdata($kode){
        $data = array() ;
        if($d = $this->getval("*", "kode = " . $this->escape($kode), "stock")){
            $data = $d;
        }
        return $data ;
    }
    

}
?>
