<?php
class Trabsensikaryawan_m extends Bismillah_Model{
   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(c.kode LIKE '{$search}%' OR c.nama LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $field = "c.kode,c.nama,c.telepon,c.tgl,c.alamat,g.keterangan as bank,c.rekening,c.anrekening,c.ktp,c.id";
      $join = "left join bank g on g.kode = c.bank";
      $dbd      = $this->select("karyawan c", $field, $where, $join, "", "c.kode ASC", $limit) ;
      $dba      = $this->select("karyawan c", "c.id", $where) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function loadgrid2($va){
      $where 	 = array() ; 
      $where[]	= "c.nip LIKE '{$va['kode']}'" ;
      $where 	 = implode(" AND ", $where) ;
      $field = "c.nip,c.tgl,j.keterangan as golongan";
      $join = "left join absensi_golongan j on j.kode = c.golongan";
      $dbd      = $this->select("absensi_karyawan_golongan c", $field, $where, $join, "", "c.tgl desc") ;
      $dba      = $this->select("absensi_karyawan_golongan c", "c.id", $where) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function saving($va){
      $error = "";

      $va['tglabsensi'] = date_2s($va['tglabsensi']);
      if($va['kode'] == "")$error .= "nip tidak valid.. \\n";
      if(!isset($va['kodeabsensim']))$error .= "kode absensi masuk tidak valid.. \\n";
      if(!isset($va['kodeabsensip']))$error .= "kode absensi pulang tidak valid.. \\n";

      $tglabsensim = $va['tglabsensi'];
      $dtm = $tglabsensim . " " . $va['masuk'];
      $tm = strtotime($dtm);

      $tglabsensip = $va['tglabsensi'];
      $dtp = $tglabsensip . " " . $va['pulang'];
      $tp = strtotime($dtp);
      //cek apakah dtmasuk lebih besar daripada dtpulang
      if($tp <= $tm){
         $tglabsensip = date("Y-m-d",strtotime($va['tglabsensi']) + (60*60*24));
         $dtp = $tglabsensip . " " . $va['pulang'];
         $tp = strtotime($dtp);
      }

      //cek valid time
      $dtm2 = date("Y-m-d H:i:s",$tm);
      $dtp2 = date("Y-m-d H:i:s",$tp);
      if($dtp <> $dtp2)$error .= "Jam pulang tidak valid.. \\n".$dtp."||".$dtp2."\\n";
      if($dtm <> $dtm2)$error .= "Jam masuk tidak valid.. \\n".$dtm."||".$dtm2."\\n";

      if($error == ""){
         $tglfinger = date_2s($va['tglabsensi']);

         //lihat jadwal absensi
         $jw = $this->perhitunganhrd_m->getjwabsensi($va['kode'],$tglfinger);
         
         $toleransimenit = $jw['jwtoleransi'];
         $toleransidtk = $toleransimenit * 60;
         $jadwalabsensimasuk = ($jw['jwmasuk'] == "None" ? $dtm : $tglabsensim ." ". $jw['jwmasuk']);
         $jadwalabsensipulang = ($jw['jwpulang'] == "None" ? $dtp : $tglabsensip ." ". $jw['jwpulang']);

         //cek selisih
         $timeabsm = strtotime($dtm) - strtotime($jadwalabsensimasuk);
         if($timeabsm <= 0 ){
            $selisihabsm = "00:00:00";
            $selisihabsdtkm = "0";
         }else{
            $selisihabsm = date("H:i:s",$timeabsm);
            $arrselisihabsm = date_parse($selisihabsm);
            $selisihabsdtkm = ($arrselisihabsm['hour']*3600) + ($arrselisihabsm['minute']*60) + $arrselisihabsm['second'];
         }

         $timeabsp = strtotime($jadwalabsensipulang) - strtotime($dtp);
         if($timeabsp <= 0 ){
            $selisihabsp = "00:00:00";
            $selisihabsdtkp = "0";
         }else{
            $selisihabsp = date("H:i:s",$timeabsp);
            $arrselisihabsp = date_parse($selisihabsp);
            $selisihabsdtkp = ($arrselisihabsp['hour']*3600) + ($arrselisihabsp['minute']*60) + $arrselisihabsp['second'];
         }

         //update data sebelumnya
         $this->edit("absensi_karyawan",array("status"=>"2"),"nip = '{$va['kode']}' and tglfinger = '$tglfinger'");

         //entry absen masuk
         $data    = array("nip"=>$va['kode'],"tgl"=> $tglabsensim,"tglfinger"=>$tglfinger,"mesin"=>"","mode"=>"1","jadwalabsensi"=>$jadwalabsensimasuk,
                  "absensi"=>$dtm,"kodeabsensi"=>$va['kodeabsensim'],"keterangan"=>"Absensi masuk [manual]","cabang"=>getsession($this, "cabang"),
                  "metode"=>"manual","status"=>"1","selisihabsensi"=>$selisihabsm,"selisihabsensidtk"=>$selisihabsdtkm,"toleransidtk"=>$toleransidtk,
                  "username"=> getsession($this, "username"),"datetime"=>date_now()) ;
         $this->insert("absensi_karyawan", $data) ;

         //entry absen pulang
         $data    = array("nip"=>$va['kode'],"tgl"=> $tglabsensip,"tglfinger"=>$tglfinger,"mesin"=>"","mode"=>"2","jadwalabsensi"=>$jadwalabsensipulang,
                  "absensi"=>$dtp,"kodeabsensi"=>$va['kodeabsensip'],"keterangan"=>"Absensi pulang [manual]","cabang"=>getsession($this, "cabang"),
                  "metode"=>"manual","status"=>"1","selisihabsensi"=>$selisihabsp,"selisihabsensidtk"=>$selisihabsdtkp,"toleransidtk"=>0,
                  "username"=> getsession($this, "username"),"datetime"=>date_now()) ;
         $this->insert("absensi_karyawan", $data) ;
         $error = "ok";
      }      
      return $error;
   }

   public function getdata($kode){
      $data = array() ;
		if($d = $this->getval("*", "kode = " . $this->escape($kode), "karyawan")){
         $data = $d;
		}
		return $data ;
   }

   public function deleting($va){
      $return = "ok";
      $va['tgl'] = date_2s($va['tgl']);
      $this->delete("absensi_karyawan_golongan", "nip = '{$va['nip']}' and tgl = '{$va['tgl']}'") ;
      
      return $return;
      
   }

   public function seekperiode($search){
      $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
      $dbd      = $this->select("sys_periode_payroll", "*", $where, "", "", "kode desc", '50') ;
      return array("db"=>$dbd) ;
   }

   public function seekkodeabsensim($search){
      $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%') and jenis = 'M'" ;
      $dbd      = $this->select("absensi_kode", "*", $where, "", "", "kode desc", '50') ;
      return array("db"=>$dbd) ;
   }

   public function seekkodeabsensip($search){
      $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%') and jenis = 'P'" ;
      $dbd      = $this->select("absensi_kode", "*", $where, "", "", "kode desc", '50') ;
      return array("db"=>$dbd) ;
   }

   public function getperiode($va){
      $where = "kode = '{$va['periode']}'" ;
      $dbd      = $this->select("sys_periode_payroll", "*", $where, "", "", "kode desc", '50') ;
      return array("db"=>$dbd) ;
   }

}
?>
