<?php
class Trpenjualanaset_m extends Bismillah_Model{

    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ; 
        if($search !== "") $where[]	= "(a.keterangan LIKE '%{$search}%')" ;
        if($va['status'] == "1"){
            $where[]	= "a.fakturpj = ''" ;
        }else{
            $where[]	= "a.fakturpj <> ''" ;
        }
        $where 	 = implode(" AND ", $where) ;
        $f        = "a.kode,a.keterangan,a.cabang,a.golongan,a.tglperolehan,a.fakturpj,a.tglpj,a.lama,
                    a.tglperolehan,a.fakturpj,a.tglpj,a.lama,a.hargaperolehan,a.hargajual,a.nilaibukujual,c.nama as customer" ;
        $join    = "left join customer c on c.kode = a.customer"; 
        $dbd      = $this->select("aset a", $f, $where, $join, "", "a.kode ASC") ;
        
        return array("db"=>$dbd, "rows"=>$this->rows($dbd)) ;
    } 

    public function saving($va, $id){ 
		$va['faktur'] = $this->getfaktur() ;
        $data    = array("fakturpj"=>$va['faktur'],"customer"=>$va['customer'],"tglpj"=>date_2s($va['tglpj']),"tglhabis"=>date_2s($va['tglpj']),
                "hargajual"=>string_2n($va['hargajual']),"nilaibukujual"=>string_2n($va['nilaibuku']), 
                "usernamejual"=> getsession($this, "username"),"datetimejual"=>date_now()) ;
        $where   = "kode = " . $this->escape($va['kode']) ;
        $this->edit("aset", $data, $where, "") ;
		
		$this->updtransaksi_m->updkartupiutangpenjualanaset($va['faktur']);
		$this->updtransaksi_m->updrekpenjualanaset($va['faktur']);
    }

    public function getdata($kode=''){
        $where 	 = "a.kode = " . $this->escape($kode);
        $join     = "left join aset_golongan g on g.kode = a.golongan left join cabang c on c.kode = a.cabang
					left join supplier s on s.kode = a.vendor left join customer k on k.kode = a.customer";
        $field    = "a.kode,a.keterangan,a.golongan,g.keterangan as ketgolongan,a.cabang, c.keterangan as ketcabang,a.lama,a.tglperolehan,a.hargaperolehan,a.unit,
                    a.jenispenyusutan,a.tarifpenyusutan,a.residu,a.faktur,s.nama as ketvendor,a.vendor,a.fakturpj,a.tglpj,a.customer,k.nama as namacustomer,
                    a.hargajual,a.nilaibukujual";
        $dbd      = $this->select("aset a", $field, $where, $join, "", "a.kode ASC","1") ;
        return $dbd;
    }

    public function seekgolaset($search){
        $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("aset_golongan", "*", $where, "", "", "keterangan ASC", '50') ;
        return array("db"=>$dbd) ;
    }
	
	public function seekcustomer($search){
        $where = "(kode LIKE '%{$search}%' OR nama LIKE '%{$search}%')" ;
        $dbd      = $this->select("customer", "*", $where, "", "", "nama ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    
	public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "AJ".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }
	
}
?>
