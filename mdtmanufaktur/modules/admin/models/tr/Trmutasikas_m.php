<?php
class Trmutasikas_m extends Bismillah_Model{
    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "t.faktur LIKE '%{$search}%'" ;
        $where[] = "t.status = '1' and t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "t.faktur,t.fktpengajuan,t.tgl,concat(t.rekening,' - ',r.keterangan) as rekkas,t.keterangan,t.debet,t.rekening,
                    t.kredit,t.diberiterima";
        $join     = "left join keuangan_rekening r on r.Kode = t.rekening";
        $dbd      = $this->select("kas_mutasi_total t", $field, $where, $join, "", "t.tgl,t. faktur ASC", $limit) ;
        $dba      = $this->select("kas_mutasi_total t", "t.id", $where) ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function saving($faktur, $va){
        $error = "";
        $faktur         = getsession($this, "ssmutasikas_faktur", "");
        if(string_2n($va['jumlah']) <= 0)$error .= "Jumlah tidak valid!!\\n";
        if(!isset($va['rekkas']))$error .= "Rek Kas tidak valid!!\\n";
        if(trim($va['keterangan']) == "")$error .= "Keterangan tidak valid!!\\n";
        if(trim($va['diberiterima']) == "")$error .= "Diberi / Diterima tidak valid!!\\n";

        //cek apakah fkt tsb sudah digunakan
        $w = "fakturrealisasi <> '' and fakturrealisasi <> '$faktur' and faktur = '{$va['fktpengajuan']}'";
        $dbd = $this->select("keuangan_pengajuan","faktur",$w);
        if($this->rows($dbd) > 0){
            $error .= "Pengajuan sudah digunakan sebelumnya!!\\n";
        }

        if($error == ""){
            $va['faktur']   = $faktur !== "" ? $faktur : $this->getfaktur() ;
            // pengajuan
            $arrpengajuan = array("fakturrealisasi"=>"","tglrealisasi"=>"0000-00-00");
            $wpengajuan = "fakturrealisasi = '{$va['faktur']}'";
            $this->edit("keuangan_pengajuan",$arrpengajuan,$wpengajuan);
            
            if($va['fktpengajuan'] <> ""){
                $arrpengajuan = array("fakturrealisasi"=>$va['faktur'],"tglrealisasi"=>$va['tgl']);
                $wpengajuan = "faktur = '{$va['fktpengajuan']}'";
                $this->edit("keuangan_pengajuan",$arrpengajuan,$wpengajuan);
            }

            //tot mutasi
            $debet = string_2n($va['jumlah']);
            $kredit = 0;
            if($va['jenis'] == "KK"){
                $kredit = string_2n($va['jumlah']);
                $debet = 0;
            }
            
            
            $data           = array("faktur"=>$va['faktur'],
                                    "fktpengajuan"=>$va['fktpengajuan'],
                                    "tgl"=>$va['tgl'],
                                    "status"=>"1",
                                    "rekening"=>$va['rekkas'],
                                    "keterangan"=>$va['keterangan'],
                                    "diberiterima"=>$va['diberiterima'],
                                    "debet"=>$debet,
                                    "kredit"=>$kredit,
                                    "cabang"=> getsession($this, "cabang"),
                                    "username"=> getsession($this, "username"),
                                    "datetime"=>date("Y-m-d H:i:s")) ;
            $where          = "faktur = " . $this->escape($faktur) ;
            $this->update("kas_mutasi_total", $data, $where, "") ;

            //insert detail
            $vaGrid = json_decode($va['grid2']);
            $this->delete("kas_mutasi_detail", "faktur = '{$va['faktur']}'" ) ;
            foreach($vaGrid as $key => $val){

                $kredit = $val->nominal;
                $debet = 0;
                if($va['jenis'] == "KK"){
                    $debet = $val->nominal;
                    $kredit = 0;

                }

                $vadetail = array("faktur"=>$va['faktur'],
                                "rekening"=>$val->kode,
                                "keterangan"=>$val->keterangan,
                                "debet"=>$debet,
                                "kredit"=>$kredit);
                $this->insert("kas_mutasi_detail",$vadetail);
            }
            $this->updtransaksi_m->updrekmutasikas($va['faktur']);
            
            $error = "ok";
        }
        return $error;
    }

    public function getdatatotal($faktur){
        $data  = array() ;
        $field = "t.faktur,t.tgl,t.diberiterima,r.keterangan as ketrekening,t.rekening,t.keterangan,t.debet,t.kredit,
                t.fktpengajuan,p.yangmengajukan,p.keterangan as ketpengajuan";
        $where = "t.faktur = '$faktur'";
        $join  = "left join keuangan_rekening r on r.kode = t.rekening
                left join keuangan_pengajuan p on p.faktur = t.fktpengajuan";
        $dbd   = $this->select("kas_mutasi_total t", $field, $where, $join) ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }

    public function getdatadetail($faktur){
        $field = "d.faktur,r.kode,concat(d.rekening,'-',r.keterangan) as ketrekening,(d.debet + d.kredit) as nominal,d.keterangan";
        $where = "d.faktur = '$faktur'";
        $join  = "left join keuangan_rekening r on r.kode = d.rekening";
        $dbd   = $this->select("kas_mutasi_detail d", $field, $where, $join) ;
        return $dbd ;
    }

    public function deleting($faktur){
        $error = "";
        if($error == ""){
            $arrpengajuan = array("fakturrealisasi"=>"","tglrealisasi"=>"0000-00-00");
            $wpengajuan = "fakturrealisasi = '$faktur'";
            $this->edit("keuangan_pengajuan",$arrpengajuan,$wpengajuan);

            $this->edit("kas_mutasi_total",array("status"=>2),"faktur = " . $this->escape($faktur));
            $this->delete("keuangan_bukubesar","faktur = " . $this->escape($faktur));
            $error = "ok";
        }
        return $error;
    }

    public function seekrekening($search){
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%') and jenis = 'D'" ;
        $dbd      = $this->select("keuangan_rekening", "*", $where, "", "", "kode ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "MK".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }

    public function getdata($kode){
        $data = array() ;
        if($d = $this->getval("*", "kode = " . $this->escape($kode), "stock")){
            $data = $d;
        }
        return $data ;
    }
    
    public function loaddetail($va){ 
        $where    = array("b.faktur = '".$va['faktur']."' and b.rekening = '".$va['rekening']."'") ;    
        //if($search !== "") $where[]   = "(b.rekening LIKE '{$search}%' OR b.keterangan LIKE '%{$search}%' OR b.debet LIKE '%{$search}%' OR b.kredit LIKE '%{$search}%')" ;
        $where    = implode(" AND ", $where) ;   

        $f        = "b.id no,b.tgl,b.faktur,b.keterangan,b.debet,b.kredit,b.kredit total,b.username" ;     
        $join     = "left join keuangan_rekening r on r.kode = b.rekening"  ;
        $dbd      = $this->select("keuangan_bukubesar b", $f, $where, $join, "", "b.id ASC") ;

        return $dbd;
    }

    public function loadgrid3($va)
    {   
        
        $faktur = $va['faktur'];
        $tgl = date_2s($va['tgl']);
        $limit = $va['offset'] . "," . $va['limit'];
        $search = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "";
        $search = $this->escape_like_str($search);
        $where = array();
        $arrdata = array();
        if ($search !== "") {
            $where[] = "(p.faktur LIKE '{$search}%' OR p.yangmengajukan LIKE '%{$search}%')";
        }

        $where[] = "p.status = '1'"; //and b.faktur is null" ;
        $where[] = "(p.fakturrealisasi = '' or p.fakturrealisasi ='$faktur') and p.tgl <= '$tgl'"; 
        $where = implode(" AND ", $where);
        $field = "p.faktur,p.tgl,p.yangmengajukan,p.keterangan,p.jumlah";
        $join = "";
        $dbd = $this->select("keuangan_pengajuan p", $field, $where, $join, "", "p.tgl asc");
        
        return array("db" => $dbd);
    }

    public function getdatapengajuan($faktur){
        $data = array();
        $where = "p.faktur = '$faktur'";
        $field = "p.faktur,p.tgl,p.yangmengajukan,p.keterangan,p.jumlah";
        $join = "left join keuangan_pengajuan_otorisasi o on o.faktur = p.faktur";
        $dbd = $this->select("keuangan_pengajuan p", $field, $where, $join, "", "");
        if ($dbr = $this->getrow($dbd)) {
            $data = $dbr;
        }
        return $data;
    }

    public function cekstatusotorisasi($fktpengajuan){
        $otorisasi = true;
        $where = "faktur = '$fktpengajuan' and tahap <> 0 and status = '0'";
        $dbd = $this->select("keuangan_pengajuan_otorisasi","*",$where);
        while($dbr = $this->getrow($dbd)){
            $otorisasi = false;
            break;
        }
        return $otorisasi;
    }
}
?>
