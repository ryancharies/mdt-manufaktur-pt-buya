<?php
class Trprosesproduksi_m extends Bismillah_Model{
    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        $tgl = date("Y-m-d");
		$where[] = "t.faktur like '$search%'";
        $where[] = "t.status = '1' and (t.tglclose >= '$tgl' or t.tglclose = '0000-00-00')";
        $where 	 = implode(" AND ", $where) ;
        $field    = "t.faktur,t.tgl,c.keterangan as cabang,t.petugas";
        $join     = "left join cabang c on c.kode = t.cabang";
        $dbd      = $this->select("produksi_total t", $field, $where, $join, "", "t. faktur ASC", $limit) ;
        $dba      = $this->select("produksi_total t", "t.id", $where) ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function saving($faktur, $va){

    }

    public function savingbb($va){
        $faktur = $this->getfaktur();
        $cabang = getsession($this, "cabang") ;        
        $vadetail = array("faktur"=>$faktur,"cabang"=>$cabang,"gudang"=>$va['gudang'],"fakturproduksi"=>$va['fakturprod'],"tgl"=>$va['tgl'],
                          "stock"=>$va['stock'],"qty"=>$va['qty'],"username"=>getsession($this, "username") ,"datetime"=>date("Y-m-d H:i:s"));
        $this->insert("produksi_bb",$vadetail);

        $this->updtransaksi_m->updkartustockprosesproduksi($faktur);
        $this->updtransaksi_m->updrekprosesproduksi($faktur);

    }

    public function savingbbautospp($va){
        $igd = $this->func_m->getinfogudang($va['gudang']) ;   

        $cField = "s.kode,s.keterangan,s.satuan,b.qty,b.hp,b.jmlhp,p.qty as qtyprod";
        $cJoin  = "LEFT JOIN stock s ON s.kode = b.stock left join produksi_produk p on p.fakturproduksi = b.faktur" ;
        $cWhere = "b.faktur = '{$va['fakturprod']}'" ;
        $dbData = $this->select("produksi_bb_standart b",$cField,$cWhere,$cJoin) ;
        while($dbr = $this->getrow($dbData)){
            $faktur = $this->getfaktur();
            $qty = $dbr['qtyprod'] * $dbr['qty'];
            $vadetail = array("faktur"=>$faktur,"cabang"=>$igd['cabang'],"gudang"=>$va['gudang'],"fakturproduksi"=>$va['fakturprod'],"tgl"=>date_2s($va['tgl']),
                              "stock"=>$dbr['kode'],"qty"=>$qty,"username"=>getsession($this, "username") ,"datetime"=>date("Y-m-d H:i:s"));
            $this->insert("produksi_bb",$vadetail);

            $this->updtransaksi_m->updkartustockprosesproduksi($faktur);
            $this->updtransaksi_m->updrekprosesproduksi($faktur);
        }

    }
    public function getdatatotal($faktur){
        $data  = array() ;
        $field = "t.faktur,t.tgl,t.cabang,c.keterangan as ketcabang,t.perbaikan,t.petugas";
        $where = "t.faktur = '$faktur'";
        $join  = "left join cabang c on c.kode = t.cabang";
        $dbd   = $this->select("produksi_total t", $field, $where, $join) ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }

    public function deletebb($faktur){
        $this->edit("produksi_bb",array("status"=>2),"faktur = " . $this->escape($faktur));
        $this->delete("stock_kartu","faktur = " . $this->escape($faktur));
        $this->delete("keuangan_bukubesar","faktur = " . $this->escape($faktur));
    }

    public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "BB".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }

    public function getdata($kode){
        $data = array() ;
        if($d = $this->getval("*", "kode = " . $this->escape($kode), "stock")){
            $data = $d;
        }
        return $data ;
    }
    public function loadgrid2($va){
        $where = array();
        $where[]	= "p.fakturproduksi = '{$va['fakturproduksi']}' and p.status = '1'" ;
        $where 	 = implode(" AND ", $where) ;
        $field = "p.faktur,p.tgl,p.gudang,p.stock,s.keterangan as namastock,p.qty,s.satuan";
        $join = "left join stock s on s.kode = p.stock";
        $dbd      = $this->select("produksi_bb p", $field, $where, $join, "", "p.tgl desc,p.datetime desc") ;
        $dba      = $this->select("produksi_bb p", "p.id", $where) ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function getdatadetail($faktur){
        $field = "p.stock,s.keterangan as namastock,p.qty,s.satuan";
        $where = "p.fakturproduksi = '$faktur'";
        $join  = "left join stock s on s.kode = p.stock";
        $dbd   = $this->select("produksi_produk p", $field, $where, $join) ;
        return $dbd ;
    }
}
?>
