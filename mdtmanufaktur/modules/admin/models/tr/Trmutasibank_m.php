<?php
class Trmutasibank_m extends Bismillah_Model{
    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "t.faktur LIKE '%{$search}%'" ;
        $where[] = "t.status = '1' and t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "t.faktur,t.fktpengajuan,t.tgl,concat(t.bank,' - ',r.keterangan) as bankket,t.keterangan,t.debet,t.bank,
                    t.kredit,t.diberiterima";
        $join     = "left join bank r on r.Kode = t.bank";
        $dbd      = $this->select("bank_mutasi_total t", $field, $where, $join, "", "t. faktur ASC", $limit) ;
        $dba      = $this->select("bank_mutasi_total t", "t.id", $where) ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function saving($faktur, $va){
        $faktur         = getsession($this, "ssmutasibank_faktur", "");
        $error = "";
        if(string_2n($va['jumlah']) <= 0)$error .= "Jumlah tidak valid!!\\n";
        if(!isset($va['bank']))$error .= "Bank tidak valid!!\\n";
        if(trim($va['keterangan']) == "")$error .= "Keterangan tidak valid!!\\n";
        if(trim($va['diberiterima']) == "")$error .= "Diberi / Diterima tidak valid!!\\n";

        //cek apakah fkt tsb sudah digunakan
        $w = "fakturrealisasi <> '' and fakturrealisasi <> '$faktur' and faktur = '{$va['fktpengajuan']}'";
        $dbd = $this->select("keuangan_pengajuan","faktur",$w);
        if($this->rows($dbd) > 0){
            $error .= "Pengajuan sudah digunakan sebelumnya!!\\n";
        }

        if($error == ""){
            
            $va['faktur']   = $faktur !== "" ? $faktur : $this->getfaktur() ;
            // pengajuan
            $arrpengajuan = array("fakturrealisasi"=>"","tglrealisasi"=>"0000-00-00");
            $wpengajuan = "fakturrealisasi = '{$va['faktur']}'";
            $this->edit("keuangan_pengajuan",$arrpengajuan,$wpengajuan);
            
            if($va['fktpengajuan'] <> ""){
                $arrpengajuan = array("fakturrealisasi"=>$va['faktur'],"tglrealisasi"=>$va['tgl']);
                $wpengajuan = "faktur = '{$va['fktpengajuan']}'";
                $this->edit("keuangan_pengajuan",$arrpengajuan,$wpengajuan);
            }

            $debet = string_2n($va['jumlah']);
            $kredit = 0;
            if($va['jenis'] == "BK"){
                $kredit = string_2n($va['jumlah']);
                $debet = 0;
            }
            
            $data           = array("faktur"=>$va['faktur'],
                                    "fktpengajuan"=>$va['fktpengajuan'],
                                    "tgl"=>$va['tgl'],
                                    "status"=>"1",
                                    "bank"=>$va['bank'],
                                    "keterangan"=>$va['keterangan'],
                                    "diberiterima"=>$va['diberiterima'],
                                    "debet"=>$debet,
                                    "kredit"=>$kredit,
                                    "cabang"=> getsession($this, "cabang"),
                                    "username"=> getsession($this, "username"),
                                    "datetime"=>date("Y-m-d H:i:s")) ;
            $where          = "faktur = " . $this->escape($faktur) ;
            $this->update("bank_mutasi_total", $data, $where, "") ;

            //insert detail
            $vaGrid = json_decode($va['grid2']);
            $this->delete("bank_mutasi_detail", "faktur = '{$va['faktur']}'" ) ;
            foreach($vaGrid as $key => $val){

                $kredit = $val->nominal;
                $debet = 0;
                if($va['jenis'] == "BK"){
                    $debet = $val->nominal;
                    $kredit = 0;

                }

                $vadetail = array("faktur"=>$va['faktur'],
                                "rekening"=>$val->kode,
                                "keterangan"=>$val->keterangan,
                                "debet"=>$debet,
                                "kredit"=>$kredit);
                $this->insert("bank_mutasi_detail",$vadetail);
            }
            $this->updtransaksi_m->updrekmutasibank($va['faktur']);
            $error = "ok";
        }
        return $error;
    }

    public function getdatatotal($faktur){
        $data  = array() ;
        $field = "t.faktur,t.tgl,t.diberiterima,r.keterangan as ketbank,t.bank,t.keterangan,t.debet,t.kredit,
                t.fktpengajuan,p.yangmengajukan,p.keterangan as ketpengajuan";
        $where = "t.faktur = '$faktur'";
        $join  = "left join bank r on r.kode = t.bank
                  left join keuangan_pengajuan p on p.faktur = t.fktpengajuan";
        $dbd   = $this->select("bank_mutasi_total t", $field, $where, $join) ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }

    public function getdatadetail($faktur){
        $field = "d.faktur,r.kode,concat(d.rekening,'-',r.keterangan) as ketrekening,(d.debet + d.kredit) as nominal,d.keterangan";
        $where = "d.faktur = '$faktur'";
        $join  = "left join keuangan_rekening r on r.kode = d.rekening";
        $dbd   = $this->select("bank_mutasi_detail d", $field, $where, $join) ;
        return $dbd ;
    }

    public function deleting($faktur){
        $error = "";
        if($error == ""){
            $arrpengajuan = array("fakturrealisasi"=>"","tglrealisasi"=>"0000-00-00");
            $wpengajuan = "fakturrealisasi = '$faktur'";
            $this->edit("keuangan_pengajuan",$arrpengajuan,$wpengajuan);

            $this->edit("bank_mutasi_total",array("status"=>2),"faktur = " . $this->escape($faktur));
            $this->delete("keuangan_bukubesar","faktur = " . $this->escape($faktur));
            $error = "ok";
        }
        return $error;
    }

    public function seekrekening($search){
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%') and jenis = 'D'" ;
        $dbd      = $this->select("keuangan_rekening", "*", $where, "", "", "kode ASC", '50') ;
        return array("db"=>$dbd) ;
    }
    
    public function seekbank($search){
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("bank", "*", $where, "", "", "kode ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "MB".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }

    public function loadgrid3($va)
    {   
        
        $faktur = $va['faktur'];
        $tgl = date_2s($va['tgl']);
        $limit = $va['offset'] . "," . $va['limit'];
        $search = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "";
        $search = $this->escape_like_str($search);
        $where = array();
        $arrdata = array();
        if ($search !== "") {
            $where[] = "(p.faktur LIKE '{$search}%' OR p.yangmengajukan LIKE '%{$search}%')";
        }

        $where[] = "p.status = '1'"; //and b.faktur is null" ;
        $where[] = "(p.fakturrealisasi = '' or p.fakturrealisasi ='$faktur') and p.tgl <= '$tgl'"; 
        $where = implode(" AND ", $where);
        $field = "p.faktur,p.tgl,p.yangmengajukan,p.keterangan,p.jumlah";
        $join = "";
        $dbd = $this->select("keuangan_pengajuan p", $field, $where, $join, "", "p.tgl asc");
        
        return array("db" => $dbd);
    }

    public function getdatapengajuan($faktur){
        $data = array();
        $where = "p.faktur = '$faktur'";
        $field = "p.faktur,p.tgl,p.yangmengajukan,p.keterangan,p.jumlah";
        $join = "left join keuangan_pengajuan_otorisasi o on o.faktur = p.faktur";
        $dbd = $this->select("keuangan_pengajuan p", $field, $where, $join, "", "");
        if ($dbr = $this->getrow($dbd)) {
            $data = $dbr;
        }
        return $data;
    }

    public function cekstatusotorisasi($fktpengajuan){
        $otorisasi = true;
        $where = "faktur = '$fktpengajuan' and tahap <> 0 and status = '0'";
        $dbd = $this->select("keuangan_pengajuan_otorisasi","*",$where);
        while($dbr = $this->getrow($dbd)){
            $otorisasi = false;
            break;
        }
        return $otorisasi;
    }

}
?>
