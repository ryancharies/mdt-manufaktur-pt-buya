<?php

class Trimportabsensikaryawan_m extends Bismillah_Model {

    public function saving( $va ) {

       // print_r($va);
        $error = '';
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        //cek valid
        if ( trim( $va['periode'] ) == '' )$error .= 'Payroll komponen gaji tidak valid !! \\n';
        if ( trim( $va['mesin'] ) == '' )$error .= 'Mesin tidak valid !! \\n';
        if ( $error == '' ) {
            $vaGrid = json_decode($va['grid1']);
            $where =  "mesin = '{$va['mesin']}' and tgl >= '{$va['tglawal']}' and tgl <= '{$va['tglakhir']}' and metode = 'import'" ;
            $this->edit("absensi_karyawan",array("status"=>'2'),$where) ;
            $this->edit("lembur",array("status"=>'2'),$where) ;
            foreach($vaGrid as $key => $val){
                if(trim($val->nip) <> ""){
                    $tglfinger = date_2s($val->tgl);
                    //insert mode masuk
                    $tgljw = date_2s($val->tgl);
                    $absensimasuk = "0000-00-00 00:00:00";
                    if(trim($val->masuk) <> "")$absensimasuk = $tgljw ." ".trim($val->masuk).":00";
                    $jwabsensimasuk = $tgljw ." ".trim($val->jwmasuk).":00";
                    $terlambat = $val->terlambat;
                    if(trim($terlambat) == ""){
                        $terlambat = "00:00:00";    
                    }else{
                        $terlambat = $val->terlambat.":00";
                    }
                    $arrterlambat = date_parse($terlambat);
                    $selisihdtk = ($arrterlambat['hour']*3600) + ($arrterlambat['minute']*60) + $arrterlambat['second'];
                    $tglmasuk = date_2s($val->tgl);
                    
                    //update data
                    $where =  "nip= '{$val->nip}' and tgl = '$tglmasuk' and mode = '1' and metode = 'import'" ;
                    $this->edit("absensi_karyawan",array("status"=>'2'),$where) ;

                    //kodeabsensi
                    $kodeabsensi =  $this->getconfig("kodeabsabsent");
                    $ketmasuk = "Absent";
                    $ketpulang = "Absent";
                    if($val->status == "Masuk"){
                        $kodeabsensi =  $this->getconfig("kodeabsmasuk");
                        $ketmasuk = "Absensi masuk [import]";
                        $ketpulang = "Absensi pulang [import]";

                    }

                    //insert data
                    $vadetail = array("nip"=>$val->nip,
                                "tgl"=>$tglmasuk,
                                "tglfinger"=>$tglfinger,
                                "mesin"=>$va['mesin'],
                                "mode" => "1",
                                "absensi"=>$absensimasuk,
                                "jadwalabsensi"=>$jwabsensimasuk,
                                "selisihabsensi"=>$terlambat,
                                "selisihabsensidtk"=>$selisihdtk,
                                "toleransidtk"=>"300",
                                "kodeabsensi"=> $kodeabsensi,
                                "keterangan"=>$ketmasuk,
                                "metode"=>"import",
                                "status"=>"1",
                                "cabang"=> getsession($this, "cabang"),
                                "username"=> getsession($this, "username"),
                                "datetime"=>date_now());
                    $this->insert("absensi_karyawan",$vadetail);
                    

                    //entry pulang
                    $absensipulang = "0000-00-00 00:00:00";
                    if(trim($val->pulang) <> "")$absensipulang = $tgljw ." ".trim($val->pulang).":00";
                    $jwabsensipulang = $tgljw ." ".trim($val->jwpulang).":00";
                    // $tglpulang = $tglmasuk;
                    if($absensimasuk >= $absensipulang && trim($val->pulang) <> ""){
                        $tglslanjutnya = date("Y-m-d",strtotime($tgljw) + (60*60*24));
                        // $tglpulang = $tglslanjutnya;
                        $absensipulang = $tglslanjutnya ." ".trim($val->pulang).":00";
                        $jwabsensipulang = $tglslanjutnya ." ".trim($val->jwpulang).":00";
                    }

                    $plgcepat = $val->plgcepat;
                    if(trim($plgcepat) == ""){
                        $plgcepat = "00:00:00";    
                    }else{
                        $plgcepat = $val->plgcepat.":00";
                    }
                    $arrplgcepat = date_parse($plgcepat);
                    $selisihdtk = ($arrplgcepat['hour']*3600) + ($arrplgcepat['minute']*60) + $arrplgcepat['second'];
                    
                    //update data
                    $where =  "nip= '{$val->nip}' and tgl = '$tglmasuk' and mode = '2' and metode = 'import'" ;
                    $this->edit("absensi_karyawan",array("status"=>'2'),$where) ;
                    
                    //insert data
                    $vadetail = array("nip"=>$val->nip,
                                "tgl"=>$tglmasuk,
                                "tglfinger"=>$tglfinger,
                                "mesin"=>$va['mesin'],
                                "mode" => "2",
                                "absensi"=>$absensipulang,
                                "jadwalabsensi"=>$jwabsensipulang,
                                "selisihabsensi"=>$plgcepat,
                                "selisihabsensidtk"=>$selisihdtk,
                                "kodeabsensi"=> $kodeabsensi,
                                "keterangan"=>$ketpulang,
                                "metode"=>"import",
                                "status"=>"1",
                                "cabang"=> getsession($this, "cabang"),
                                "username"=> getsession($this, "username"),
                                "datetime"=>date_now());
                    $this->insert("absensi_karyawan",$vadetail);
                    

                    if(trim($val->lembur) <> ""){
                        $nolembur = $this->getnolembur();
                       // $error .= $tgljw."|";
                        $arrlembur = array("nolembur"=>$nolembur,
                                            "tgl"=>$tgljw,
                                            "keterangan"=>"Lembur otomtasi by import",
                                            "status"=>"1",
                                            "mesin"=>$va['mesin'],
                                            "metode"=>"import",
                                            "cabang"=> getsession($this, "cabang"),
                                            "username"=> getsession($this, "username"),
                                            "datetime"=>date_now());
                        $this->insert("lembur",$arrlembur);

                        
                        //$datetimeakhir = $jwabsensipulang;

                        $jmljamlembur = trim($val->lembur).":00";
                        $jmljamlemburstr = waktu_2strintval($jmljamlembur,"+");
                       // $error .= $jmljamlembur."#";

                        $timeawal = strtotime($jwabsensipulang);//strtotime($jmljamlemburstr,strtotime($jwabsensipulang));
                        $datetimeawal = date("Y-m-d H:i:s",$timeawal);
                        $datetimeakhir = strtotime($jmljamlemburstr,strtotime($datetimeawal));
                        $datetimeakhir = date("Y-m-d H:i:s",$datetimeakhir);

                        $arrlemburk = array("nolembur"=>$nolembur,
                                            "nip"=>$val->nip,
                                            "keterangan"=>"Lembur otomatis by import ".$datetimeakhir,
                                            "datetimeawal"=>$datetimeawal,
                                            "datetimeakhir"=>$datetimeakhir);
                        $this->insert("lembur_karyawan",$arrlemburk);
                    }
                }
            }
        }
        //simpan detail
        return $error;
    }

    public function seekperiode($search){
        $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("sys_periode_payroll", "*", $where, "", "", "kode desc", '50') ;
        return array("db"=>$dbd) ;
    }
    
    public function seekmesin($search){
        $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("absensi_mesin", "*", $where, "", "", "kode asc", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getperiode($va){
        $where = "kode = '{$va['periode']}'" ;
        $dbd      = $this->select("sys_periode_payroll", "*", $where, "", "", "kode desc", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getagspinjaman($nopinjaman,$periode){
        $return = array("pokok"=>0,"status"=>0);
        $where = "nopinjaman = '$nopinjaman' and periode = '$periode'" ;
        $dbd   = $this->select("payroll_pinjaman_karyawan_angsuran", "pokok,status", $where) ;
        if($dbr = $this->getrow($dbd)){
            $return = $dbr;
        }
        return $return;
    }

    public function getnolembur($l = true)
    {
        $cabang = getsession($this, "cabang");
        $key = "LBR" . $cabang . date("ymd");
        $n = $this->getincrement($key, $l, 4);
        $faktur = $key . $n;
        return $faktur;
    }
}
?>
