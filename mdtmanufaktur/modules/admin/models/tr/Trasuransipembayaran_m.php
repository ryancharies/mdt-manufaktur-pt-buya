<?php
class Trasuransipembayaran_m extends Bismillah_Model{
    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "t.faktur LIKE '%{$search}%'" ;
        $where[] = "t.status = '1' and t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "t.faktur,t.tgl,a.keterangan asuransi,t.karyawan,t.perusahaan,t.total,t.rekkas,t.cabang,t.status,t.username,t.datetime";
        $join     = "left join asuransi a on a.kode = t.asuransi";
        $dbd      = $this->select("asuransi_pembayaran_total t", $field, $where, $join, "", "t. faktur ASC", $limit) ;
        $dba      = $this->select("asuransi_pembayaran_total t", "t.id", $where, $join, "", "t. faktur ASC") ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function saving($faktur, $va){
        $faktur         = getsession($this, "ssasuransipembayaran_faktur", "");
        $va['faktur']   = $faktur !== "" ? $faktur : $this->getfaktur() ;
        $total = string_2n($va['totkaryawan']) + string_2n($va['totperusahaan']);
        $data           = array("faktur"=>$va['faktur'],
                                "tgl"=>date_2s($va['tgl']),
                                "karyawan"=>string_2n($va['totkaryawan']),
                                "perusahaan"=>string_2n($va['totperusahaan']),
                                "total"=>$total,
                                "rekkas"=>$va['rekkasbank'],
                                "asuransi"=>$va['asuransi'],
                                "keterangan"=>$va['keterangan'],
                                "status"=>"1",
                                "cabang"=> getsession($this, "cabang"),
                                "username"=> getsession($this, "username"),
                                "datetime"=>date("Y-m-d H:i:s")) ;
        $where          = "faktur = " . $this->escape($faktur) ;
        $this->update("asuransi_pembayaran_total", $data, $where, "") ;

        //insert detail
        $vaGrid = json_decode($va['grid2']);
        $this->delete("asuransi_pembayaran_detail", "faktur = '{$va['faktur']}'" ) ;
        foreach($vaGrid as $key => $val){

            $vadetail = array("faktur"=>$va['faktur'],
                              "nopendaftaran"=>$val->nopendaftaran,
                              "tarifperusahaan"=>$val->tarifperusahaan,
                              "tarifkaryawan"=>$val->tarifkaryawan);
            $this->insert("asuransi_pembayaran_detail",$vadetail);
        }
        $this->updtransaksi_m->updrekasuransipembayaran($va['faktur']);
    }

    public function getdatatotal($faktur){
        $data  = array() ;
        $field = "t.faktur,t.tgl,t.asuransi,a.keterangan ketasuransi,t.karyawan,t.perusahaan,t.total,t.rekkas,t.keterangan,
                    t.cabang,t.status,t.username,t.datetime,r.keterangan as ketrekkas";
        $where = "t.faktur = '$faktur'";
        $join  = "left join asuransi a on a.kode = t.asuransi left join keuangan_rekening r on r.kode = t.rekkas";
        $dbd   = $this->select("asuransi_pembayaran_total t", $field, $where, $join,"t.faktur") ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }

    public function deleting($faktur){
        $this->edit("asuransi_pembayaran_total",array("status"=>2),"faktur = " . $this->escape($faktur));
        $this->delete("keuangan_bukubesar","faktur = " . $this->escape($faktur));
    }

    public function seekrekening($search){
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%') and jenis = 'D'" ;
        $dbd      = $this->select("keuangan_rekening", "*", $where, "", "", "kode ASC", '100') ;
        return array("db"=>$dbd) ;
    }
    

    public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "IP".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }

    public function seekasuransi($search){
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("asuransi", "*", $where, "", "", "kode ASC", '100') ;
        return array("db"=>$dbd) ;
    }

    public function loaddataasuransi($va){
        $tgl = date_2s($va['tgl']);
        
            $w = "k.asuransi = '{$va['asuransi']}' and k.tgl <= '$tgl' and status = '1'";
            $j = "left join karyawan p on p.kode = k.karyawan left join asuransi a on a.kode = k.asuransi";
            $f = "k.nopendaftaran,k.noasuransi,k.tgl,k.karyawan,k.asuransi,k.produk,p.nama,p.alamat,a.keterangan as ketasuransi";
            $dbd = $this->select("asuransi_karyawan k",$f,$w,$j);
        
        
        return $dbd;
    }

    public function loaddataasuransiedit($va){
        
            $w = "d.faktur = '{$va['faktur']}'";
            $j = "left join asuransi_karyawan k on k.nopendaftaran = d.nopendaftaran
                left join karyawan p on p.kode = k.karyawan left join asuransi a on a.kode = k.asuransi";
            $f = "k.nopendaftaran,k.noasuransi,k.tgl,k.karyawan,k.asuransi,k.produk,p.nama,p.alamat,a.keterangan as ketasuransi,
                d.tarifkaryawan,d.tarifperusahaan";
            $dbd = $this->select("asuransi_pembayaran_detail d",$f,$w,$j);
        
        
        return $dbd;
    }

}
?>
