<?php
class Trotpengajuandana_m extends Bismillah_Model{
    public function loadgrid($va){
		$username = getsession($this, "username");
       
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "p.faktur LIKE '%{$search}%'" ;
        $where[] = "p.status = '1' and o.username = '$username' and o.tahap <> 0 and o.status = '0'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "p.faktur,p.tgl,p.keterangan,p.jumlah,p.yangmengajukan,p.jenis,o.tahap,
        (select b.tahap from keuangan_pengajuan_otorisasi b where b.faktur = p.faktur and b.tahap <> '0' and b.status = '0' order by b.tahap asc limit 1) tahapakhir";
        $join     = "left join keuangan_pengajuan p on p.faktur = o.faktur";
        $dbd      = $this->select("keuangan_pengajuan_otorisasi o", $field, $where, $join, "", "p.id asc") ;

        return array("db"=>$dbd) ;
    }

    public function saving($faktur, $va){
		$username = getsession($this, "username");
        
        $jumlah = string_2n($va['jumlah']);
        $error = "";

        if($jumlah <= 0)$error .= "Jumlah yang diajukan tidak valid !!\\n";
        if(empty($va['yangmengajukan']))$error .= "Yang mengajukan tidak valid !!\\n";
        if(empty($va['keterangan']))$error .= "Keterangan tidak valid !!\\n";
        if(empty($va['faktur']))$error .= "Faktur tidak valid !!\\n";
        if(empty($va['ketotorisasi']))$error .= "Ket Otorisasi tidak valid !!\\n";
        if(empty($username))$error .= "Username tidak valid !!\\n";
        

        if($error == ""){
                $where = "faktur = '{$va['faktur']}' and username = '$username'";
                $vadetail = array("datetimeotorisasi"=>date_now(),
                                "status"=>$va['statusot'],
                                "ketotorisasi"=>$va['ketotorisasi']
                                );
                $this->edit("keuangan_pengajuan_otorisasi",$vadetail,$where);
            

            $error = "ok";
        }
        return $error;
    }

    public function getdatatotal($faktur){
        $data  = array() ;
        $field = "t.faktur,t.tgl,t.keterangan,t.jumlah,t.yangmengajukan,t.jenis,t.cabang";
        $where = "t.faktur = '$faktur'";
        $join  = "";
        $dbd   = $this->select("keuangan_pengajuan t", $field, $where, $join) ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }

    public function getdatadetail($faktur){
        $field = "d.faktur,d.username,u.fullname as namakaryawan,d.jabatan,d.tahap";
        $where = "d.faktur = '$faktur'";
        $join  = "left join sys_username u on u.username = d.username";
        $dbd   = $this->select("keuangan_pengajuan_otorisasi d", $field, $where, $join,"","d.tahap asc") ;
        return $dbd ;
    }

    public function deleting($faktur){
        $error = "";
        $dbd = $this->select("keuangan_pengajuan","faktur","faktur = '$faktur' and tglotorisasi <> '0000-00-00'");
        if($dbr = $this->getrow($dbd)){
            $error .= "Data tidak bisa dihapus karena sudah proses otorisasi";
        }else{
            $dbd = $this->select("keuangan_pengajuan_otorisasi","faktur","faktur = '$faktur' and status <> '0'");
            if($dbr = $this->getrow($dbd)){
                $error .= "Data tidak bisa dihapus karena sedang tahap otorisasi";
            }
        }
        
        if($error == ""){
            $this->edit("keuangan_pengajuan",array("status"=>2),"faktur = " . $this->escape($faktur));
        }
        
        return $error;
    }

    public function seekotorisasi($search){
        $where = "(username LIKE '{$search}%' OR fullname LIKE '%{$search}%')" ;
        $dbd      = $this->select("sys_username", "*", $where, "", "", "fullname ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "RF".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }
}
?>
