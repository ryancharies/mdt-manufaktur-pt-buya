<?php
class Trpencairanbg_m extends Bismillah_Model{

    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ; 
        if($search !== "") $where[]	= "(a.nobgcek LIKE '%{$search}%')" ;
        if($va['jenis'] == "1"){
            $where[]	= "a.fakturcair = '' and a.status = '1'" ;
        }else if($va['jenis'] == "2"){
            $where[]	= "a.fakturcair <> '' and a.status = '1'" ;
        }else if($va['jenis'] == "3"){
            $where[]	= "a.status = '3'" ;
        }
        $where 	 = implode(" AND ", $where) ;
        $f        = "a.*,b.keterangan as ketbank,s.nama as namasupplier" ;
        $join    = "left join supplier s on s.kode = a.kpddari left join bank b on b.kode = a.bank"; 
        $dbd      = $this->select("bg_list a", $f, $where, $join, "", "a.tgl desc") ;
        
        return array("db"=>$dbd, "rows"=>$this->rows($dbd)) ;
    } 

    public function saving($va, $id){ 
        //print_r($va);
        if($va['status'] == "1"){
            $va['fakturcair'] = $this->getfaktur() ;
        }else{
            $va['fakturcair'] = "";
            $va['nominal'] = 0 ;
        }
		
        $data    = array("fakturcair"=>$va['fakturcair'],"tglcair"=>date_2s($va['tglcair']),"nominalcair"=>string_2n($va['nominal']),
                "keterangan"=>$va['keterangan'],"status"=>$va['status'], 
                "usernamecair"=> getsession($this, "username"),"datetimecair"=>date_now()) ;
        $where   = "nobgcek = " . $this->escape($va['nobgcek']) ." and faktur = '{$va['faktur']}'" ;
        $this->edit("bg_list", $data, $where, "") ;
		
		if($va['status'] == "1")$this->updtransaksi_m->updrekpencairanbg($va['fakturcair']);
    }

    public function getdata($nobgcek='',$faktur=''){
        $where 	 = "a.nobgcek = " . $this->escape($nobgcek) . " and a.faktur = '$faktur'";
        $f        = "a.*,b.keterangan as ketbank,s.nama as namasupplier" ;
        $join    = "left join supplier s on s.kode = a.kpddari left join bank b on b.kode = a.bank"; 
        $dbd      = $this->select("bg_list a", $f, $where, $join, "", "a.tgl desc") ;
        return $dbd;
    }

    public function seekgolaset($search){
        $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("aset_golongan", "*", $where, "", "", "keterangan ASC", '50') ;
        return array("db"=>$dbd) ;
    }
	
	public function seekcustomer($search){
        $where = "(kode LIKE '%{$search}%' OR nama LIKE '%{$search}%')" ;
        $dbd      = $this->select("customer", "*", $where, "", "", "nama ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    
	public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "BC".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }
    
    public function cetakdm($nobgcek,$faktur){
        $this->escpos->connect();
        $cpl = 80 ;
        //set content print
        //header
        // $vaprint = array();
		
		$where 	 = "a.nobgcek = " . $this->escape($nobgcek) . " and a.faktur = '$faktur'";
        $f        = "a.*,b.keterangan as ketbank,s.nama as namasupplier" ;
        $join    = "left join supplier s on s.kode = a.kpddari left join bank b on b.kode = a.bank"; 
        $dbd      = $this->select("bg_list a", $f, $where, $join, "", "a.tgl desc") ;
        if($dbr = $this->getrow($dbd)){
            $arrcab = $this->func_m->GetDataCabang($dbr['cabang']);
            
            $this->escpos->teks(str_pad($arrcab['nama'],61," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad($arrcab['alamat'],55," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],80," ",STR_PAD_RIGHT));
			
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->posisiteks("center");
            //$this->escpos->textsize(4,4);
            $this->escpos->teks("BG Keluar");
            $this->escpos->posisiteks("left");
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));

            $this->escpos->teks(str_pad("Tgl              : ".date_2d($dbr['tglcair']),80," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("No BG/CEK        : ".$dbr['nobgcek'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Bank             : ".$dbr['ketbank'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("No Rekening      : ".$dbr['norekening'],80," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("Nominal          : ".string_2s($dbr['nominal']),80," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("Kepada           : ".$dbr['namasupplier'],80," ",STR_PAD_RIGHT));
			
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("Disetujui,",40," ",STR_PAD_BOTH).str_pad("Bag. Keuangan,",40," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("(....................)",40," ",STR_PAD_BOTH).str_pad("(....................)",40," ",STR_PAD_BOTH));
		
		}
			
        // foreach($vaprint as $key => $val){
        //     $this->escpos->teks($val);
        // }
        $this->escpos->cetak(true);
    }
}
?>
