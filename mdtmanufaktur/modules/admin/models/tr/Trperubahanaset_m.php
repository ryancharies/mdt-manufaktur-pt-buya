<?php
class Trperubahanaset_m extends Bismillah_Model{
    public function saving($va){ 
        $faktur= getsession($this, "sstrperubahanaset_id", "");
		$va['faktur'] = $faktur !== "" ? $va['faktur'] : $this->func_m->getfaktur("AE",$va['tgl'],$va['cabang'],true) ;
        $va['tgl'] = date_2s($va['tgl']);
        $return = "ok";
        //cek apakah ada data perubahhan untuk tanggal ini
        $this->db->where('kode',$va['kode']);
        $this->db->where('tgl',$va['tgl']);
        $this->db->where('faktur <>',$va['faktur']);
        $db2 = $this->db->select("*")->from("aset_perubahan")
            ->get(); //print_r($this->db->last_query());
        $r2  = $db2->row_array();
        if(!empty($r2)){
            $return = "Aset sudah dilakukan perubahan pada tanggal ini, Perubahan tidak dapat dilakukan!!";
        }else{
            $grid2 = json_decode($va['grid2'],true);
            $vendors = array();
            foreach($grid2 as $key => $val) {
                $vendors[$val['vendor']] = array("vendor"=>$val['vendor'],"namavendor"=>$val['namavendor'],"hutang"=>$val['hutang']);
            }

            $dataawal = array("tglperolehan"=>"","unit"=>0,"hargaperolehan"=>0,"lama_bulan"=>0,"jenispenyusutan"=>'',"tarifpenyusutan"=>0,"residu"=>0,"nilaibuku"=>0);
            $dataakhir = array("hargaperolehan_tambahan"=>0,"hargaperolehan_akhir"=>0,"lama_bulan_tambahan"=>0,"lama_bulan_akhir"=>0,"jenispenyusutan"=>'',"tarifpenyusutan"=>0,"residu"=>0,"nilaibuku"=>0);


            //cek data aset
            $this->db->where('a.kode',$va['kode']);
            $field = "a.cabang,a.unit,a.tglperolehan,a.kode,a.lama,a.hargaperolehan,a.residu,a.tarifpenyusutan,a.jenispenyusutan,g.keterangan ketgolongan,k.keterangan ketkelompok";
            $db = $this->db->select($field)->from("aset a")
                ->join("aset_golongan g","g.kode = a.golongan","left")
                ->join("aset_kelompok k","k.kode = a.kelompok","left")
                ->get(); //print_r($this->db->last_query());
            $r  = $db->row_array();
            if(!empty($r)){
                $tglbulanlalu = date("Y-m-d",nextmonth(strtotime(date_bom($va['tgl'])),-1));
                $tglbulanlalu = date_eom($tglbulanlalu);
                $peny = $this->perhitungan_m->getpenyusutan($r['kode'],$tglbulanlalu);


                $dataawal['tglperolehan'] = $r['tglperolehan'];
                $dataawal['unit'] = $r['unit'];
                $dataawal['hargaperolehan'] = $r['hargaperolehan'];
                $dataawal['lama_bulan'] = $r['lama'] * 12;
                $dataawal['jenispenyustan'] = $r['jenispenyusutan'];
                $dataawal['tarifpenyusutan'] = $r['tarifpenyusutan'];
                $dataawal['residu'] = $r['residu'];
                $dataawal['nilaibuku'] = $dataawal['hargaperolehan'] - $peny['akhir']; //perlu mengambil dari get penyusutan

                //apabila datanya sudah ada perubahan maka mengambil data terakhir perubahan

                
                $dataakhir['hargaperolehan_tambahan'] = string_2n($va['hp_tambah']);
                $dataakhir['hargaperolehan_akhir'] = $r['hargaperolehan'] + $dataakhir['hargaperolehan_tambahan'];
                $dataakhir['lama_bulan_tambahan'] = string_2n($va['lama']);
                $dataakhir['lama_bulan_akhir'] = ($r['lama'] * 12) + $dataakhir['lama_bulan_tambahan'];
                $dataakhir['lama_bulan_sisa'] = ($r['lama'] * 12) - $peny['ke'];
                $dataakhir['lama_bulan_sisa'] = max($dataakhir['lama_bulan_sisa'],0);
                $dataakhir['lama_bulan_sisa'] += $dataakhir['lama_bulan_tambahan'];
                $dataakhir['peny_ke'] = $peny['ke'];


                $dataakhir['jenispenyusutan'] = $va['jenis'];
                $dataakhir['tarifpenyusutan'] = string_2n($va['tarifpeny']);
                $dataakhir['nilaibuku'] = $dataawal['nilaibuku'] + $dataakhir['hargaperolehan_tambahan'];
            }

            
            

            $data    = array("kode"=>$va['kode'],"kelompok"=>$va['kelaset'],"cabang"=>$r['cabang'],
                            "tgl"=>date_2s($va['tgl']),"dataawal"=>json_encode($dataawal),"dataakhir"=>json_encode($dataakhir),
                            "faktur"=>$va['faktur'],"vendors"=>json_encode($vendors),"username"=> getsession($this, "username"),"datetime"=>date_now()) ;
            $where   = "kode = " . $this->escape($va['kode']) ;
            $this->update("aset_perubahan", $data, $where, "") ;
        }

        
		
		$this->updtransaksi_m->updkartuhutangperubahanaset($va['faktur']);
		$this->updtransaksi_m->updrekperubahanaset($va['faktur']);
        return $return;
    }
	
}
?>
