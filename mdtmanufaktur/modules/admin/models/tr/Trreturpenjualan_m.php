<?php
class Trreturpenjualan_m extends Bismillah_Model{
    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "t.faktur LIKE '%{$search}%' or s.nama = '%{$search}%'" ;
        $where[] = "t.status = '1' and t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "t.faktur,t.fktpj,t.tgl,s.nama as customer,t.total,t.persppn,t.ppn,t.subtotal";
        $join     = "left join customer s on s.Kode = t.customer";
        $dbd      = $this->select("penjualan_retur_total t", $field, $where, $join, "", "t. faktur ASC", $limit) ;
        $dba      = $this->select("penjualan_retur_total t", "t.id", $where) ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function saving($faktur, $va){
        $faktur         = getsession($this, "ssreturpenjualan_faktur", "");
        $va['faktur']   = $faktur !== "" ? $faktur : $this->getfaktur() ;
        $data           = array("faktur"=>$va['faktur'],
                                "fktpj"=>$va['fktpj'],
                                "tgl"=>$va['tgl'],
                                "subtotal"=>string_2n($va['subtotal']),
                                "persppn"=>string_2n($va['persppn']),
                                "ppn"=>string_2n($va['ppn']),
                                "total"=>string_2n($va['total']),
                                "status"=>"1",
                                "gudang"=>$va['gudang'],
                                "customer"=>$va['customer'],
                                "cabang"=> getsession($this, "cabang"),
                                "username"=> getsession($this, "username"),
                                "datetime"=>date("Y-m-d H:i:s")) ;
        $where          = "faktur = " . $this->escape($faktur) ;
        $this->update("penjualan_retur_total", $data, $where, "") ;

        //insert detail retur
        $vaGrid = json_decode($va['grid2']);
        $this->delete("penjualan_retur_detail", "faktur = '{$va['faktur']}'" ) ;
        foreach($vaGrid as $key => $val){
            $cKdStockGrid = $val->stock;
            $arrstock = $this->perhitungan_m->getdatastock($cKdStockGrid) ;
            // if($dbRKD = $this->getrow($dbKD)){
            //   $cKodeStock = $dbRKD['Kode'];
            // }
            $hp = 0;//$this->gethpbypj($arrstock['kode'],$va['fktpj']);
            $vadetail = array("faktur"=>$va['faktur'],
                              "stock"=>$arrstock['kode'],
                              "qty"=>$val->qty,
                              "harga"=>$val->harga,
                              "jumlah"=>$val->jumlah,
                              "hp"=>$hp,
                              "stock_kelompok"=>$arrstock['stock_kelompok'],
                              "jenis_kelompok"=>$arrstock['jenis_kelompok']);
            $this->insert("penjualan_retur_detail",$vadetail);
        }
        
        $this->updtransaksi_m->updkartupiutangreturpenjualan($va['faktur']);
        $this->updtransaksi_m->updkartustockreturpenjualan($va['faktur']);
        $this->updtransaksi_m->updrekreturpenjualan($va['faktur']);
    }

    public function getdatatotal($faktur){
        $data  = array() ;
        $field = "t.faktur,t.tgl,t.gudang,t.customer,g.keterangan as ketgudang,t.total,t.persppn,t.ppn,
                s.nama as namacustomer,t.subtotal,t.fktpj";
        $where = "t.faktur = '$faktur'";
        $join  = "left join gudang g on g.kode = t.gudang left join customer s on s.kode = t.customer";
        $dbd   = $this->select("penjualan_retur_total t", $field, $where, $join) ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }

    public function getdatadetail($faktur){
        $field = "d.stock,s.keterangan as namastock,d.harga,d.qty,s.satuan,d.jumlah";
        $where = "d.faktur = '$faktur'";
        $join  = "left join stock s on s.kode = d.stock";
        $dbd   = $this->select("penjualan_retur_detail d", $field, $where, $join) ;
        return $dbd ;
    }

    public function deleting($faktur){
        $return = "ok";
        $dbd = $this->select("piutang_kartu", "id", "faktur <> '$faktur' and fkt = '$faktur'");
        if ($dbr = $this->getrow($dbd)) {
            $return = "Data tidak bisa dihapus karena sudah masuk ke proses pelunasan!!";
        } else {
            $this->edit("penjualan_retur_total",array("status"=>2),"faktur = " . $this->escape($faktur));
            $this->delete("stock_kartu", "faktur = " . $this->escape($faktur));
            $this->delete("piutang_kartu", "faktur = " . $this->escape($faktur));
            $this->delete("keuangan_bukubesar", "faktur = " . $this->escape($faktur));
        }
        return $return;
    }

    public function seekgudang($search){
        $where = "kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%'" ;
        $dbd      = $this->select("gudang", "*", $where, "", "", "keterangan ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    public function seekcustomer($search){
        $where = "kode LIKE '{$search}%' OR nama LIKE '%{$search}%'" ;
        $dbd      = $this->select("customer", "*", $where, "", "", "nama ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getdatastock2($cKodeStock){
        $cWhere = "kode = '$cKodeStock' or barcode = '$cKodeStock'";
        $dbData = $this->select("stock","Kode,Keterangan,Satuan",$cWhere);
        return $dbData ;
    }

    public function getdatastock($va){
        $kode = $va['cKodeStock'];
        if(!empty($va['fktpj'])){
            $where 	 = array() ;
            $where[]	= "(s.tampil = 'P' or s.tampil = 'S')" ;
            $where[]	= "d.faktur = '{$va['fktpj']}' and d.stock = '$kode'" ;
            $where 	 = implode(" AND ", $where) ;
            $join    = "left join stock s on s.kode = d.stock";
            $group = "s.kode";
            $dbd      = $this->select("penjualan_detail d", "s.kode,s.keterangan,s.satuan,d.harga", $where, $join, $group, "s.kode ASC") ;
            
        }else{
            $where 	 = array() ;
            $where[]	= "(tampil = 'P' or tampil = 'S') and kode = '$kode'" ;
            $where 	 = implode(" AND ", $where) ;
            $dbd      = $this->select("stock", "kode,keterangan,satuan,0 as harga", $where, "", "", "kode ASC") ;
        }
      return $dbd ;
    }

    public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "RJ".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }

    public function loadgrid3($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        
        if(!empty($va['fktpj'])){
            $where 	 = array() ;
            if($search !== "") $where[]	= "(s.kode LIKE '{$search}%' OR s.keterangan LIKE '%{$search}%')" ;
            $where[]	= "(s.tampil = 'P' or s.tampil = 'S')" ;
            $where[]	= "d.faktur = '{$va['fktpj']}'" ;
            $where 	 = implode(" AND ", $where) ;
            $join    = "left join stock s on s.kode = d.stock";
            $group = "s.kode";
            $dbd      = $this->select("penjualan_detail d", "s.kode,s.keterangan,s.satuan", $where, $join, $group, "s.kode ASC", $limit) ;
            $dba      = $this->select("penjualan_detail d", "d.id", $where,$join,$group) ;
        }else{
            $where 	 = array() ;
            if($search !== "") $where[]	= "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%')" ;
            $where[]	= "tampil = 'P' or tampil = 'S'" ;
            $where 	 = implode(" AND ", $where) ;
            $dbd      = $this->select("stock", "kode,keterangan,satuan", $where, "", "", "kode ASC", $limit) ;
            $dba      = $this->select("stock", "id", $where) ;
        }

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function getdata($va){
        $kode = $va['kode'];
        $data = array() ;
        
        if(!empty($va['fktpj'])){
            $where 	 = array() ;
            $where[]	= "(s.tampil = 'P' or s.tampil = 'S')" ;
            $where[]	= "d.faktur = '{$va['fktpj']}' and d.stock = '$kode'" ;
            $where 	 = implode(" AND ", $where) ;
            $join    = "left join stock s on s.kode = d.stock";
            $group = "s.kode";
            $dbd      = $this->select("penjualan_detail d", "s.kode,s.keterangan,s.satuan,d.harga", $where, $join, $group, "s.kode ASC") ;
            
        }else{
            $where 	 = array() ;
            $where[]	= "(tampil = 'P' or tampil = 'S') and kode = '$kode'" ;
            $where 	 = implode(" AND ", $where) ;
            $dbd      = $this->select("stock", "kode,keterangan,satuan,0 as harga", $where, "", "", "kode ASC") ;
        }
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
		return $data ;
   }

    public function loadgrid4($va){
        $limit    = 50;//$va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "(p.faktur LIKE '{$search}%' OR s.nama LIKE '%{$search}%')" ;
        $where[]	= "p.status = '1' and p.faktur not like 'KV%' and b.faktur is null" ;
        if(isset($va['customer'])) $where[]	= "p.customer = '{$va['customer']}'" ;
        $where 	 = implode(" AND ", $where) ;
        $field = "p.faktur,p.tgl,s.nama as customer,p.ppn,p.persppn";
        $join ="left join customer s on s.kode = p.customer left join penjualan_retur_total b on b.fktpj = p.faktur";
        $dbd      = $this->select("penjualan_total p", $field, $where, $join, "", "p.tgl desc,p.faktur desc", $limit) ;
        // $dba      = $this->select("penjualan_total p", "p.id", $where, $join) ;

        return array("db"=>$dbd, "rows"=>0 ) ;
    }

    public function getdatado($fktdo){
        $data = array() ;
        $where	= "p.faktur = '$fktdo'" ;
        $field = "p.faktur,p.tgl,p.customer,s.nama as namacustomer,p.persppn";
        $join ="left join customer s on s.kode = p.customer";
        $dbd      = $this->select("penjualan_total p", $field, $where, $join, "", "p.faktur DESC") ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }
}
?>
