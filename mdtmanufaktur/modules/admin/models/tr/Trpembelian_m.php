<?php
class Trpembelian_m extends Bismillah_Model
{
   
    public function getdatadetailpo($fktpo)
    {
        $field = "k.stock,s.keterangan as namastock,s.satuan,
                (select d.harga from po_detail d where d.faktur = '$fktpo' and d.stock = k.stock limit 1) as harga,
                ifnull(sum(k.debet-k.kredit),0) qty";
        $where = "k.fktpo = '$fktpo'";
        $join = "left join stock s on s.kode = d.stock left join po_kartu k on k.fktpo = d.faktur and k.stock = d.stock";
        $dbd = $this->select("po_detail d", $field, $where, $join, "k.stock");
        return $dbd;
    }

    public function deleting($faktur)
    {
        $return = "ok";
        $dbd = $this->select("hutang_kartu","id","faktur <> '$faktur' and fkt = '$faktur'");
        if ($dbr = $this->getrow($dbd)) {
            $return = "Data tidak bisa dihapus karena sudah masuk ke proses pelunasan!!";
        }else{
            $this->delete("keuangan_bukubesar", "faktur = " . $this->escape($faktur));
            $this->delete("stock_kartu", "faktur = " . $this->escape($faktur));
            $this->delete("po_kartu", "faktur = " . $this->escape($faktur));
            $this->delete("hutang_kartu", "faktur = " . $this->escape($faktur));
            $this->edit("pembelian_total", array("status" => 2, "fktpo" => ""), "faktur = " . $this->escape($faktur));
        }
        return $return;
    }

    public function cekpelunasan($faktur)
    {
        $return = false;
        $dbd = $this->select("hutang_kartu","id","faktur <> '$faktur' and fkt = '$faktur'");
        if ($dbr = $this->getrow($dbd)) {
            $return = true;
        }
        return $return;
    }

    public function getfaktur($cabang,$tgl,$l = true)
    {
        $tgl = date_2s($tgl);
        $key = "PB" . $cabang . date("ymd",strtotime($tgl));
        $n = $this->getincrement($key, $l, 5);
        $faktur = $key . $n;
        return $faktur;
    }

    public function loadgrid4($va)
    {
        $limit = $va['offset'] . "," . $va['limit'];
        $search = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "";
        $search = $this->escape_like_str($search);
        $where = array();
        $arrdata = array();
        if ($search !== "") {
            $where[] = "(p.faktur LIKE '{$search}%' OR s.nama LIKE '%{$search}%')";
        }

        $where[] = "p.status = '1' "; //and b.faktur is null" ;
        $where = implode(" AND ", $where);
        $field = "p.faktur,p.tgl,s.nama as supplier,p.gudang,g.keterangan as ketgudang,k.fktpo,
                    ifnull(sum(k.debet-k.kredit),0) as saldo";
        $join = "left join po_total p on p.faktur = k.fktpo left join supplier s on s.kode = p.supplier left join gudang g on g.kode = p.gudang";
        $dbd = $this->select("po_kartu k", $field, $where, $join, "concat(k.fktpo,k.stock) having saldo > 0", "p.faktur asc", $limit);
        while ($dbr = $this->getrow($dbd)) {
            $arrdata[$dbr['faktur']] = $dbr;
        }

        return array("db" => $arrdata);
    }

    public function getdata($kode)
    {
        $data = array();
        if ($d = $this->getval("*", "kode = " . $this->escape($kode), "stock")) {
            $data = $d;
        }
        return $data;
    }
    public function getdatapo($fktpo)
    {
        $data = array();
        $where = "p.faktur = '$fktpo'";
        $field = "p.faktur,p.tgl,p.supplier,s.nama as namasupplier,p.gudang,g.keterangan as ketgudang";
        $join = "left join supplier s on s.kode = p.supplier left join gudang g on g.kode = p.gudang";
        $dbd = $this->select("po_total p", $field, $where, $join, "", "p.faktur DESC");
        if ($dbr = $this->getrow($dbd)) {
            $data = $dbr;
        }
        return $data;
    }

    public function getdatasupplier($supplier)
    {
        $where = "kode = '$supplier'";
        $arrresp = array("terminhari" => 0);
        $dbd = $this->select("supplier", "*", $where, "", "", "nama ASC", '50');
        if ($dbr = $this->getrow($dbd)) {
            $arrresp['terminhari'] = $dbr['terminhari'];
        }
        return $arrresp;
    }
}
