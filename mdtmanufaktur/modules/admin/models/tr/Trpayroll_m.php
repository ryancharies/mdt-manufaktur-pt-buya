<?php

class Trpayroll_m extends Bismillah_Model {

    public function loadgrid( $va ) {
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $va['tglawal'] = date_2s($va['tglawal']);
        $where = "k.tgl <= '{$va['tglakhir']}' and (k.tglkeluar > '{$va['tglakhir']}' or k.tglkeluar = '0000-00-00')";
       
        $field = "k.kode,k.nama,k.id";
        
        $join = "";
        $dbd = $this->select("karyawan k", $field, $where, $join, "k.kode", "k.kode ASC");
        $dba = $this->select("karyawan k", $field, $where,$join, "k.kode", "k.kode ASC");
        return array("db" => $dbd, "rows" =>$this->rows($dba));
    }

    public function saving( $va ) {

       // print_r($va);
        $error = '';
        //cek valid
        $va['grid2'] = json_decode($va['grid2']);
        if ( trim( $va['periode'] ) == '' )$error .= 'Periode tidak valid !! \\n';
        if ( trim( $va['tglawal'] ) == '' or trim( $va['tglawal'] ) == '00-00-0000')$error .= 'Tgl Awal tidak valid !! \\n';
        if ( trim( $va['tglakhir'] ) == '' or trim( $va['tglakhir'] ) == '00-00-0000')$error .= 'Tgl Akhir tidak valid !! \\n';
        if ( trim( $va['tgl'] ) == '' or trim( $va['tgl'] ) == '00-00-0000')$error .= 'Tgl tidak valid !! \\n';
        if(empty($va['grid2']))$error .= 'Pembayaran tidak valid !! \\n';

        if ( $error == '' ) {
            //$this->edit("payroll_posting",array("status"=>'2'),"periode = '{$va['periode']}'");

            $va['tgl'] = date_2s($va['tgl']);
            $va['tglakhir'] = date_2s($va['tglakhir']);
            
            $datakomp = array();
            //load data komponen payroll

            $kodepinjaman = $this->getconfig("kogapypinjkaryawan");

            $where = "status = '1' and kode <> '$kodepinjaman'";
            $dbd2 = $this->trpayroll_m->select("payroll_komponen","kode,diluarjadwal,keterangan,periode,perhitungan,potongan",$where);
            while($dbr2 = $this->trpayroll_m->getrow($dbd2)){
                $datakomp[$dbr2['kode']] = $dbr2;
            }

            //loop karyawan
            $total = 0;
            $faktur = $this->getfaktur();
            $where = "k.tgl <= '{$va['tglakhir']}' and (k.tglkeluar > '{$va['tglakhir']}' or k.tglkeluar = '0000-00-00')";
            $field = "k.kode,k.nama";
            $join = "";
            $dbd = $this->select("karyawan k", $field, $where, $join, "k.kode", "k.kode ASC");
            while($dbr = $this->getrow($dbd)){
                //jab bag
                $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$va['tglakhir']);
                $bagian = $this->perhitunganhrd_m->getbagian($dbr['kode'],$va['tglakhir']);

                
                $kodepinjaman = $this->getconfig("kogapypinjkaryawan");
                
                //loop komponen gaji
                $totkaryawan = 0;
                // $where2 = "status = '1' and perhitungan <> 'K'";
                // $field2 = "kode,keterangan,periode,perhitungan,potongan,diluarjadwal";
                // $dbd2 = $this->select("payroll_komponen",$field2,$where2);
                // while($dbr2 = $this->getrow($dbd2)){
                foreach($datakomp as $key => $dbr2){
                    if($dbr2['perhitungan'] == "A"){//proses asuransi                        
                        $asuransi = $this->perhitunganhrd_m->getdetailasuransipayroll($dbr['kode'],$dbr2['kode'],$va['tglakhir']);
                        //if(!empty($asuransi)){
                            foreach($asuransi as $key => $val){
                                $arrasuransi = array("faktur"=>$faktur,"nip"=>$dbr['kode'],"nopendaftaran"=>$val['nopendaftaran'],
                                            "tarifkaryawan"=>$val['tarifkaryawan']);
                                $this->insert("payroll_posting_asuransi",$arrasuransi);
                            }
                        //}
                    }

                    $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawan($dbr['kode'],$dbr2['kode'],$va['periode']);
                    $arrdata = array("faktur"=>$faktur,"nip"=>$dbr['kode'],"payroll"=>$dbr2['kode'],"periode"=>$dbr2['periode'],
                            "perhitungan"=>$dbr2['perhitungan'],
                            "potongan"=>$dbr2['potongan'],"diluarjadwal"=>$dbr2['diluarjadwal'],
                            "nominal"=>$payroll['nominal'],"jmlperiode"=>$payroll['jmlperiode'],"jmlskala"=>$payroll['jmlskala'],
                            "total"=>$payroll['total']);
                    $this->insert("payroll_posting_komponen",$arrdata);

                    if($dbr2['potongan']){
                        $totkaryawan -= $payroll['total'];
                    }else{
                        $totkaryawan += $payroll['total'];
                    }
                }

                //kasbon
                if($kodepinjaman <> ""){
                    $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjaman($dbr['kode'],$va['periode']);
                    $arrdata = array("faktur"=>$faktur,"nip"=>$dbr['kode'],"payroll"=>$kodepinjaman,"periode"=>"B",
                            "potongan"=>"1","diluarjadwal"=>"0","nominal"=>$kasbon['total'],"perhitungan"=>"K",
                            "nominal"=>$kasbon['total'],"jmlperiode"=>"1","jmlskala"=>"1",
                            "total"=>$kasbon['total']);
                    $this->insert("payroll_posting_komponen",$arrdata);

                    //insert detail angsuran
                    $detailpinjaman = $kasbon['detail'];
                    foreach($detailpinjaman as $key => $val){
                        $arrd = array("faktur"=>$faktur,"nip"=>$dbr['kode'],"nopinjaman"=>$val['nopinjaman'],"pokok"=>$val['pokok']);
                        $this->insert("payroll_posting_pinjaman_karyawan",$arrd);

                        //update angsuran
                        $whereangs = "periode = '{$va['periode']}' and nopinjaman = '{$val['nopinjaman']}' and status = '0'";
                        $arrangs = array("faktur"=>$faktur,"status"=>"1");
                        $this->edit("payroll_pinjaman_karyawan_angsuran",$arrangs,$whereangs);
                    }

                    $totkaryawan -= $kasbon['total'];
                }
                
                $total += $totkaryawan;

            }

            //insert pembayaran
            foreach($va['grid2'] as $key => $val){
                $arrd = array("faktur"=>$faktur,"rekening"=>$val->kode,"nominal"=>string_2n($val->nominal));
                $this->insert("payroll_posting_pembayaran",$arrd);
            }

            $data    = array( 'faktur'=>$faktur, 'tgl'=>$va['tgl'], 'periode'=>$va['periode'], 'total'=>$total,
                    'cabang'=> getsession($this, "cabang"),'username'=> getsession( $this, 'username' ), 'datetime'=>date_now() ) ;
            $where   = "faktur = '$faktur'" ;
            $this->update('payroll_posting', $data, $where) ;

            $this->updtransaksi_m->updrekpayroll($faktur);

        }
        //simpan detail
        return $error;
    }

    public function seekperiode($search){
        $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("sys_periode_payroll", "*", $where, "", "", "kode desc", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getperiode($va){
        $where = "kode = '{$va['periode']}'" ;
        $dbd      = $this->select("sys_periode_payroll", "*", $where, "", "", "kode desc", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "UP".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }

    public function seekrekening($search){
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%') and jenis = 'D'" ;
        $dbd      = $this->select("keuangan_rekening", "*", $where, "", "", "kode ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getpembayaran($faktur){
        $where = "p.faktur = '$faktur'" ;
        $field = "p.rekening as kode,p.nominal,concat(p.rekening,'-',r.keterangan) as ketketerangan";
        $join = "left join keuangan_rekening r on r.kode = p.rekening";
        $dbd      = $this->select("payroll_posting_pembayaran p", $field, $where, $join, "") ;
        return $dbd ;
    }
}
?>
