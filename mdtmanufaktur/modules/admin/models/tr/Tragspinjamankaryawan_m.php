<?php
class Tragspinjamankaryawan_m extends Bismillah_Model{
    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "t.faktur LIKE '%{$search}%'" ;
        $where[] = "t.status = '1' and t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "t.faktur,t.tgl,t.nopinjaman,p.nip,k.nama,t.pokok,t.keterangan";
        $join     = "left join pinjaman_karyawan p on p.nopinjaman = t.nopinjaman left join karyawan k on k.kode = p.nip";
        $dbd      = $this->select("pinjaman_karyawan_angsuran_total t", $field, $where, $join, "", "t. faktur ASC", $limit) ;
        $dba      = $this->select("pinjaman_karyawan_angsuran_total t", "t.id", $where, $join, "", "t. faktur ASC") ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function saving($faktur, $va){
        $faktur         = getsession($this, "ssagspinjamankaryawan_faktur", "");
        $va['faktur']   = $faktur !== "" ? $faktur : $this->getfaktur() ;
        $data           = array("faktur"=>$va['faktur'],
                                "tgl"=>date_2s($va['tgl']),
                                "nopinjaman"=>$va['nopinjaman'],
                                "keterangan"=>$va['keterangan'],
                                "pokok"=>string_2n($va['pokok']),
                                "status"=>"1",
                                "cabang"=> getsession($this, "cabang"),
                                "username"=> getsession($this, "username"),
                                "datetime"=>date("Y-m-d H:i:s")) ;
        $where          = "faktur = " . $this->escape($faktur) ;
        $this->update("pinjaman_karyawan_angsuran_total", $data, $where, "") ;

        //insert detail
        $vaGrid = json_decode($va['grid2']);
        $this->delete("pinjaman_karyawan_angsuran_detail", "faktur = '{$va['faktur']}'" ) ;
        foreach($vaGrid as $key => $val){

            $vadetail = array("faktur"=>$va['faktur'],
                              "rekening"=>$val->kode,
                              "jumlah"=>$val->nominal);
            $this->insert("pinjaman_karyawan_angsuran_detail",$vadetail);
        }
        $this->updtransaksi_m->updkartuangsuranpinjamankaryawan($va['faktur']);
        $this->updtransaksi_m->updrekangsuranpinjamankaryawan($va['faktur']);
    }

    public function getdatatotal($faktur){
        $data  = array() ;
        $field = "a.faktur,a.tgl,a.keterangan,a.pokok,a.nopinjaman,p.nip,p.tgl as tglrealisasi,p.tglawalags,p.plafond,p.lama,k.nama,
                ifnull(sum(b.debet-b.kredit),0) as bakidebet";
        $where = "a.faktur = '$faktur'";
        $join  = "left join pinjaman_karyawan p on p.nopinjaman = a.nopinjaman left join karyawan k on k.kode = p.nip
                left join pinjaman_karyawan_kartu b on b.nopinjaman = a.nopinjaman and b.tgl <= a.tgl and b.faktur <> a.faktur";
        $dbd   = $this->select("pinjaman_karyawan_angsuran_total a", $field, $where, $join,"a.faktur") ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }

    public function getdatadetail($faktur){
        $field = "d.faktur,r.kode,concat(d.rekening,'-',r.keterangan) as ketrekening,d.jumlah as nominal,d.rekening,
                r.keterangan as ketrek";
        $where = "d.faktur = '$faktur'";
        $join  = "left join keuangan_rekening r on r.kode = d.rekening";
        $dbd   = $this->select("pinjaman_karyawan_angsuran_detail d", $field, $where, $join) ;
        return $dbd ;
    }

    public function deleting($faktur){
        $this->edit("pinjaman_karyawan_angsuran_total",array("status"=>2),"faktur = " . $this->escape($faktur));
        $this->delete("keuangan_bukubesar","faktur = " . $this->escape($faktur));
        $this->delete("pinjaman_karyawan_kartu","faktur = " . $this->escape($faktur));

        $nopinjaman = $this->getval("nopinjaman", "faktur = '$faktur'", "pinjaman_karyawan_angsuran_total","","","");
        $this->updtransaksi_m->updstatuslunaspinjamankaryawan($nopinjaman);
    }

    public function seekrekening($search){
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%') and jenis = 'D'" ;
        $dbd      = $this->select("keuangan_rekening", "*", $where, "", "", "kode ASC", '100') ;
        return array("db"=>$dbd) ;
    }
    

    public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "AG".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }

    public function loadgrid3($va)
    {
        $limit = $va['offset'] . "," . $va['limit'];
        $search = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "";
        $search = $this->escape_like_str($search);
        $where = array();
        if ($search !== "") {
            $where[] = "(k.nama LIKE '{$search}%' OR p.nopinjaman LIKE '%{$search}%')";
        }
        $va['tgl'] = date_2s($va['tgl']);
        $where[] = "p.tgl <= '{$va['tgl']}' and (p.tgllunas >= '{$va['tgl']}' or p.tgllunas = '0000-00-00' or p.fktlunas = '{$va['faktur']}')";
        $where = implode(" AND ", $where);
        $field = "p.nopinjaman,p.nip,k.nama,p.plafond,ifnull(sum(b.debet - b.kredit),0) as bakidebet";//,ifnull(sum(b.debet - b.kredit),0) as bakidebet
        
        $join = "left join karyawan k on k.kode = p.nip
                left join pinjaman_karyawan_kartu b on b.nopinjaman = p.nopinjaman and b.faktur <> '{$va['faktur']}'";//left join pinjaman_karyawan_kartu b on b.nopinjaman = p.nopinjaman and b.faktur <> '{$va['faktur']}'
        $dbd = $this->select("pinjaman_karyawan p", $field, $where, $join, "p.nopinjaman having bakidebet <> 0", "p.nip ASC", $limit);// having bakidebet <> 0
        $dba = $this->select("pinjaman_karyawan p", $field, $where,$join, "p.nopinjaman having bakidebet <> 0", "p.nip ASC");
        return array("db" => $dbd, "rows" =>$this->rows($dba));//$this->rows($dba)
    }
    
    public function getdatanopinjaman($va){
        
        $va['tgl'] = date_2s($va['tgl']);
        $where[] = "p.tgl <= '{$va['tgl']}' and p.nopinjaman = '{$va['nopinjaman']}'";
        $where = implode(" AND ", $where);
        $field = "p.nopinjaman,p.nip,k.nama,p.plafond,p.lama,p.tgl as tglrealisasi,p.tglawalags,
                ifnull(sum(b.debet - b.kredit),0) as bakidebet";//,ifnull(sum(b.debet - b.kredit),0) as bakidebet
        
        $join = "left join karyawan k on k.kode = p.nip
                left join pinjaman_karyawan_kartu b on b.nopinjaman = p.nopinjaman ";//left join pinjaman_karyawan_kartu b on b.nopinjaman = p.nopinjaman and b.faktur <> '{$va['faktur']}'
        $dbd = $this->select("pinjaman_karyawan p", $field, $where, $join, "p.nopinjaman having bakidebet <> 0", "p.nip ASC");// having bakidebet <> 0
        
        return $dbd;
    }

}
?>
