<?php
class Trjwgolabsensi_m extends Bismillah_Model{
   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(c.kode LIKE '{$search}%' OR c.keterangan LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $field = "c.kode,c.keterangan";
      $join = "";
      $dbd      = $this->select("absensi_golongan c", $field, $where, $join, "", "c.kode ASC", $limit) ;
      $dba      = $this->select("absensi_golongan c", "c.id", $where) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function loadgrid2($va){
      $where 	 = array() ; 
      $where[]	= "c.golongan LIKE '{$va['kode']}'" ;
      $where 	 = implode(" AND ", $where) ;
      $field = "c.golongan,c.tgl,c.mulaihari,j.keterangan as jadwal";
      $join = "left join absensi_jadwal j on j.kode = c.jadwal";
      $dbd      = $this->select("absensi_golongan_jadwal c", $field, $where, $join, "", "c.tgl desc") ;
      $dba      = $this->select("absensi_golongan_jadwal c", "c.id", $where,$join) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function saving($va){
      $error = "";

      $va['tgl'] = date_2d($va['tgl']);
      if(!isset($va['jadwal']))$error .= "Jadwal tidak boleh kosong.. \\n";
      if($va['kode'] == "")$error .= "Kode belum diisi.. \\n";
      if($va['tgl'] == "")$error .= "Tgl belum diisi.. \\n";
      
      //cek maxs nohari
      $harimulai = false;
      $dbd = $this->select("absensi_jadwal_detail","nohari","kode = '{$va['jadwal']}' and nohari = '{$va['mulaihari']}'");
      if($dbr = $this->getrow($dbd)){
         $harimulai = true;
      }
      if(!$harimulai)$error .= "Mulai hari tidak valid.. \\n";

      if($error == ""){
         $data    = array("golongan"=>$va['kode'], "jadwal"=>$va['jadwal'],"mulaihari"=>$va['mulaihari'],"tgl"=>date_2s($va['tgl']),
                  "username"=> getsession($this, "username"),"datetime"=>date_now()) ;
         $where   = "golongan = '{$va['kode']}' and tgl = '{$va['tgl']}'";
         $this->update("absensi_golongan_jadwal", $data, $where) ;
         $error = "ok";
      }      
      return $error;
   }

   public function getdata($kode){
      $data = array() ;
		if($d = $this->getval("*", "kode = " . $this->escape($kode), "absensi_golongan")){
         $data = $d;
		}
		return $data ;
   }

   public function deleting($va){
      $return = "ok";
      $va['tgl'] = date_2s($va['tgl']);
      $this->delete("absensi_golongan_jadwal", "golongan = '{$va['golongan']}' and tgl = '{$va['tgl']}'") ;
      
      return $return;
      
   }

   public function seekjadwal($search){
      $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
      $dbd      = $this->select("absensi_jadwal", "*", $where, "", "", "keterangan ASC", '50') ;
      return array("db"=>$dbd) ;
   }

}
?>
