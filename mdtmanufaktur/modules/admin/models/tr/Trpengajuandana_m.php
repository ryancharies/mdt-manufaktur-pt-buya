<?php
class Trpengajuandana_m extends Bismillah_Model{
    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "t.faktur LIKE '%{$search}%'" ;
        $where[] = "t.status = '1' and t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "t.faktur,t.tgl,t.keterangan,t.jumlah,t.yangmengajukan,t.jenis";
        $join     = "";
        $dbd      = $this->select("keuangan_pengajuan t", $field, $where, $join, "", "t.tgl,t. faktur ASC", $limit) ;
        $dba      = $this->select("keuangan_pengajuan t", "t.id", $where) ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function saving($faktur, $va){
        
        $jumlah = string_2n($va['jumlah']);
        $error = "";

        if($jumlah <= 0)$error .= "Jumlah yang diajukan tidak valid !!\\n";
        if(empty($va['yangmengajukan']))$error .= "Yang mengajukan tidak valid !!\\n";
        if(empty($va['keterangan']))$error .= "Keterangan tidak valid !!\\n";
        if(empty($va['faktur']))$error .= "Faktur tidak valid !!\\n";

        if($error == ""){
            $va['tgl'] = date_2s($va['tgl']);
            $faktur         = getsession($this, "sspengajuandana_faktur", "");
            $va['faktur']   = $faktur !== "" ? $faktur : $this->getfaktur() ;
            $data           = array("faktur"=>$va['faktur'],
                                    "tgl"=>$va['tgl'],
                                    "status"=>"1",
                                    "keterangan"=>$va['keterangan'],
                                    "jenis"=>$va['jenis'],
                                    "yangmengajukan"=>$va['yangmengajukan'],
                                    "jumlah"=>string_2n($va['jumlah']),
                                    "cabang"=> getsession($this, "cabang"),
                                    "username"=> getsession($this, "username"),
                                    "datetime"=>date("Y-m-d H:i:s")) ;
            $where          = "faktur = " . $this->escape($faktur) ;
            $this->update("keuangan_pengajuan", $data, $where, "") ;

            //insert detail
            $vaGrid = json_decode($va['grid2']);
            $this->delete("keuangan_pengajuan_otorisasi", "faktur = '{$va['faktur']}'" ) ;
            foreach($vaGrid as $key => $val){
                $vadetail = array("faktur"=>$va['faktur'],
                                "username"=>$val->username,
                                "jabatan"=>$val->jabatan,
                                "tahap"=>$val->tahap
                                );
                $this->insert("keuangan_pengajuan_otorisasi",$vadetail);
            }

            $error = "ok";
        }
        return $error;
    }

    public function getdatatotal($faktur){
        $data  = array() ;
        $field = "t.faktur,t.tgl,t.keterangan,t.jumlah,t.yangmengajukan,t.jenis,t.cabang";
        $where = "t.faktur = '$faktur'";
        $join  = "";
        $dbd   = $this->select("keuangan_pengajuan t", $field, $where, $join) ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }

    public function getdatadetail($faktur){
        $field = "d.faktur,d.username,u.fullname as namakaryawan,d.jabatan,d.tahap";
        $where = "d.faktur = '$faktur'";
        $join  = "left join sys_username u on u.username = d.username";
        $dbd   = $this->select("keuangan_pengajuan_otorisasi d", $field, $where, $join,"","d.tahap asc") ;
        return $dbd ;
    }

    public function deleting($faktur){
        $error = "";
        $dbd = $this->select("keuangan_pengajuan","faktur","faktur = '$faktur' and tglotorisasi <> '0000-00-00'");
        if($dbr = $this->getrow($dbd)){
            $error .= "Data tidak bisa dihapus karena sudah proses otorisasi";
        }else{
            $dbd = $this->select("keuangan_pengajuan_otorisasi","faktur","faktur = '$faktur' and status <> '0'");
            if($dbr = $this->getrow($dbd)){
                $error .= "Data tidak bisa dihapus karena sedang tahap otorisasi";
            }
        }
        
        if($error == ""){
            $this->edit("keuangan_pengajuan",array("status"=>2),"faktur = " . $this->escape($faktur));
        }
        
        return $error;
    }

    public function seekotorisasi($search){
        $where = "(username LIKE '{$search}%' OR fullname LIKE '%{$search}%')" ;
        $dbd      = $this->select("sys_username", "*", $where, "", "", "fullname ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "RF".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }

    public function cekprosesotorisasi($fktpengajuan){
        $proses = false;
        $where = "faktur = '$fktpengajuan' and tahap <> 0 and status = '1'";
        $dbd = $this->select("keuangan_pengajuan_otorisasi","*",$where);
        while($dbr = $this->getrow($dbd)){
            $proses = true;
            break;
        }
        return $proses;
    }
}
?>
