<?php
class Trabsensipostingfinger_m extends Bismillah_Model{
    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "b.nopendaftaran LIKE '%{$search}%'" ;
        $where[] = "f.tgl = '{$va['tgl']}'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "f.nip,k.nama,f.id,f.tgl,f.datetimeabsensi,f.mesin,m.cabang,m.keterangan as ketmesin,
                    c.keterangan as ketcabang";
        $join     = "left join karyawan k on k.kode = f.nip left join absensi_mesin m on m.kode = f.mesin
                    left join cabang c on c.kode = m.cabang";
        $dbd      = $this->select("absensi_karyawan_tmp f", $field, $where, $join, "", "f.id ASC") ;
        

        return array("db"=>$dbd) ;
    }

    public function posting($va){
        $error = "";
        
        if($error == ""){
            $grid = json_decode($va['grid']);
            foreach($grid as $key => $val){
                if($val->jenis <> "Tidak terdeteksi"){
                    

                    $tglfinger = date_2s($val->tgl);
                    $tgl = date("Y-m-d",strtotime($val->absensi));
                    $mode = "";
                    $toleransidtk = ($val->toleransi*60);
                    if($val->jenis == "Masuk")$mode = "1";
                    if($val->jenis == "Pulang")$mode = "2";

                    $selisihabs = "00:00:00";
                    $timeselisih = $val->selisih;
                    if($timeselisih > 0){
                        $selisihjam = floor(devide($timeselisih,3600));
                        $selisihmenit = floor(devide($timeselisih - (3600*$selisihjam),60));
                        $selisihdetik = $timeselisih - (3600*$selisihjam) - ($selisihmenit * 60);

                        $selisihabs = str_pad($selisihjam,2,"0",STR_PAD_LEFT).":".str_pad($selisihmenit,2,"0",STR_PAD_LEFT).":".str_pad($selisihdetik,2,"0",STR_PAD_LEFT);
                    }

                    //sebelum di entry di edit dulu dta sebelumnya
                    $where =  "tgl = '$tglfinger' and mode = '$mode' and nip = '{$val->nip}'" ;
                    $this->edit("absensi_karyawan",array("status"=>'2'),$where) ;

                    //entry data posting
                    $data    = array("nip"=>$val->nip,"tgl"=>$tgl,"tglfinger"=>$tglfinger,"mesin"=>$val->mesin,"mode"=>$mode,
                    "jadwalabsensi"=>$val->jadwal,"absensi"=>$val->absensi,"kodeabsensi"=>$this->getconfig("kodeabsmasuk"),
                    "keterangan"=>"Absensi ".$val->jenis." [finger]","cabang"=>$val->cabang,
                    "metode"=>"Finger auto","status"=>"1","selisihabsensi"=>$selisihabs,"selisihabsensidtk"=>$val->selisih,
                    "toleransidtk"=>$toleransidtk,"username"=> getsession($this, "username"),"datetime"=>date_now()) ;
                    $this->insert("absensi_karyawan", $data) ;
                }
            }
        
            $error = "ok";
        }

        return $error;
        
    }


    public function seekasuransi($search){
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("asuransi", "*", $where, "", "", "kode ASC", '100') ;
        return array("db"=>$dbd) ;
    }
    

    

}
?>
