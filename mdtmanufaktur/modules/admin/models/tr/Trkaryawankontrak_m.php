<?php
class Trkaryawankontrak_m extends Bismillah_Model{
   public function loadgrid($va){
      $va['tglawal'] = date_2s($va['tglawal']);
      $va['tglakhir'] = date_2s($va['tglakhir']);
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(k.nopk LIKE '{$search}%' OR k.noperjanjian LIKE '{$search}%' OR c.kode LIKE '{$search}%' OR c.nama LIKE '%{$search}%')" ;
      $where [] = "k.tgl >= '{$va['tglawal']}' and k.tgl <= '{$va['tglakhir']}'";
      $where [] = "k.status = '1'";
      $where 	 = implode(" AND ", $where) ;
      $field = "c.kode,c.nama,c.telepon,c.tgl,c.alamat,c.ktp,k.noperjanjian,k.nomor,k.tgl,k.lama,k.periode,k.tglawal,k.tglakhir,
            b.keterangan as bagian, j.keterangan as jabatan";
      $join = "left join karyawan c on c.kode = k.karyawan left join bagian b on b.kode = k.bagian left join jabatan j on j.kode = k.jabatan";
      $dbd      = $this->select("perjanjian_kerja k", $field, $where, $join, "", "k.noperjanjian ASC", $limit) ;
      $dba      = $this->select("perjanjian_kerja k", "k.id", $where,$join, "", "k.noperjanjian ASC") ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function saving($va){
      $noperjanjian         = getsession($this, "sskaryawankontrak_noperjanjian", "");
      $va['noperjanjian']   = $noperjanjian !== "" ? $noperjanjian : $this->getnoperjanjian($va['tgl']) ;

      $error = "";
      if(trim($va['kode']) == "") $error .= "Karyawan tidak valid!! \\n";
      if(trim($va['tgl']) == "") $error .= "Tgl tidak valid!! \\n";
      if($va['lama'] <= 0) $error .= "Lama tidak valid!! \\n";
      if(trim($va['periode']) == "") $error .= "Periode tidak valid!! \\n";
      if(trim($va['tglawal']) == "" or trim($va['tglakhir']) == "" or date_2s($va['tglakhir']) < date_2s($va['tglawal'])) $error .= "Tgl awal atau tgl akhir tidak valid!! \\n";
      
      if(!isset($va['jabatan']))$va['jabatan'] = "";
      if(!isset($va['bagian']))$va['bagian'] = "";
      if($error == ""){
         $error = "ok";
         $arr = array("noperjanjian"=>$va['noperjanjian'],"nomor"=>$va['nomor'],"karyawan"=>$va['kode'],"tgl"=>date_2s($va['tgl']),
                     "lama"=>$va['lama'],"jabatan"=>$va['jabatan'],"bagian"=>$va['bagian'],"periode"=>$va['periode'],"tglawal"=>date_2s($va['tglpkawal']),
                     "tglakhir"=>date_2s($va['tglpkakhir']),"cabang"=>getsession($this,"cabang"),"status"=>"1",
                     "username"=>getsession($this,"username"),"datetime"=>date_now());
         $this->update("perjanjian_kerja",$arr,"noperjanjian = '{$va['noperjanjian']}'");
      }

      return $error;
   }

   public function getdata($noperjanjian){
      $data = array() ;
      $field = "c.kode,c.nama,c.telepon,c.tgl,c.alamat,c.ktp,k.noperjanjian,k.nomor,k.tgl,k.lama,k.periode,k.tglawal,k.tglakhir,
               k.bagian,b.keterangan as ketbagian,k.jabatan,j.keterangan as ketjabatan";
      $join = "left join karyawan c on c.kode = k.karyawan left join bagian b on b.kode = k.bagian
               left join jabatan j on j.kode = k.jabatan";
      $where = "k.noperjanjian = '$noperjanjian'";
      $data      = $this->select("perjanjian_kerja k", $field, $where, $join, "", "k.noperjanjian ASC") ;
		return $data ;
   }

   public function getfoto($kode){
      $where = "kode LIKE '$kode'" ;
      $dbd      = $this->select("sys_upload", "*", $where, "", "", "id") ;
      return $dbd;
   }

   public function loadgrid2($va){
      $limit = $va['offset'] . "," . $va['limit'];
      $search = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "";
      $search = $this->escape_like_str($search);
      $where = array();
      if ($search !== "") {
         $where[] = "(kode LIKE '{$search}%' OR nama LIKE '%{$search}%')";
      }

      $where = implode(" AND ", $where);
      $dbd = $this->select("karyawan", "kode,nama", $where, "", "", "kode ASC", $limit);
      $dba = $this->select("karyawan", "id", $where);
      return array("db" => $dbd, "rows" => $this->rows($dba));
   }

   public function getnoperjanjian($tgl,$l=true){
      $cabang = getsession($this, "cabang") ;
      $key  = "PK-".$cabang."/".date("y/m",strtotime($tgl))."-";
      $n    = $this->getincrement($key, $l,5);
      $noperjanjian    = $key.$n ;
      return $noperjanjian ;

   }

   public function seekbagian($search){
      $where = "(keterangan LIKE '%{$search}%')" ;
      $dbd      = $this->select("bagian", "*", $where, "", "", "keterangan ASC", '50') ;
      return array("db"=>$dbd) ;
   }

   public function seekjabatan($search){
      $where = "(keterangan LIKE '%{$search}%')" ;
      $dbd      = $this->select("jabatan", "*", $where, "", "", "keterangan ASC", '50') ;
      return array("db"=>$dbd) ;
   }

   public function seeksurat($search){
      $where = "(judul LIKE '%{$search}%') and status = '1'" ;
      $dbd      = $this->select("surat", "*", $where, "", "", "judul ASC", '50') ;
      return array("db"=>$dbd) ;
   }

   public function deleting($noperjanjian){
      $this->edit("perjanjian_kerja",array("status"=>"2"),"noperjanjian = " . $this->escape($noperjanjian));
   }
}
?>
