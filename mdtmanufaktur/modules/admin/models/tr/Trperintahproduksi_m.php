<?php
class Trperintahproduksi_m extends Bismillah_Model{

    public function getdatatotal($faktur){
        $data  = array() ;
        $field = "t.faktur,t.tgl,t.cabang,c.keterangan as ketcabang,p.stock,p.bb,p.qty,p.btkl,t.petugas,
                  p.bop,p.hargapokok,p.jumlah,s.satuan,s.keterangan,t.perbaikan,p.gudangperbaikan,
                  g.keterangan as ketgudang";
        $where = "t.faktur = '$faktur'";
        $join  = "left join cabang c on c.kode = t.cabang left join produksi_produk p on p.fakturproduksi = t. faktur";
        $join  .= "  left join stock s on s.kode = p.stock left join gudang g on g.kode = p.gudangperbaikan";
        $dbd   = $this->select("produksi_total t", $field, $where, $join) ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }

    public function deleting($faktur){
        $this->edit("produksi_total",array("status"=>2),"faktur = " . $this->escape($faktur));
        $this->delete("keuangan_bukubesar","faktur = " . $this->escape($faktur));
        $this->delete("stock_kartu","faktur = " . $this->escape($faktur));
        
        $this->edit("produksi_bb",array("status"=>2),"fakturproduksi = " . $this->escape($faktur));
        $dbd   = $this->select("produksi_bb", "faktur", "fakturproduksi = '$faktur'", "") ;
        while($dbr = $this->getrow($dbd)){
            $this->edit("produksi_total",array("status"=>2),"faktur = '{$dbr['faktur']}'");
            $this->delete("keuangan_bukubesar","faktur = '{$dbr['faktur']}'");
            $this->delete("stock_kartu","faktur = '{$dbr['faktur']}'");
        }

    }
    
    public function getdata($kode){
        $data = array() ;
        if($d = $this->getval("*", "kode = " . $this->escape($kode), "stock")){
            $data = $d;
        }
        return $data ;
    }
    
    public function autoloadkomponen($stock){
        $where[]	= "k.stock = '$stock'" ;
        $where 	 = implode(" AND ", $where) ;
        $field = "k.komponen kodebb,k.qty as qtybb,s.keterangan as namabb,s.satuan as satbb,k.hp as hpbb, (qty * hp) as jmlhpbb";
        $join  = "left join stock s on s.kode = k.komponen";
        $dbd      = $this->select("stock_komponen k", $field, $where, $join, "", "komponen ASC") ;

        return $dbd  ;
    }
    
    public function getdatadetail($faktur){
        $field = "b.stock as kodebb,b.qty as qtybb,b.hp as hpbb,b.jmlhp as jmlhpbb,s.satuan as satbb,s.keterangan as namabb";
        $where = "b.faktur = '$faktur'";
        $join  = "left join stock s on s.kode = b.stock";
        $dbd   = $this->select("produksi_bb_standart b", $field, $where, $join) ;
        return $dbd ;
    }
}
?>
