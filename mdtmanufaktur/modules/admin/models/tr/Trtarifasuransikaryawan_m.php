<?php
class Trtarifasuransikaryawan_m extends Bismillah_Model{
    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "(b.nopendaftaran LIKE '%{$search}%' or k.nama LIKE '%{$search}%')" ;
        $where[] = "b.status = '1' and b.tgl <= '{$va['tglakhir']}' and (k.tglkeluar >= '{$va['tglakhir']}' or k.tglkeluar = '0000-00-00')";
        if(isset($va['asuransi'])) $where[] = "b.asuransi = '{$va['asuransi']}'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "b.nopendaftaran,b.tgl,b.noasuransi,a.keterangan as asuransi,b.produk,
                    b.karyawan as nip,k.nama,b.id";
        $join     = "left join asuransi a on a.kode = b.asuransi left join karyawan k on k.kode = b.karyawan";
        $dbd      = $this->select("asuransi_karyawan b", $field, $where, $join, "", "b.nopendaftaran ASC", $limit) ;
        $dba      = $this->select("asuransi_karyawan b", "b.id", $where, $join, "", "b.nopendaftaran ASC") ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function saving($va){
        $error = "";
        if(trim($va['nopendaftaran']) == "")$error .= "No Pendaftaran tidak valid !! \\n";

        if($error == ""){
            $tgl = date_2s($va['tgl']);
            $where = "tgl = '$tgl' and nopendaftaran = '{$va['nopendaftaran']}'";
            $this->edit("asuransi_karyawan_tarif",array("status"=>"2"),$where) ;

            $data           = array("nopendaftaran"=>$va['nopendaftaran'],
                                    "tgl"=>$tgl,
                                    "karyawan"=>string_2n($va['karyawan']),
                                    "perusahaan"=>string_2n($va['perusahaan']),
                                    "status"=>"1",
                                    "username"=> getsession($this, "username"),
                                    "datetime"=>date("Y-m-d H:i:s")) ;
            $this->insert("asuransi_karyawan_tarif", $data) ;
            $error = "ok";
        }

        return $error;
        
    }


    public function seekasuransi($search){
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("asuransi", "*", $where, "", "", "kode ASC", '100') ;
        return array("db"=>$dbd) ;
    }
    

    

}
?>
