<?php
class Trhasilproduksi_m extends Bismillah_Model{
    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        $tgl = date("Y-m-d");
        $where[] = "t.faktur like '$search%'";
		$where[] = "t.status = '1' and t.tglclose = '0000-00-00'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "t.faktur,t.tgl,c.keterangan as cabang,t.petugas";
        $join     = "left join cabang c on c.kode = t.cabang";
        $dbd      = $this->select("produksi_total t", $field, $where, $join, "", "t. faktur ASC", $limit) ;
        $dba      = $this->select("produksi_total t", "t.id", $where) ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function saving($faktur, $va){

    }
    
    public function savingprod($va){
        $faktur = $this->getfaktur();
        $igd = $this->func_m->getinfogudang($va['gudang']) ;    
        
        $regu = $this->getval("regu", "faktur = '{$va['fakturprod']}'", "produksi_total");
        $qty_target = $this->getval("qty", "fakturproduksi = '{$va['fakturprod']}' and stock = '{$va['stock']}'", "produksi_produk");
        if($qty_target == "") $qty_target = 0;
        $vadetail = array("faktur"=>$faktur,"cabang"=>$igd['cabang'],"gudang"=>$va['gudang'],"fakturproduksi"=>$va['fakturprod'],"tgl"=>$va['tgl'],"regu"=>$regu,
                          "stock"=>$va['stock'],"qty_target"=>$qty_target,"qty"=>$va['qty'],"username"=>getsession($this, "username"),"datetime"=>date("Y-m-d H:i:s"));
        $this->insert("produksi_hasil",$vadetail);
        
        $this->edit("produksi_total",array("tglclose"=>$va['tgl']),"faktur = '{$va['fakturprod']}'");

        $pencapaian_pers = devide($va['qty'],$qty_target) * 100;
        $regu = json_decode($regu,true);
        if(!empty($regu)){
            $vacapaian = array("fakturproduksi"=>$va['fakturprod'],"fakturproduksihasil"=>$faktur,"tgl"=>$va['tgl'],"nip"=>$regu['karu'],"stock"=>$va['stock'],
                        "regu"=>$regu['kode'],"karu_status"=>"1","target"=>$qty_target,"pencapaian"=>$va['qty'],"pencapaian_pers"=>$pencapaian_pers,
                        "cabang"=>$igd['cabang']);
            $this->insert("produksi_pencapaian",$vacapaian);
            foreach($regu['anggota'] as $key2){
                $vacapaian = array("fakturproduksi"=>$va['fakturprod'],"fakturproduksihasil"=>$faktur,"tgl"=>$va['tgl'],"nip"=>$key2,"stock"=>$va['stock'],
                        "regu"=>$regu['kode'],"karu_status"=>"0","target"=>$qty_target,"pencapaian"=>$va['qty'],"pencapaian_pers"=>$pencapaian_pers,
                        "cabang"=>$igd['cabang']);
                $this->insert("produksi_pencapaian",$vacapaian);
            }
        }

        $this->updtransaksi_m->updkartustockhasilproduksi($faktur);
        $this->updtransaksi_m->updrekhasilproduksi($faktur);
    }
    public function getdatatotal($faktur){
        $data  = array() ;
        $field = "t.faktur,t.tgl,t.cabang,c.keterangan as ketcabang,t.perbaikan,t.petugas";
        $where = "t.faktur = '$faktur'";
        $join  = "left join cabang c on c.kode = t.cabang";
        $dbd   = $this->select("produksi_total t", $field, $where, $join) ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }

    public function deleteprod($va){
        $faktur = $va['faktur'];
        $this->edit("produksi_hasil",array("status"=>2),"faktur = " . $this->escape($faktur));
        $dbd      = $this->select("produksi_hasil", "id", "faktur = '{$va['fakturprod']}' and status = '1'") ;
        if($this->rows($dbd) > 0){
            $this->edit("produksi_total",array("tglclose"=>"0000-00-00"),"faktur = '{$va['fakturprod']}'");
        }
        $this->delete("stock_kartu","faktur = " . $this->escape($faktur));
        $this->delete("keuangan_bukubesar","faktur = " . $this->escape($faktur));

    }

    public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "PD".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }
    
    public function getdata($kode){
        $data = array() ;
        if($d = $this->getval("*", "kode = " . $this->escape($kode), "stock")){
            $data = $d;
        }
        return $data ;
    }
    
    public function getdataproduksiproduk($kode,$fakturprod){
        $where[]	= "s.kode = '$kode'" ;
        $where[]	= "p.fakturproduksi = '$fakturprod'" ;
        $where 	 = implode(" AND ", $where) ;
        $join = "left join stock s on s.kode = p.stock";
        $dbd      = $this->select("produksi_produk p", "s.kode,s.keterangan,p.qty,s.satuan", $where,$join, "", "s.kode ASC") ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }

        return $data ;
    }

    public function loadgrid2($va){
        $where = array();
        $where[]	= "p.fakturproduksi = '{$va['fakturproduksi']}' and p.status = '1'" ;
        $where 	 = implode(" AND ", $where) ;
        $field = "p.faktur,p.tgl,p.gudang,p.stock,s.keterangan as namastock,p.qty,s.satuan";
        $join = "left join stock s on s.kode = p.stock";
        $dbd      = $this->select("produksi_hasil p", $field, $where, $join, "", "p.stock ASC") ;
        $dba      = $this->select("produksi_hasil p", "p.id", $where) ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function loadgrid3($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        $where[]	= "s.tampil = 'P'" ;
        $where[]	= "p.fakturproduksi = '{$va['fakturproduksi']}'" ;
        if($search !== "") $where[]	= "(p.stock LIKE '{$search}%' OR s.keterangan LIKE '%{$search}%')" ;
        $where 	 = implode(" AND ", $where) ;
        $join = "left join stock s on s.kode = p.stock";
        $dbd      = $this->select("produksi_produk p", "s.kode,s.keterangan,p.qty,s.satuan,p.fakturproduksi", $where,$join, "", "s.kode ASC", $limit) ;
        $dba      = $this->select("produksi_produk p", "p.id", $where,$join) ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }
    
    public function loadgrid3old($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $where[]	= "tampil = 'P'" ;
        $where 	 = implode(" AND ", $where) ;
        $dbd      = $this->select("stock", "kode,keterangan,satuan", $where, "", "", "kode ASC", $limit) ;
        $dba      = $this->select("stock", "id", $where) ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function getdatadetail($faktur){
        $field = "p.stock,s.keterangan as namastock,p.qty,s.satuan";
        $where = "p.fakturproduksi = '$faktur'";
        $join  = "left join stock s on s.kode = p.stock";
        $dbd   = $this->select("produksi_produk p", $field, $where, $join) ;
        return $dbd ;
    }
}
?>
