<?php
class Trmutasibg_m extends Bismillah_Model
{
    public function loadgrid($va)
    {
        $limit = $va['offset'] . "," . $va['limit'];
        $search = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "";
        $search = $this->escape_like_str($search);
        $where = array();
        if ($search !== "") {
            $where[] = "t.faktur LIKE '%{$search}%'";
        }

        $where[] = "t.status = '1' and t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}'";
        $where = implode(" AND ", $where);
        $field = "t.faktur,t.tgl,t.keterangan,t.total,s.nama diberiterima,concat(r.kode,'-',r.keterangan) as rekening,t.jenis";
        $join = "left join supplier s on s.kode = t.diberiterima left join keuangan_rekening r on r.kode = t.rekening";
        $dbd = $this->select("bg_mutasi_total t", $field, $where, $join, "", "t. faktur ASC", $limit);
        $dba = $this->select("bg_mutasi_total t", "t.id", $where);

        return array("db" => $dbd, "rows" => $this->rows($dba));
    }

    public function saving($faktur, $va)
    {
        $total = string_2n($va['jumlah']);

        $faktur = getsession($this, "ssmutasibg_faktur", "");
        $va['faktur'] = $faktur !== "" ? $faktur : $this->getfaktur();

        //cek no bg
        $data = array("faktur" => $va['faktur'],
            "tgl" => $va['tgl'],
            "status" => "1",
            "keterangan" => $va['keterangan'],
            "rekening" => $va['rekening'],
            "diberiterima" => $va['diberiterima'],
            "total" => $total,
            "jenis" => "K",
            "cabang" => getsession($this, "cabang"),
            "username" => getsession($this, "username"),
            "datetime" => date("Y-m-d H:i:s"));
        $where = "faktur = " . $this->escape($faktur);
        $this->update("bg_mutasi_total", $data, $where, "");

        //insert detail
        $vaGrid = json_decode($va['grid2']);
        $this->delete("bg_list", "faktur = '{$va['faktur']}' and fakturcair = ''");
        foreach ($vaGrid as $key => $val) {
            $vadetail = array("faktur" => $va['faktur'],
                "cabang" => getsession($this, "cabang"),
                "bank" => $val->bank,
                "bgcek" => $val->bgcek,
                "norekening" => $val->norekening,
                "nobgcek" => $val->nobgcek,
                "tgl" => date_2s($va['tgl']),
                "tgljthtmp" => date_2s($val->jthtmp),
                "nominal" => string_2n($val->nominal),
                "jenis" => "K",
                "username" => getsession($this, "username"),
                "datetime" => date_now(),
                "kpddari" => $va['diberiterima'],
                "sc" => "S",
            );
            $this->update("bg_list", $vadetail, "faktur = '{$va['faktur']}' and nobgcek = '{$val->nobgcek}'");
        }
        $this->updtransaksi_m->updrekmutasibg($va['faktur']);

    }

    public function getdatatotal($faktur)
    {
        $data = array();
        $field = "t.faktur,t.tgl,t.diberiterima,t.keterangan,t.total,s.nama as namasupplier, t.rekening , r.keterangan as ketrekening";
        $where = "t.faktur = '$faktur'";
        $join = "left join supplier s on s.kode = t.diberiterima left join keuangan_rekening r on r.kode = t.rekening";
        $dbd = $this->select("bg_mutasi_total t", $field, $where, $join);
        if ($dbr = $this->getrow($dbd)) {
            $data = $dbr;
        }
        return $data;
    }

    public function getdatadetail($faktur)
    {
        $field = "d.faktur,r.kode,concat(d.bank,'-',b.keterangan) as ketbank,d.nominal,d.bgcek,d.norekening,
                  d.nobgcek,d.tgljthtmp as jthtmp,d.bank,d.fakturcair";
        $where = "d.faktur = '$faktur'";
        $join = "left join bank b on b.kode = d.bank left join keuangan_rekening r on r.kode = b.rekening";
        $dbd = $this->select("bg_list d", $field, $where, $join);
        return $dbd;
    }

    public function deleting($faktur)
    {
        $error = "";
        $error = $this->cekvalidasidel($faktur);
        if ($error == "") {
            $this->edit("bg_mutasi_total", array("status" => 2), "faktur = " . $this->escape($faktur));
            $this->delete("keuangan_bukubesar", "faktur = " . $this->escape($faktur));
            $this->edit("bg_list",array("status"=>2), "faktur = " . $this->escape($faktur)) ;
        }
        return $error;

    }

    public function seekbank($search)
    {
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%')";
        $dbd = $this->select("bank", "*", $where, "", "", "kode ASC", '');
        return array("db" => $dbd);
    }

    public function seekrekening($search)
    {
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%') and jenis = 'D'";
        $dbd = $this->select("keuangan_rekening", "*", $where, "", "", "kode ASC", '');
        return array("db" => $dbd);
    }

    public function seekdiberiterima($search)
    {
        $where = "(kode LIKE '{$search}%' OR nama LIKE '%{$search}%')";
        $dbd = $this->select("supplier", "*", $where, "", "", "nama ASC", '');
        return array("db" => $dbd);
    }

    public function getfaktur($l = true)
    {
        $cabang = getsession($this, "cabang");
        $key = "MG" . $cabang . date("ymd");
        $n = $this->getincrement($key, $l, 5);
        $faktur = $key . $n;
        return $faktur;
    }

    public function cekvalidasidel($faktur)
    {
        $error = "";
        $dbd = $this->select("bg_list", "nobgcek", "faktur = '$faktur' and fakturcair <> ''");
        if ($dbr = $this->getrow($dbd)) {
            $error .= "No bg " . $dbr['nobgcek'] . " sudah cair";
        }
        return $error;
    }
}
