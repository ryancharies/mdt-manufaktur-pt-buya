<?php

class Trpotgajipinjamankaryawan_m extends Bismillah_Model {

    public function loadgrid( $va ) {
        $va['tgl'] = date_2s($va['tgl']);
        $where = "p.tgl <= '{$va['tgl']}' and (p.tgllunas >= '{$va['tgl']}' or p.tgllunas = '0000-00-00') and p.status = '1'";
       
        $field = "p.nopinjaman,p.nip,k.nama,p.plafond,p.lama,p.tgl tglrealisasi,p.tglawalags";//,ifnull(sum(b.debet - b.kredit),0) as bakidebet
        
        $join = "left join karyawan k on k.kode = p.nip";//left join pinjaman_karyawan_kartu b on b.nopinjaman = p.nopinjaman and b.faktur <> '{$va['faktur']}'
        $dbd = $this->select("pinjaman_karyawan p", $field, $where, $join, "p.nopinjaman", "p.nip ASC");// having bakidebet <> 0
        $dba = $this->select("pinjaman_karyawan p", $field, $where,$join, "p.nopinjaman", "p.nip ASC");
        return array("db" => $dbd, "rows" =>$this->rows($dba));
    }

    public function saving( $va ) {

       // print_r($va);
        $error = '';
        $va['tgl'] = date_2s($va['tgl']);
        //cek valid
        if ( trim( $va['periode'] ) == '' )$error .= 'Payroll komponen gaji tidak valid !! \\n';
        if ( trim( $va['nopinjaman'] ) == '' )$error .= 'Nopinjaman tidak valid !! \\n';
        if ( trim( $va['nominal'] ) == '' )$error .= 'Nominal tidak valid !! \\n';

        if ( $error == '' ) {
            $data    = array( 'faktur'=>"", 'tgl'=>$va['tgl'], 'periode'=>$va['periode'], 'pokok'=>$va['nominal'],
            'nopinjaman'=>$va['nopinjaman'],'keterangan'=>"Potongan pinjaman payroll ","status"=>0,
            'cabang'=> getsession($this, "cabang"),'username'=> getsession( $this, 'username' ), 'datetime'=>date_now() ) ;
            $where   = "periode = '{$va['periode']}' and nopinjaman = '{$va['nopinjaman']}'" ;
            $this->update('payroll_pinjaman_karyawan_angsuran', $data, $where, '' ) ;

        }
        //simpan detail
        return $error;
    }

    public function seekperiode($search){
        $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("sys_periode_payroll", "*", $where, "", "", "kode desc", '50') ;
        return array("db"=>$dbd) ;
     }

    public function getperiode($va){
        $where = "kode = '{$va['periode']}'" ;
        $dbd      = $this->select("sys_periode_payroll", "*", $where, "", "", "kode desc", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getagspinjaman($nopinjaman,$periode){
        $return = array("pokok"=>0,"status"=>0);
        $where = "nopinjaman = '$nopinjaman' and periode = '$periode'" ;
        $dbd   = $this->select("payroll_pinjaman_karyawan_angsuran", "pokok,status", $where) ;
        if($dbr = $this->getrow($dbd)){
            $return = $dbr;
        }
        return $return;
    }
}
?>
