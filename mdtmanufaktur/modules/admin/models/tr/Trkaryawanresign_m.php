<?php
class Trkaryawanresign_m extends Bismillah_Model{
   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(c.kode LIKE '{$search}%' OR c.nama LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $field = "c.kode,c.nama,c.telepon,c.tgl,c.alamat,g.keterangan as bank,c.rekening,c.anrekening,c.ktp,c.tglkeluar,c.ketkeluar";
      $join = "left join bank g on g.kode = c.bank";
      $dbd      = $this->select("karyawan c", $field, $where, $join, "", "c.kode ASC", $limit) ;
      $dba      = $this->select("karyawan c", "c.id", $where) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function saving($va){
      $data    = array("tglkeluar"=>date_2s($va['tglkeluar']),"ketkeluar"=>$va['ketkeluar']) ;
      $where   = "kode = " . $this->escape($va['kode']) ;
      $this->edit("karyawan", $data, $where, "") ;

      $this->delete("absensi_karyawan_pin","nip =". $this->escape($va['kode']) );

   }

   public function getdata($kode){
      $data = array() ;
		if($d = $this->getval("*", "kode = " . $this->escape($kode), "karyawan")){
         $data = $d;
		}
		return $data ;
   }

   public function getfoto($kode){
      $where = "kode LIKE '$kode'" ;
      $dbd      = $this->select("sys_upload", "*", $where, "", "", "id") ;
      return $dbd;
   }
}
?>
