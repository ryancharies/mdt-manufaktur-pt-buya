<?php
class Tradjstock_m extends Bismillah_Model
{
    public function loadgrid($va)
    {
        $limit = $va['offset'] . "," . $va['limit'];
        $search = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "";
        $search = $this->escape_like_str($search);
        $where = array();
        if ($search !== "") {
            $where[] = "t.faktur LIKE '%{$search}%' or s.keterangan = '%{$search}%'";
        }

        $where[] = "t.status = '1' and t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}'";
        $where = implode(" AND ", $where);
        $field = "t.faktur,t.tgl,t.stock,s.satuan,s.keterangan as namastock,t.qtysistem,t.qtyfisik,t.qtyselisih,
                t.gudang,g.keterangan as ketgudang,t.keterangan";
        $join = "left join stock s on s.kode = t.stock left join gudang g on g.kode = t.gudang";
        $dbd = $this->select("stock_adj t", $field, $where, $join, "", "t.tgl,t. faktur ASC");
        $dba = $this->select("stock_adj t", "t.id", $where, $join);

        return array("db" => $dbd, "rows" => $this->rows($dba));
    }

    public function saving($faktur, $va)
    {
        $faktur = getsession($this, "ssadjstock_faktur", "");
        $va['faktur'] = $faktur !== "" ? $faktur : $this->getfaktur();
        
        $dstok = $this->perhitungan_m->getdatastock($va['stock']);
        $kodestock = $dstok['kode'];
        
        $data = array("faktur" => $va['faktur'],
            "tgl" => date_2s($va['tgl']),
            "stock" => $kodestock,
            "qtysistem" => string_2n($va['saldosistem']),
            "qtyfisik" => string_2n($va['saldofisik']),
            "qtyselisih" => string_2n($va['selisih']),
            "status" => "1",
            "gudang" => $va['gudang'],
            "keterangan" => $va['keterangan'],
            "cabang" => getsession($this, "cabang"),
            "username" => getsession($this, "username"),
            "datetime" => date_now());
        $where = "faktur = " . $this->escape($faktur);
        $this->update("stock_adj", $data, $where, "");

        //update kartu stock

        $this->updtransaksi_m->updkartustockadjstock($va['faktur']);
        $this->updtransaksi_m->updrekadjstock($va['faktur']);
    }

    public function getdatatotal($faktur)
    {
        $data = array();
        $field = "t.faktur,t.tgl,t.stock,s.satuan,s.keterangan as namastock,t.qtysistem,t.qtyfisik,t.qtyselisih,
                t.gudang,g.keterangan as ketgudang,t.keterangan";
        $join = "left join stock s on s.kode = t.stock left join gudang g on g.kode = t.gudang";
        $where = "t.faktur = '$faktur'";
        $dbd = $this->select("stock_adj t", $field, $where, $join);
        if ($dbr = $this->getrow($dbd)) {
            $data = $dbr;
        }
        return $data;
    }

    public function deleting($faktur)
    {
        $return = "ok";
        $this->delete("keuangan_bukubesar", "faktur = " . $this->escape($faktur));
        $this->delete("stock_kartu", "faktur = " . $this->escape($faktur));
        $this->edit("stock_adj", array("status" => 2), "faktur = " . $this->escape($faktur));

        return $return;
    }

    public function getfaktur($l = true)
    {
        $cabang = getsession($this, "cabang");
        $key = "PS" . $cabang . date("ymd");
        $n = $this->getincrement($key, $l, 5);
        $faktur = $key . $n;
        return $faktur;
    }

    public function getdata($kode)
    {
        $data = array();
        if ($d = $this->getval("*", "kode = " . $this->escape($kode), "stock")) {
            $data = $d;
        }
        return $data;
    }

}
