<?php
class Trpinjamankaryawan_m extends Bismillah_Model
{
    public function loadgrid($va)
    {
        $limit = $va['offset'] . "," . $va['limit'];
        $search = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "";
        $search = $this->escape_like_str($search);
        $where = array();
        if ($search !== "") {
            $where[] = "(p.nopinjaman LIKE '%{$search}%' or k.kode LIKE '%{$search}%' or k.nama LIKE '%{$search}%' or p.fktrealisasi LIKE '%{$search}%' or p.keterangan = '%{$search}%')";
        }

        $where[] = "p.status = '1' and p.tgl >= '{$va['tglawal']}' and p.tgl <= '{$va['tglakhir']}'";
        $where = implode(" AND ", $where);
        $field = "p.nopinjaman,p.tgl,p.nip,k.nama,p.keterangan,p.plafond,p.lama,p.fakturrealisasi,b.keterangan as bank,p.tglawalags,p.id";
        $join = "left join karyawan k on k.kode = p.nip left join bank b on b.kode = p.bank";
        $dbd = $this->select("pinjaman_karyawan p", $field, $where, $join, "", "p.tgl,p.nopinjaman ASC");
        $dba = $this->select("pinjaman_karyawan p", "p.id", $where, $join);

        return array("db" => $dbd, "rows" => $this->rows($dba));
    }

    public function saving($kode, $va)
    {
        $faktur = getsession($this, "sspinjamankaryawan_kode", "");
        $va['nopinjaman'] = $kode !== "" ? $kode : $this->getnopinjaman($va['nip']);
        $va['fktrealisasi'] = $kode !== "" ? $va['fktrealisasi'] : $this->getfaktur();
        $data = array("nopinjaman" => $va['nopinjaman'],
            "tgl" => date_2s($va['tgl']),
            "nip" => $va['nip'],
            "keterangan" => $va['keterangan'],
            "plafond" => string_2n($va['plafond']),
            "lama" => string_2n($va['lama']),
            "fakturrealisasi" => $va['fktrealisasi'],
            "bank" => $va['bank'],
            "tglawalags" => date_2s($va['tglawalags']),
            "cabang" => getsession($this, "cabang"),
            "username" => getsession($this, "username"),
            "datetime" => date_now());
        $where = "nopinjaman = " . $this->escape($va['nopinjaman']);
        $this->update("pinjaman_karyawan", $data, $where, "");

        $this->updtransaksi_m->updkartupencairanpinjamankaryawan($va['fktrealisasi']);
        $this->updtransaksi_m->updrekpencairanpinjamankaryawan($va['fktrealisasi']);
    }

    public function getdatatotal($nopinjaman)
    {
        $data = array();
        $field = "p.nopinjaman,p.tgl,p.nip,p.keterangan,p.plafond,p.lama,p.fakturrealisasi,p.bank,p.tglawalags,b.keterangan as ketbank,k.nama";
        $join = "left join bank b on b.kode = p.bank left join karyawan k on k.kode = p.nip";
        $where = "p.nopinjaman = '$nopinjaman'";
        $dbd = $this->select("pinjaman_karyawan p", $field, $where, $join);
        if ($dbr = $this->getrow($dbd)) {
            $data = $dbr;
        }
        return $data;
    }

    public function deleting($nopinjaman)
    {
        $error = "";
        $faktur = "";
        $field = "p.nopinjaman,p.tgl,p.nip,p.keterangan,p.plafond,p.lama,p.fakturrealisasi,p.bank,p.tglawalags,b.keterangan as ketbank,k.nama";
        $join = "left join bank b on b.kode = p.bank left join karyawan k on k.kode = p.nip";
        $where = "p.nopinjaman = '$nopinjaman'";
        $dbd = $this->select("pinjaman_karyawan p", $field, $where, $join);
        if ($dbr = $this->getrow($dbd)) {
            $dbd2 = $this->select("pinjaman_karyawan_kartu", "id", "nopinjaman = '{$dbr['nopinjaman']}' and faktur <> '{$dbr['fakturrealisasi']}'");
            if ($this->rows($dbd2) > 0) {
                $error .= "Pinjaman sudah pernah dilakukan angsuran \\n";
            }
            $faktur = $dbr['fakturrealisasi'];
        }

        if($error == ""){
            $this->delete("keuangan_bukubesar", "faktur = " . $this->escape($faktur));
            $this->delete("pinjaman_karyawan_kartu", "faktur = " . $this->escape($faktur));
            $this->edit("pinjaman_karyawan", array("status" => "2"), "nopinjaman = " . $this->escape($nopinjaman));
        }
        

        return $error;
    }

    public function seekbank($search)
    {
        $where = "kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%'";
        $dbd = $this->select("bank", "*", $where, "", "", "keterangan ASC", '50');
        return array("db" => $dbd);
    }

    public function getfaktur($l = true)
    {
        $cabang = getsession($this, "cabang");
        $key = "R0" . $cabang . date("ymd");
        $n = $this->getincrement($key, $l, 5);
        $faktur = $key . $n;
        return $faktur;
    }

    public function getnopinjaman($nip,$l = true)
    {
        $key = "KRD~" . $nip;
        $n = $this->getincrement($key, $l, 5);
        $faktur = $nip."-" . $n;
        return $faktur;
    }

    public function loadgrid3($va)
    {
        $limit = $va['offset'] . "," . $va['limit'];
        $search = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "";
        $search = $this->escape_like_str($search);
        $where = array();
        if ($search !== "") {
            $where[] = "(kode LIKE '{$search}%' OR nama LIKE '%{$search}%')";
        }

        $where = implode(" AND ", $where);
        $dbd = $this->select("karyawan", "kode,nama", $where, "", "", "kode ASC", $limit);
        $dba = $this->select("karyawan", "id", $where);
        return array("db" => $dbd, "rows" => $this->rows($dba));
    }

    public function getdata($kode)
    {
        $data = array();
        if ($d = $this->getval("*", "kode = " . $this->escape($kode), "karyawan")) {
            $data = $d;
        }
        return $data;
    }

}
