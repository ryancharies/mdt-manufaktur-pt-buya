<?php

class Trthr_m extends Bismillah_Model {

    public function loadgrid( $va ) {
        $va['tgl'] = date_2s($va['tgl']);
        $where = "k.tgl <= '{$va['tgl']}' and (k.tglkeluar > '{$va['tgl']}' or k.tglkeluar = '0000-00-00')";
       
        $field = "k.kode,k.nama";
        
        $join = "";
        $dbd = $this->select("karyawan k", $field, $where, $join, "k.kode", "k.kode ASC");
        $dba = $this->select("karyawan k", $field, $where,$join, "k.kode", "k.kode ASC");
        return array("db" => $dbd, "rows" =>$this->rows($dba));
    }

    public function saving( $va ) {

       // print_r($va);
        $error = '';
        //cek valid
        $va['grid2'] = json_decode($va['grid2']);
        if ( trim( $va['tgl'] ) == '' or trim( $va['tgl'] ) == '00-00-0000')$error .= 'Tgl tidak valid !! \\n';
        if(empty($va['grid2']))$error .= 'Pembayaran tidak valid !! \\n';

        if ( $error == '' ) {
            //$this->edit("payroll_posting",array("status"=>'2'),"periode = '{$va['periode']}'");

            $va['tgl'] = date_2s($va['tgl']);

            //loop karyawan
            $total = 0;
            $faktur = $this->getfaktur();
            $where = "k.tgl <= '{$va['tgl']}' and (k.tglkeluar > '{$va['tgl']}' or k.tglkeluar = '0000-00-00')";
            $field = "k.kode,k.nama";
            $join = "";
            $dbd = $this->select("karyawan k", $field, $where, $join, "k.kode", "k.kode ASC");
            while($dbr = $this->getrow($dbd)){
                //jab bag
                // $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$va['tgl']);
                // $bagian = $this->perhitunganhrd_m->getbagian($dbr['kode'],$va['tgl']);

                $thr = $this->perhitunganhrd_m->getthrkaryawan($dbr['kode'],$va['tgl']);                


                //loop komponen gaji
                $totkaryawan = 0;
                foreach($thr['detail'] as $key => $val){
                    $arrdata = array("faktur"=>$faktur,"nip"=>$dbr['kode'],"payroll"=>$val['payroll'],"periode"=>$val['periode'],
                            "perhitungan"=>$val['perhitungan'],"potongan"=>$val['potongan'],"diluarjadwal"=>$val['diluarjadwal'],
                            "lamakerja"=>$val['lamakerja'],"nominal"=>$val['nominal'],"prosentase"=>$val['pers'],
                            "total"=>$val['jumlah']);
                    $this->insert("thr_posting_komponen",$arrdata);

                    if($val['potongan']){
                        $totkaryawan -= $val['jumlah'];
                    }else{
                        $totkaryawan += $val['jumlah'];
                    }
                }
                
                $total += $totkaryawan;

            }

            //insert pembayaran
            foreach($va['grid2'] as $key => $val){
                $arrd = array("faktur"=>$faktur,"rekening"=>$val->kode,"nominal"=>string_2n($val->nominal));
                $this->insert("thr_posting_pembayaran",$arrd);
            }

            $data    = array( 'faktur'=>$faktur, 'tgl'=>$va['tgl'], 'total'=>$total,
                    'cabang'=> getsession($this, "cabang"),'username'=> getsession( $this, 'username' ), 'datetime'=>date_now() ) ;
            $where   = "faktur = '$faktur'" ;
            $this->update('thr_posting', $data, $where) ;

            $this->updtransaksi_m->updrekthr($faktur);

        }
        //simpan detail
        return $error;
    }


    public function getperiode($va){
        $where = "kode = '{$va['periode']}'" ;
        $dbd      = $this->select("sys_periode_payroll", "*", $where, "", "", "kode desc", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getfaktur($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "HR".$cabang.date("ymd");
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }

    public function seekrekening($search){
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%') and jenis = 'D'" ;
        $dbd      = $this->select("keuangan_rekening", "*", $where, "", "", "kode ASC", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getpembayaran($faktur){
        $where = "p.faktur = '$faktur'" ;
        $field = "p.rekening as kode,p.nominal,concat(p.rekening,'-',r.keterangan) as ketketerangan";
        $join = "left join keuangan_rekening r on r.kode = p.rekening";
        $dbd      = $this->select("thr_posting_pembayaran p", $field, $where, $join, "") ;
        return $dbd ;
    }
}
?>
