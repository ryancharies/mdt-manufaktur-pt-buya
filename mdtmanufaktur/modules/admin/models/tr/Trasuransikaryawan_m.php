<?php
class Trasuransikaryawan_m extends Bismillah_Model{
    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "b.nopendaftaran LIKE '%{$search}%'" ;
        $where[] = "b.status = '1' and b.tgl >= '{$va['tglawal']}' and b.tgl <= '{$va['tglakhir']}'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "b.nopendaftaran,b.tgl,b.noasuransi,a.keterangan as asuransi,b.produk,b.karyawan as nip,k.nama";
        $join     = "left join asuransi a on a.kode = b.asuransi left join karyawan k on k.kode = b.karyawan";
        $dbd      = $this->select("asuransi_karyawan b", $field, $where, $join, "", "b.nopendaftaran ASC", $limit) ;
        $dba      = $this->select("asuransi_karyawan b", "b.id", $where, $join, "", "b.nopendaftaran ASC") ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function saving($nopendaftaran, $va){
        $error = "";
        if(trim($va['nopendaftaran']) == "")$error .= "No Pendaftaran tidak valid !! \\n";
        if(trim($va['noasuransi']) == "")$error .= "No Asuransi tidak valid !! \\n";
        if(!isset($va['asuransi']))$error .= "Asuransi belum dipilih !! \\n";
        if(trim($va['produk']) == "")$error .= "Produk tidak valid !! \\n";
        if(trim($va['nip']) == "")$error .= "Karyawan belum dipilih !! \\n";

        if($error == ""){
            $nopendaftaran         = getsession($this, "ssasuransikaryawan_nopendaftaran", "");
            $va['nopendaftaran']   = $nopendaftaran !== "" ? $nopendaftaran : $this->getnopendaftaran() ;
            $data           = array("nopendaftaran"=>$va['nopendaftaran'],
                                    "tgl"=>date_2s($va['tgl']),
                                    "noasuransi"=>$va['noasuransi'],
                                    "asuransi"=>$va['asuransi'],
                                    "produk"=>$va['produk'],
                                    "karyawan"=>$va['nip'],
                                    "status"=>"1",
                                    "username"=> getsession($this, "username"),
                                    "datetime"=>date("Y-m-d H:i:s")) ;
            $where          = "nopendaftaran = " . $this->escape($nopendaftaran) ;
            $this->update("asuransi_karyawan", $data, $where, "") ;
            $error = "ok";
        }

        return $error;
        
    }

    public function getdatatotal($nopendaftaran){
        $data  = array() ;
        $field = "b.nopendaftaran,b.tgl,b.noasuransi,b.asuransi,a.keterangan as ketasuransi,b.produk,b.karyawan as nip,k.nama";
        $where = "b.nopendaftaran = '$nopendaftaran'";
        $join  = "left join asuransi a on a.kode = b.asuransi left join karyawan k on k.kode = b.karyawan";
        $dbd   = $this->select("asuransi_karyawan b", $field, $where, $join,"b.nopendaftaran") ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }

    
    public function deleting($nopendaftaran){
        $error = "";
        if(trim($nopendaftaran) == "") $error .= "No pendaftaran tidak valid!!!";

        if($error == ""){
            $this->edit("asuransi_karyawan",array("status"=>2),"nopendaftaran = " . $this->escape($nopendaftaran));
            $error = "ok";
        }

        return $error;
        
    }

    public function seekasuransi($search){
        $where = "(kode LIKE '{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("asuransi", "*", $where, "", "", "kode ASC", '100') ;
        return array("db"=>$dbd) ;
    }
    

    public function getnopendaftaran($l=true){
        $cabang = getsession($this, "cabang") ;
        $key  = "INS".$cabang.date("y");
        $n    = $this->getincrement($key, $l,5);
        $return    = $key.$n ;
        return $return ;
    }

    public function loadgrid2($va)
    {
        $limit = $va['offset'] . "," . $va['limit'];
        $search = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "";
        $search = $this->escape_like_str($search);
        $where = array();
        if ($search !== "") {
            $where[] = "(k.nama LIKE '{$search}%' OR k.kode LIKE '%{$search}%')";
        }
        $va['tgl'] = date_2s($va['tgl']);
        $where[] = "k.tgl <= '{$va['tgl']}'";
        $where = implode(" AND ", $where);
        $field = "k.kode as nip,k.nama";
        
        $join = "";
        $dbd = $this->select("karyawan k", $field, $where, $join, "", "k.kode ASC", $limit);
        $dba = $this->select("karyawan k", $field, $where,$join, "", "k.kode ASC");
        return array("db" => $dbd, "rows" =>$this->rows($dba));
    }
    
    public function getdatakaryawan($va){
        
        $va['tgl'] = date_2s($va['tgl']);
        $where[] = "k.tgl <= '{$va['tgl']}' and k.kode = '{$va['nip']}'";
        $where = implode(" AND ", $where);
        $field = "k.kode,k.nama";
        $join = "";
        $dbd = $this->select("karyawan k", $field, $where, $join, "", "k.kode ASC");
        
        return $dbd;
    }

}
?>
