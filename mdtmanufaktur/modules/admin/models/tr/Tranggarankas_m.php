<?php
class Tranggarankas_m extends Bismillah_Model{
    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "a.periode LIKE '%{$search}%'" ;
        $where[] = "a.status = '1' and a.periode >= '{$va['periodeawal']}' and a.periode <= '{$va['periodeakhir']}'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "substr(a.periode,1,4) as periode,SUM(IF(r.aruskas = 'o', a.nominal, 0)) AS oprasional,
                    SUM(IF(r.aruskas = 'p', a.nominal, 0)) AS pendanaan,SUM(IF(r.aruskas = 'i', a.nominal, 0)) AS investasi,
                    a.username,a.datetime,sum(a.nominal) as nominal";
        $join     = "left join keuangan_rekening r on r.kode = a.rekening";
        $dbd      = $this->select("anggaran_kas a", $field, $where, $join, "substr(a.periode,1,4)", "a.periode ASC") ;
       
        
        return array("db"=>$dbd, "rows"=> $this->rows($dbd) ) ;
    }
    
    public function loadgrid2($va){
      $where 	 = array() ; 
      $where[]    = "r.aruskas <> 'k'";
      $where[]    = "r.jenis = 'D'";
      $where 	 = implode(" AND ", $where) ;
      $join     = "";
      $f        = "r.kode,r.keterangan,r.aruskas" ; 
      $dbd      = $this->select("keuangan_rekening r", $f, $where, $join, "r.kode", "r.aruskas,r.kode ASC") ;
      $dba      = $this->select("keuangan_rekening r", $f, $where, $join, "r.kode", "r.aruskas,r.kode ASC") ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba)) ;
   } 

    public function saving($va){
        $tahun = $va['periode'];
        
        //insert detail
        $vaGrid = json_decode($va['grid2']);
        $cabang = getsession($this,"cabang") ;
        $username = getsession($this,"username") ;
        $datetime = date_now() ;

        $this->edit("anggaran_kas",array("status"=>"2"), "cabang  = '$cabang' and periode like '$tahun%'" ) ;
        foreach($vaGrid as $key => $val){
            $n = 0 ;
            $rekening = $val->kode;
            //print_r($val);
            foreach($val as $key2 => $val2){
                if($n >= 5 && $n <= 16 && $val2 > 0){
                    $bln = $n-4;
                    $periode = date("Y-m-t",mktime(0,0,0,$bln,1,$tahun));      
                    $arr = array("periode"=>$periode,"cabang"=>$cabang,"rekening"=>$rekening,"nominal"=>$val2,"status"=>"1","username"=>$username,
                                "datetime"=>$datetime);  
                    $this->insert("anggaran_kas",$arr);
                }
                $n++;
            }
        }
    }

    public function loadanggarankas($tglawal,$tglakhir){
        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);

        $arrreturn = array();
        $arrwhere = array();
        $arrwhere[] = "a.periode >= '$tglawal' and a.periode <= '$tglakhir'"; 
        $wherefkt = implode(" or ",$arrwhere);
        $dbd2      = $this->select("keuangan_rekening", "kode") ;
        while($dbr2 = $this->getrow($dbd2)){
            $arrreturn[$dbr2['kode']] = array("penggunaan"=>0);
            if($wherefkt <> ""){
                $rekening = $dbr2['kode'];
                $where3 = "r.aruskas <> 'k' and a.rekening like '$rekening%' and (".$wherefkt.")" ;
                $join3 = "left join keuangan_rekening r on r.kode = a.rekening";
                $dbd3      = $this->select("anggaran_kas a", "a.rekening,sum(a.nominal) as nominal", $where3, $join3, "a.rekening having nominal > 0", "") ;
                if($dbr3 = $this->getrow($dbd3)){
                    $arrreturn[$dbr2['kode']]['penggunaan'] += $dbr3['nominal'];
                }
            }

        }
        /* $faktur = $dbr['faktur'];
            $join3 = "left join keuangan_rekening r on r.kode = b.rekening";
            $where3 = "r.aruskas <> 'k'";
            $dbd3      = $this->select("keuangan_bukubesar b", "b.rekening,b.debet,b.kredit", $where3, $join3, "b.faktur", "b.faktur ASC") ;
            while($dbr3 = $this->getrow($dbd3) ){
                $arrreturn[$dbr3['rekening']]['sumberdana'] += $dbr3['kredit'];
                $arrreturn[$dbr3['rekening']]['penggunaan'] += $dbr3['debet'];
            }*/
        return $arrreturn ;
    }

    public function loadrekeningstt($stt,$jenis="D"){
        $where = "aruskas ='$stt'" ;
        if($jenis <> "") $where .= " and jenis = '$jenis'";
        $dbd      = $this->select("keuangan_rekening", "*", $where, "", "", "kode ASC") ;
        return $dbd ;
    }
}
?>
