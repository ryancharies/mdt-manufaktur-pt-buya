<?php

class Rptchecklistaccounting_m extends Bismillah_Model
{

    public function checkpersediaan($tgl)
    {
        $cabang = getsession($this, "cabang");
        $tgl = date_2s($tgl);
        $data = array();
        $cField = "*";
        $cWhere = "";
        $vaJoin = "";
        $dbData = $this->select("stock_group", $cField, $cWhere, $vaJoin);
        while ($dbr = $this->getrow($dbData)) {
            $data[$dbr['kode']] = array("kode" => $dbr['kode'], "rekening" => $dbr['rekpersd'],
                "keterangan" => $dbr['keterangan'], "neraca" => 0, "nominatif" => 0);
            //cek saldo neraca
            $data[$dbr['kode']]['neraca'] = $this->perhitungan_m->getsaldo($tgl, $dbr['rekpersd']);

            //cek saldo stock
            $dbs = $this->select("stock", "kode", "stock_group = '{$dbr['kode']}'");
            while ($dbrs = $this->getrow($dbs)) {
                $arrhp = $this->perhitungan_m->getsaldohpstock($dbrs['kode'], $tgl, $cabang);
                $data[$dbr['kode']]['nominatif'] += $arrhp['hptot'];
            }
        }

        return $data;
    }

    public function checksaldohutangpb($tgl)
    {
        $cabang = getsession($this, "cabang");
        $tgl = date_2s($tgl);
        $data = array("neraca" => 0, "nominatif" => 0);
        $rekhutpb = $this->getconfig("rekpbhut");
        $data['neraca'] = $this->perhitungan_m->getsaldo($tgl, $rekhutpb);

        $dba = $this->select("supplier", "kode");
        while ($dbr = $this->getrow($dba)) {
            $data['nominatif'] += $this->perhitungan_m->GetSaldoAkhirHutang($dbr['kode'], $tgl, 'PB', 'H', true);
            $data['nominatif'] += $this->perhitungan_m->GetSaldoAkhirHutang($dbr['kode'], $tgl, 'KV', 'H', true);
            $data['nominatif'] += $this->perhitungan_m->GetSaldoAkhirHutang($dbr['kode'], $tgl, 'RB', 'H', true);
        }

        return $data;
    }

    public function checksaldohutangas($tgl)
    {
        $cabang = getsession($this, "cabang");
        $tgl = date_2s($tgl);
        $data = array();
        $cField = "g.rekhutpembelian,g.kode,r.keterangan as ketrekening,g.keterangan";
        $cWhere = "";
        $vaJoin = "left join keuangan_rekening r on r.kode = g.rekhutpembelian";
        $dbData = $this->select("aset_golongan g", $cField, $cWhere, $vaJoin);
        while ($dbr = $this->getrow($dbData)) {
            $key = $dbr['rekhutpembelian'];
            if(!isset($data[$key])){
                $data[$key] = array("rekening" => $dbr['rekhutpembelian'],"ketrekening"=>$dbr['ketrekening'],
                "detail" => array(), "neraca" => $this->perhitungan_m->getsaldo($tgl, $dbr['rekhutpembelian']),
                "nominatif" => 0);
            }
            $data[$key]['detail'][$dbr['kode']] = array("keterangan"=>$dbr['keterangan'],"nominatif"=>0);
            $golongan = $dbr['kode'];
            $dbs = $this->select("aset", "kode,vendors,faktur", "golongan = '$golongan' and vendors is not null");
            while ($dbrs = $this->getrow($dbs)) {
                $vendors = json_decode($dbrs['vendors'],true) ;
                foreach($vendors as $key2 => $val){
                    $nominatif = $this->perhitungan_m->GetSaldoAkhirHutang($val['vendor'], $tgl,$dbrs['faktur']);
                    $data[$key]['nominatif'] += $nominatif;
                    $data[$key]['detail'][$dbr['kode']]['nominatif'] += $nominatif;
                }
            }            
        }
        return $data;
    }


    public function checksaldopiutangpj($tgl)
    {
        $cabang = getsession($this, "cabang");
        $tgl = date_2s($tgl);
        $data = array("neraca" => 0, "nominatif" => 0);
        $rek = $this->getconfig("rekpjpiutang");
        $data['neraca'] = $this->perhitungan_m->getsaldo($tgl, $rek);

        $dba = $this->select("customer", "kode");
        while ($dbr = $this->getrow($dba)) {
            $data['nominatif'] += $this->perhitungan_m->GetSaldoAkhirPiutang($dbr['kode'], $tgl);
        }

        return $data;
    }


    public function checksaldoporsekot($tgl)
    {
        $cabang = getsession($this, "cabang");
        $tgl = date_2s($tgl);
        $data = array();
        $cField = "k.kode,k.rekening,r.keterangan as ketrekening";
        $cWhere = "k.jenis = 'P'";
        $vaJoin = "left join keuangan_rekening r on r.kode = k.rekening";
        $dbData = $this->select("kodetransaksi k", $cField, $cWhere, $vaJoin, "");
        while ($dbr = $this->getrow($dbData)) {
            //$data['neraca'] += $this->perhitungan_m->getsaldo($tgl, $dbr['rekening']);
            $key = $dbr['rekening'];
            if(!isset($data[$key])){
                $data[$key] = array("rekening" => $dbr['rekening'],"ketrekening"=>$dbr['ketrekening'],
                "neraca" => $this->perhitungan_m->getsaldo($tgl, $dbr['rekening']),
                "nominatif" => 0);
            }

            $dbds = $this->select("supplier", "kode", "", "");
            while ($dbrs = $this->getrow($dbds)) {
                $data[$key]['nominatif'] += $this->perhitungan_m->GetSaldoAkhirHutang($dbrs['kode'], $tgl, "", "P",false,$dbr['kode']);
            }
        }


        return $data;
    }

    public function checksaldoum($tgl)
    {
        $cabang = getsession($this, "cabang");
        $tgl = date_2s($tgl);
        $data = array("neraca" => 0, "nominatif" => 0);
        $cField = "k.kode,k.rekening,r.keterangan as ketrekening";
        $cWhere = "k.jenis = 'U'";
        $vaJoin = "left join keuangan_rekening r on r.kode = k.rekening";
        $dbData = $this->select("kodetransaksi k", $cField, $cWhere, $vaJoin, "k.rekening");
        while ($dbr = $this->getrow($dbData)) {
            $data['neraca'] += $this->perhitungan_m->getsaldo($tgl, $dbr['rekening']);
        }

        $dbds = $this->select("customer", "kode", "", "");
        while ($dbrs = $this->getrow($dbds)) {
            $data['nominatif'] += $this->perhitungan_m->GetSaldoAkhirpiutang($dbrs['kode'], $tgl, "", "U");
        }

        return $data;
    }

    public function checksaldodp($tgl)
    {
        $cabang = getsession($this, "cabang");
        $tgl = date_2s($tgl);
        $data = array("neraca" => 0, "nominatif" => 0);
        $cField = "k.kode,k.rekening,r.keterangan as ketrekening";
        $cWhere = "k.jenis = 'D'";
        $vaJoin = "left join keuangan_rekening r on r.kode = k.rekening";
        $dbData = $this->select("kodetransaksi k", $cField, $cWhere, $vaJoin, "k.rekening");
        while ($dbr = $this->getrow($dbData)) {
            $data['neraca'] += $this->perhitungan_m->getsaldo($tgl, $dbr['rekening']);
        }

        $dbds = $this->select("customer", "kode", "", "");
        while ($dbrs = $this->getrow($dbds)) {
            $data['nominatif'] += $this->perhitungan_m->GetSaldoAkhirpiutang($dbrs['kode'], $tgl, "", "D");
        }

        return $data;
    }

    public function checksaldohutbg($tgl)
    {
        $cabang = getsession($this, "cabang");
        $tgl = date_2s($tgl);
        $data = array("neraca" => 0, "nominatif" => 0);
        $rek = $this->getconfig("rekhutbg");
        $data['neraca'] = $this->perhitungan_m->getsaldo($tgl, $rek);

        $where = "(a.tglcair > '$tgl' or a.tglcair = '0000-00-00') and a.status = '1' and a.tgl <= '$tgl'";
        $f = "a.*,b.keterangan as ketbank,s.nama as namasupplier";
        $join = "left join supplier s on s.kode = a.kpddari left join bank b on b.kode = a.bank";
        $dbds = $this->select("bg_list a", $f, $where, $join, "", "a.tgl desc");
        while ($dbrs = $this->getrow($dbds)) {
            $data['nominatif'] += $dbrs['nominal'];
        }

        return $data;
    }

    public function checksaldokasbonkaryawan($tgl){
        $cabang = getsession($this, "cabang");
        $tgl = date_2s($tgl);
        $data = array();
        $kogapypinjkaryawan = $this->getconfig("kogapypinjkaryawan") ;
        //$rek = $this->getval("rekakt", "kode = '$kogapypinjkaryawan'", "payroll_komponen");
        
        

        $field = "p.nopinjaman,p.nip,ifnull(sum(k.debet-k.kredit),0) as bakidebet";
        $where = "(p.tgllunas > '$tgl' or p.tgllunas = '0000-00-00') and p.status = '1' and p.tgl <= '$tgl' and k.tgl <= '$tgl'";
        $join  = "left join pinjaman_karyawan_kartu k on k.nopinjaman = p.nopinjaman";
        $dbds  = $this->select("pinjaman_karyawan p", $field, $where, $join, "p.nopinjaman", "");
        while ($dbrs = $this->getrow($dbds)) {
            $bagian = $this->perhitunganhrd_m->getbagian($dbrs['nip'],$tgl);
            $rek = $this->getval("rekening", "payroll = '$kogapypinjkaryawan' and bagian = '{$bagian['bagian']}'", "payroll_komponen_rekening_bagian");
            $key = $rek;
            if(!isset($data[$key])){
                $neraca = $this->perhitungan_m->getsaldo($tgl, $rek);
                $ketrek = $this->getval("keterangan", "kode = '$rek'", "keuangan_rekening");

                $data[$key] = array("rekening"=>$rek,"keterangan"=>$ketrek,"neraca"=>$neraca,"nominatif"=>0);
            }

            $data[$key]['nominatif'] += $dbrs['bakidebet'];
        }
        return $data;
    }
}
