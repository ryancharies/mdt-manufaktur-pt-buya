<?php
class Rptrealisasikas_m extends Bismillah_Model{
    public function loadrekkas(){
        $dbd      = $this->select("keuangan_rekening", "*", "jenis='D' and aruskas = 'k'") ;
        return $dbd ;
    }
    public function loaddatakas($va){
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $where  = "r.aruskas = 'k' and b.tgl >= '{$va['tglawal']}' and b.tgl <= '{$va['tglakhir']}'";
        //$where .= "and (b.faktur like 'KK%' or b.faktur like 'KM%' or )";
        $field    = "b.faktur,b.tgl,b.cabang,b.rekening,b.keterangan,b.debet,b.kredit";
        $join     = "left join keuangan_rekening r on r.Kode = b.rekening";
        $dbd      = $this->select("keuangan_bukubesar b", $field, $where, $join, "", "b.tgl,b.faktur ASC") ;
        return $dbd ;
    }
}
?>
