<?php

class Rptkpikaryawan_m extends Bismillah_Model {

    public function loadgrid( $va ) {
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $where = "k.tgl <= '{$va['tglakhir']}' and (k.tglkeluar > '{$va['tglakhir']}' or k.tglkeluar = '0000-00-00')";
       
        $field = "k.kode,k.nama,k.rekening";
        
        $join = "";
        $dbd = $this->select("karyawan k", $field, $where, $join, "k.kode", "k.kode ASC");
        $dba = $this->select("karyawan k", $field, $where,$join, "k.kode", "k.kode ASC");
        return array("db" => $dbd, "rows" =>$this->rows($dba));
    }

    public function loaddaftargaji( $va ) {
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $where = "k.tgl <= '{$va['tglakhir']}' and (k.tglkeluar > '{$va['tglakhir']}' or k.tglkeluar = '0000-00-00') and k.rekening <> ''";
       
        $field = "k.kode,k.nama,k.rekening";
        
        $join = "";
        $dbd = $this->select("karyawan k", $field, $where, $join, "k.kode", "k.kode ASC");
        $dba = $this->select("karyawan k", $field, $where,$join, "k.kode", "k.kode ASC");
        return array("db" => $dbd, "rows" =>$this->rows($dba));
    }

    public function datakpi(){
        $where = "" ;
        $field = "kode,keterangan,perhitungan,satuan";
        $join = "";
        $dbd      = $this->select("kpi_indikator", $field, $where, $join, "") ;
        return $dbd ;
    }
}
?>
