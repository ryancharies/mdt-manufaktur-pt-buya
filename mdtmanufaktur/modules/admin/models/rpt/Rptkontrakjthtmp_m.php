<?php

class Rptkontrakjthtmp_m extends Bismillah_Model{

    public function loadgrid($va){
        $limit = $va['offset'] . "," . $va['limit'];
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $where [] = "k.tglakhir >= '{$va['tglawal']}' and k.tglakhir <= '{$va['tglakhir']}'";
        $where [] = "k.status = '1'";
        $where 	 = implode(" AND ", $where) ;
        $field = "c.kode,c.nama,c.telepon,c.tgl,c.alamat,c.ktp,k.noperjanjian,k.nomor,k.tgl,k.lama,k.periode,k.tglawal,k.tglakhir,
            b.keterangan as bagian, j.keterangan as jabatan";
        $join = "left join karyawan c on c.kode = k.karyawan left join bagian b on b.kode = k.bagian left join jabatan j on j.kode = k.jabatan";
        $dbd      = $this->select("perjanjian_kerja k", $field, $where, $join, "", "k.noperjanjian ASC", $limit) ;
        $dba      = $this->select("perjanjian_kerja k", "k.id", $where,$join, "", "k.noperjanjian ASC") ;
        return array("db" => $dbd, "rows" =>$this->rows($dba));//$this->rows($dba)
    }

    public function loaddata($va){
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $where [] = "k.tglakhir >= '{$va['tglawal']}' and k.tglakhir <= '{$va['tglakhir']}'";
        $where [] = "k.status = '1'";
        $where 	 = implode(" AND ", $where) ;
        $field = "c.kode,c.nama,c.telepon,c.tgl,c.alamat,c.ktp,k.noperjanjian,k.nomor,k.tgl,k.lama,k.periode,k.tglawal,k.tglakhir,
            b.keterangan as bagian, j.keterangan as jabatan";
        $join = "left join karyawan c on c.kode = k.karyawan left join bagian b on b.kode = k.bagian left join jabatan j on j.kode = k.jabatan";
        $dbd =  $this->select("perjanjian_kerja k", $field, $where, $join, "", "k.noperjanjian ASC");
        return array("db" => $dbd);//$this->rows($dba)
    }
}
