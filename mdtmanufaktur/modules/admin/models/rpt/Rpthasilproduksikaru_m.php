<?php

class Rpthasilproduksikaru_m extends Bismillah_Model{

    public function getdetailproduksikaru($va){
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);

        $this->db->where('t.tgl >=', $va['tglawal']);
        $this->db->where('t.tgl <=', $va['tglakhir']);
        $this->db->where('h.status', "1");
        $this->db->where('t.status', "1");
        
        $this->db->like('JSON_EXTRACT(t.regu, "$.karu")',$va['karu']);


        $f = "s.kode,s.keterangan,ifnull(sum(h.qty),0) as prodselesai,s.satuan";

        $dbd = $this->db->select($f)
                ->from("stock s")
                ->join("produksi_hasil h","h.stock = s.kode","left")
                ->join("produksi_total t","t.faktur = h.fakturproduksi","left")
                ->group_by("s.kode")
                ->get();
        return $dbd ;
    }

}
