<?php

class Rptkartupinjamankaryawan_m extends Bismillah_Model{

    public function loadgrid($va){
        $where      = array() ;
        $where[]    = "k.nopinjaman = '{$va['nopinjaman']}' and k.tgl >= '{$va['tglAwal']}' and k.tgl <= '{$va['tglAkhir']}'";
        $where      = implode(" AND ", $where) ;
        $field      = "k.faktur,k.tgl,k.keterangan,k.debet,k.kredit";
        $cJoin      = "";
        $dbd        = $this->select("pinjaman_karyawan_kartu k", $field, $where, $cJoin ,"", "k.tgl ASC,k.faktur asc") ;
        $dba        = $this->select("pinjaman_karyawan_kartu k", "k.id", $where) ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function getdata($kode){
        $data = array() ;
        if($d = $this->getval("*", "kode = " . $this->escape($kode), "customer")){
            $data = $d;
        }
        return $data ;
    }

    public function loadgrid2($va){
        $limit = $va['offset'] . "," . $va['limit'];
        $search = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "";
        $search = $this->escape_like_str($search);
        $where = array();
        if ($search !== "") {
            $where[] = "(k.nama LIKE '{$search}%' OR p.nopinjaman LIKE '%{$search}%')";
        }
        $where[] = "p.status = '1'";
        $where = implode(" AND ", $where);
        $field = "p.nopinjaman,p.nip,k.nama,p.plafond,p.lama";//,ifnull(sum(b.debet - b.kredit),0) as bakidebet
        
        $join = "left join karyawan k on k.kode = p.nip";//left join pinjaman_karyawan_kartu b on b.nopinjaman = p.nopinjaman and b.faktur <> '{$va['faktur']}'
        $dbd = $this->select("pinjaman_karyawan p", $field, $where, $join, "p.nopinjaman", "p.nip ASC", $limit);// having bakidebet <> 0
        $dba = $this->select("pinjaman_karyawan p", $field, $where,$join, "p.nopinjaman", "p.nip ASC");
        return array("db" => $dbd, "rows" =>$this->rows($dba));//$this->rows($dba)
    }


    public function getdatanopinjaman($va){
        
        $where[] = "p.nopinjaman = '{$va['nopinjaman']}'";
        $where = implode(" AND ", $where);
        $field = "p.nopinjaman,p.nip,k.nama,p.plafond,p.lama,p.tgl as tglrealisasi,p.tglawalags";//,ifnull(sum(b.debet - b.kredit),0) as bakidebet
        
        $join = "left join karyawan k on k.kode = p.nip
                left join pinjaman_karyawan_kartu b on b.nopinjaman = p.nopinjaman ";//left join pinjaman_karyawan_kartu b on b.nopinjaman = p.nopinjaman and b.faktur <> '{$va['faktur']}'
        $dbd = $this->select("pinjaman_karyawan p", $field, $where, $join, "p.nopinjaman", "p.nip ASC");// having bakidebet <> 0
        
        return $dbd;
    }
}
