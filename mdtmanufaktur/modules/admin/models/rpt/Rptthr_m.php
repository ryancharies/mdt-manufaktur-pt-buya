<?php

class Rptthr_m extends Bismillah_Model {

    public function loadgrid( $va ) {
        $va['tgl'] = date_2s($va['tgl']);
        $where = "k.tgl <= '{$va['tgl']}' and (k.tglkeluar > '{$va['tgl']}' or k.tglkeluar = '0000-00-00')";
       
        $field = "k.kode,k.nama";
        
        $join = "";
        $dbd = $this->select("karyawan k", $field, $where, $join, "k.kode", "k.kode ASC");
        $dba = $this->select("karyawan k", $field, $where,$join, "k.kode", "k.kode ASC");
        return array("db" => $dbd, "rows" =>$this->rows($dba));
    }

    
    public function getpembayaran($faktur){
        $where = "p.faktur = '$faktur'" ;
        $field = "p.rekening as kode,p.nominal,concat(p.rekening,'-',r.keterangan) as ketketerangan";
        $join = "left join keuangan_rekening r on r.kode = p.rekening";
        $dbd      = $this->select("thr_posting_pembayaran p", $field, $where, $join, "") ;
        return $dbd ;
    }
}
?>
