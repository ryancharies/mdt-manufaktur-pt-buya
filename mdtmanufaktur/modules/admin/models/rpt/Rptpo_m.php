<?php

class Rptpo_m extends Bismillah_Model{

   
    public function getDetailPO($cFaktur){
        $cField = "s.kode, p.faktur,s.keterangan,p.spesifikasi,p.jumlah,p.harga,
                   (select ifnull(sum(debet-kredit),0) from po_kartu where fktpo =  p.faktur and stock = p.stock ) qtysisa,
                   (select ifnull(sum(qty),0) from po_detail where faktur =  p.faktur and stock = p.stock ) qty,
                   s.satuan, t.Tgl, sp.nama as supplier,t.fktpr";
        $cJoin  = "LEFT JOIN stock s ON s.kode = p.stock
                    left join po_kartu k on k.fktpo = p.faktur and k.stock = p.stock
                   Left JOIN po_total t on t.faktur = p.faktur
                   LEFT JOIN supplier sp on sp.kode = t.supplier" ;
        $cWhere = "p.faktur = '".$cFaktur."'" ;
        $dbData = $this->select("po_detail p",$cField,$cWhere,$cJoin,"concat(k.fktpo,k.stock)") ;//"concat(k.fktpo,p.stock)"
        return $dbData ;
    }

    public function GetDataPerFaktur($faktur){
        $data  = array() ;
        $cField = "p.faktur,t.total,t.Tgl,s.nama as supplier,t.fktpr,s.terminhari,s.namabank,
        s.rekening,s.atasnamarekening,s.notelepon,s.kontakpersonal";
        $cWhere = "p.faktur = '".$faktur."'" ;
        $vaJoin = "LEFT JOIN po_total t ON t.faktur = p.faktur
                   LEFT JOIN supplier s on s.kode = t.supplier" ;
        $dbData = $this->select("po_detail p",$cField,$cWhere,$vaJoin,"") ;
        if($dbr = $this->getrow($dbData)){
            $data = $dbr;
        }
        return $data ;
    }

  
    public function getdatasupplier($kode){
        $data  = array() ;
        $cField = "*";
        $cWhere = "kode = '".$kode."'" ;
        $vaJoin = "" ;
        $dbData = $this->select("supplier",$cField,$cWhere,$vaJoin,"") ;
        if($dbr = $this->getrow($dbData)){
            $data = $dbr;
        }
        return $data ;
    }
}
