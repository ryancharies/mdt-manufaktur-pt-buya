<?php
class Rptanggarankas_m extends Bismillah_Model{
    public function loadrekkas(){
        $dbd      = $this->select("keuangan_rekening", "*", "jenis='D' and aruskas = 'k'") ;
        return $dbd ;
    }
    public function loaddataanggarankas($va){
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $where  = "t.status = '1' and t.periode >= '{$va['tglawal']}' and t.periode <= '{$va['tglakhir']}'";
        $field    = "t.faktur,t.periode as tgl,t.cabang,t.rekening,t.keterangan,if(t.jenis='KK',t.nominal,0) kredit,
                    if(t.jenis='KM',t.nominal,0) debet,t.jenis,t.status,t.username,t.datetime";
        $join     = "left join keuangan_rekening r on r.Kode = t.rekening";
        $dbd      = $this->select("anggaran_kas t", $field, $where, $join, "", "t.periode,t. faktur ASC") ;
        return $dbd ;
    }

    public function loadbgjatuhtempo($va){
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $where	= "a.tgljthtmp >= '{$va['tglawal']}' and a.tgljthtmp <= '{$va['tglakhir']}' and a.status = '1'" ;
        $f        = "a.*,b.keterangan as ketbank,s.nama as namasupplier" ;
        $join    = "left join supplier s on s.kode = a.kpddari left join bank b on b.kode = a.bank"; 
        $dbd      = $this->select("bg_list a", $f, $where, $join, "", "a.tgl asc") ;
        
        return $dbd ;
    }

    public function loadhtjatuhtempo($va){
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $where	= "a.jthtmp >= '{$va['tglawal']}' and a.jthtmp <= '{$va['tglakhir']}' and a.status = '1' and a.hutang > 0" ;
        $f        = "a.*,s.nama as namasupplier" ;
        $join    = "left join supplier s on s.kode = a.supplier"; 
        $dbd      = $this->select("pembelian_total a", $f, $where, $join, "", "a.tgl asc") ;
        
        return $dbd ;
    }

}
?>
