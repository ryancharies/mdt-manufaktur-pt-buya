<?php

class Rptpelunasanhutang_m extends Bismillah_Model{

    public function loadgrid($va){
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->escape_like_str($search) ;
        $where 	 = array() ;
        if($search !== "") $where[]	= "t.faktur LIKE '%{$search}%' or s.nama = '%{$search}%'" ;
        $where[] = "t.status = '1' and t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}'";
        $where 	 = implode(" AND ", $where) ;
        $field    = "t.faktur,t.tgl,s.nama as supplier,t.subtotal,t.diskon,t.pembulatan,t.kasbank,t.pembelian,t.keterangan,
                    t.retur,b.keterangan as ketrekkasbank,t.persekot,t.bgcek";
        $join     = "left join supplier s on s.Kode = t.supplier left join bank b on b.kode = t.rekkasbank";
        $dbd      = $this->select("hutang_pelunasan_total t", $field, $where, $join, "", "t. faktur ASC", $limit) ;
        $dba      = $this->select("hutang_pelunasan_total t", "t.id", $where) ;

        return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
    }

    public function getDetail($cFaktur){
        $cField = "p.fkt,p.jumlah,p.jenis";
        $cJoin  = "" ;
        $cWhere = "p.faktur = '".$cFaktur."'" ;
        $dbData = $this->select("hutang_pelunasan_detail p",$cField,$cWhere,$cJoin) ;
        return $dbData ;
    }

    public function getdetailpelunasanhutang($cFaktur){
        $cField = "p.fkt,p.jumlah,p.jenis";
        $cJoin  = "" ;
        $cWhere = "p.faktur = '".$cFaktur."'" ;
        $dbData = $this->select("hutang_pelunasan_detail p",$cField,$cWhere,$cJoin) ;
        return $dbData ;
    }

    public function GetDataPerFaktur($faktur){
        $data  = array() ;
        $field = "t.faktur,t.tgl,t.supplier,s.nama as namasupplier,t.pembelian,t.retur,t.subtotal,t.kasbank,t.rekkasbank,t.keterangan,
                    b.keterangan as ketrekkasbank,t.diskon,t.pembulatan,t.persekot,t.bgcek,t.cabang";
        $where = "t.faktur = '$faktur'";
        $join  = "left join supplier s on s.kode = t.supplier left join bank b on b.kode = t.rekkasbank";
        $dbd   = $this->select("hutang_pelunasan_total t", $field, $where, $join) ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }

    public function getTotalpelunasanhutang($tglawal,$tglakhir){
        $field = "t.faktur,t.tgl,t.supplier,s.nama as namasupplier,t.pembelian,t.retur,t.subtotal,t.kasbank,t.rekkasbank,t.keterangan,
                    b.keterangan as ketrekkasbank,t.diskon,t.pembulatan,t.persekot,t.bgcek";
        $where = "t.tgl >= '".$tglawal."' AND t.tgl <= '".$tglakhir."' and status = '1'" ;
        $join  = "left join supplier s on s.kode = t.supplier left join bank b on b.kode = t.rekkasbank";
        $dbd   = $this->select("hutang_pelunasan_total t", $field, $where, $join) ;
        return $dbd ;
    }

    public function getdatabg($faktur){
        $where = "a.faktur = '$faktur'" ;
        $dbd      = $this->select("bg_list a", "a.*,b.keterangan as ketbank", $where, "left join bank b on b.kode = a.bank", "", "a.id ASC") ;
        return array("db"=>$dbd) ;
    }

    public function cetakdm($faktur){
        $this->escpos->connect();
        $cpl = 80;
        $data = $this->GetDataPerFaktur($faktur);
        if(!empty($data)){
            $arrcab = $this->func_m->GetDataCabang($data['cabang']);
            $arrcab['alamat'] = substr($arrcab['alamat'],0,50);
            $arrcab['telp'] = substr($arrcab['telp'],0,50);
            $data['namasupplier'] = substr($data['namasupplier'],0,20);

            $this->escpos->teks(str_pad($arrcab['nama'],50," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad($arrcab['alamat'],80," ",STR_PAD_RIGHT));	 
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->posisiteks("center");
            //$this->escpos->textsize(4,4);
            $this->escpos->teks("Pelunasan Hutang");
            $this->escpos->posisiteks("left");
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            //header
            $this->escpos->teks(str_pad("Faktur",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad($data['faktur'],20," ",STR_PAD_RIGHT).
                                str_pad(" ",2," ",STR_PAD_BOTH).
                                str_pad("Supplier",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad($data['namasupplier'],20," ",STR_PAD_LEFT));
            $this->escpos->teks(str_pad("Tanggal",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(date_2d($data['tgl']),20," ",STR_PAD_RIGHT).
                                str_pad(" ",2," ",STR_PAD_BOTH).
                                str_pad("",16," ",STR_PAD_RIGHT).
                                str_pad("",3," ",STR_PAD_BOTH).
                                str_pad("",20," ",STR_PAD_LEFT));
            
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            //body pelunasan
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("No",5," ",STR_PAD_BOTH).
						 //"|".str_pad("Kode",12," ",STR_PAD_BOTH).
                         "|".str_pad("FKT",20," ",STR_PAD_BOTH).
						 "|".str_pad("Jumlah",25," ",STR_PAD_BOTH).
						 "|".str_pad("Jenis",25," ",STR_PAD_BOTH)."|");
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $dbd = $this->getdetailpelunasanhutang($faktur);
            $n = 0 ;
            while($dbr = $this->getrow($dbd)){
                $n++;
                $this->escpos->teks("|".str_pad($n,5," ",STR_PAD_BOTH).
						 //"|".str_pad("Kode",12," ",STR_PAD_BOTH).
                         "|".str_pad($dbr['fkt'],20," ",STR_PAD_BOTH).
						 "|".str_pad(string_2s($dbr['jumlah']),25," ",STR_PAD_BOTH).
						 "|".str_pad($dbr['jenis'],25," ",STR_PAD_BOTH)."|");
            }
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            
            //bgcek
            $vdbd = $this->getdatabg($faktur);
            if($this->rows($vdbd['db']) > 0){
                $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
                $this->escpos->teks("|".str_pad("No",2," ",STR_PAD_BOTH).
						 //"|".str_pad("Kode",12," ",STR_PAD_BOTH).
                         "|".str_pad("Bank",15," ",STR_PAD_BOTH).
						 "|".str_pad("BGCEK",5," ",STR_PAD_BOTH).
                         "|".str_pad("No Rekening",15," ",STR_PAD_BOTH).
                         "|".str_pad("No BGCEK",12," ",STR_PAD_BOTH).
                         "|".str_pad("Total",15," ",STR_PAD_BOTH).
                         "|".str_pad("JthTmp",8," ",STR_PAD_BOTH).
                         "|");
                $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
                $n = 0 ;
                while($dbr = $this->getrow($vdbd['db'])){
                    $n++;
                    // $status = "Belum Cair";
                    // if($dbr['fakturcair'] <> '') $status = "Sudah Cair";
                    $this->escpos->teks("|".str_pad($n,2," ",STR_PAD_BOTH).
						 //"|".str_pad("Kode",12," ",STR_PAD_BOTH).
                         "|".str_pad($dbr['ketbank'],15," ",STR_PAD_BOTH).
						 "|".str_pad($dbr['bgcek'],5," ",STR_PAD_BOTH).
                         "|".str_pad($dbr['norekening'],15," ",STR_PAD_BOTH).
                         "|".str_pad($dbr['nobgcek'],12," ",STR_PAD_BOTH).
                         "|".str_pad(string_2s($dbr['nominal']),15," ",STR_PAD_BOTH).
                         "|".str_pad(date_2d($dbr['tgljthtmp']),8," ",STR_PAD_BOTH).
                         "|");
                }
                $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
                $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            }

            //footer
            $this->escpos->teks(str_pad("Pembelian",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($data['pembelian']),20," ",STR_PAD_LEFT).
                                str_pad(" ",2," ",STR_PAD_BOTH).
                                str_pad("Retur",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($data['retur']),20," ",STR_PAD_LEFT));
            $this->escpos->teks(str_pad("Subtotal",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($data['subtotal']),20," ",STR_PAD_LEFT).
                                str_pad(" ",2," ",STR_PAD_BOTH).
                                str_pad("Porsekot",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($data['persekot']),20," ",STR_PAD_LEFT));
            $this->escpos->teks(str_pad("Kas / Bank",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($data['kasbank']),20," ",STR_PAD_LEFT).
                                str_pad(" ",2," ",STR_PAD_BOTH).
                                str_pad("BG / CEK",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($data['bgcek']),20," ",STR_PAD_LEFT));
            $this->escpos->teks(str_pad("Diskon",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($data['diskon']),20," ",STR_PAD_LEFT).
                                str_pad(" ",2," ",STR_PAD_BOTH).
                                str_pad("Pembulatan",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($data['pembulatan']),20," ",STR_PAD_LEFT));
            $this->escpos->teks(str_pad("Diskon",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($data['diskon']),20," ",STR_PAD_LEFT).
                                str_pad(" ",2," ",STR_PAD_BOTH).
                                str_pad("Pembulatan",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($data['pembulatan']),20," ",STR_PAD_LEFT));
            $this->escpos->teks(str_pad("Rek Kas / Bank",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad($data['ketrekkasbank'],20," ",STR_PAD_RIGHT).
                                str_pad(" ",2," ",STR_PAD_BOTH).
                                str_pad("",16," ",STR_PAD_RIGHT).
                                str_pad("",3," ",STR_PAD_BOTH).
                                str_pad("",20," ",STR_PAD_LEFT));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));

            //ttd
            $now  = date_2b($data['tgl']) ;
            $tglttd = $this->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $this->escpos->teks(str_pad($tglttd,40," ",STR_PAD_LEFT));        
            $this->escpos->teks(str_pad("Yang Menerima,",40," ",STR_PAD_BOTH).
                                str_pad("Bag. Keuangan,",40," ",STR_PAD_BOTH));      
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));  
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("(.......................)",40," ",STR_PAD_BOTH).
                                str_pad("(.......................),",40," ",STR_PAD_BOTH)); 
        
            $this->escpos->cetak(true);
        }

        //$dbdD = $this->getDetailpelunasanhutang($faktur);
    }

}
