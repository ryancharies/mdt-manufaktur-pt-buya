<?php

class Rptsaldopinjamankaryawan_m extends Bismillah_Model{

    public function loadgrid($va){
        $limit = $va['offset'] . "," . $va['limit'];
        $va['tgl'] = date_2s($va['tgl']);
        $where = "p.tgl <= '{$va['tgl']}' and (p.tgllunas >= '{$va['tgl']}' or p.tgllunas = '0000-00-00') and p.status = '1'";
       
        $field = "p.nopinjaman,p.nip,k.nama,p.plafond,p.lama,p.tgl";//,ifnull(sum(b.debet - b.kredit),0) as bakidebet
        
        $join = "left join karyawan k on k.kode = p.nip";//left join pinjaman_karyawan_kartu b on b.nopinjaman = p.nopinjaman and b.faktur <> '{$va['faktur']}'
        $dbd = $this->select("pinjaman_karyawan p", $field, $where, $join, "p.nopinjaman", "p.nip ASC", $limit);// having bakidebet <> 0
        $dba = $this->select("pinjaman_karyawan p", $field, $where,$join, "p.nopinjaman", "p.nip ASC");
        return array("db" => $dbd, "rows" =>$this->rows($dba));//$this->rows($dba)
    }

    public function loaddata($va){
        $va['tgl'] = date_2s($va['tgl']);
        $where = "p.tgl <= '{$va['tgl']}' and (p.tgllunas >= '{$va['tgl']}' or p.tgllunas = '0000-00-00') and p.status = '1'";
       
        $field = "p.nopinjaman,p.nip,k.nama,p.plafond,p.lama,p.tgl";//,ifnull(sum(b.debet - b.kredit),0) as bakidebet
        
        $join = "left join karyawan k on k.kode = p.nip";//left join pinjaman_karyawan_kartu b on b.nopinjaman = p.nopinjaman and b.faktur <> '{$va['faktur']}'
        $dbd = $this->select("pinjaman_karyawan p", $field, $where, $join, "p.nopinjaman", "p.nip ASC");
        return array("db" => $dbd);//$this->rows($dba)
    }
}
