<?php

class Rptpayroll_m extends Bismillah_Model {

    public function loadgrid( $va ) {
        
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $where = "k.tgl <= '{$va['tglakhir']}' and (k.tglkeluar > '{$va['tglakhir']}' or k.tglkeluar = '0000-00-00')";
        if($va['cari'] <> "") $where .= " and (k.kode like '%{$va['cari']}%' or k.nama like '%{$va['cari']}%' or k.rekening like '%{$va['cari']}%')";
        $field = "k.kode,k.nama,k.rekening";
        
        $join = "";
        $dbd = $this->select("karyawan k", $field, $where, $join, "k.kode", "k.kode ASC");
        $dba = $this->select("karyawan k", $field, $where,$join, "k.kode", "k.kode ASC");
        return array("db" => $dbd, "rows" =>$this->rows($dba));
    }

    public function loaddaftargaji( $va ) {
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $where = "k.tgl <= '{$va['tglakhir']}' and (k.tglkeluar > '{$va['tglakhir']}' or k.tglkeluar = '0000-00-00') and k.rekening <> ''";
       
        $field = "k.kode,k.nama,k.rekening";
        
        $join = "";
        $dbd = $this->select("karyawan k", $field, $where, $join, "k.kode", "k.kode ASC");
        $dba = $this->select("karyawan k", $field, $where,$join, "k.kode", "k.kode ASC");
        return array("db" => $dbd, "rows" =>$this->rows($dba));
    }

    public function getdatakaryawan( $kode ) {
        $where = "k.kode = '$kode'";
       
        $field = "k.kode,k.nama";
        
        $join = "";
        $dbd = $this->select("karyawan k", $field, $where, $join, "k.kode", "k.kode ASC");
        return $dbd;
    }

    
    public function seekperiode($search){
        $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("sys_periode_payroll", "*", $where, "", "", "kode desc", '50') ;
        return array("db"=>$dbd) ;
    }
    
    public function seekbagian($search){
        $where = "(keterangan LIKE '%{$search}%')" ;
        $dbd      = $this->select("bagian", "*", $where, "", "", "kode", '50') ;
        return array("db"=>$dbd) ;
    }

    public function getperiode($va){
        $where = "kode = '{$va['periode']}'" ;
        $dbd      = $this->select("sys_periode_payroll", "*", $where, "", "", "kode desc", '50') ;
        return array("db"=>$dbd) ;
    }
    
    public function getpembayaran($faktur){
        $where = "p.faktur = '$faktur'" ;
        $field = "p.rekening as kode,p.nominal,r.keterangan as ketrekening";
        $join = "left join keuangan_rekening r on r.kode = p.rekening";
        $dbd      = $this->select("payroll_posting_pembayaran p", $field, $where, $join, "") ;
        return $dbd ;
    }
}
?>
