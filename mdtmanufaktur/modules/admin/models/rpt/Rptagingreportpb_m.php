<?php

class Rptagingreportpb_m extends Bismillah_Model{

    public function loadgrid($va){        
        $search     = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search     = $this->escape_like_str($search) ;
        $where      = array() ;
        $where[] = "t.tgl <= '{$va['tglakhir']}'" ;
        if($search !== "") $where[] = "t.faktur LIKE '%{$search}%'" ;
        $where      = implode(" AND ", $where) ;
        $join = "left join hutang_kartu k on k.fkt = t.faktur and k.tgl <= '{$va['tglakhir']}' 
                left join supplier s on s.kode = t.supplier";
        $field = "t.faktur,t.tgl,t.total,sum(k.debet-k.kredit) as saldo,t.jthtmp,
                s.nama as supplier,t.fktpo";
        $dbd      = $this->select("pembelian_total t", $field, $where, $join, "t.faktur having saldo <> 0", "t.jthtmp asc,t.faktur ASC") ;
        return array("db"=>$dbd, "rows"=> $this->rows($dbd)) ;
    }

    public function loadhutpembelianaset($va){
        $search     = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search     = $this->escape_like_str($search) ;
        $where      = array() ;
        $where[] = "t.tglperolehan <= '{$va['tglakhir']}'" ;
        if($search !== "") $where[] = "t.faktur LIKE '%{$search}%'" ;
        $where      = implode(" AND ", $where) ;
        $join = "left join hutang_kartu k on k.fkt = t.faktur and k.tgl <= '{$va['tglakhir']}'
                left join supplier s on s.kode = t.vendor";
        $field = "t.faktur,t.tglperolehan tgl,t.vendor,t.hargaperolehan total,sum(k.debet-k.kredit) as saldo,t.tglperolehan jthtmp,
                s.nama as supplier,t.keterangan,t.kode";
        $dbd      = $this->select("aset t", $field, $where, $join, "t.faktur having saldo <> 0 ", "t.tglperolehan,t.faktur ASC") ;
        return array("db"=>$dbd) ;
    }

    public function getdatapembelianaset($kode){
        $where 	 = "a.kode = " . $this->escape($kode);
        $join     = "left join aset_golongan g on g.kode = a.golongan left join cabang c on c.kode = a.cabang
					left join supplier s on s.kode = a.vendor";
        $field    = "a.kode,a.keterangan,a.golongan,g.keterangan as ketgolongan,a.cabang, c.keterangan as ketcabang,a.lama,a.tglperolehan,a.hargaperolehan,a.unit,
                    a.jenispenyusutan,a.tarifpenyusutan,a.residu,a.faktur,s.nama as ketvendor,a.vendor";
        $dbd      = $this->select("aset a", $field, $where, $join, "", "a.kode ASC","1") ;
        return $dbd;
    }

    public function getDetailPembelian($cFaktur){
        $cField = "s.Kode as KodeStock, p.faktur,s.keterangan,p.jumlah as pembelian,p.harga as HargaSatuan,p.qty,t.jthtmp,
                    s.satuan,p.diskonitem as hargadiskon,p.totalitem as total, t.Tgl, sp.nama as supplier";
        $cJoin  = "LEFT JOIN stock s ON s.kode = p.stock
                   Left JOIN pembelian_total t on t.faktur = p.faktur
                   LEFT JOIN supplier sp on sp.kode = t.supplier" ;
        $cWhere = "p.faktur = '".$cFaktur."'" ;
        $dbData = $this->select("pembelian_detail p",$cField,$cWhere,$cJoin) ;
        return $dbData ;
    }

    public function GetDataPerFaktur($faktur){
        $data  = array() ;
        $cField = "p.faktur,SUM(t.subtotal) as subtotal, SUM(t.total) as total, SUM(t.diskon) as diskon,t.jthtmp,
                   SUM(t.ppn) as ppn, t.Tgl, s.nama as supplier,t.persppn,t.fktpo";
        $cWhere = "p.faktur = '".$faktur."'" ;
        $vaJoin = "LEFT JOIN pembelian_total t ON t.faktur = p.faktur
                   LEFT JOIN supplier s on s.kode = t.supplier" ;
        $dbData = $this->select("pembelian_detail p",$cField,$cWhere,$vaJoin) ;
        if($dbr = $this->getrow($dbData)){
            $data = $dbr;
        }
        return $data ;
    }

    public function getTotalPembelian($dTglAkhir){
        $where      = array() ;
        $where[] = "t.tgl <= '$dTglAkhir'" ;
        $where      = implode(" AND ", $where) ;
        $join = "left join hutang_kartu k on k.fkt = t.faktur and k.tgl <= '$dTglAkhir' 
                left join supplier s on s.kode = t.supplier";
        $field = "t.faktur,t.tgl,t.total,sum(k.debet-k.kredit) as saldo,t.jthtmp,
                s.nama as supplier,t.fktpo";
        $dbd      = $this->select("pembelian_total t", $field, $where, $join, "t.faktur having saldo <> 0", "t.jthtmp asc,t.faktur ASC") ;
        return $dbd ;
    }

    public function getTotalPembelianaset($dTglAkhir){
        $where      = array() ;
        $where[] = "t.tglperolehan <= '$dTglAkhir'" ;
        $where      = implode(" AND ", $where) ;
        $join = "left join hutang_kartu k on k.fkt = t.faktur and k.tgl <= '$dTglAkhir'
                left join supplier s on s.kode = t.vendor";
        $field = "t.faktur,t.tglperolehan tgl,t.vendor,t.hargaperolehan total,sum(k.debet-k.kredit) as saldo,t.tglperolehan jthtmp,
                s.nama as supplier,t.keterangan,t.kode";
        $dbd      = $this->select("aset t", $field, $where, $join, "t.faktur having saldo <> 0 ", "t.tglperolehan,t.faktur ASC") ;
        return $dbd ;
    }

}
