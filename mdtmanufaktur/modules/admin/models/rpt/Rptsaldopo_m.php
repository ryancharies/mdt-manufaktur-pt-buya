<?php

class Rptsaldopo_m extends Bismillah_Model{

    public function loadgrid($va){
        $va['tglakhir']  = date_2s($va['tglakhir']);
        $search     = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search     = $this->escape_like_str($search) ;
        $where      = array() ;
        if($search !== "") $where[] = "p.faktur LIKE '%{$search}%' and s.nama LIKE '%{$search}%'" ;
        $where[]    = "p.tgl <= '{$va['tglakhir']}'";
        $where[]    = "p.status = '1'";
        $where      = implode(" AND ", $where) ;
        $field = "p.faktur,p.tgl,s.nama as supplier,p.gudang,g.keterangan as ketgudang,k.fktpo,p.total,s.nama as supplier,p.fktpr,
                    ifnull(sum(k.debet-k.kredit),0) as saldo,
                    (select r.hp from po_kartu r where r.faktur = p.faktur and r.stock = k.stock limit 1) as hp";
        $join = "left join po_total p on p.faktur = k.fktpo left join supplier s on s.kode = p.supplier left join gudang g on g.kode = p.gudang";
        $dbd = $this->select("po_kartu k", $field, $where, $join, "concat(k.fktpo,k.stock) having saldo > 0", "p.faktur asc");

        return $dbd ;
    }

    public function getDetailPO($cFaktur,$tgl){
        $tgl = date_2s($tgl);
        $cField = "s.kode, p.faktur,s.keterangan,p.spesifikasi,p.jumlah,p.harga,ifnull(sum(k.debet-k.kredit),0) qtysisa,
                   p.qty,s.satuan, t.Tgl, sp.nama as supplier,t.fktpr,
                   (select r.hp from po_kartu r where r.faktur = p.faktur and r.stock = k.stock) as hp";
        $cJoin  = "LEFT JOIN stock s ON s.kode = p.stock
                    left join po_kartu k on k.fktpo = p.faktur and k.stock = p.stock
                   Left JOIN po_total t on t.faktur = p.faktur
                   LEFT JOIN supplier sp on sp.kode = t.supplier" ;
        $cWhere = "p.faktur = '".$cFaktur."' and k.tgl <= '$tgl'" ;
        $dbData = $this->select("po_detail p",$cField,$cWhere,$cJoin,"concat(k.fktpo,k.stock)") ;
        return $dbData ;
    }

    public function GetDataPerFaktur($faktur){
        $data  = array() ;
        $cField = "p.faktur,t.total,t.Tgl,s.nama as supplier,t.fktpr,s.terminhari,s.namabank,
        s.rekening,s.atasnamarekening,s.notelepon,s.kontakpersonal";
        $cWhere = "p.faktur = '".$faktur."'" ;
        $vaJoin = "LEFT JOIN po_total t ON t.faktur = p.faktur
                   LEFT JOIN supplier s on s.kode = t.supplier" ;
        $dbData = $this->select("po_detail p",$cField,$cWhere,$vaJoin,"") ;
        if($dbr = $this->getrow($dbData)){
            $data = $dbr;
        }
        return $data ;
    }

    public function getTotalPO($va){
        $va['tglakhir']  = date_2s($va['tglakhir']);
        $where      = array() ;
        $where[]    = "p.tgl <= '{$va['tglakhir']}'";
        $where[]    = "p.status = '1'";
        $where      = implode(" AND ", $where) ;
        $field = "p.faktur,p.tgl,s.nama as supplier,p.gudang,g.keterangan as ketgudang,k.fktpo,p.total,s.nama as supplier,p.fktpr,
                    ifnull(sum(k.debet-k.kredit),0) as saldo,
                    (select r.hp from po_kartu r where r.faktur = p.faktur and r.stock = k.stock limit 1) as hp";
        $join = "left join po_total p on p.faktur = k.fktpo left join supplier s on s.kode = p.supplier left join gudang g on g.kode = p.gudang";
        $dbd = $this->select("po_kartu k", $field, $where, $join, "concat(k.fktpo,k.stock) having saldo > 0", "p.faktur asc");

        return $dbd ;
    }

    public function getdatasupplier($kode){
        $data  = array() ;
        $cField = "*";
        $cWhere = "kode = '".$kode."'" ;
        $vaJoin = "" ;
        $dbData = $this->select("supplier",$cField,$cWhere,$vaJoin,"") ;
        if($dbr = $this->getrow($dbData)){
            $data = $dbr;
        }
        return $data ;
    }
}
