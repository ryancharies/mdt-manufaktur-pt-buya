<?php
class Rptriwayatabsensikaryawan_m extends Bismillah_Model{
   public function loadgrid($va){
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->escape_like_str($search) ;
      $where 	 = array() ; 
      if($search !== "") $where[]	= "(c.kode LIKE '{$search}%' OR c.nama LIKE '%{$search}%')" ;
      $where 	 = implode(" AND ", $where) ;
      $field = "c.kode,c.nama,c.telepon,c.tgl,c.alamat,g.keterangan as bank,c.rekening,c.anrekening,c.ktp,c.id";
      $join = "left join bank g on g.kode = c.bank";
      $dbd      = $this->select("karyawan c", $field, $where, $join, "", "c.kode ASC", $limit) ;
      $dba      = $this->select("karyawan c", "c.id", $where) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function loadgrid2($va){
      $where 	 = array() ; 
      $where[]	= "c.nip LIKE '{$va['kode']}'" ;
      $where 	 = implode(" AND ", $where) ;
      $field = "c.nip,c.tgl,j.keterangan as golongan";
      $join = "left join absensi_golongan j on j.kode = c.golongan";
      $dbd      = $this->select("absensi_karyawan_golongan c", $field, $where, $join, "", "c.tgl desc") ;
      $dba      = $this->select("absensi_karyawan_golongan c", "c.id", $where) ;

      return array("db"=>$dbd, "rows"=> $this->rows($dba) ) ;
   }

   public function getdata($kode){
      $data = array() ;
		if($d = $this->getval("*", "kode = " . $this->escape($kode), "karyawan")){
         $data = $d;
		}
		return $data ;
   }

   public function deleting($va){
      $return = "ok";
      $va['tgl'] = date_2s($va['tgl']);
      $this->delete("absensi_karyawan_golongan", "nip = '{$va['nip']}' and tgl = '{$va['tgl']}'") ;
      
      return $return;
      
   }

   public function seekperiode($search){
      $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%')" ;
      $dbd      = $this->select("sys_periode_payroll", "*", $where, "", "", "kode desc", '50') ;
      return array("db"=>$dbd) ;
   }

   public function seekkodeabsensim($search){
      $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%') and jenis = 'M'" ;
      $dbd      = $this->select("absensi_kode", "*", $where, "", "", "kode desc", '50') ;
      return array("db"=>$dbd) ;
   }

   public function seekkodeabsensip($search){
      $where = "(kode LIKE '%{$search}%' OR keterangan LIKE '%{$search}%') and jenis = 'P'" ;
      $dbd      = $this->select("absensi_kode", "*", $where, "", "", "kode desc", '50') ;
      return array("db"=>$dbd) ;
   }

   public function getperiode($va){
      $where = "kode = '{$va['periode']}'" ;
      $dbd      = $this->select("sys_periode_payroll", "*", $where, "", "", "kode desc", '50') ;
      return array("db"=>$dbd) ;
   }

}
?>
