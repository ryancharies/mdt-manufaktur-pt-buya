<?php

class Rptpokartu_m extends Bismillah_Model{

    public function loaddetailpo($faktur){
        $field = "d.stock,s.keterangan as namastock,d.spesifikasi,d.harga,d.qty,s.satuan,d.jumlah";
        $where = "d.faktur = '$faktur' and t.status = '1'";
        $join  = "left join po_total t on t.faktur = d.faktur left join stock s on s.kode = d.stock";
        $dbd   = $this->select("po_detail d", $field, $where, $join) ;
        return $dbd ;
    }

    public function loaddetailpbbypo($fakturpo,$tgl){
        $tgl = date_2s($tgl);
        $field = "d.stock,d.qty,s.keterangan as namastock,s.satuan";
        $where = "t.fktpo = '$fakturpo' and t.status = '1' and t.tgl <= '$tgl'";
        $join  = "left join pembelian_total t on t.faktur = d.faktur left join stock s on s.kode = d.stock ";
        $dbd   = $this->select("pembelian_detail d", $field, $where, $join) ;
        return $dbd ;
    }

    public function loadgrid($faktur){
        $field = "d.stock,s.keterangan as namastock,d.spesifikasi,d.harga,d.qty,s.satuan,d.jumlah";
        $where = "d.faktur = '$faktur'";
        $join  = "left join stock s on s.kode = d.stock";
        $dbd   = $this->select("po_detail d", $field, $where, $join) ;
        return $dbd ;
    }

    public function getdata($kode){
        $data = array() ;
        if($d = $this->getval("*", "kode = " . $this->escape($kode), "stock")){
            $data = $d;
        }
        return $data ;
    }
    

    public function getdatatotalpo($va){
        $data  = array() ;
        $va['tgl'] = date_2s($va['tgl']);
        $field = "t.faktur,t.tgl,t.gudang,t.supplier,t.fktpr,g.keterangan as ketgudang,t.total,s.nama as namasupplier";
        $where = "t.faktur = '{$va['faktur']}' and t.tgl <= '{$va['tgl']}' and t.status = '1'";
        $join  = "left join gudang g on g.kode = t.gudang left join supplier s on s.kode = t.supplier";
        $dbd   = $this->select("po_total t", $field, $where, $join) ;
        if($dbr = $this->getrow($dbd)){
            $data = $dbr;
        }
        return $data ;
    }

    public function loadpokartustock($faktur,$stock,$tgl){
        $tgl = date_2s($tgl);
        $field = "k.faktur,k.keterangan,k.debet,k.kredit,k.username,k.datetime,k.tgl,s.satuan,s.keterangan as namastock";
        $where = "k.fktpo = '$faktur' and k.stock = '$stock' and k.tgl <= '$tgl'";
        $join  = "left join stock s on s.kode = k.stock";
        $dbd   = $this->select("po_kartu k", $field, $where, $join,"","k.tgl asc,k.debet desc") ;
        return $dbd ;
    }
}
