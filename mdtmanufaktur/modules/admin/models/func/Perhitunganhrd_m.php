<?php
class Perhitunganhrd_m extends Bismillah_Model
{

    public function getnilaigajiskalajabatan($payroll,$jabatan,$tglawal,$tglakhir)
    {
        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);
        $return = 0;
        $field = "payroll,tgl,jabatan,nominal";
        $where = "payroll = '$payroll' and jabatan = '$jabatan' and tgl >= '$tglawal' and tgl <= '$tglakhir'";
        $join  = "";
        $dbd   = $this->select("payroll_komponen_skala_jabatan", $field, $where, $join,"","tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $return = $dbr['nominal'];
        }
        return $return;
    }

    public function getjabatan($nip,$tgl)
    {
        $tgl = date_2s($tgl);
        $arrreturn = array("jabatan"=>"","keterangan"=>"");
        $field = "k.jabatan,j.keterangan";
        $where = "k.nip = '$nip' and k.tgl <= '$tgl'";
        $join  = "left join jabatan j on j.kode = k.jabatan";
        $dbd   = $this->select("karyawan_jabatan k", $field, $where, $join,"","k.tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $arrreturn = $dbr;
        }
        return $arrreturn;
    }

    public function getbagian($nip,$tgl)
    {
        $tgl = date_2s($tgl);
        $arrreturn = array("bagian"=>"","keterangan"=>"");
        $field = "k.bagian,j.keterangan";
        $where = "k.nip = '$nip' and k.tgl <= '$tgl'";
        $join  = "left join bagian j on j.kode = k.bagian";
        $dbd   = $this->select("karyawan_bagian k", $field, $where, $join,"","k.tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $arrreturn = $dbr;
        }
        return $arrreturn;
    }

    public function getnilaigajiskalakaryawan($payroll,$nip,$tglawal,$tglakhir)
    {
        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);
        $return = 0;
        $field = "payroll,tgl,nip,nominal";
        $where = "payroll = '$payroll' and nip = '$nip' and tgl >= '$tglawal' and tgl <= '$tglakhir'";
        // if($payroll == '103')echo $where;
        $join  = "";
        $dbd   = $this->select("payroll_komponen_skala_karyawan", $field, $where, $join,"","tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $return = $dbr['nominal'];
        }
        return $return;
    }

    public function getnilaigajiskalalamakerja($payroll,$lama,$tglawal,$tglakhir)
    {
        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);
        $return = 0;
        $field = "payroll,tgl,lama,nominal";
        $where = "payroll = '$payroll' and lama = '$lama' and tgl >= '$tglawal' and tgl <= '$tglakhir'";
        $join  = "";
        $dbd   = $this->select("payroll_komponen_skala_lamakerja", $field, $where, $join,"","tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $return = $dbr['nominal'];
        }
        return $return;
    }

    public function getprosentasegajiskalathr($payroll,$lama,$tgl)
    {
        $tgl = date_2s($tgl);
        $return = 0;
        $field = "payroll,tgl,lama,prosentase";
        $where = "payroll = '$payroll' and lama = '$lama' and tgl <= '$tgl'";
        $join  = "";
        $dbd   = $this->select("payroll_komponen_skala_thr", $field, $where, $join,"","tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $return = $dbr['prosentase'];
        }
        return $return;
    }

    public function getthrkaryawan($nip,$tgl){
        $tgl = date_2s($tgl);
        $periode = date("Ym",strtotime($tgl));
        $return = array("total"=>0,"detail"=>array());
        $karyawan = $this->getdatakaryawan($nip,$tgl);
        $dbd = $this->select("payroll_komponen","*","status = '1' and periode = 'B' and perhitungan <> 'K'");
        while($dbr = $this->getrow($dbd)){
            $kodepayroll = $dbr['kode'];
            $pers = $this->getprosentasegajiskalathr($kodepayroll,$karyawan['lamakerjathn'],$tgl);
            
            
            if($pers > 0){
                $payroll = $this->getpayrollkomponenkaryawan($nip,$kodepayroll,$periode);

                $jumlah = $payroll['nominal'] * devide($pers,100) ;
                $return['detail'][$kodepayroll] = array("payroll"=>$kodepayroll,"periode"=>$dbr['periode'],"potongan"=>$dbr['potongan'],
                                                    "perhitungan"=>$dbr['perhitungan'],"diluarjadwal"=>$dbr['diluarjadwal'],
                                                    "pers"=>$pers,"nominal" => $payroll['nominal'],"lamakerja"=>$karyawan['lamakerjathn'],
                                                    "jumlah"=>$jumlah);
                $return['total'] += $jumlah;
            }
            
        }
        
        return $return;
    }

    public function getthrkaryawanposting($nip,$tgl){
        $arrreturn = array("total"=>0,"detail"=>array());
        $field = "k.nip,k.payroll,k.periode,k.potongan,k.perhitungan,k.diluarjadwal,k.nominal,k.prosentase,k.total,k.lamakerja";
        $where = "k.nip = '$nip' and p.tgl = '$tgl' and p.status = '1'";
        $join =  "left join thr_posting p on p.faktur = k.faktur";
        $dbd = $this->select("thr_posting_komponen k",$field,$where,$join);
        while($dbr = $this->getrow($dbd)){
            $arrreturn['detail'][$dbr['payroll']] = array("payroll"=>$dbr['payroll'],"pers"=>$dbr['prosentase'],
                                                        "nominal" => $dbr['nominal'],"lamakerja"=>$dbr['lamakerja'],
                                                        "jumlah"=>$dbr['total']);
            $arrreturn['total'] += $dbr['total'];
        }
        return $arrreturn;
    }

    public function getnilaigajiskalaperiode($payroll,$lama,$tglawal,$tglakhir)
    {
        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);
        $return = 0;
        $field = "payroll,tgl,lama,nominal";
        $where = "payroll = '$payroll' and lama = '$lama' and tgl >= '$tglawal' and tgl <= '$tglakhir'";
        $join  = "";
        $dbd   = $this->select("payroll_komponen_skala_periode", $field, $where, $join,"","tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $return = $dbr['nominal'];
        }
        return $return;
    }

    public function getnilaigajiskalabagian($payroll,$bagian,$tglawal,$tglakhir)
    {
        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);
        $return = 0;
        $field = "payroll,tgl,bagian,nominal";
        $where = "payroll = '$payroll' and bagian = '$bagian' and tgl >= '$tglawal' and tgl <= '$tglakhir'";
        $join  = "";
        $dbd   = $this->select("payroll_komponen_skala_bagian", $field, $where, $join,"","tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $return = $dbr['nominal'];
        }
        return $return;
    }

    public function gettarifasuransikaryawan($nopendaftaran,$tgl)
    {
        $tgl = date_2s($tgl);
        $return = array("karyawan"=>0,"perusahaan"=>0);
        $field = "tgl,karyawan,perusahaan";
        $where = "nopendaftaran = '$nopendaftaran' and tgl <= '$tgl' and status = '1'";
        $join  = "";
        $dbd   = $this->select("asuransi_karyawan_tarif", $field, $where, $join,"","tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $return['karyawan'] = $dbr['karyawan'];
            $return['perusahaan'] = $dbr['perusahaan'];
        }
        return $return;
    }
    public function getnilaiasuransipayroll($nip,$payroll,$tgl){
        $tgl = date_2s($tgl);
        $return = array("karyawan"=>0,"perusahaan"=>0);
        $field = "k.nopendaftaran,k.noasuransi,k.asuransi";
        $where = "k.karyawan = '$nip' and k.tgl <= '$tgl' and status = '1' and a.payroll = '$payroll'";
        $join  = "left join asuransi a on a.kode = k.asuransi";
        $dbd   = $this->select("asuransi_karyawan k", $field, $where, $join,"","tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $asuransi = $this->gettarifasuransikaryawan($dbr['nopendaftaran'],$tgl);
            $return['karyawan'] += $asuransi['karyawan'];
            $return['perusahaan'] += $asuransi['perusahaan'];
        }
        return $return;
    }

    public function getdetailasuransipayroll($nip,$payroll,$tgl){
        $tgl = date_2s($tgl);
        $return = array();
        $field = "k.nopendaftaran,k.noasuransi,k.asuransi";
        $where = "k.karyawan = '$nip' and k.tgl <= '$tgl' and k.status = '1' and a.payroll = '$payroll'";
        $join  = "left join asuransi a on a.kode = k.asuransi";
        $dbd   = $this->select("asuransi_karyawan k", $field, $where, $join,"","tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $asuransi = $this->gettarifasuransikaryawan($dbr['nopendaftaran'],$tgl);
            $return[$dbr['nopendaftaran']] = array("nopendaftaran"=>$dbr['nopendaftaran'],"noasuransi"=>$dbr['noasuransi'],"asuransi"=>$dbr['asuransi'],
                    "tarifkaryawan"=>$asuransi['karyawan']);
        }
        return $return;
    }

    public function getjadwal($tgl,$plafond,$lama){
        $tgl = date_2s($tgl);
        $plafond = string_2n($plafond);
        $lama = string_2n($lama);
        $return = array();
        for($n=1;$n<=$lama;$n++){   
            $tgl = date("Y-m-d", nextmonth(strtotime($tgl), $n - 1));
            $agspokok = round(devide($plafond,$lama));
            if($n == $lama){
                $jmlagssebelumnya = $agspokok * ($lama-1);
                $agspokok = $plafond - $jmlagssebelumnya;
            }
            $return[] = array("ke"=>$n,"jthtmp"=>$tgl,"agspokok" => $agspokok);
        }
        return $return;
    }

    public function getagspokok($plafond,$ke,$lama){
       
        $plafond = string_2n($plafond);
        $lama = string_2n($lama);
        $ke = string_2n($ke);

        $return = 0;
        
        $agspokok = 0 ;
        
        if($ke <= $lama){
            $agspokok = round(devide($plafond,$lama));
        }
        //echo $ke ."||" . $lama . " -- ";
        if($ke == $lama){
            $jmlagssebelumnya = $agspokok * ($lama-1);
            $agspokok = $plafond - $jmlagssebelumnya;
        }

        $return = $agspokok;
        return $return;
    }

    public function getsaldopinjamankaryawan($nopinjaman,$tgl){
        $tgl = date_2s($tgl);
        $return = 0;
        $field = "ifnull(sum(debet-kredit),0) as saldo";
        $where = "nopinjaman = '$nopinjaman' and tgl <= '$tgl'";
        $join  = "";
        $dbd   = $this->select("pinjaman_karyawan_kartu", $field, $where, $join,"","tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $return = $dbr['saldo'];
        }
        return $return;
    }

    public function getgolabsensi($nip,$tgl)
    {
        $tgl = date_2s($tgl);
        $arrreturn = array("golongan"=>"","keterangan"=>"");
        $field = "k.golongan,j.keterangan";
        $where = "k.nip = '$nip' and k.tgl <= '$tgl'";
        $join  = "left join absensi_golongan j on j.kode = k.golongan";
        $dbd   = $this->select("absensi_karyawan_golongan k", $field, $where, $join,"","k.tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $arrreturn = $dbr;
        }
        return $arrreturn;
    }

    public function getjwabsensibygol($golongan,$tgl)
    {
        $tgl = date_2s($tgl);
        $arrreturn = array("jadwal"=>"","keterangan"=>"","mulaihari"=>1,"tglberlaku"=>"","siklus"=>"");
        $field = "k.jadwal,j.keterangan,k.mulaihari,k.tgl as tglberlaku,j.siklus";
        $where = "k.golongan = '$golongan' and k.tgl <= '$tgl'";
        $join  = "left join absensi_jadwal j on j.kode = k.jadwal";
        $dbd   = $this->select("absensi_golongan_jadwal k", $field, $where, $join,"","k.tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $arrreturn = $dbr;
        }
        return $arrreturn;
    }

    public function getjwabsensibynohari($jadwal,$nohari)
    {
        $arrreturn = array("masuk"=>"--:--","pulang"=>"--:--","libur"=>"","siklus"=>"",
                            "toleransi"=>0,"mulailembur"=>0,"maxlembur"=>0,
                            "bukaabsenmasuk"=>0,"tutupabsenmasuk"=>0,"bukaabsenpulang"=>0,"tutupabsenpulang"=>0,
                            "keterangan"=>"");
        $field = "j.keterangan,j.siklus,j.toleransi,j.mulailembur,j.maxlembur,j.bukaabsenmasuk,j.tutupabsenmasuk,
                j.bukaabsenpulang,j.tutupabsenpulang,d.masuk,d.pulang,d.libur";
        $where = "j.kode = '$jadwal' and d.nohari = '$nohari'";
        $join  = "left join absensi_jadwal j on j.kode = d.kode";
        $dbd   = $this->select("absensi_jadwal_detail d", $field, $where, $join,"","","") ;
        if($dbr = $this->getrow($dbd)){
            
            $arrreturn = $dbr;
            if($dbr['libur']){
                $arrreturn['masuk'] = "Libur";
                $arrreturn['pulang'] = "Libur";
                $arrreturn['toleransi'] = "Libur";
            }
        }
        return $arrreturn;
    }


    public function getke($datetimeawal, $datetimeakhir,$type = "B")
    {
        //echo $datetimeawal ."||". $datetimeakhir;
        $ndatetimeakhir = date_2t($datetimeakhir);
        $njthtmp = date_2t($datetimeawal);
        $ndatetimeawal = date_2t($datetimeawal);
        $ke = 0;
        $n = 0 ;
       while ($njthtmp < $ndatetimeakhir) {
            if($type == "J"){ //  jam
                $n++;
                $njthtmp = strtotime('+'.$n.' hour', strtotime($datetimeawal));//$ndatetimeawal + ($n * 60*60);
            }else if($type == "H"){ // hari
                $n++;
                $njthtmp = strtotime('+'.$n.' day', strtotime($datetimeawal));//$ndatetimeawal + ($n * 24*60*60);
            }else if($type == "B"){ // bulan
                $n++;
                $njthtmp = strtotime('+'.$n.' month', strtotime($datetimeawal));//nextmonth($ndatetimeawal, $n);
            }else if($type == "T"){ // tahun
                $n++;
                $njthtmp = strtotime('+'.$n.' year', strtotime($datetimeawal));
            }
            $ke++;            
        }

        return $ke;
    }

    public function getjthtmpbyperiode($datetimeawal,$lama=0,$type="J"){
        $return = strtotime($datetimeawal);
        
        if($type == "J"){ //  jam
            $return = strtotime('+'.$lama.' hour', strtotime($datetimeawal));//$ndatetimeawal + ($n * 60*60);
        }else if($type == "H"){ // hari
            $return = strtotime('+'.$lama.' day', strtotime($datetimeawal));//$ndatetimeawal + ($n * 24*60*60);
        }else if($type == "B"){ // bulan
            $return = strtotime('+'.$lama.' month', strtotime($datetimeawal));//nextmonth($ndatetimeawal, $n);
        }else if($type == "T"){ // tahun
            $return = strtotime('+'.$lama.' year', strtotime($datetimeawal));
        }
        return $return;
    }

    public function gettunggakanpinjamankaryawan($nopinjaman,$tglrealisasi,$tglagsawal,$tgl,$plafond,$lama){
        $tgl = date_2s($tgl);
        $tglagsawal = date_2s($tglagsawal);
        $tglrealisasi = date_2s($tglrealisasi);

        $return = array("jmlkewajiban"=>0,"tpokok"=>0); 
        $ke = $this->getke($tglagsawal,$tgl);
        

        //menghitung kewajiban
        $jmlkewajiban = 0 ;
        for($i=1;$i<=$ke;$i++){
            $pokok = $this->getagspokok($plafond,$i,$lama);
            $jmlkewajiban += $pokok;
        }
        $return['jmlkewajiban'] = $jmlkewajiban;


        //menghitung jml angsuran
        $jmlangsuran = 0 ;
        $field = "ifnull(sum(kredit),0) as angsuran";
        $where = "nopinjaman = '$nopinjaman' and tgl <= '$tgl'";
        $join  = "";
        $dbd   = $this->select("pinjaman_karyawan_kartu", $field, $where, $join,"","tgl desc","1") ;
        if($dbr = $this->getrow($dbd)){
            $jmlangsuran = $dbr['angsuran'];
        }

        $return['tpokok'] = $jmlkewajiban - $jmlangsuran;
        $return['tpokok'] = max($return['tpokok'],0);

        return $return;
    }

    public function getpayrollpotangsuranpinjaman($nip,$periode){
        $return = array("total"=>0,"detail"=>array());
        $field = "a.tgl,a.periode,a.cabang,a.nopinjaman,a.keterangan,a.pokok";
        $where = "a.periode = '$periode' and a.status = '0' and k.nip = '$nip' and k.status = '1'";
		$join  = "left join pinjaman_karyawan k on k.nopinjaman = a.nopinjaman and k.nip = '$nip'";
        $dbd   = $this->select("payroll_pinjaman_karyawan_angsuran a",$field,$where,$join);
        
        while($dbr = $this->getrow($dbd)){
            $return['detail'][] = array("tgl"=>$dbr['tgl'],"periode"=>$dbr['periode'],"cabang"=>$dbr['cabang'],
                                        "nopinjaman"=>$dbr['nopinjaman'],"keterangan"=>$dbr['keterangan'],"pokok"=>$dbr['pokok']);
            $return['total'] += $dbr['pokok'];     
        }
        return $return;
    }

    public function getpayrollkpi($nip,$periode){
        $return = array("total"=>0,"detail"=>array());
        $field = "a.peringkat,a.nominal";
        $where = "a.nip = '$nip' and a.periode = '$periode' and a.status = '1' and a.posting = '1'";
		$join  = "";
        $dbd   = $this->select("kpi_nilai_total a",$field,$where,$join);
        
        if($dbr = $this->getrow($dbd)){
            // $return['detail'][] = array("tgl"=>$dbr['tgl'],"periode"=>$dbr['periode'],"cabang"=>$dbr['cabang'],
            //                             "nopinjaman"=>$dbr['nopinjaman'],"keterangan"=>$dbr['keterangan'],"pokok"=>$dbr['pokok']);
            $return['total'] = $dbr['nominal'];     
        }
        return $return;
    }

    public function getpayrollpotangsuranpinjamanposting($nip,$periode){
        $return = array("total"=>0,"detail"=>array());
        $field = "k.nip,k.nopinjaman,k.pokok,p.periode,p.cabang,a.keterangan,p.tgl";
        $where = "p.periode = '$periode' and p.status = '1' and k.nip = '$nip'";
        $join = "left join payroll_posting p on p.faktur = k.faktur
                left join payroll_pinjaman_karyawan_angsuran a on a.nopinjaman = k.nopinjaman and 
                a.faktur = k.faktur and a.status = '1'";
        $dbd   = $this->select("payroll_posting_pinjaman_karyawan k",$field,$where,$join);
        while($dbr = $this->getrow($dbd)){
            $return['detail'][] = array("tgl"=>$dbr['tgl'],"periode"=>$dbr['periode'],"cabang"=>$dbr['cabang'],
                                        "nopinjaman"=>$dbr['nopinjaman'],"keterangan"=>$dbr['keterangan'],"pokok"=>$dbr['pokok']);
            $return['total'] += $dbr['pokok'];     
        }
        return $return;
    }

    public function getdatakaryawanbypinabsensi($pin,$mesin){
        $return = array("nip"=>"","nama"=>"","alamat"=>"");
        $field = "p.pin,p.nip,p.mesin,k.nama,k.alamat";
        $where = "p.pin = '$pin' and p.mesin = '$mesin'";
        $join = "left join karyawan k on k.kode = p.nip";
        $dbd   = $this->select("absensi_karyawan_pin p", $field, $where, $join,"","","") ;
        if($dbr = $this->getrow($dbd)){
            $return['nip'] = $dbr['nip'];
            $return['nama'] = $dbr['nama'];
            $return['alamat'] = $dbr['alamat'];
        }
        return $return;
    }


    public function getdatakaryawan($nip,$tgl){
        $return = array("nip"=>"","nama"=>"","alamat"=>"","lamakerjathn"=>0,"kodejabatan"=>"","ketjabatan"=>"",
                "kodebagian"=>"","ketbagian"=>"","ktp"=>"","telepon"=>"");
        $field = "k.kode,k.tgl,k.nama,k.alamat,k.ktp,k.telepon";
        $where = "k.kode = '$nip'";
        $join = "";
        $dbd   = $this->select("karyawan k", $field, $where, $join,"","","") ;
        if($dbr = $this->getrow($dbd)){
            $return['nip'] = $dbr['kode'];
            $return['ktp'] = $dbr['ktp'];
            $return['telepon'] = $dbr['telepon'];
            $return['nama'] = $dbr['nama'];
            $return['alamat'] = $dbr['alamat'];
            $return['lamakerjathn'] = $this->getke($dbr['tgl'], $tgl,"T");
            
            $jabatan = $this->getjabatan($nip,$tgl);
            $return['kodejabatan'] = $jabatan['jabatan'];
            $return['ketjabatan'] = $jabatan['keterangan'];

            $bagian = $this->getbagian($nip,$tgl);
            $return['kodebagian'] = $bagian['bagian'];
            $return['ketbagian'] = $bagian['keterangan'];

        }
        return $return;
    }

    public function getjwabsensi($nip,$tgl){
        $return = array("jwmasuk"=>"None","jwpulang"=>"None","jwtoleransi"=>"0","golongan"=>"","ketgolongan"=>"",
                        "jadwal"=>"","ketjadwal"=>"","mulailembur"=>0,"maxlembur"=>0,"libur"=>"",
                        "bukaabsenmasuk"=>0,"tutupabsenmasuk"=>0,"bukaabsenpulang"=>0,"tutupabsenpulang"=>0);
        $tgl = date_2s($tgl);
        // melihat gol absensi 
        $arrgol = $this->getgolabsensi($nip,$tgl);
        
        $return['golongan'] = $arrgol['golongan'];
        $return['ketgolongan'] = $arrgol['keterangan'];

        //melihat kode jadwal
        if($arrgol['golongan'] <> ""){
            $arrjw = $this->getjwabsensibygol($arrgol['golongan'],$tgl);
            $return['jadwal'] = $arrjw['jadwal'];
            $return['ketjadwal'] = $arrjw['keterangan'];

            //melihat jadwal tgl tsb
            if($arrjw['tglberlaku'] <> "" && $arrjw['mulaihari'] <> ""){
                $date2 = strtotime($tgl);
                $date1 = strtotime($arrjw['tglberlaku']);
                $diff = abs($date2 - $date1);
                $jmldiffhari = devide($diff,86400);//86400 jml time dlm 1 hari untuk melihat jml hari sd skrg
                $nohari = $jmldiffhari % $arrjw['siklus'];
                if($nohari == 0 ) $nohari = $arrjw['siklus'];
                $arrjwhari = $this->getjwabsensibynohari($arrjw['jadwal'],$nohari);
                $return['jwmasuk'] = $arrjwhari['masuk'];
                $return['jwpulang'] = $arrjwhari['pulang'];
                $return['jwtoleransi'] = $arrjwhari['toleransi'];
                $return['libur'] = $arrjwhari['libur'];

                $return['mulailembur'] = $arrjwhari['mulailembur'];
                $return['maxlembur']   = $arrjwhari['maxlembur'];
                $return['bukaabsenmasuk'] = $arrjwhari['bukaabsenmasuk'];
                $return['tutupabsenmasuk'] = $arrjwhari['tutupabsenmasuk'];
                $return['bukaabsenpulang'] = $arrjwhari['bukaabsenpulang'];
                $return['tutupabsenpulang'] = $arrjwhari['tutupabsenpulang'];
            }

            
        }

       
        
        return $return;
    }

    public function getabsensi($nip,$tgl){
        $return = array("wktmasuk"=>"--:--:--","wktpulang"=>"--:--:--","kodeabsenmasuk"=>"","ketkodeabsenmasuk"=>"","kodeabsenpulang"=>"",
                        "terlambat"=>"--:--:--","plgcepat"=>"--:--:--","ketkodeabsenpulang"=>"");
        $tgl = date_2s($tgl);
        $field = "a.absensi,a.mode,a.kodeabsensi,k.keterangan as ketkodeabsensi,a.selisihabsensi";
        $join = "left join absensi_kode k on k.kode = a.kodeabsensi";
        $where = "a.nip = '$nip' and a.tglfinger = '$tgl' and a.status = '1'";
        $dbd = $this->select("absensi_karyawan a",$field,$where,$join);
        while($dbr = $this->getrow($dbd)){
            //print_r($dbr);
            if($dbr['mode'] == "1"){
                $return['wktmasuk'] = date("H:i:s",strtotime($dbr['absensi']));
                $return['kodeabsenmasuk'] = $dbr['kodeabsensi'];
                $return['ketkodeabsenmasuk'] = $dbr['ketkodeabsensi'];
                $return['terlambat'] = $dbr['selisihabsensi'];
            }else if($dbr['mode'] == "2"){
                $return['wktpulang'] = date("H:i:s",strtotime($dbr['absensi']));
                $return['kodeabsenpulang'] = $dbr['kodeabsensi'];
                $return['ketkodeabsenpulang'] = $dbr['ketkodeabsensi'];
                $return['plgcepat'] = $dbr['selisihabsensi'];

            }
        }
        return $return;
    }

    public function getperiode($periode){
        $arrreturn = array("tglawal"=>"0000-00-00","tglakhir"=>"0000-00-00");
        $where = "kode = '$periode' and status = '1'" ;
        $dbd      = $this->select("sys_periode_payroll", "*", $where, "", "", "kode desc", '1') ;
        if($dbr = $this->getrow($dbd)){
            $arrreturn['tglawal'] = $dbr['tglawal'];
            $arrreturn['tglakhir'] = $dbr['tglakhir'];
        }   
        return $arrreturn;
    }

    public function getlembur($nip,$tglawal,$tglakhir){
        $return = array();
        // $return = array("datetimeawal"=>"0000-00-00 00:00:00","datetimeakhir"=>"0000-00-00 00:00:00","nolembur"=>"","keterangan"=>"",
        //                 "metode"=>"","tgl"=>"0000-00-00","totlemburh"=>0);
        $datetimeawal = $tglawal." 00:00:00";
        $datetimeakhir = $tglakhir." 23:59:59";
        $field = "k.datetimeawal,k.datetimeakhir,k.nolembur,k.keterangan,l.metode,l.tgl";
        $join = "left join lembur l on l.nolembur = k.nolembur";
        //$where = "k.nip = '$nip' and k.datetimeawal >= '$datetimeawal' and k.datetimeawal <= '$datetimeakhir' and l.status = '1' and l.tgl >= '$tglawal' and l.tgl <= '$tglakhir'";
        $where = "k.nip = '$nip' and l.status = '1' and l.tgl >= '$tglawal' and l.tgl <= '$tglakhir'";
       // echo $where;
        $tottimelembur = 0;
        $dbd = $this->select("lembur_karyawan k",$field,$where,$join);
        while($dbr = $this->getrow($dbd)){
            $arr = array();
            $arr['datetimeawal'] = $dbr['datetimeawal'];
            $arr['datetimeakhir'] = $dbr['datetimeakhir'];
            $arr['nolembur'] = $dbr['nolembur'];
            $arr['keterangan'] = $dbr['keterangan'];
            $arr['metode'] = $dbr['metode'];
            $arr['tgl'] = $dbr['tgl'];
            $timelembur = strtotime($dbr['datetimeakhir']) - strtotime($dbr['datetimeawal']);
            $tottimelembur += $timelembur; 

            $totlemburh = devide($timelembur,3600);
            $arr['totlemburh'] = intval($totlemburh);
            $arr['timelembur'] = $timelembur;
            $return[$dbr['nolembur']] = $arr;

            /*$totlemburh = devide($timelembur,3600);
            $arr['totlemburh'] = intval($totlemburh);
            $return[$dbr['nolembur']] = $arr;*/
        }

        // $totlemburh = devide($tottimelembur,3600);
        // $jamlembur = intval($totlemburh);
        // $totlemburmenit = devide($tottimelembur - ($jamlembur * 3600),60);
        // $lemburmenit = intval($totlemburmenit);
        // if($lemburmenit >= 50){
        //     $totlemburh += 1;
        // }else if($lemburmenit >= 30){
        //     $totlemburh += 0.5;
        // }
        // //khusus buya


        return $return;
    }

    public function getlemburdetail($nip,$tgl){
        $tgl = date_2s($tgl);
        $arr = array("datetimeawal"=>"0000-00-00 00:00:00","datetimeakhir"=>"0000-00-00 00:00:00","nolembur"=>"","keterangan"=>"",
                    "metode"=>"","tgl"=>"0000-00-00","totlemburh"=>0);
        $field = "k.datetimeawal,k.datetimeakhir,k.nolembur,k.keterangan,l.metode,l.tgl";
        $join = "left join lembur l on l.nolembur = k.nolembur";
        $where = "k.nip = '$nip' and l.tgl = '$tgl' and l.status = '1'";
       // echo $where;
        $dbd = $this->select("lembur_karyawan k",$field,$where,$join);
        if($dbr = $this->getrow($dbd)){
            
            $arr['datetimeawal'] = $dbr['datetimeawal'];
            $arr['datetimeakhir'] = $dbr['datetimeakhir'];
            $arr['nolembur'] = $dbr['nolembur'];
            $arr['keterangan'] = $dbr['keterangan'];
            $arr['metode'] = $dbr['metode'];
            $arr['tgl'] = $dbr['tgl'];
            $timelembur = strtotime($dbr['datetimeakhir']) - strtotime($dbr['datetimeawal']);
            $totlemburh = devide($timelembur,3600);
            $arr['totlemburh'] = intval($totlemburh);
            //$return[$dbr['nolembur']] = $arr;
        }
        return $arr;
    }

    public function getpayrollkomponenkaryawan($nip,$payroll,$periode){
        
        $arrreturn = array("nominal"=>0,"jmlperiode"=>0,"total"=>0,"periode"=>"","jmlskala"=>0,"perhitungan"=>"");
        $periode2 = $this->getperiode($periode);
        $tglawalperiode = date_2s($periode2['tglawal']);
        $tglakhir = date_2s($periode2['tglakhir']);
        $karyawan = $this->getdatakaryawan($nip,$tglakhir);
        
       // print_r($karyawan);
        if($karyawan['nip'] <> ""){

            //cek komponen
            $dbd = $this->select("payroll_komponen","*","kode = '$payroll'");
            if($dbr = $this->getrow($dbd)){
                $arrreturn['periode'] = $dbr['periode'];
                $arrreturn['perhitungan'] = $dbr['perhitungan'];
                if($dbr['resetperiode'] <> '1'){
                    $tglawal = "0000-00-00";
                }else{
                    $tglawal = $tglawalperiode;
                }

                //cek periode
                if($dbr['periode'] == "B"){
                    $arrreturn['jmlperiode'] = 1;
                    $arrreturn['jmlskala'] = $arrreturn['jmlperiode'];
                }else if($dbr['periode'] == "H"){
                    // melihat kode absensi yang digunakan pada payroll tsb
                    $arrwhere2 = array();
                    $dbd1 = $this->select("payroll_komponen_absensi","absensi","payroll = '$payroll'");
                    while($dbr1 = $this->getrow($dbd1)){
                        $arrwhere2[] = "kodeabsensi = '{$dbr1['absensi']}'";
                    }

                    $where2 = "tglfinger >= '{$periode2['tglawal']}' and tglfinger <= '{$periode2['tglakhir']}' 
                                and status = '1' and mode = '1' and nip = '$nip' and (".implode(" or ",$arrwhere2).")";
                    //cek absensi
                    $dbd2 = $this->select("absensi_karyawan","ifnull(count(id),0) as jml",$where2);
                    if($dbr2 = $this->getrow($dbd2)){
                        $arrreturn['jmlperiode'] = $dbr2['jml'] - $dbr['periodemulai'];
                    }
                    $arrreturn['jmlskala'] = $arrreturn['jmlperiode'];
                }else if($dbr['periode'] == "J"){
                    if($dbr['diluarjadwal'] == '1'){
                        //cek ke tabel lembur
                        $arrlembur = $this->getlembur($nip,$periode2['tglawal'],$periode2['tglakhir']);
                        
                        if($dbr['kalkulasi'] == 0){ // bukan kalkulasi
                            //print_r($arrlembur);
                            foreach($arrlembur as $key => $val){
                                $arrreturn['jmlperiode'] += $val['totlemburh'] - $dbr['periodemulai'];
                                $periodskala = devide($val['totlemburh'],$dbr['stepskala']);
                                
                                //echo "isi".$periodskala."__".$dbr['stepskala']."++".$val['totlemburh']."==";
                                $periodskala = intval($periodskala);
                                $arrreturn['jmlskala'] += $periodskala;
                            }
                        }else if($dbr['kalkulasi'] == 1){//kalkulasi
                            $tottimelembur = 0;
                            foreach($arrlembur as $key => $val){
                                if(($dbr['periodemulai']*60) <= $val['timelembur']){
                                    $tottimelembur += $val['timelembur'];
                                }
                                
                            }

                            $totlemburh = devide($tottimelembur,3600);
                            
                            //khusus buya
                            $jamlembur = intval($totlemburh);
                            $totlemburmenit = devide($tottimelembur - ($jamlembur * 3600),60);
                            $lemburmenit = intval($totlemburmenit);
                            if($lemburmenit >= 50){
                                $jamlembur += 1;
                            }else if($lemburmenit >= 30){
                                $jamlembur += 0.5;
                            }

                            $arrreturn['jmlperiode'] = $jamlembur;
                            $periodskala = devide($jamlembur,$dbr['stepskala']);
                            
                            $arrreturn['jmlskala'] = $periodskala;
                        }
                    }else{
                        
                        if($dbr['potterlambat'] == '1'){ // pot terlambat
                            //cek absensi
                            $periodemulai = $dbr['periodemulai'];
                            $jmlterlambat = 0 ;
                            $where2 = "tglfinger >= '{$periode2['tglawal']}' and tglfinger <= '{$periode2['tglakhir']}' 
                                    and status = '1' and mode = '1' and nip = '$nip' and selisihabsensidtk > toleransidtk";
                            $field2 = "ifnull(sum(if(ceil((selisihabsensidtk-toleransidtk)/3600) < $periodemulai,$periodemulai,ceil((selisihabsensidtk-toleransidtk)/3600)) - $periodemulai),0) as jml";
                            $dbd2 = $this->select("absensi_karyawan",$field2,$where2); //SUBSTR(selisihabsensi, 1, 2)
                            if($dbr2 = $this->getrow($dbd2)){
                                $jmlterlambat = $dbr2['jml'];
                                $arrreturn['jmlperiode'] += $dbr2['jml'];
                            }
                            $arrreturn['jmlskala'] += $jmlterlambat;
                        }
                        
                        if($dbr['potplgcepat'] == '1'){ // pot plgcepat
                            //cek absensi
                            $periodemulai = $dbr['periodemulai'];
                            $jmlplgcepat = 0 ;
                            $where2 = "tglfinger >= '{$periode2['tglawal']}' and tglfinger <= '{$periode2['tglakhir']}' 
                                    and status = '1' and mode = '2' and nip = '$nip' and selisihabsensidtk > toleransidtk";
                            $field2 = "ifnull(sum(if(floor((selisihabsensidtk-toleransidtk)/3600) < $periodemulai,$periodemulai,floor((selisihabsensidtk-toleransidtk)/3600)) - $periodemulai),0) as jml";
                            $dbd2 = $this->select("absensi_karyawan",$field2,$where2);
                            if($dbr2 = $this->getrow($dbd2)){
                                $jmlplgcepat = $dbr2['jml'];
                                $arrreturn['jmlperiode'] += $dbr2['jml'];
                            }
                            $arrreturn['jmlskala'] += $jmlplgcepat;
                        }
                    }
                }

                //cek perhitungan
                if($dbr['perhitungan'] == 'C'){ //custom / perkaryawan
                    $arrreturn['nominal'] = $this->getnilaigajiskalakaryawan($payroll,$nip,$tglawal,$tglakhir);
                    $arrreturn['total'] = $arrreturn['nominal'] * $arrreturn['jmlperiode'];
                }else if($dbr['perhitungan'] == 'L'){ // lama kerja 
                    $lama = $karyawan['lamakerjathn'] ;
                    $arrreturn['nominal'] = $this->getnilaigajiskalalamakerja($payroll,$lama,$tglawal,$tglakhir);
                   // echo "Nominal -> ".$arrreturn['nominal'];
                    $arrreturn['total'] = $arrreturn['nominal'] * $arrreturn['jmlperiode'];
                   // echo "total -> ".$arrreturn['total'];
                }else if($dbr['perhitungan'] == 'J'){ // Jabatan 
                    $jabatan = $karyawan['kodejabatan'];
                    $arrreturn['nominal'] = $this->getnilaigajiskalajabatan($payroll,$jabatan,$tglawal,$tglakhir);
                    $arrreturn['total'] = $arrreturn['nominal'] * $arrreturn['jmlperiode'];
                }else if($dbr['perhitungan'] == 'B'){ // Bagian 
                    $bagian = $karyawan['kodebagian'];
                    $arrreturn['nominal'] = $this->getnilaigajiskalabagian($payroll,$bagian,$tglawal,$tglakhir);
                    $arrreturn['total'] = $arrreturn['nominal'] * $arrreturn['jmlperiode'];
                }else if($dbr['perhitungan'] == 'P'){ // Periode 
                    $lama = $dbr['stepskala'];
                    $arrreturn['nominal'] = $this->getnilaigajiskalaperiode($payroll,$lama,$tglawal,$tglakhir);
                    $arrreturn['total'] = $this->getnilaigajiskalaperiode($payroll,$arrreturn['jmlperiode'],$tglawal,$tglakhir);
                }else if($dbr['perhitungan'] == 'K'){ // khusus 

                    $kodepinjaman = $this->getconfig("kogapypinjkaryawan"); // kasbon
                    $kodekpi = $this->getconfig("kogapykpi"); // kasbon
                    if($payroll == $kodepinjaman && $kodepinjaman <> ""){
                        $kasbon = $this->getpayrollpotangsuranpinjaman($nip,$periode);
                        $arrreturn['nominal'] = $kasbon['total'];
                        $arrreturn['total'] = $kasbon['total'];
                    }else if($payroll == $kodekpi && $kodekpi <> ""){// kpi
                        $kpi = $this->getpayrollkpi($nip,$periode);
                        $arrreturn['nominal'] = $kpi['total'];
                        $arrreturn['total'] = $kpi['total'];
                    }    
                }else if($dbr['perhitungan'] == 'A'){
                    $asuransi = $this->getnilaiasuransipayroll($nip,$payroll,$tglakhir);
                    $arrreturn['nominal'] = $asuransi['karyawan'];
                    $arrreturn['total'] = $arrreturn['nominal'] * $arrreturn['jmlperiode'];
                }
            }
        }
        return $arrreturn;
    }

    public function getpayrollkomponenkaryawanposting($nip,$payroll,$periode){
        $arrreturn = array("nominal"=>0,"jmlperiode"=>0,"total"=>0,"periode"=>"","jmlskala"=>0,"perhitungan"=>"");
        $field = "k.nominal,k.jmlperiode,k.total,k.periode,k.jmlskala,k.perhitungan";
        $where = "k.nip = '$nip' and k.payroll = '$payroll' and p.periode = '$periode' and p.status = '1'";
        $join =  "left join payroll_posting p on p.faktur = k.faktur";
        $dbd = $this->select("payroll_posting_komponen k",$field,$where,$join);
        while($dbr = $this->getrow($dbd)){
            $arrreturn['nominal'] = $dbr['nominal'];
            $arrreturn['jmlperiode'] = $dbr['jmlperiode'];
            $arrreturn['total'] = $dbr['total'];
            $arrreturn['periode'] = $dbr['periode'];
            $arrreturn['jmlskala'] = $dbr['jmlskala'];
            $arrreturn['perhitungan'] = $dbr['perhitungan'];
        }
        return $arrreturn;
    }

    public function getkolombelumposting(){
        $arrkolom = array();
        $arrkolom[] = array("field"=> "kode", "caption"=> "NIP", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "nama", "caption"=> "Nama", "size"=> "200px", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "bagian", "caption"=> "Bagian", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "jabatan", "caption"=> "Jabatan", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        
        $kodepinjaman = $this->getconfig("kogapypinjkaryawan");
        
        $where = "(periode = 'J' or periode = 'H') and status = '1' and kode <> '$kodepinjaman'";
        $dbd = $this->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
        while($dbr = $this->getrow($dbd)){
            $arrkolom[] = array("field"=> "hk_".$dbr['kode'], "caption"=> "HK. ".$dbr['keterangan'], "size"=> "70px", "render"=> "float:2", "sortable"=>false);    
        }
        
       // $kodepinjaman = $this->trpayroll_m->getconfig("kogapypinjkaryawan");
        $where = "(periode = 'J' or periode = 'H') and status = '1' and kode <> '$kodepinjaman'";
        $dbd = $this->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
        while($dbr = $this->getrow($dbd)){
            $arrkolom[] = array("field"=> "uh_".$dbr['kode'], "caption"=> "UH. ".$dbr['keterangan'], "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        }

        $where = "(periode = 'J' or periode = 'H' or periode = 'B') and status = '1' and kode <> '$kodepinjaman'";
        $dbd = $this->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
        while($dbr = $this->getrow($dbd)){
            $arrkolom[] = array("field"=> "ub_".$dbr['kode'], "caption"=> "UB. ".$dbr['keterangan'], "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        }

        $arrkolom[] = array("field"=> "gajikotor", "caption"=> "Upah Kotor", "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        $arrkolom[] = array("field"=> "pinjamankaryawan", "caption"=> "BON", "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        $arrkolom[] = array("field"=> "gajibersih", "caption"=> "Upah Bersih", "size"=> "100px", "render"=> "float:2", "sortable"=>false);

        return $arrkolom;
    }

    public function getkolomsudahposting($periode){
        $arrkolom = array();
        $arrkolom[] = array("field"=> "kode", "caption"=> "NIP", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "nama", "caption"=> "Nama", "size"=> "200px", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "bagian", "caption"=> "Bagian", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "jabatan", "caption"=> "Jabatan", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        
        $kodepinjaman = $this->getconfig("kogapypinjkaryawan");
        $where = "(k.periode = 'J' or k.periode = 'H') and k.kode <> '$kodepinjaman' and t.status = '1' and t.periode = '$periode'";
        $join = "left join payroll_komponen k on k.kode = p.payroll left join payroll_posting t on t.faktur = p.faktur";
        $dbd = $this->select("payroll_posting_komponen p","k.kode,k.keterangan,k.periode,k.perhitungan,k.potongan",$where,$join,"k.kode");
        while($dbr = $this->getrow($dbd)){
            $arrkolom[] = array("field"=> "hk_".$dbr['kode'], "caption"=> "HK. ".$dbr['keterangan'], "size"=> "70px", "render"=> "float:2", "sortable"=>false);    
        }

        $where = "(k.periode = 'J' or k.periode = 'H') and k.kode <> '$kodepinjaman' and t.status = '1' and t.periode = '$periode'";
        $join = "left join payroll_komponen k on k.kode = p.payroll left join payroll_posting t on t.faktur = p.faktur";
        $dbd = $this->select("payroll_posting_komponen p","k.kode,k.keterangan,k.periode,k.perhitungan,k.potongan",$where,$join,"k.kode");
        while($dbr = $this->getrow($dbd)){
            $arrkolom[] = array("field"=> "uh_".$dbr['kode'], "caption"=> "UH. ".$dbr['keterangan'], "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        }

        $where = "(k.periode = 'J' or k.periode = 'H' or k.periode = 'B') and t.status = '1' and t.periode = '$periode' and p.payroll <> '$kodepinjaman'";
        $join = "left join payroll_komponen k on k.kode = p.payroll left join payroll_posting t on t.faktur = p.faktur";
        $dbd = $this->select("payroll_posting_komponen p","k.kode,k.keterangan,k.periode,k.perhitungan,k.potongan",$where,$join,"k.kode");
        while($dbr = $this->getrow($dbd)){
            $arrkolom[] = array("field"=> "ub_".$dbr['kode'], "caption"=> "UB. ".$dbr['keterangan'], "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        }

        $arrkolom[] = array("field"=> "gajikotor", "caption"=> "Upah Kotor", "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        $arrkolom[] = array("field"=> "pinjamankaryawan", "caption"=> "BON", "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        $arrkolom[] = array("field"=> "gajibersih", "caption"=> "Upah Bersih", "size"=> "100px", "render"=> "float:2", "sortable"=>false);

        return $arrkolom;
    }

    /**KPI */

    public function getbobotkpiindikator($kode,$jabatan,$bagian,$tgl){
        $tgl = date_2s($tgl);
        $return = 0;
        $field = "bobot";
        $where = "indikator = '$kode' and jabatan = '$jabatan' and bagian = '$bagian' and tgl <= '$tgl'";
        $dbd = $this->select("kpi_indikator_bobot",$field,$where,'','','tgl desc',1);
        while($dbr = $this->getrow($dbd)){
            $return = $dbr['bobot'];
        }
        return $return;
    }

    public function getkpikaryawan($nip,$tglawal,$tglakhir,$perhitungan){
        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);
        $return = array("target"=>0,"pencapaian"=>0);
        if($nip <> ""){
            if($perhitungan == "1"){//Masuk Tepat Waktu
                $jmlmasuk = 0 ; 
                $where2 = "tglfinger >= '$tglawal' and tglfinger <= '$tglakhir' 
                                and status = '1' and mode = '1' and nip = '$nip'";
                
                $dbd2 = $this->select("absensi_karyawan","ifnull(count(id),0) as jml",$where2);
                if($dbr2 = $this->getrow($dbd2)){
                    $jmlmasuk = $dbr2['jml'];
                }

                //cek data terlambat
                $jmlterlambat = 0 ;
                $where2 = "tglfinger >= '$tglawal' and tglfinger <= '$tglakhir' 
                        and status = '1' and mode = '1' and nip = '$nip' and selisihabsensidtk > toleransidtk";
                $field2 = "count(id) as jml";
                $dbd2 = $this->select("absensi_karyawan",$field2,$where2); //SUBSTR(selisihabsensi, 1, 2)
                if($dbr2 = $this->getrow($dbd2)){
                    $jmlterlambat = $dbr2['jml'];
                }
                
                $return['target'] = $jmlmasuk;
                $return['pencapaian'] = $jmlmasuk - $jmlterlambat;
            }else if($perhitungan == "2"){//Masuk Kerja

                //target -> cek jadwal
                for($tgl = $tglawal;$tgl<=$tglakhir;$tgl = date("Y-m-d",strtotime($tgl)+(60*60*24))){
                    $jw = $this->getjwabsensi($nip,$tgl);
                    if($jw['libur'] == "0")$return['target']++;
                }
                

                //pencapaian 
                $where2 = "tglfinger >= '$tglawal' and tglfinger <= '$tglakhir' 
                                and status = '1' and mode = '1' and nip = '$nip'";
                
                $dbd2 = $this->select("absensi_karyawan","ifnull(count(id),0) as jml",$where2);
                if($dbr2 = $this->getrow($dbd2)){
                    $return['pencapaian'] = $dbr2['jml'];
                }
            }
        }

        return $return;
    }

    public function getptkpkaryawan($nip,$tgl){
        $tgl = date_2s($tgl);
        $arrreturn = array("ptkp"=>"","keterangan"=>"","jmlptkp"=>0);
        $field = "k.ptkp,j.keterangan";
        $where = "k.nip = '$nip' and k.tgl <= '$tgl'";
        $join  = "left join pajak_ptkp j on j.kode = k.ptkp";
        $dbd   = $this->select("karyawan_ptkp k", $field, $where, $join,"","k.tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $arrreturn['ptkp'] = $dbr['ptkp'];
            $arrreturn['keterangan'] = $dbr['keterangan'];
            $jmlptkp = $this->getjmlptkp($dbr['ptkp'],$tgl);
            $arrreturn['jmlptkp'] = $jmlptkp;

        }
        return $arrreturn;
    }

    public function getjmlptkp($ptkp,$tgl){
        $tgl = date_2s($tgl);
        $return = 0;
        $field = "k.jumlahtanggungan";
        $where = "k.kode = '$ptkp' and k.tgl <= '$tgl'";
        $join  = "";
        $dbd   = $this->select("pajak_ptkp_jumlahtanggungan k", $field, $where, $join,"","k.tgl desc","1") ;
        while($dbr = $this->getrow($dbd)){
            $return = $dbr['jumlahtanggungan'];
        }
        return $return;
    }

    public function getpph21posting($nip,$tglawal,$tglakhir){
        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);
        $arrreturn = array("penambah"=>0,"pengurang"=>0,"neto"=>0,"ptkp"=>"","nilaiptkp"=>0,"pkp"=>0,"pph21"=>0,"detailprogressif"=>array(),
                        "detailpenambahan"=>array(),"detailpengurang"=>array());

        //penambah
        $field = "k.nip,sum(k.nominal) as nominal,o.keterangan";
        $join = "left join payroll_posting p on p.faktur = k.faktur left join karyawan a on a.kode = k.nip
        left join payroll_komponen o on o.kode = k.payroll";
        $where = "p.status = '1' and p.tgl >= '$tglawal' and p.tgl<= '$tglakhir' and o.perhitunganpph21 = '1' and k.potongan = '0' and k.nip = '$nip'";
        $dbd = $this->select("payroll_posting_komponen k",$field,$where,$join,"k.payroll");
        while($dbr = $this->getrow($dbd)){
            $arrreturn['penambah'] += $dbr['nominal'];
            $arrreturn['detailpenambahan'][] = array("keterangan"=>$dbr['keterangan'],"nominal"=>$dbr['nominal']); 
        }

        //asuransi karyawan
        $field = "ifnull(sum(d.tarifperusahaan + d.tarifkaryawan),0) as jmlasuransi,b.keterangan";
        $join  = "left join asuransi_pembayaran_total t on t.faktur = d.faktur 
                left join asuransi_karyawan a on a.nopendaftaran = d.nopendaftaran
                left join asuransi b on b.kode = a.asuransi";
        $where = "t.status = '1' and a.karyawan = '$nip' and t.tgl >= '$tglawal' and t.tgl <= '$tglakhir'";
        $dbd = $this->select("asuransi_pembayaran_detail d",$field,$where,$join,"a.nopendaftaran");
        while($dbr = $this->getrow($dbd)){
            $arrreturn['penambah'] += $dbr['jmlasuransi'];
            $arrreturn['detailpenambahan'][] = array("keterangan"=>$dbr['keterangan'],"nominal"=>$dbr['jmlasuransi']); 
        }

        //by jabatan
        $byjabatan = devide(5,100) * $arrreturn['penambah'];
        $byjabatan = min(500000,$byjabatan);
        $arrreturn['pengurang'] += $byjabatan;
        $arrreturn['detailpengurang'][] = array("keterangan"=>"Biaya jabatan 5% ".string_2s($arrreturn['penambah']),"nominal"=>$byjabatan); 

        //potongan
        $field = "k.nip,sum(k.nominal) as nominal,o.keterangan";
        $join = "left join payroll_posting p on p.faktur = k.faktur left join karyawan a on a.kode = k.nip
        left join payroll_komponen o on o.kode = k.payroll";
        $where = "p.status = '1' and p.tgl >= '$tglawal' and p.tgl<= '$tglakhir' and o.perhitunganpph21 = '1' and k.potongan = '1' and k.nip = '$nip'";
        $dbd = $this->select("payroll_posting_komponen k",$field,$where,$join,"k.payroll");
        while($dbr = $this->getrow($dbd)){
            $arrreturn['pengurang'] += $dbr['nominal'];
            $arrreturn['detailpengurang'][] = array("keterangan"=>$dbr['keterangan'],"nominal"=>$dbr['nominal']); 
        }

        //ptkp
        $ptkp = $this->getptkpkaryawan($nip,$tglakhir);
       // echo $tglakhir;
        $arrreturn['ptkp'] = $ptkp['ptkp'];
        $arrreturn['nilaiptkp'] = $ptkp['jmlptkp'];
        
        $arrreturn['neto'] = $arrreturn['penambah'] - $arrreturn['pengurang'];
        $pkp = $arrreturn['neto'] - $arrreturn['nilaiptkp'];
        if($ptkp['ptkp'] == "") $pkp = 0 ;
        $arrreturn['pkp'] = max(0,$pkp);
        
        //hitung progressif
        $pph21prog = $this->getpph21terhutang($arrreturn['pkp'],$tglakhir);
        $arrreturn['detailprogressif'] = $pph21prog['detail'];
        $arrreturn['pph21'] = $pph21prog['pph21terhutang'];
        //print_r($arrreturn);
        return $arrreturn;
    }

    public function getpph21terhutang($pkp,$tgl,$lnpwp=true){
        $tgl = date_2s($tgl);
        $arrreturn = array("pph21terhutang"=>0,"detail"=>array());
        $dbd      = $this->select( 'pajak_pph21_tarifprogresif p', "p.tgl", "p.tgl <= '$tgl'", '', '', 'p.tgl desc', "1" ) ;
        if($dbr = $this->getrow($dbd)){
            $tgl = $dbr['tgl'];
            $n = 0;
            $data = array();
            $dba      = $this->select( 'pajak_pph21_tarifprogresif p', 'p.perstarif,p.penghasilanthn', "p.tgl = '$tgl' and p.npwp = '1'", "", '', 'p.penghasilanthn asc' ) ;
            while($dbra = $this->getrow($dba)){
                $n++;
                $data[$n] = array("perstarif"=>$dbra['perstarif'],"penghasilan"=>$dbra['penghasilanthn']);
            }
            
            $jmlprog = count($data);
            $pph21terhutang = 0 ;
            $penghasilan = $pkp ;
            foreach($data as $key => $val){
                $nilai = min($penghasilan,$val['penghasilan']);
                if($jmlprog == $key) $nilai = $penghasilan;
                $pph21prog = $nilai * devide($val['perstarif'],100);
                $pph21terhutang += $pph21prog; 
                $penghasilan -= $nilai;
                if($pph21prog > 0)$arrreturn['detail'][] = array("perstarif"=>$val['perstarif'],"penghasilan"=>$val['penghasilan'],"nilaipkpprog"=>$nilai,"pph21"=>$pph21prog,"npwp"=>true);
                
            }   

            if(!$lnpwp){ // tidak punya npwp
                $data = array();
                $dba      = $this->select( 'pajak_pph21_tarifprogresif p', 'p.perstarif,p.penghasilanthn', "p.tgl = '$tgl' and p.npwp = '0'", "", '', 'p.penghasilanthn asc' ) ;
                while($dbra = $this->getrow($dba)){
                    $n++;
                    $nilai = $pph21terhutang;
                    $pph21prog = $nilai * devide($val['perstarif'],100);
                    $pph21terhutang += $pph21prog; 
                    $arrreturn['detail'][] = array("perstarif"=>$dbra['perstarif'],"penghasilan"=>$dbra['penghasilanthn'],"nilaipkpprog"=>$nilai,"pph21"=>$pph21prog,"npwp"=>false);
                }
            }

            $arrreturn['pph21terhutang'] = $pph21terhutang;
        }

        return $arrreturn;
    }   
}
