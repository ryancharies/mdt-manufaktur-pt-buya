<?php
class Func_m extends Bismillah_Model{
	//session_start();
    public function postCURL($_url, $_param){
        $postData = '';
        //create name value pairs seperated by &
        foreach($_param as $k => $v) 
        { 
          $postData .= $k . '='.$v.'&'; 
        }
        rtrim($postData, '&');


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, false); 
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    

        $output=curl_exec($ch);

        curl_close($ch);

        return $output;
    }

    public function getfaktur($key,$tgl,$cabang='',$l=true){
        $cabang = ($cabang == "" || $cabang == "null")?getsession($this, "cabang"):$cabang ;
        $key  = $key.$cabang.date("ymd",strtotime($tgl));
        $n    = $this->getincrement($key, $l,5);
        $faktur    = $key.$n ;
        return $faktur ;
    }

    public function GetKeterangan($cKode,$cFieldKeterangan,$cTable){
        if(empty($cFieldKeterangan)){
            $cFieldKeterangan = "keterangan" ;
        }
        $dbData = $this->select($cTable,$cFieldKeterangan,"Kode = '$cKode'");
        return $this->getrow($dbData) ;
    }
    
    public function GetDataCabang($cabang){
        $va = array("kode"=>"","nama"=>"","alamat"=>"","telp"=>"","kota"=>"");
        $dbData = $this->select("cabang","*","Kode = '$cabang'");
        if($dbr = $this->getrow($dbData)){
            $va['kode'] = $dbr['kode'];
            $va['nama'] = $dbr['keterangan'];
            $va['alamat'] = $dbr['alamat'];
            $va['telp'] = $dbr['telp'];
            $va['kota'] = $dbr['kota'];
        }
        return $va;
    }

    public function getinfogudang($gudang){
        $return = array("ketgudang"=>"","cabang"=>"","ketcabang"=>"");

        $field = "g.kode,g.keterangan,g.cabang,c.keterangan as ketcabang";
        $join = "left join cabang c on c.kode = g.cabang";
        $dbd = $this->select("gudang g",$field,"g.kode = '$gudang'",$join);
        if($dbr = $this->getrow($dbd)){
            $return['ketgudang'] = $dbr['keterangan'];
            $return['cabang'] = $dbr['cabang'];
            $return['ketcabang'] = $dbr['ketcabang'];
        }
        return $return;

    }
    
    public function devide($a,$b){
        $return = 0;
        if($a > 0 and $b > 0){
            $return = $a / $b;
        }
        return $return ;
    }

	
	// Dot Matrix Delivery Order
    public function cetakdo($faktur){
        $this->escpos->connect();
        $cpl = 80 ;
        //set content print
        //header
        // $vaprint = array();
        $tgl = date("Y-m-d");
        //$this->escpos->teks(str_pad("PT. BUYA BAROKAH",80," ",STR_PAD_BOTH));
        //$this->escpos->teks(str_pad("DELIVERY ORDER (DO)",80," ",STR_PAD_BOTH));
        $cField = "p.faktur, t.tgl, s.nama as customer,t.agen,t.supir,t.cabang";
        $cWhere = "p.faktur = '".$faktur."'" ;
        $vaJoin = "LEFT JOIN do_total t ON t.faktur = p.faktur
                   LEFT JOIN customer s on s.kode = t.customer" ;
        $dbData = $this->select("do_detail p",$cField,$cWhere,$vaJoin) ;
        if($dbr = $this->getrow($dbData)){
            $dbr['customer'] = substr($dbr['customer'],0,18);
            $arrcab = $this->GetDataCabang($dbr['cabang']);
            $arrcab['alamat'] = substr($arrcab['alamat'],0,50);
            $arrcab['telp'] = substr($arrcab['telp'],0,39);
            $dbr['customer'] = substr($dbr['customer'] ,0,19);
            $dbr['supir'] = substr($dbr['supir'] ,0,19);
            $dbr['agen'] = substr($dbr['agen'] ,0,19);
            $tgl = $dbr['tgl'];
			$this->escpos->teks(str_pad($arrcab['nama'],40," ",STR_PAD_RIGHT). str_pad("No. PO   : ".$faktur,30," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad($arrcab['alamat'],50," ",STR_PAD_RIGHT).str_pad("Customer : ".$dbr['customer'],30," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],50," ",STR_PAD_RIGHT).str_pad("Agen     : ".$dbr['agen'],30," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("",50," ",STR_PAD_RIGHT). str_pad("Tgl      : ".date_2d($dbr['tgl']),30," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("",50," ",STR_PAD_RIGHT). str_pad("Supir    : ".$dbr['supir'],30," ",STR_PAD_RIGHT));


            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            
            
            $title = "DELIVERY ORDER (DO)";
            $this->escpos->posisiteks("center");
            $this->escpos->teks(str_repeat("=",strlen($title)));
            $this->escpos->teks("DELIVERY ORDER (DO)");
            $this->escpos->teks(str_repeat("=",strlen($title)));
            //$this->escpos->posisiteks("left");
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            //$this->escpos->teks(str_pad("Faktur : ".$faktur,30," ",STR_PAD_RIGHT) . str_pad("Tgl : ".date_2d($dbr['tgl']),25," ",STR_PAD_RIGHT) ;

            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("No",3," ",STR_PAD_BOTH)."|".str_pad("Kode",12," ",STR_PAD_BOTH)."|".str_pad("Nama Barang",37," ",STR_PAD_BOTH)."|".str_pad("Jumlah",12," ",STR_PAD_BOTH)."|".str_pad("Satuan",10," ",STR_PAD_BOTH)."|");
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));

            $cF = "s.kode, p.faktur,s.keterangan,p.qty,s.satuan, t.Tgl, sp.nama as customer,t.agen";
            $cJ  = "LEFT JOIN stock s ON s.kode = p.stock
                   Left JOIN do_total t on t.faktur = p.faktur
                   LEFT JOIN customer sp on sp.kode = t.customer" ;
            $cW = "p.faktur = '".$faktur."'" ;
            $dbD = $this->select("do_detail p",$cF,$cW,$cJ) ;
            $n = 0 ;
            while($dbr2 = $this->getrow($dbD)){
                $n++;
                $this->escpos->teks("|".str_pad($n,3," ",STR_PAD_BOTH).
							 "|".str_pad($dbr2['kode'],12," ",STR_PAD_BOTH).
							 "|".str_pad($dbr2['keterangan'],37," ",STR_PAD_RIGHT).
							 "|".str_pad(string_2n($dbr2['qty']),12," ",STR_PAD_LEFT).
							 "|".str_pad($dbr2['satuan'],10," ",STR_PAD_BOTH)."|");
            }
        }
        $now  = date_2b($tgl) ;
        $this->escpos->teks(str_pad($this->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'],40," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("Gudang",40," ",STR_PAD_BOTH).str_pad("Penjualan",40," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("(...............)",40," ",STR_PAD_BOTH).str_pad("(...............)",40," ",STR_PAD_BOTH));
        //  Setting URL To Fetch Data From
        //$contentprint = implode("~",$vaprint);

        // //echo('alert("'.$contentprint.'");');
        $this->escpos->cetak(true);
    }
	
		// Dot Matrix Surat Jalan
    public function cetaksj($faktur){
        $this->escpos->connect();
        $cpl = 80 ;
        //set content print
        //header
        // $vaprint = array();
        $tgl = date("Y-m-d");
        $cField = "p.faktur,t.do, t.Tgl, t.petugaspengirim,t.nopol,s.nama as customer,t.kernet,t.agen,t.cabang";
        $cWhere = "p.faktur = '".$faktur."'" ;
        $vaJoin = "LEFT JOIN sj_total t ON t.faktur = p.faktur
                   LEFT JOIN customer s on s.kode = t.customer" ;
        $dbData = $this->select("sj_detail p",$cField,$cWhere,$vaJoin) ;
        if($dbrsj = $this->getrow($dbData)){
            $arrcab = $this->GetDataCabang($dbrsj['cabang']);
            $arrcab['alamat'] = substr($arrcab['alamat'],0,50);
            $arrcab['telp'] = substr($arrcab['telp'],0,50);
            $dbrsj['customer'] = substr($dbrsj['customer'] ,0,19);
            $dbrsj['agen'] = substr($dbrsj['agen'] ,0,19);
            $tgl = $dbrsj['Tgl'];
            $this->escpos->teks(str_pad($arrcab['nama'],50," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad($arrcab['alamat'],50," ",STR_PAD_RIGHT). str_pad("Kepada Yang Terhormat : ",30," ",STR_PAD_RIGHT));	 
            $this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],50," ",STR_PAD_RIGHT). str_pad("".$dbrsj['customer'],30," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("",50," ",STR_PAD_RIGHT). str_pad("".$dbrsj['agen'],30," ",STR_PAD_RIGHT));

            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->posisiteks("center");
            //$this->escpos->textsize(4,4);
            $this->escpos->teks("SURAT JALAN (SJ)");
            $this->escpos->posisiteks("left");
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			
			//$this->escpos->teks(str_pad("Customer : ".$dbrsj['customer'],25," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("No SJ : ".$faktur,28," ",STR_PAD_RIGHT) . str_pad
								("No DO   : ".$dbrsj['do'],30," ",STR_PAD_RIGHT) . str_pad
								("Supir  : ".$dbrsj['petugaspengirim'],22," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("Tgl   : ".date_2d($dbrsj['Tgl']),28," ",STR_PAD_RIGHT) .
						 str_pad("No. Pol : ".$dbrsj['nopol'],30," ",STR_PAD_RIGHT) .
						 str_pad("Kernet : ".$dbrsj['kernet'],22," ",STR_PAD_RIGHT));

            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("No",3," ",STR_PAD_BOTH).
						 "|".str_pad("Kode",12," ",STR_PAD_BOTH).
						 "|".str_pad("Nama Barang",30," ",STR_PAD_BOTH).
						 "|".str_pad("Jumlah",10," ",STR_PAD_BOTH).
						 "|".str_pad("Retur",8," ",STR_PAD_BOTH).
						 "|".str_pad("Diterima",10," ",STR_PAD_BOTH)."|");
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));

            $cF = "s.kode, p.faktur,t.do,s.keterangan,p.qty,s.satuan, t.Tgl,t.petugaspengirim,t.nopol,t.kernet,
                    sp.nama as customer";
            $cJ  = "LEFT JOIN stock s ON s.kode = p.stock
                   Left JOIN sj_total t on t.faktur = p.faktur
                   LEFT JOIN customer sp on sp.kode = t.customer" ;
            $cW = "p.faktur = '".$faktur."'" ;
            $dbD = $this->select("sj_detail p",$cF,$cW,$cJ) ;
            $n = 0 ;
            while($dbr2 = $this->getrow($dbD)){
                $n++;
                $dbr2['keterangan'] = substr($dbr2['keterangan'],0,30);
                $this->escpos->teks("|".str_pad($n,3," ",STR_PAD_BOTH).
							 "|".str_pad($dbr2['kode'],12," ",STR_PAD_BOTH).
							 "|".str_pad($dbr2['keterangan'],30," ",STR_PAD_RIGHT).
							 "|".str_pad($dbr2['qty'],10," ",STR_PAD_LEFT).
							 "|".str_pad("",8," ",STR_PAD_LEFT).
							 "|".str_pad("",10," ",STR_PAD_BOTH)."|");
            }

        }
        $this->escpos->teks(str_pad("",$cpl,"=",STR_PAD_BOTH));
		$this->escpos->teks(str_pad("Note  Kembali Galon Kosong KH-Q  : ___ pcs",80," ",STR_PAD_RIGHT));
		$this->escpos->teks(str_pad("      Kembali Galon Kosong BUYA  : ___ pcs",80," ",STR_PAD_RIGHT));
		$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $now  = date_2b($tgl) ;
        $this->escpos->teks(str_pad(" ",27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad($this->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'],27," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("Penerima",27," ",STR_PAD_BOTH).str_pad("Supir",26," ",STR_PAD_BOTH).str_pad("Gudang",27," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("(...............)",27," ",STR_PAD_BOTH).str_pad("(...............)",26," ",STR_PAD_BOTH).str_pad("(...............)",27," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("Tgl Jam Bongkar: ",26,"_",STR_PAD_RIGHT).str_pad(" Tgl Jam Muat:",26,"_",STR_PAD_RIGHT).str_pad("",27," ",STR_PAD_BOTH));
        //  Setting URL To Fetch Data From
        // foreach($vaprint as $key => $val){
        //     $this->escpos->teks($val);
        // }
        $this->escpos->cetak(true);
    }
	
	public function cetakiv($faktur){
        $this->escpos->connect();
        $cpl = 80 ;
        //set content print
        //header
        // $vaprint = array();
        $totaltagihan = 0 ;
        $tgl = date("Y-m-d");
        $cField = "t.faktur,t.tgl,t.subtotal,t.diskon,t.ppn,t.total,t.kas,t.komplimen,c.nama as namacustomer,
                 c.alamat, t.piutang,t.sj, s.do,t.cabang";
        $cWhere = "t.faktur = '".$faktur."'" ;
        $vaJoin = "left join customer c on c.kode = t.customer
					left join sj_total s on s.faktur = t.sj";
        $dbData = $this->select("penjualan_total t",$cField,$cWhere,$vaJoin) ;
        if($dbr = $this->getrow($dbData)){
            $tgl = $dbr['tgl'];
            $arrcab = $this->GetDataCabang($dbr['cabang']);
            $this->escpos->teks(str_pad($arrcab['nama'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad($arrcab['alamat'],55," ",STR_PAD_RIGHT). str_pad("Faktur Penjualan",25," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],80," ",STR_PAD_RIGHT));

			
			$this->escpos->teks(str_pad("No. Faktur     : ".$faktur,80," ",STR_PAD_RIGHT));
			
			$this->escpos->teks(str_pad("Tanggal        : ".date_2d($dbr['tgl']),46," ",STR_PAD_RIGHT).
						 str_pad("Tgl Jatuh Tempo : ",34," ",STR_PAD_RIGHT));
						 
            $this->escpos->teks(str_pad("Nama Pembeli   : ".$dbr['namacustomer'],46," ",STR_PAD_RIGHT).
						 str_pad("Nomor DO        : ".$dbr['do'],34," ",STR_PAD_RIGHT));
						 
            $this->escpos->teks(str_pad("Alamat Pembeli : ".$dbr['alamat'],46," ",STR_PAD_RIGHT).
						 str_pad("Nomor SJ        : ".$dbr['sj'],34," ",STR_PAD_RIGHT));
		
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("No",3," ",STR_PAD_BOTH).
						 //"|".str_pad("Kode",12," ",STR_PAD_BOTH).
						 "|".str_pad("Nama Barang",25," ",STR_PAD_BOTH).
						 "|".str_pad("Jumlah",16," ",STR_PAD_BOTH).
						 "|".str_pad("Harga",13," ",STR_PAD_BOTH).
						 "|".str_pad("Total",17," ",STR_PAD_BOTH)."|");
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));

            $cF = "d.stock,s.keterangan as namastock,d.harga,d.qty,s.satuan,d.totalitem as jumlah, (d.hp * d.qty) as hpp";
            $cJ  = "left join stock s on s.kode = d.stock" ;
            $cW = "d.faktur = '".$faktur."'" ;
            $dbD = $this->select("penjualan_detail d",$cF,$cW,$cJ) ;
            $n = 0 ;
            while($dbr2 = $this->getrow($dbD)){
                $n++;
                $dbr2['namastock'] = substr($dbr2['namastock'],0,25);
                $this->escpos->teks("|".str_pad($n,3," ",STR_PAD_BOTH).
							 //"|".str_pad($dbr2['kode'],12," ",STR_PAD_BOTH).
							 "|".str_pad($dbr2['namastock'],25," ",STR_PAD_RIGHT).
							 "|".str_pad(string_2s($dbr2['qty'],0),16," ",STR_PAD_LEFT).
							 "|".str_pad(string_2s($dbr2['harga'],0),13," ",STR_PAD_LEFT).
							 "|".str_pad(string_2s($dbr2['jumlah'],0),17," ",STR_PAD_LEFT)."|");
            }
			
		}
        $this->escpos->teks(str_pad("",$cpl,"=",STR_PAD_BOTH));
		$this->escpos->teks(str_pad(" ",25," ",STR_PAD_BOTH). str_pad("Jumlah Harga Jual",25," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad(string_2s($dbr['subtotal']),20," ",STR_PAD_LEFT));
		//$this->escpos->teks(str_pad(" ",25," ",STR_PAD_BOTH). str_pad("Potongan",25," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad(string_2s($dbr['diskon']),20," ",STR_PAD_LEFT));
		//$this->escpos->teks(str_pad(" ",25," ",STR_PAD_BOTH). str_pad("Dasar Pengenaan Pajak",25," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad(string_2s($dbr['subtotal']-$dbr['diskon']),20," ",STR_PAD_LEFT));
		//$this->escpos->teks(str_pad(" ",25," ",STR_PAD_BOTH). str_pad("PPN",25," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad(string_2s($dbr['ppn']),20," ",STR_PAD_LEFT));
		//$this->escpos->teks(str_pad(" ",25," ",STR_PAD_BOTH). str_pad("Total",25," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad(string_2s($dbr['total']),20," ",STR_PAD_LEFT));
		$totaltagihan += $dbr['total'];
        //cetak retur
        $fr = "t.faktur,t.tgl,t.subtotal,t.ppn,t.total,c.nama as namacustomer,
                 c.alamat,t.fktpj,t.cabang";
        $wr = "t.fktpj = '".$faktur."' and t.status = '1'" ;
        $jr = "left join customer c on c.kode = t.customer";
        $dbdr = $this->select("penjualan_retur_total t",$fr,$wr,$jr) ;
        $retur = 0 ;
        while($dbrr = $this->getrow($dbdr)){
            $retur++;
            $fakturretur = $dbrr['faktur'];
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            if($retur == 1)$this->escpos->teks(str_pad("Retur",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("No. Faktur     : ".$dbrr['faktur'],40," ",STR_PAD_RIGHT).
                                str_pad(" ",5," ",STR_PAD_BOTH).
                                str_pad("Tanggal        : ".date_2d($dbrr['tgl']),35," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("No",3," ",STR_PAD_BOTH).
                                             //"|".str_pad("Kode",12," ",STR_PAD_BOTH).
                                             "|".str_pad("Nama Barang",25," ",STR_PAD_BOTH).
                                             "|".str_pad("Jumlah",16," ",STR_PAD_BOTH).
                                             "|".str_pad("Harga",13," ",STR_PAD_BOTH).
                                             "|".str_pad("Total",17," ",STR_PAD_BOTH)."|");
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
                    
            $cF = "d.stock,s.keterangan as namastock,d.harga,d.qty,s.satuan,d.jumlah as jumlah, (d.hp * d.qty) as hpp";
            $cJ  = "left join stock s on s.kode = d.stock" ;
            $cW = "d.faktur = '".$fakturretur."'" ;
            $dbD = $this->select("penjualan_retur_detail d",$cF,$cW,$cJ) ;
            $n = 0 ;
            while($dbr2 = $this->getrow($dbD)){
                $n++;
                $this->escpos->teks("|".str_pad($n,3," ",STR_PAD_BOTH).
                                    //"|".str_pad($dbr2['kode'],12," ",STR_PAD_BOTH).
                                    "|".str_pad($dbr2['namastock'],25," ",STR_PAD_RIGHT).
                                    "|".str_pad(string_2s($dbr2['qty'],0),16," ",STR_PAD_LEFT).
                                    "|".str_pad(string_2s($dbr2['harga'],0),13," ",STR_PAD_LEFT).
                                    "|".str_pad(string_2s($dbr2['jumlah'],0),17," ",STR_PAD_LEFT)."|");
            }

            $this->escpos->teks(str_pad("",$cpl,"=",STR_PAD_BOTH));
            $this->escpos->teks(str_pad(" ",25," ",STR_PAD_BOTH). str_pad("Jumlah Retur",25," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad(string_2s($dbrr['subtotal']),20," ",STR_PAD_LEFT));
           // $this->escpos->teks(str_pad(" ",25," ",STR_PAD_BOTH). str_pad("Dasar Pengenaan Pajak",25," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad(string_2s($dbrr['subtotal']),20," ",STR_PAD_LEFT));
           // $this->escpos->teks(str_pad(" ",25," ",STR_PAD_BOTH). str_pad("PPN",25," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad(string_2s($dbrr['ppn']),20," ",STR_PAD_LEFT));
            //$this->escpos->teks(str_pad(" ",25," ",STR_PAD_BOTH). str_pad("Total",25," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad(string_2s($dbrr['total']),20," ",STR_PAD_LEFT));
            $totaltagihan -= $dbrr['total'];
        }

        $this->escpos->teks(str_pad(" ",25," ",STR_PAD_BOTH). str_pad("Total Tagihan",25," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad(string_2s($totaltagihan),20," ",STR_PAD_LEFT));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
		$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
		$this->escpos->teks(str_pad("Mohon transfer pembayaran ke nomor rekening  bank BRI cabang kudus :",80," ",STR_PAD_RIGHT));
		$this->escpos->teks(str_pad("  A/N  : PT. Buya Barokah Kudus",80," ",STR_PAD_RIGHT));
		$this->escpos->teks(str_pad("  A/C  : 0038 01 000 741 302",80," ",STR_PAD_RIGHT));
        $now  = date_2b($tgl) ;
        $this->escpos->teks(str_pad(" ",27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad($this->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'],27," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad(" ",27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad("Bag. Keuangan",27," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad(" ",27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad("(...............)",27," ",STR_PAD_BOTH));
        
        // foreach($vaprint as $key => $val){
        //     $this->escpos->teks($val);
        // }
        $this->escpos->cetak(true);
    }

    public function cetakpp($faktur){
        $this->escpos->connect();
        $cpl = 80 ;
        $field = "t.faktur,t.tgl,t.customer,s.nama as namacustomer,t.penjualan,t.retur,t.subtotal,t.kasbank,t.rekkasbank,
                    b.keterangan as ketrekkasbank,t.diskon,t.pembulatan,t.uangmuka,t.cabang";
        $where = "t.faktur = '$faktur'";
        $join  = "left join customer s on s.kode = t.customer left join bank b on b.kode = t.rekkasbank";
        $dbd   = $this->select("piutang_pelunasan_total t", $field, $where, $join) ;
        $tgl = date("Y-m-d");
        if($dbr = $this->getrow($dbd)){
            $arrcab = $this->GetDataCabang($dbr['cabang']);
            $this->escpos->teks(str_pad($arrcab['nama'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad($arrcab['alamat'],55," ",STR_PAD_RIGHT). str_pad("Bukti Pelunasan",25," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],80," ",STR_PAD_RIGHT));
            $tgl = $dbr['tgl'];
			
			$this->escpos->teks(str_pad("No. Faktur     : ".$faktur,80," ",STR_PAD_RIGHT));
			
			$this->escpos->teks(str_pad("Tanggal        : ".date_2d($dbr['tgl']),40," ",STR_PAD_RIGHT).
                         str_pad("Customer    : ",34,$dbr['namacustomer'],STR_PAD_RIGHT));
                         
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("No",3," ",STR_PAD_BOTH).
						 //"|".str_pad("Kode",12," ",STR_PAD_BOTH).
                         "|".str_pad("Faktur",16," ",STR_PAD_BOTH).
                         "|".str_pad("Faktur SJ",16," ",STR_PAD_BOTH).
						 "|".str_pad("Jumlah",20," ",STR_PAD_BOTH).
						 "|".str_pad("Jenis",19," ",STR_PAD_BOTH)."|");
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));

            $f2 = "p.fkt,p.jumlah,p.jenis,c.sj";
            $j2  = "left join penjualan_total c on c.faktur = p.fkt" ;
            $w2 = "p.faktur = '".$faktur."'" ;
            $dbd2 = $this->select("piutang_pelunasan_detail p",$f2,$w2,$j2) ;
            $n = 0 ;
            while($dbr2 = $this->getrow($dbd2)){
                $n++;
                $this->escpos->teks("|".str_pad($n,3," ",STR_PAD_LEFT).
							 //"|".str_pad($dbr2['kode'],12," ",STR_PAD_BOTH).
                             "|".str_pad($dbr2['fkt'],16," ",STR_PAD_BOTH).
                             "|".str_pad($dbr2['sj'],16," ",STR_PAD_BOTH).
                            "|".str_pad(string_2s($dbr2['jumlah']),20," ",STR_PAD_LEFT).
                            "|".str_pad($dbr2['jenis'],19," ",STR_PAD_BOTH)."|");
            }
            $this->escpos->teks(str_pad("",$cpl,"=",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("Penjualan",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($dbr['penjualan']),20," ",STR_PAD_LEFT).
                                str_pad(" ",2," ",STR_PAD_BOTH).
                                str_pad("Retur",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($dbr['retur']),20," ",STR_PAD_LEFT));
            $this->escpos->teks(str_pad("Subtotal",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($dbr['subtotal']),20," ",STR_PAD_LEFT).
                                str_pad(" ",2," ",STR_PAD_BOTH).
                                str_pad("Uang Muka",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($dbr['uangmuka']),20," ",STR_PAD_LEFT));
            $this->escpos->teks(str_pad("Pembulatan",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($dbr['pembulatan']),20," ",STR_PAD_LEFT).
                                str_pad(" ",2," ",STR_PAD_BOTH).
                                str_pad("Diskon",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($dbr['diskon']),20," ",STR_PAD_LEFT));
            $this->escpos->teks(str_pad("Kas / Bank",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad(string_2s($dbr['kasbank']),20," ",STR_PAD_LEFT).
                                str_pad(" ",2," ",STR_PAD_BOTH).
                                str_pad("Ket",16," ",STR_PAD_RIGHT).
                                str_pad(":",3," ",STR_PAD_BOTH).
                                str_pad($dbr['ketrekkasbank'],20," ",STR_PAD_RIGHT));
			 
		}
        
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $now  = date_2b($tgl) ;
        $this->escpos->teks(str_pad(" ",27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad($this->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'],27," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("Yang Membayar",27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad("Bag. Keuangan",27," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("(...............)",27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad("(...............)",27," ",STR_PAD_BOTH));
        
        // foreach($vaprint as $key => $val){
        //     $this->escpos->teks($val);
        // }
        $this->escpos->cetak(true);
    }
	
		// Dot Matrix Purchase Order Order
    public function cetakpo($faktur){
        /*$cpl = $this->config_m->getconfig("cfgkasirprintcpl");
        if($cpl == "")*/
        $cpl = 80 ;
        //set content print
        //header
        
        //connect
        $this->escpos->connect();

        // $vaprint = array();
        $tgl = date("Y-m-d");
        $cField = "p.id,p.faktur, sum(t.total) total,  t.tgl as Tgl, s.nama as supplier,t.fktpr,t.cabang,
                s.terminhari,s.namabank,
                s.rekening,s.atasnamarekening,s.notelepon,s.kontakpersonal";
        $cWhere = "p.faktur = '".$faktur."'" ;
        $vaJoin = "LEFT JOIN po_total t ON t.faktur = p.faktur
                   LEFT JOIN supplier s on s.kode = t.supplier" ;
        $dbData = $this->select("po_detail p",$cField,$cWhere,$vaJoin,"p.id") ;
        
        if($dbr = $this->getrow($dbData)){
            
            $arrcab = $this->GetDataCabang($dbr['cabang']);
            $tgl = $dbr['Tgl'];
            $this->escpos->teks(str_pad($arrcab['nama'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad($arrcab['alamat'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->posisiteks("center");
            //$this->escpos->textsize(4,4);
            $this->escpos->teks("PURCHASE ORDER (PO)");
            $this->escpos->posisiteks("left");
            $this->escpos->teks(str_pad("No. PO :".$faktur,80," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));

			//$this->escpos->teks(str_pad("Faktur : ".$faktur,30," ",STR_PAD_RIGHT) . str_pad("Tgl : ".date_2d($dbr['Tgl']),25," ",STR_PAD_RIGHT) . str_pad("Supplier : ".$dbr['supplier'],25," ",STR_PAD_RIGHT));

            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("No",3," ",STR_PAD_BOTH).
						 //"|".str_pad("Kode",10," ",STR_PAD_BOTH).
						 "|".str_pad("Nama Barang",25," ",STR_PAD_BOTH).
						 "|".str_pad("Harga",10," ",STR_PAD_BOTH).
						 "|".str_pad("Qty",16," ",STR_PAD_BOTH).
						 "|".str_pad("Sat",3," ",STR_PAD_BOTH).
						 "|".str_pad("Total",16," ",STR_PAD_BOTH)."|");
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));

            $cF = "s.kode, p.faktur,s.keterangan,p.jumlah,p.harga,p.qty,s.satuan, t.tgl as Tgl, 
                    sp.nama as supplier,t.fktpr,p.spesifikasi";
            $cJ  = "LEFT JOIN stock s ON s.kode = p.stock
                   Left JOIN po_total t on t.faktur = p.faktur
                   LEFT JOIN supplier sp on sp.kode = t.supplier" ;
            $cW = "p.faktur = '".$faktur."'" ;
            $dbD = $this->select("po_detail p",$cF,$cW,$cJ,"p.id") ;
            $n = 0 ;
            $total = 0 ;
            $arrspesifikasi = array();
            while($dbr2 = $this->getrow($dbD)){
                $n++;
                if(trim($dbr2['spesifikasi']) !== "")$arrspesifikasi[$dbr2['kode']] = array("keterangan"=>$dbr2['keterangan'],"spesifikasi"=>$dbr2['spesifikasi']);
                
                $total += $dbr2['jumlah'] ;
				$dbr2['keterangan'] = substr($dbr2['keterangan'],0,25);
				$dbr2['satuan'] = substr($dbr2['satuan'],0,25);
                $this->escpos->teks("|".str_pad($n,3," ",STR_PAD_BOTH).
							 //"|".str_pad($dbr2['kode'],10," ",STR_PAD_BOTH).
							 "|".str_pad($dbr2['keterangan'],25," ",STR_PAD_RIGHT).
							 "|".str_pad(string_2s($dbr2['harga']),10," ",STR_PAD_LEFT).
							 "|".str_pad(string_2s($dbr2['qty']),16," ",STR_PAD_LEFT).
							 "|".str_pad($dbr2['satuan'],3," ",STR_PAD_BOTH).
							 "|".str_pad(string_2s($dbr2['jumlah']),16," ",STR_PAD_LEFT)."|");
            }
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("Total",61," ",STR_PAD_BOTH).
							 "|".str_pad(string_2s($total),16," ",STR_PAD_LEFT)."|");
			$this->escpos->teks(str_pad("",$cpl,"=",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("*) Ketentuan :",80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("   1. Term of payment ". $dbr['terminhari'] ." hari setelah barang diterima oleh pihak perusahaan",80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("   2. Waktu pengiriman dan spesifikasi barang sesuai dengan permintaan dari",80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("      perusahaan",80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("   3. Pembayaran akan dilakukan oleh pihak perusahaan ke nomor rekening bank",80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("      supplier yang jelas dengan data sebagai berikut :",80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("      a. Nama bank                    : ".$dbr['namabank'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("      b. No. Rekening supplier        : ".$dbr['rekening'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("      c. Nama dalam rekening supplier : ".$dbr['atasnamarekening'],80," ",STR_PAD_RIGHT));

			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			//$this->escpos->teks(str_pad("".date_2d($dbrsj['Tgl']),80," ",STR_PAD_RIGHT));

			// $this->escpos->teks(str_pad("      c. Nama dalam rekening supplier	:",80," ",STR_PAD_RIGHT));

            $now  = date_2b($tgl) ;
            $this->escpos->teks(str_pad($this->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'],$cpl," ",STR_PAD_RIGHT));
            //$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $arruserttd = array("Diajukan"=>"Staff Pembelian","Diperiksa"=>"Manager","Mengetahui"=>"Direktur","Disetujui"=>"Direktur Utama");
            $ttdlevel = 2;
            if($dbr['total'] > $this->getconfig('pbminttddirektur',0))$ttdlevel = 3;
            if($dbr['total'] > $this->getconfig('pbminttddirut',0))$ttdlevel = 4;
            $ttd1 = "";
            $ttd2 = "";
            $ttd3 = "";
            $n = 0;
            $chrttd = devide($cpl,$ttdlevel);
            $chrttd = floor($chrttd);
            foreach($arruserttd as $key => $val){
                $n++;
                if($n >= $ttdlevel){
                    $key = "Disetujui";
                    
                }

                if($n <= $ttdlevel){
                    $ttd1 .= str_pad($key,$chrttd," ",STR_PAD_BOTH);
                    $ttd2 .= str_pad("(............)",$chrttd," ",STR_PAD_BOTH);
                    $ttd3 .= str_pad($val,$chrttd," ",STR_PAD_BOTH);
                }
            }
            $this->escpos->teks($ttd1);
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks($ttd2);
            $this->escpos->teks($ttd3);

            if(!empty($arrspesifikasi)){
                $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
                $this->escpos->teks(str_pad("*) Spesifikasi Barang :",80," ",STR_PAD_RIGHT));
                foreach($arrspesifikasi as $keysp => $valsp){
                    $valsp['keterangan'] = substr($valsp['keterangan'],0,25);
                    $this->escpos->teks(str_pad($valsp['keterangan'],25," ",STR_PAD_RIGHT).
                                        str_pad(":",3," ",STR_PAD_BOTH).
                                        str_pad($valsp['spesifikasi'],50," ",STR_PAD_RIGHT));
                }
            }

            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("Diterima",80," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("  Supplier              : ".$dbr['supplier'],80," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("  Contact Person        : ".$dbr['kontakpersonal'],80," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("  No.HP/Telp. Kantor    : ".$dbr['notelepon'],80," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));

            $this->escpos->teks(str_pad(" Tanda Tangan",80," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("(............)",80," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("   Penerima",80," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));

            $this->escpos->teks(str_pad("Ket: 1.Supplier 2.Pembelian  3.Gudang",80," ",STR_PAD_RIGHT));

        }

        


        // foreach($vaprint as $key => $val){
        //     $this->escpos->teks($val);
        // }
        $this->escpos->cetak(true);

    }

    public function cetaklpbpo($faktur){
        /*$cpl = $this->config_m->getconfig("cfgkasirprintcpl");
        if($cpl == "")*/
        $cpl = 80 ;
        //set content print
        //header
        
        //connect
        $this->escpos->connect();

        // $vaprint = array();
        $tgl = date("Y-m-d");
        $cField = "t.id,t.faktur, sum(t.total) total,  t.tgl as Tgl, s.nama as supplier,t.fktpr,t.cabang";
        $cWhere = "t.faktur = '".$faktur."'" ;
        $vaJoin = "LEFT JOIN supplier s on s.kode = t.supplier" ;
        $dbData = $this->select("po_total t",$cField,$cWhere,$vaJoin,"t.id") ;
        if($dbr = $this->getrow($dbData)){
            $arrcab = $this->GetDataCabang($dbr['cabang']);
            $tgl = $dbr['Tgl'];
            $this->escpos->teks(str_pad($arrcab['nama'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad($arrcab['alamat'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->posisiteks("center");
            //$this->escpos->textsize(4,4);
            $this->escpos->teks("LEMBAR PENERIMAAN BARANG (LPB)");
            $this->escpos->posisiteks("left");
            $this->escpos->teks(str_pad("No. PO :".$faktur,80," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));

			//$this->escpos->teks(str_pad("Faktur : ".$faktur,30," ",STR_PAD_RIGHT) . str_pad("Tgl : ".date_2d($dbr['Tgl']),25," ",STR_PAD_RIGHT) . str_pad("Supplier : ".$dbr['supplier'],25," ",STR_PAD_RIGHT));

            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("No",3," ",STR_PAD_BOTH).
						 //"|".str_pad("Kode",10," ",STR_PAD_BOTH).
                         "|".str_pad("Nama Barang",21," ",STR_PAD_BOTH).
                         "|".str_pad("Sat",4," ",STR_PAD_BOTH).
                         "|".str_pad("Order",11," ",STR_PAD_BOTH).
                         "|".str_pad("Sdh Terima",11," ",STR_PAD_BOTH).
                         "|".str_pad("Sisa",11," ",STR_PAD_BOTH).
						 "|".str_pad("Qty Terima",11," ",STR_PAD_BOTH)."|");
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));

            $cF = "s.kode, p.faktur,s.keterangan,p.jumlah,p.harga,p.qty,s.satuan, t.tgl as Tgl, sp.nama as supplier,t.fktpr,
                ifnull(sum(k.debet-k.kredit),0) qtysisa";
            $cJ  = "LEFT JOIN stock s ON s.kode = p.stock
                    left join po_kartu k on k.fktpo = p.faktur and k.stock = p.stock
                   Left JOIN po_total t on t.faktur = p.faktur
                   LEFT JOIN supplier sp on sp.kode = t.supplier" ;
            $cW = "p.faktur = '".$faktur."'" ;
            $dbD = $this->select("po_detail p",$cF,$cW,$cJ,"p.id") ;
            $n = 0 ;
            $total = 0 ;
            while($dbr2 = $this->getrow($dbD)){
                $n++;
                $diterima = $dbr2['qty'] - $dbr2['qtysisa'];
                $dbr2['keterangan'] = substr($dbr2['keterangan'],0,21);
                $dbr2['satuan'] = substr($dbr2['satuan'],0,4);
                $this->escpos->teks("|".str_pad($n,3," ",STR_PAD_LEFT).
                             //"|".str_pad("Kode",10," ",STR_PAD_BOTH).
                             "|".str_pad($dbr2['keterangan'],21," ",STR_PAD_RIGHT).
                             "|".str_pad($dbr2['satuan'],4," ",STR_PAD_BOTH).
                             "|".str_pad($dbr2['qty'],11," ",STR_PAD_LEFT).
                             "|".str_pad($diterima,11," ",STR_PAD_LEFT).
                             "|".str_pad($dbr2['qtysisa'],11," ",STR_PAD_LEFT).
                             "|".str_pad(" ",11," ",STR_PAD_LEFT)."|");
            }

            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));

        }

        $now  = date_2b($tgl) ;
        $this->escpos->teks(str_pad($this->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'],$cpl," ",STR_PAD_RIGHT));
        //$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $arruserttd = array("Pengirim"=>"","Penerima"=>"Bag. Gudang");
        $ttdlevel = 2;
        $ttd1 = "";
        $ttd2 = "";
        $ttd3 = "";
        $n = 0;
        $chrttd = devide($cpl,$ttdlevel);
        $chrttd = floor($chrttd);
        foreach($arruserttd as $key => $val){
            $n++;

            if($n <= $ttdlevel){
                $ttd1 .= str_pad($key,$chrttd," ",STR_PAD_BOTH);
                $ttd2 .= str_pad("(............)",$chrttd," ",STR_PAD_BOTH);
                $ttd3 .= str_pad($val,$chrttd," ",STR_PAD_BOTH);
            }
        }
        $this->escpos->teks($ttd1);
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks($ttd2);
        $this->escpos->teks($ttd3);


        // foreach($vaprint as $key => $val){
        //     $this->escpos->teks($val);
        // }
        $this->escpos->cetak(true);

    }


	// Dot Matrix Kas Masuk
    public function cetakkm($faktur){
        $this->escpos->connect();
        $cpl = 80 ;
        //set content print
        //header
        // $vaprint = array();
       
        $cField = "t.faktur,t.tgl,t.diberiterima,r.keterangan as ketrekening,t.rekening,t.keterangan,t.debet,t.cabang";
        $cWhere = "t.faktur = '".$faktur."'" ;
        $vaJoin = "left join keuangan_rekening r on r.kode = t.rekening" ;
        $dbData = $this->select("kas_mutasi_total t",$cField,$cWhere,$vaJoin) ;
        if($dbr = $this->getrow($dbData)){
            $arrcab = $this->GetDataCabang($dbr['cabang']);

            $this->escpos->teks(str_pad($arrcab['nama'],61," ",STR_PAD_RIGHT) . str_pad("Kudus, ".date_2d($dbr['tgl']),19," ",STR_PAD_LEFT));
			$this->escpos->teks(str_pad($arrcab['alamat'],55," ",STR_PAD_RIGHT) . str_pad("NO. KM : ".$faktur,25," ",STR_PAD_LEFT));
			$this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],80," ",STR_PAD_RIGHT));
			
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->posisiteks("center");
            //$this->escpos->textsize(4,4);
            $this->escpos->teks("KAS MASUK (KM)");
            $this->escpos->posisiteks("left");
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			
			$this->escpos->teks(str_pad("Diterima dari    : ".$dbr['diberiterima'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Sejumlah         : Rp. ".string_2s($dbr['debet']),80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Terbilang        : ".trim(terbilang($dbr['debet'])." Rupiah"),80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Untuk Pembayaran : ".$dbr['keterangan'],80," ",STR_PAD_RIGHT));

			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("Yang membayar,",40," ",STR_PAD_BOTH).str_pad("Bag. Keuangan,",40," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("(....................)",40," ",STR_PAD_BOTH).str_pad("(....................)",40," ",STR_PAD_BOTH));

		}

        // foreach($vaprint as $key => $val){
        //     $this->escpos->teks($val);
        // }
        $this->escpos->cetak(true);
    }
	
	// Dot Matrix Kas Keluar
	public function cetakkk($faktur){
        $this->escpos->connect();
        $cpl = 80 ;
        //set content print
        //header
        // $vaprint = array();
		
		$cField = "t.faktur,t.tgl,t.diberiterima,r.keterangan as ketrekening,t.rekening,t.keterangan,t.kredit,t.cabang";
        $cWhere = "t.faktur = '".$faktur."'" ;
        $vaJoin = "left join keuangan_rekening r on r.kode = t.rekening" ;
        $dbData = $this->select("kas_mutasi_total t",$cField,$cWhere,$vaJoin) ;
        if($dbr = $this->getrow($dbData)){
            $arrcab = $this->GetDataCabang($dbr['cabang']);
            
            $this->escpos->teks(str_pad($arrcab['nama'],61," ",STR_PAD_RIGHT) . str_pad("Kudus, ".date_2d($dbr['tgl']),19," ",STR_PAD_LEFT));
			$this->escpos->teks(str_pad($arrcab['alamat'],55," ",STR_PAD_RIGHT) . str_pad("NO. KK : ".$faktur,25," ",STR_PAD_LEFT));
			$this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],80," ",STR_PAD_RIGHT));
			
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->posisiteks("center");
            //$this->escpos->textsize(4,4);
            $this->escpos->teks("KAS KELUAR (KK)");
            $this->escpos->posisiteks("left");
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			
			$this->escpos->teks(str_pad("Di bayar ke      : ".$dbr['diberiterima'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Sejumlah         : Rp. ".string_2s($dbr['kredit']),80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Terbilang        : ".trim(terbilang($dbr['kredit'])." Rupiah"),80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Untuk Pembayaran : ".$dbr['keterangan'],80," ",STR_PAD_RIGHT));
			
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("Yang menerima,",40," ",STR_PAD_BOTH).str_pad("Bag. Keuangan,",40," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("(....................)",40," ",STR_PAD_BOTH).str_pad("(....................)",40," ",STR_PAD_BOTH));
		
		}
			
        // foreach($vaprint as $key => $val){
        //     $this->escpos->teks($val);
        // }
        $this->escpos->cetak(true);
    }

    // Dot Matrix Bank Masuk
    public function cetakbm($faktur){
        $this->escpos->connect();
        $cpl = 80 ;
        //set content print
        //header
        // $vaprint = array();
        $this->escpos->teks(str_pad($faktur,$cpl," ",STR_PAD_BOTH));
        $cField = "t.faktur,t.tgl,t.diberiterima,b.keterangan as ketbank,t.bank,t.keterangan,t.debet,t.cabang";
        $cWhere = "t.faktur = '".$faktur."'" ;
        $vaJoin = "left join bank b on b.kode = t.bank" ;
        $dbData = $this->select("bank_mutasi_total t",$cField,$cWhere,$vaJoin) ;
        if($dbr = $this->getrow($dbData)){
            $arrcab = $this->GetDataCabang($dbr['cabang']);
            
            $this->escpos->teks(str_pad($arrcab['nama'],61," ",STR_PAD_RIGHT) . str_pad("Kudus, ".date_2d($dbr['tgl']),19," ",STR_PAD_LEFT));
			$this->escpos->teks(str_pad($arrcab['alamat'],55," ",STR_PAD_RIGHT) . str_pad("NO. KM : ".$faktur,25," ",STR_PAD_LEFT));
			$this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],80," ",STR_PAD_RIGHT));
			
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->posisiteks("center");
            //$this->escpos->textsize(4,4);
            $this->escpos->teks("BANK MASUK (BM)");
            $this->escpos->posisiteks("left");
            $this->escpos->teks(str_pad("Bank : ".$dbr['ketbank'],$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("Diterima dari    : ".$dbr['diberiterima'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Sejumlah         : Rp. ".string_2s($dbr['debet']),80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Terbilang        : ".trim(terbilang($dbr['debet'])." Rupiah"),80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Untuk Pembayaran : ".substr($dbr['keterangan'],0,60),80," ",STR_PAD_RIGHT));

			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("Yang membayar,",40," ",STR_PAD_BOTH).str_pad("Bag. Keuangan,",40," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("(....................)",40," ",STR_PAD_BOTH).str_pad("(....................)",40," ",STR_PAD_BOTH));

		}

        // foreach($vaprint as $key => $val){
        //     $this->escpos->teks($val);
        // }
        $this->escpos->cetak(true);
    }

    // Dot Matrix Bank keluar
    public function cetakbk($faktur){
        $this->escpos->connect();
        $cpl = 80 ;
        //set content print
        //header
        // $vaprint = array();
        $this->escpos->teks(str_pad($faktur,$cpl," ",STR_PAD_BOTH));
        $cField = "t.faktur,t.tgl,t.diberiterima,b.keterangan as ketbank,t.bank,t.keterangan,t.kredit,t.cabang";
        $cWhere = "t.faktur = '".$faktur."'" ;
        $vaJoin = "left join bank b on b.kode = t.bank" ;
        $dbData = $this->select("bank_mutasi_total t",$cField,$cWhere,$vaJoin) ;
        if($dbr = $this->getrow($dbData)){
            $arrcab = $this->GetDataCabang($dbr['cabang']);

            $this->escpos->teks(str_pad($arrcab['nama'],61," ",STR_PAD_RIGHT) . str_pad("Kudus, ".date_2d($dbr['tgl']),19," ",STR_PAD_LEFT));
			$this->escpos->teks(str_pad($arrcab['alamat'],55," ",STR_PAD_RIGHT) . str_pad("NO. KM : ".$faktur,25," ",STR_PAD_LEFT));
			$this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],80," ",STR_PAD_RIGHT));
			
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->posisiteks("center");
            //$this->escpos->textsize(4,4);
            $this->escpos->teks("BANK KELUAR (BK)");
            $this->escpos->posisiteks("left");
            $this->escpos->teks(str_pad("Bank : ".$dbr['ketbank'],$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("Dibayar Ke       : ".$dbr['diberiterima'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Sejumlah         : Rp. ".string_2s($dbr['kredit']),80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Terbilang        : ".trim(terbilang($dbr['kredit'])." Rupiah"),80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad("Untuk Pembayaran : ".substr($dbr['keterangan'],0,60),80," ",STR_PAD_RIGHT));

			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("Yang Menerima,",40," ",STR_PAD_BOTH).str_pad("Bag. Keuangan,",40," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("(....................)",40," ",STR_PAD_BOTH).str_pad("(....................)",40," ",STR_PAD_BOTH));

		}

        // foreach($vaprint as $key => $val){
        //     $this->escpos->teks($val);
        // }
        $this->escpos->cetak(true);
    }

    public function cetakperintahproduksi($faktur){
        $cpl = 80 ;
        
        $cField = "t.faktur,t.tgl,t.hargapokok,t.bb,t.btkl,t.bop,p.stock,s.keterangan,t.perbaikan,t.cabang,t.petugas,
                    s.satuan,p.qty,p.bb,p.btkl,p.bop,p.hargapokok,p.jumlah,p.hargapokokperbaikan,p.jumlahperbaikan";
        $cWhere = "t.faktur = '".$faktur."'" ;
        $vaJoin = "left join produksi_produk p on p.fakturproduksi = t.faktur left join stock s on s.kode = p.stock" ;
        $dbData = $this->select("produksi_total t",$cField,$cWhere,$vaJoin) ;
        if($dbr = $this->getrow($dbData)){
            $this->escpos->connect();
            //header
            $arrcab = $this->func_m->GetDataCabang($dbr['cabang']);
            $this->escpos->teks($arrcab['kode'] ." - ".$arrcab['nama']);
            $this->escpos->teks($arrcab['alamat'] . " / ". $arrcab['telp']);
            $this->escpos->posisiteks("center");
            $this->escpos->textsize(2,2);
            $this->escpos->teks("PERINTAH PRODUKSI");
            //$this->escpos->posisiteks("left");
            
            $this->escpos->teks(str_pad("Faktur",8," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($faktur,17," ",STR_PAD_RIGHT).
                        str_pad("Tgl",9," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad(date_2d($dbr['tgl']),13," ",STR_PAD_RIGHT).
                        str_pad("Perbaikan",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['perbaikan'],14," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("Karu",8," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['petugas'],69," ",STR_PAD_RIGHT));
            
            //table
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks( "|".str_pad("No",2," ",STR_PAD_BOTH).
						 "|".str_pad("Kode",10," ",STR_PAD_BOTH).
						 "|".str_pad("Nama Barang",15," ",STR_PAD_BOTH).
						 "|".str_pad("Qty",8," ",STR_PAD_BOTH).
						 "|".str_pad("Sat",5," ",STR_PAD_BOTH).
                         "|".str_pad("HP",9," ",STR_PAD_BOTH).
                         "|".str_pad("JmlHP",10," ",STR_PAD_BOTH).
                         "|".str_pad("Qty BB",12," ",STR_PAD_BOTH)."|");
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $field2 = "s.kode,s.keterangan,s.satuan,b.qty,b.hp,b.jmlhp";
            $join2  = "LEFT JOIN stock s ON s.kode = b.stock " ;
            $where2 = "b.faktur = '".$faktur."'" ;
            $dbd2 = $this->select("produksi_bb_standart b",$field2,$where2,$join2) ;
            $n = 0 ;
            while($dbr2 = $this->getrow($dbd2)){
                $n++;
				$dbr2['keterangan'] = substr($dbr2['keterangan'],0,15);
                $dbr2['satuan'] = substr($dbr2['satuan'],0,5);
                $qtybb = $dbr2['qty'] * $dbr['qty'];
                $this->escpos->teks("|".str_pad($n,2," ",STR_PAD_LEFT).
						 "|".str_pad($dbr2['kode'],10," ",STR_PAD_BOTH).
						 "|".str_pad($dbr2['keterangan'],15," ",STR_PAD_RIGHT).
						 "|".str_pad($dbr2['qty'],8," ",STR_PAD_LEFT).
						 "|".str_pad($dbr2['satuan'],5," ",STR_PAD_BOTH).
                         "|".str_pad($dbr2['hp'],9," ",STR_PAD_LEFT).
                         "|".str_pad($dbr2['jmlhp'],10," ",STR_PAD_LEFT).
                         "|".str_pad($qtybb,12," ",STR_PAD_LEFT)."|");
            }
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
			$this->escpos->teks(" ");
            
            //footer
            $dbr['keterangan'] = substr($dbr['keterangan'],0,26);

            $this->escpos->teks(str_pad("Produk",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['keterangan'],26," ",STR_PAD_RIGHT).
                                str_pad("Qty",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['qty']. " [" . $dbr['satuan'] . "]",26," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("BB",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['bb'],26," ",STR_PAD_RIGHT).
                                str_pad("HP. Perb",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['hargapokokperbaikan'],26," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("BTKL",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['btkl'],26," ",STR_PAD_RIGHT).
                                str_pad("HP",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['hargapokok'],26," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("Jml. Perb",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['jumlahperbaikan'],26," ",STR_PAD_RIGHT).
                                str_pad("BOP",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['bop'],26," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("Jml HP",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['jumlah'],26," ",STR_PAD_RIGHT));
            
            //ttd
            $this->escpos->teks(" ");
            $this->escpos->teks(str_pad("Menyetujui,",40," ",STR_PAD_BOTH).str_pad("Adm. Produksi,",40," ",STR_PAD_BOTH));
            $this->escpos->teks(" ");
            $this->escpos->teks(" ");
            $this->escpos->teks(" ");
            $this->escpos->teks(str_pad("(....................)",40," ",STR_PAD_BOTH).str_pad("(....................)",40," ",STR_PAD_BOTH));

            $this->escpos->cetak(true);
        }        
    }
    public function cetakhasilproduksi($faktur){
        $cpl = 80 ;
        
        $cField = "h.faktur,h.tgl,h.hp as hargapokok,(h.hp * h.qty) as jumlah,t.btkl,t.bop,h.stock,s.keterangan,t.perbaikan,t.cabang,t.petugas,
                    s.satuan,h.qty,h.fakturproduksi,(sum(b.hp * b.qty)/h.qty) bb,p.bop,p.btkl,p.hargapokokperbaikan,p.jumlahperbaikan";
        $cWhere = "h.faktur = '".$faktur."' and h.status = '1'" ;
        $vaJoin = "left join produksi_total t on t.faktur = h.fakturproduksi left join produksi_produk p on p.fakturproduksi = t.faktur ";
        $vaJoin .= "left join stock s on s.kode = h.stock left join produksi_bb b on b.fakturproduksi = t.faktur and b.status = '1'" ;
        $dbData = $this->select("produksi_hasil h",$cField,$cWhere,$vaJoin,"h.faktur") ;
        if($dbr = $this->getrow($dbData)){
            $this->escpos->connect();
            //header
            $arrcab = $this->func_m->GetDataCabang($dbr['cabang']);
            $this->escpos->teks($arrcab['kode'] ." - ".$arrcab['nama']);
            $this->escpos->teks($arrcab['alamat'] . " / ". $arrcab['telp']);
            $this->escpos->posisiteks("center");
            $this->escpos->textsize(2,2);
            $this->escpos->teks("DETAIL HASIL PRODUKSI");
            //$this->escpos->posisiteks("left");
            
            $this->escpos->teks(str_pad("Faktur",8," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($faktur,17," ",STR_PAD_RIGHT).
                        str_pad("Tgl",9," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad(date_2d($dbr['tgl']),13," ",STR_PAD_RIGHT).
                        str_pad("Perbaikan",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['perbaikan'],14," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("Fkt. PR",8," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['fakturproduksi'],17," ",STR_PAD_RIGHT).
                        str_pad("Karu",9," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['petugas'],40," ",STR_PAD_RIGHT));
            
            //table
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks( "|".str_pad("No",2," ",STR_PAD_BOTH).
						 "|".str_pad("Kode",10," ",STR_PAD_BOTH).
						 "|".str_pad("Nama Barang",20," ",STR_PAD_BOTH).
						 "|".str_pad("Qty",10," ",STR_PAD_BOTH).
						 "|".str_pad("Sat",7," ",STR_PAD_BOTH).
                         "|".str_pad("HP",11," ",STR_PAD_BOTH).
                         "|".str_pad("JmlHP",12," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $fakturpr = $dbr['fakturproduksi'];
            $field2 = "s.kode,s.keterangan,s.satuan,b.qty,b.hp,(b.qty * b.hp) as jmlhp";
            $join2  = "LEFT JOIN stock s ON s.kode = b.stock " ;
            $where2 = "b.fakturproduksi = '".$fakturpr."' and b.status = '1'" ;
            $dbd2 = $this->select("produksi_bb b",$field2,$where2,$join2) ;
            $n = 0 ;
            while($dbr2 = $this->getrow($dbd2)){
                $n++;
				$dbr2['keterangan'] = substr($dbr2['keterangan'],0,20);
                $dbr2['satuan'] = substr($dbr2['satuan'],0,7);
                $qtybb = $dbr2['qty'] * $dbr['qty'];
                $this->escpos->teks("|".str_pad($n,2," ",STR_PAD_LEFT).
						 "|".str_pad($dbr2['kode'],10," ",STR_PAD_BOTH).
						 "|".str_pad($dbr2['keterangan'],20," ",STR_PAD_RIGHT).
						 "|".str_pad($dbr2['qty'],10," ",STR_PAD_LEFT).
						 "|".str_pad($dbr2['satuan'],7," ",STR_PAD_BOTH).
                         "|".str_pad($dbr2['hp'],11," ",STR_PAD_LEFT).
                         "|".str_pad($dbr2['jmlhp'],12," ",STR_PAD_LEFT)."|");
            }
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
			$this->escpos->teks(" ");
            
            //footer
            $this->escpos->teks(str_pad("Produk",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['keterangan'],26," ",STR_PAD_RIGHT).
                                str_pad("Qty",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['qty']. " [" . $dbr['satuan'] . "]",26," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("BB",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['bb'],26," ",STR_PAD_RIGHT).
                                str_pad("HP. Perb",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['hargapokokperbaikan'],26," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("BTKL",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['btkl'],26," ",STR_PAD_RIGHT).
                                str_pad("HP",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['hargapokok'],26," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("Jml. Perb",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['jumlahperbaikan'],26," ",STR_PAD_RIGHT).
                                str_pad("BOP",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['bop'],26," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("Jml HP",10," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($dbr['jumlah'],26," ",STR_PAD_RIGHT));
            
            //ttd
            $this->escpos->teks(" ");
            $this->escpos->teks(str_pad("Penerimaan,",40," ",STR_PAD_BOTH).str_pad("Karu,",40," ",STR_PAD_BOTH));
            $this->escpos->teks(" ");
            $this->escpos->teks(" ");
            $this->escpos->teks(" ");
            $this->escpos->teks(str_pad("(....................)",40," ",STR_PAD_BOTH).str_pad("(....................)",40," ",STR_PAD_BOTH));

            $this->escpos->cetak(true);
        }        
    }

    public function cetakmp($faktur){
        $this->escpos->connect();
        $cpl = 80 ;
        $field = "t.faktur,t.tgl,t.cabang,k.keterangan as ketkdtr,t.kodetransaksi,t.jumlah,t.supplier,s.nama as namasupplier,
                k.dk,t.keterangan as keterangan";
        $where = "t.faktur = '$faktur'";
        $join  = "left join kodetransaksi k on k.kode = t.kodetransaksi";
        $join  .= " left join supplier s on s.kode = t.supplier";
        $dbd   = $this->select("persekot_mutasi_total t", $field, $where, $join) ;
        $ttd = "Yang Membayar";
        $tgl = date("Y-m-d");
        if($dbr = $this->getrow($dbd)){
            $arrcab = $this->GetDataCabang($dbr['cabang']);
            $this->escpos->teks(str_pad($arrcab['nama'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad($arrcab['alamat'],55," ",STR_PAD_RIGHT). str_pad("Bukti Porsekot",25," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],80," ",STR_PAD_RIGHT));

			$tgl = $dbr['tgl'];
			$this->escpos->teks(str_pad("No. Faktur     : ".$faktur,80," ",STR_PAD_RIGHT));
            
            $pembyr = "Diterima Dari : ";
            if($dbr['dk'] == "d"){
                $pembyr = "Dibayar Kpd : ";
                $ttd = "Yang Menerima";
            }
            $dbr['namasupplier'] = substr($dbr['namasupplier'],0,40);
			$this->escpos->teks(str_pad("Tanggal        : ".date_2d($dbr['tgl']),25," ",STR_PAD_RIGHT).
                         str_pad($pembyr.$dbr['namasupplier'],55," ",STR_PAD_RIGHT));
            $this->escpos->teks(str_pad("Keterangan     : ".$dbr['keterangan'],55," ",STR_PAD_RIGHT));
                         
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("No",3," ",STR_PAD_BOTH).
						 //"|".str_pad("Kode",12," ",STR_PAD_BOTH).
						 "|".str_pad("Keterangan",53," ",STR_PAD_BOTH).
						 "|".str_pad("Jumlah",20," ",STR_PAD_BOTH)."|");
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));

            $f2 = "d.faktur,r.kode,r.keterangan as ketrekening,d.jumlah as nominal,d.rekening,
                r.keterangan as ketrek";
            $w2 = "d.faktur = '$faktur'";
            $j2  = "left join keuangan_rekening r on r.kode = d.rekening";
            $dbd2   = $this->select("persekot_mutasi_detail d", $f2, $w2, $j2) ;
            $n = 0 ;
            $jml = 0;
            while($dbr2 = $this->getrow($dbd2)){
                $n++;
                $this->escpos->teks("|".str_pad($n,3," ",STR_PAD_LEFT).
						 //"|".str_pad("Kode",12," ",STR_PAD_BOTH).
						 "|".str_pad($dbr2['ketrekening'],53," ",STR_PAD_RIGHT).
                         "|".str_pad(string_2s($dbr2['nominal']),20," ",STR_PAD_LEFT)."|");
                $jml += $dbr2['nominal'];
            }
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("Total",57," ",STR_PAD_BOTH).
                         "|".str_pad(string_2s($jml),20," ",STR_PAD_LEFT)."|");
            $this->escpos->teks(str_pad("",$cpl,"=",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            
			 
		}
        
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $now  = date_2b($tgl) ;
        
        $this->escpos->teks(str_pad(" ",27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad($this->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'],27," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad($ttd,27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad("Bag. Keuangan",27," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("(...............)",27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad("(...............)",27," ",STR_PAD_BOTH));
        
        // foreach($vaprint as $key => $val){
        //     $this->escpos->teks($val);
        // }
        $this->escpos->cetak(true);
    }

    public function cetakmu($faktur){
        $this->escpos->connect();
        $cpl = 80 ;
        $field = "t.faktur,t.cabang,t.tgl,k.keterangan as ketkdtr,t.kodetransaksi,t.jumlah,t.customer,s.nama as namacustomer,k.dk";
        $where = "t.faktur = '$faktur'";
        $join  = "left join kodetransaksi k on k.kode = t.kodetransaksi";
        $join  .= " left join customer s on s.kode = t.customer";
        $dbd   = $this->select("uangmuka_mutasi_total t", $field, $where, $join) ;
        $ttd = "Yang Membayar";
        $tgl = date("Y-m-d");
        if($dbr = $this->getrow($dbd)){
            $arrcab = $this->GetDataCabang($dbr['cabang']);
            $this->escpos->teks(str_pad($arrcab['nama'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad($arrcab['alamat'],55," ",STR_PAD_RIGHT). str_pad("Bukti Uangmuka",25," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],80," ",STR_PAD_RIGHT));

			
			$this->escpos->teks(str_pad("No. Faktur     : ".$faktur,80," ",STR_PAD_RIGHT));
            
            $pembyr = "Diterima Dari : ";
            if($dbr['dk'] == "d"){
                $pembyr = "Dibayar Kpd : ";
                $ttd = "Yang Menerima";
            }
            $dbr['namacustomer'] = substr($dbr['namacustomer'],0,40);
			$this->escpos->teks(str_pad("Tanggal        : ".date_2d($dbr['tgl']),30," ",STR_PAD_RIGHT).
                         str_pad($pembyr.$dbr['namacustomer'],50," ",STR_PAD_RIGHT));
            $tgl = $dbr['tgl'];
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("No",3," ",STR_PAD_BOTH).
						 //"|".str_pad("Kode",12," ",STR_PAD_BOTH).
						 "|".str_pad("Keterangan",53," ",STR_PAD_BOTH).
						 "|".str_pad("Jumlah",20," ",STR_PAD_BOTH)."|");
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));

            $f2 = "d.faktur,r.kode,r.keterangan as ketrekening,d.jumlah as nominal,d.rekening,
                r.keterangan as ketrek";
            $w2 = "d.faktur = '$faktur'";
            $j2  = "left join keuangan_rekening r on r.kode = d.rekening";
            $dbd2   = $this->select("uangmuka_mutasi_detail d", $f2, $w2, $j2) ;
            $n = 0 ;
            $jml = 0;
            while($dbr2 = $this->getrow($dbd2)){
                $n++;
                $this->escpos->teks("|".str_pad($n,3," ",STR_PAD_LEFT).
						 //"|".str_pad("Kode",12," ",STR_PAD_BOTH).
						 "|".str_pad($dbr2['ketrekening'],53," ",STR_PAD_RIGHT).
                         "|".str_pad(string_2s($dbr2['nominal']),20," ",STR_PAD_LEFT)."|");
                $jml += $dbr2['nominal'];
            }
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("Total",57," ",STR_PAD_BOTH).
                         "|".str_pad(string_2s($jml),20," ",STR_PAD_LEFT)."|");
            $this->escpos->teks(str_pad("",$cpl,"=",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            
			 
		}
        
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $now  = date_2b($tgl) ;
        
        $this->escpos->teks(str_pad(" ",27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad($this->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'],27," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad($ttd,27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad("Bag. Keuangan",27," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("(...............)",27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad("(...............)",27," ",STR_PAD_BOTH));
        
        // foreach($vaprint as $key => $val){
        //     $this->escpos->teks($val);
        // }
        $this->escpos->cetak(true);
    }

    public function cetakdp($faktur){
        $this->escpos->connect();
        $cpl = 80 ;
        $field = "t.faktur,t.cabang,t.tgl,k.keterangan as ketkdtr,t.kodetransaksi,t.jumlah,t.customer,s.nama as namacustomer,k.dk";
        $where = "t.faktur = '$faktur'";
        $join  = "left join kodetransaksi k on k.kode = t.kodetransaksi";
        $join  .= " left join customer s on s.kode = t.customer";
        $dbd   = $this->select("deposit_mutasi_total t", $field, $where, $join) ;
        $ttd = "Yang Membayar";
        $tgl = date("Y-m-d");
        if($dbr = $this->getrow($dbd)){
            $arrcab = $this->GetDataCabang($dbr['cabang']);
            $this->escpos->teks(str_pad($arrcab['nama'],80," ",STR_PAD_RIGHT));
			$this->escpos->teks(str_pad($arrcab['alamat'],55," ",STR_PAD_RIGHT). str_pad("Bukti Deposit",25," ",STR_PAD_BOTH));
			$this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],80," ",STR_PAD_RIGHT));

			
			$this->escpos->teks(str_pad("No. Faktur     : ".$faktur,80," ",STR_PAD_RIGHT));
            
            $pembyr = "Diterima Dari : ";
            if($dbr['dk'] == "d"){
                $pembyr = "Dibayar Kpd : ";
                $ttd = "Yang Menerima";
            }
            $dbr['namacustomer'] = substr($dbr['namacustomer'],0,40);
			$this->escpos->teks(str_pad("Tanggal        : ".date_2d($dbr['tgl']),30," ",STR_PAD_RIGHT).
                         str_pad($pembyr.$dbr['namacustomer'],50," ",STR_PAD_RIGHT));
            $tgl = $dbr['tgl'];
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("No",3," ",STR_PAD_BOTH).
						 //"|".str_pad("Kode",12," ",STR_PAD_BOTH).
						 "|".str_pad("Keterangan",53," ",STR_PAD_BOTH).
						 "|".str_pad("Jumlah",20," ",STR_PAD_BOTH)."|");
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));

            $f2 = "d.faktur,r.kode,r.keterangan as ketrekening,d.jumlah as nominal,d.rekening,
                r.keterangan as ketrek";
            $w2 = "d.faktur = '$faktur'";
            $j2  = "left join keuangan_rekening r on r.kode = d.rekening";
            $dbd2   = $this->select("deposit_mutasi_detail d", $f2, $w2, $j2) ;
            $n = 0 ;
            $jml = 0;
            while($dbr2 = $this->getrow($dbd2)){
                $n++;
                $this->escpos->teks("|".str_pad($n,3," ",STR_PAD_LEFT).
						 //"|".str_pad("Kode",12," ",STR_PAD_BOTH).
						 "|".str_pad($dbr2['ketrekening'],53," ",STR_PAD_RIGHT).
                         "|".str_pad(string_2s($dbr2['nominal']),20," ",STR_PAD_LEFT)."|");
                $jml += $dbr2['nominal'];
            }
            $this->escpos->teks(str_pad("",$cpl,"-",STR_PAD_BOTH));
            $this->escpos->teks("|".str_pad("Total",57," ",STR_PAD_BOTH).
                         "|".str_pad(string_2s($jml),20," ",STR_PAD_LEFT)."|");
            $this->escpos->teks(str_pad("",$cpl,"=",STR_PAD_BOTH));
            $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
            
			 
		}
        
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $now  = date_2b($tgl) ;
        
        $this->escpos->teks(str_pad(" ",27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad($this->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'],27," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad($ttd,27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad("Bag. Keuangan",27," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
        $this->escpos->teks(str_pad("(...............)",27," ",STR_PAD_BOTH).str_pad(" ",26," ",STR_PAD_BOTH).str_pad("(...............)",27," ",STR_PAD_BOTH));
        
        // foreach($vaprint as $key => $val){
        //     $this->escpos->teks($val);
        // }
        $this->escpos->cetak(true);
    }
}
?>