<?php
class Perhitungan_m extends Bismillah_Model
{

    public function getkepenyusutan($tglawal, $tgl, $lama)
    {

        $ntgl = date_2t($tgl);
        $njthtmp = date_2t($tglawal);
        $ntglawal = date_2t($tglawal);
        $ke = 0;

        while ($njthtmp < $ntgl) {
            $ke++;
            $njthtmp = nextmonth($ntglawal, $ke);
        }

        return $ke;
    }

    public function getpenyusutan($kode, $tgl)
    {
        $tgl = date_2s($tgl);
        $va = array('awal' => 0, 'bulan ini' => 0, 'akhir' => 0, 'hargaperolehan' => 0, 'ke' => 0, 'jadwal' => array());
        $cfield = "kode,tglperolehan,hargaperolehan,residu,jenispenyusutan,lama,penyusutanperbulan,tarifpenyusutan,tglhabis";
        $dbData = $this->select("aset", $cfield, "Kode = '$kode' and (tglhabis > '$tgl' or tglhabis = '0000-00-00')");
        if ($dbRow = $this->getrow($dbData)) {
            $va['hargaperolehan'] = $dbRow['hargaperolehan'];
            $tglawal = date_bom($dbRow['tglperolehan']);
            $lama = $dbRow['lama'] * 12;
            $jenispenyusutan = $dbRow['jenispenyusutan'];
            $residu = $dbRow['residu'];
            $hp = $dbRow['hargaperolehan'];
            $ke_tot = 0 ;
            //cek terlebih dahulu di tabel perubahan aset
            $this->db->where('a.kode',$kode);
            $this->db->where('a.tgl <',$tgl);
            $this->db->where('a.status',"1");
            $field = "a.*";
            $db2 = $this->db->select($field)->from("aset_perubahan a")
                    ->order_by("a.tgl desc")
                    ->limit("1") 
                    ->get(); 
            $r2  = $db2->row_array();
            if(isset($r2)){
                $d_akhir = json_decode($r2['dataakhir'],true);
                $va['hargaperolehan'] = $d_akhir['hargaperolehan_akhir'];
                $tglawal = date_bom($r2['tgl']);
                $lama = $d_akhir['lama_bulan_sisa'];
                $jenispenyusutan = $d_akhir['jenispenyusutan'];
                $residu = $d_akhir['residu'];
                $hp = $d_akhir['nilaibuku'];
                $va['awal'] = $va['hargaperolehan'] - $d_akhir['nilaibuku'];
                $ke_tot += $d_akhir['peny_ke'];
            }
            
            
            if ($jenispenyusutan == "1") { // metode garis lurus
                // print_r($va);
                
                $tgl = date_eom($tgl);
                $ke = $this->getkepenyusutan($tglawal, $tgl, $lama);
                $va['ke'] = $ke_tot + $ke;
                //if($dbRow['Status'] == 1 && $dbRow['TglPerolehan'] <= Date2String($dTgl)) $nKe ++ ;
                $hargaperolehan = $hp - $residu;
                $hargaperolehantot = $va['hargaperolehan'] - $residu;
                $penyusutan = round(devide($hargaperolehan, $lama), 0);
                if ($ke == 0 and $lama > 0) {
                    $va['awal'] += 0;
                    $va['bulan ini'] = 0;
                    $va['akhir'] = 0;
                } else if ($ke < $lama) {
                    $va['awal'] += $penyusutan * ($ke - 1);
                    $va['bulan ini'] = $penyusutan;
                    $va['akhir'] = $va['awal'] + $va['bulan ini'];
                } else if ($ke == $lama) {
                    $va['awal'] += $penyusutan * ($ke - 1);
                    $va['akhir'] = $hargaperolehantot;
                    $va['bulan ini'] = $va['akhir'] - $va['awal'];
                } else if ($lama == 0) { // untuk yg tidak di susustkan
                    $va['awal'] += 0;
                    $va['akhir'] = 0;
                    $va['bulan ini'] = 0;

                } else {
                    $va['awal'] = $hargaperolehantot;
                    $va['akhir'] = $hargaperolehantot;
                    $va['bulan ini'] = 0;
                }

                //echo(" Ke " . $nKe . " ++ " . $va ['Awal'] . " => " . $va ['Bulan Ini']  . " -> " . $va ['Akhir'] . " ; ") ;
            } else {
                $ntglperolehan = date_2t($dbRow['tglperolehan']);
                $lama = $dbRow['lama'] * 12; //- intval(date("m",$nTglPerolehan)) + 1 ;
                $lamathn = 0;
                $saldo = $dbRow['hargaperolehan'] - $dbRow['residu'];
                $penyusutan = 0;
                for ($n = 1; $n <= $lama; $n++) {
                    $djthtmp = date_eom(date("d-m-Y", nextmonth($ntglperolehan, $n - 1)));
                    if ($lamathn == 0 || intval(date("m", date_2t($djthtmp))) == 1) {
                        $lamathn++;
                        if ($lamathn <= $dbRow['lama']) {
                            $penyusutan = round(($saldo * $dbRow['tarifpenyusutan']) / 1200);
                        } else {
                            $pembagi = intval(date("m", $ntglperolehan)) - 1;
                            $penyusutan = round($saldo / $pembagi, 0);
                        }
                    }
                    if ($n == $lama) {
                        $penyusutan = $saldo - $dbRow['residu'];
                    }

                    $saldo -= $penyusutan;
                    $va['jadwal'][$n] = array("ke" => $n, "jthtmp" => $djthtmp, "penyusutan" => $penyusutan, "nilai buku" => $saldo);
                }
                if (date_2t($dbRow['tglperolehan']) <= date_2t($tgl)) {
                    $ke = $this->getkepenyusutan($dbRow['tglperolehan'], $tgl, 10000) + 1;
                    $va['ke'] = $ke;
                    if (isset($va['jadwal'][$ke])) {
                        $penyusutan = $va['jadwal'][$ke]['penyusutan'];
                        $va['bulan ini'] = $penyusutan;
                        $va['akhir'] = $dbRow['hargaperolehan'] - $va['jadwal'][$ke]['nilai buku'];
                        $va['awal'] = $va['akhir'] - $va['bulan ini'];
                    } else if ($ke > $lama) {
                        $va['bulan ini'] = 0;
                        $va['awal'] = $dbRow['hargaperolehan'] - $dbRow['residu'];
                        $va['akhir'] = $va['awal'];
                    }
                }
            }
        }
        return $va;
    }
    public function getsaldoakhirstock($cKode, $dTgl, $gudang = '', $cabang = '')
    {
        $dTgl = date_2s($dTgl);
        $cField = "ifnull(SUM(debet-kredit),0) as Saldo";
        $saldo = 0;
        $where = "stock = '$cKode' and Tgl <= '$dTgl'";
        if ($gudang != '') {
            $where .= " and gudang = '$gudang'";
        }

        if ($cabang != '') {
            $where .= " and cabang = '$cabang'";
        }

        $dbData = $this->select("stock_kartu", $cField, $where);
        if ($dbr = $this->getrow($dbData)) {
            $saldo = $dbr['Saldo'];
        }
        return $saldo;
    }

    public function getsaldohpstock($kode, $tgl, $cabang)
    {
        $tgl = date_2s($tgl);
        $arrhp = array();
        $arr = array("hptot" => 0, "detailhp" => array());
        $tglakhir = "0000-00-00";
        $field = "tgl";
        $where = "tgl <= '$tgl' and kode = '$kode' and cabang = '$cabang'";
        $dbd = $this->select("stock_hp", $field, $where, "", "", "tgl desc", "1");
        if ($dbr = $this->getrow($dbd)) {
            $tglakhir = $dbr['tgl'];
        }

        //lihat saldo kemarin
        $field = "faktur,tgl,kode,cabang,qty,hp,datetime";
        $where = "tgl = '$tglakhir' and kode = '$kode' and cabang = '$cabang'";
        $dbd = $this->select("stock_hp", $field, $where, "", "", "id asc");
        while ($dbr = $this->getrow($dbd)) {
            $arrhp[] = $dbr;
        }
        //hitung hp
        ksort($arrhp);
        $arr['detailhp'] = $arrhp;
        foreach ($arrhp as $key => $val) {
            $arr['hptot'] += $val['qty'] * $val['hp'];
        }

        return $arr;
    }

    public function gethistorydebetsaldo($kode, $tgl, $cabang, $saldo,$not_fkt='')
    {
        $tgl = date_2s($tgl);
        $arrhp = array();
        $field = "faktur,tgl,stock,debet,datetime_insert,id,hp";
        $where = "tgl <= '$tgl' and stock = '$kode' and cabang = '$cabang' and debet > 0";
        //$where = "tgl <= '$tgl' and stock = '$kode' and cabang = '$cabang' and faktur not like 'SP%' and debet > 0";
        if($not_fkt <> ''){
             $where .= " and faktur <> '$not_fkt'";
        }
        $dbd = $this->select("stock_kartu", $field, $where, "", "", "tgl desc,datetime_insert desc,id desc");
        while ($dbr = $this->getrow($dbd)) {
            if ($saldo > 0) {
                $qty = min($saldo, $dbr['debet']);
                $arrhp[] = array("faktur" => $dbr['faktur'], "tgl" => $dbr['tgl'], "qty" => $qty, "hp" => $dbr['hp']);
                $saldo -= $qty;
            } else {
                break;
            }
        }
        
        if($saldo > 0){// untuk data yang ternyata tidak masuk di pembelian tetapi ketika stock opname ditemukan
            $arrhp[] = array("faktur" => "kelebihan", "tgl" => $tgl, "qty" => $saldo, "hp" => 0.00);
        }

        krsort($arrhp);
        return $arrhp;
    }
    public function gethpstock($kode, $tglawal, $tglakhir, $saldo, $qtyambil, $cabang, $caraperhitungan, $periode)
    {
        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);

        $saldostock = $saldo;
        $qtydikeluarkan = $qtyambil;
        $arr = array("hp" => 0, "hptot" => 0, "detailhp" => array(), "hpdikeluarkan" => 0);
        if ($caraperhitungan == "F") {
            //if($periode == "Hari"){
            $arrhp = array();
            //untuk mengambil saldo di tgl terakhir
            $tglsebelumnya = "0000-00-00";
            $field = "tgl";
            $where = "tgl < '$tglawal' and kode = '$kode' and cabang = '$cabang'";
            $dbd = $this->select("stock_hp", $field, $where, "", "", "tgl desc", "1");
            if ($dbr = $this->getrow($dbd)) {
                $tglsebelumnya = $dbr['tgl'];
            }

            //lihat saldo kemarin
            $field = "faktur,tgl,kode,cabang,qty,hp,datetime";
            $where = "tgl = '$tglsebelumnya' and kode = '$kode' and cabang = '$cabang'";
            $dbd = $this->select("stock_hp", $field, $where, "", "", "id asc");
            while ($dbr = $this->getrow($dbd)) {
                $arrhp[] = $dbr;
            }

            //lihat pembelian, produksi hasil, retur jual per tgl periode
            $tglsave = $tglsebelumnya;
            $field = "faktur,tgl,stock as kode,cabang,debet as qty,hp,datetime_insert datetime";
            $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and stock = '$kode' and cabang = '$cabang' and debet > 0 ";
            $dbd = $this->select("stock_kartu", $field, $where, "", "", "tgl,id asc,datetime asc");
            $masukhariini = 0;
            while ($dbr = $this->getrow($dbd)) {
                $arrhp[] = $dbr;
                $masukhariini += $dbr['qty'];
                $tglsave = $tglakhir;
            }

            //lihat apakah ada mutasi keluar tgl skrg
            $field = "stock";
            $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and stock = '$kode' and cabang = '$cabang'";
            $dbd = $this->select("produksi_bb", $field, $where, "", "", "id asc", "1");
            if ($dbr = $this->getrow($dbd)) {
                $tglsave = $tglakhir;
            }

            $field = "d.stock";
            $where = "t.tgl >= '$tglawal' and t.tgl <= '$tglakhir' and d.stock = '$kode' and t.cabang = '$cabang'";
            $join = "left join pembelian_retur_total t on t.faktur = d.faktur";
            $dbd = $this->select("pembelian_retur_detail d", $field, $where, $join, "", "d.id asc", "1");
            if ($dbr = $this->getrow($dbd)) {
                $tglsave = $tglakhir;
            }

            $field = "d.stock";
            $where = "t.tgl >= '$tglawal' and t.tgl <= '$tglakhir' and d.stock = '$kode' and t.cabang = '$cabang'";
            $join = "left join penjualan_total t on t.faktur = d.faktur";
            $dbd = $this->select("penjualan_detail d", $field, $where, $join, "", "d.id asc", "1");
            if ($dbr = $this->getrow($dbd)) {
                $tglsave = $tglakhir;
            }

            // $field = "d.kode";
            // $where = "t.tgl >= '$tglawal' and t.tgl <= '$tglakhir' and d.kode = '$kode' and t.cabang = '$cabang'";
            // $join = "left join stock_opname_posting_total t on t.faktur = d.faktur";
            // $dbd = $this->select("stock_opname_posting_detail d", $field, $where, $join, "", "d.id asc", "1");
            // if ($dbr = $this->getrow($dbd)) {
            //     $tglsave = $tglakhir;
            // }

            $field = "d.stock";
            $where = "t.tgl >= '$tglawal' and t.tgl <= '$tglakhir' and d.stock = '$kode' and t.cabang = '$cabang'";
            $join = "left join produksi_total t on t.faktur = d.fakturproduksi";
            $dbd = $this->select("produksi_produk d", $field, $where, $join, "", "d.id asc", "1");
            if ($dbr = $this->getrow($dbd)) {
                $tglsave = $tglakhir;
            }

            $field = "d.stock";
            $where = "d.tgl >= '$tglawal' and d.tgl <= '$tglakhir' and d.stock = '$kode' and d.cabang = '$cabang'";
            $join = "";
            $dbd = $this->select("stock_adj d", $field, $where, $join, "", "d.id asc", "1");
            if ($dbr = $this->getrow($dbd)) {
                $tglsave = $tglakhir;
            }

            $field = "d.stock";
            $where = "t.tgl >= '$tglawal' and t.tgl <= '$tglakhir' and d.stock = '$kode' and t.cabang = '$cabang'";
            $join = "left join stock_keluar_total t on t.faktur = d.faktur";
            $dbd = $this->select("stock_keluar_detail d", $field, $where, $join, "", "d.id asc", "1");
            if ($dbr = $this->getrow($dbd)) {
                $tglsave = $tglakhir;
            }

            if ($tglsave == $tglakhir) {
                krsort($arrhp);
                $arrsave = array();
                foreach ($arrhp as $key => $val) {
                    if ($saldo > 0) {
                        $qtypertr = min($saldo, $val['qty']);
                        if ($qtypertr > 0) {
                            $val['qty'] = $qtypertr;
                            $arrsave[] = $val;
                        }
                        $saldo -= $qtypertr;
                    }
                }

                if ($saldostock <= 0) {
                    $arrsave[] = array("faktur" => "kosong", "tgl" => $tglsave, "kode" => $kode, "cabang" => $cabang, "qty" => 0, "hp" => 0, "datetime" => date("Y-m-d H:i:s"));
                }
                //if($kode == "1810000003") print_r($arrsave);
                krsort($arrsave);
                //mengurangsi saldo dengan stock di ambil
                foreach ($arrsave as $key => $val) {
                    if ($qtydikeluarkan > 0) {
                        //$qtydikeluarkan = max($qtydikeluarkan,0);
                        //if($qtydikeluarkan > $saldo) $qtydikeluarkan = $saldo;
                        $qtybts = min($val['qty'], $qtydikeluarkan);

                        $arr['hpdikeluarkan'] += $qtybts * $val['hp'];
                        $arrsave[$key]['qty'] = $val['qty'] - $qtybts;
                        $qtydikeluarkan -= $qtybts;
                    }

                }

                $this->delete("stock_hp", "tgl = '$tglsave' and kode = '$kode'");

                foreach ($arrsave as $key => $val) {
                    $val['tgl'] = $tglsave;
                    if ($val['qty'] >= 0) {
                        if ($val['qty'] == 0) {
                            $val['hp'] = 0;
                        }

                        $this->insert("stock_hp", $val);
                    }
                }

                $arrhp = $arrsave;
            }

            //hitung hp
            krsort($arrhp);
            $arr['detailhp'] = $arrhp;
            foreach ($arrhp as $key => $val) {
                $arr['hptot'] += $val['qty'] * $val['hp'];
            }

            //}
        }
        return $arr;
    }

    public function gethpbypj($kode, $fktpj)
    {
        $hp = 0;
        if ($fktpj != "") {
            $where = array();
            $where[] = "d.faktur = '$fktpj' and d.stock = '$kode'";
            $where = implode(" AND ", $where);
            $join = "";
            $group = "d.stock";
            $dbd = $this->select("penjualan_detail d", "d.hp", $where, $join, $group, "d.stock ASC");
            if ($dbr = $this->getrow($dbd)) {
                $hp = $dbr['hp'];
            }
        }
        return $hp;
    }

    public function getdetailsaldostock($kode, $tgl, $cabang)
    {
        $tgl = date_2s($tgl);
        $arr = array("hp" => 0, "hptot" => 0, "detail" => array(), "qtytot" => 0);

        $arrhp = array();
        //untuk mengambil saldo di tgl terakhir
        $tglsebelumnya = "0000-00-00";
        $field = "tgl";
        $where = "tgl <= '$tgl' and kode = '$kode' and cabang = '$cabang'";
        $dbd = $this->select("stock_hp", $field, $where, "", "", "tgl desc", "1");
        if ($dbr = $this->getrow($dbd)) {
            $tglsebelumnya = $dbr['tgl'];
        }

        //lihat saldo
        $field = "faktur,tgl,kode,cabang,qty,hp,datetime";
        $where = "tgl = '$tglsebelumnya' and kode = '$kode' and cabang = '$cabang'";
        $dbd = $this->select("stock_hp", $field, $where, "", "", "id asc");
        while ($dbr = $this->getrow($dbd)) {
            $arrhp[] = $dbr;
        }

        $arr['detailhp'] = $arrhp;
        foreach ($arrhp as $key => $val) {
            $arr['hptot'] += $val['qty'] * $val['hp'];
            $arr['qtytot'] += $val['qty'];

        }

        return $arr;
    }

    public function GetSaldoAkhirHutang($supplier, $tgl, $fkt = '', $jenis = 'H', $fktlike = false, $kdtr = '')
    {
        $tgl = date_2s($tgl);
        $cField = "IFNULL(SUM(Debet-Kredit),0) as Saldo";
        if ($jenis == 'P') {
            $cField = "IFNULL(SUM(Kredit-Debet),0) as Saldo";
        }

        $saldo = 0;
        $where = "supplier = '$supplier' and Tgl <= '$tgl'";
        if ($fkt != '') {
            if ($fktlike) {
                $where .= " and fkt like '$fkt%'";
            } else {
                $where .= " and fkt = '$fkt'";
            }
        }
        if ($kdtr != '') {
            $where .= " and kodetransaksi = '$kdtr'";
        }

        if ($jenis != '') {
            $where .= " and jenis = '$jenis'";
        }

        $dbData = $this->select("hutang_kartu", $cField, $where);
        if ($dbr = $this->getrow($dbData)) {
            $saldo = $dbr['Saldo'];
        }
        return $saldo;
    }

    public function getsaldohutangdetail($supplier,$tgl, $fkt = '', $jenis = 'H', $fktlike = false, $kdtr = ''){
        $tgl = date_2s($tgl);
        $arrreturn = array();
        $field = "p.faktur,p.tgl,p.jthtmp,p.fktpo,p.subtotal,p.ppn,p.diskon,p.total";
        $field .= ",ifnull(sum(k.kredit),0) as pembayaran,IFNULL(SUM(k.debet-k.kredit),0) as saldo";
        if ($jenis == 'P') {
            $field .= ",ifnull(sum(k.debet),0) as pembayaran,IFNULL(SUM(k.kredit-k.debet),0) as saldo";
        }

        $where = "k.supplier = '$supplier' and k.tgl <= '$tgl'";
        if ($fkt != '') {
            if ($fktlike) {
                $where .= " and k.fkt like '$fkt%'";
            } else {
                $where .= " and k.fkt = '$fkt'";
            }
        }
        if ($kdtr != '') {
            $where .= " and k.kodetransaksi = '$kdtr'";
        }

        if ($jenis != '') {
            $where .= " and k.jenis = '$jenis'";
        }

        $join = "left join pembelian_total p on p.faktur = k.fkt";
        $dbData = $this->select("hutang_kartu k", $field, $where,$join,"k.fkt having saldo <> 0","p.tgl asc");
        while ($dbr = $this->getrow($dbData)) {
            $arrreturn[] = $dbr;
        }
        return $arrreturn;
    }

    public function GetSaldoAkhirpiutang($customer, $tgl, $fkt = '', $jenis = 'P')
    {
        $tgl = date_2s($tgl);
        $cField = "IFNULL(SUM(Debet-Kredit),0) as Saldo";
        if ($jenis == 'U' || $jenis == 'D') {
            $cField = "IFNULL(SUM(Kredit-Debet),0) as Saldo";
        }

        $saldo = 0;
        $where = "customer = '$customer' and Tgl <= '$tgl'";
        if ($fkt != '') {
            $where .= " and fkt = '$fkt'";
        }

        if ($jenis != '') {
            $where .= " and jenis = '$jenis'";
        }

        $dbData = $this->select("piutang_kartu", $cField, $where);
        if ($dbr = $this->getrow($dbData)) {
            $saldo = $dbr['Saldo'];
        }
        return $saldo;
    }

    /**Keuangan**/
    public function getsaldoawal($tgl, $rekening, $rekening2 = '', $penihilan = "T", $cabang = array())
    {
        
        $c = substr($rekening, 0, 1);
        $tgl = date_2s($tgl);
        $tglkemarin = date("Y-m-d", strtotime($tgl) - (24 * 60 * 60));
        if ($c == "1" || $c == "5" || $c == "6" || $c == "8" || $c == "9") {
            $f = "ifnull(sum(debet-kredit),0) saldo";
        } else {
            $f = "ifnull(sum(kredit-debet),0) saldo";
        }
        $saldo = 0;
        if ($rekening2 != "") {
            $this->db->where('rekening >=',$rekening);
            $this->db->where('rekening <=',$rekening2);

        } else {
            $this->db->like('rekening',$rekening,'after');

        }
        if ($penihilan == "T") {
            $thn = date("Y", strtotime($tgl));
            $fktpenihilan = "TH" . $thn;
            $this->db->not_like('faktur',$fktpenihilan,'after');

        }

        $this->db->where('tgl <=',$tglkemarin);

        if (!empty($cabang)){
            $this->db->group_start();
            foreach($cabang as $kcab){
                $this->db->or_where('cabang',$kcab);
            }
            $this->db->group_end();

        }else{
            $this->db->where('cabang',getsession($this, "cabang"));
        }
        
        $db2 = $this->db->select($f)->from("keuangan_bukubesar")
                ->get(); 
        $r2  = $db2->row_array();
        if(isset($r2)){
            $saldo = $r2['saldo'];
        }
        return $saldo;
    }
    public function getsaldo($tgl, $rekening, $rekening2 = '', $penihilan = "T", $cabang = array())
    {
        $c = substr($rekening, 0, 1);
        $tgl = date_2s($tgl);
        if ($c == "1" || $c == "5" || $c == "6" || $c == "8" || $c == "9") {
            $f = "ifnull(sum(debet-kredit),0) saldo";
        } else {
            $f = "ifnull(sum(kredit-debet),0) saldo";
        }

        $saldo = 0;
        if ($rekening2 != "") {
            $this->db->where('rekening >=',$rekening);
            $this->db->where('rekening <=',$rekening2);

        } else {
            $this->db->like('rekening',$rekening,'after');

        }
        if ($penihilan == "T") {
            $thn = date("Y", strtotime($tgl));
            $fktpenihilan = "TH" . $thn;
            $this->db->not_like('faktur',$fktpenihilan,'after');

        }

        $this->db->where('tgl <=',$tgl);


        if (!empty($cabang)){
            $this->db->group_start();
            foreach($cabang as $kcab){
                $this->db->or_where('cabang',$kcab);
            }
            $this->db->group_end();

        }else{
            $this->db->where('cabang',getsession($this, "cabang"));
        }
        
        $db2 = $this->db->select($f)->from("keuangan_bukubesar")
                ->get(); 
        $r2  = $db2->row_array();
        if(isset($r2)){
            $saldo = $r2['saldo'];
        }
        return $saldo;
    }

    public function getdebet($tglawal, $tglakhir, $rekening, $fakturlike = '', $rekening2 = '', $penihilan = "T",$lfktnotlike=false, $cabang = array())
    {
        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);
        $f = "ifnull(sum(debet),0) debet";

        $saldo = 0;
        if ($rekening2 != "") {
            $this->db->where('rekening >=',$rekening);
            $this->db->where('rekening <=',$rekening2);

        } else {
            $this->db->like('rekening',$rekening,'after');

        }

        if ($penihilan == "T") {
            $thn = date("Y", strtotime($tglakhir));
            $fktpenihilan = "TH" . $thn;
            $this->db->not_like('faktur',$fktpenihilan,'after');

        }

        $this->db->where('tgl <=',$tglakhir);
        $this->db->where('tgl >=',$tglawal);


        if ($fakturlike != '') {
            if($lfktnotlike){
                $this->db->not_like('faktur',$fakturlike,'after');
            }else{
                $this->db->like('faktur',$fakturlike,'after');
            }    
        }

        if (!empty($cabang)){
            $this->db->group_start();
            foreach($cabang as $kcab){
                $this->db->or_where('cabang',$kcab);
            }
            $this->db->group_end();

        }else{
            $this->db->where('cabang',getsession($this, "cabang"));
        }

        $db2 = $this->db->select($f)->from("keuangan_bukubesar")
                ->get(); 
        $r2  = $db2->row_array();
        if(isset($r2)){
            $saldo = $r2['debet'];
        }

        return $saldo;
    }

    public function getkredit($tglawal, $tglakhir, $rekening, $fakturlike = '', $rekening2 = '', $penihilan = "T",$lfktnotlike=false, $cabang = array())
    {
        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);
        $f = "ifnull(sum(kredit),0) kredit";

        $saldo = 0;
        if ($rekening2 != "") {
            $this->db->where('rekening >=',$rekening);
            $this->db->where('rekening <=',$rekening2);

        } else {
            $this->db->like('rekening',$rekening,'after');

        }

        if ($penihilan == "T") {
            $thn = date("Y", strtotime($tglakhir));
            $fktpenihilan = "TH" . $thn;
            $this->db->not_like('faktur',$fktpenihilan,'after');

        }

        $this->db->where('tgl <=',$tglakhir);
        $this->db->where('tgl >=',$tglawal);


        if ($fakturlike != '') {
            if($lfktnotlike){
                $this->db->not_like('faktur',$fakturlike,'after');
            }else{
                $this->db->like('faktur',$fakturlike,'after');
            }    
        }

        if (!empty($cabang)){
            $this->db->group_start();
            foreach($cabang as $kcab){
                $this->db->or_where('cabang',$kcab);
            }
            $this->db->group_end();

        }else{
            $this->db->where('cabang',getsession($this, "cabang"));
        }

        $db2 = $this->db->select($f)->from("keuangan_bukubesar")
                ->get(); 
        $r2  = $db2->row_array();
        if(isset($r2)){
            $saldo = $r2['kredit'];
        }
        return $saldo;
    }

    public function loadrekening($rekawal, $rekakhir)
    {
        $where = array();
        $where[] = "kode >= '$rekawal' and kode <= '$rekakhir'";
        $where = implode(" AND ", $where);
        $field = "kode,keterangan,jenis";
        $join = "";
        $dbd = $this->select("keuangan_rekening", $field, $where, $join, "", "kode ASC");
        return $dbd;
    }

    public function getlr($tglawal, $tglakhir, $level = 6,$penihilan = "T", $cabang = array())
    {
        $tglkemarin = date("d-m-Y", strtotime($tglawal) - (24 * 60 * 60));
        $n = 0;
        //pend opr
        $totpendopr = array("saldoawal" => 0, "debet" => 0, "kredit" => 0, "saldoakhir" => 0, "saldoakhirperiod" => 0);
        $rekawal = $this->getconfig("rekpendoprawal");
        $rekakhir = $this->getconfig("rekpendoprakhir");
        $dbd = $this->loadrekening($rekawal, $rekakhir);
        while ($dbr = $this->getrow($dbd)) {
            $vs = $dbr;
            $vs['saldoawal'] = $this->getsaldoawal($tglawal, $vs['kode'],"",$penihilan,$cabang);
            $vs['debet'] = $this->getdebet($tglawal, $tglakhir, $vs['kode'],"","",$penihilan,false,$cabang);
            $vs['kredit'] = $this->getkredit($tglawal, $tglakhir, $vs['kode'],"","",$penihilan,false,$cabang);
            $vs['saldoakhir'] = $vs['saldoawal'] + $vs['kredit'] - $vs['debet'];
            $vs['saldoakhirperiod'] = $vs['kredit'] - $vs['debet'];

            //sum tot
            if ($vs['jenis'] == "D") {
                $totpendopr["saldoawal"] += $vs['saldoawal'];
                $totpendopr["debet"] += $vs['debet'];
                $totpendopr["kredit"] += $vs['kredit'];
                $totpendopr["saldoakhir"] += $vs['saldoakhir'];
                $totpendopr["saldoakhirperiod"] += $vs['saldoakhirperiod'];
            }
            $vs['saldoawal'] = string_2sz($vs['saldoawal']);
            $vs['debet'] = string_2sz($vs['debet']);
            $vs['kredit'] = string_2sz($vs['kredit']);
            $vs['saldoakhir'] = string_2sz($vs['saldoakhir']);
            $vs['saldoakhirperiod'] = string_2sz($vs['saldoakhirperiod']);

            $arrkd = explode(".", $vs['kode']);
            $levelkd = count($arrkd);
            //bold text
            if ($vs['jenis'] == "I" and $level > $levelkd) {
                foreach ($vs as $key => $val) {
                    if ($key != "jenis") {
                        $vs[$key] = "<b>" . $val . "</b>";
                    }

                }
            }
            //unset($vs['jenis']);

            if ($level >= $levelkd) {
                $vare[$n++] = $vs;
            }
        }

        //HPP
        $tothpp = array("saldoawal" => 0, "debet" => 0, "kredit" => 0, "saldoakhir" => 0, "saldoakhirperiod" => 0);
        $rekawal = $this->getconfig("rekhppawal");
        $rekakhir = $this->getconfig("rekhppakhir");
        $dbd = $this->loadrekening($rekawal, $rekakhir);
        while ($dbr = $this->getrow($dbd)) {
            $vs = $dbr;
            $vs['saldoawal'] = $this->getsaldoawal($tglawal, $vs['kode'],"",$penihilan,$cabang);
            $vs['debet'] = $this->getdebet($tglawal, $tglakhir, $vs['kode'],"","",$penihilan,false,$cabang);
            $vs['kredit'] = $this->getkredit($tglawal, $tglakhir, $vs['kode'],"","",$penihilan,false,$cabang);
            $vs['saldoakhir'] = $vs['saldoawal'] + $vs['debet'] - $vs['kredit'];
            $vs['saldoakhirperiod'] = $vs['debet'] - $vs['kredit'];

            //sum
            if ($vs['jenis'] == "D") {
                $tothpp["saldoawal"] += $vs['saldoawal'];
                $tothpp["debet"] += $vs['debet'];
                $tothpp["kredit"] += $vs['kredit'];
                $tothpp["saldoakhir"] += $vs['saldoakhir'];
                $tothpp["saldoakhirperiod"] += $vs['saldoakhirperiod'];

            }
            $vs['saldoawal'] = string_2sz($vs['saldoawal']);
            $vs['debet'] = string_2sz($vs['debet']);
            $vs['kredit'] = string_2sz($vs['kredit']);
            $vs['saldoakhir'] = string_2sz($vs['saldoakhir']);
            $vs['saldoakhirperiod'] = string_2sz($vs['saldoakhirperiod']);

            $arrkd = explode(".", $vs['kode']);
            $levelkd = count($arrkd);
            //bold text
            if ($vs['jenis'] == "I" and $level > $levelkd) {
                foreach ($vs as $key => $val) {
                    if ($key != "jenis") {
                        $vs[$key] = "<b>" . $val . "</b>";
                    }

                }
            }
            //unset($vs['jenis']);

            if ($level >= $levelkd) {
                $vare[$n++] = $vs;
            }
        }
        //pend - hpp
        $totpendbruto = array("saldoawal" => $totpendopr["saldoawal"] - $tothpp["saldoawal"],
            "debet" => $totpendopr["debet"] - $tothpp["debet"],
            "kredit" => $totpendopr["kredit"] - $tothpp["kredit"],
            "saldoakhir" => $totpendopr["saldoakhir"] - $tothpp["saldoakhir"],
            "saldoakhirperiod" => $totpendopr["saldoakhirperiod"] - $tothpp["saldoakhirperiod"]);
        $vare[$n++] = array("kode" => "", "keterangan" => "<b>LABA BRUTO</b>",
            "saldoawal" => "<b>" . string_2sz($totpendbruto['saldoawal']) . "</b>",
            "debet" => "<b>" . string_2sz($totpendbruto['debet']) . "</b>",
            "kredit" => "<b>" . string_2sz($totpendbruto['kredit']) . "</b>",
            "saldoakhir" => "<b>" . string_2sz($totpendbruto['saldoakhir']) . "</b>",
            "saldoakhirperiod" => "<b>" . string_2sz($totpendbruto['saldoakhirperiod']) . "</b>",
            "jenis" => "I");

        //by opr
        $totbyopr = array("saldoawal" => 0, "debet" => 0, "kredit" => 0, "saldoakhir" => 0, "saldoakhirperiod" => 0);
        $rekawal = $this->getconfig("rekbyoprawal");
        $rekakhir = $this->getconfig("rekbyoprakhir");
        $dbd = $this->loadrekening($rekawal, $rekakhir);
        while ($dbr = $this->getrow($dbd)) {
            $vs = $dbr;
            $vs['saldoawal'] = $this->getsaldoawal($tglawal, $vs['kode'],"",$penihilan,$cabang);
            $vs['debet'] = $this->getdebet($tglawal, $tglakhir, $vs['kode'],"","",$penihilan,false,$cabang);
            $vs['kredit'] = $this->getkredit($tglawal, $tglakhir, $vs['kode'],"","",$penihilan,false,$cabang);
            $vs['saldoakhir'] = $vs['saldoawal'] + $vs['debet'] - $vs['kredit'];
            $vs['saldoakhirperiod'] = $vs['debet'] - $vs['kredit'];

            //sum
            if ($vs['jenis'] == "D") {
                $totbyopr["saldoawal"] += $vs['saldoawal'];
                $totbyopr["debet"] += $vs['debet'];
                $totbyopr["kredit"] += $vs['kredit'];
                $totbyopr["saldoakhir"] += $vs['saldoakhir'];
                $totbyopr["saldoakhirperiod"] += $vs['saldoakhirperiod'];
            }
            $vs['saldoawal'] = string_2sz($vs['saldoawal']);
            $vs['debet'] = string_2sz($vs['debet']);
            $vs['kredit'] = string_2sz($vs['kredit']);
            $vs['saldoakhir'] = string_2sz($vs['saldoakhir']);
            $vs['saldoakhirperiod'] = string_2sz($vs['saldoakhirperiod']);

            $arrkd = explode(".", $vs['kode']);
            $levelkd = count($arrkd);
            //bold text
            if ($vs['jenis'] == "I" and $level > $levelkd) {
                foreach ($vs as $key => $val) {
                    if ($key != "jenis") {
                        $vs[$key] = "<b>" . $val . "</b>";
                    }

                }
            }
            //unset($vs['jenis']);

            if ($level >= $levelkd) {
                $vare[$n++] = $vs;
            }
        }

        //pend - by opr
        $totlropr = array("saldoawal" => $totpendbruto["saldoawal"] - $totbyopr["saldoawal"],
            "debet" => $totpendbruto["debet"] - $totbyopr["debet"],
            "kredit" => $totpendbruto["kredit"] - $totbyopr["kredit"],
            "saldoakhir" => $totpendbruto["saldoakhir"] - $totbyopr["saldoakhir"],
            "saldoakhirperiod" => $totpendbruto["saldoakhirperiod"] - $totbyopr["saldoakhirperiod"]);
        $vare[$n++] = array("kode" => "", "keterangan" => "<b>LABA RUGI OPERASIONAL</b>",
            "saldoawal" => "<b>" . string_2sz($totlropr['saldoawal']) . "</b>",
            "debet" => "<b>" . string_2sz($totlropr['debet']) . "</b>",
            "kredit" => "<b>" . string_2sz($totlropr['kredit']) . "</b>",
            "saldoakhir" => "<b>" . string_2sz($totlropr['saldoakhir']) . "</b>",
            "saldoakhirperiod" => "<b>" . string_2sz($totlropr['saldoakhirperiod']) . "</b>",
            "jenis" => "I");

        //pend non opr
        $totpendnonopr = array("saldoawal" => 0, "debet" => 0, "kredit" => 0, "saldoakhir" => 0, "saldoakhirperiod" => 0);
        $rekawal = $this->getconfig("rekpendnonoprawal");
        $rekakhir = $this->getconfig("rekpendnonoprakhir");
        $dbd = $this->loadrekening($rekawal, $rekakhir);
        while ($dbr = $this->getrow($dbd)) {
            $vs = $dbr;
            $vs['saldoawal'] = $this->getsaldoawal($tglawal, $vs['kode'],"",$penihilan,$cabang);
            $vs['debet'] = $this->getdebet($tglawal, $tglakhir, $vs['kode'],"","",$penihilan,false,$cabang);
            $vs['kredit'] = $this->getkredit($tglawal, $tglakhir, $vs['kode'],"","",$penihilan,false,$cabang);
            $vs['saldoakhir'] = $vs['saldoawal'] + $vs['kredit'] - $vs['debet'];
            $vs['saldoakhirperiod'] = $vs['kredit'] - $vs['debet'];

            //sum tot
            if ($vs['jenis'] == "D") {
                $totpendnonopr["saldoawal"] += $vs['saldoawal'];
                $totpendnonopr["debet"] += $vs['debet'];
                $totpendnonopr["kredit"] += $vs['kredit'];
                $totpendnonopr["saldoakhir"] += $vs['saldoakhir'];
                $totpendnonopr["saldoakhirperiod"] += $vs['saldoakhirperiod'];
            }
            $vs['saldoawal'] = string_2sz($vs['saldoawal']);
            $vs['debet'] = string_2sz($vs['debet']);
            $vs['kredit'] = string_2sz($vs['kredit']);
            $vs['saldoakhir'] = string_2sz($vs['saldoakhir']);
            $vs['saldoakhirperiod'] = string_2sz($vs['saldoakhirperiod']);

            $arrkd = explode(".", $vs['kode']);
            $levelkd = count($arrkd);
            //bold text
            if ($vs['jenis'] == "I" and $level > $levelkd) {
                foreach ($vs as $key => $val) {
                    if ($key != "jenis") {
                        $vs[$key] = "<b>" . $val . "</b>";
                    }

                }
            }
            //unset($vs['jenis']);

            if ($level >= $levelkd) {
                $vare[$n++] = $vs;
            }
        }

        //by non opr
        $totbynonopr = array("saldoawal" => 0, "debet" => 0, "kredit" => 0, "saldoakhir" => 0, "saldoakhirperiod" => 0);
        $rekawal = $this->getconfig("rekbynonoprawal");
        $rekakhir = $this->getconfig("rekbynonoprakhir");
        $dbd = $this->loadrekening($rekawal, $rekakhir);
        while ($dbr = $this->getrow($dbd)) {
            $vs = $dbr;
            $vs['saldoawal'] = $this->getsaldoawal($tglawal, $vs['kode'],"",$penihilan,$cabang);
            $vs['debet'] = $this->getdebet($tglawal, $tglakhir, $vs['kode'],"","",$penihilan,false,$cabang);
            $vs['kredit'] = $this->getkredit($tglawal, $tglakhir, $vs['kode'],"","",$penihilan,false,$cabang);
            $vs['saldoakhir'] = $vs['saldoawal'] + $vs['debet'] - $vs['kredit'];
            $vs['saldoakhirperiod'] = $vs['debet'] - $vs['kredit'];

            //sum
            if ($vs['jenis'] == "D") {
                $totbynonopr["saldoawal"] += $vs['saldoawal'];
                $totbynonopr["debet"] += $vs['debet'];
                $totbynonopr["kredit"] += $vs['kredit'];
                $totbynonopr["saldoakhir"] += $vs['saldoakhir'];
                $totbynonopr["saldoakhirperiod"] += $vs['saldoakhirperiod'];
            }
            $vs['saldoawal'] = string_2sz($vs['saldoawal']);
            $vs['debet'] = string_2sz($vs['debet']);
            $vs['kredit'] = string_2sz($vs['kredit']);
            $vs['saldoakhir'] = string_2sz($vs['saldoakhir']);
            $vs['saldoakhirperiod'] = string_2sz($vs['saldoakhirperiod']);

            $arrkd = explode(".", $vs['kode']);
            $levelkd = count($arrkd);
            //bold text
            if ($vs['jenis'] == "I" and $level > $levelkd) {
                foreach ($vs as $key => $val) {
                    if ($key != "jenis") {
                        $vs[$key] = "<b>" . $val . "</b>";
                    }

                }
            }
            //unset($vs['jenis']);

            if ($level >= $levelkd) {
                $vare[$n++] = $vs;
            }
        }

        //pend - by non opr
        $totlrnonopr = array("saldoawal" => $totpendnonopr["saldoawal"] - $totbynonopr["saldoawal"],
            "debet" => $totpendnonopr["debet"] - $totbynonopr["debet"],
            "kredit" => $totpendnonopr["kredit"] - $totbynonopr["kredit"],
            "saldoakhir" => $totpendnonopr["saldoakhir"] - $totbynonopr["saldoakhir"],
            "saldoakhirperiod" => $totpendnonopr["saldoakhirperiod"] - $totbynonopr["saldoakhirperiod"]);
        $vare[$n++] = array("kode" => "", "keterangan" => "<b>LABA RUGI NON OPERASIONAL</b>",
            "saldoawal" => "<b>" . string_2sz($totlrnonopr['saldoawal']) . "</b>",
            "debet" => "<b>" . string_2sz($totlrnonopr['debet']) . "</b>",
            "kredit" => "<b>" . string_2sz($totlrnonopr['kredit']) . "</b>",
            "saldoakhir" => "<b>" . string_2sz($totlrnonopr['saldoakhir']) . "</b>",
            "saldoakhirperiod" => "<b>" . string_2sz($totlrnonopr['saldoakhirperiod']) . "</b>",
            "jenis" => "I");

        //LR sebelum pajak
        $totlrsblmpjk = array("saldoawal" => $totlropr["saldoawal"] + $totlrnonopr["saldoawal"],
            "debet" => $totlropr["debet"] + $totlrnonopr["debet"],
            "kredit" => $totlropr["kredit"] + $totlrnonopr["kredit"],
            "saldoakhir" => $totlropr["saldoakhir"] + $totlrnonopr["saldoakhir"],
            "saldoakhirperiod" => $totlropr["saldoakhirperiod"] + $totlrnonopr["saldoakhirperiod"]);
        $vare[$n++] = array("kode" => "", "keterangan" => "<b>LABA RUGI SEBELUM PAJAK</b>",
            "saldoawal" => "<b>" . string_2sz($totlrsblmpjk['saldoawal']) . "</b>",
            "debet" => "<b>" . string_2sz($totlrsblmpjk['debet']) . "</b>",
            "kredit" => "<b>" . string_2sz($totlrsblmpjk['kredit']) . "</b>",
            "saldoakhir" => "<b>" . string_2sz($totlrsblmpjk['saldoakhir']) . "</b>",
            "saldoakhirperiod" => "<b>" . string_2sz($totlrsblmpjk['saldoakhirperiod']) . "</b>",
            "jenis" => "I");

        //pajak
        $totpajak = array("saldoawal" => 0, "debet" => 0, "kredit" => 0, "saldoakhir" => 0, "saldoakhirperiod" => 0);
        $rekawal = $this->getconfig("rekpajakawal");
        $rekakhir = $this->getconfig("rekpajakakhir");
        $dbd = $this->loadrekening($rekawal, $rekakhir);
        while ($dbr = $this->getrow($dbd)) {
            $vs = $dbr;
            $vs['saldoawal'] = $this->getsaldoawal($tglawal, $vs['kode'],"",$penihilan,$cabang);
            $vs['debet'] = $this->getdebet($tglawal, $tglakhir, $vs['kode'],"","",$penihilan,false,$cabang);
            $vs['kredit'] = $this->getkredit($tglawal, $tglakhir, $vs['kode'],"","",$penihilan,false,$cabang);
            $vs['saldoakhir'] = $vs['saldoawal'] + $vs['debet'] - $vs['kredit'];
            $vs['saldoakhirperiod'] = $vs['debet'] - $vs['kredit'];

            //sum
            if ($vs['jenis'] == "D") {
                $totpajak["saldoawal"] += $vs['saldoawal'];
                $totpajak["debet"] += $vs['debet'];
                $totpajak["kredit"] += $vs['kredit'];
                $totpajak["saldoakhir"] += $vs['saldoakhir'];
                $totpajak["saldoakhirperiod"] += $vs['saldoakhirperiod'];
            }
            $vs['saldoawal'] = string_2sz($vs['saldoawal']);
            $vs['debet'] = string_2sz($vs['debet']);
            $vs['kredit'] = string_2sz($vs['kredit']);
            $vs['saldoakhir'] = string_2sz($vs['saldoakhir']);
            $vs['saldoakhirperiod'] = string_2sz($vs['saldoakhirperiod']);

            $arrkd = explode(".", $vs['kode']);
            $levelkd = count($arrkd);
            //bold text
            if ($vs['jenis'] == "I" and $level > $levelkd) {
                foreach ($vs as $key => $val) {
                    if ($key != "jenis") {
                        $vs[$key] = "<b>" . $val . "</b>";
                    }

                }
            }
            //unset($vs['jenis']);

            if ($level >= $levelkd) {
                $vare[$n++] = $vs;
            }
        }

        //LR setelah pajak
        $totlrstlhpjk = array("saldoawal" => $totlrsblmpjk["saldoawal"] - $totpajak["saldoawal"],
            "debet" => $totlrsblmpjk["debet"] - $totpajak["debet"],
            "kredit" => $totlrsblmpjk["kredit"] - $totpajak["kredit"],
            "saldoakhir" => $totlrsblmpjk["saldoakhir"] - $totpajak["saldoakhir"],
            "saldoakhirperiod" => $totlrsblmpjk["saldoakhirperiod"] - $totpajak["saldoakhirperiod"]);
        $vare[$n++] = array("kode" => "<mdtlr12>", "keterangan" => "<b>LABA RUGI SETELAH PAJAK</b>",
            "saldoawal" => "<b>" . string_2sz($totlrstlhpjk['saldoawal']) . "</b>",
            "debet" => "<b>" . string_2sz($totlrstlhpjk['debet']) . "</b>",
            "kredit" => "<b>" . string_2sz($totlrstlhpjk['kredit']) . "</b>",
            "saldoakhir" => "<b>" . string_2sz($totlrstlhpjk['saldoakhir']) . "</b>",
            "saldoakhirperiod" => "<b>" . string_2sz($totlrstlhpjk['saldoakhirperiod']) . "</b></mdtlr12>",
            "jenis" => "I");

        //tampilkan ke grid
        $vare = array("total" => count($vare), "records" => $vare, "lrstlhpjk" => $totlrstlhpjk);
        return $vare;
    }

    public function getcfghpp($tgl)
    {
        $tgl = date_2s($tgl);
        $va = array("caraperhitungan" => "", "periode" => "", "bdp" => "");
        $where = "Tgl <= '$tgl'";
        $field = "*";
        $dbData = $this->select("sys_config_hpp", $field, $where, '', '', 'Tgl desc', 1);
        if ($dbr = $this->getrow($dbData)) {
            $va = $dbr;
        }
        return $va;

    }

    public function analisapenjualanproduk($kode, $tglawal, $tglakhir)
    {
        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);
        $arr = array("penjualan" => 0, "hpp" => 0, "margin" => 0, "persmargin" => 0);
        $field = "ifnull(sum(d.totalitem),0) as penjualan,ifnull(sum(d.hp * d.qty),0) as hpp";
        $where = "d.stock = '$kode' and t.tgl >= '$tglawal' and t.tgl <= '$tglakhir' and t.status = '1'";
        $join = "left join penjualan_total t on t.faktur = d.faktur";
        $dbd = $this->select("penjualan_detail d", $field, $where, $join, '', '');
        if ($dbr = $this->getrow($dbd)) {
            $arr['penjualan'] += $dbr['penjualan'];
            $arr['hpp'] += $dbr['hpp'];
        }

        $arr['margin'] = $arr['penjualan'] - $arr['hpp'];
        $arr['persmargin'] = devide($arr['margin'], $arr['penjualan']) * 100;
        return $arr;
    }

    public function hargapokokproduksistock($kode, $tglawal, $tglakhir)
    {
        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);
        $arr = array("BBB" => 0, "BTKL" => 0, "BOP" => 0, "jmlperbaikan" => 0, "Qty" => 0);
        $field = "b.faktur,b.fakturproduksi,b.tgl,b.cabang,b.gudang,b.stock,b.qty,b.username,
                        b.datetime,g.rekpersd,b.hp,t.btkl,t.bop,sum(h.hp * h.qty) as bb, sum(h.hptotkeluar) as bb2,
                        p.jumlahperbaikan jmlperbaikan";
        $where = "b.stock = '$kode' and b.status = '1' and b.tgl >= '$tglawal' and b.tgl <= '$tglakhir'";
        $join = "left join stock s on s.kode = b.stock left join stock_group g on g.kode = s.stock_group ";
        $join .= "left join produksi_total t on t.faktur = b.fakturproduksi ";
        $join .= "left join produksi_bb h on h.fakturproduksi = t.faktur and h.status = '1' ";
        $join .= "left join produksi_produk p on p.fakturproduksi = t.faktur";
        $dbd = $this->select("produksi_hasil b", $field, $where, $join, "b.faktur");
        while ($dbr = $this->getrow($dbd)) {
            $arr['jmlperbaikan'] += $dbr['jmlperbaikan'];
            $arr['BBB'] += $dbr['bb2'];
            $arr['BTKL'] += $dbr['btkl'];
            $arr['BOP'] += $dbr['bop'];
            $arr['Qty'] += $dbr['qty'];
        }

        return $arr;
    }

    public function getdatastockpo($fktpo, $stock, $tgl)
    {
        $arr = array("jmlpo" => 0, "jmlpb" => 0, "sisa" => 0, "tglakhir" => "");
        $tgl = date_2s($tgl);

        //cek awal PO
        $field = "ifnull(sum(d.qty),0) as jmlpo";
        $where = "d.faktur = '$fktpo' and d.stock = '$stock' and t.status = '1' and t.tgl <= '$tgl'";
        $join = "left join po_total t on t.faktur = d.faktur";
        $dbd = $this->select("po_detail d", $field, $where, $join, "d.stock");
        if ($dbr = $this->getrow($dbd)) {
            $arr['jmlpo'] = $dbr['jmlpo'];
        }
        $arr['sisa'] = $arr['jmlpo'];
        //cek penggunaan PO
        $field = "ifnull(sum(d.qty),0) as jmlpb,t.tgl";
        $where = "t.fktpo = '$fktpo' and d.stock = '$stock' and t.status = '1' and t.tgl <= '$tgl'";
        $join = "left join pembelian_total t on t.faktur = d.faktur";
        $dbd = $this->select("pembelian_detail d", $field, $where, $join, "t.faktur,t.tgl");
        while ($dbr = $this->getrow($dbd)) {
            $arr['jmlpb'] += $dbr['jmlpb'];
            $arr['sisa'] -= $dbr['jmlpb'];
            if ($arr['sisa'] <= 0) {
                $arr['tglakhir'] = $dbr['tgl'];
            }
        }
        return $arr;
    }

    public function getanggarankasrek($periode, $rekening, $cabang = '')
    {
        $arr = array("nominal" => 0, "datetime" => "");

        $where = "periode like '$periode%' and status = '1' and rekening = '$rekening'";
        if (!empty($cabang)) {
            $where .= " and cabang = '$cabang'";
        }

        $field = "*";
        $dbData = $this->select("anggaran_kas", $field, $where, '', '', 'datetime desc', 1);
        if ($dbr = $this->getrow($dbData)) {
            $arr['nominal'] = $dbr['nominal'];
            $arr['datetime'] = $dbr['datetime'];
        }
        return $arr;
    }

    public function getdetailtransaksi($faktur)
    {
        $arrket = array();
        $arrreturn = array("ketdetail"=>"");
        $typefkt = substr($faktur,0,2);
        if($typefkt == "CS"){
            $field = "s.keterangan as namastock,d.harga,d.qty,s.satuan,d.totalitem as jumlah";
            $where = "d.faktur = '$faktur'";
            $join  = "left join stock s on s.kode = d.stock";
            $dbd   = $this->select("penjualan_detail d", $field, $where, $join) ;
            
            $n = 0 ;
            while($dbr = $this->getrow($dbd)){
                $n++;
                $arrket[] = str_pad($n.".",3," ",STR_PAD_LEFT)." ".$dbr['namastock']." ".$dbr['satuan'];
                $arrket[] = string_2s($dbr['harga'])." x ".$dbr['qty'] ." = ".string_2s($dbr['jumlah']);
            }

            
        }else if($typefkt == "RJ"){
            $field = "s.keterangan namastock,d.jumlah,d.harga,d.qty,s.satuan";
            $where = "d.faktur = '$faktur'";
            $join  = "left join stock s on s.kode = d.stock";
            $dbd   = $this->select("penjualan_retur_detail d", $field, $where, $join) ;
            
            $n = 0 ;
            while($dbr = $this->getrow($dbd)){
                $n++;
                $arrket[] = str_pad($n.".",3," ",STR_PAD_LEFT)." ".$dbr['namastock']." ".$dbr['satuan'];
                $arrket[] = string_2s($dbr['harga'])." x ".$dbr['qty'] ." = ".string_2s($dbr['jumlah']);
            }

            
        }
        $arrreturn['ketdetail'] = implode("\n",$arrket);
        return $arrreturn;
    }

    public function gethjstockcustomer($kode, $customer)
    {
        $hjjenis = "";
        
        //mencari jenis harga customer
        $dbdcust = $this->select("customer","jenishj","kode = '$customer'");
        if($dbrcust = $this->getrow($dbdcust)){
            $hjjenis = $dbrcust['jenishj'];
        }

        $return = 0;
        $where = "stock = '$kode' and hjjenis = '$hjjenis'";
        $join = "";
        $group = "";
        $dbd = $this->select("stock_hj_jenis", "hj", $where, $join, $group, "id desc");
        if ($dbr = $this->getrow($dbd)) {
            $return = $dbr['hj'];
        }
        
        return $return;
    }

    public function getdatastock($kodestock){
        $return = array("kode"=>"","stock_kelompok"=>"","jenis_kelompok"=>"");
        $where = "s.kode = '$kodestock' or s.barcode = '$kodestock'";
        $join = "left join stock_kelompok k on k.kode = s.stock_kelompok";
        $field = "s.kode,s.keterangan,s.satuan,s.stock_kelompok,k.jenis jenis_kelompok";
        $dbd = $this->select("stock s",$field,$where,$join);
        if($dbr = $this->getrow($dbd)){
            $return['kode'] = $dbr['kode'];
            $return['stock_kelompok'] = $dbr['stock_kelompok'];
            $return['jenis_kelompok'] = $dbr['jenis_kelompok'];
        }
        return $return ;
    }
}
