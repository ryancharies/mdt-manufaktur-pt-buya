<?php
class Updtransaksi_m extends Bismillah_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('func/perhitungan_m');
        $this->load->model('func/perhitunganhrd_m');
    }
    public function updjurnal($faktur, $cabang, $tgl = '0000-00-00', $rekening = '', $keterangan = '', $debet = 0, $kredit = 0, $datetime = '0000-00-00 00:00:00', $username = '')
    {
        $tgl = date_2s($tgl);
        $arr = array("faktur" => $faktur, "cabang" => $cabang, "tgl" => $tgl, "rekening" => $rekening,
            "keterangan" => sql_2sql($keterangan), "debet" => string_2n($debet), "kredit" => string_2n($kredit),
            "datetime" => $datetime, "username" => $username);
        $this->insert("keuangan_jurnal", $arr);
    }

    public function updbukubesar($faktur, $cabang, $tgl = '0000-00-00', $rekening = '', $keterangan= '', $debet = 0, $kredit = 0, $datetime = '0000-00-00 00:00:00', $username = '')
    {
        if ($debet > 0 or $kredit > 0) {
            $tgl = date_2s($tgl);
            $arr = array("faktur" => $faktur, "cabang" => $cabang, "tgl" => $tgl, "rekening" => $rekening,
                "keterangan" => sql_2sql($keterangan), "debet" => string_2n($debet), "kredit" => string_2n($kredit),
                "datetime" => $datetime, "username" => $username);
            $this->insert("keuangan_bukubesar", $arr);
        }
    }

    public function updrekjurnal($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $where = "faktur = '$faktur'";
        $join = "";
        $field = "faktur,cabang,tgl,rekening,keterangan,debet,kredit,datetime,username";
        $dbd = $this->select("keuangan_jurnal", $field, $where, $join, "", "debet desc");
        while ($dbr = $this->getrow($dbd)) {
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekening'], $dbr['keterangan'], $dbr['debet'], $dbr['kredit'], $dbr['datetime'], $dbr['username']);
        }
    }
    public function updkartustock($faktur, $stock, $tgl = '0000-00-00', $gudang= '', $cabang= '', $keterangan= '', $debet = 0, $kredit = 0, $hp = 0, $username = '', $datetime = '0000-00-00 00:00:00')
    {
        if ($debet > 0 or $kredit > 0) {
            if ($username == '') {
                $username = getsession($this, "username");
            }

            $vainsert = array("faktur" => $faktur, "stock" => $stock, "tgl" => $tgl, "keterangan" => $keterangan, "debet" => $debet, "kredit" => $kredit,
                "gudang" => $gudang, "cabang" => $cabang, "hp" => $hp, "username" => $username, "datetime_insert" => $datetime);
            $this->insert("stock_kartu", $vainsert);
        }
    }

    public function updkartuhutang($faktur, $fkt, $supplier, $tgl = '0000-00-00', $cabang= '', $keterangan= '', $debet = 0, $kredit = 0, $username = '', $datetime = '0000-00-00 00:00:00', $jenis = 'H', $kodetransaksi = "")
    {
        if ($debet > 0 or $kredit > 0) {
            if ($username == '') {
                $username = getsession($this, "username");
            }

            if ($datetime == '0000-00-00 00:00:00') {
                $datetime = date("Y-m-d H:i:s");
            }

            $vainsert = array("faktur" => $faktur, "fkt" => $fkt, "supplier" => $supplier, "tgl" => $tgl,
                "keterangan" => $keterangan, "debet" => $debet, "kredit" => $kredit, "jenis" => $jenis,
                "kodetransaksi" => $kodetransaksi,
                "cabang" => $cabang, "username" => $username, "datetime" => $datetime);
            $this->insert("hutang_kartu", $vainsert);
        }
    }

    public function updkartupiutang($faktur, $fkt, $customer, $tgl = '0000-00-00', $cabang= '', $keterangan= '', $debet = 0, $kredit = 0, $username = '', $datetime = '0000-00-00 00:00:00', $jenis = 'P', $kodetransaksi = "")
    {
        if ($debet > 0 or $kredit > 0) {
            if ($username == '') {
                $username = getsession($this, "username");
            }

            if ($datetime == '0000-00-00 00:00:00') {
                $datetime = date("Y-m-d H:i:s");
            }

            $vainsert = array("faktur" => $faktur, "fkt" => $fkt, "customer" => $customer, "tgl" => $tgl,
                "keterangan" => $keterangan, "debet" => $debet, "kredit" => $kredit, "jenis" => $jenis,
                "kodetransaksi" => $kodetransaksi,
                "cabang" => $cabang, "username" => $username, "datetime" => $datetime);
            $this->insert("piutang_kartu", $vainsert);
        }
    }

    public function updpokartu($faktur, $fktpo, $stock, $tgl = '0000-00-00', $cabang= '', $gudang= '', $keterangan= '', $debet = 0, $kredit = 0, $hp = 0, $username = '', $datetime = '0000-00-00 00:00:00')
    {
        if (($debet > 0 or $kredit > 0) and $fktpo != "") {
            if ($username == '') {
                $username = getsession($this, "username");
            }

            $vainsert = array("faktur" => $faktur, "fktpo" => $fktpo, "stock" => $stock, "tgl" => $tgl, "keterangan" => $keterangan, "debet" => $debet, "kredit" => $kredit,
                "cabang" => $cabang, "gudang" => $gudang, "hp" => $hp, "username" => $username, "datetime" => $datetime);
            $this->insert("po_kartu", $vainsert);
        }
    }
    /********************po*******************************/
    public function updpokartupurchaseorder($faktur)
    {
        $this->delete("po_kartu", "faktur = '$faktur'");
        $field = "d.stock,s.keterangan as namastock,d.spesifikasi,d.harga,d.qty,s.satuan,d.jumlah,t.tgl,t.cabang,t.gudang,p.nama,t.username,t.datetime";
        $where = "d.faktur = '$faktur'";
        $join = "left join stock s on s.kode = d.stock left join po_total t on t.faktur = d.faktur left join supplier p on p.kode = t.supplier";
        $dbd = $this->select("po_detail d", $field, $where, $join);
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = "PO " . $dbr['nama'];
            $this->updpokartu($faktur, $faktur, $dbr['stock'], $dbr['tgl'], $dbr['cabang'], $dbr['gudang'], $keterangan,
                $dbr['qty'], 0, $dbr['harga'], $dbr['username'], $dbr['datetime']);
        }

    }
    /********************pembelian*******************************/
    public function updkartustockreturpembelian($faktur)
    {
        $this->delete("stock_kartu", "faktur = '$faktur'");
        $field = "d.id,d.faktur,d.stock,d.harga,d.qty,d.totalitem,t.username,t.tgl,t.gudang,t.cabang,s.nama,t.datetime";
        $where = "d.faktur = '$faktur'";
        $join = "left join pembelian_retur_total t on t.faktur = d.faktur left join supplier s on s.kode = t.supplier";

        $dbd = $this->select("pembelian_retur_detail d", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $hp = 0;
            $keterangan = "Retur pembelian stock " . $dbr['nama'];
            $cfghpp = $this->perhitungan_m->getcfghpp($dbr['tgl']);
            $saldoqty = $this->perhitungan_m->getsaldoakhirstock($dbr['stock'], $dbr['tgl']);
            $arrhp = $this->perhitungan_m->gethpstock($dbr['stock'], $dbr['tgl'], $dbr['tgl'], $saldoqty, $dbr['qty'], $dbr['cabang'],
                $cfghpp['caraperhitungan'], $cfghpp['periode']);
            $hp = devide($arrhp['hpdikeluarkan'], $dbr['qty']);
            $this->edit("pembelian_retur_detail", array("hp" => $hp, "tothpkeluar" => $arrhp['hpdikeluarkan']), "id = '{$dbr['id']}'");
            if ($dbr['totalitem'] > 0 and $dbr['qty'] > 0) {
                $hp = $dbr['totalitem'] / $dbr['qty'];
            }

            $this->updkartustock($faktur, $dbr['stock'], $dbr['tgl'], $dbr['gudang'], $dbr['cabang'], $keterangan, 0, $dbr['qty'],
                $hp, $dbr['username'], $dbr['datetime']);

        }
    }

    public function updkartustockpembelian($faktur)
    {
        $this->delete("stock_kartu", "faktur = '$faktur'");
        $this->delete("po_kartu", "faktur = '$faktur'");
        $field = "d.id,d.faktur,d.stock,d.harga,d.qty,d.totalitem,d.username,t.tgl,t.gudang,t.cabang,
                 t.datetime_insert as datetime,t.persppn,t.fktpo";
        $where = "d.faktur = '$faktur'";
        $join = "left join pembelian_total t on t.faktur = d.faktur";
        $keterangan = "pembelian stock fkt[" . $faktur . "]";
        $dbd = $this->select("pembelian_detail d", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $hp = 0;
            if ($dbr['totalitem'] > 0 and $dbr['qty'] > 0) {
                $hp = devide($dbr['totalitem'], $dbr['qty']);
                $hp += $hp * devide($dbr['persppn'], 100);
            }
            $this->edit("pembelian_detail", array("hp" => $hp), "id = '{$dbr['id']}'");
            $this->updkartustock($faktur, $dbr['stock'], $dbr['tgl'], $dbr['gudang'], $dbr['cabang'], $keterangan, $dbr['qty'],
                0, $hp, $dbr['username'], $dbr['datetime']);

            $cfghpp = $this->perhitungan_m->getcfghpp($dbr['tgl']);
            $saldoqty = $this->perhitungan_m->getsaldoakhirstock($dbr['stock'], $dbr['tgl'], "", $dbr['cabang']);
            $arrhp = $this->perhitungan_m->gethpstock($dbr['stock'], $dbr['tgl'], $dbr['tgl'], $saldoqty, 0, $dbr['cabang'],
                $cfghpp['caraperhitungan'], $cfghpp['periode']);

            // po kartu
            if ($dbr['fktpo'] != "") {
                $this->updpokartu($faktur, $dbr['fktpo'], $dbr['stock'], $dbr['tgl'], $dbr['cabang'], $dbr['gudang'], $keterangan,
                    0, $dbr['qty'], $hp, $dbr['username'], $dbr['datetime']);
            }
        }
    }

    public function updrekpembelian($faktur)
    {
        $rekkas = getsession($this, "rekkas");
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "t.faktur,t.tgl,t.cabang,s.nama,t.subtotal,t.diskon,t.pembulatan,t.ppn,t.total,t.hutang,t.kas,t.username,t.datetime_insert datetime";
        $where = "t.faktur = '$faktur'";
        $join = "left join supplier s on s.kode = t.supplier";
        $dbd = $this->select("pembelian_total t", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $vapersd = array();
            $f = "d.stock,d.harga,d.qty,d.jumlah,d.totalitem,g.keterangan,g.rekpersd,s.stock_group,d.hp";
            $w = "d.faktur = '$faktur'";
            $j = "left join stock s on s.kode = d.stock left join stock_group g on g.kode = s.stock_group";
            $dbd2 = $this->select("pembelian_detail d", $f, $w, $j, "");
            $jmlperd = 0;
            while ($dbr2 = $this->getrow($dbd2)) {
                if (!isset($vapersd[$dbr2['stock_group']])) {
                    $vapersd[$dbr2['stock_group']] = array("jml" => 0, "rekpersd" => $dbr2['rekpersd'], "keterangan" => $dbr2['keterangan']);
                }

                $vapersd[$dbr2['stock_group']]['jml'] += $dbr2['hp'] * $dbr2['qty'];
                $jmlperd += $dbr2['hp'] * $dbr2['qty'];
            }

            foreach ($vapersd as $key => $val) {
                $ket = "Persd. Pembelian " . $val['keterangan'];
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $val['rekpersd'], $ket, $val['jml'], 0, $dbr['datetime'], $dbr['username']);
            }

            $ket = "Pembulatan Pembelian " . $dbr['nama'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekselisih"), $ket, $dbr['pembulatan'], 0, $dbr['datetime'], $dbr['username']);

            $ket = "Hutang Pembelian " . $dbr['nama'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekpbhut"), $ket, 0, $dbr['hutang'], $dbr['datetime'], $dbr['username']);

            $ket = "Disc. Pembelian " . $dbr['nama'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekpbdisc"), $ket, 0, $dbr['diskon'], $dbr['datetime'], $dbr['username']);

            //$ket= "PPn Pembelian ".$dbr['nama'];
            //$this->updbukubesar($dbr['faktur'],$dbr['cabang'],$dbr['tgl'],$this->getconfig("rekpbppn"),$ket,$dbr['ppn'],0,$dbr['datetime'],$dbr['username']);

            $selisih = ($jmlperd) - ($dbr['hutang'] + $dbr['diskon'] - $dbr['pembulatan']);
            $ket = "Selisih Pembelian " . $dbr['nama'];
            if ($selisih < 0) {
                $selisih = $selisih * -1;
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekselisih"), $ket, $selisih, 0, $dbr['datetime'], $dbr['username']);
            } else if ($selisih > 0) {
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekselisih"), $ket, 0, $selisih, $dbr['datetime'], $dbr['username']);
            }
        }
    }

    public function updkartuhutangpembelian($faktur)
    {
        $rekkas = getsession($this, "rekkas");
        $this->delete("hutang_kartu", "faktur = '$faktur'");
        $field = "t.faktur,t.tgl,t.cabang,s.nama as namasupplier,t.supplier,t.subtotal,t.diskon,t.ppn,t.total,t.hutang,t.kas,
                    t.username,t.datetime_insert datetime";
        $where = "t.faktur = '$faktur'";
        $join = "left join supplier s on s.kode = t.supplier";
        $dbd = $this->select("pembelian_total t", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = "Hut Pembelian [" . $faktur . "] an " . $dbr['namasupplier'];
            $this->updkartuhutang($faktur, $faktur, $dbr['supplier'], $dbr['tgl'], $dbr['cabang'], $keterangan,
                $dbr['hutang'], 0, $dbr['username'], $dbr['datetime']);
        }
    }

    public function updkartuhutangreturpembelian($faktur)
    {
        $rekkas = getsession($this, "rekkas");
        $this->delete("hutang_kartu", "faktur = '$faktur'");
        $field = "t.faktur,t.tgl,t.cabang,s.nama as namasupplier,t.supplier,t.subtotal,t.total,
                    t.username,t.datetime";
        $where = "t.faktur = '$faktur'";
        $join = "left join supplier s on s.kode = t.supplier";
        $dbd = $this->select("pembelian_retur_total t", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = "Retur Pembelian [" . $faktur . "] an " . $dbr['namasupplier'];
            $this->updkartuhutang($faktur, $faktur, $dbr['supplier'], $dbr['tgl'], $dbr['cabang'], $keterangan,
                0, $dbr['total'], $dbr['username'], $dbr['datetime']);
        }
    }

    public function updrekreturpembelian($faktur)
    {
        $rekkas = getsession($this, "rekkas");
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "t.faktur,t.tgl,t.cabang,s.nama,t.subtotal,t.total,t.username,t.datetime";
        $where = "t.faktur = '$faktur'";
        $join = "left join supplier s on s.kode = t.supplier";
        $dbd = $this->select("pembelian_retur_total t", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $vapersd = array();
            $f = "d.stock,d.harga,d.qty,d.jumlah,d.totalitem,g.keterangan,g.rekpersd,s.stock_group,d.tothpkeluar,d.hp";
            $w = "d.faktur = '$faktur'";
            $j = "left join stock s on s.kode = d.stock left join stock_group g on g.kode = s.stock_group";
            $dbd2 = $this->select("pembelian_retur_detail d", $f, $w, $j, "");
            $persd = 0;
            while ($dbr2 = $this->getrow($dbd2)) {
                if (!isset($vapersd[$dbr2['stock_group']])) {
                    $vapersd[$dbr2['stock_group']] = array("jml" => 0, "rekpersd" => $dbr2['rekpersd'], "keterangan" => $dbr2['keterangan']);
                }

                $vapersd[$dbr2['stock_group']]['jml'] += $dbr2['tothpkeluar']; //$dbr2['qty'] * $dbr2['hp'];
                $persd += $dbr2['tothpkeluar']; //$dbr2['qty'] * $dbr2['hp'];
            }

            foreach ($vapersd as $key => $val) {
                $ket = "Persd. Retur Pembelian " . $val['keterangan'];
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $val['rekpersd'], $ket, 0, $val['jml'], $dbr['datetime'], $dbr['username']);
            }

            $ket = "Retur Pembelian " . $dbr['nama'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekpbhut"), $ket, $dbr['total'], 0, $dbr['datetime'], $dbr['username']);

            $selisih = $dbr['total'] - $persd;
            $ket = "Selisih Retur Pembelian " . $dbr['nama'];
            if ($selisih < 0) {
                $selisih = $selisih * -1;
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekselisih"), $ket, $selisih, 0, $dbr['datetime'], $dbr['username']);
            } else if ($selisih > 0) {
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekselisih"), $ket, 0, $selisih, $dbr['datetime'], $dbr['username']);
            }

        }
    }

    public function updrekhutangpelunasan($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "t.faktur,t.tgl,t.cabang,t.username,t.datetime,s.nama as namasupplier,b.rekening,b.keterangan as ketrekkasbank,t.keterangan,
                t.kasbank,t.subtotal,t.diskon,t.pembulatan,t.persekot,t.kdtrpersekot,k.keterangan as ketpersekot,t.bgcek,
                k.rekening as rekkdtrpersekot,k.dk as dktrpersekot";
        $where = "t.faktur = '$faktur'";
        $join = "left join supplier s on s.kode = t.supplier left join bank b on b.kode = t.rekkasbank left join kodetransaksi k on k.kode = t.kdtrpersekot";
        $dbd = $this->select("hutang_pelunasan_total t", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $nSubPB = $dbr['subtotal'];
            //cek dulu pelunasan aset
            $f = "ifnull(sum(d.jumlah),0) as Jumlah,g.kode,g.rekhutpembelian";
            $w = "d.faktur = '$faktur' and left(fkt,2)= 'AS'";
            $j = "left join aset a on a.faktur = d.fkt left join aset_golongan g on g.kode = a.golongan";
            $dbd2 = $this->select("hutang_pelunasan_detail d", $f, $w, $j, "g.kode");
            while ($dbr2 = $this->getrow($dbd2)) {
                $ket =  $dbr['keterangan']." Aset an " . $dbr['namasupplier'];
                $nSubPB -= $dbr2['Jumlah'];
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr2['rekhutpembelian'], $ket, $dbr2['Jumlah'], 0, $dbr['datetime'], $dbr['username']);
            }
            $dsubpb = 0;
            $ksubpb = 0;
            if ($nSubPB > 0) {
                $dsubpb = $nSubPB;
            }

            if ($nSubPB < 0) {
                $ksubpb = $nSubPB * -1;
            }

            $ket = $dbr['keterangan']." an " . $dbr['namasupplier'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekpbhut"), $ket, $dsubpb, $ksubpb, $dbr['datetime'], $dbr['username']);
            $ket = "Pembulatan ".$dbr['keterangan']." an " . $dbr['namasupplier'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekpbhutpembulatan"), $ket, $dbr['pembulatan'], 0, $dbr['datetime'], $dbr['username']);
            $ket = $dbr['keterangan']." an " . $dbr['namasupplier'] . " - " . $dbr['ketrekkasbank'];

            $dkb = 0;
            $kkb = 0;
            if ($dbr['kasbank'] > 0) {
                $kkb = $dbr['kasbank'];
            }

            if ($dbr['kasbank'] < 0) {
                $dkb = $dbr['kasbank'] * -1;
            }

            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekening'], $ket, $dkb, $kkb, $dbr['datetime'], $dbr['username']);
            $ket = "Diskon ".$dbr['keterangan']." an " . $dbr['namasupplier'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekpbhutdisc"), $ket, 0, $dbr['diskon'], $dbr['datetime'], $dbr['username']);
            $ket = "BG/Cek ".$dbr['keterangan']." an " . $dbr['namasupplier'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekhutbg"), $ket, 0, $dbr['bgcek'], $dbr['datetime'], $dbr['username']);
            if ($dbr['persekot'] > 0) {
                $ket = $dbr['ketpersekot'] . " an " . $dbr['namasupplier'];
                $dpskt = 0;
                $kpskt = 0;
                if ($dbr['dktrpersekot'] == "K") {
                    $kpskt = $dbr['persekot'];
                } else if ($dbr['dktrpersekot'] == "D") {
                    $dpskt = $dbr['persekot'];
                }
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekkdtrpersekot'], $ket, $dpskt, $kpskt, $dbr['datetime'], $dbr['username']);
            }

        }
    }

    public function updkartuhutangpelunasan($faktur)
    {
        $this->delete("hutang_kartu", "faktur = '$faktur'");
        $field = "d.faktur,d.fkt,d.jumlah,d.jenis,t.tgl,t.cabang,t.username,t.datetime,s.nama as namasupplier,t.supplier,t.keterangan";
        $where = "d.faktur = '$faktur'";
        $join = "left join hutang_pelunasan_total t on t.faktur = d.faktur left join supplier s on s.kode = t.supplier";
        $dbd = $this->select("hutang_pelunasan_detail d", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = $dbr['keterangan']." [" . $faktur . "] an " . $dbr['namasupplier'];
            $debet = 0;
            $kredit = $dbr['jumlah'];
            if ($dbr['jenis'] == 'Retur Pembelian') {
                $debet = $dbr['jumlah'];
                $kredit = 0;
            }
            $this->updkartuhutang($faktur, $dbr['fkt'], $dbr['supplier'], $dbr['tgl'], $dbr['cabang'], $keterangan,
                $debet, $kredit, $dbr['username'], $dbr['datetime'], "H");
        }

        // update kartu persekot
        $field = "t.faktur,t.tgl,t.cabang,t.username,t.datetime,s.nama as namasupplier,b.rekening,b.keterangan as ketrekkasbank,
                t.kasbank,t.subtotal,t.diskon,t.pembulatan,t.persekot,t.kdtrpersekot,k.keterangan as ketpersekot,
                k.rekening as rekkdtrpersekot,k.dk as dktrpersekot,t.supplier";
        $where = "t.faktur = '$faktur'";
        $join = "left join supplier s on s.kode = t.supplier left join bank b on b.kode = t.rekkasbank left join kodetransaksi k on k.kode = t.kdtrpersekot";
        $dbd = $this->select("hutang_pelunasan_total t", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = $dbr['ketpersekot'];
            $debet = 0;
            $kredit = 0;
            if ($dbr['dktrpersekot'] == "K") {
                $debet = $dbr['persekot'];

            } else if ($dbr['dktrpersekot'] == "D") {
                $kredit = $dbr['persekot'];
            }
            $this->updkartuhutang($faktur, $faktur, $dbr['supplier'], $dbr['tgl'], $dbr['cabang'], $keterangan,
                $debet, $kredit, $dbr['username'], $dbr['datetime'], "P", $dbr['kdtrpersekot']);
        }
    }

    public function updkartuhutangpembelianaset($faktur)
    {
        $this->delete("hutang_kartu", "faktur = '$faktur'");
        $field = "a.tglperolehan,a.kode,a.faktur,a.keterangan,a.hargaperolehan,a.vendor,a.vendors,a.cabang,a.username,a.datetime,a.cabang";
        $where = "a.faktur = '$faktur'";
        $join = "";
        $dbd = $this->select("aset a", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            
            if ($dbr['tglperolehan'] >= '2020-01-01') {

                $vendors = json_decode($dbr['vendors'],true);
                foreach($vendors as $key => $val){
                    $keterangan = "Pembelian aset [" . $dbr['kode'] . " - " . $val['namavendor'] . "]";
                    $this->updkartuhutang($faktur, $faktur, $val['vendor'], $dbr['tglperolehan'], $dbr['cabang'], $keterangan,
                        $val['hutang'], 0, $dbr['username'], $dbr['datetime']);
                }
                
            }

        }
    }

    public function updrekpembelianaset($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "a.tglperolehan,a.kode,a.faktur,a.keterangan,a.hargaperolehan,a.vendor,a.cabang,a.username,a.datetime,g.rekaktiva,
				g.rekhutpembelian,a.cabang";
        $where = "a.faktur = '$faktur'";
        $join = "left join aset_golongan g on g.kode = a.golongan";
        $dbd = $this->select("aset a", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $ket = "Pembelian aset [" . $dbr['kode'] . " - " . $dbr['keterangan'] . "]";
            if ($dbr['tglperolehan'] >= '2020-01-01') {
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tglperolehan'], $dbr['rekaktiva'], $ket, $dbr['hargaperolehan'], 0, $dbr['datetime'], $dbr['username']);
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tglperolehan'], $dbr['rekhutpembelian'], $ket, 0, $dbr['hargaperolehan'], $dbr['datetime'], $dbr['username']);
            }
        }
    }

    public function updkartuhutangperubahanaset($faktur)
    {
        

        $this->delete("hutang_kartu", "faktur = '$faktur'");
        $field = "a.faktur,a.kode,a.tgl,a.cabang,a.vendors,a.tgl,a.username,a.datetime";
        $where = "a.faktur = '$faktur'";
        $join = "";
        $dbd = $this->select("aset_perubahan a", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            
            if ($dbr['tgl'] >= '2020-01-01') {

                $vendors = json_decode($dbr['vendors'],true);
                foreach($vendors as $key => $val){
                    $keterangan = "Perubahan aset [" . $dbr['kode'] . " - " . $val['namavendor'] . "]";
                    $this->updkartuhutang($faktur, $faktur, $val['vendor'], $dbr['tgl'], $dbr['cabang'], $keterangan,
                        $val['hutang'], 0, $dbr['username'], $dbr['datetime']);
                }
                
            }

        }
    }

    public function updrekperubahanaset($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "p.tgl,p.kode,p.faktur,a.keterangan,p.dataakhir,p.cabang,p.username,p.datetime,g.rekaktiva,
				g.rekhutpembelian,p.cabang";
        $where = "p.faktur = '$faktur'";
        $join = "left join aset a on a.kode = p.kode left join aset_golongan g on g.kode = a.golongan";
        $dbd = $this->select("aset_perubahan p", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $ket = "Perubahan aset [" . $dbr['kode'] . " - " . $dbr['keterangan'] . "]";
            if ($dbr['tgl'] >= '2020-01-01') {
                $d_akhir = json_decode($dbr['dataakhir'],true);
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekaktiva'], $ket, $d_akhir['hargaperolehan_tambahan'], 0, $dbr['datetime'], $dbr['username']);
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekhutpembelian'], $ket, 0, $d_akhir['hargaperolehan_tambahan'], $dbr['datetime'], $dbr['username']);
            }
        }
    }

    /***************************************************/

    /********************penjualan*******************************/
    public function updkartustockpenjualan($faktur)
    {
        $this->delete("stock_kartu", "faktur = '$faktur'");
        $field = "d.id,d.faktur,d.stock,d.harga,d.qty,d.totalitem,d.username,t.tgl,t.gudang,t.cabang,t.datetime_insert";
        $where = "d.faktur = '$faktur'";
        $join = "left join penjualan_total t on t.faktur = d.faktur";
        $keterangan = "Penjualan kasir fkt[" . $faktur . "]";
        $dbd = $this->select("penjualan_detail d", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $hp = 0;
            $cfghpp = $this->perhitungan_m->getcfghpp($dbr['tgl']);
            $saldoqty = $this->perhitungan_m->getsaldoakhirstock($dbr['stock'], $dbr['tgl'], "", $dbr['cabang']);
            $arrhp = $this->perhitungan_m->gethpstock($dbr['stock'], $dbr['tgl'], $dbr['tgl'], $saldoqty, $dbr['qty'], $dbr['cabang'],
                $cfghpp['caraperhitungan'], $cfghpp['periode']);
            $hp = devide($arrhp['hpdikeluarkan'], $dbr['qty']);
            $this->edit("penjualan_detail", array("hp" => $hp, "tothpkeluar" => $arrhp['hpdikeluarkan']), "id = {$dbr['id']}");
            $this->updkartustock($faktur, $dbr['stock'], $dbr['tgl'], $dbr['gudang'], $dbr['cabang'],
                $keterangan, 0, $dbr['qty'], $hp, $dbr['username'], $dbr['datetime_insert']);
        }
    }

    public function updrekpenjualan($faktur)
    {
        $rekkas = getsession($this, "rekkas");
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "t.faktur,t.tgl,t.cabang,s.nama,t.subtotal,t.total,t.kas,t.piutang,
                  t.username,t.datetime_update datetime,g.rekpj,t.ppn";
        $where = "t.faktur = '$faktur' and t.sttkonversi = '0'";
        $join = "left join customer s on s.kode = t.customer left join customer_golongan g on g.kode = s.golongan";
        $dbd = $this->select("penjualan_total t", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $vapersd = array();
            $ket = "Kas Penjualan " . $dbr['nama'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $rekkas, $ket, $dbr['kas'], 0, $dbr['datetime'], $dbr['username']);
            $ket = "Piutang Penjualan " . $dbr['nama'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekpjpiutang"), $ket, $dbr['piutang'], 0, $dbr['datetime'], $dbr['username']);
            $ket = "Penjualan " . $dbr['nama'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekpj'], $ket, 0, $dbr['subtotal'], $dbr['datetime'], $dbr['username']);
            $ket = "PPN Penjualan " . $dbr['nama'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekpjppn"), $ket, 0, $dbr['ppn'], $dbr['datetime'], $dbr['username']);

            //hitung hpp
            $vapersd = array();
            $f = "d.stock,d.harga,d.qty,d.jumlah,d.totalitem,g.keterangan,g.rekpersd,s.stock_group,g.rekhpp,
                g.rekpj,d.hp,d.tothpkeluar";
            $w = "d.faktur = '$faktur'";
            $j = "left join stock s on s.kode = d.stock left join stock_group g on g.kode = s.stock_group";
            $dbd2 = $this->select("penjualan_detail d", $f, $w, $j, "");
            while ($dbr2 = $this->getrow($dbd2)) {
                if (!isset($vapersd[$dbr2['stock_group']])) {
                    $vapersd[$dbr2['stock_group']] = array("jml" => 0, "hpp" => 0, "tothpp" => 0, "rekpersd" => $dbr2['rekpersd'], "rekhpp" => $dbr2['rekhpp'], "rekpj" => $dbr2['rekpj'], "keterangan" => $dbr2['keterangan']);
                }

                $vapersd[$dbr2['stock_group']]['jml'] += $dbr2['totalitem'];
                $vapersd[$dbr2['stock_group']]['hpp'] += $dbr2['hp'] * $dbr2['qty'];
                $vapersd[$dbr2['stock_group']]['tothpp'] += $dbr2['tothpkeluar'];
            }

            foreach ($vapersd as $key => $val) {
                $ket = "HPP " . $val['keterangan'];

                $selisihhp = $val['tothpp'] - $val['hpp'];
                $sdebet = $selisihhp;
                $skredit = 0;
                if ($selisihhp < 0) {
                    $sdebet = 0;
                    $skredit = $selisihhp;
                }

                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $val['rekhpp'], $ket, $val['tothpp'], 0, $dbr['datetime'], $dbr['username']);
                // $this->updbukubesar($dbr['faktur'],$dbr['cabang'],$dbr['tgl'],$val['rekhpp'],"Selisih ".$ket,$sdebet,$skredit,$dbr['datetime'],$dbr['username']);
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $val['rekpersd'], $ket, 0, $val['tothpp'], $dbr['datetime'], $dbr['username']);
            }

        }
    }

    public function updkartupiutangpenjualan($faktur)
    {
        $rekkas = getsession($this, "rekkas");
        $this->delete("piutang_kartu", "faktur = '$faktur'");
        $field = "t.faktur,t.tgl,t.cabang,s.nama as namacustomer,t.customer,t.subtotal,t.diskon,t.ppn,t.total,t.piutang,t.kas,
                    t.username,t.datetime_update datetime";
        $where = "t.faktur = '$faktur'";
        $join = "left join customer s on s.kode = t.customer";
        $dbd = $this->select("penjualan_total t", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = "Piutang Penjualan [" . $faktur . "] an " . $dbr['namacustomer'];
            $this->updkartupiutang($faktur, $faktur, $dbr['customer'], $dbr['tgl'], $dbr['cabang'], $keterangan,
                $dbr['piutang'], 0, $dbr['username'], $dbr['datetime']);
        }
    }

    public function updkartupiutangreturpenjualan($faktur)
    {
        $this->delete("piutang_kartu", "faktur = '$faktur'");
        $field = "t.faktur,t.tgl,t.cabang,s.nama as namacustomer,t.customer,t.subtotal,t.total,
                    t.username,t.datetime";
        $where = "t.faktur = '$faktur'";
        $join = "left join customer s on s.kode = t.customer";
        $dbd = $this->select("penjualan_retur_total t", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = "Retur Penjualan [" . $faktur . "] an " . $dbr['namacustomer'];
            $this->updkartupiutang($faktur, $faktur, $dbr['customer'], $dbr['tgl'], $dbr['cabang'], $keterangan,
                0, $dbr['total'], $dbr['username'], $dbr['datetime']);
        }
    }

    public function updkartustockreturpenjualan($faktur)
    {
        $this->delete("stock_kartu", "faktur = '$faktur'");
        $field = "d.faktur,d.stock,d.harga,d.qty,d.jumlah,t.username,t.tgl,t.gudang,t.cabang,s.nama,t.datetime,d.hp,t.fktpj";
        $where = "d.faktur = '$faktur'";
        $join = "left join penjualan_retur_total t on t.faktur = d.faktur left join customer s on s.kode = t.customer";

        $dbd = $this->select("penjualan_retur_detail d", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {

            $hp = $this->perhitungan_m->gethpbypj($dbr['stock'], $dbr['fktpj']);
            $keterangan = "retur penjualan stock " . $dbr['nama'];
            $this->edit("penjualan_retur_detail", array("hp" => $hp), "faktur = '$faktur' and stock = '{$dbr['stock']}'");

            $this->updkartustock($faktur, $dbr['stock'], $dbr['tgl'], $dbr['gudang'], $dbr['cabang'], $keterangan, $dbr['qty'], 0,
                $hp, $dbr['username'], $dbr['datetime']);

            $cfghpp = $this->perhitungan_m->getcfghpp($dbr['tgl']);
            $saldoqty = $this->perhitungan_m->getsaldoakhirstock($dbr['stock'], $dbr['tgl'], "", $dbr['cabang']);
            $arrhp = $this->perhitungan_m->gethpstock($dbr['stock'], $dbr['tgl'], $dbr['tgl'], $saldoqty, 0, $dbr['cabang'],
                $cfghpp['caraperhitungan'], $cfghpp['periode']);
        }
    }

    public function updrekreturpenjualan($faktur)
    {
        $rekkas = getsession($this, "rekkas");
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "t.faktur,t.tgl,t.cabang,s.nama,t.subtotal,t.total,t.username,t.datetime,g.rekrj,t.fktpj,t.ppn,t.persppn";
        $where = "t.faktur = '$faktur'";
        $join = "left join customer s on s.kode = t.customer left join customer_golongan g on g.kode = s.golongan";
        $dbd = $this->select("penjualan_retur_total t", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $vapersd = array();

            $ket = "Retur Penjualan - " . $dbr['fktpj'] . "-" . $dbr['nama'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekrj'], $ket, $dbr['subtotal'], 0, $dbr['datetime'], $dbr['username']);
            $ket = "Piutang Retur Penjualan " . $dbr['nama'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekpjpiutang"), $ket, 0, $dbr['total'], $dbr['datetime'], $dbr['username']);

            $ket = "PPN Retur Penjualan " . $dbr['persppn'] . " persen ";
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekpjppn"), $ket, $dbr['ppn'], 0, $dbr['datetime'], $dbr['username']);

            //hitung hpp
            $vapersd = array();
            $f = "d.stock,d.harga,d.qty,d.jumlah,g.keterangan,g.rekpersd,s.stock_group,g.rekhpp,g.rekpj,d.hp";
            $w = "d.faktur = '$faktur'";
            $j = "left join stock s on s.kode = d.stock left join stock_group g on g.kode = s.stock_group";
            $dbd2 = $this->select("penjualan_retur_detail d", $f, $w, $j, "");
            while ($dbr2 = $this->getrow($dbd2)) {
                if (!isset($vapersd[$dbr2['stock_group']])) {
                    $vapersd[$dbr2['stock_group']] = array("jml" => 0, "hpp" => 0, "rekpersd" => $dbr2['rekpersd'], "rekhpp" => $dbr2['rekhpp'], "rekpj" => $dbr2['rekpj'], "keterangan" => $dbr2['keterangan']);
                }

                $vapersd[$dbr2['stock_group']]['jml'] += $dbr2['jumlah'];
                $vapersd[$dbr2['stock_group']]['hpp'] += $dbr2['hp'] * $dbr2['qty'];
            }

            foreach ($vapersd as $key => $val) {
                $ket = "HPP Retur PJ  - " . $dbr['fktpj'] . "-" . $val['keterangan'];
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $val['rekpersd'], $ket, $val['hpp'], 0, $dbr['datetime'], $dbr['username']);
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $val['rekhpp'], $ket, 0, $val['hpp'], $dbr['datetime'], $dbr['username']);
            }
        }
    }

    public function updkartupiutangpelunasan($faktur)
    {
        $this->delete("piutang_kartu", "faktur = '$faktur'");
        $field = "d.faktur,d.fkt,d.jumlah,d.jenis,t.tgl,t.cabang,t.username,t.datetime,s.nama as namacustomer,
                  t.customer";
        $where = "d.faktur = '$faktur'";
        $join = "left join piutang_pelunasan_total t on t.faktur = d.faktur left join customer s on s.kode = t.customer";
        $dbd = $this->select("piutang_pelunasan_detail d", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = "Pelunasan Piutang [" . $faktur . "] an " . $dbr['namacustomer'];
            $debet = 0;
            $kredit = $dbr['jumlah'];
            if ($dbr['jenis'] == 'Retur Penjualan') {
                $debet = $dbr['jumlah'];
                $kredit = 0;
            }
            $this->updkartupiutang($faktur, $dbr['fkt'], $dbr['customer'], $dbr['tgl'], $dbr['cabang'], $keterangan,
                $debet, $kredit, $dbr['username'], $dbr['datetime'], "P");
        }

        //ups karytu uang muka
        $field = "t.faktur,t.tgl,t.cabang,t.username,t.datetime,s.nama as namacustomer,b.rekening,b.keterangan as ketrekkasbank,
                t.kasbank,t.subtotal,t.diskon,t.pembulatan,t.uangmuka,t.kdtruangmuka,k.keterangan as ketuangmuka,t.customer,
                k.rekening as rekkdtruangmuka,k.dk as dktruangmuka";
        $where = "t.faktur = '$faktur'";
        $join = "left join customer s on s.kode = t.customer left join bank b on b.kode = t.rekkasbank  left join kodetransaksi k on k.kode = t.kdtruangmuka";
        $dbd = $this->select("piutang_pelunasan_total t", $field, $where, $join, "");

        while ($dbr = $this->getrow($dbd)) {
            $keterangan = $dbr['ketuangmuka'];
            $debet = 0;
            $kredit = 0;
            if ($dbr['dktruangmuka'] == "K") {
                $kredit = $dbr['uangmuka'];

            } else if ($dbr['dktruangmuka'] == "D") {
                $debet = $dbr['uangmuka'];
            }
            $this->updkartupiutang($faktur, $faktur, $dbr['customer'], $dbr['tgl'], $dbr['cabang'], $keterangan,
                $debet, $kredit, $dbr['username'], $dbr['datetime'], "U", $dbr['kdtruangmuka']);
        }
    }

    public function updrekpiutangpelunasan($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "t.faktur,t.tgl,t.cabang,t.username,t.datetime,s.nama as namacustomer,b.rekening,b.keterangan as ketrekkasbank,
                t.kasbank,t.subtotal,t.diskon,t.pembulatan,t.uangmuka,t.kdtruangmuka,k.keterangan as ketuangmuka,
                k.rekening as rekkdtruangmuka,k.dk as dktruangmuka";
        $where = "t.faktur = '$faktur'";
        $join = "left join customer s on s.kode = t.customer left join bank b on b.kode = t.rekkasbank  left join kodetransaksi k on k.kode = t.kdtruangmuka";
        $dbd = $this->select("piutang_pelunasan_total t", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $ket = "Pelunasan Piutang an " . $dbr['namacustomer'] . " - " . $dbr['ketrekkasbank'];
            $dkb = $dbr['kasbank'];
            $kkb = 0;
            if ($dbr['kasbank'] <= 0) {
                $dkb = 0;
                $kkb = $dbr['kasbank'] * -1;
            }
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekening'], $ket, $dkb, $kkb, $dbr['datetime'], $dbr['username']);
            $ket = "Pembulatan Pelunasan Piutang an " . $dbr['namacustomer'] . " - " . $dbr['ketrekkasbank'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekpjpiutangpembulatan"), $ket, $dbr['pembulatan'], 0, $dbr['datetime'],$dbr['username']);
            
	        $piutang = $dbr['subtotal'];

            //cek pellunasan penjualan aset
            $piutaset = 0 ;
            $arraset = array();
            $join2 = "left join aset a on a.fakturpj = d.fkt left join aset_golongan g on g.kode = a.golongan";
            $field2 = "d.fkt,d.jumlah,g.rekpiutangpenjualan";
            $dbd2 = $this->select("piutang_pelunasan_detail d", $field2, "d.faktur = '$faktur' and d.fkt like 'AJ%'",$join2);    
            while($dbr2 = $this->getrow($dbd2)){
                $keyaset = $dbr2['rekpiutangpenjualan']; 
                if(!isset($arraset[$keyaset]))$arraset[$keyaset] = array("rekeningpiutang"=>$dbr2['rekpiutangpenjualan'],"jumlah"=>0);
                $arraset[$keyaset]['jumlah'] += $dbr2['jumlah'];
            }	    
	    
            foreach($arraset as $keyaset => $valaset){
                $ket = "Pelunasan Piutang PJ Aset an " . $dbr['namacustomer'];
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $valaset['rekeningpiutang'], $ket, 0, $valaset['jumlah'], $dbr['datetime'], $dbr['username']);
                $piutaset += $valaset['jumlah'];
            }

            $piutang -= $piutaset;	    

            $ket = "Pelunasan Piutang an " . $dbr['namacustomer'];
            $dst = 0;
            $kst = $piutang;
            if ($piutang <= 0) {
                $dst = $piutang * -1;
                $kst = 0;
            }
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekpjpiutang"), $ket, $dst, $kst, $dbr['datetime'], $dbr['username']);

            $ket = "Pembulatan (Kelebihan) Pelunasan Piutang an " . $dbr['namacustomer'];
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekpjpiutangdisc"), $ket, 0, $dbr['diskon'], $dbr['datetime'], $dbr['username']);
            if ($dbr['uangmuka'] > 0) {
                $ket = $dbr['ketuangmuka'] . " an " . $dbr['namacustomer'];
                $dum = 0;
                $kum = 0;
                if ($dbr['dktruangmuka'] == "K") {
                    $kum = $dbr['uangmuka'];
                } else if ($dbr['dktruangmuka'] == "D") {
                    $dum = $dbr['uangmuka'];
                }
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekkdtruangmuka'], $ket, $dum, $kum, $dbr['datetime'], $dbr['username']);
            }

            // $ket = "Pelunasan Piutang an ".$dbr['namacustomer']." - ".$dbr['ketrekkasbank'];
            // $this->updbukubesar($dbr['faktur'],$dbr['cabang'],$dbr['tgl'],$dbr['rekening'],$ket,$dbr['kasbank'],0,$dbr['datetime'],$dbr['username']);
            // $ket = "Pembulatan Pelunasan Piutang an ".$dbr['namacustomer']." - ".$dbr['ketrekkasbank'];
            // $this->updbukubesar($dbr['faktur'],$dbr['cabang'],$dbr['tgl'],$this->getconfig("rekpjpiutangpembulatan"),$ket,$dbr['pembulatan'],0,$dbr['username']);
            // $ket = "Pelunasan Piutang an ".$dbr['namacustomer'];
            // $this->updbukubesar($dbr['faktur'],$dbr['cabang'],$dbr['tgl'],$this->getconfig("rekpjpiutang"),$ket,0,$dbr['subtotal'],$dbr['datetime'],$dbr['username']);
            // $ket = "Diskon Pelunasan Piutang an ".$dbr['namacustomer'];
            // $this->updbukubesar($dbr['faktur'],$dbr['cabang'],$dbr['tgl'],$this->getconfig("rekpjpiutangdisc"),$ket,0,$dbr['diskon'],$dbr['datetime'],$dbr['username']);
            // if($dbr['uangmuka'] > 0){
            //     $ket = $dbr['ketuangmuka']." an ".$dbr['namacustomer'];
            //     $dum = 0;
            //     $kum = 0;
            //     if($dbr['dktruangmuka'] == "K"){
            //         $kum = $dbr['uangmuka'];
            //     }else if($dbr['dktruangmuka'] == "D"){
            //         $dum = $dbr['uangmuka'];
            //     }
            //     $this->updbukubesar($dbr['faktur'],$dbr['cabang'],$dbr['tgl'],$dbr['rekkdtruangmuka'],$ket,$dum,$kum,$dbr['datetime'],$dbr['username']);
            // }

        }
    }

    public function updkartupiutangpenjualanaset($faktur)
    {
        $this->delete("piutang_kartu", "faktur = '$faktur'");
        $field = "a.fakturpj,a.kode,a.tglpj,a.keterangan,a.hargajual,a.nilaibukujual,a.cabang,
                a.usernamejual,a.datetimejual,a.hargaperolehan,a.customer";
        $where = "a.fakturpj = '$faktur'";
        $join = "left join customer c on c.kode = a.customer";
        $dbd = $this->select("aset a", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = "penjualan aset [" . $dbr['kode'] . " - " . $dbr['keterangan'] . "]";
            if ($dbr['tglpj'] >= '2020-01-01') {
                $this->updkartupiutang($faktur, $faktur, $dbr['customer'], $dbr['tglpj'], $dbr['cabang'], $keterangan,
                    $dbr['hargajual'], 0, $dbr['usernamejual'], $dbr['datetimejual'], "P");
            }

        }
    }

    public function updrekpenjualanaset($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "a.fakturpj,a.kode,a.tglpj,a.keterangan,a.hargajual,a.nilaibukujual,a.cabang,g.rekakmpeny,
                a.usernamejual,a.datetimejual,a.hargaperolehan,g.rekaktiva,g.rekhutpembelian,g.rekpiutangpenjualan,
                g.reklabapenjualan,g.rekrugipenjualan";
        $where = "a.fakturpj = '$faktur'";
        $join = "left join aset_golongan g on g.kode = a.golongan left join customer c on c.kode = a.customer";
        $dbd = $this->select("aset a", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {

            if ($dbr['tglpj'] >= '2020-01-01') {
                $akmpeny = $dbr['hargaperolehan'] - $dbr['nilaibukujual'];
                $ket = "Penyusutan Penjualan aset [" . $dbr['kode'] . " - " . $dbr['keterangan'] . "]";
                $this->updbukubesar($dbr['fakturpj'], $dbr['cabang'], $dbr['tglpj'], $dbr['rekakmpeny'], $ket, $akmpeny, 0, $dbr['datetimejual'], $dbr['usernamejual']);
                $ket = "Piutang Penjualan aset [" . $dbr['kode'] . " - " . $dbr['keterangan'] . "]";
                $this->updbukubesar($dbr['fakturpj'], $dbr['cabang'], $dbr['tglpj'], $dbr['rekpiutangpenjualan'], $ket, $dbr['hargajual'], 0, $dbr['datetimejual'], $dbr['usernamejual']);

                $lkredit = $dbr['hargajual'] - $dbr['nilaibukujual'];
                $ldebet = 0;
                $reklaba = $dbr['reklabapenjualan'];
                if ($lkredit < 0) {
                    $reklaba = $dbr['rekrugipenjualan'];
                    $ldebet = $lkredit * -1;
                    $lkredit = 0;

                }
                $ket = "Laba-rugi penjualan aset [" . $dbr['kode'] . " - " . $dbr['keterangan'] . "]";
                $this->updbukubesar($dbr['fakturpj'], $dbr['cabang'], $dbr['tglpj'], $reklaba, $ket, $ldebet, $lkredit, $dbr['datetimejual'], $dbr['usernamejual']);
                $ket = "HP Penjualan aset [" . $dbr['kode'] . " - " . $dbr['keterangan'] . "]";
                $this->updbukubesar($dbr['fakturpj'], $dbr['cabang'], $dbr['tglpj'], $dbr['rekaktiva'], $ket, 0, $dbr['hargaperolehan'], $dbr['datetimejual'], $dbr['usernamejual']);
            }
        }
    }
    /***************************************************/
    /**********Proses Produksi*****************************************/
    public function updkartustockperintahproduksi($faktur)
    {
        $this->delete("stock_kartu", "faktur = '$faktur'");
        $field = "b.id,b.faktur,b.tgl,b.cabang,b.username,b.datetime,b.bb,b.btkl,b.bop";
        $where = "b.faktur = '$faktur' and b.status = '1' and b.perbaikan = 'Y'";
        $join = "";
        $dbd = $this->select("produksi_total b", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {

            $keterangan = "Pengambilan produk proses perbaikan produksi [" . $dbr['faktur'] . "]";
            $cfghpp = $this->perhitungan_m->getcfghpp($dbr['tgl']);

            //select produk yg diperbaiki
            $where2 = "p.fakturproduksi = '{$dbr['faktur']}'";
            $field2 = "p.stock,p.qty,p.gudangperbaikan as gudang";
            $dbd2 = $this->select("produksi_produk p", $field2, $where2, "");
            while ($dbr2 = $this->getrow($dbd2)) {
                $saldoqty = $this->perhitungan_m->getsaldoakhirstock($dbr2['stock'], $dbr['tgl'], "", $dbr['cabang']);
                $arrhp = $this->perhitungan_m->gethpstock($dbr2['stock'], $dbr['tgl'], $dbr['tgl'], $saldoqty, $dbr2['qty'], $dbr['cabang'],
                    $cfghpp['caraperhitungan'], $cfghpp['periode']);
                $hp = devide($arrhp['hpdikeluarkan'], $dbr2['qty']);
                //edit nilai hp
                $this->edit("produksi_produk", array("hargapokokperbaikan" => $hp, "jumlahperbaikan" => $arrhp['hpdikeluarkan']), "fakturproduksi = '{$dbr['faktur']}' and stock = '{$dbr2['stock']}'");
                $this->updkartustock($faktur, $dbr2['stock'], $dbr['tgl'], $dbr2['gudang'], $dbr['cabang'], $keterangan, 0, $dbr2['qty'],
                    $hp, $dbr['username'], $dbr['datetime']);

            }
        }
    }
    public function updrekperintahproduksi($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "b.id,b.faktur,b.tgl,b.cabang,b.username,b.datetime,b.bb,b.btkl,b.bop";
        $where = "b.faktur = '$faktur' and b.status = '1'";
        $join = "";
        $dbd = $this->select("produksi_total b", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $ket = "Perintah Produksi BDP BTKL";
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekprbdpbtkl"), $ket, $dbr['btkl'], 0, $dbr['datetime'], $dbr['username']);
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekprbtkldibebankan"), $ket, 0, $dbr['btkl'], $dbr['datetime'], $dbr['username']);

            $ket = "Perintah Produksi BDP BOP";
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekprbdpbop"), $ket, $dbr['bop'], 0, $dbr['datetime'], $dbr['username']);
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekprbopdibebankan"), $ket, 0, $dbr['bop'], $dbr['datetime'], $dbr['username']);

            $where2 = "p.fakturproduksi = '{$dbr['faktur']}'";
            $field2 = "p.stock,p.qty,p.gudangperbaikan as gudang,p.jumlahperbaikan,g.rekpersd";
            $join2 = "left join stock s on s.kode = p.stock left join stock_group g on g.kode = s.stock_group";
            $dbd2 = $this->select("produksi_produk p", $field2, $where2, $join2);
            while ($dbr2 = $this->getrow($dbd2)) {
                $ket = "Perbaikan Produk BDP BBB";
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekprbdpbbb"), $ket, $dbr2['jumlahperbaikan'], 0, $dbr['datetime'], $dbr['username']);
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr2['rekpersd'], $ket, 0, $dbr2['jumlahperbaikan'], $dbr['datetime'], $dbr['username']);
            }
        }
    }
    public function updkartustockprosesproduksi($faktur)
    {
        $this->delete("stock_kartu", "faktur = '$faktur'");

        $field = "b.id,b.faktur,b.fakturproduksi,b.tgl,b.cabang,b.gudang,b.stock,b.qty,b.username,b.datetime";
        $where = "b.faktur = '$faktur' and b.status = '1'";
        $join = "";
        $dbd = $this->select("produksi_bb b", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $hp = 0;
            $keterangan = "Pengambilan Untuk proses produksi [" . $dbr['fakturproduksi'] . "]";
            $cfghpp = $this->perhitungan_m->getcfghpp($dbr['tgl']);
            $saldoqty = $this->perhitungan_m->getsaldoakhirstock($dbr['stock'], $dbr['tgl'], "", $dbr['cabang']);
            $arrhp = $this->perhitungan_m->gethpstock($dbr['stock'], $dbr['tgl'], $dbr['tgl'], $saldoqty, $dbr['qty'], $dbr['cabang'],
                $cfghpp['caraperhitungan'], $cfghpp['periode']);
            $hp = devide($arrhp['hpdikeluarkan'], $dbr['qty']);
            //edit nilai hp
            $this->edit("produksi_bb", array("hp" => $hp, "hptotkeluar" => $arrhp['hpdikeluarkan']), "id = '{$dbr['id']}'");
            $this->updkartustock($faktur, $dbr['stock'], $dbr['tgl'], $dbr['gudang'], $dbr['cabang'], $keterangan, 0, $dbr['qty'],
                $hp, $dbr['username'], $dbr['datetime']);

        }
    }
    public function updrekprosesproduksi($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "b.faktur,b.fakturproduksi,b.tgl,b.cabang,b.gudang,b.stock,b.qty,b.username,b.datetime,g.rekpersd,b.hp,b.hptotkeluar";
        $where = "b.faktur = '$faktur' and b.status = '1'";
        $join = "left join stock s on s.kode = b.stock left join stock_group g on g.kode = s.stock_group";
        $dbd = $this->select("produksi_bb b", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $jumlahproduksi = $dbr['hptotkeluar'];
            //$persdbrgproduksi = $dbr['hptotkeluar'];
            $ket = "Pengambilan Untuk proses produksi [" . $dbr['fakturproduksi'] . "]";
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekprbdpbbb"), $ket, $jumlahproduksi, 0, $dbr['datetime'], $dbr['username']);
            $ket = "Pengambilan Untuk proses produksi [" . $dbr['fakturproduksi'] . "]";
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekpersd'], $ket, 0, $jumlahproduksi, $dbr['datetime'], $dbr['username']);
            //$selisih = ($persdbrgproduksi) - ($jumlahproduksi);
            // $ket= "Selisih proses produksi  [".$dbr['fakturproduksi']."]";
            /*if($selisih > 0){
        $this->updbukubesar($dbr['faktur'],$dbr['cabang'],$dbr['tgl'],$this->getconfig("rekselisih"),$ket,$selisih,0,$dbr['datetime'],$dbr['username']);
        }else if($selisih < 0){
        $selisih = $selisih * -1;
        $this->updbukubesar($dbr['faktur'],$dbr['cabang'],$dbr['tgl'],$this->getconfig("rekselisih"),$ket,0,$selisih,$dbr['datetime'],$dbr['username']);
        }
         */
        }
    }
    /***************************************************/
    /**********Hasil Produksi*****************************************/
    public function updkartustockhasilproduksi($faktur)
    {
        $this->delete("stock_kartu", "faktur = '$faktur'");
        $field = "b.faktur,b.fakturproduksi,b.tgl,b.cabang,b.gudang,b.stock,b.qty,
                        b.username,b.datetime,t.btkl,t.bop,sum(h.hp * h.qty) as bb, sum(h.hptotkeluar) as bb2";
        $where = "b.faktur = '$faktur' and b.status = '1'";
        $join = "left join produksi_total t on t.faktur = b.fakturproduksi ";
        $join .= "left join produksi_bb h on h.fakturproduksi = t.faktur and h.status = '1'";
        $dbd = $this->select("produksi_hasil b", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {

            $perbaikan = 0;
            $where2 = "p.fakturproduksi = '{$dbr['fakturproduksi']}'";
            $field2 = "p.stock,p.qty,p.gudangperbaikan as gudang,p.jumlahperbaikan,g.rekpersd";
            $join2 = "left join stock s on s.kode = p.stock left join stock_group g on g.kode = s.stock_group";
            $dbd2 = $this->select("produksi_produk p", $field2, $where2, $join2);
            while ($dbr2 = $this->getrow($dbd2)) {
                $perbaikan += $dbr2['jumlahperbaikan'];
            }

            $tothp = $perbaikan + $dbr['btkl'] + $dbr['bop'] + $dbr['bb2'];
            $hp = devide($tothp, $dbr['qty']);
            $keterangan = "Hasil produksi [" . $dbr['fakturproduksi'] . "]";
            $this->edit("produksi_hasil", array("hp" => $hp), "faktur = " . $this->escape($faktur));
            $this->updkartustock($faktur, $dbr['stock'], $dbr['tgl'], $dbr['gudang'], $dbr['cabang'], $keterangan, $dbr['qty'], 0,
                $hp, $dbr['username'], $dbr['datetime']);
            $cfghpp = $this->perhitungan_m->getcfghpp($dbr['tgl']);
            $saldoqty = $this->perhitungan_m->getsaldoakhirstock($dbr['stock'], $dbr['tgl'], "", $dbr['cabang']);
            $arrhp = $this->perhitungan_m->gethpstock($dbr['stock'], $dbr['tgl'], $dbr['tgl'], $saldoqty, 0, $dbr['cabang'],
                $cfghpp['caraperhitungan'], $cfghpp['periode']);

        }
    }
    public function updrekhasilproduksi($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "b.faktur,b.fakturproduksi,b.tgl,b.cabang,b.gudang,b.stock,b.qty,b.username,
                        b.datetime,g.rekpersd,b.hp,t.btkl,t.bop,sum(h.hp * h.qty) as bb, sum(h.hptotkeluar) as bb2";
        $where = "b.faktur = '$faktur' and b.status = '1'";
        $join = "left join stock s on s.kode = b.stock left join stock_group g on g.kode = s.stock_group ";
        $join .= "left join produksi_total t on t.faktur = b.fakturproduksi ";
        $join .= "left join produksi_bb h on h.fakturproduksi = t.faktur and h.status = '1' ";
        $dbd = $this->select("produksi_hasil b", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $cfghpp = $this->perhitungan_m->getcfghpp($dbr['tgl']);

            $perbaikan = 0;
            $where2 = "p.fakturproduksi = '{$dbr['fakturproduksi']}'";
            $field2 = "p.stock,p.qty,p.gudangperbaikan as gudang,p.jumlahperbaikan,g.rekpersd";
            $join2 = "left join stock s on s.kode = p.stock left join stock_group g on g.kode = s.stock_group";
            $dbd2 = $this->select("produksi_produk p", $field2, $where2, $join2);
            while ($dbr2 = $this->getrow($dbd2)) {
                $perbaikan += $dbr2['jumlahperbaikan'];
            }

            $jumlahproduksi = $dbr['btkl'] + $dbr['bop'] + $dbr['bb2'] + $perbaikan;
            $persdbrgproduksi = $dbr['qty'] * $dbr['hp'];
            $ket = "Hasil produksi [" . $dbr['fakturproduksi'] . "]";
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekpersd'], $ket, $persdbrgproduksi, 0, $dbr['datetime'], $dbr['username']);
            $ket = "Perbaikan produksi [" . $dbr['fakturproduksi'] . "]";
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekprbdpbbb"), $ket, 0, $perbaikan, $dbr['datetime'], $dbr['username']);
            $ket = "BBB Hasil produksi [" . $dbr['fakturproduksi'] . "]";
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekprbdpbbb"), $ket, 0, $dbr['bb2'], $dbr['datetime'], $dbr['username']);
            $ket = "BTKL Hasil produksi [" . $dbr['fakturproduksi'] . "]";
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekprbdpbtkl"), $ket, 0, $dbr['btkl'], $dbr['datetime'], $dbr['username']);
            $ket = "BOP Hasil produksi [" . $dbr['fakturproduksi'] . "]";
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekprbdpbop"), $ket, 0, $dbr['bop'], $dbr['datetime'], $dbr['username']);

            $selisih = ($persdbrgproduksi) - ($jumlahproduksi);
            $ket = "Selisih hasil produksi  [" . $dbr['fakturproduksi'] . "]";
            if ($selisih < 0) {
                $selisih = $selisih * -1;
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekselisih"), $ket, $selisih, 0, $dbr['datetime'], $dbr['username']);
            } else if ($selisih > 0) {
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekselisih"), $ket, 0, $selisih, $dbr['datetime'], $dbr['username']);
            }
        }
    }
    /***************************************************/
    /************Mutasi kas***************************************/
    public function updrekmutasikas($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "t.faktur,t.tgl,t.diberiterima,r.keterangan as ketrekening,t.rekening,t.keterangan,t.debet,t.kredit,
                        t.cabang,t.datetime,t.username";
        $where = "t.faktur = '$faktur' and t.status = '1'";
        $join = "left join keuangan_rekening r on r.kode = t.rekening";
        $dbd = $this->select("kas_mutasi_total t", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekening'], $dbr['keterangan'],
                $dbr['debet'], $dbr['kredit'], $dbr['datetime'], $dbr['username']);

            //jurnal lawan ambil dari detail
            $f = "d.faktur,d.rekening,d.keterangan,d.debet,d.kredit";
            $w = "d.faktur = '$faktur'";
            $j = "";
            $dbd2 = $this->select("kas_mutasi_detail d", $f, $w, $j, "");
            while ($dbr2 = $this->getrow($dbd2)) {
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr2['rekening'], $dbr2['keterangan'],
                    $dbr2['debet'], $dbr2['kredit'], $dbr['datetime'], $dbr['username']);
            }
        }
    }
    /***************************************************/
    /************Mutasi bank***************************************/
    public function updrekmutasibank($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "t.faktur,t.tgl,t.diberiterima,r.keterangan as ketrekening,b.rekening,t.keterangan,t.debet,t.kredit,
                        t.cabang,t.datetime,t.username";
        $where = "t.faktur = '$faktur' and t.status = '1'";
        $join = "left join bank b on b.kode = t.bank left join keuangan_rekening r on r.kode = b.rekening";
        $dbd = $this->select("bank_mutasi_total t", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekening'], $dbr['keterangan'],
                $dbr['debet'], $dbr['kredit'], $dbr['datetime'], $dbr['username']);

            //jurnal lawan ambil dari detail
            $f = "d.faktur,d.rekening,d.keterangan,d.debet,d.kredit";
            $w = "d.faktur = '$faktur'";
            $j = "";
            $dbd2 = $this->select("bank_mutasi_detail d", $f, $w, $j, "");
            while ($dbr2 = $this->getrow($dbd2)) {
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr2['rekening'], $dbr2['keterangan'],
                    $dbr2['debet'], $dbr2['kredit'], $dbr['datetime'], $dbr['username']);
            }
        }
    }
    /***************************************************/
    /************Mutasi BG***************************************/
    public function updrekmutasibg($faktur)
    {
        //  echo("alert('dfkg');");
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "t.faktur,t.tgl,t.diberiterima,r.keterangan as ketrekening,t.rekening,t.keterangan,t.total,
                        t.cabang,t.datetime,t.username";
        $where = "t.faktur = '$faktur' and t.status = '1'";
        $join = "left join keuangan_rekening r on r.kode = t.rekening";
        $dbd = $this->select("bg_mutasi_total t", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekening'], $dbr['keterangan'],
                $dbr['total'], 0, $dbr['datetime'], $dbr['username']);
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekhutbg"), $dbr['keterangan'],
                0, $dbr['total'], $dbr['datetime'], $dbr['username']);

        }
    }

    public function updrekpencairanbg($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");

        $where = "a.fakturcair = " . $this->escape($faktur);
        $field = "a.*,b.keterangan as ketbank,s.nama as namasupplier,b.rekening as rekbank";
        $join = "left join supplier s on s.kode = a.kpddari left join bank b on b.kode = a.bank";
        $dbd = $this->select("bg_list a", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = "Pencairan BG/CEK no " . $dbr['nobgcek'] . " kepada " . $dbr['namasupplier'];
            $this->updbukubesar($dbr['fakturcair'], $dbr['cabang'], $dbr['tglcair'], $this->getconfig("rekhutbg"), $keterangan,
                $dbr['nominalcair'], 0, $dbr['datetimecair'], $dbr['usernamecair']);
            $this->updbukubesar($dbr['fakturcair'], $dbr['cabang'], $dbr['tglcair'], $dbr['rekbank'], $keterangan,
                0, $dbr['nominalcair'], $dbr['datetimecair'], $dbr['usernamecair']);
        }
    }
    /***************************************************/
    /************Mutasi Persekot***************************************/
    public function updkartuhutangpersekot($faktur)
    {
        $this->delete("hutang_kartu", "faktur = '$faktur'");

        $field = "t.faktur,t.tgl,k.keterangan as ketkdtr,t.kodetransaksi,t.cabang,t.jumlah,t.supplier,s.nama as namasupplier,
                k.dk,t.username,t.datetime,t.keterangan";
        $where = "t.faktur = '$faktur'";
        $join = "left join kodetransaksi k on k.kode = t.kodetransaksi";
        $join .= " left join supplier s on s.kode = t.supplier";
        $dbd = $this->select("persekot_mutasi_total t", $field, $where, $join);
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = $dbr['ketkdtr'] . " [" . $faktur . "] an " . $dbr['namasupplier'] . $dbr['keterangan'];
            $debet = $dbr['jumlah'];
            $kredit = 0;
            if ($dbr['dk'] == 'D') {
                $debet = 0;
                $kredit = $dbr['jumlah'];
            }
            $this->updkartuhutang($faktur, $faktur, $dbr['supplier'], $dbr['tgl'], $dbr['cabang'],
                $keterangan, $debet, $kredit, $dbr['username'], $dbr['datetime'], "P", $dbr['kodetransaksi']);
        }
    }

    public function updrekmutasipersekot($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");

        $field = "t.faktur,t.tgl,k.keterangan as ketkdtr,t.kodetransaksi,t.cabang,t.jumlah,t.supplier,s.nama as namasupplier,
                  k.dk,t.username,t.datetime,k.rekening,t.keterangan";
        $where = "t.faktur = '$faktur'";
        $join = "left join kodetransaksi k on k.kode = t.kodetransaksi";
        $join .= " left join supplier s on s.kode = t.supplier";
        $dbd = $this->select("persekot_mutasi_total t", $field, $where, $join);
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = $dbr['ketkdtr'] . " [" . $faktur . "] an " . $dbr['namasupplier'] . $dbr['keterangan'];

            $debet = 0;
            $kredit = $dbr['jumlah'];
            if ($dbr['dk'] == 'D') {
                $debet = $dbr['jumlah'];
                $kredit = 0;
            }
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekening'], $keterangan,
                $debet, $kredit, $dbr['datetime'], $dbr['username']);

            $f = "d.faktur,r.kode,concat(d.rekening,'-',r.keterangan) as ketrekening,d.jumlah,d.rekening";
            $w = "d.faktur = '$faktur'";
            $j = "left join keuangan_rekening r on r.kode = d.rekening";
            $d = $this->select("persekot_mutasi_detail d", $f, $w, $j);
            while ($dbr2 = $this->getrow($d)) {
                $debet2 = 0;
                $kredit2 = $dbr2['jumlah'];
                if ($dbr['dk'] == 'K') {
                    $debet2 = $dbr2['jumlah'];
                    $kredit2 = 0;
                }

                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr2['rekening'], $keterangan,
                    $debet2, $kredit2, $dbr['datetime'], $dbr['username']);
            }
        }
    }

    /***************************************************/
    /************Mutasi Uang Muka Penjualan***************************************/
    public function updkartupiutanguangmuka($faktur)
    {
        $this->delete("piutang_kartu", "faktur = '$faktur'");

        $field = "t.faktur,t.tgl,k.keterangan as ketkdtr,t.kodetransaksi,t.cabang,t.jumlah,t.customer,s.nama as namacustomer,k.dk,t.username,t.datetime";
        $where = "t.faktur = '$faktur'";
        $join = "left join kodetransaksi k on k.kode = t.kodetransaksi";
        $join .= " left join customer s on s.kode = t.customer";
        $dbd = $this->select("uangmuka_mutasi_total t", $field, $where, $join);
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = $dbr['ketkdtr'] . " [" . $faktur . "] an " . $dbr['namacustomer'];
            $debet = $dbr['jumlah'];
            $kredit = 0;
            if ($dbr['dk'] == 'K') {
                $debet = 0;
                $kredit = $dbr['jumlah'];
            }
            $this->updkartupiutang($faktur, $faktur, $dbr['customer'], $dbr['tgl'], $dbr['cabang'],
                $keterangan, $debet, $kredit, $dbr['username'], $dbr['datetime'], "U", $dbr['kodetransaksi']);
        }
    }

    public function updrekmutasiuangmuka($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");

        $field = "t.faktur,t.tgl,k.keterangan as ketkdtr,t.kodetransaksi,t.cabang,t.jumlah,t.customer,s.nama as namacustomer,
                  k.dk,t.username,t.datetime,k.rekening";
        $where = "t.faktur = '$faktur'";
        $join = "left join kodetransaksi k on k.kode = t.kodetransaksi";
        $join .= " left join customer s on s.kode = t.customer";
        $dbd = $this->select("uangmuka_mutasi_total t", $field, $where, $join);
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = $dbr['ketkdtr'] . " [" . $faktur . "] an " . $dbr['namacustomer'];

            $debet = 0;
            $kredit = $dbr['jumlah'];
            if ($dbr['dk'] == 'D') {
                $debet = $dbr['jumlah'];
                $kredit = 0;
            }
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekening'], $keterangan,
                $debet, $kredit, $dbr['datetime'], $dbr['username']);

            $f = "d.faktur,r.kode,concat(d.rekening,'-',r.keterangan) as ketrekening,d.jumlah,d.rekening";
            $w = "d.faktur = '$faktur'";
            $j = "left join keuangan_rekening r on r.kode = d.rekening";
            $d = $this->select("uangmuka_mutasi_detail d", $f, $w, $j);
            while ($dbr2 = $this->getrow($d)) {
                $debet2 = 0;
                $kredit2 = $dbr2['jumlah'];
                if ($dbr['dk'] == 'K') {
                    $debet2 = $dbr2['jumlah'];
                    $kredit2 = 0;
                }

                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr2['rekening'], $keterangan,
                    $debet2, $kredit2, $dbr['datetime'], $dbr['username']);
            }
        }
    }

    /***************************************************/
    /************Mutasi Deposit Penjualan***************************************/
    public function updkartupiutangdeposit($faktur)
    {
        $this->delete("piutang_kartu", "faktur = '$faktur'");

        $field = "t.faktur,t.tgl,k.keterangan as ketkdtr,t.kodetransaksi,t.cabang,t.jumlah,t.customer,s.nama as namacustomer,k.dk,t.username,t.datetime";
        $where = "t.faktur = '$faktur'";
        $join = "left join kodetransaksi k on k.kode = t.kodetransaksi";
        $join .= " left join customer s on s.kode = t.customer";
        $dbd = $this->select("deposit_mutasi_total t", $field, $where, $join);
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = $dbr['ketkdtr'] . " [" . $faktur . "] an " . $dbr['namacustomer'];
            $debet = $dbr['jumlah'];
            $kredit = 0;
            if ($dbr['dk'] == 'K') {
                $debet = 0;
                $kredit = $dbr['jumlah'];
            }
            $this->updkartupiutang($faktur, $faktur, $dbr['customer'], $dbr['tgl'], $dbr['cabang'],
                $keterangan, $debet, $kredit, $dbr['username'], $dbr['datetime'], "D", $dbr['kodetransaksi']);
        }
    }

    public function updrekmutasideposit($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");

        $field = "t.faktur,t.tgl,k.keterangan as ketkdtr,t.kodetransaksi,t.cabang,t.jumlah,t.customer,s.nama as namacustomer,
                  k.dk,t.username,t.datetime,k.rekening";
        $where = "t.faktur = '$faktur'";
        $join = "left join kodetransaksi k on k.kode = t.kodetransaksi";
        $join .= " left join customer s on s.kode = t.customer";
        $dbd = $this->select("deposit_mutasi_total t", $field, $where, $join);
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = $dbr['ketkdtr'] . " [" . $faktur . "] an " . $dbr['namacustomer'];

            $debet = 0;
            $kredit = $dbr['jumlah'];
            if ($dbr['dk'] == 'D') {
                $debet = $dbr['jumlah'];
                $kredit = 0;
            }
            $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr['rekening'], $keterangan,
                $debet, $kredit, $dbr['datetime'], $dbr['username']);

            $f = "d.faktur,r.kode,concat(d.rekening,'-',r.keterangan) as ketrekening,d.jumlah,d.rekening";
            $w = "d.faktur = '$faktur'";
            $j = "left join keuangan_rekening r on r.kode = d.rekening";
            $d = $this->select("deposit_mutasi_detail d", $f, $w, $j);
            while ($dbr2 = $this->getrow($d)) {
                $debet2 = 0;
                $kredit2 = $dbr2['jumlah'];
                if ($dbr['dk'] == 'K') {
                    $debet2 = $dbr2['jumlah'];
                    $kredit2 = 0;
                }

                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr2['rekening'], $keterangan,
                    $debet2, $kredit2, $dbr['datetime'], $dbr['username']);
            }
        }
    }

    /***************************************************/
    /**********Opname stock*****************************************/
    public function updkartustockadjstock($faktur)
    {
        $this->delete("stock_kartu", "faktur = '$faktur'");
        $field = "t.id,t.faktur,t.tgl,t.stock,s.satuan,s.keterangan as namastock,t.qtysistem,t.qtyfisik,t.qtyselisih,t.cabang,
                t.gudang,g.keterangan as ketgudang,t.keterangan,t.username,t.datetime";
        $join = "left join stock s on s.kode = t.stock left join gudang g on g.kode = t.gudang";
        $where = "t.faktur = '$faktur'";
        $dbd = $this->select("stock_adj t", $field, $where, $join);
        while ($dbr = $this->getrow($dbd)) {
            $this->edit("stock_adj", array("hp" => 0, "tothp" => 0), "id = {$dbr['id']}");
            if ($dbr['qtyselisih'] > 0) {
                $cfghpp = $this->perhitungan_m->getcfghpp($dbr['tgl']);
                $saldoqty = $this->perhitungan_m->getsaldoakhirstock($dbr['stock'], $dbr['tgl'], "", $dbr['cabang']);
                $arrhp = $this->perhitungan_m->gethpstock($dbr['stock'], $dbr['tgl'], $dbr['tgl'], $saldoqty, $dbr['qtyselisih'], $dbr['cabang'],
                    $cfghpp['caraperhitungan'], $cfghpp['periode']);
                $hp = devide($arrhp['hpdikeluarkan'], $dbr['qtyselisih']);
                $this->edit("stock_adj", array("hp" => $hp, "tothp" => $arrhp['hpdikeluarkan']), "id = {$dbr['id']}");
                $this->updkartustock($faktur, $dbr['stock'], $dbr['tgl'], $dbr['gudang'], $dbr['cabang'], $dbr['keterangan'], 0, $dbr['qtyselisih'],
                    $hp, $dbr['username'], $dbr['datetime']);

            } else {
                $dbr['qtyselisih'] = $dbr['qtyselisih'] * -1;

                $hp = 0;
                $this->updkartustock($faktur, $dbr['stock'], $dbr['tgl'], $dbr['gudang'], $dbr['cabang'], $dbr['keterangan'], $dbr['qtyselisih'], 0,
                    $hp, $dbr['username'], $dbr['datetime']);

                $cfghpp = $this->perhitungan_m->getcfghpp($dbr['tgl']);
                $saldoqty = $this->perhitungan_m->getsaldoakhirstock($dbr['stock'], $dbr['tgl'], "", $dbr['cabang']);
                $arrhp = $this->perhitungan_m->gethpstock($dbr['stock'], $dbr['tgl'], $dbr['tgl'], $saldoqty, 0, $dbr['cabang'],
                    $cfghpp['caraperhitungan'], $cfghpp['periode']);
            }

        }
    }

    public function updrekadjstock($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "t.id,t.faktur,t.tgl,t.stock,s.satuan,s.keterangan as namastock,t.qtysistem,t.qtyfisik,t.qtyselisih,t.cabang,
                t.gudang,g.keterangan as ketgudang,t.keterangan,t.username,t.datetime,t.hp,t.tothp,b.rekpersd,t.tothp";
        $join = "left join stock s on s.kode = t.stock left join gudang g on g.kode = t.gudang
                left join stock_group b on b.kode = s.stock_group";
        $where = "t.faktur = '$faktur'";
        $dbd = $this->select("stock_adj t", $field, $where, $join);
        while ($dbr = $this->getrow($dbd)) {
            $dpersd = 0;
            $kpersd = $dbr['tothp'];
            if ($dbr['qtyselisih'] < 0) {
                $dpersd = $dbr['tothp'];
                $kpersd = 0;
            }
            $this->updbukubesar($faktur, $dbr['cabang'], $dbr['tgl'], $dbr['rekpersd'], $dbr['keterangan'],
                $dpersd, $kpersd, $dbr['datetime'], $dbr['username']);

            $this->updbukubesar($faktur, $dbr['cabang'], $dbr['tgl'], $this->getconfig("rekselisihopname"), $dbr['keterangan'],
                $kpersd, $dpersd, $dbr['datetime'], $dbr['username']);

        }
    }

    public function updkartustockopname($faktur)
    {
        $this->delete("stock_kartu", "faktur = '$faktur'");
        $field = "t.faktur,d.kode,d.saldosistem,d.saldoreal,t.username,t.cabang,t.gudang,t.datetime,t.tgl";
        $where = "d.faktur = '$faktur' and t.status = '1'";
        $join = "left join stock_opname_posting_total t on t.faktur = d.faktur ";
        $dbd = $this->select("stock_opname_posting_detail d", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            //STOCK HP DI HAPUS DULU
            //$this->delete("stock_hp", "tgl = '{$dbr['tgl']}' and kode = '{$dbr['kode']}' and cabang = '{$dbr['cabang']}'");

            $cfghpp = $this->perhitungan_m->getcfghpp($dbr['tgl']);

            $keterangan = "Opname Stock [" . $dbr['faktur'] . "]";
            //$this->edit("produksi_hasil",array("hp"=>$hp),"faktur = " . $this->escape($faktur));
            $hp = 0;
            $hptot = 0;
            $debet = 0;
            $kredit = $dbr['saldosistem'];
            if ($dbr['saldosistem'] < 0) {
                $kredit = 0;
                $debet = $dbr['saldosistem'] * -1;
            }
            $arrhp = $this->perhitungan_m->getsaldohpstock($dbr['kode'], $dbr['tgl'], $dbr['cabang']);

                $arrdata = $arrhp['detailhp'];
                $hptot = $arrhp['hptot'];
            
            if($dbr['saldoreal'] <> $dbr['saldosistem']){
                if ($kredit > 0) {
                    
                    $saldosistem = $kredit;
                    //stock dikeluarkan sesuai saldo yang ada di stock_hp dan saldo sistem
                    foreach ($arrdata as $key => $val) {
                        if ($saldosistem > 0) {
                            $qtydikeluarkan = min($saldosistem, $val['qty']);
                            $this->updkartustock($faktur, $dbr['kode'], $dbr['tgl'], $dbr['gudang'], $dbr['cabang'], $keterangan, 0, $qtydikeluarkan,
                                $val['hp'], $dbr['username'], $dbr['datetime']);
                            $saldosistem -= $qtydikeluarkan;
                        }
                    }
                    //apabila dari proses sebelumnya masihada sisa dan tak terdeteksi hpnya atau tidak masuk daklam data hp maka di keluarakan dengan hp 0
                    if ($saldosistem > 0) {

                        $keterangan = "Opname Stock Selisih [" . $dbr['faktur'] . "]";
                        $this->updkartustock($faktur, $dbr['kode'], $dbr['tgl'], $dbr['gudang'], $dbr['cabang'], $keterangan, 0, $saldosistem,
                            0, $dbr['username'], $dbr['datetime']);

                    }
                } else {
                    $this->updkartustock($faktur, $dbr['kode'], $dbr['tgl'], $dbr['gudang'], $dbr['cabang'], $keterangan, $debet, 0,
                        0, $dbr['username'], $dbr['datetime']);

                }
            }


            $hprealtot = 0;
            if ($dbr['saldoreal'] > 0) {
                //$this->delete("stock_hp", "kode = '{$dbr['kode']}' and tgl = '{$dbr['tgl']}' and cabang = '{$dbr['cabang']}'");
                if($dbr['saldoreal'] <> $dbr['saldosistem']){
                    $this->delete("stock_hp", "tgl = '{$dbr['tgl']}' and kode = '{$dbr['kode']}' and cabang = '{$dbr['cabang']}'");

                    $arrhist = $arrhp = $this->perhitungan_m->gethistorydebetsaldo($dbr['kode'], $dbr['tgl'], $dbr['cabang'], $dbr['saldoreal'],$faktur);
                    foreach ($arrhist as $key => $val) {
                        $hprealtot += $val['hp'] * $val['qty'];
                        $this->updkartustock($faktur, $dbr['kode'], $dbr['tgl'], $dbr['gudang'], $dbr['cabang'], $keterangan, $val['qty'], 0,
                            $val['hp'], $dbr['username'], $dbr['datetime']);
                        $val = array("faktur" => $faktur, "tgl" => $dbr['tgl'], "kode" => $dbr['kode'],
                            "cabang" => $dbr['cabang'], "qty" => $val['qty'], "hp" => $val['hp'], "datetime" => $dbr['datetime']);
                        $this->insert("stock_hp", $val);
                    }
                }else{
                    $hprealtot = $hptot;
                }
                
            } else {
                $this->delete("stock_hp", "tgl = '{$dbr['tgl']}' and kode = '{$dbr['kode']}' and cabang = '{$dbr['cabang']}'");

                $val = array("faktur" => $faktur, "tgl" => $dbr['tgl'], "kode" => $dbr['kode'],
                    "cabang" => $dbr['cabang'], "qty" => 0, "hp" => 0, "datetime" => $dbr['datetime']);
                $this->insert("stock_hp", $val);

            }


            $this->edit("stock_opname_posting_detail", array("nilaipersdsistem" => $hptot, "nilaipersdreal" => $hprealtot), "faktur = '$faktur' and kode ='{$dbr['kode']}'");
            /*$arrhp = $this->perhitungan_m->gethpstock($dbr['stock'],$dbr['tgl'],$dbr['tgl'],$saldoqty,0,$dbr['cabang'],
        $cfghpp['caraperhitungan'],$cfghpp['periode']);*/

        }
    }
    public function updrekstockopname($faktur)
    {
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $vajurnal = array();
        $field = "t.faktur,d.kode,d.saldosistem,d.saldoreal,t.username,t.cabang,
                        t.gudang,t.datetime,t.tgl,d.nilaipersdsistem,d.nilaipersdreal,s.stock_group,g.rekpersd,g.keterangan as ketgol";
        $where = "d.faktur = '$faktur' and t.status = '1'";
        $join = "left join stock_opname_posting_total t on t.faktur = d.faktur left join stock s on s.kode = d.kode";
        $join .= " left join stock_group g on g.kode = s.stock_group";
        $dbd = $this->select("stock_opname_posting_detail d", $field, $where, $join, "");
        while ($dbr = $this->getrow($dbd)) {
            if (!isset($vajurnal[$dbr['stock_group']])) {
                $vajurnal[$dbr['stock_group']] = array("tgl" => $dbr['tgl'], "cabang" => $dbr['cabang'],
                    "rekpersd" => $dbr['rekpersd'], "keterangan" => $dbr['ketgol'],
                    "debet" => 0, "kredit" => 0,
                    "rekselisihopname" => $this->getconfig("rekselisihopname"),
                    "username" => $dbr['username'], "datetime" => $dbr['datetime']);
            }
            $vajurnal[$dbr['stock_group']]['debet'] += $dbr['nilaipersdreal'];
            $vajurnal[$dbr['stock_group']]['kredit'] += $dbr['nilaipersdsistem'];
        }

       // print_r($vajurnal);

        //upd bukubesar opname
        $arrjurnal = array();
        foreach ($vajurnal as $key => $val) {
            $selisih = $val['debet'] - $val['kredit'];
            $debet = $selisih;
            $kredit = 0;
            if ($selisih < 0) {
                $debet = 0;
                $kredit = $selisih * -1;
            }
            $keterangan = "selisih stock opname ".$val['keterangan']." real dengan sistem";
            $this->updbukubesar($faktur, $val['cabang'], $val['tgl'], $val['rekpersd'], $keterangan,
                $debet, $kredit, $val['datetime'], $val['username']);
            $this->updbukubesar($faktur, $val['cabang'], $val['tgl'], $val['rekselisihopname'], $keterangan,
                $kredit, $debet, $val['datetime'], $val['username']);

            /*if (!isset($arrjurnal[$val['rekpersd']])) {
                $arrjurnal[$val['rekpersd']] = array("cabang" => $val['cabang'], "tgl" => $val['tgl'], "saldoreal" => 0,
                    "username" => $val['username'], "datetime" => $val['datetime'],
                    "rekselisihopname" => $val['rekselisihopname']);
            }
            $arrjurnal[$val['rekpersd']]['saldoreal'] += $val['debet'];*/
        }

        //jurnal selisih neraca dan real
       /* foreach ($arrjurnal as $key => $val) {
            $saldoneraca = $this->perhitungan_m->getsaldo($val['tgl'], $key);
            $selisih = $val['saldoreal'] - $saldoneraca;
            $debet = $selisih;
            $kredit = 0;
            if ($selisih < 0) {
                $debet = 0;
                $kredit = $selisih * -1;
            }

            $keterangan = "selisih stock opname real dengan neraca";
            $this->updbukubesar($faktur, $val['cabang'], $val['tgl'], $key, $keterangan,
                $debet, $kredit, $val['datetime'], $val['username']);
            $this->updbukubesar($faktur, $val['cabang'], $val['tgl'], $val['rekselisihopname'], $keterangan,
                $kredit, $debet, $val['datetime'], $val['username']);
        }*/

    }

    /**********Stock Keluar*****************************************/
    public function updkartustockkeluar($faktur)
    {
        $this->delete("stock_kartu", "faktur = '$faktur'");
        $field = "d.id,t.faktur,t.tgl,d.stock,d.qty,t.gudang,t.cabang,t.jenis,t.keterangan,t.username,t.datetime_insert";
        $join = "left join stock_keluar_total t on t.faktur = d.faktur";
        $where = "t.faktur = '$faktur'";
        $dbd = $this->select("stock_keluar_detail d", $field, $where, $join);
        while ($dbr = $this->getrow($dbd)) {
            $this->edit("stock_keluar_detail", array("hp" => 0, "tothpkeluar" => 0), "id = {$dbr['id']}");
            
            $cfghpp = $this->perhitungan_m->getcfghpp($dbr['tgl']);
            $saldoqty = $this->perhitungan_m->getsaldoakhirstock($dbr['stock'], $dbr['tgl'], "", $dbr['cabang']);
            
            $arrhp = $this->perhitungan_m->gethpstock($dbr['stock'], $dbr['tgl'], $dbr['tgl'], $saldoqty, $dbr['qty'], $dbr['cabang'],
                    $cfghpp['caraperhitungan'], $cfghpp['periode']);
            $hp = devide($arrhp['hpdikeluarkan'], $dbr['qty']);
            $this->edit("stock_keluar_detail", array("hp" => $hp, "tothpkeluar" => $arrhp['hpdikeluarkan']), "id = {$dbr['id']}");
            $this->updkartustock($faktur, $dbr['stock'], $dbr['tgl'], $dbr['gudang'], $dbr['cabang'], "Stock keluar - ".$dbr['keterangan'], 0, $dbr['qty'],
                    $hp, $dbr['username'], $dbr['datetime_insert']);
        }
    }

    public function updrekstockkeluar($faktur)
    {
        $arrjurnal = array();
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $ft = "faktur,tgl,gudang,cabang,jenis,keterangan,username,datetime_insert,username";
        $jt = "";
        $wt = "faktur = '$faktur'";
        $dbdt = $this->select("stock_keluar_total", $ft, $wt, $jt);
        if ($dbrt = $this->getrow($dbdt)) {

            $field = "d.id,d.stock,d.qty,d.tothpkeluar,d.hp,b.rekpersd,b.rekhpp,s.stock_group,b.keterangan ketgroup";
            $join = "left join stock s on s.kode = d.stock
                    left join stock_group b on b.kode = s.stock_group";
            $where = "d.faktur = '$faktur'";
            $dbd = $this->select("stock_keluar_detail d", $field, $where, $join);
            while ($dbr = $this->getrow($dbd)) {
                if(!isset($arrjurnal[$dbr['stock_group']])) $arrjurnal[$dbr['stock_group']] = array("ket"=>$dbr['ketgroup'],
                    "rekpersd"=>$dbr['rekpersd'],"rekhpp"=>$dbr['rekhpp'],"tothp"=>0);
                $arrjurnal[$dbr['stock_group']]['tothp'] += $dbr['tothpkeluar'];
            }

            foreach($arrjurnal as $key => $val){
                $this->updbukubesar($faktur, $dbrt['cabang'], $dbrt['tgl'], $val['rekhpp'], "Stock keluar ".$val['ket'],
                    $val['tothp'], 0, $dbrt['datetime_insert'], $dbrt['username']);

                $this->updbukubesar($faktur, $dbrt['cabang'], $dbrt['tgl'], $val['rekpersd'],  "Stock keluar ".$val['ket'],
                    0, $val['tothp'], $dbrt['datetime_insert'], $dbrt['username']);
            }
        }
    }

    /***************************************************/
    /*  *******  **          **        *****************/
    /*  *******  **  ******  **  *****  ****************/
    /*  *******  **  ******  **  ******  ***************/
    /*           **          **  ******  ***************/
    /*  *******  **  ****  ****  ******  ***************/
    /*  *******  **  *****  ***  *****  ****************/
    /*  *******  **  ******  **        *****************/
    /***************************************************/
    public function updkartupinjamankaryawan($faktur,$nopinjaman,$tgl,$keterangan,$debet,$kredit,$cabang,$username,$datetime){
        $tgl = date_2s($tgl);

        $arr = array("faktur" => $faktur, "nopinjaman" => $nopinjaman, "tgl" => $tgl, 
            "keterangan" => sql_2sql($keterangan), "debet" => string_2n($debet), "kredit" => string_2n($kredit),
            "cabang" => $cabang,"datetime" => $datetime, "username" => $username);
        $this->insert("pinjaman_karyawan_kartu", $arr);
        $this->updstatuslunaspinjamankaryawan($nopinjaman);
        
    }

    public function updstatuslunaspinjamankaryawan($nopinjaman){
        $this->edit("pinjaman_karyawan",array("tgllunas"=>"0000-00-00","fktlunas"=>""),"nopinjaman = '$nopinjaman'");
        $dbd = $this->select("pinjaman_karyawan_kartu","faktur,tgl,debet,kredit","nopinjaman = '$nopinjaman'","","","debet desc,tgl asc, datetime asc");
        $bakidebet = 0 ;
        
        while($dbr = $this->getrow($dbd)){
            $bakidebet += $dbr['debet'] - $dbr['kredit'];
            if($bakidebet <= 0){
                $this->edit("pinjaman_karyawan",array("tgllunas"=>$dbr['tgl'],"fktlunas"=>$dbr['faktur']),"nopinjaman = '$nopinjaman'");
                break;
            }
        }
    }
    public function updkartupencairanpinjamankaryawan($faktur){
        $this->delete("pinjaman_karyawan_kartu", "faktur = '$faktur'");
        $field = "p.nopinjaman,p.tgl,p.fakturrealisasi,p.plafond,p.nip,k.nama,p.keterangan,p.cabang,p.username,p.datetime";
        $join = "left join karyawan k on k.kode = p.nip";
        $where = "p.fakturrealisasi = '$faktur' and p.status = '1'";
        $dbd = $this->select("pinjaman_karyawan p", $field, $where, $join);
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = "Pencairan Pinjaman an ".$dbr['nama']." ".$dbr['keterangan'];
            $this->updkartupinjamankaryawan($dbr['fakturrealisasi'],$dbr['nopinjaman'],$dbr['tgl'],
                $keterangan,$dbr['plafond'],0,$dbr['cabang'],$dbr['username'],$dbr['datetime']);
        }
    }

    public function updrekpencairanpinjamankaryawan($faktur){
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "p.nopinjaman,p.tgl,p.fakturrealisasi,p.plafond,p.nip,k.nama,p.keterangan,p.cabang,p.username,p.datetime,p.bank,b.rekening rekbank";
        $join = "left join karyawan k on k.kode = p.nip left join bank b on b.kode = p.bank";
        $where = "p.fakturrealisasi = '$faktur' and p.status = '1' and p.sttkonversi = '0'";
        $dbd = $this->select("pinjaman_karyawan p", $field, $where, $join);
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = "Pencairan Pinjaman an ".$dbr['nama']." ".$dbr['keterangan'];

            $bagian = $this->perhitunganhrd_m->getbagian($dbr['nip'],$dbr['tgl']);
            $rekpinj = $this->getval("rekening", "payroll = '{$this->getconfig("kogapypinjkaryawan")}' and bagian = '{$bagian['bagian']}'", "payroll_komponen_rekening_bagian");
            //$rekpinj = $this->getval("rekening", "kode = '{$this->getconfig("kogapypinjkaryawan")}'", "payroll_komponen");
            $this->updbukubesar($faktur, $dbr['cabang'],$dbr['tgl'],$rekpinj, $keterangan,$dbr['plafond'],0,$dbr['datetime'],$dbr['username']);
            $this->updbukubesar($faktur, $dbr['cabang'],$dbr['tgl'],$dbr['rekbank'], $keterangan,0,$dbr['plafond'],$dbr['datetime'],$dbr['username']);
        }
    }

    public function updkartuangsuranpinjamankaryawan($faktur){
        $this->delete("pinjaman_karyawan_kartu", "faktur = '$faktur'");
        
        $field = "a.faktur,a.tgl,a.keterangan,a.pokok,a.nopinjaman,p.nip,
            p.tglawalags,p.plafond,p.lama,k.nama,a.cabang,a.username,a.datetime";
        $where = "a.faktur = '$faktur'";
        $join  = "left join pinjaman_karyawan p on p.nopinjaman = a.nopinjaman left join karyawan k on k.kode = p.nip";
        $dbd   = $this->select("pinjaman_karyawan_angsuran_total a", $field, $where, $join,"a.faktur") ;

        while ($dbr = $this->getrow($dbd)) {
            $this->updkartupinjamankaryawan($dbr['faktur'],$dbr['nopinjaman'],$dbr['tgl'],
                $dbr['keterangan'],0,$dbr['pokok'],$dbr['cabang'],$dbr['username'],$dbr['datetime']);
        }
    }

    public function updrekangsuranpinjamankaryawan($faktur){
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        
        $field = "a.faktur,a.tgl,a.keterangan,a.pokok,a.nopinjaman,p.nip,
            p.tglawalags,p.plafond,p.lama,k.nama,a.cabang,a.username,a.datetime";
        $where = "a.faktur = '$faktur' and a.sttkonversi = '0'";
        $join  = "left join pinjaman_karyawan p on p.nopinjaman = a.nopinjaman left join karyawan k on k.kode = p.nip";
        $dbd   = $this->select("pinjaman_karyawan_angsuran_total a", $field, $where, $join,"a.faktur") ;

        while ($dbr = $this->getrow($dbd)) {
            //$rekpinj = $this->getval("rekening", "kode = '{$this->getconfig("kogapypinjkaryawan")}'", "payroll_komponen");
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['nip'],$dbr['tgl']);
            $rekpinj = $this->getval("rekening", "payroll = '{$this->getconfig("kogapypinjkaryawan")}' and bagian = '{$bagian['bagian']}'", "payroll_komponen_rekening_bagian");
            $this->updbukubesar($faktur, $dbr['cabang'],$dbr['tgl'],$rekpinj, $dbr['keterangan'],0,$dbr['pokok'],$dbr['datetime'],$dbr['username']);

            $f = "d.faktur,r.kode,concat(d.rekening,'-',r.keterangan) as ketrekening,d.jumlah,d.rekening";
            $w = "d.faktur = '$faktur'";
            $j = "left join keuangan_rekening r on r.kode = d.rekening";
            $d = $this->select("pinjaman_karyawan_angsuran_detail d", $f, $w, $j);
            while ($dbr2 = $this->getrow($d)) {
                $this->updbukubesar($dbr['faktur'], $dbr['cabang'], $dbr['tgl'], $dbr2['rekening'], $dbr['keterangan'],
                    $dbr2['jumlah'],0, $dbr['datetime'], $dbr['username']);
            }
        }
    }

    public function updrekpayroll($faktur){
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $this->delete("pinjaman_karyawan_kartu", "faktur = '$faktur'");
        
        $field = "p.username,p.datetime,p.cabang,p.periode,p.tgl,p.faktur";
        $where = "p.status = '1' and p.faktur = '$faktur'";
        $dbd = $this->select("payroll_posting p",$field,$where);
        if($dbr = $this->getrow($dbd)){
            $jurnal = array();

            //angsuran
            $fieldags = "a.nip,a.nopinjaman,a.pokok";
            $whereags = "a.faktur = '$faktur'";
            $joinags  = "";
            $dbdags = $this->select("payroll_posting_pinjaman_karyawan a",$fieldags,$whereags,$joinags); 
            while($dbrags = $this->getrow($dbdags)){
                $this->updkartupinjamankaryawan($faktur,$dbrags['nopinjaman'],$dbr['tgl'],
                    "Potongan pinjaman karyawan [payroll]",0,$dbrags['pokok'],$dbr['cabang'],$dbr['username'],$dbr['datetime']);
            }

            $pembulatan = 0 ;
            //menyusun rekening dari komponen
            $field2 = "k.payroll,k.total,k.potongan,k.nip";
            $where2 = "k.faktur = '$faktur'";
            $join2  = "left join payroll_komponen c on c.kode = k.payroll";
            $dbd2 = $this->select("payroll_posting_komponen k",$field2,$where2,$join2);            
            while($dbr2 = $this->getrow($dbd2)){

                $bagian = $this->perhitunganhrd_m->getbagian($dbr2['nip'],$dbr['tgl']);
                $rekening = $this->getval("rekening", "payroll = '{$dbr2['payroll']}' and bagian = '{$bagian['bagian']}'", "payroll_komponen_rekening_bagian");
                
                $key = $bagian['bagian']." - " . $rekening;
                if(!isset($jurnal[$key]))$jurnal[$key] = array("rekening"=>$rekening,
                                    "keterangan"=>"Gaji periode ".$dbr['periode'],"debet"=>0,"kredit"=>0);

                if($dbr2['potongan'] == "1"){
                    $jurnal[$key]['kredit'] += $dbr2['total'];
                    $pembulatan +=  $dbr2['total'];

                }else{
                    $jurnal[$key]['debet'] += $dbr2['total'];
                    $pembulatan -=  $dbr2['total'];

                }

            }

            //rekening pembayaran
            $fieldpby = "b.rekening,b.nominal";
            $wherepby = "b.faktur = '$faktur'";
            $joinpby  = "";
            $dbdpby = $this->select("payroll_posting_pembayaran b",$fieldpby,$wherepby,$joinpby);            
            while($dbrpby = $this->getrow($dbdpby)){
                $key = $dbrpby['rekening'];
                if(!isset($jurnal[$key]))$jurnal[$key] = array("rekening"=>$dbrpby['rekening'],
                                    "keterangan"=>"Gaji periode ".$dbr['periode'],"debet"=>0,"kredit"=>0);

                $jurnal[$key]['kredit'] += $dbrpby['nominal'];
                $pembulatan +=  $dbrpby['nominal'];

            }


            //membentuk jurnal
            foreach($jurnal as $key => $val){
                $this->updbukubesar($faktur, $dbr['cabang'], $dbr['tgl'], $val['rekening'], $val['keterangan'],
                    $val['debet'],$val['kredit'], $dbr['datetime'], $dbr['username']);
            }

            $dpembulatan = 0 ;
            $kpembulatan = 0 ;
            if($pembulatan > 0){
                $dpembulatan = $pembulatan;
            }else{
                $kpembulatan = abs($pembulatan) ;
            }

            $rekpembulatan = $this->getconfig("rekpypembulatanposting");
            $this->updbukubesar($faktur, $dbr['cabang'], $dbr['tgl'],$rekpembulatan,"Pembulatan Posting Payroll ".$dbr['periode'],
                $dpembulatan,$kpembulatan, $dbr['datetime'], $dbr['username']);
        }
    }

    public function updrekthr($faktur){
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        
        $field = "p.username,p.datetime,p.cabang,p.tgl,p.faktur";
        $where = "p.status = '1' and p.faktur = '$faktur'";
        $dbd = $this->select("thr_posting p",$field,$where);
        if($dbr = $this->getrow($dbd)){
            $jurnal = array();
            $kodethr = $this->getconfig("kogapythr");
            $thr = 0 ;
            //melihat detail menghitung total
            $field2 = "k.payroll,k.total,k.potongan,k.nip";
            $where2 = "k.faktur = '$faktur'";
            $join2  = "";
            $dbd2 = $this->select("thr_posting_komponen k",$field2,$where2,$join2);            
            while($dbr2 = $this->getrow($dbd2)){

                $bagian = $this->perhitunganhrd_m->getbagian($dbr2['nip'],$dbr['tgl']);
                
                $rekening = $this->getval("rekening", "payroll = '$kodethr' and bagian = '{$bagian['bagian']}'", "payroll_komponen_rekening_bagian");
                
                $key = $bagian['bagian']." - " . $rekening;
                if(!isset($jurnal[$key]))$jurnal[$key] = array("rekening"=>$rekening,
                                    "keterangan"=>"Tunjangan Hari Raya ","debet"=>0,"kredit"=>0);

                if($dbr2['potongan'] == "1"){
                    $jurnal[$key]['kredit'] += $dbr2['total'];
                }else{
                    $jurnal[$key]['debet'] += $dbr2['total'];
                }


            }

            //rekening pembayaran
            $fieldpby = "b.rekening,b.nominal";
            $wherepby = "b.faktur = '$faktur'";
            $joinpby  = "";
            $dbdpby = $this->select("thr_posting_pembayaran b",$fieldpby,$wherepby,$joinpby);            
            while($dbrpby = $this->getrow($dbdpby)){
                $key = $dbrpby['rekening'];
                if(!isset($jurnal[$key]))$jurnal[$key] = array("rekening"=>$dbrpby['rekening'],
                                    "keterangan"=>"Tunjangan Hari Raya ","debet"=>0,"kredit"=>0);

                $jurnal[$key]['kredit'] += $dbrpby['nominal'];
            }

            //membentuk jurnal
            foreach($jurnal as $key => $val){
                $this->updbukubesar($faktur, $dbr['cabang'], $dbr['tgl'], $val['rekening'], $val['keterangan'],
                    $val['debet'],$val['kredit'], $dbr['datetime'], $dbr['username']);
            }

        }
    }

    public function updrekasuransipembayaran($faktur){
        $this->delete("keuangan_bukubesar", "faktur = '$faktur'");
        $field = "t.faktur,t.tgl,t.asuransi,a.keterangan ketasuransi,t.karyawan,
                t.perusahaan,t.total,t.rekkas,t.keterangan,
                t.cabang,t.status,t.username,t.datetime,a.payroll";
        $where = "t.faktur = '$faktur' and t.status = '1'";
        $join  = "left join asuransi a on a.kode = t.asuransi";
        $dbd   = $this->select("asuransi_pembayaran_total t", $field, $where, $join,"t.faktur") ;
        while ($dbr = $this->getrow($dbd)) {
            $keterangan = "Pembayaran asuransi ".$dbr['ketasuransi'];

            //ambil detail
            $jurnal = array();
            $w = "d.faktur = '$faktur'";
            $j = "left join asuransi_karyawan k on k.nopendaftaran = d.nopendaftaran
                left join karyawan p on p.kode = k.karyawan left join asuransi a on a.kode = k.asuransi";
            $f = "k.nopendaftaran,k.noasuransi,k.tgl,k.karyawan,k.asuransi,k.produk,p.nama,p.alamat,a.keterangan as ketasuransi,
                d.tarifkaryawan,d.tarifperusahaan";
            $dbd2 = $this->select("asuransi_pembayaran_detail d",$f,$w,$j,"d.id");
            while($dbr2 = $this->getrow($dbd2)){
                $bagian = $this->perhitunganhrd_m->getbagian($dbr2['karyawan'],$dbr['tgl']);
                
                $rekening = $this->getval("rekening", "payroll = '{$dbr['payroll']}' and bagian = '{$bagian['bagian']}'", "payroll_komponen_rekening_bagian");
                $rekbyasuransi = $this->getval("rekbyasuransi", "kode = '{$bagian['bagian']}'", "bagian");
                
                $key = $bagian['bagian'];
                if(!isset($jurnal[$key]))$jurnal[$key] = array("rekpayroll"=>$rekening,"rekbyasuransi"=>$rekbyasuransi,
                                    "keterangan"=>$keterangan ." ".$bagian['keterangan'],"karyawan"=>0,"perusahaan"=>0);

                $jurnal[$key]['karyawan'] += $dbr2['tarifkaryawan'];
                $jurnal[$key]['perusahaan'] += $dbr2['tarifperusahaan'];
            }

            foreach($jurnal as $key => $val){
                //biaya perusahaan
                $this->updbukubesar($faktur, $dbr['cabang'],$dbr['tgl'],$val['rekpayroll'], $val['keterangan'],$val['karyawan'],0,$dbr['datetime'],$dbr['username']);
                $this->updbukubesar($faktur, $dbr['cabang'],$dbr['tgl'],$val['rekbyasuransi'], $val['keterangan'],$val['perusahaan'],0,$dbr['datetime'],$dbr['username']);       
            }
            
            //total kas / bank keluar
            $this->updbukubesar($faktur, $dbr['cabang'],$dbr['tgl'],$dbr['rekkas'], $keterangan,0,$dbr['total'],$dbr['datetime'],$dbr['username']);
        }
    }

    /***************************************************/
    /**********Posting*****************************************/
    public function postingharian($tglawal, $tglakhir, $cabang)
    {
        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);
        $this->delete("stock_kartu", "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang'");
        $this->delete("stock_hp", "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang'");
        $this->delete("keuangan_bukubesar", "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang'");
        $this->delete("po_kartu", "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang'");

        //PO
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("po_total", "faktur", $where, "", "");
        while ($dbr = $this->getrow($dbd)) {
            $this->updpokartupurchaseorder($dbr['faktur']);
        }
       
        //PEMBELIAN
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("pembelian_total", "faktur", $where, "", "");
        while ($dbr = $this->getrow($dbd)) {
            $this->updkartustockpembelian($dbr['faktur']);
            $this->updrekpembelian($dbr['faktur']);
        }

        //PEMBELIAN ASET
        $where = "tglperolehan >= '$tglawal' and tglperolehan <= '$tglakhir' and cabang = '$cabang'";
        $dbd = $this->select("aset", "faktur", $where, "", "");
        while ($dbr = $this->getrow($dbd)) {
            $this->updkartuhutangpembelianaset($dbr['faktur']);
            $this->updrekpembelianaset($dbr['faktur']);
        }

        //RETUR PEMBELIAN
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("pembelian_retur_total", "faktur", $where, "", "");
        while ($dbr = $this->getrow($dbd)) {
            $this->updkartustockreturpembelian($dbr['faktur']);
            $this->updrekreturpembelian($dbr['faktur']);
        }

        //penyesuaian stock
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("stock_adj", "faktur", $where, "", "faktur");
        while ($dbr = $this->getrow($dbd)) {
            $this->updkartustockadjstock($dbr['faktur']);
            $this->updrekadjstock($dbr['faktur']);
        }


        //keluar stock
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("stock_keluar_total", "faktur", $where, "", "faktur");
        while ($dbr = $this->getrow($dbd)) {
            $this->updkartustockkeluar($dbr['faktur']);
            $this->updrekstockkeluar($dbr['faktur']);
        }

        //PELUNASAN HUTANG
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("hutang_pelunasan_total", "faktur", $where, "", "");
        while ($dbr = $this->getrow($dbd)) {
            $this->updrekhutangpelunasan($dbr['faktur']);

        }

        //RETUR PENJUALAN PRODUK
        $where = "cabang = '$cabang' and tgl >= '$tglawal' and tgl <= '$tglakhir' and status = '1'";
        $dbd = $this->select("penjualan_retur_total", "faktur", $where, "", "");
        while ($dbr = $this->getrow($dbd)) {
            $this->updkartustockreturpenjualan($dbr['faktur']);
            $this->updrekreturpenjualan($dbr['faktur']);
        }

        //PERINTAH PRODUKSI
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("produksi_total", "faktur", $where, "", "","faktur asc","");
        while ($dbr = $this->getrow($dbd)) {
            $this->updkartustockperintahproduksi($dbr['faktur']);
            $this->updrekperintahproduksi($dbr['faktur']);
        }

       //PROSES PRODUKSI
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("produksi_bb", "faktur", $where, "", "");
        while ($dbr = $this->getrow($dbd)) {
            $this->updkartustockprosesproduksi($dbr['faktur']);
            $this->updrekprosesproduksi($dbr['faktur']);
        }

        //HASIL PRODUKSI
        $where = "cabang = '$cabang' and tgl >= '$tglawal' and tgl <= '$tglakhir' and status = '1'";
        $dbd = $this->select("produksi_hasil", "faktur", $where, "", "");
        while ($dbr = $this->getrow($dbd)) {
            $this->updkartustockhasilproduksi($dbr['faktur']);
            $this->updrekhasilproduksi($dbr['faktur']);
        }

        

        //PENJUALAN PRODUK
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("penjualan_total", "faktur", $where, "", "");
        while ($dbr = $this->getrow($dbd)) {
            $this->updkartustockpenjualan($dbr['faktur']);
            $this->updrekpenjualan($dbr['faktur']);
        }

        //PENJUALAN ASET
        $where = "tglpj >= '$tglawal' and tglpj <= '$tglakhir' and cabang = '$cabang'";
        $dbd = $this->select("aset", "fakturpj", $where, "", "");
        while ($dbr = $this->getrow($dbd)) {
            $this->updkartupiutangpenjualanaset($dbr['fakturpj']);
            $this->updrekpenjualanaset($dbr['fakturpj']);
        }

        

        //stock opname
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("stock_opname_posting_total", "faktur", $where, "", "faktur");
        while ($dbr = $this->getrow($dbd)) {
            $this->updkartustockopname($dbr['faktur']);
            $this->updrekstockopname($dbr['faktur']);
        }

        //PELUNASAN PIUTANG
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("piutang_pelunasan_total", "faktur", $where, "", "");
        while ($dbr = $this->getrow($dbd)) {
            $this->updrekpiutangpelunasan($dbr['faktur']);
        }

        //JURNAL
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang'";
        $dbd = $this->select("keuangan_jurnal", "faktur", $where, "", "faktur");
        while ($dbr = $this->getrow($dbd)) {
            $this->updrekjurnal($dbr['faktur']);
        }

        //Mutasi Kas
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("kas_mutasi_total", "faktur", $where, "", "faktur");
        while ($dbr = $this->getrow($dbd)) {
            $this->updrekmutasikas($dbr['faktur']);
        }

        //Mutasi Bank
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("bank_mutasi_total", "faktur", $where, "", "faktur");
        while ($dbr = $this->getrow($dbd)) {
            $this->updrekmutasibank($dbr['faktur']);
        }

        //Mutasi Persekot
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("persekot_mutasi_total", "faktur", $where, "", "faktur");
        while ($dbr = $this->getrow($dbd)) {
            $this->updrekmutasipersekot($dbr['faktur']);
        }

        //Mutasi uangmuka
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("uangmuka_mutasi_total", "faktur", $where, "", "faktur");
        while ($dbr = $this->getrow($dbd)) {
            $this->updrekmutasiuangmuka($dbr['faktur']);
        }

        //Mutasi BG
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("bg_mutasi_total", "faktur", $where, "", "faktur");
        while ($dbr = $this->getrow($dbd)) {
            $this->updrekmutasibg($dbr['faktur']);
        }

        //Pencairan BG
        $where = "tglcair >= '$tglawal' and tglcair <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("bg_list", "fakturcair", $where, "", "fakturcair");
        while ($dbr = $this->getrow($dbd)) {
            $this->updrekpencairanbg($dbr['fakturcair']);
        }

        //pencairan pinjaman
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("pinjaman_karyawan", "fakturrealisasi", $where, "", "fakturrealisasi");
        while ($dbr = $this->getrow($dbd)) {
            $this->updkartupencairanpinjamankaryawan($dbr['fakturrealisasi']);
            $this->updrekpencairanpinjamankaryawan($dbr['fakturrealisasi']);
        }

        //payroll
        $where = "tgl >= '$tglawal' and tgl <= '$tglakhir' and cabang = '$cabang' and status = '1'";
        $dbd = $this->select("payroll_posting", "faktur", $where, "", "faktur");
        while ($dbr = $this->getrow($dbd)) {
            $this->updrekpayroll($dbr['faktur']);
        }

    }
    /***************************************************/
}
