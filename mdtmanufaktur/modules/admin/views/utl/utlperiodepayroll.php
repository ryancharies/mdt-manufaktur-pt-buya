<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tperiodepayroll">
                        <button class="btn btn-tab tpel active" href="#tperiodepayroll_1" data-toggle="tab" >Daftar Periode Payroll</button>
                        <button class="btn btn-tab tpel" href="#tperiodepayroll_2" data-toggle="tab">Periode Payroll</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.utlperiodepayroll.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tperiodepayroll_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tperiodepayroll_2">
                    <table class="osxtable form">
                        <tr>
                            <td width="14%"><label for="kode">Kode</label> </td>
                            <td width="1%">:</td>
                            <td >
                                <input readonly type="text" id="kode" name="kode" class="form-control" placeholder="Kode" required>
                            </td>
                        </tr>
                        <tr>
                            <td width="14%"><label for="keterangan">Keterangan</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Keterangan" required>
                            </td>
                        </tr>
                        <tr>
                            <td width="14%"><label for="tglawal">Tgl Awal</label> </td>
                            <td width="1%">:</td>
                            <td >
                                <input caption="sd" style="width:80px" type="text" class="form-control datetr" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                            </td>
                        </tr>
                        <tr>
                            <td width="14%"><label for="tglakhir">Tgl Akhir</label> </td>
                            <td width="1%">:</td>
                            <td >
                                <input caption="sd" style="width:80px" type="text" class="form-control datetr" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.utlperiodepayroll.grid1_data 	 = null ;
    bos.utlperiodepayroll.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.utlperiodepayroll.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.utlperiodepayroll.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,frozen:true,style:"text-align:center;"},
                { field: 'keterangan', caption: 'Keterangan', size: '250px', sortable: false,frozen:true},
                { field: 'tglawal', caption: 'Tgl Awal', size: '100px', sortable: false},
                { field: 'tglakhir', caption: 'Tgl Akhir', size: '100px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.utlperiodepayroll.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.utlperiodepayroll.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.utlperiodepayroll.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.utlperiodepayroll.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.utlperiodepayroll.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.utlperiodepayroll.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.utlperiodepayroll.cmdnonaktif		= function(id){
        if(confirm("Non Aktifkan Periode?")){
            bjs.ajax(this.url + '/nonaktif', 'kode=' + id);
        }
    }

    bos.utlperiodepayroll.cmdaktif		= function(id){
        if(confirm("Aktifkan Periode?")){
            bjs.ajax(this.url + '/aktif', 'kode=' + id);
        }
    }

    bos.utlperiodepayroll.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        bjs.ajax(this.url + "/init") ;
        bjs.ajax(this.url + '/getkode') ;
    }

    bos.utlperiodepayroll.settab 		= function(n){
        this.obj.find("#tperiodepayroll button:eq("+n+")").tab("show") ;
    }

    bos.utlperiodepayroll.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.utlperiodepayroll.grid1_render() ;
            bos.utlperiodepayroll.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#kode").focus() ;
        }
    }

    bos.utlperiodepayroll.initcomp	= function(){
        /*bjs.initselect({
            class : "#" + this.id + " .select2",
            clear : true
        }) ;*/
        bjs.ajax(this.url + '/getkode') ;
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;        
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.utlperiodepayroll.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.utlperiodepayroll.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.utlperiodepayroll.grid1_destroy() ;
        }) ;
    }

    bos.utlperiodepayroll.objs = bos.utlperiodepayroll.obj.find("#cmdsave") ;
    bos.utlperiodepayroll.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(confirm("Data disimpan??")){
                if(bjs.isvalidform(this)){                
                    bjs.ajax( bos.utlperiodepayroll.url + '/saving', bjs.getdataform(this)) ;
                }
            }
        });
    }

    $(function(){
        bos.utlperiodepayroll.initcomp() ;
        bos.utlperiodepayroll.initcallback() ;
        bos.utlperiodepayroll.initfunc() ;
    }) ;
</script>
