<div class="header active">
	<table class="header-table">
		<tr>
			<td class="icon" ><i class="fa fa-building"></i></td>
			<td class="title">
			MDT Support Online
			</td>
			<td class="button">
				<table class="header-button" align="right">
					<tr>
						<td>
							<div class="btn-circle btn-close transition" onclick="bos.supportonline.close()">
								<img src="./uploads/titlebar/close.png">
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div><!-- end header -->
<div class="body">
	<div class="bodyfix scrollme" style="height:100%">
	<iframe height="100%" width="100%" frameborder="0" src="https://support.mdtsolution.com/mdtsupport/public/frame?authtoken=<?= $authtoken ?>&username=<?= $username ?>"></iframe>
</div>
</div>

