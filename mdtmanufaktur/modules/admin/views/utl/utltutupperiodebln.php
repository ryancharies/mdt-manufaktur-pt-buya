<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<form novalidate>         
    <div class="bodyfix scrollme" style="height:100%"> 
        <table class="osxtable form" border="0">
            <tr>
                <td width="80px"><label for="periode">Periode</label> </td>
                <td width="20px">:</td>
                <td> 
                    <input style="width:80px" type="text" class="form-control datetryears" id="periode" name="periode" required value=<?=date("Y")?> <?=date_periodset(false)?> <?=$mintgl?>>
                </td>
            </tr>
        </table> 
        <div class="row" style="height: calc(100% - 50px);"> 
            <div class="col-sm-12 full-height">
                <div id="grid1" class="full-height"></div>
            </div>  
        </div> 
    </div>
</form>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.utltutupperiodebln.grid1_data 	 = null ;
    bos.utltutupperiodebln.grid1_loaddata= function(){
        var periode = bos.utltutupperiodebln.obj.find("#periode").val();
        this.grid1_data 		= {'periode':periode} ;
    }

    bos.utltutupperiodebln.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.utltutupperiodebln.base_url + "/loadgrid",
            postData : this.grid1_data ,
            header 		: 'No Show',
            show 		: {
                footer 		: false,
                header 		: false,
                toolbar		: false,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'no', caption: 'No',render:'int', size: '30px', sortable: false},
                { field: 'bulan', caption: 'Bulan', size: '120px', sortable: false},
                { field: 'status', caption: 'Status', size: '150px',sortable: false},
                { field: 'btnopen', caption: '', size: '80px', sortable: false},
                { field: 'btnclose', caption: '', size: '80px', sortable: false}
            ]
        });
    }

    bos.utltutupperiodebln.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.utltutupperiodebln.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.utltutupperiodebln.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.utltutupperiodebln.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.utltutupperiodebln.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }


    bos.utltutupperiodebln.initcomp	= function(){

        bjs.initdatetryears("#" + this.id + " .datetryears") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }  

    bos.utltutupperiodebln.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.utltutupperiodebln.tabsaction( e.i )  ;
        });  

        this.obj.on("remove",function(){
            bos.utltutupperiodebln.grid1_destroy() ;
        }) ;   	

    }
    bos.utltutupperiodebln.cmdclose = function(periode){
        var cfrm = confirm("Apakah periode akan ditutup? Pastikan anda sudah melakukan proses posting sesuai satndart, dan laporan anda benar!!");
        if(cfrm){
            bjs.ajax(this.url + '/closeperiode', 'periode=' + periode);
        }
    }
    bos.utltutupperiodebln.cmdopen = function(periode){
        var cfrm = confirm("Apakah periode akan dibuka? perlu diketahui apabla periode dibuka maka akan membuka semua periode mulai dari periode yang dipilih sampai dengan sekarang!!");
        if(cfrm){
            bjs.ajax(this.url + '/openperiode', 'periode=' + periode);
        }
    }

    bos.utltutupperiodebln.objs = bos.utltutupperiodebln.obj.find("#cmdposting") ;
    bos.utltutupperiodebln.initfunc 		= function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("#periode").on("change", function(e){
            bos.utltutupperiodebln.grid1_reloaddata();
        }) ;
    }
    $(function(){
        bos.utltutupperiodebln.initcomp() ;
        bos.utltutupperiodebln.initcallback() ;
        bos.utltutupperiodebln.initfunc() ;
    }) ;
</script>