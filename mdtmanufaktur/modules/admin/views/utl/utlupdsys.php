<style media="screen">

    .lebodi{
        background-color: #f5f5f5;
    }

</style>
<form>         
    <div class="bodyfix scrollme lebodi" style="height:100%"> 
        <table class="osxtable form" border="0">
            <tr>
                <td>Upload ZIP</td>
                <td>:</td>
                <td>
                    <input class="form-control" type="file" name="uplFile" id="uplFile" size="20"/>                    
                </td>
            </tr>
            <!-- <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>
                    <button class="btn btn-primary pull-right" id="cmdUpdate">Update</button>
                </td>
            </tr> -->
        </table>
    </div>
</form>

<script type="text/javascript">
	if(typeof bos === "undefined") window.location.href = "<?=base_url()?>";

    bos.utlupdsys.initcomp	= function(){

        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }  

    bos.utlupdsys.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.utlupdsys.tabsaction( e.i )  ;
        }); 
    }

    bos.utlupdsys.initfunc		= function(){
        this.obj.find("form").on("submit", function(e){ 
            e.preventDefault() ; 
        });

        this.obj.find("#uplFile").on("change", function(e){
            e.preventDefault() ;
            bos.utlupdsys.cfile    = e.target.files ;
            bos.utlupdsys.gfile    = new FormData() ;
            $.each(bos.utlupdsys.cfile, function(cKey,cValue){
                bos.utlupdsys.gfile.append(cKey,cValue) ;
            }) ;
            
            
            bjs.ajaxfile(bos.utlupdsys.base_url + "/file_upload", bos.utlupdsys.gfile, this) ;
        }) ;
	}

    $(function(){
        bos.utlupdsys.initcomp() ;
        bos.utlupdsys.initcallback() ;
        bos.utlupdsys.initfunc() ;
    }) ;

</script>
