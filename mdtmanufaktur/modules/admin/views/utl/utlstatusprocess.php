<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<form novalidate>         
    <div class="bodyfix scrollme" style="height:100%">
        <div class="row" style="height: calc(100% - 50px);"> 
            <div class="col-sm-12 full-height">
                <div id="grid1" class="full-height"></div>
            </div>  
        </div> 
    </div>
</form>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.utlstatusprocess.grid1_data 	 = null ;
    bos.utlstatusprocess.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.utlstatusprocess.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.utlstatusprocess.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'no', caption: 'No', size: '40px', sortable: false},
                { field: 'list', caption: 'List', size: '200px', sortable: false},
                { field: 'status', caption: 'Status', size: '80px', sortable: false}
            ]
        });
    }

    bos.utlstatusprocess.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.utlstatusprocess.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.utlstatusprocess.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.utlstatusprocess.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.utlstatusprocess.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }


    bos.utlstatusprocess.initcomp	= function(){

        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }  

    bos.utlstatusprocess.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.utlstatusprocess.tabsaction( e.i )  ;
        });  

        this.obj.on("remove",function(){
            bos.utlstatusprocess.grid1_destroy() ;
        }) ;   	

    }

    bos.utlstatusprocess.objs = bos.utlstatusprocess.obj.find("#cmdposting") ;
    bos.utlstatusprocess.initfunc 		= function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){ 
            e.preventDefault() ;
        });
    }
    $(function(){
        bos.utlstatusprocess.initcomp() ;
        bos.utlstatusprocess.initcallback() ;
        bos.utlstatusprocess.initfunc() ;
    }) ;
</script>