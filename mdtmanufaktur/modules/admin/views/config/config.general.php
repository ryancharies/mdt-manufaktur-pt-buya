<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<label for="app_company">Nama Perusahaan</label>
			<input type="text" name="app_company" id="app_company" placeholder="Nama Perusahaan" value="<?=$app_company?>"
			class="form-control">
        </div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<label for="app_company_telp">Telp Perusahaan</label>
			<input type="text" name="app_company_telp" id="app_company_telp" placeholder="Telp Perusahaan" value="<?=$app_company_telp?>"
			class="form-control">
        </div>
	</div>
	<div class="col-sm-12">
		<div class="form-group">
			<label for="app_company_addr">Alamat Perusahaan</label>
			<input type="text" name="app_company_addr" id="app_company_addr" placeholder="Alamat Perusahaan" value="<?=$app_company_addr?>"
			class="form-control">
        </div>
	</div>
	<div class="col-sm-12">
		<div class="form-group">
			<label for="app_title">App Title</label>
			<input type="text" name="app_title" id="app_title" placeholder="App Title" value="<?=$app_title?>"
			class="form-control">
        </div>
	</div>

	<div class="col-sm-6">
		<div class="form-group">
			<label for="fapp_logo">App Logo <span id="idlapp_logo"></span></label>
			<input type="file" id="app_logo" accept="image/*" class="fupload">
			<div id="idapp_logo"><?=$app_logo?></div>
        </div>
	</div>

	<div class="col-sm-6">
		<div class="form-group">
			<label for="fapp_login_image">App login Image <span id="idlapp_login_image"></span></label>
			<input type="file" id="app_login_image" accept="image/*" class="fupload">
			<div id="idapp_login_image"><?=$app_login_image?></div>
        </div>
	</div>
</div>
