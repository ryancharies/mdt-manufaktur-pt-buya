<div class="header active">
	<table class="header-table">
		<tr>
			<td class="icon" ><i class="fa fa-building"></i></td>
			<td class="title">
				Ganti Password
			</td>
			<td class="button">
				<table class="header-button" align="right">
					<tr>
						<td>
							<div class="btn-circle btn-close transition" onclick="bos.gantipassword.close()">
								<img src="./uploads/titlebar/close.png">
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div><!-- end header -->
<div class="body">
	<form>
	<div class="bodyfix scrollme" style="height:100%">
		<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="password" id="lblpasswordskrg">Password Sekarang</label>
							<input type="password" class="form-control" name="passwordskrg" id="passwordskrg" placeholder="Password Sekarang" required>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
						<label for="passwordbru">Password Baru</label>
							<input type="password" class="form-control" name="passwordbru" id="passwordbru" placeholder="Password Baru" required>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="form-group">
							<label for="passwordbru2" id="lblpasswordbru2">Masukkan Ulang Password Baru</label>
							<input type="password" class="form-control" name="passwordbru2" id="passwordbru2" placeholder="Masukkan Ulang Password Baru" required>
						</div>
					</div>
                    
				</div>
	</div>
	<div class="footer fix" style="height:32px"">
		<button class="btn btn-primary btn-block" id="cmdsave">Simpan</button>
	
	</div>
	</from>
</div>
<script type="text/javascript">
	if(typeof bos === "undefined") window.location.href = "<?=base_url()?>";
	bos.gantipassword.init 			= function(){
		this.obj.find("#passwordskrg").val("").attr("readonly", false).focus() ;
		this.obj.find("#passwordbru").val("") ;
        this.obj.find("#passwordbru2").val("") ;
	}

	bos.gantipassword.initcomp		= function(){
		bjs.initenter(this.obj.find("form")) ;
		this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
	}

	bos.gantipassword.initcallback	= function(){
		
	}

	bos.gantipassword.cekpassbru = function(){
		if(this.obj.find("#passwordbru").val() !== this.obj.find("#passwordbru2").val()){
			this.obj.find("#lblpasswordbru2").css("color", "red");
		}else{
			this.obj.find("#lblpasswordbru2").css("color", "black");
		}
	}
	
	bos.gantipassword.cekpassskrg = function(){
		bjs.ajax(this.base_url+ '/cekpassskrg', bjs.getdataform(this.obj.find("form"))) ;
	}

   	bos.gantipassword.cmdsave       = bos.gantipassword.obj.find("#cmdsave") ;
	bos.gantipassword.initfunc		= function(){
		this.obj.find('#passwordbru').on("blur", function(e){
			bos.gantipassword.cekpassbru();
		});
		this.obj.find('#passwordbru2').on("blur", function(e){
			bos.gantipassword.cekpassbru();
		});

		this.obj.find('#passwordskrg').on("blur", function(e){
			bos.gantipassword.cekpassskrg();
		});

		this.obj.find('form').on("submit", function(e){
         e.preventDefault() ;
			if( bjs.isvalidform(this) ){
				bjs.ajax( bos.gantipassword.base_url + '/saving', bjs.getdataform(this) , bos.gantipassword.cmdsave) ;
			}
		}) ;
	}

	$(function(){
		bos.gantipassword.initcomp() ;
		bos.gantipassword.initcallback() ;
		bos.gantipassword.initfunc() ;
	})
</script>
