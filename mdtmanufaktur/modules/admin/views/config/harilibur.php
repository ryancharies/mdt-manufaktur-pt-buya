<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Setup Hari Libur</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.harilibur.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div>
<div class="body">
    <form novalidate>         
        <div class="bodyfix scrollme" style="height:100%;width:100%"> 
            <div data-provide="calendar" style="height:100%;width:100%" class="hariliburcalendar"></div>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    bos.harilibur.data = <?= $harilibur ;?>;
    bos.harilibur.initcomp	= function(){

       // bjs.initdateyears("#" + this.id + " .dateyears") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        
        


        
    }  

    bos.harilibur.setdatalibur = function(thn){
        bjs.ajax(this.url + '/updatecalender', "thn="+thn) ;

        // var gan = [
        //             {
        //                 startDate: new Date(e.currentYear, 1, 4),
        //                 endDate: new Date(e.currentYear, 1, 4),
        //                 color: 'red'
        //             },
        //             {
        //                 startDate: new Date(e.currentYear, 2, 7),
        //                 endDate: new Date(e.currentYear, 2, 11),
        //                 color: 'pink'
        //             },
        //             {
        //                 startDate: new Date(e.currentYear, 3, 5),
        //                 endDate: new Date(e.currentYear, 5, 15),
        //                 color: '#48A38C'
        //             }
        //         ];
        //         calendar.setDataSource(gan);
    }

    bos.harilibur.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.harilibur.tabsaction( e.i )  ;
        });  

        this.obj.on("remove",function(){
            bos.harilibur.grid1_destroy() ;
        }) ;   	

    }

    bos.harilibur.objs = bos.harilibur.obj.find("#cmdposting") ;
    bos.harilibur.initfunc 		= function(){
        this.obj.find("form").on("submit", function(e){ 
            e.preventDefault() ;
        });

    }

    bos.harilibur.setharilibur = function(datalibur){
        bos.harilibur.data = datalibur;
        var gan = [];
        for(var k in datalibur) {
            gan.push({
                        startDate: new Date(datalibur[k].tahun, datalibur[k].bulan-1,datalibur[k].hari),
                        endDate: new Date(datalibur[k].tahun, datalibur[k].bulan-1,datalibur[k].hari),
                        color: 'red'
                    });
        }

        calendar.setDataSource(gan);
    }
    
    bos.harilibur.updateharilibur = function(e){
        var tgl = datejs_2s(e.date);
        var ada = false;
        console.log(bos.harilibur.data);
        for(var k in bos.harilibur.data){
            if(bos.harilibur.data[k].tgl == tgl){
                ada = true;
            }            
        }

        if(ada){
            if(confirm("Tgl dirubah menjadi hari normal?")){
                bjs.ajax(this.url + '/saving', "tgl="+tgl+"&libur=0") ;
            }
        }else{
            if(confirm("Tgl dijadikan hari libur?")){
                bjs.ajax(this.url + '/saving', "tgl="+tgl+"&libur=1") ;
            }
        }
    }

    const calendar = new Calendar('.hariliburcalendar', {
                            clickDay: function(e) {
                                bos.harilibur.updateharilibur(e);  
                            },
                            yearChanged: function(e) {
                                bos.harilibur.setdatalibur(e.currentYear);                
                            }
                        });
    
    $(function(){
        calendar.setStyle('background');
        calendar.setLanguage('id');
        
        bos.harilibur.setharilibur(bos.harilibur.data);

        bos.harilibur.initcomp() ;
        bos.harilibur.initcallback() ;
        bos.harilibur.initfunc() ;
    }) ;

    
</script>