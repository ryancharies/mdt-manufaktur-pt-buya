<div class="header active">
	<table class="header-table">
		<tr>
			<td class="icon" ><i class="fa fa-building"></i></td>
			<td class="title">
				Info Aplikasi
			</td>
			<td class="button">
				<table class="header-button" align="right">
					<tr>
						<td>
							<div class="btn-circle btn-close transition" onclick="bos.infoaplikasi.close()">
								<img src="./uploads/titlebar/close.png">
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div><!-- end header -->
<div class="body">
	<form>
	<div class="scrollme" style="height:100%;width:100%;padding-top:30px;">
	<b>Version 2.7.4 (15-Okt-23)</b>
		<ul> Perubahan:
			<li>Perubahan typo pada beebrapa menu</li>
			<li>Perubahan tgl pada PO agar bisa edit mundur</li>
		</ul>
		<b>Version 2.7.3 (29-Jul-23)</b>
		<ul> Perubahan:
			<li>Hasil produksi mengambil dari data regu</li>
			<li>Konversi data regu untuk petugas yang belum di entry menggunakan data regu</li>
		</ul>
		<b>Version 2.7.2 (19-Jul-23)</b>
		<ul> Penambahan:
			<li>KPI perhitungan hasil produksi</li>
			<li>Reward perjabatan</li>
		</ul>
		<ul> Perubahan:
			<li>KPI Penilaian</li>
			<li>Faktur induk pada kartu hutang</li>
			<li>Faktur induk pada kartu piutang</li>
		</ul>
		<b>Version 2.7.1 (04-Jul-23)</b>
		<ul> Perubahan:
			<li>User Cabang</li>
			<li>Konsolidasi Cabang Lap Neraca & Aset</li>
			<li>Konversi Penjualan Free</li>
		</ul>
		<b>Version 2.7.0 (25-Jun-23)</b>
		<ul> Penambahan:
			<li>KPI</li>
			<li>Konsolidasi Cabang</li>
		</ul>
		<b>Version 2.6.2 (12-Jun-23)</b>
		<ul> Penyesuaian:
			<li>Menu Pelunasan hutang & piutang</li>
			<li>Optimasi database</li>
		</ul>
		<b>Version 2.6.0 (13-April-23)</b>
		<ul> Penyesuaian:
			<li>Pelunasan hutang untuk transaksi perubahan aset</li>
			<li>Laporan penyusutan aktiva & inventaris untuk transaksi perubahan aset</li>
		</ul>
		<ul> Penambahan:
			<li>Perubahan Aset & Inventaris</li>
		</ul>
		<b>Version 2.5.4 (05-Maret-23)</b>
		<ul> Perbaikan:
			<li>Laporan Material Produk</li>
			<li>Laporan Kartu PO</li>
		</ul>
		<ul> Penyesuaian:
			<li>Master Aset & Inventaris untuk kelompok aset</li>
		</ul>
		<ul> Penambahan:
			<li>Kelompok Aset & Inventaris</li>
		</ul>
		<b>Version 2.5.3 (28-Februari-23)</b>
		<ul> Penyesuaian:
			<li>Validasi pada transaksi pelunasan piutang</li>
			<li>Penambahan pencarian berdasakan nama pada setup tarif asuransi</li>
			<li>Penyesuaian backup DB (optimasi)</li>
		</ul>
		<b>Version 2.5.2 (03-Februari-23)</b>
		<ul> Penambahan:
			<li>Lap. penjualan produk untuk bulanan dan grafik</li>
		</ul>
		<ul> Penyesuaian:
			<li>Perubahan slip untuk 1x periode bulanan agar hilang</li>
			<li>Penambahan hapus PIN pada sistem ketika karyawan resign</li>
			<li>Optimasi laporan</li>
		</ul>
		<b>Version 2.5.1 (17-Jan-23)</b>
		<ul> Penambahan:
			<li>Lap. penjualan produk</li>
		</ul>
		<ul> Penyesuaian:
			<li>Transaksi penjualan (kelompok stock)</li>
			<li>Transaksi retur penjualan (kelompok stock)</li>
		</ul>
		<ul> Perbaikan:
			<li>Laporan analisa keuangan semester</li>
			<li>Transaksi pembelian aste (Master aktiva dan BDD)</li>
			<li>Lap Checklist Accounting</li>
		</ul>
		<b>Version 2.5.0 (11-Jan-23)</b>
		<ul> Penambahan:
			<li>Master Dati 1 Provinsi</li>
			<li>Master Dati 2 Kota Kabupaten</li>
			<li>Master Kelompok Stock</li>
		</ul>
		<ul> Perubahan:
			<li>Multi vendor pembelian aset</li>
			<li>Penambahan kota pada master supplier</li>
			<li>Penambahan kelompok pada master stock</li> 
		</ul>
		<b>Version 2.4.18 (06-Des-22)</b>
		<ul> Penambahan:
			<li>Riwayat produksi pada detil perintah produksi</li>
		</ul>
		<ul> Perubahan:
			<li>Tidak bisa melakukan hapus pada saat edit hasil produksi, harus dilakukan transaksi batal produksi</li>
		</ul>
		<b>Version 2.4.17 (02-Des-22)</b>
		<ul> Penambahan:
			<li>Laporan Analisa Ratio Keuangan</li>
		</ul>
		<ul> Perubahan:
			<li>Laporan Analisa Ratio Bulan, Triwulan, Semester, Tahun dijadikan satu pada laporan analisa ratio keuangan</li>
			<li>Penyesuaian ukuran bagian pilih stok</li>
		</ul>
		<ul> Perbaikan:
			<li>Penguncian tanggal pada transaksi proses dan hasil produksi</li>
		</ul>
		<b>Version 2.4.16 (29-Nov-22)</b>
		<ul> Penambahan:
			<li>Jenis pengeluaran barang (Sample)</li>
			<li>Detail mutasi bank pada grid daftar transaksi mutasi bank</li>
		</ul>
		<ul> Perbaikan:
			<li>Lebar daftar stock untuk menu-menu transaksi yang berkaitan dengan stok</li>
		</ul>
		<b>Version 2.4.15 (21-Nov-22)</b>		
		<ul> Penambahan:
			<li>Penambahan “Detail” di Mutasi Kas dan Mutasi Bank untuk mengetahui lawan dari akun kas/bank</li>
			<li>Penambahan lebar keterangan untuk detial mutai bank</li>
			<li>Penambahan menu stok keluar</li>
			<li>Perubahan rumus prosentase lap triwulan</li>
			<li>Penambahan total debit kredit di kartu stock</li>
			<li>Penambahan master pendidikan terakhir</li>
			<li>Penambahan tingkat pendidikan pada master karyawan</li>
			<li>Penambahan grafik laba rugi bulanan</li>
			<li>Penambahan seting rest periode payroll, untuk komponen payroll yang harus di isi ulang setiap bulan</li>
		</ul>
		<ul> Perbaikan:
			<li>Perbaikan Filter ketika edit penjualan atau hapus penjualan apabila faktur sudah dilunasai maka data gak bisa dilunasi.</li>
			<li>Perbaikan beberapa bug</li>
		</ul>
		<b>Version 2.4.14 (26-Oktober-22)</b>		
		<ul> Perbaikan:
			<li>Judul form laporan uang muka</li>
			<li>Pencarian bahan baku pada transaksi proses produksi</li>
		</ul>
		<b>Version 2.4.13 (24-Oktober-22)</b>		
		<ul> Penambahan:
			<li>Tanggal lahir pada master karyawan</li>
		</ul>
		<ul> Perubahan:
			<li>Penyesuain sistem perhitungan unutk penentuan Lembur, dimana data yang diupload tidak boleh jadwal kosong</li>
		</ul>
		<ul> Perbaikan:
			<li>Pengambilan data lembur</li>
			<li>pencarian data penjualan pada menu retur penjualan</li>
		</ul>
		<b>Version 2.4.12 (19-Oktober-22)</b>		
		<ul> Penambahan:
			<li>Nilai pembulatan pada saat posting gaji</li>
			<li>Konfigurasi rekening pembulatan gaji pada menu konfigurasi sistem</li>
		</ul>
		<b>Version 2.4.11 (18-Juli-22)</b>		
		<ul> Perbaikan:
			<li>Perbaikan beberapa bug</li>
		</ul>
		<b>Version 2.4.10 (16-Juli-22)</b>		
		<ul> Penambahan:
			<li>Nilai persediaan pada posting stok opname</li>
		</ul>
		<ul> Perubahan:
			<li>Rumus perhitungan prosentase triwulan</li>
		</ul>
		<ul> Perbaikan:
			<li>Typo pada beberapa laporan</li>
		</ul>
		<b>Version 2.4.10 (21-Juni-21)</b>		
		<ul> Penambahan:
			<li>Penambahan jenis harga</li>
		</ul>
		<ul> Perbaikan:
			<li>Total Lap Realisasi Kas & Anggaran Kas</li>
		</ul>
		<b>Version 2.4.9 (25-Februari-21)</b>		
		<ul> Penambahan:
			<li>Penambahan laporan perbandingan neraca tahunan</li>
			<li>Penambahan laporan perbandingan laba rugi tahunan</li>
		</ul>
		<b>Version 2.4.8 (25-Februari-21)</b>		
		<ul> Perubahan:
			<li>Jth Tmp dirubah menjadi Jatuh Tempo pada laporan saldo htang pembelian</li>
			<li>Perubahan susunan kolom pada Lap Analisa Ratio Keuangan Triwulan Periode Terbaru ada di depan</li>
			<li>Perubahan susunan kolom pada Lap Analisa Ratio Keuangan Tahunan Periode Terbaru ada di depan</li>
		</ul>
		<b>Version 2.4.7 (03-Februari-21)</b>		
		<ul> Perubahan:
			<li>Tanda tangan pada laporan saldo stock</li>
			<li>Tanda tangan pada laporan hasil produksi karu</li>
			<li>Perubahan nama laporan pembelian jatuh tempo menjadi lap pembayaran jatuh tempo</li>
			<li>Perubahan kolom kasbank -> kas pada laporan total pelunasan piutang</li>
		</ul>
		<ul> Penambahan:
			<li>Pilihan penihilan ya / tidak untuk bisa melihat nilai saldo setelah dan sebelum posting akhir tahun</li>
		</ul>
		<b>Version 2.4.6 (29-Januari-21)</b>		
		<ul> Penambahan:
			<li>Kolom keterangan pada transaksi pelunasan hutang</li>
			<li>Kolom keterangan pada lap pelunasan hutang</li>
		</ul>
		<b>Version 2.4.5 (17-Januari-21)</b>		
		<ul> Perubahan:
			<li>Penambhaan titik-ririk pada ttd di laporan</li>
			<li>Perubahan keterangan hutang jatuh tempo pembelian di lap anggaran kas</li>
		</ul>
		<b>Version 2.4.4 (16-Januari-21)</b>		
		<ul> Perubahan:
			<li>Lap. Saldo Hutang displit antara hutang aset dan bahan baku</li>
		</ul>
		<b>Version 2.4.3 (14-Januari-21)</b>		
		<ul> Penambahan:
			<li>Laporan Realisasi Kas</li>
		</ul>
		<ul> Perubahan:
			<li>TTD yang kurang sesuai pada beberapa laporan</li>
			<li>Urutan laporan anggaran kas</li>
		</ul>
		<b>Version 2.4.2 (4-Januari-21)</b>		
		<ul> Pembenahan:
			<li>TTD direktur pada laporan anggaran kas</li>
			<li>Pembenahan bug pada stock opname</li>
		</ul>
		<ul> Perubahan:
			<li>Penyesuaian Lap Payroll </li>
			<li>Penyesuaian Posting Payroll </li>
		</ul>
		<b>Version 2.4.1 (26-Oktober-20)</b>		
		<ul> Penambahan:
			<li>Lap. PPh21 karyawan</li>
		</ul>
		<ul> Perubahan:
			<li>Penyesuaian Lap Payroll </li>
			<li>Penyesuaian Posting Payroll </li>
		</ul>
		<b>Version 2.4.0 (13-Oktober-20)</b>		
		<ul> Penambahan:
			<li>KPI Indikator</li>
			<li>KPI Setup Bobot</li>
			<li>Laporan KPI</li>
		</ul>
		<b>Version 2.3.2 (09-September-20)</b>		
		<ul> Penambahan:
			<li>Kontrak kerja karyawan</li>
			<li>Laporan kontrak jatuh tempo</li>
			<li>Cetak Kontrak Karyawan</li>
			<li>Upload Surat Surat</li>
		</ul>
		<b>Version 2.3.1 (24-Agustus-20)</b>		
		<ul> Penambahan:
			<li>Cetak daftar gaji pada laporan slip</li>
			<li>Summary perbagian pada cetak gaji</li>
		</ul>
		<ul> Perubahan:
			<li>Payroll yang nilainya 0 tidak ditampilkan</li>
		</ul>
		<b>Version 2.3.0 (10-Agustus-20)</b>		
		<ul> Penambahan:
			<li>Modul Pajak</li>
			<li>Laporan Hasil Produksi Karu</li>
			<li>Pembenahan Beberapa Bug</li>
		</ul>
		<b>Version 2.2.1 (20-Juli-20)</b>		
		<ul> Penambahan:
			<li>List Service App pada toolbar</li>
		</ul>
		<ul> Perbaikan:
			<li>Pembenahan jurnal pelunasan piutang penjualan aset</li>
			<li>Beberapa bug laporan</li>
		</ul>
		<b>Version 2.2.0 (20-Juli-20)</b>		
		<ul> Penambahan:
			<li>Posting Finger</li>
			<li>User info mesin</li>
		</ul>
		<b>Version 2.1.2 (30-Juni-20)</b>		
		<ul> Perbaikan:
			<li>Potongan terlambat dan pulang cepat</li>
			<li>Master dan setup nominal asuransi</li>
		</ul>
		<b>Version 2.1.1 (24-Juni-20)</b>		
		<ul> Perbaikan:
			<li>Jumlah hari kerja pada posting payroll</li>
			<li>Cetak slip payroll yang tidak bisa dipilih</li>
			<li>Pinjaman karyawan / kasbon yang tidak terjurnal</li>
		</ul>
		<b>Version 2.1.0 (27-Mei-20)</b>		
		<ul> Penambahan:
			<li>Menu Pengajuan Dana dan singkron dengan menu mutasi kas dan mutasi bank</li>
			<li>Kota dimaster cabang</li>
			<li>Detail retur pada laporan retur penjualan</li>
			<li>Cetak dotmatrix pada laporan detail pelunasan hutang</li>
			<li>Notif update aplikasi</li>
			<li>Bilyet giro yang tidak dicairkan</li>
		</ul>
		<ul>Perubahan
			<li>Menu Pencairan BG menjadi menu Proses BG Gantung</li>
		</ul>
		<hr>
		<b>Version 2.0.0 (08-Mei-20)</b>		
		<ul> Penambahan:
			<li>Modul HRD (Data Karyawan, Absensi, Payroll, THR)</li>
			<li>KOnversi Data Karyawan</li>
		</ul>
		<ul> Perbaikan:
			<li>Cetak Invoice retur yang sudah dihapus tidak akan muncul lagi</li>
		</ul>
		<hr>
		<b>Version 1.0.17 (02-Mar-20)</b>		
		<ul> Penambahan:
			<li>Grafik Penjualan</li>
		</ul>
		<hr>
		<b>Version 1.0.16 (20-Feb-20)</b>		
		<ul> Penambahan:
			<li>Laporan Analisa Ratio Keuangan Bulanan</li>
		</ul>
		<ul> Perubahan:
			<li>Laporan Penjualan detail (pdf) tampilan susunan total tagihan</li>
			<li>Laporan Pelunasan Piutang ditambah kolom faktur SJ</li>
		</ul>
		<ul> Perbaikan:
			<li>Laporan aruskas</li>
		</ul>
		<hr>
		<b>Version 1.0.15 (16-Feb-20)</b>		
		<ul> Penambahan:
			<li>Detail retur pada cetak invoice pdf (menu laporan penjualan)</li>
			<li>Penambahan Fitur Deposit Customer</li>
			<li>Mutasi kas pada cetakkan kas opname</li>
			<li>Saldo deposit pada cheklist accounting</li>
		</ul>
		<hr>
		<b>Version 1.0.14 (08-Feb-20)</b>		
		<ul> Penambahan:
			<li>Laporan Neraca Lajur</li>
		</ul>
		<hr>
		<b>Version 1.0.13 (06-Feb-20)</b>		
		<ul> Penambahan:
			<li>Menu periode transaksi untuk menutup dan membuka periode transaksi agar tidak sembarang tanggal bisa digunakan</li>
			<li>Fix beberapa bug</li>
		</ul>
		<hr>
		<b>Version 1.0.12 (04-Feb-20)</b>		
		<ul> Penambahan:
			<li>Menu Laporan Jatuh Tempo Pembelian</li>
			<li>Cetak dotmatrix pada menu pencairan BG</li>
		</ul>
		<hr>
		<b>Version 1.0.11 (02-Feb-20)</b>		
		<ul> Penambahan:
			<li>Transaksi Penyesuaian Stock</li>
		</ul>
		<hr>
		<b>Version 1.0.10 (01-Feb-20)</b>		
		<ul> Penambahan:
			<li>Laporan Saldo PO</li>
			<li>Anggaran Kas Harian</li>
			<li>Cetak Laporan Cheklist Accounting</li>
		</ul>
		<hr>
		<b>Version 1.0.9 (31-Jan-20)</b>		
		<ul> Penambahan:
			<li>Saldo hutang pembelian aset pada cheklist accounting</li>
			<li>Detail porsekot pada cheklist accounting</li>
		</ul>
		<hr>
		<b>Version 1.0.8 (30-Jan-20)</b>		
		<ul> Perbaikan:
			<li>Penambahan 'Rupiah' pada cetakkan KK, KM, BK, dan BM</li>
			<li>Penyesuaian Spesifikasi pada cetakkan PO</li>
		</ul>
		<hr>
		<b>Version 1.0.7 (20-Jan-20)</b>		
		<ul> Penambahan:
			<li>Penambahan Konversi Untuk BG</li>
			<li>Penambahan Lap Cheklist Accounting</li>
		</ul>
		<hr>
		<b>Version 1.0.6 (16-Jan-20)</b>		
		<ul> Perbaikan:
			<li>Pembenahan Laporan harga pokok produksi produk</li>
			<li>Pembenahan Laporan harga pokok produksi</li>		
			<li>Pembenahan Laporan harga pokok penjualan</li>
			<li>Pembenahan Laporan neraca</li>
			<li>Pembenahan Laporan laba rugi</li>
			<li>Fix beberapa bug</li>
		</ul>
		<hr>
		<b>Version 1.0.5 (12-Jan-20)</b>
		<ul> Penambahan:
			<li>Pelunasan hutang dengan bg / cek</li>
			<li>Penyesuaian pada laporan pelunasan hutang untuk penambahan BG / CEK</li>
			<li>Transaksi Pencairan BG / Cek</li>
			<li>Lap Saldo BG</li>
		</ul>
		<ul> Perubahan:
			<li>Struktur faktur transaksi PO hanya tersusun key,cabang,tahun,frekuaensi</li>	
		</ul>
		<ul> Perbaikan:
			<li>Fix laporan saldo piutang</li>	
			<li>Menu mutasi BG (perbaiki dulu struktur database untuk mutasi total)</li>	
			<li>Fix beberapa bug</li>
		</ul>
		<hr>
		<b>Version 1.0.4 (10-Jan-20)</b>
		<ul> Penambahan:
			<li>Menu Penjualan Aset</li>
			<li>Setup rekening piutang, laba , rugi penjualan aset dimenu golongan aset</li>
			<li>Setup jenis dimenu kode transaksi</li>
			<li>Cetak LPB (Lembar Penerimaan Barang) pada menu Lap. Purchase Order</li>
			<li>Penambahan kontak personal pada master supplier</li>
		</ul>
		<ul> Perbaikan:
			<li> Untuk Pembelian Aset + inventaris juga ditampilkan di laporan hutang pembelian</li>
			<li> Fix bug pada pelunasan piutang (retur penjualan)</li>
			<li> Fix bug delete pada beberapa transaksi</li>
			<li> Fix bug pada cetak PO</li>			
		</ul>
		<hr>
		<b>Version 1.0.3 (09-Jan-20)</b>
		<ul> Penambahan:
			<li> Penambahan menu laporan hutang pembelian (untuk melihat faktur pembelian yang belum lunas beserta jatuh tempo pembayarannya)</li>
		</ul>
		<ul> Perbaikan:
			<li> Pembenahan lap pembelian untuk pembulatan harga</li>
			<li> Pembenahan Lap HP Produksi dan HP Penjualan dengan format portrait dan bergaris</li>
			<li> Bank keluar dirubah dari yang membayar ke yang menerima</li>			
		</ul>
	</div>
	</from>
</div>
<script type="text/javascript">
	if(typeof bos === "undefined") window.location.href = "<?=base_url()?>";

	bos.infoaplikasi.initcomp		= function(){
		bjs.initenter(this.obj.find("form")) ;
		this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
	}

	$(function(){
		bos.infoaplikasi.initcomp() ;
	})
</script>
