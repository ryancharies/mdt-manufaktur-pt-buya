<table class="osxtable form">
    <tr>
        <td width='150px'><label for="kogapypinjkaryawan">Komp. Gaji Pinjaman Karyawan</label> </td>
        <td width='5px'>:</td>
        <td colspan = '3'>
            <select name="kogapypinjkaryawan" id="kogapypinjkaryawan" class="form-control select koga" style="width:100%"
                    data-placeholder="Komponen Gaji Pinjaman Karyawan">
                <option value="<?= $kogapypinjkaryawan ?>" selected='selected'><?= $kogapypinjkaryawan ?> - <?= $ketkogapypinjkaryawan ?></option>
            </select>
        </td>
    </tr>
    <tr>
        <td width='150px'><label for="kogapythr">Komp. Gaji THR</label> </td>
        <td width='5px'>:</td>
        <td colspan = '3'>
            <select name="kogapythr" id="kogapythr" class="form-control select koga" style="width:100%"
                    data-placeholder="Komponen Gaji THR">
                <option value="<?= $kogapythr ?>" selected='selected'><?= $kogapythr ?> - <?= $ketkogapythr ?></option>
            </select>
        </td>
    </tr>
    <tr>
        <td width='150px'><label for="kogapythr">Komp. Gaji KPI</label> </td>
        <td width='5px'>:</td>
        <td colspan = '3'>
            <select name="kogapykpi" id="kogapykpi" class="form-control select koga" style="width:100%"
                    data-placeholder="Komponen Gaji KPI">
                <option value="<?= $kogapykpi ?>" selected='selected'><?= $kogapykpi ?> - <?= $ketkogapykpi ?></option>
            </select>
        </td>
    </tr>
    <tr>
        <td width='150px'><label for="rekpypembulatanposting">Rek. Pemb Posting</label> </td>
        <td width='5px'>:</td>
        <td>
            <select name="rekpypembulatanposting" id="rekpypembulatanposting" class="form-control select rekakt" style="width:100%"
                    data-placeholder="Rek. Pembulatan Posting Gaji">
                <option value="<?= $rekpypembulatanposting ?>" selected='selected'><?= $rekpypembulatanposting ?> - <?= $ketrekpypembulatanposting ?></option>
            </select>
        </td>
    </tr>
</table>