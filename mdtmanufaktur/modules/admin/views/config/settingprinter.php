<div class="header active">
	<table class="header-table">
		<tr>
			<td class="icon" ><i class="fa fa-building"></i></td>
			<td class="title">
				Setting Printer Dot Matrix
			</td>
			<td class="button">
				<table class="header-button" align="right">
					<tr>
						<td>
							<div class="btn-circle btn-close transition" onclick="bos.settingprinter.close()">
								<img src="./uploads/titlebar/close.png">
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div><!-- end header -->
<div class="body">
	<form>
	<div class="bodyfix scrollme" style="height:100%">
		<iframe src="http://127.0.0.1:2700" style="overflow:hidden;height:100%;width:100%"></iframe>
	</div>
	</from>
</div>
<script type="text/javascript">
	if(typeof bos === "undefined") window.location.href = "<?=base_url()?>";

	bos.settingprinter.initcomp		= function(){
		bjs.initenter(this.obj.find("form")) ;
		this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
	}

	$(function(){
		bos.settingprinter.initcomp() ;
	})
</script>
