<table class="osxtable form">
    <tr>
        <td width='150px'><label for="kodeabsmasuk">Masuk</label> </td>
        <td width='5px'>:</td>
        <td colspan = '3'>
            <select name="kodeabsmasuk" id="kodeabsmasuk" class="form-control select koabs" style="width:100%"
                    data-placeholder="Kode Abensi Masuk">
                <option value="<?= $kodeabsmasuk ?>" selected='selected'><?= $kodeabsmasuk ?> - <?= $ketkodeabsmasuk ?></option>
            </select>
        </td>
    </tr>
    <tr>
        <td width='150px'><label for="kodeabsabsent">Absent</label> </td>
        <td width='5px'>:</td>
        <td colspan = '3'>
            <select name="kodeabsabsent" id="kodeabsabsent" class="form-control select koabs" style="width:100%"
                    data-placeholder="Kode Abensi Absent">
                <option value="<?= $kodeabsabsent ?>" selected='selected'><?= $kodeabsabsent ?> - <?= $ketkodeabsabsent ?></option>
            </select>
        </td>
    </tr>
</table>