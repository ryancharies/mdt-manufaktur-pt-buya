<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tkaryawan">
                        <button class="btn btn-tab tpel active" href="#tkaryawan_1" data-toggle="tab" >Daftar Karyawan</button>
                        <button class="btn btn-tab tpel" href="#tkaryawan_2" data-toggle="tab">Karyawan</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstkaryawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tkaryawan_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tkaryawan_2">
                    <table class="osxtable form">
                        <tr>
                            <td width="14%"><label for="kode">Kode</label> </td>
                            <td width="1%">:</td>
                            <td >
                                <input readonly type="text" id="kode" name="kode" class="form-control" placeholder="Kode" required>
                            </td>
                        
                            <td width="14%"><label for="nama">Nama</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama" required>
                            </td>
                        </tr>
                        <tr>
                            <td width="14%"><label for="ktp">No KTP</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input type="text" id="ktp" name="ktp" class="form-control" placeholder="No KTP" required>
                            </td>
                        
                            <td width="14%"><label for="notelepon">No Telp</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input type="text" id="notelepon" name="notelepon" class="form-control" placeholder="notelepon" required>
                            </td>
                        </tr>
                        <tr>
                            <td width="14%"><label for="alamat">Alamat</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input type="text" id="alamat" name="alamat" class="form-control" placeholder="alamat" required>
                            </td>
                        
                            <td width="14%"><label for="tgl">Tgl Masuk</label> </td>
                            <td width="1%">:</td>
                            <td >
                                <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" placeholder="Tanggal Masuk" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                            </td>
                        </tr>
                        <tr>
                            <td width="14%"><label for="tgllahir">Tgl Lahir</label> </td>
                            <td width="1%">:</td>
                            <td >
                                <input style="width:80px" type="text" class="form-control datetr" id="tgllahir" name="tgllahir" placeholder="Tanggal Lahir" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                            </td>
                            <td width="14%"><label for="rekening">Rekening</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input type="text" id="rekening" name="rekening" class="form-control" placeholder="Rekening" required>
                            </td>
                        
                            
                        </tr>
                        <tr>
                            <td><label for="bank">Bank</label> </td>
                            <td>:</td>
                            <td>
                                <select name="bank" id="bank" class="form-control scons" style="width:100%"
                                    data-placeholder="Bank" data-sf="load_bank"></select>
                            </td>
                            <td width="14%"><label for="anrekening">An Rekening</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input type="text" id="anrekening" name="anrekening" class="form-control" placeholder="Atas Nama Rekening" required>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="pendidikan_tingkat">Tingkat Pendidikan</label> </td>
                            <td>:</td>
                            <td>
                                <select name="pendidikan_tingkat" id="pendidikan_tingkat" class="form-control scons" style="width:100%"
                                    data-placeholder="Tingkat Pendidikan" data-sf="load_pendidikan_tingkat"></select>
                            </td>
                            <td width="14%"></td>
                            <td width="1%"></td>
                            <td>

                            </td>
                        </tr>
                        <tr>
                            <td colspan = "6">
                                Foto Karyawan <span id="idlfotokaryawan"></span>
                                <table height = "200px" width = "100%">
                                    <tr><td height = '20px'><input type="file" id="fotokaryawan" accept="image/*" class="fupload"></td></tr>
                                    <tr><td height="180px">
                                        <div class="row" id="fotobrowse" style="height:200px;width:100%;overflow: scroll;">
                                        </div>
                                    </td></tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstkaryawan.grid1_data 	 = null ;
    bos.mstkaryawan.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstkaryawan.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstkaryawan.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,frozen:true,style:"text-align:center;"},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false,frozen:true},
                { field: 'ktp', caption: 'No KTP', size: '100px', sortable: false,frozen:true},
                { field: 'tgllahir', caption: 'Tgl Lahir', size: '80px', sortable: false,style:"text-align:center;"},
                { field: 'alamat', caption: 'Alamat', size: '200px', sortable: false},
                { field: 'telepon', caption: 'Telepon', size: '100px', sortable: false},
                { field: 'tgl', caption: 'Tgl Masuk', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'rekening', caption: 'Rekening', size: '100px', sortable: false},
                { field: 'anrekening', caption: 'AN Rekening', size: '200px', sortable: false},
                { field: 'bank', caption: 'Bank', size: '130px', sortable: false},
                { field: 'pendidikan_tingkat', caption: 'Tingkat Pendidikan', size: '100px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstkaryawan.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstkaryawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstkaryawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstkaryawan.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstkaryawan.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.mstkaryawan.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.mstkaryawan.cmddelete		= function(id){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + id);
        }
    }

    bos.mstkaryawan.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#nama").val("") ;
        this.obj.find("#notelepon").val("") ;
        this.obj.find("#ktp").val("") ;
        this.obj.find("#alamat").val("") ;
        this.obj.find("#rekening").val("") ;
        this.obj.find("#anrekening").val("") ;
        this.obj.find("#tgllahir").val("") ;
        this.obj.find("#tgl").val("") ;
        this.obj.find("#bank").sval("") ;
        this.obj.find("#pendidikan_tingkat").sval("") ;
        
        bjs.ajax(this.url + "/init") ;
        bjs.ajax(this.url + '/getkode') ;
        this.obj.find("#fotobrowse").html("");
    }

    bos.mstkaryawan.settab 		= function(n){
        this.obj.find("#tkaryawan button:eq("+n+")").tab("show") ;
    }

    bos.mstkaryawan.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstkaryawan.grid1_render() ;
            bos.mstkaryawan.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#kode").focus() ;
        }
    }

    bos.mstkaryawan.initcomp	= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons",
        }) ;
        bjs.ajax(this.url + '/getkode') ;
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;        
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.mstkaryawan.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstkaryawan.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstkaryawan.grid1_destroy() ;
        }) ;
    }

    bos.mstkaryawan.deletefoto = function(file){
        var cfrm = "Apakah foto dihapus?? jika foto di hapus, maka tidak dapat dikembalikan";
        if(confirm(cfrm)){     
            bjs.ajax(bos.mstkaryawan.url + '/deletefoto',"file="+file) ;   
        }        
    }

    bos.mstkaryawan.objs = bos.mstkaryawan.obj.find("#cmdsave") ;
    bos.mstkaryawan.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){

                //mengambil data foto
                var cImg = "" ;
                if(typeof bos.mstkaryawan.obj.find("#ckfotoutama") == "object"){
                    var ck = bos.mstkaryawan.obj.find("[id=ckfotoutama]") ;
                    nc = "0" ;
                    if(typeof ck.length == "undefined"){
                        if(ck.checked) nc = "1" ;
                            cImg = ck.value + "`" + nc ;
                    }else{      
                        for(n=0;n<ck.length;n++){
                            nc = "0" ;
                            if(ck[n].checked) nc = "1" ;
                            cImg += ck[n].value + "`" + nc + "~" ;
                        }
                    }
                }
                cImg = encodeURIComponent(cImg);

                bjs.ajax( bos.mstkaryawan.url + '/saving', bjs.getdataform(this)+"&imgupload="+cImg, bos.mstkaryawan.objs) ;
            }
        });

        this.obj.find(".fupload").on("change", function(e){
			bos.mstkaryawan.uname	= $(this).attr("id") ;
			e.preventDefault() ;

            bos.mstkaryawan.fal    = e.target.files ;
            bos.mstkaryawan.gfal   = new FormData() ;
            $.each(bos.mstkaryawan.fal, function(key,val){
              bos.mstkaryawan.gfal.append(key,val) ;
            }) ;

            bos.mstkaryawan.obj.find("#idl" + bos.mstkaryawan.uname).html("<i class='fa fa-spinner fa-pulse'></i>");
            bos.mstkaryawan.obj.find("#id" + bos.mstkaryawan.uname).html("") ;

            bjs.ajaxfile(bos.mstkaryawan.base_url + "/saving_image/" + bos.mstkaryawan.uname , bos.mstkaryawan.gfal, this) ;
		}) ;
    }
    

    $(function(){
        bos.mstkaryawan.initcomp() ;
        bos.mstkaryawan.initcallback() ;
        bos.mstkaryawan.initfunc() ;
    }) ;
</script>
