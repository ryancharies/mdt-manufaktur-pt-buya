<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpel">
                        <button class="btn btn-tab tpel active" href="#tpel_1" data-toggle="tab" >Daftar Aset & Inventaris</button>
                        <button class="btn btn-tab tpel" href="#tpel_2" data-toggle="tab">Aset & Inventaris</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstaktivainventaris.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpel_1" style="padding-top:5px;">
                    <div class="row">
                        <div class="col-md-1">
                            <div class="form-group">
                                <label>Tgl Awal</label>
                                <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label>Tgl Akhir</label>
                                <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Cabang</label>
                                <div class="input-group">
                                    <select name="skd_cabang" id="skd_cabang" class="form-control s2" style="width:100%"
                                                            data-placeholder="Pilih Cabang" data-sf="load_cabang"></select>
                                    <span class="input-group-addon">
                                        <span id="cmdcabang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                                    </span>
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group full-width">
                                <label>&nbsp;</label>
                                <div class="input-group full-width">
                                        <button type="button" class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row" style="height: calc(100% - 50px);"> -->
                        <div id="grid1" class="full-height" style="height: calc(100% - 50px);"></div>
                    <!-- </div> -->
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpel_2">
                    <table width='100%'>
                        <tr>
                            <td>
                                <table class="osxtable form">
                                    <tr>
                                        <td width="150px"><label for="kode">Kode</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input readonly type="text" maxlength="8" id="kode" name="kode" class="form-control" placeholder="kode" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="cabang">Cabang</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select name="cabang" id="cabang" class="form-control scons" style="width:100%"
                                                    data-placeholder="Cabang / Kantor" data-sf="load_cabang" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="faktur">faktur</label> </td>
                                        <td>:</td>
                                        <td >
                                            <input readonly type="text" maxlength="8" id="faktur" name="faktur" class="form-control" placeholder="faktur" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="keterangan">Keterangan</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="golaset">Gol. Aset</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select name="golaset" id="golaset" class="form-control scons" style="width:100%"
                                                    data-placeholder="Golongan Aset" data-sf="load_golaset" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="kelaset">Kelompok Aset</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select name="kelaset" id="kelaset" class="form-control scons" style="width:100%"
                                                    data-placeholder="Kelompok Aset" data-sf="load_kelaset" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="tglperolehan">Tgl. Perolehan</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input style="width:80px" type="text" class="form-control datetr" id="tglperolehan" name="tglperolehan" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td width="150px"><label for="lama">Lama (Tahun)</label> </td>
                                        <td width="5px">:</td>
                                        <td>
                                            <input  style="width:50px" maxlength="5" type="text" name="lama" id="lama" class="form-control number" value="0">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="hp">Harga Perolehan</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input  style="width:200px" maxlength="20" type="text" name="hp" id="hp" class="form-control number" value="0">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="unit">Unit</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input  style="width:50px" maxlength="7" type="text" name="unit" id="unit" class="form-control number" value="0" required>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="jenis">Jenis Peny</label> </td>
                                        <td>:</td>
                                        <td>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" class="jenis" value="1" checked>
                                                    Garis Lurus
                                                </label>
                                                &nbsp;&nbsp;
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" class="jenis" value="2">
                                                    Saldo Menurun
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="tarifpeny">Tarif Peny (%)</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input  style="width:50px" maxlength="7" type="text" name="tarifpeny" id="tarifpeny" class="form-control number" value="0">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="residu">Nilai Residu</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input  style="width:200px" maxlength="20" type="text" name="residu" id="residu" class="form-control number" value="0">

                                        </td>
                                    </tr>
                                </table>

                            </td>
                            <td width='50%' valign='top'>
                                <table width='100%'>
                                    <tr>
                                        <td width='100%'>
                                            <table class="osxtable form">                                    
                                                <tr>
                                                    <td width = '300px' ><label for="vendor">Vendor / Supplier</label> </td>
                                                    <td><label for="hutang">Hutang</label> </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td><select name="vendor" id="vendor" class="form-control scons" style="width:100%"
                                                        data-placeholder="Vendor / Supplier" data-sf="load_supplier"></select></td>
                                                    <td><input maxlength="20" type="text" name="hutang" id="hutang" class="form-control number" value="0"></td>
                                                    <td><button type="button" class="btn btn-primary pull-right" id="cmdok">OK</button></td>
                                                </tr>
                                                
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height = '300px'>
                                            <div id="grid2" class="full-height"></div>
                                        </td>
                                    </tr>
                                </table>
                                
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstaktivainventaris.grid1_data 	 = null ;
    bos.mstaktivainventaris.grid1_loaddata= function(){
        mdl_cabang.vargr.selection = bos.mstaktivainventaris.obj.find('#skd_cabang').val();

		this.grid1_data 		= {
			"tglawal"	   : this.obj.find("#tglawal").val(),
            "tglakhir"	   : this.obj.find("#tglakhir").val(),
			'skd_cabang':JSON.stringify(mdl_cabang.vargr.selection)
		} ;
    }

    bos.mstaktivainventaris.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstaktivainventaris.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '120px', sortable: false,frozen:true},
				{ field: 'faktur', caption: 'Faktur', size: '120px', sortable: false,frozen:true},
                { field: 'keterangan', caption: 'Keterangan', size: '150px', sortable: false,frozen:true},
                { field: 'cabang', caption: 'Cabang', size: '80px', sortable: false,frozen:true},
				{ field: 'vendors', caption: 'Vendor', size: '80px', sortable: false,frozen:true},
                { field: 'golongan', caption: 'Golongan', size: '100px', sortable: false,frozen:true},
                { field: 'kelompok', caption: 'Kelompok', size: '100px', sortable: false,frozen:true},
                { field: 'tglperolehan', caption: 'Tgl Perolehan', size: '80px', sortable: false,frozen:true},
                { field: 'lama', caption: 'Lama', size: '80px', sortable: false},
                { field: 'hargaperolehan', caption: 'Harga Perolehan', size: '100px', sortable: false},
                { field: 'unit', caption: 'Unit', size: '80px', sortable: false},
                { field: 'jenispenyusutan', caption: 'Jenis Penyusutan', size: '100px', sortable: false},
                { field: 'tarifpenyusutan', caption: 'Tarif Penyusutan', size: '100px', sortable: false},
                { field: 'residu', caption: 'Nilai Residu', size: '100px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstaktivainventaris.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstaktivainventaris.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstaktivainventaris.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstaktivainventaris.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstaktivainventaris.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail pembelian
    bos.mstaktivainventaris.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
            multiSelect : false,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'vendor', caption: 'Vendor / Supplier', size: '100px', sortable: false },
                { field: 'namavendor', caption: 'Nama Vendor / Supplier', size: '200px', sortable: false },
                { field: 'hutang',render:'float:2', caption: 'Hutang', size: '100px', editable:{type:'int'},sortable: false, style:'text-align:right'},
                { field: 'act', caption: ' ', size: '80px', sortable: false }
            ],
            summary: [
                { recid: "ZZZZ", vendor: '', namavendor: 'TOTAL', hutang: 0 }
            ],
            onChange: function(event){
                event.onComplete = function () {
                        w2ui[event.target].save();
                        bos.mstaktivainventaris.grid2_hitungtotal();
                }
            }
        });


    }

    bos.mstaktivainventaris.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.mstaktivainventaris.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.mstaktivainventaris.grid2_append    = function(vendor,hutang){
        var datagrid  = w2ui[this.id + '_grid2'].records;
        var lnew      = true;
        var nQty      = 1;
        var recid     = "";
        if(no <= datagrid.length){
            recid = no;
            w2ui[this.id + '_grid2'].set(recid,{stock: kode, namastock: keterangan, qty: qty, satuan:satuan});
        }else{
            recid = no;
            var Hapus = "<button type='button' onclick = 'bos.mstaktivainventaris.grid2_deleterow("+recid+")' class='btn btn-danger btn-grid'>Delete</button>";
            w2ui[this.id + '_grid2'].add([
                { recid:recid,
                 no: no,
                 stock: kode,
                 namastock: keterangan,
                 qty: qty,
                 satuan:satuan,
                 cmddelete:Hapus}
            ]) ;
        }
        bos.mstaktivainventaris.initdetail();
        bos.mstaktivainventaris.obj.find("#vendor").focus() ;
    }

    bos.mstaktivainventaris.grid2_deleterow = function(recid){
        if(confirm("Item di hapus dari detail vendor???"+recid)){
            w2ui[this.id + '_grid2'].select(recid);
            w2ui[this.id + '_grid2'].delete(true);
            bos.mstaktivainventaris.grid2_hitungtotal();
        }
    }

    bos.mstaktivainventaris.grid2_hitungtotal 			= function(){
        bos.mstaktivainventaris.gr2 = w2ui[this.id + '_grid2'].records;
        bos.mstaktivainventaris.tothtg = 0 ;

        $.each(bos.mstaktivainventaris.gr2, function(i, v){
            bos.mstaktivainventaris.tothtg += v.hutang;
        });

        w2ui[this.id + '_grid2'].set("ZZZZ",{hutang:bos.mstaktivainventaris.tothtg});
    }

    bos.mstaktivainventaris.cmdedit		= function(id){
        bos.mstaktivainventaris.act = 1;
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.mstaktivainventaris.cmddelete		= function(id,fkt){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + id + '&faktur=' + fkt);
        }
    }

    bos.mstaktivainventaris.getfaktur = function(){
        if(bos.mstaktivainventaris.act == 0){
            bos.mstaktivainventaris.cabang = bos.mstaktivainventaris.obj.find("#cabang").val();
            bos.mstaktivainventaris.tgl = bos.mstaktivainventaris.obj.find("#tglperolehan").val();
            bjs.ajax(this.url + '/getfaktur','cabang='+bos.mstaktivainventaris.cabang+"&tgl="+bos.mstaktivainventaris.tgl,'',function(hasil){
                bos.mstaktivainventaris.obj.find("#faktur").val(hasil);
            }) ;
        }
        
    }

    bos.mstaktivainventaris.init				= function(){
        this.obj.find("#kode").val("") ;
		this.obj.find("#faktur").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#cabang").sval("") ;
        this.obj.find("#cabang").removeAttr('readonly');

        this.obj.find("#golaset").sval("") ;
        this.obj.find("#kelaset").sval("") ;
		this.obj.find("#aset").sval("") ;
        this.obj.find("#tglperolehan").val("<?=date("d-m-Y")?>");
        this.obj.find("#lama").val("0") ;
        this.obj.find("#hp").val("0") ;
        this.obj.find("#unit").val("1") ;
        //this.obj.find("#jenis").val("1") ;
        this.obj.find("#tarifpeny").val("0") ;
        this.obj.find("#residu").val("0") ;
        bjs.ajax(this.url + "/init") ;
        bos.mstaktivainventaris.getkode();
        bos.mstaktivainventaris.getfaktur();
        bos.mstaktivainventaris.setopt("jenis","1");
        bos.mstaktivainventaris.initdetail();

        bos.mstaktivainventaris.act = 0 ;

        w2ui[this.id + '_grid2'].clear();
        w2ui[this.id + '_grid2'].add({recid: "ZZZZ", vendor: '', namavendor: 'TOTAL', hutang: 0,w2ui:{summary:true}});

    }

    bos.mstaktivainventaris.getkode 			= function(){
        bjs.ajax(this.url + '/getkode') ;
    }

    bos.mstaktivainventaris.initdetail 			= function(){
        this.obj.find("#vendor").sval("") ;
        this.obj.find("#hutang").val("") ;
    }

    bos.mstaktivainventaris.settab 		= function(n){
        this.obj.find("#tpel button:eq("+n+")").tab("show") ;
    }

    bos.mstaktivainventaris.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstaktivainventaris.grid1_render() ;
            bos.mstaktivainventaris.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            bos.mstaktivainventaris.grid2_reload() ;

            this.obj.find("#kode").focus() ;
        }
    }

    bos.mstaktivainventaris.initcomp	= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons",
            clear : true
        }) ;

        bjs.initselect({
            class : "#" + this.id + " .s2",
            multi : true
        }) ;

        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;
        bos.mstaktivainventaris.init();
    }

    bos.mstaktivainventaris.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstaktivainventaris.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstaktivainventaris.grid1_destroy() ;
            bos.mstaktivainventaris.grid2_destroy() ;
        }) ;
    }
    bos.mstaktivainventaris.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }
    bos.mstaktivainventaris.objs = bos.mstaktivainventaris.obj.find("#cmdsave") ;
    bos.mstaktivainventaris.initfunc 		= function(){
        this.init() ;
        

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bos.mstaktivainventaris.hp = string_2n(bos.mstaktivainventaris.obj.find("#hp").val());

                if(bos.mstaktivainventaris.tothtg == bos.mstaktivainventaris.hp){
                    bos.mstaktivainventaris.gr2 =  w2ui['bos-form-mstaktivainventaris_grid2'].records;
                    bos.mstaktivainventaris.gr2 = JSON.stringify(bos.mstaktivainventaris.gr2);
                    bjs.ajax( bos.mstaktivainventaris.url + '/saving', bjs.getdataform(this)+"&grid2="+bos.mstaktivainventaris.gr2 , bos.mstaktivainventaris.objs) ;
                }else{
                    alert("Nilai harga perolehan dan total hutang tidak sama!!");
                }                
            }
        });

        this.obj.find("#cmdrefresh").on("click", function(){ 
			bos.mstaktivainventaris.grid1_reloaddata() ; 
		}) ;

        this.obj.find("#hutang").on("blur", function(e){
            bos.mstaktivainventaris.htg = string_2n(bos.mstaktivainventaris.obj.find("#hutang").val());
            bos.mstaktivainventaris.obj.find("#hutang").val($.number(bos.mstaktivainventaris.htg,2));
        });

        this.obj.find('#kelaset').on("select2:selecting", function(e) { 
            bos.mstaktivainventaris.kelaset = bos.mstaktivainventaris.obj.find("#kelaset").val();
            bjs.ajax( bos.mstaktivainventaris.url + '/getkelaset',"kode="+e.params.args.data.id,'',function(data){
                data = JSON.parse(data);
                bos.mstaktivainventaris.obj.find("#lama").val(data['lama']);

            }) ;
        });

        this.obj.find("#cmdok").on("click", function(e){
            bos.mstaktivainventaris.vendor = bos.mstaktivainventaris.obj.find("#vendor").val();
            bos.mstaktivainventaris.namavendor = bos.mstaktivainventaris.obj.find("#vendor").text();
            bos.mstaktivainventaris.hutang = string_2n(bos.mstaktivainventaris.obj.find("#hutang").val());
            if(bos.mstaktivainventaris.vendor !== "" && bos.mstaktivainventaris.hutang > 0){
                if( w2ui['bos-form-mstaktivainventaris_grid2'].get(bos.mstaktivainventaris.vendor) !== null ){
                    w2ui['bos-form-mstaktivainventaris_grid2'].set(bos.mstaktivainventaris.vendor,{
                        hutang: bos.mstaktivainventaris.hutang
                    });
                }else{
                    bos.mstaktivainventaris._act = "<button type='button' onclick = 'bos.mstaktivainventaris.grid2_deleterow("+bos.mstaktivainventaris.vendor+")' class='btn btn-danger btn-grid'>Delete</button>";
                    w2ui['bos-form-mstaktivainventaris_grid2'].add([{ 
                        recid: bos.mstaktivainventaris.vendor,
                        vendor: bos.mstaktivainventaris.vendor,
                        namavendor: bos.mstaktivainventaris.namavendor,
                        hutang: bos.mstaktivainventaris.hutang,
                        act: bos.mstaktivainventaris._act,
                    }]) ;
                }
                bos.mstaktivainventaris.initdetail();
                bos.mstaktivainventaris.grid2_hitungtotal();

            }else{
                alert("Data hutang vendor tidak valid!!");
            }
        }) ;

        this.obj.find('#cabang').on('change', function(){
            bos.mstaktivainventaris.getfaktur();
        });

        this.obj.find('#cmdcabang').on('click', function(){
            mdl_cabang.vargr.selection = bos.mstaktivainventaris.obj.find('#skd_cabang').val();
            mdl_cabang.open(function(r){
                r = JSON.parse(r);
                bos.mstaktivainventaris.cabang = [];
                $.each(r, function(i, v){
                    bos.mstaktivainventaris.cabang.push({'id':v.kode,'text':v.keterangan})                   
                });
                bos.mstaktivainventaris.obj.find('#skd_cabang').sval(bos.mstaktivainventaris.cabang);
            });
        
            
        });
    }
    $(function(){
        bos.mstaktivainventaris.initcomp() ;
        bos.mstaktivainventaris.initcallback() ;
        bos.mstaktivainventaris.initfunc() ;
    }) ;
    

</script>
