<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tdt2">
                        <button class="btn btn-tab tdt2 active" href="#tdt2_1" data-toggle="tab" >Daftar Dati 2 (Kota / Kabupaten)</button>
                        <button class="btn btn-tab tdt2" href="#tdt2_2" data-toggle="tab">Dati 2 (Kota / Kabupaten)</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstdati2.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tdt2_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tdt2_2">
                    <table class="osxtable form">
                        <tr>
                            <td width="14%"><label for="kode">Kode</label> </td>
                            <td width="1%">:</td>
                            <td >
                                <input type="text" id="kode" name="kode" class="form-control" placeholder="kode" readonly required>
                            </td>
                        </tr>
                        <tr>
                            <td width="14%"><label for="keterangan">Nama Dati 2 (Kota / Kab)</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Nama Dati 2 (Kota / Kab)" required>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="dati_1">Dati 1 (Provinsi)</label> </td>
                            <td>:</td>
                            <td>
                                <select name="dati_1" id="dati_1" class="form-control scons" style="width:100%"
                                    data-placeholder="Dati 1 (Provinsi)" data-sf="load_dati_1" required></select>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstdati2.grid1_data 	 = null ;
    bos.mstdati2.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstdati2.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstdati2.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false},
                { field: 'keterangan', caption: 'Nama Dati 2 (Kota/Kab)', size: '150px', sortable: false},
                { field: 'dati_1', caption: 'Nama Dati 1 (Provinsi)', size: '150px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstdati2.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstdati2.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstdati2.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstdati2.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstdati2.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.mstdati2.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.mstdati2.cmddelete		= function(id){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + id);
        }
    }

    bos.mstdati2.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#dati_1").sval("") ;

        bos.mstdati2.getkode();
        bjs.ajax(this.url + "/init") ;
    }

    bos.mstdati2.settab 		= function(n){
        this.obj.find("#tdt2 button:eq("+n+")").tab("show") ;
    }

    bos.mstdati2.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstdati2.grid1_render() ;
            bos.mstdati2.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#kode").focus() ;
        }
    }

    bos.mstdati2.getkode = function(){
        bjs.ajax(bos.mstdati2.url + '/getkode','','',function(fkt){
            bos.mstdati2.obj.find("#kode").val(fkt) ;
        }) ;
    }

    bos.mstdati2.initcomp	= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons"
        }) ;
        bos.mstdati2.getkode();
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.tdt2') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.mstdati2.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstdati2.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstdati2.grid1_destroy() ;
        }) ;
    }

    bos.mstdati2.objs = bos.mstdati2.obj.find("#cmdsave") ;
    bos.mstdati2.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bjs.ajax( bos.mstdati2.url + '/saving', bjs.getdataform(this) , bos.mstdati2.objs) ;
            }
        });
    }

    $(function(){
        bos.mstdati2.initcomp() ;
        bos.mstdati2.initcallback() ;
        bos.mstdati2.initfunc() ;
    }) ;
</script>
