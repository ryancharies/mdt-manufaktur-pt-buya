<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">User Info Finger (Mesin)</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstuserinfofinger.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <table width="100%">
                <tr>
                    <td width="40%">
                        <table width="100%">
                            <tr>
                                <td><b>:: Mesin Absensi ::</b></td>
                            </tr>
                            <tr>
                                <td height="400px">
                                    <div id="grid2" class="full-height"></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="osxtable form" height="25px" width="100%">
                                    <table>
                                        <tr>
                                            <td width="85px">
                                                <button type="button" class="btn btn-primary pull-right" id="cmdproses">Proses</button>
                                            </td>
                                            <td>*) Klik proses untuk mengambil data di mesin dan disimpan di sistem</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width = "5px"></td>
                    <td>
                        <table width="100%">
                            <tr>
                                <td class="osxtable form" height="25px" width="100%">
                                    <table>
                                        <tr>
                                            <td width="120px">
                                                <select name="mesin" id="mesin" class="form-control select" style="width:100%"
                                                    data-placeholder="Mesin"></select>
                                            </td>
                                            <td width="85px">
                                                <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="400px">
                                    <div id="grid1" class="full-height"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>            
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    bos.mstuserinfofinger.grid1_data    = null ;
    bos.mstuserinfofinger.grid1_loaddata= function(){
        var mesin = bos.mstuserinfofinger.obj.find("#mesin").val();
        this.grid1_data 		= {'mesin':mesin} ;
    }

    bos.mstuserinfofinger.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            url 	: bos.mstuserinfofinger.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers  : true
            },
            multiSearch		: false,
            columns: [                
                { field: 'pin', caption: 'PIN', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'nama', caption: 'Nama', size: '150px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'password', caption: 'Password', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'groupfp', caption: 'Group FP', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'privilege', caption: 'Privilege', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'card', caption: 'Card', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'pin2', caption: 'PIN 2', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'tz1', caption: 'TZ 1', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'tz2', caption: 'TZ 2', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'tz3', caption: 'TZ 3', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'mesin', caption: 'Mesin', size: '150px', sortable: false , style:"text-align:left"},
            ]
        });
    }

    bos.mstuserinfofinger.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstuserinfofinger.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstuserinfofinger.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstuserinfofinger.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstuserinfofinger.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    /*grid mesin absensi*/
    bos.mstuserinfofinger.grid2_data    = null ;
    bos.mstuserinfofinger.grid2_loaddata= function(){
        this.grid2_data 		= {} ;
    }

    bos.mstuserinfofinger.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            url 	: bos.mstuserinfofinger.base_url + "/loadgrid2",
            postData: this.grid2_data ,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers  : true,
                selectColumn: true,
                multiSelect:true
            },
            multiSearch		: false,
            columns: [                
                { field: 'kode', caption: 'Kode', size: '50px', sortable: false , style:"text-align:center"},
                { field: 'keterangan', caption: 'Keterangan', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'status', caption: 'Status', size: '100px', sortable: false , style:"text-align:center"}
            ],
            onSelect: function(event) {
                event.onComplete = function () {
                    this.set(event.recid,{status:"Akan diproses"});
                   // w2ui[event.target].save();
                }
                //this.set(event.recid,{cetakslip:"Cetak Slip"});
            },
            onUnselect: function(event) {
                event.onComplete = function () {
                    this.set(event.recid,{status:""});
                    //w2ui[event.target].save();
                
                }
                //this.set(event.recid,{cetakslip:"Tidak Cetak Slip"});
            },  
        });
    }

    bos.mstuserinfofinger.grid2_setdata	= function(){
        w2ui[this.id + '_grid2'].postData 	= this.grid2_data ;
    }
    bos.mstuserinfofinger.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.mstuserinfofinger.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.mstuserinfofinger.grid2_render 	= function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.mstuserinfofinger.grid2_reloaddata	= function(){
        this.grid2_loaddata() ;
        this.grid2_setdata() ;
        this.grid2_reload() ;
    }
    
    bos.mstuserinfofinger.initcomp		= function(){
        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_loaddata() ;
        this.grid2_load() ;
        w2ui[this.id + '_grid1'].hideColumn('selisih');

        this.obj.find('#mesin').select2({
            ajax: {
                url: bos.mstuserinfofinger.base_url + '/seekmesin',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        
    }

    bos.mstuserinfofinger.initcallback	= function(){
        this.obj.on('remove', function(){
            bos.mstuserinfofinger.grid1_destroy() ;
            bos.mstuserinfofinger.grid2_destroy() ;
        }) ;
    }
    
    bos.mstuserinfofinger.cmdproses = bos.mstuserinfofinger.obj.find("#cmdproses") ;
    bos.mstuserinfofinger.initfunc	   = function(){
    

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.mstuserinfofinger.grid1_reloaddata();
        });


        this.obj.find("#cmdproses").on("click", function(e){
            if(confirm("Apakah data user finger di sistem akan diperbarui??\n"+
                         "data ini akan menimpa data yang sudah ada sebelumnya")){
                var datagrid =  w2ui['bos-form-mstuserinfofinger_grid2'].records;
                var datacount =  w2ui['bos-form-mstuserinfofinger_grid2'].records.length;
                datagrid = JSON.stringify(datagrid);
                if(datacount > 0){
                    bjs.ajax( bos.mstuserinfofinger.base_url + '/proses', bjs.getdataform(bos.mstuserinfofinger.obj.find("form"))+"&grid="+datagrid, bos.mstuserinfofinger.cmdproses) ;
                }else{
                    alert("Data tidak valid!!");
                }
            }
        });

       
    }

    

    $(function(){
        bos.mstuserinfofinger.initcomp() ;
        bos.mstuserinfofinger.initcallback() ;
        bos.mstuserinfofinger.initfunc() ;
    });
</script>