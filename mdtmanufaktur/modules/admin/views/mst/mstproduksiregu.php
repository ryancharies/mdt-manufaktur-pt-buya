<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tprdregu">
                        <button class="btn btn-tab tprdregu active" href="#tprdregu_1" data-toggle="tab" >Daftar Regu Produksi</button>
                        <button class="btn btn-tab tprdregu" href="#tprdregu_2" data-toggle="tab">Regu Produksi</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstproduksiregu.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tprdregu_1" >
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <label>Cabang</label>
                                <div class="input-group">
                                    <select name="skd_cabang" id="skd_cabang" class="form-control s2" style="width:100%"
                                                            data-placeholder="Pilih Cabang" data-sf="load_cabang"></select>
                                    <span class="input-group-addon">
                                        <span id="cmdcabang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                                    </span>
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group full-width">
                                <label>&nbsp;</label>
                                <div class="input-group full-width">
                                        <button type="button" class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="grid1" class="full-height" style="height: calc(100% - 50px);"></div>
                    
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tprdregu_2">
                    <table width='100%'>
                        <tr>
                            <td>
                                <table class="osxtable form">
                                    <tr>
                                        <td width="150px"><label for="kode">Kode</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input readonly type="text" maxlength="8" id="kode" name="kode" class="form-control" placeholder="kode" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="cabang">Cabang</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select name="cabang" id="cabang" class="form-control scons" style="width:100%"
                                                    data-placeholder="Cabang / Kantor" data-sf="load_cabang" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="keterangan">Keterangan</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Keterangan" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="karu">Karu</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select name="karu" id="karu" class="form-control scons" style="width:100%"
                                                    data-placeholder="Pilih Karu" data-sf="load_karyawan" required></select>
                                        </td>
                                    </tr>
                                </table>
                                
                            </td>
                        </tr>
                        <tr>
                            <td width='100%'>                                
                                <table class="osxtable form">                                    
                                    <tr>
                                        <td><label for="vendor">Anggota</label> </td>
                                        <td width = '100px'></td>
                                    </tr>
                                    <tr>
                                        <td><select name="anggota" id="anggota" class="form-control scons" style="width:100%"
                                                    data-placeholder="Pilih anggota" data-sf="load_karyawan"></select></td>
                                        <td><button type="button" class="btn btn-primary pull-right" id="cmdok">Tambah</button></td>
                                    </tr>
                                    
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = '300px'>
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                    
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstproduksiregu.grid1_data 	 = null ;
    bos.mstproduksiregu.grid1_loaddata= function(){
        mdl_cabang.vargr.selection = bos.mstproduksiregu.obj.find('#skd_cabang').val();

		this.grid1_data 		= {
			'skd_cabang':JSON.stringify(mdl_cabang.vargr.selection)
		} ;
    }

    bos.mstproduksiregu.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstproduksiregu.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '80px', sortable: false},
                { field: 'keterangan', caption: 'Keterangan', size: '150px', sortable: false},
                { field: 'cabang', caption: 'Cabang', size: '80px', sortable: false},
				{ field: 'karu', caption: 'Karu', size: '150px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstproduksiregu.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstproduksiregu.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstproduksiregu.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstproduksiregu.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstproduksiregu.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail pembelian
    bos.mstproduksiregu.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
            multiSelect : false,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'nip', caption: 'NIP', size: '100px', sortable: false },
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false },
                { field: 'jabatan', caption: 'Jabatan', size: '150px', sortable: false },
                { field: 'act', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstproduksiregu.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.mstproduksiregu.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.mstproduksiregu.grid2_deleterow = function(recid){
        if(confirm("Item di hapus dari detail vendor???"+recid)){
            w2ui[this.id + '_grid2'].select(recid);
            w2ui[this.id + '_grid2'].delete(true);
        }
    }

    bos.mstproduksiregu.grid2_hitungtotal 			= function(){
        bos.mstproduksiregu.gr2 = w2ui[this.id + '_grid2'].records;
        bos.mstproduksiregu.tothtg = 0 ;

        $.each(bos.mstproduksiregu.gr2, function(i, v){
            bos.mstproduksiregu.tothtg += v.hutang;
        });

        w2ui[this.id + '_grid2'].set("ZZZZ",{hutang:bos.mstproduksiregu.tothtg});
    }

    bos.mstproduksiregu.cmdedit		= function(kode){
        bos.mstproduksiregu.act = 1;
        bjs.ajax(this.url + '/editing', 'kode=' + kode,"",function(hasil){
            
            hasil = JSON.parse(hasil);
            with(bos.mstproduksiregu.obj){
                find("#kode").val(hasil.kode) ;
                find("#keterangan").val(hasil.keterangan) ;
                find("#cabang").sval([{id:hasil.cabang,text:hasil.ketcabang}]) ;

                find("#karu").sval([{id:hasil.karu,text:hasil.namakaru}]) ;
                find("#karu")
            }

            $.each(hasil.anggota, function(i, v){
                console.log(v);

                v._act = "<button type='button' onclick = 'bos.mstproduksiregu.grid2_deleterow("+i+")' class='btn btn-danger btn-grid'>Delete</button>";

                w2ui['bos-form-mstproduksiregu_grid2'].add([{ 
                            recid: i,
                            nip: i,
                            nama: v.nama,
                            jabatan: v.jabatan,
                            act: v._act
                        }]) ;

            });
            

            bos.mstproduksiregu.settab(1) ;
        });
    }

    bos.mstproduksiregu.cmddelete		= function(kode){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + kode,"",function(hasil){
                if(hasil == "ok"){
                    bos.mstproduksiregu.grid1_reload() ;
                }
            });
        }
    }

    bos.mstproduksiregu.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#cabang").sval("") ;

        this.obj.find("#karu").sval("") ;


        bjs.ajax(this.url + "/init") ;
        bos.mstproduksiregu.getkode();
        bos.mstproduksiregu.initdetail();

        bos.mstproduksiregu.act = 0 ;

        w2ui[this.id + '_grid2'].clear();

    }

    bos.mstproduksiregu.getkode 			= function(){
        if(bos.mstproduksiregu.act == 0){
            bos.mstproduksiregu.cabang = bos.mstproduksiregu.obj.find("#cabang").val();
            bjs.ajax(this.url + '/getkode','cabang='+bos.mstproduksiregu.cabang,'',function(hasil){
                bos.mstproduksiregu.obj.find("#kode").val(hasil);
            }) ;
        }
    }

    bos.mstproduksiregu.initdetail 			= function(){
        this.obj.find("#anggota").sval("") ;
    }

    bos.mstproduksiregu.settab 		= function(n){
        this.obj.find("#tprdregu button:eq("+n+")").tab("show") ;
    }

    bos.mstproduksiregu.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstproduksiregu.grid1_render() ;
            bos.mstproduksiregu.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            bos.mstproduksiregu.grid2_reload() ;

            this.obj.find("#cabang").focus() ;
        }
    }

    bos.mstproduksiregu.initcomp	= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons",
            clear : false
        }) ;

        bjs.initselect({
            class : "#" + this.id + " .s2",
            clear : true,
            multi:true
        }) ;

        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tprdregu') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;
        bos.mstproduksiregu.init();
    }

    bos.mstproduksiregu.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstproduksiregu.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstproduksiregu.grid1_destroy() ;
            bos.mstproduksiregu.grid2_destroy() ;
        }) ;
    }
    bos.mstproduksiregu.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }
    bos.mstproduksiregu.objs = bos.mstproduksiregu.obj.find("#cmdsave") ;
    bos.mstproduksiregu.objo = bos.mstproduksiregu.obj.find("#cmdok") ;
    bos.mstproduksiregu.initfunc 		= function(){
        this.init() ;
        

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){

                bos.mstproduksiregu.gr2 =  w2ui['bos-form-mstproduksiregu_grid2'].records;
                if(bos.mstproduksiregu.gr2.length > 0){
                    bos.mstproduksiregu.gr2 = JSON.stringify(bos.mstproduksiregu.gr2);
                    bjs.ajax( bos.mstproduksiregu.url + '/saving', bjs.getdataform(this)+"&grid2="+bos.mstproduksiregu.gr2 , bos.mstproduksiregu.objs,function(hasil){
                        if(hasil == "ok"){
                            bos.mstproduksiregu.settab(0) ;
                        }
                    }) ;
                }else{
                    alert("anggota tidak valid!!");
                }
                    
                                
            }
        });

        this.obj.find("#cmdrefresh").on("click", function(){ 
			bos.mstproduksiregu.grid1_reloaddata() ; 
		}) ;

        this.obj.find("#cmdok").on("click", function(e){
            bos.mstproduksiregu.anggota = bos.mstproduksiregu.obj.find("#anggota").val();
            bos.mstproduksiregu.karu = bos.mstproduksiregu.obj.find("#karu").val();
            bos.mstproduksiregu.kode = bos.mstproduksiregu.obj.find("#kode").val();
            if(bos.mstproduksiregu.karu !== bos.mstproduksiregu.anggota){
                bjs.ajax( bos.mstproduksiregu.url + '/seekanggota', "kode="+bos.mstproduksiregu.kode+"&anggota="+bos.mstproduksiregu.anggota , bos.mstproduksiregu.objo,
                function(v){
                    v = JSON.parse(v);

                    v._act = "<button type='button' onclick = 'bos.mstproduksiregu.grid2_deleterow("+v.kode+")' class='btn btn-danger btn-grid'>Delete</button>";

                    if( w2ui['bos-form-mstproduksiregu_grid2'].get(v.kode) == null ){
                        w2ui['bos-form-mstproduksiregu_grid2'].add([{ 
                            recid: v.kode,
                            nip: v.kode,
                            nama: v.nama,
                            jabatan: v.jabatan,
                            act: v._act
                        }]) ;

                        bos.mstproduksiregu.obj.find("#anggota").sval("");
                    }else{
                        alert("Anggota sudah terdaftar!!");
                    }
                }) ;
            }else{
                alert("Anggota tidak boleh sama dengan karu!!");
            }
            
        }) ;

        this.obj.find('#cabang').on('change', function(){
            bos.mstproduksiregu.cabang = bos.mstproduksiregu.obj.find("#cabang").val();
            bos.mstproduksiregu.obj.find('#karu').attr('data-sp',bos.mstproduksiregu.cabang);
            bos.mstproduksiregu.obj.find('#anggota').attr('data-sp',bos.mstproduksiregu.cabang);
            bos.mstproduksiregu.getkode();
        });

        this.obj.find('#cmdcabang').on('click', function(){
            mdl_cabang.vargr.selection = bos.mstproduksiregu.obj.find('#skd_cabang').val();
            mdl_cabang.open(function(r){
                r = JSON.parse(r);
                bos.mstproduksiregu.cabang = [];
                $.each(r, function(i, v){
                    bos.mstproduksiregu.cabang.push({'id':v.kode,'text':v.keterangan})                   
                });
                bos.mstproduksiregu.obj.find('#skd_cabang').sval(bos.mstproduksiregu.cabang);
            });
        
            
        });
    }
    $(function(){
        bos.mstproduksiregu.initcomp() ;
        bos.mstproduksiregu.initcallback() ;
        bos.mstproduksiregu.initfunc() ;
    }) ;
    

</script>
