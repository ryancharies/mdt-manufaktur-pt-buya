<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpph21trfprog">
                        <button class="btn btn-tab tpel active" href="#tpph21trfprog_1" data-toggle="tab" >Daftar Setup Tarif Progresif</button>
                        <button class="btn btn-tab tpel" href="#tpph21trfprog_2" data-toggle="tab">Setup Tarif Progresif</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstpph21tarifprogresif.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpph21trfprog_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpph21trfprog_2">
                    <table width = "100%">
                        <tr>
                            <td>
                                <table class="osxtable form">
                                    <tr>
                                        <td width="14%"><label for="tgl">Tgl</label> </td>
                                        <td width="1%">:</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" value=<?=date("d-m-Y")?> <?=date_set()?><?=$mintgl?>>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width = "100%">
                                    <tr>
                                        <td width = '48%' height = '300px'>
                                            :: NPWP
                                            <div id="grid2" class="full-height" ></div>
                                        </td>
                                        <td width = '4%'>&nbsp;</td>
                                        <td width = '48%' height = '300px'>
                                            :: Non NPWP (Penambahan Pengenaan Tarif)
                                            <div id="grid3" class="full-height"></div>
                                        </td>
                                    <tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button type= "button" class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    bos.mstpph21tarifprogresif.grid1_data 	 = null ;
    bos.mstpph21tarifprogresif.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstpph21tarifprogresif.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstpph21tarifprogresif.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            multiSearch		: false, 
            columns: [
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false,style:'text-align:center'},
                { field: 'npwp',caption: 'NPWP', size: '300px', sortable: false},
                { field: 'nonnpwp',caption: 'Non NPWP', size: '300px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '40px', sortable: false }
            ]
        });
    }

    bos.mstpph21tarifprogresif.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstpph21tarifprogresif.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstpph21tarifprogresif.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstpph21tarifprogresif.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstpph21tarifprogresif.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //set tarif npwp
    bos.mstpph21tarifprogresif.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            url 	: bos.mstpph21tarifprogresif.base_url + "/loadgrid2",
            postData : this.grid2_data ,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'penghasilan', caption: 'Pengahasilan / Thn',render:'float:2', size: '150px', sortable: false,editable:{type:'int'}},
                { field: 'perstarif', caption: 'Tarif(%)',render:'float:2', size: '100px', sortable: false,editable:{type:'int'}}
            ],
            onChange: function(event){
                event.onComplete = function () {
                        w2ui[event.target].save();
                }
            }
        });


    }

    bos.mstpph21tarifprogresif.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.mstpph21tarifprogresif.grid2_setdata	= function(){
        w2ui[this.id + '_grid2'].postData 	= this.grid2_data ;
    }

    bos.mstpph21tarifprogresif.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.mstpph21tarifprogresif.grid2_loaddata		= function(){
        var tgl = this.obj.find("#tgl").val();

        this.grid2_data 		= {'tgl':tgl} ;
        // w2ui['bos-form-mstpph21tarifprogresif_grid2'].clear();
        // bjs.ajax( bos.mstpph21tarifprogresif.base_url + '/loadgrid2', "tgl="+tgl) ;
    }

    bos.mstpph21tarifprogresif.grid2_render 	= function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.mstpph21tarifprogresif.grid2_reloaddata	= function(){
        this.grid2_loaddata() ;
        this.grid2_setdata() ;
        this.grid2_reload() ;
    }

    //set tarif non npwp
    bos.mstpph21tarifprogresif.grid3_load    = function(){
        this.obj.find("#grid3").w2grid({
            name	: this.id + '_grid3',
            url 	: bos.mstpph21tarifprogresif.base_url + "/loadgrid3",
            postData : this.grid3_data ,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'penghasilan', caption: 'Pengahasilan / Thn',render:'float:2', size: '150px', sortable: false,editable:{type:'int'}},
                { field: 'perstarif', caption: 'Tarif(%)',render:'float:2', size: '100px', sortable: false,editable:{type:'int'}}
            ],
            onChange: function(event){
                event.onComplete = function () {
                        w2ui[event.target].save();
                }
            }
        });


    }

    bos.mstpph21tarifprogresif.grid3_destroy 	= function(){
        if(w2ui[this.id + '_grid3'] !== undefined){
            w2ui[this.id + '_grid3'].destroy() ;
        }
    }

    bos.mstpph21tarifprogresif.grid3_setdata	= function(){
        w2ui[this.id + '_grid3'].postData 	= this.grid3_data ;
    }

    bos.mstpph21tarifprogresif.grid3_reload		= function(){
        w2ui[this.id + '_grid3'].reload() ;
    }

    bos.mstpph21tarifprogresif.grid3_loaddata		= function(){
        var tgl = this.obj.find("#tgl").val();
        this.grid3_data 		= {'tgl':tgl} ;
    }

    bos.mstpph21tarifprogresif.grid3_render 	= function(){
        this.obj.find("#grid3").w2render(this.id + '_grid3') ;
    }
    
    bos.mstpph21tarifprogresif.grid3_reloaddata	= function(){
        this.grid3_loaddata() ;
        this.grid3_setdata() ;
        this.grid3_reload() ;
    }

    bos.mstpph21tarifprogresif.savingskala		= function(payroll,bagian,nominal){
        var tgl = this.obj.find("#tgl").val();
        bjs.ajax(this.url + '/saving', 'payroll=' + payroll + '&bagian=' + bagian + '&nominal=' + nominal + "&tgl="+tgl);
    }

    bos.mstpph21tarifprogresif.cmdedit		= function(tgl){
        bjs.ajax(this.url + '/edit', 'tgl=' + tgl);
    }

   
    bos.mstpph21tarifprogresif.init				= function(){
        bjs.ajax(this.url + "/init") ;
    }
    
    bos.mstpph21tarifprogresif.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.mstpph21tarifprogresif.settab 		= function(n){
        this.obj.find("#tpph21trfprog button:eq("+n+")").tab("show") ;
    }

    bos.mstpph21tarifprogresif.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstpph21tarifprogresif.grid1_render() ;
            bos.mstpph21tarifprogresif.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            
            bos.mstpph21tarifprogresif.grid2_reloaddata();
            bos.mstpph21tarifprogresif.grid3_reloaddata();

            // bos.mstpph21tarifprogresif.grid2_render();
            // bos.mstpph21tarifprogresif.grid3_render();
        }
    }

    bos.mstpph21tarifprogresif.initcomp	= function(){
        bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.mstpph21tarifprogresif.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstpph21tarifprogresif.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstpph21tarifprogresif.grid1_destroy() ;
            bos.mstpph21tarifprogresif.grid2_destroy() ;
            bos.mstpph21tarifprogresif.grid3_destroy() ;
        }) ;
    }

    bos.mstpph21tarifprogresif.objs = bos.mstpph21tarifprogresif.obj.find("#cmdsave") ;
    bos.mstpph21tarifprogresif.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_loaddata();
        this.grid2_load() ;
        this.grid3_loaddata();
        this.grid3_load() ;
       

        this.obj.find("#cmdsave").on("click",function(){
            if(confirm("Jadwal Absensi disimpan??")){
                var datagrid2 =  w2ui['bos-form-mstpph21tarifprogresif_grid2'].records;
                datagrid2 = JSON.stringify(datagrid2);

                var datagrid3 =  w2ui['bos-form-mstpph21tarifprogresif_grid3'].records;
                datagrid3 = JSON.stringify(datagrid3);

                bjs.ajax( bos.mstpph21tarifprogresif.base_url + '/saving', bjs.getdataform(bos.mstpph21tarifprogresif.obj.find("form"))+"&grid2="+datagrid2+"&grid3="+datagrid3 , bos.mstpph21tarifprogresif.objs) ;
            }
        });

        this.obj.find("#tgl").on("change",function(){
            bos.mstpph21tarifprogresif.grid2_reloaddata();
            bos.mstpph21tarifprogresif.grid3_reloaddata();
        });
    }

    $(function(){
        bos.mstpph21tarifprogresif.initcomp() ;
        bos.mstpph21tarifprogresif.initcallback() ;
        bos.mstpph21tarifprogresif.initfunc() ;
    }) ;
</script>
