<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpaycomp">
                        <button class="btn btn-tab tpel active" href="#tpaycomp_1" data-toggle="tab" >Daftar Komponen Payroll</button>
                        <button class="btn btn-tab tpel" href="#tpaycomp_2" data-toggle="tab">Komponen Payroll</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstpayrollkomponen.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpaycomp_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpaycomp_2">
                    <table width = "100%">
                        <tr>
                            <td>
                                <table  class="osxtable form">
                                    <tr>
                                        <td width="14%"><label for="kode">Kode</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input type="text" id="kode" name="kode" class="form-control" placeholder="kode" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="keterangan">Keterangan</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="periode">Periode</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select name="periode" id="periode" class="form-control select" style="width:100%"
                                                data-sf="load_periode" data-placeholder="Periode" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="periodemulai">Periode Mulai</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input type="number" id="periodemulai" name="periodemulai" class="form-control" placeholder="Periode Mulai" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="perhitungan">Perhitungan</label> </td>
                                        <td width="1%">:</td>
                                        <td width="50%">
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" id="perhitungan" name="perhitungan"  value="C" checked>
                                                    Custom / Karyawan
                                                </label>
                                                &nbsp;&nbsp;
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" id="perhitungan" name="perhitungan"  value="L" checked>
                                                    Lama Kerja
                                                </label>
                                                &nbsp;&nbsp;
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" id= "perhitungan" name="perhitungan"  value="J">
                                                    Jabatan
                                                </label>
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" id= "perhitungan" name="perhitungan"  value="B">
                                                    Bagian
                                                </label>
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" id= "perhitungan" name="perhitungan"  value="P">
                                                    Periode
                                                </label>
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" id= "perhitungan" name="perhitungan"  value="K">
                                                    Khusus
                                                </label>
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" id= "perhitungan" name="perhitungan"  value="A">
                                                    Asuransi
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="stepskala">Step Skala</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input type="number" id="stepskala" name="stepskala" class="form-control" placeholder="Step Skala" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="kalkulasi">Kalkulasi</label> </td>
                                        <td width="1%">:</td>
                                        <td width="50%">
                                            <label>
                                                    <input type="checkbox" id="kalkulasi" name="kalkulasi"  value="0">
                                                    Ya (Akan dikalkulasi selama periode payroll)
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="diluarjadwal">Diluar Jadwal</label> </td>
                                        <td width="1%">:</td>
                                        <td width="50%">
                                            <label>
                                                    <input type="checkbox" id="diluarjadwal" name="diluarjadwal"  value="0">
                                                    Ya (Disesuaikan dengan jadwal absensi termasuk libur)
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="potongan">Potongan</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <label>
                                                    <input type="checkbox" id="potongan" name="potongan"  value="0">
                                                    Ya&nbsp;&nbsp;
                                            </label>

                                            <label>
                                                    <input type="checkbox" id="potterlambat" name="potterlambat"  value="0">
                                                    Terlambat&nbsp;&nbsp;
                                            </label>

                                            <label>
                                                    <input type="checkbox" id="potplgcepat" name="potplgcepat"  value="0">
                                                    Pulang Cepat&nbsp;&nbsp;
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="perhitunganpph21">Perhitungan PPh 21</label> </td>
                                        <td width="1%">:</td>
                                        <td width="50%">
                                            <label>
                                                    <input type="checkbox" id="perhitunganpph21" name="perhitunganpph21"  value="0">
                                                    Ya (Akan mengakumulasi gaji bruto perhitungan PPh21)
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="resetperiode">Reset Periode</label> </td>
                                        <td width="1%">:</td>
                                        <td width="50%">
                                            <label>
                                                    <input type="checkbox" id="resetperiode" name="resetperiode"  value="0">
                                                    Ya (Nilai setup nilai akan dikosongkan apabila periode berubah / tidak ada setup pada periode tsb kecuali asuransi & kasbon)
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table height = "270px" width = "100%">
                                    <tr>
                                        <td width = "50%"  >
                                            <b>*) Khusus untuk peiode harian</b>
                                            <div id="grid2" class="full-height"></div>
                                        </td>
                                        <td >
                                            <b>*) Setup rekening akuntansi per bagian</b>
                                            <div id="grid3" class="full-height"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstpayrollkomponen.grid1_data 	 = null ;
    bos.mstpayrollkomponen.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstpayrollkomponen.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstpayrollkomponen.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,frozen:true},
                { field: 'keterangan',caption: 'Keterangan', size: '150px', sortable: false,frozen:true},
                { field: 'periode',caption: 'Periode', size: '100px', sortable: false},
                { field: 'periodemulai',caption: 'Periode Mulai', size: '100px', sortable: false},
                { field: 'perhitungan',caption: 'Perhitungan', size: '100px', sortable: false},
                { field: 'potongan',caption: 'Potongan', size: '100px', sortable: false},
                { field: 'stepskala',caption: 'Step Skala', size: '100px', sortable: false},
                { field: 'diluarjadwal',caption: 'Diluar Jadwal', size: '100px', sortable: false},
                { field: 'kalkulasi',caption: 'Kalkulasi', size: '100px', sortable: false},
                { field: 'perhitunganpph21',caption: 'Perh, PPh 21', size: '100px', sortable: false},
                { field: 'resetperiode',caption: 'Reset Periode', size: '100px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstpayrollkomponen.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstpayrollkomponen.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstpayrollkomponen.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstpayrollkomponen.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstpayrollkomponen.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //absen kode
    bos.mstpayrollkomponen.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            recordHeight : 30,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true,
                selectColumn: true,
                multiSelect:true
            },
            columns: [
                { field: 'kode', caption: 'Kode', size: '80px', sortable: false, style:'text-align:center'},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false, style:'text-align:left'},
                { field: 'jenis', caption: 'Jenis', size: '100px', sortable: false}
            ]
        });


    }

    bos.mstpayrollkomponen.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.mstpayrollkomponen.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.mstpayrollkomponen.grid2_loaddata		= function(){
        var kode = this.obj.find("#kode").val();
        w2ui['bos-form-mstpayrollkomponen_grid2'].clear();
        bjs.ajax( bos.mstpayrollkomponen.base_url + '/loadgrid2', "kode="+kode) ;
    }

    //By Rek akt
    bos.mstpayrollkomponen.grid3_load    = function(){
        var listrekening = <?= $listrekening?>;
        this.obj.find("#grid3").w2grid({
            name	: this.id + '_grid3',
            recordHeight : 30,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'kode', caption: 'Kode', size: '80px', sortable: false, style:'text-align:center'},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false, style:'text-align:left'},
                { field: 'rekening', caption: 'Rekening', size: '400px', sortable: false,
                    editable: { type: 'list', items: listrekening, showAll: true },
                    render: function (record, index, col_index) {
                        var html = this.getCellValue(index, col_index);
                        return html || '';
                    }
                }
            ],
            onChange: function(event){
                event.onComplete = function () {
                        w2ui[event.target].save();
                }
            }
        });


    }

    bos.mstpayrollkomponen.grid3_destroy 	= function(){
        if(w2ui[this.id + '_grid3'] !== undefined){
            w2ui[this.id + '_grid3'].destroy() ;
        }
    }

    bos.mstpayrollkomponen.grid3_reload		= function(){
        w2ui[this.id + '_grid3'].reload() ;
    }

    bos.mstpayrollkomponen.grid3_loaddata		= function(){
        var kode = this.obj.find("#kode").val();
        w2ui['bos-form-mstpayrollkomponen_grid3'].clear();
        bjs.ajax( bos.mstpayrollkomponen.base_url + '/loadgrid3', "kode="+kode) ;
    }

    bos.mstpayrollkomponen.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.mstpayrollkomponen.cmdnonaktifkan		= function(id){
        if(confirm("Nonaktifkan data?")){
            bjs.ajax(this.url + '/nonaktifkan', 'kode=' + id);
        }
    }

    bos.mstpayrollkomponen.cmdaktifkan		= function(id){
        if(confirm("Nonaktifkan data?")){
            bjs.ajax(this.url + '/aktifkan', 'kode=' + id);
        }
    }

    bos.mstpayrollkomponen.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#stepskala").val("1") ;
        this.obj.find("#potongan").prop('checked', false);
        this.obj.find("#potterlambat").prop('checked', false);
        this.obj.find("#potplgcepat").prop('checked', false);
        this.obj.find("#diluarjadwal").prop('checked', false);
        this.obj.find("#kalkulasi").prop('checked', false);
        this.obj.find("#perhitunganpph21").prop('checked', false);
        this.obj.find("#resetperiode").prop('checked', false);
        this.obj.find("#periodemulai").val("0") ;
        this.obj.find("#periode").sval("") ;
        bos.mstpayrollkomponen.setopt("perhitungan","C");
        bjs.ajax(this.url + "/init") ;
    }
    
    bos.mstpayrollkomponen.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.mstpayrollkomponen.settab 		= function(n){
        this.obj.find("#tpaycomp button:eq("+n+")").tab("show") ;
    }

    bos.mstpayrollkomponen.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstpayrollkomponen.grid1_render() ;
            bos.mstpayrollkomponen.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#keterangan").focus() ;
            bos.mstpayrollkomponen.grid2_loaddata();
            bos.mstpayrollkomponen.grid3_loaddata();
        }
    }

    bos.mstpayrollkomponen.initcomp	= function(){
        bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.obj.find('#rekening').select2({
            ajax: {
                url: bos.mstpayrollkomponen.base_url + '/seekrekening',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
    }

    bos.mstpayrollkomponen.setcheckpotongan= function(){
        this.obj.find("#potongan").prop('checked', true);
    }

    bos.mstpayrollkomponen.clearcheckpotongan= function(){
        this.obj.find("#potterlambat").prop('checked', false);
        this.obj.find("#potplgcepat").prop('checked', false);
    }

    bos.mstpayrollkomponen.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstpayrollkomponen.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstpayrollkomponen.grid1_destroy() ;
            bos.mstpayrollkomponen.grid2_destroy() ;
            bos.mstpayrollkomponen.grid3_destroy() ;
        }) ;
    }

    bos.mstpayrollkomponen.objs = bos.mstpayrollkomponen.obj.find("#cmdsave") ;
    bos.mstpayrollkomponen.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;
        this.grid3_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                var datagrid2 =  w2ui['bos-form-mstpayrollkomponen_grid2'].records;
                datagrid2 = JSON.stringify(datagrid2);

                var datagrid3 =  w2ui['bos-form-mstpayrollkomponen_grid3'].records;
                datagrid3 = JSON.stringify(datagrid3);

                var grid2select = w2ui['bos-form-mstpayrollkomponen_grid2'].getSelection();
                bjs.ajax( bos.mstpayrollkomponen.url + '/saving', bjs.getdataform(this)+"&grid2="+datagrid2+"&grid2select="+grid2select+"&grid3="+datagrid3 , bos.mstpayrollkomponen.objs) ;
            }
        });

        this.obj.find("#potongan").on("click", function(e){            
            if($(this).prop('checked') == false){
                bos.mstpayrollkomponen.clearcheckpotongan();
            }

        });

        this.obj.find("#potterlambat").on("click", function(e){            
            if($(this).prop('checked') == true){
                bos.mstpayrollkomponen.setcheckpotongan();
            }
        });

        this.obj.find("#potplgcepat").on("click", function(e){            
            if($(this).prop('checked') == true){
                bos.mstpayrollkomponen.setcheckpotongan();
            }
        });
    }

    
    $(function(){
        bos.mstpayrollkomponen.initcomp() ;
        bos.mstpayrollkomponen.initcallback() ;
        bos.mstpayrollkomponen.initfunc() ;
    }) ;
</script>
