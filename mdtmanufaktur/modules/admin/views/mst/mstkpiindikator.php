<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tkpiindikator">
                        <button class="btn btn-tab tpel active" href="#tkpiindikator_1" data-toggle="tab" >Daftar Indikator KPI</button>
                        <button class="btn btn-tab tpel" href="#tkpiindikator_2" data-toggle="tab">Indikator KPI</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstkpiindikator.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tkpiindikator_1" style="padding-top:5px;">
                    
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tkpiindikator_2">
                    <table width = "100%">
                        <tr>
                            <td>
                                <table  class="osxtable form">
                                    <tr>
                                        <td width="14%"><label for="kode">Kode</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input readonly type="text" id="kode" name="kode" class="form-control" placeholder="kode" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="keterangan">Keterangan</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="perhitungan">Perhitungan</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <select name="perhitungan" id="perhitungan" class="form-control select" style="width:100%"
                                                data-placeholder="Pilih Perhitungan" required>
                                                    <option value="0">Custom</option>
                                                    <option value="1">Masuk Tepat Waktu</option>
                                                    <option value="2">Masuk Kerja</option>
                                                    <option value="3">Hasil Produksi</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="periode">Periode</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select name="periode" id="periode" class="form-control scons" style="width:100%"
                                                data-sf="load_periode" data-placeholder="Periode" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="penilaian">Penilaian</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select name="penilaian" id="penilaian" class="form-control select" style="width:100%"
                                                    data-placeholder="Pilih Penilaian">
                                                    <option value = "0">Umum</option>
                                                    <option value = "1" checked>Khusus</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="jabatan">Jabatan</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select name="jabatan" id="jabatan" class="form-control scons" style="width:100%"
                                                data-sf="load_jabatan" data-placeholder="Jabatan"></select>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstkpiindikator.grid1_data 	 = null ;
    bos.mstkpiindikator.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstkpiindikator.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstkpiindikator.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,frozen:true},
                { field: 'keterangan',caption: 'Keterangan', size: '200px', sortable: false,frozen:true},
                { field: 'perhitungan',caption: 'Perhitungan', size: '100px', sortable: false},
                { field: 'periode',caption: 'Periode', size: '100px', sortable: false},
                { field: 'penilaian',caption: 'Penilaian', size: '100px', sortable: false},
                { field: 'jabatan',caption: 'Jabatan', size: '150px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstkpiindikator.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstkpiindikator.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstkpiindikator.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstkpiindikator.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstkpiindikator.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.mstkpiindikator.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.mstkpiindikator.cmdnonaktifkan		= function(id){
        if(confirm("Nonaktifkan data?")){
            bjs.ajax(this.url + '/nonaktifkan', 'kode=' + id);
        }
    }

    bos.mstkpiindikator.cmdaktifkan		= function(id){
        if(confirm("Nonaktifkan data?")){
            bjs.ajax(this.url + '/aktifkan', 'kode=' + id);
        }
    }

    bos.mstkpiindikator.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#penilaian").val("1").trigger('change');
        this.obj.find("#periode").sval("") ;
        this.obj.find("#jabatan").sval("") ;

        this.obj.find("#perhitungan").val("0").trigger('change');

        bjs.ajax(this.url + "/init") ;
        bjs.ajax(this.url + "/getkode") ;
    }
    
    bos.mstkpiindikator.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.mstkpiindikator.settab 		= function(n){
        this.obj.find("#tkpiindikator button:eq("+n+")").tab("show") ;
    }

    bos.mstkpiindikator.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstkpiindikator.grid1_render() ;
            bos.mstkpiindikator.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#keterangan").focus() ;
        }
    }

    bos.mstkpiindikator.initcomp	= function(){
        bjs.initselect({
			class : "#" + this.id + " .scons"
		}) ;
        this.obj.find('.select').select2();
        bjs.ajax(this.url + '/getkode') ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.mstkpiindikator.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstkpiindikator.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstkpiindikator.grid1_destroy() ;
        }) ;
    }

    bos.mstkpiindikator.objs = bos.mstkpiindikator.obj.find("#cmdsave") ;
    bos.mstkpiindikator.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bjs.ajax( bos.mstkpiindikator.url + '/saving', bjs.getdataform(this), bos.mstkpiindikator.objs) ;
            }
        });

        this.obj.find('#penilaian').on("change", function(e) { 
            bos.mstkpiindikator.penilaian = bos.mstkpiindikator.obj.find("#penilaian").val();
            console.log(bos.mstkpiindikator.penilaian);
            if(bos.mstkpiindikator.penilaian == "0"){
                bos.mstkpiindikator.obj.find("#jabatan").prop('disabled', true);
                bos.mstkpiindikator.obj.find("#jabatan").sval("") ;
            }else{
                bos.mstkpiindikator.obj.find("#jabatan").prop('disabled', false);

            }
        });
    }

    
    $(function(){
        bos.mstkpiindikator.initcomp() ;
        bos.mstkpiindikator.initcallback() ;
        bos.mstkpiindikator.initfunc() ;
    }) ;
</script>
