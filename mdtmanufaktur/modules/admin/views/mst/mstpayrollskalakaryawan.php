<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpaycompkrywn">
                        <button class="btn btn-tab tpel active" href="#tpaycompkrywn_1" data-toggle="tab" >Daftar Karyawan</button>
                        <button class="btn btn-tab tpel" href="#tpaycompkrywn_2" data-toggle="tab">Payroll Skala Karyawan</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstpayrollskalakaryawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpaycompkrywn_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpaycompkrywn_2">
                    <table class="osxtable form">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td width="14%"><label for="nip">NIP</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" class="form-control" id="nip" name="nip" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="nama">Nama</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" class="form-control" id="nama" name="nama" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="alamat">Alamat</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" class="form-control" id="alamat" name="alamat" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="jabatan">Jabatan</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" class="form-control" id="jabatan" name="jabatan" readonly>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <hr/>
                                <table>
                                    <tr>
                                        <td width="14%"><label for="tgl">Tgl</label> </td>
                                        <td width="1%">:</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" value=<?=date("d-m-Y")?> <?=date_set()?><?=$mintgl?>>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "300px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            *) Data akan langsung tersimpan ketika sudah diedit
        </div>

        <div class="modal fade" style="position:absolute;" id="wrap-preview-detail-d" role="dialog" data-backdrop="false" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="wm-title">History Perubahan Nilai Komponen</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Komponen Gaji</label>
                                    <input type="text" name="payroll" id="payroll" class="form-control" placeholder="Payroll" readonly = true>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="grid3" style="height:250px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    bos.mstpayrollskalakaryawan.grid1_data 	 = null ;
    bos.mstpayrollskalakaryawan.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstpayrollskalakaryawan.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstpayrollskalakaryawan.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false},
                { field: 'nama',caption: 'Nama', size: '150px', sortable: false},
                { field: 'alamat',caption: 'Alamat', size: '100px', sortable: false},
                { field: 'telepon',caption: 'Telepon', size: '100px', sortable: false},
                { field: 'cmddetail', caption: ' ', size: '40px', sortable: false }
            ]
        });
    }

    bos.mstpayrollskalakaryawan.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstpayrollskalakaryawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstpayrollskalakaryawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstpayrollskalakaryawan.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstpayrollskalakaryawan.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //project
    bos.mstpayrollskalakaryawan.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false ,style:'text-align:center'},
                { field: 'keterangan', caption: 'Komponen', size: '150px', sortable: false},
                { field: 'periode', caption: 'Periode', size: '100px', sortable: false ,style:'text-align:center'},
                { field: 'nominal', caption: 'Nominal', size: '100px', sortable: false,render:'float:2',editable:{type:'int'}}
            ],
            onChange: function(event){
                //console.log(event);
                if(confirm("Apakah perubahan disimpan???")){
                    var kodepayroll = this.getCellValue(event.index,0);                    
                    bos.mstpayrollskalakaryawan.savingskala(kodepayroll,event.value_new);    
                }else{
                    //this.set(event.recid, { namefield : event.value_previous });
                    bos.mstpayrollskalakaryawan.grid2_loaddata();
                }
            }
        });


    }

    bos.mstpayrollskalakaryawan.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.mstpayrollskalakaryawan.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.mstpayrollskalakaryawan.grid2_loaddata		= function(){
        var tgl = this.obj.find("#tgl").val();
        var nip = this.obj.find("#nip").val();
        w2ui['bos-form-mstpayrollskalakaryawan_grid2'].clear();
        bjs.ajax( bos.mstpayrollskalakaryawan.base_url + '/loadgrid2', "tgl="+tgl+"&nip="+nip) ;
    }


    //grid detail perubahan nominal
    bos.mstpayrollskalakaryawan.grid3_data    = null ;
    bos.mstpayrollskalakaryawan.grid3_loaddata= function(){
        this.grid3_data         = {} ;
    }


    bos.mstpayrollskalakaryawan.grid3_load    = function(){
        this.obj.find("#grid3").w2grid({
            name    : this.id + '_grid3',
            show: {
                footer      : false,
                toolbar     : false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            multiSearch     : false,
            columns: [
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false ,style:'text-align:center'},
                { field: 'jabatan', caption: 'Jabatan', size: '120px', sortable: false},
                { field: 'nominal', caption: 'Nominal', size: '100px', sortable: false,render:'float:2'}
            ]
        });
    }

    bos.mstpayrollskalakaryawan.grid3_reload     = function(){
        w2ui[this.id + '_grid3'].reload() ;
    }
    bos.mstpayrollskalakaryawan.grid3_destroy    = function(){
        if(w2ui[this.id + '_grid3'] !== undefined){
            w2ui[this.id + '_grid3'].destroy() ;
        }
    }

    bos.mstpayrollskalakaryawan.grid3_render     = function(){
        this.obj.find("#grid3").w2render(this.id + '_grid3') ;
    }

    bos.mstpayrollskalakaryawan.grid3_reloaddata = function(){
        this.grid3_reload() ;
    }

    bos.mstpayrollskalakaryawan.loadmodalpreview      = function(l){
        this.obj.find("#wrap-preview-detail-d").modal(l) ;
    }

    bos.mstpayrollskalakaryawan.savingskala		= function(payroll,nominal){
        var tgl = this.obj.find("#tgl").val();
        var nip = this.obj.find("#nip").val();
        bjs.ajax(this.url + '/saving', 'payroll=' + payroll + '&nip=' + nip + '&nominal=' + nominal + "&tgl="+tgl);
    }

    bos.mstpayrollskalakaryawan.cmddetail		= function(payroll){
        bjs.ajax(this.url + '/detail', 'payroll=' + payroll);
    }

   
    bos.mstpayrollskalakaryawan.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#potongan").prop('checked', false);
        this.obj.find("#periode").sval("") ;
        bos.mstpayrollskalakaryawan.setopt("perhitungan","C");
        bjs.ajax(this.url + "/init") ;
    }
    
    bos.mstpayrollskalakaryawan.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.mstpayrollskalakaryawan.settab 		= function(n){
        this.obj.find("#tpaycompkrywn button:eq("+n+")").tab("show") ;
    }

    bos.mstpayrollskalakaryawan.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstpayrollskalakaryawan.grid1_render() ;
            bos.mstpayrollskalakaryawan.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#keterangan").focus() ;
            bos.mstpayrollskalakaryawan.grid2_loaddata();
        }
    }

    bos.mstpayrollskalakaryawan.initcomp	= function(){
        bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.mstpayrollskalakaryawan.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstpayrollskalakaryawan.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstpayrollskalakaryawan.grid1_destroy() ;
            bos.mstpayrollskalakaryawan.grid2_destroy() ;
            bos.mstpayrollskalakaryawan.grid3_destroy() ;
        }) ;
    }

    bos.mstpayrollskalakaryawan.objs = bos.mstpayrollskalakaryawan.obj.find("#cmdsave") ;
    bos.mstpayrollskalakaryawan.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;

        this.grid3_load() ;

       

        this.obj.find("#cmdrefresh").on("click",function(){
            bos.mstpayrollskalakaryawan.grid2_loaddata();
        });
    }

    $(function(){
        bos.mstpayrollskalakaryawan.initcomp() ;
        bos.mstpayrollskalakaryawan.initcallback() ;
        bos.mstpayrollskalakaryawan.initfunc() ;
    }) ;
</script>
