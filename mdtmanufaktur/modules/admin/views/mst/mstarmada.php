<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tamd">
                        <button class="btn btn-tab tpel active" href="#tamd_1" data-toggle="tab" >Daftar Armada</button>
                        <button class="btn btn-tab tpel" href="#tamd_2" data-toggle="tab">Armada</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstarmada.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">

    <div class="bodyfix scrollme" style="height:100%">
        <div class="tab-content full-height">
            <div role="tabpanel" class="tab-pane active full-height" id="tamd_1" style="padding-top:5px;">
                <div id="grid1" class="full-height"></div>
            </div>
            <div role="tabpanel" class="tab-pane fade full-height" id="tamd_2">
                <form novalidate>
                    <table class="osxtable form">
                        <tr>
                            <td width="14%"><label for="kode">NoPol</label> </td>
                            <td width="1%">:</td>
                            <td >
                                <input type="text" id="kode" name="kode" class="form-control" placeholder="nopol" required>
                            </td>
                        </tr>
                        <tr>
                            <td width="14%"><label for="keterangan">Keterangan</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan" required>
                            </td>
                        </tr>
                    </table>

                        <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
                    
                </form>
            </div>
        </div>
    </div>


</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstarmada.grid1_data 	 = null ;
    bos.mstarmada.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstarmada.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstarmada.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'No Pol', size: '100px', sortable: false},
                { field: 'keterangan', caption: 'Keterangan', size: '300px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstarmada.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstarmada.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstarmada.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstarmada.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstarmada.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.mstarmada.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
        
    }

    

    bos.mstarmada.cmddelete		= function(id){
        /*if(confirm("Hapus Data?")){
           bjs.ajax(this.url + '/deleting', 'kode=' + id);
        }*/
        var lc = {"module":"","name":"Ganti Password","md5":"","obj":"gantipassword","loc":"config/gantipassword","icon":"ion-filing","size":{"width":400,"height":250},"opt":{"title":false}};
			lc.toString();
			bjs.form(lc);
    }

    bos.mstarmada.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        bjs.ajax(this.url + "/init") ;
    }

    bos.mstarmada.settab 		= function(n){
        this.obj.find("#tamd button:eq("+n+")").tab("show") ;
    }

    bos.mstarmada.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstarmada.grid1_render() ;
            bos.mstarmada.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#kode").focus() ;
        }
    }

    bos.mstarmada.initcomp	= function(){
        /*bjs.initselect({
            class : "#" + this.id + " .select2",
            clear : true
        }) ;*/
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.mstarmada.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstarmada.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstarmada.grid1_destroy() ;
        }) ;
    }

    bos.mstarmada.objs = bos.mstarmada.obj.find("#cmdsave") ;
    bos.mstarmada.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bjs.ajax( bos.mstarmada.url + '/saving', bjs.getdataform(this) , bos.mstarmada.objs) ;
            }
        });
    }

    $(function(){
        bos.mstarmada.initcomp() ;
        bos.mstarmada.initcallback() ;
        bos.mstarmada.initfunc() ;
    }) ;
</script>
