<style media="screen">

    .tab-content{
        background-color: #f8f8f8;
    }

    .optLabel{
        margin-left: 2px;
        margin-right: 20px ;
        font-weight: normal;
    }

    #divOpenDate{
        margin-top: 3px;
    }

</style>
<form>
<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="ion-ios-book"></i></td>
            <td class="title">
                <b>Master Surat-Surat Human Resource & Personnel Information Management </b>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstsurathr.closingform()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <div class="bodyfix scrollme" style="padding-right:0px;padding-left:0px; height: 100% !important;">
    	<div class="row" style="background-color:rgb(215, 215, 215);padding: 5px;text-align: center;">
    		<div class="col-sm-12">
             <div class="nav">
                <div class="btn-group" id="ta_add">
                   <button class="btn btn-tab tad active" href="#ta_add_1" data-toggle="tab">Data Records</button>
                   <button class="btn btn-tab tad" href="#ta_add_2" data-toggle="tab">Entry Point</button>
                </div>
             </div>
    		</div>
    	</div>
        <div class="tab-content" class="scroolme" style="padding: 5px; height: calc(100% - 75px);">
            <div role="tabpanel" class="tab-pane active full-height" id="ta_add_1">
                <div id="grid1" class="full-height"></div>
            </div>
            <div role="tabpanel" class="tab-pane fade full-height" id="ta_add_2">
                <form>
                    <div class="col-md-4">
                        <table class="osxtable form">
                            <tr>
                                <td class="no-padding no-margin">
                                    <table>
                                        <tr>
                                            <td width="100px"><label>Kode</label></td>
                                            <td width="10px">:</td>
                                            <td>
                                                <input maxlength="20" style="width:100px;" type="text" class="form-control col-md-4" name="cKode" id="cKode" placeholder="auto generate" readonly>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px"><label>Judul</label></td>
                                            <td width="10px">:</td>
                                            <td><input style="width:300px" type="text" class="form-control" name="cJudul" id="cJudul" placeholder="Judul Draft"></td>
                                        </tr>
                                        <tr>
                                            <td width="100px"><label>Tanggal</label></td>
                                            <td width="10px">:</td>
                                            <td>
                                                <div class="input-group">
                                                    <input  style="width:210px" 
                                                        type="text" 
                                                        class="form-control date" 
                                                        id="dTgl" 
                                                        name="dTgl" 
                                                        placeholder="dd-mm-yyyy"
                                                        value=<?=date("d-m-Y")?> <?=date_set()?> 
                                                    >
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-8">
                        <table style="margin-top: 10px;" class="osxtable form">
                            <textarea name="txtDraft" id="txtDraft" style="margin-top: 10px;" placeholder="Create Your Draft Here..."></textarea>
                        </table>    
                    </div>
                </form>
            </div>
        </div>
        
        <div class="footer fix">
            <input type="hidden" name="cHiddenPar" id="cHiddenPar">
            <input type="hidden" name="chKode" id="chKode">

            <input type="submit" class="btn btn-primary pull-right" style="margin:auto;" onclick="bos.mstsurathr.actSave()" id="cmdSave" value="Save">
    		<!-- <input type="button" class="btn btn-info pull-right" style="margin:auto; margin-right: 5px;" id="cmdCancel" value="Cancel"> -->
    	</div>
    </div>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    /***************************** Grid Start *******************************/
    bos.mstsurathr.grid1_data    = null ;
    bos.mstsurathr.grid1_loaddata= function(){
        //var dTgl = bos.mstsurathr.obj.find("#tglakhir").val();
        this.grid1_data         = {} ;
    }

    bos.mstsurathr.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name    : this.id + '_grid1',
            limit   : 100 ,
            url     : bos.mstsurathr.base_url + "/LoadGrid",
            postData: this.grid1_data ,
            show: {
                footer      : true,
                toolbar     : true,
                toolbarColumns  : false
            },
            multiSearch     : false,
            columns: [
                { field: 'No', caption: '#', size: '30px', sortable: false, style:"text-align:center"},
                { field: 'kode', caption: 'Kode', size: '80px', sortable: false, style:"text-align:left"},
                { field: 'judul', caption: 'Judul Surat', size: '150px', sortable: false , style:"text-align:left", recordHeight : 80},
                { field: 'tgl', caption: 'Tanggal Pembuatan', size: '150px', sortable: false , style:"text-align:center", recordHeight : 100},
                { field: 'cmdEdit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmdDelete', caption: ' ', size: '80px', sortable: false },
            ]
        });
    }

    bos.mstsurathr.grid1_setdata  = function(){
        w2ui[this.id + '_grid1'].postData   = this.grid1_data ;
    }
    bos.mstsurathr.grid1_reload       = function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstsurathr.grid1_destroy  = function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstsurathr.grid1_render   = function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstsurathr.grid1_reloaddata   = function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }
    /***************************** Grid Ends *******************************/

    bos.mstsurathr.settab 		= function(n){
		this.obj.find("#ta_add button:eq("+n+")").tab("show") ;
	}

    bos.mstsurathr.initTinyMCE = function(){
        tinymce.init({
            selector: '#txtDraft',
            height: 450,
            file_browser_callback_types: 'file image media',
            file_picker_types: 'file image media',   
            forced_root_block : "",
            force_br_newlines : true,
            force_p_newlines : false,
            menubar: true,
            plugins: ['advlist autolink lists link image charmap print preview anchor textcolor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table contextmenu paste code help wordcount'],
            toolbar : 'undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            toolbar1: 'undo redo | insert | styleselect table | bold italic | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media ',
            toolbar2: 'print preview | forecolor backcolor emoticons | fontselect | fontsizeselect | codesample code fullscreen',
            images_upload_credentials: true,
            images_upload_url: bos.mstsurathr.base_url + "/UploadTinyMCE", 
            images_upload_handler: function (blobInfo, success, failure) {
                var xhr, formData;
                
                xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', bos.mstsurathr.base_url + "/UploadTinyMCE");
                
                xhr.onload = function() {
                    var json;
                    
                    if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                    }
                    
                    json = JSON.parse(xhr.responseText);
                    
                    if (!json || typeof json.location != 'string') {
                        failure('Invalid JSON: ' + xhr.responseText);
                        return;
                    }
                    
                    success(json.location);
                };
                    
                formData = new FormData();
                formData.append('file', blobInfo.blob(), blobInfo.filename());
                xhr.send(formData);
            }

        });
    }

	bos.mstsurathr.initcomp	= function(){
        bjs_os.inittab(this.obj, '.tad') ;
        $("#cHiddenPar").val("1");
        bjs.initdate("#" + this.id + " .date") ;
        this.grid1_loaddata() ;
        this.grid1_load() ;
        
        bos.mstsurathr.initTinyMCE() ;
	}

	bos.mstsurathr.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstsurathr.tabsaction( e.i )  ;
        });

        this.obj.on('remove', function(){
            bos.mstsurathr.grid1_destroy() ;
            tinymce.remove() ;
            tinymce.destroy() ;
        }) ;

    }
    
    bos.mstsurathr.initForm = function(){
        //clear all form control
        this.obj.find("#cKode").val("") ;
        this.obj.find("#chKode").val("") ;
        this.obj.find("#cJudul").val("");
        this.obj.find("#txtDraft").val("");
        tinymce.activeEditor.setContent("");
        bos.mstsurathr.initcallback() ;
        this.grid1_reloaddata() ;
        //etc...
    }

    bos.mstsurathr.setContentJS = function(par){
        tinymce.activeEditor.setContent(par , {format: 'raw'});
    }

    bos.mstsurathr.cmdEdit = function(id){
        $("#cHiddenPar").val("2");
        this.settab(1) ; 
        $("#cKode").val(id) ;
        $("#chKode").val(id) ;
        bjs.ajax(this.url + '/editing', 'cKode=' + id);
    }

    bos.mstsurathr.cmdDelete = function (id){
        $("#cKode").val(id) ;
        if(confirm("Are you sure to delete this data?")){
            bjs.ajax(this.url + '/deleting', 'cKode=' + id);   
        }else{
            this.fin("Close this form then!")
        }
    }

    bos.mstsurathr.closingform = function(){
        bos.mstsurathr.close();
    }
    
    bos.mstsurathr.objs = bos.mstsurathr.obj.find("#cmdSave") ;
    bos.mstsurathr.initFunc = function(){

        this.obj.find('form').on("submit", function(e){
            e.preventDefault() ;
            if( bjs.isvalidform(this) ){
                bjs.ajax( bos.mstsurathr.base_url + '/ValidSaving', bjs.getdataform(this), bos.mstsurathr.cmdSave) ;
            }
        }) ;

        this.obj.find('#cmdCancel').on("click", function(e){
            bos.mstsurathr.initForm() ;
        });
    }

    bos.mstsurathr.fin = function(par=''){
        return alert(par);
    }

	$(function(){
		bos.mstsurathr.initcomp() ;
		bos.mstsurathr.initcallback() ;
        bos.mstsurathr.initForm() ;
		bos.mstsurathr.initFunc() ;

        setTimeout(function(){
            // do something??
        },100) ;
	}) ;
</script>
