<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpinkrywn">
                        <button class="btn btn-tab tpel active" href="#tpinkrywn_1" data-toggle="tab" >Daftar Karyawan</button>
                        <button class="btn btn-tab tpel" href="#tpinkrywn_2" data-toggle="tab">PIN Karyawan</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstpinkaryawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpinkrywn_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpinkrywn_2">
                    <table class="osxtable form">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td width="14%"><label for="nip">NIP</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" class="form-control" id="nip" name="nip" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="nama">Nama</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" class="form-control" id="nama" name="nama" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="alamat">Alamat</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" class="form-control" id="alamat" name="alamat" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="jabatan">Jabatan</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" class="form-control" id="jabatan" name="jabatan" readonly>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "400px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            *) Data akan langsung tersimpan ketika sudah diedit
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    bos.mstpinkaryawan.grid1_data 	 = null ;
    bos.mstpinkaryawan.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstpinkaryawan.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstpinkaryawan.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false},
                { field: 'nama',caption: 'Nama', size: '150px', sortable: false},
                { field: 'alamat',caption: 'Alamat', size: '100px', sortable: false},
                { field: 'telepon',caption: 'Telepon', size: '100px', sortable: false},
                { field: 'cmddetail', caption: ' ', size: '40px', sortable: false }
            ]
        });
    }

    bos.mstpinkaryawan.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstpinkaryawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstpinkaryawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstpinkaryawan.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstpinkaryawan.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //project
    bos.mstpinkaryawan.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false ,style:'text-align:center'},
                { field: 'keterangan', caption: 'Mesin', size: '150px', sortable: false},
                { field: 'pin', caption: 'PIN', size: '100px', sortable: false ,style:'text-align:center',editable:{type:'text'}}
            ],
            onChange: function(event){
                //console.log(event);
                if(confirm("Apakah perubahan disimpan???")){
                    var mesin = this.getCellValue(event.index,0);                    
                    bos.mstpinkaryawan.savingpin(mesin,event.value_new);    
                }else{
                    //this.set(event.recid, { namefield : event.value_previous });
                    bos.mstpinkaryawan.grid2_loaddata();
                }

                event.onComplete = function () {
                        w2ui[event.target].save();
                }
            }
        });


    }

    bos.mstpinkaryawan.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.mstpinkaryawan.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.mstpinkaryawan.grid2_loaddata		= function(){
        var nip = this.obj.find("#nip").val();
        w2ui['bos-form-mstpinkaryawan_grid2'].clear();
        bjs.ajax( bos.mstpinkaryawan.base_url + '/loadgrid2', "nip="+nip) ;
    }

    bos.mstpinkaryawan.savingpin		= function(mesin,pin){
        var nip = this.obj.find("#nip").val();
        bjs.ajax(this.url + '/saving', 'mesin=' + mesin + '&nip=' + nip + '&pin=' + pin);
    }

    bos.mstpinkaryawan.cmddetail		= function(payroll){
        bjs.ajax(this.url + '/detail', 'payroll=' + payroll);
    }

   
    bos.mstpinkaryawan.init				= function(){
        this.obj.find("#nip").val("") ;
        this.obj.find("#nama").val("") ;
        this.obj.find("#jabatan").val("") ;
        this.obj.find("#alamat").val("") ;
        bjs.ajax(this.url + "/init") ;
    }
    
    bos.mstpinkaryawan.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.mstpinkaryawan.settab 		= function(n){
        this.obj.find("#tpinkrywn button:eq("+n+")").tab("show") ;
    }

    bos.mstpinkaryawan.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstpinkaryawan.grid1_render() ;
            bos.mstpinkaryawan.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            bos.mstpinkaryawan.grid2_loaddata();
        }
    }

    bos.mstpinkaryawan.initcomp	= function(){
        bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.mstpinkaryawan.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstpinkaryawan.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstpinkaryawan.grid1_destroy() ;
            bos.mstpinkaryawan.grid2_destroy() ;
        }) ;
    }

    bos.mstpinkaryawan.objs = bos.mstpinkaryawan.obj.find("#cmdsave") ;
    bos.mstpinkaryawan.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;
    }

    $(function(){
        bos.mstpinkaryawan.initcomp() ;
        bos.mstpinkaryawan.initcallback() ;
        bos.mstpinkaryawan.initfunc() ;
    }) ;
</script>
