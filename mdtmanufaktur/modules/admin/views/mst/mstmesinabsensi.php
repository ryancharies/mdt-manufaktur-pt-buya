<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tmsnabsensi">
                        <button class="btn btn-tab tpel active" href="#tmsnabsensi_1" data-toggle="tab" >Daftar Mesin Absensi</button>
                        <button class="btn btn-tab tpel" href="#tmsnabsensi_2" data-toggle="tab">Mesin Absensi</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstmesinabsensi.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tmsnabsensi_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tmsnabsensi_2">
                    <table class="osxtable form">
                        <tr>
                            <td width="100px"><label for="kode">Kode</label> </td>
                            <td width="5px">:</td>
                            <td >
                                <input type="text" id="kode" name="kode" class="form-control" placeholder="kode" required>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="keterangan">Keterangan</label> </td>
                            <td>:</td>
                            <td>
                                <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan" required>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="snmesin">Serial Number</label> </td>
                            <td>:</td>
                            <td>
                                <input type="text" id="snmesin" name="snmesin" class="form-control" placeholder="Serial Number Mesin">
                            </td>
                        </tr>
                        <tr>
                            <td><label for="ipmesin">IP Address</label> </td>
                            <td>:</td>
                            <td>
                                <input type="text" id="ipmesin" name="ipmesin" class="form-control" placeholder="IP Address Mesin">
                            </td>
                        </tr>
                        <tr>
                            <td><label for="portmesin">Port</label> </td>
                            <td>:</td>
                            <td>
                                <input type="text" id="portmesin" name="portmesin" class="form-control" placeholder="Port">
                            </td>
                        </tr>
                        <tr>
                            <td><label for="macaddrmesin">Mac Address</label> </td>
                            <td>:</td>
                            <td>
                                <input type="text" id="macaddrmesin" name="macaddrmesin" class="form-control" placeholder="Mac Address Mesin">
                            </td>
                        </tr>
                        <tr>
                            <td><label for="jenis">Jenis</label> </td>
                            <td>:</td>
                            <td>
                                <div class="radio-inline">
                                    <label>
                                        <input type="radio" name="jenis" class="jenis" value="M" checked>
                                        Master
                                    </label>
                                    &nbsp;&nbsp;
                                </div>
                                <div class="radio-inline">
                                    <label>
                                        <input type="radio" name="jenis" class="jenis" value="N">
                                        Non Master
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="cabang">Kantor / Cabang</label> </td>
                            <td>:</td>
                            <td>
                                <select name="cabang" id="cabang" class="form-control select" style="width:100%"
                                        data-placeholder="Kantor / Cabang" required></select>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstmesinabsensi.grid1_data 	 = null ;
    bos.mstmesinabsensi.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstmesinabsensi.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstmesinabsensi.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,frozen:true},
                { field: 'keterangan',caption: 'Keterangan', size: '200px', sortable: false,frozen:true},
                { field: 'snmesin',caption: 'SN', size: '100px', sortable: false},
                { field: 'ipmesin',caption: 'IP', size: '100px', sortable: false},
                { field: 'portmesin',caption: 'Port', size: '50px', sortable: false},
                { field: 'macaddrmesin',caption: 'Mac Addr', size: '100px', sortable: false},
                { field: 'jenis',caption: 'Jenis', size: '100px', sortable: false},
                { field: 'cabang',caption: 'Kantor / Cabang', size: '200px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstmesinabsensi.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstmesinabsensi.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstmesinabsensi.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstmesinabsensi.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstmesinabsensi.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.mstmesinabsensi.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.mstmesinabsensi.cmddelete		= function(id){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + id);
        }
    }

    bos.mstmesinabsensi.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#snmesin").val("") ;
        this.obj.find("#ipmesin").val("") ;
        this.obj.find("#portmesin").val("") ;
        this.obj.find("#macaddrmesin").val("") ;
        this.obj.find("#cabang").sval("") ;
        bos.mstmesinabsensi.setopt("jenis","N");
        bjs.ajax(this.url + "/init") ;
    }
    
    bos.mstmesinabsensi.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.mstmesinabsensi.settab 		= function(n){
        this.obj.find("#tmsnabsensi button:eq("+n+")").tab("show") ;
    }

    bos.mstmesinabsensi.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstmesinabsensi.grid1_render() ;
            bos.mstmesinabsensi.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#kode").focus() ;
        }
    }

    bos.mstmesinabsensi.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.mstmesinabsensi.initcomp	= function(){
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.obj.find('#cabang').select2({
            ajax: {
                url: bos.mstmesinabsensi.base_url + '/seekcabang',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
    }

    bos.mstmesinabsensi.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstmesinabsensi.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstmesinabsensi.grid1_destroy() ;
        }) ;
    }

    bos.mstmesinabsensi.objs = bos.mstmesinabsensi.obj.find("#cmdsave") ;
    bos.mstmesinabsensi.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bjs.ajax( bos.mstmesinabsensi.url + '/saving', bjs.getdataform(this) , bos.mstmesinabsensi.objs) ;
            }
        });
    }

    $(function(){
        bos.mstmesinabsensi.initcomp() ;
        bos.mstmesinabsensi.initcallback() ;
        bos.mstmesinabsensi.initfunc() ;
    }) ;
</script>
