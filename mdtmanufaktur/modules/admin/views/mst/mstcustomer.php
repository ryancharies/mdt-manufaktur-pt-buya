<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tcust">
                        <button class="btn btn-tab tpel active" href="#tcust_1" data-toggle="tab" >Daftar Customer</button>
                        <button class="btn btn-tab tpel" href="#tcust_2" data-toggle="tab">Customer</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstcustomer.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tcust_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tcust_2">
                    <table class="osxtable form">
                        <tr>
                            <td width="150px"><label for="kode">Kode</label> </td>
                            <td width="5px">:</td>
                            <td >
                                <input readonly type="text" id="kode" name="kode" class="form-control" placeholder="Kode" required>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="nama">Nama</label> </td>
                            <td>:</td>
                            <td>
                                <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama" required>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="notelepon">No Telp</label> </td>
                            <td>:</td>
                            <td>
                                <input type="text" id="notelepon" name="notelepon" class="form-control" placeholder="notelepon" required>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="email">Email</label> </td>
                            <td>:</td>
                            <td>
                                <input type="text" id="email" name="email" class="form-control" placeholder="email" required>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="alamat">Alamat</label> </td>
                            <td>:</td>
                            <td>
                                <input type="text" id="alamat" name="alamat" class="form-control" placeholder="alamat" required>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="dati_2">Dati 2 (Kota / Kabupaten)</label> </td>
                            <td>:</td>
                            <td>
                                <select name="dati_2" id="dati_2" class="form-control scons" style="width:100%"
                                    data-placeholder="Dati 2 (Kota / Kabupaten)" data-sf="load_dati_2" required></select>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="golongan">Golongan</label> </td>
                            <td>:</td>
                            <td>
                                <select name="golongan" id="golongan" class="form-control select" style="width:100%"
                                    data-placeholder="Golongan"></select>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="jenishj">Jenis Harga</label> </td>
                            <td>:</td>
                            <td>
                                <select name="jenishj" id="jenishj" class="form-control select" style="width:100%"
                                    data-placeholder="Jenis Harga"></select>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstcustomer.grid1_data 	 = null ;
    bos.mstcustomer.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstcustomer.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstcustomer.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false},
                { field: 'nama', caption: 'Nama', size: '300px', sortable: false},
                { field: 'notelepon', caption: 'No Telp', size: '100px', sortable: false},
                { field: 'email', caption: 'Email', size: '100px', sortable: false},
                { field: 'alamat', caption: 'Alamat', size: '100px', sortable: false},
                { field: 'ketgol', caption: 'Golongan', size: '100px', sortable: false},
                { field: 'ketjenishj', caption: 'Jenis Harga', size: '100px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstcustomer.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstcustomer.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstcustomer.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstcustomer.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstcustomer.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.mstcustomer.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.mstcustomer.cmddelete		= function(id){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + id);
        }
    }

    bos.mstcustomer.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#nama").val("") ;
        this.obj.find("#notelepon").val("") ;
        this.obj.find("#email").val("") ;
        this.obj.find("#alamat").val("") ;
        this.obj.find("#golongan").sval("") ;
        this.obj.find("#jenishj").sval("") ;
        this.obj.find("#dati_2").sval("") ;

        bjs.ajax(this.url + "/init") ;
        bjs.ajax(this.url + '/getkode') ;
    }

    bos.mstcustomer.settab 		= function(n){
        this.obj.find("#tcust button:eq("+n+")").tab("show") ;
    }

    bos.mstcustomer.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstcustomer.grid1_render() ;
            bos.mstcustomer.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#kode").focus() ;
        }
    }

    bos.mstcustomer.initcomp	= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons"
        }) ;

        bjs.ajax(this.url + '/getkode') ;
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.obj.find('#golongan').select2({
            ajax: {
                url: bos.mstcustomer.base_url + '/seekgolongan',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.obj.find('#jenishj').select2({
            ajax: {
                url: bos.mstcustomer.base_url + '/seekjenishj',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

    }

    bos.mstcustomer.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstcustomer.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstcustomer.grid1_destroy() ;
        }) ;
    }

    bos.mstcustomer.objs = bos.mstcustomer.obj.find("#cmdsave") ;
    bos.mstcustomer.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bjs.ajax( bos.mstcustomer.url + '/saving', bjs.getdataform(this) , bos.mstcustomer.objs) ;
            }
        });
    }
    
    
    $(function(){
        bos.mstcustomer.initcomp() ;
        bos.mstcustomer.initcallback() ;
        bos.mstcustomer.initfunc() ;
    }) ;
</script>
