<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Payroll Skala Periode</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstpayrollskalaperiode.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <table class="osxtable form">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td width="100px"><label for="tgl">Tgl</label> </td>
                                        <td width="5px">:</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" value=<?=date("d-m-Y")?> <?=date_set()?><?=$mintgl?>>
                                        </td>
                                        <td width="100px"><label for="periode">Periode</label> </td>
                                        <td width="5px">:</td>
                                        <td width="200px">
                                            <select name="periode" id="periode" class="form-control select" style="width:100%"
                                                data-sf="load_periode" data-placeholder="Periode" required></select>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "400px" >
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
            </table>               
        </div>
        <div class="footer fix hidden" style="height:32px">
            *) Data akan langsung tersimpan ketika sudah diedit
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    
    
    bos.mstpayrollskalaperiode.grid1_load    = function(periode){
        var grid1_kolom = [{ field: 'keterangan', caption: 'Pilih Periode dan Klik Refresh', size: '400px', sortable: false }];
        if(periode == "J")grid1_kolom = <?= $kolomgridjam; ?>;
        if(periode == "H")grid1_kolom = <?= $kolomgridhari; ?>;
        if(periode == "B")grid1_kolom = <?= $kolomgridbulan; ?>;

        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: grid1_kolom,
            onChange: function(event){
                var namefield = grid1_kolom[event.column].field;
                //console.log(event);
                if(confirm("Apakah perubahan disimpan???")){
                    var kodelamakerja = this.getCellValue(event.index,0);                    
                    bos.mstpayrollskalaperiode.savingskala(namefield,kodelamakerja,event.value_new);    
                }else{
                    //this.set(event.recid, { namefield : event.value_previous });
                    bos.mstpayrollskalaperiode.grid1_loaddata();
                }
            }
        });


    }

    bos.mstpayrollskalaperiode.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstpayrollskalaperiode.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.mstpayrollskalaperiode.grid1_loaddata		= function(){
        var tgl = this.obj.find("#tgl").val();
        var periode = this.obj.find("#periode").val();
        // console.log(periode);
        w2ui['bos-form-mstpayrollskalaperiode_grid1'].clear();
        bjs.ajax( bos.mstpayrollskalaperiode.base_url + '/loadgrid1', "tgl="+tgl+"&periode="+periode) ;
    }


    bos.mstpayrollskalaperiode.savingskala		= function(payroll,lamakerja,nominal){
        var tgl = this.obj.find("#tgl").val();
        bjs.ajax(this.url + '/saving', 'payroll=' + payroll + '&lamakerja=' + lamakerja + '&nominal=' + nominal + "&tgl="+tgl);
    }

   
    bos.mstpayrollskalaperiode.init				= function(){
        bjs.ajax(this.url + "/init") ;
    }
    
   
    bos.mstpayrollskalaperiode.initcomp	= function(){
        bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.mstpayrollskalaperiode.initcallback	= function(){


        this.obj.on("remove",function(){
            bos.mstpayrollskalaperiode.grid1_destroy() ;
        }) ;
    }

    bos.mstpayrollskalaperiode.objs = bos.mstpayrollskalaperiode.obj.find("#cmdsave") ;
    bos.mstpayrollskalaperiode.initfunc 		= function(){
        this.init() ;
        this.grid1_load("default") ;
        this.grid1_loaddata("default");
       

        this.obj.find("#cmdrefresh").on("click",function(){
            bos.mstpayrollskalaperiode.grid1_loaddata();
        });

        this.obj.find("#periode").on("change",function(){
            var periode = bos.mstpayrollskalaperiode.obj.find("#periode").val();
            bos.mstpayrollskalaperiode.grid1_destroy();
            bos.mstpayrollskalaperiode.grid1_load(periode);
            bos.mstpayrollskalaperiode.grid1_loaddata();
        });

    }

    $(function(){
        bos.mstpayrollskalaperiode.initcomp() ;
        bos.mstpayrollskalaperiode.initcallback() ;
        bos.mstpayrollskalaperiode.initfunc() ;
    }) ;
</script>
