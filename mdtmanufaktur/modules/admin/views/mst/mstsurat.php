<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tsurat">
                        <button class="btn btn-tab tpel active" href="#tsurat_1" data-toggle="tab" >Daftar Surat</button>
                        <button class="btn btn-tab tpel" href="#tsurat_2" data-toggle="tab">Surat</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstsurat.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tsurat_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tsurat_2">
                    <table class="osxtable form">
                        <tr>
                            <td width="100px"><label for="kode">Kode</label> </td>
                            <td width="5px">:</td>
                            <td >
                                <input readonly type="text" id="kode" name="kode" class="form-control" placeholder="Kode" required>
                            </td>
                        
                            <td><label for="keterangan">Keterangan</label> </td>
                            <td>:</td>
                            <td>
                                <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Keterangan / Nama Surat" required>
                            </td>
                        </tr>
                        <tr>
                            <td colspan = "6">
                                Upload File <span id="idlfilesurat"></span>
                                <table width = "100%">
                                    <tr><td height = '20px'>
                                        <input class="form-control" type="file" name="filesurat" id="filesurat" size="20"/>
                                    </td>
                                    <td>
                                        <input readonly type="text" id="namafile" name="namafile" class="form-control" placeholder="Nama File" required>
                                    </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button type = "button" class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstsurat.grid1_data 	 = null ;
    bos.mstsurat.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstsurat.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstsurat.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers : true
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'keterangan', caption: 'Keterangan / Nama Surat', size: '200px', sortable: false},
                { field: 'path', caption: 'File', size: '100px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstsurat.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstsurat.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstsurat.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstsurat.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstsurat.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.mstsurat.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.mstsurat.cmddelete		= function(id){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + id);
        }
    }

    bos.mstsurat.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#namasurat").val("") ;
        bjs.ajax(this.url + "/init") ;
        bjs.ajax(this.url + '/getkode') ;
    }

    bos.mstsurat.settab 		= function(n){
        this.obj.find("#tsurat button:eq("+n+")").tab("show") ;
    }

    bos.mstsurat.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstsurat.grid1_render() ;
            bos.mstsurat.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#kode").focus() ;
        }
    }

    bos.mstsurat.initcomp	= function(){
        /*bjs.initselect({
            class : "#" + this.id + " .select2",
            clear : true
        }) ;*/
        bjs.ajax(this.url + '/getkode') ;
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;        
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.mstsurat.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstsurat.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstsurat.grid1_destroy() ;
        }) ;
    }

    bos.mstsurat.deletefile = function(file){
        var cfrm = "Apakah file dihapus?? jika file di hapus, maka tidak dapat dikembalikan";
        if(confirm(cfrm)){     
            bjs.ajax(bos.mstsurat.url + '/deletefile',"file="+file) ;   
        }        
    }

    bos.mstsurat.objs = bos.mstsurat.obj.find("#cmdsave") ;
    bos.mstsurat.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("#cmdsave").on("click", function(e){
            //console.log(bos.mstsurat.gfile);
            bjs.ajax( bos.mstsurat.url + '/saving', bjs.getdataform(bos.mstsurat.obj.find("form")), bos.mstsurat.objs) ;
            
        });

        this.obj.find("#filesurat").on("change", function(e){
            bos.mstsurat.uname	= $(this).attr("id") ;
            e.preventDefault() ;
            bos.mstsurat.cfile    = e.target.files ;
            bos.mstsurat.gfile    = new FormData() ;
            $.each(bos.mstsurat.cfile, function(cKey,cValue){
                bos.mstsurat.gfile.append(cKey,cValue) ;
            }) ;
            
            bos.mstsurat.obj.find("#idl" + bos.mstsurat.uname).html("<i class='fa fa-spinner fa-pulse'></i>");
            bos.mstsurat.obj.find("#id" + bos.mstsurat.uname).html("") ;

            bjs.ajaxfile(bos.mstsurat.base_url + "/file_upload/"+ bos.mstsurat.uname, bos.mstsurat.gfile, this) ;
        }) ;
    }

    $(function(){
        bos.mstsurat.initcomp() ;
        bos.mstsurat.initcallback() ;
        bos.mstsurat.initfunc() ;
    }) ;
</script>
