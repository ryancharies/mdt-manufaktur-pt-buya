<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tjwabsensi">
                        <button class="btn btn-tab tpel active" href="#tjwabsensi_1" data-toggle="tab" >Daftar Jadwal Absensi</button>
                        <button class="btn btn-tab tpel" href="#tjwabsensi_2" data-toggle="tab">Jadwal Absensi</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstjwabsensi.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tjwabsensi_1" style="padding-top:5px;">
                    <table width="100%">
                        <tr>
                            <td height="500px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tjwabsensi_2">
                    <table width="100%">
                        <tr>
                            <td width="100%" class="osxtable form">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="kode">Kode</label> </td>
                                        <td width="1%">:</td>
                                        <td width="35%">
                                            <input type="text" id="kode" name="kode" class="form-control" placeholder="Kode" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="keterangan">Keterangan</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan" required>
                                        </td>
                                    </tr>
                                   
                                    <tr>
                                        <td width="14%"><label for="sistem">Sistem</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="sistem" class="sistem" value="N">
                                                    Tidak Wajib Absen
                                                </label>
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="sistem" class="sistem" value="J" checked>
                                                    Terjadwal
                                                </label>
                                                &nbsp;&nbsp;
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="sistem" class="sistem" value="K">
                                                    Terjadwal Bebas
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <hr/>
                            </td>
                            
                        </tr>
                        <tr>
                            <td class="osxtable form">
                                <table>
                                    <tr>
                                        <td width="150px"><label for="siklus">Siklus</label> </td>
                                        <td width="5px">:</td>
                                        <td width="100px">
                                            <input type="text" name="siklus" id="siklus" class="form-control number" value="0"> 
                                        </td>
                                        <td width="5px">Hari</td>            
                                        <td width="150px"><label for="mulailembur">Mulai Lembur</label> </td>
                                        <td width="5px">:</td>
                                        <td width="100px">
                                            <input type="text" name="mulailembur" id="mulailembur" class="form-control number" value="0"> 
                                        </td>
                                        <td width="5px">Menit</td>
                                        <td width="150px"><label for="maxlembur">Max Lembur</label> </td>
                                        <td width="5px">:</td>
                                        <td width="100px">
                                            <input type="text" name="maxlembur" id="maxlembur" class="form-control number" value="0"> 
                                        </td>
                                        <td width="5px">Menit</td>
                                        
                                    </tr>

                                    <tr>
                                        <td width="150px"><label for="toleransi">Toleransi Terlambat</label> </td>
                                        <td width="5px">:</td>
                                        <td width="100px">
                                            <input type="text" name="toleransi" id="toleransi" class="form-control number" value="0"> 
                                        </td>
                                        <td width="5px">Menit</td>
                                        <td width="150px"><label for="bukaabsenmasuk">Buka Absen Masuk</label> </td>
                                        <td width="5px">:</td>
                                        <td width="100px">
                                            <input type="text" name="bukaabsenmasuk" id="bukaabsenmasuk" class="form-control number" value="0"> 
                                        </td>
                                        <td width="5px">Menit</td>  
                                        <td width="150px"><label for="tutupabsenmasuk">Tutup Absen Masuk</label> </td>
                                        <td width="5px">:</td>
                                        <td width="100px">
                                            <input type="text" name="tutupabsenmasuk" id="tutupabsenmasuk" class="form-control number" value="0"> 
                                        </td>
                                        <td width="5px">Menit</td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><label for="bukaabsenpulang">Buka Absen Pulang</label> </td>
                                        <td width="5px">:</td>
                                        <td width="100px">
                                            <input type="text" name="bukaabsenpulang" id="bukaabsenpulang" class="form-control number" value="0"> 
                                        </td>
                                        <td width="5px">Menit</td>
                                        <td width="150px"><label for="tutupabsenpulang">Tutup Absen Pulang</label> </td>
                                        <td width="5px">:</td>
                                        <td width="100px">
                                            <input type="text" name="tutupabsenpulang" id="tutupabsenpulang" class="form-control number" value="0"> 
                                        </td>
                                        <td width="5px">Menit</td>
                                    </tr>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "310px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button type="button" class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    //grid daftar po
    bos.mstjwabsensi.grid1_data    = null ;
    bos.mstjwabsensi.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstjwabsensi.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.mstjwabsensi.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            multiSearch		: false,
            columns: [
                { field: 'kode', caption: 'Kode', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false },
                { field: 'sistem', caption: 'Sistem', size: '100px', sortable: false , style:"text-align:left"},
                { field: 'siklus', caption: 'Siklus (Hari)', size: '100px', sortable: false, render:'int' },
                { field: 'toleransi', caption: 'Toleransi (Menit)', size: '100px', sortable: false, render:'int' },
                { field: 'mulailembur', caption: 'Mulai Lembur (Menit)', size: '100px', sortable: false, render:'int' },
                { field: 'maxlembur', caption: 'Max Lembur (Menit)', size: '100px', sortable: false, render:'int' },
                { field: 'bukaabsenmasuk', caption: 'Buka Absen Masuk (Menit)', size: '100px', sortable: false, render:'int' },
                { field: 'tutupabsenmasuk', caption: 'Tutup Absen Masuk (Menit)', size: '100px', sortable: false, render:'int' },
                { field: 'bukaabsenpulang', caption: 'Buka Absen Pulang (Menit)', size: '100px', sortable: false, render:'int' },
                { field: 'tutupabsenpulang', caption: 'Tutup Absen Pulang (Menit)', size: '100px', sortable: false, render:'int' },
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstjwabsensi.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstjwabsensi.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstjwabsensi.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstjwabsensi.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstjwabsensi.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail
    bos.mstjwabsensi.statusdetail = <?php echo $statusdetail; ?>;
    bos.mstjwabsensi.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'keterangan', caption: 'Keterangan', size: '100px', sortable: false },
                { field: 'masuk', caption: 'Jam Masuk', size: '120px', sortable: false, style:"text-align:center", editable: { type: 'text' } },
                { field: 'pulang', caption: 'Jam Pulang', size: '100px', sortable: false , style:"text-align:center", editable: { type: 'text' } },
                { field: 'status', caption: 'Status', size: '100px', sortable: false, style:"text-align:center",
                    editable: { type: 'select', items: [{ id: '', text: '' }].concat(bos.mstjwabsensi.statusdetail)}, 
                    render: function (record, index, col_index) {
                        var html = '';
                        for (var p in bos.mstjwabsensi.statusdetail) {
                            if (bos.mstjwabsensi.statusdetail[p].id == this.getCellValue(index, col_index)) html = bos.mstjwabsensi.statusdetail[p].text;
                        }
                        return html;
                    }}
            ],
            onChange: function(event){
                event.onComplete = function () {
                        w2ui[event.target].save();
                }
            }
        });


    }

    bos.mstjwabsensi.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.mstjwabsensi.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.mstjwabsensi.grid2_update = function(){
        var datagrid2 =  w2ui[this.id + '_grid2'].records;
        var jmldata = datagrid2.length;
        var siklus = this.obj.find("#siklus").val() ;
        
        if(siklus > jmldata){
            //var selisih = siklus - jmldata;
            for(var i = jmldata+1 ; i<= siklus ; i++){
                w2ui[this.id + '_grid2'].add([
                    { recid:i,
                    keterangan: "Hari Ke "+i,
                    masuk: "00:00:00",
                    pulang:"00:00:00",
                    status: "0"}
                ]) ;
            }
        }else if(jmldata > siklus){
            for(var i = siklus+1 ; i<= jmldata ; i++){
                w2ui[this.id + '_grid2'].select(i);
                w2ui[this.id + '_grid2'].delete(true);
            }
            
        }
        
            
    }


    bos.mstjwabsensi.cmdedit 		= function(kode){
        bjs.ajax(this.url + '/editing', 'kode=' + kode);
    }

    bos.mstjwabsensi.cmddelete 	= function(kode){
        if(confirm("Nonaktifkan Jadwal?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + kode);
        }
    }
    

    bos.mstjwabsensi.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.mstjwabsensi.init 			= function(){
        this.obj.find("#keterangan").val("") ;
        bos.mstjwabsensi.setopt("sistem","J");
        this.obj.find("#siklus").val("0") ;
        this.obj.find("#mulailembur").val("0") ;
        this.obj.find("#maxlembur").val("9999") ;
        this.obj.find("#toleransi").val("0") ;
        this.obj.find("#bukaabsenmasuk").val("0") ;
        this.obj.find("#tutupabsenmasuk").val("0") ;
        this.obj.find("#bukaabsenpulang").val("0") ;
        this.obj.find("#tutupabsenpulang").val("0") ;


        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getkode') ;
        w2ui[this.id + '_grid2'].clear();

    }

    bos.mstjwabsensi.settab 		= function(n){
        this.obj.find("#tjwabsensi button:eq("+n+")").tab("show") ;
    }

    bos.mstjwabsensi.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstjwabsensi.grid1_render() ;
            bos.mstjwabsensi.init() ;
        }else{
            bos.mstjwabsensi.grid2_reload() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#keterangan").focus() ;
        }
    }
    
    bos.mstjwabsensi.initcomp		= function(){
        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;
        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getkode') ;
    }

    bos.mstjwabsensi.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstjwabsensi.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.mstjwabsensi.grid1_destroy() ;
            bos.mstjwabsensi.grid2_destroy() ;
        }) ;
    }

    bos.mstjwabsensi.objs = bos.mstjwabsensi.obj.find("#cmdsave") ;
    bos.mstjwabsensi.initfunc	   = function(){
        

        this.obj.find("#cmdsave").on("click", function(e){
            e.preventDefault() ;
            if( bjs.isvalidform(bos.mstjwabsensi.obj.find("form")) ){
                if(confirm("Jadwal Absensi disimpan??")){
                    var datagrid2 =  w2ui['bos-form-mstjwabsensi_grid2'].records;
                    datagrid2 = JSON.stringify(datagrid2);
                    bjs.ajax( bos.mstjwabsensi.base_url + '/saving', bjs.getdataform(bos.mstjwabsensi.obj.find("form"))+"&grid2="+datagrid2 , bos.mstjwabsensi.cmdsave) ;
                }
            }

        }) ;

        this.obj.find("#siklus").on("blur", function(e){
            bos.mstjwabsensi.grid2_update();
        });

    
    }

    $(function(){
        bos.mstjwabsensi.initcomp() ;
        bos.mstjwabsensi.initcallback() ;
        bos.mstjwabsensi.initfunc() ;
    });
</script>