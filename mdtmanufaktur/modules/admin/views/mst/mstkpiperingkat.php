<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tmstkpiperingkat">
                        <button class="btn btn-tab tpel active" href="#tmstkpiperingkat_1" data-toggle="tab" >Daftar Peringkat KPI</button>
                        <button class="btn btn-tab tpel" href="#tmstkpiperingkat_2" data-toggle="tab">Peringkat KPI</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstkpiperingkat.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>

        <div class="bodyfix scrollme" style="height:100%">
            
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tmstkpiperingkat_1" style="padding-top:5px;">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label>Jabatan</label>
                                <div class="input-group">
                                    <select name="skd_jabatan" id="skd_jabatan" class="form-control s2" style="width:100%"
                                                            data-placeholder="Pilih Jabatan" data-sf="load_jabatan"></select>
                                    <span class="input-group-addon">
                                        <span id="cmdcabang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                                    </span>
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group full-width">
                                <label>&nbsp;</label>
                                <div class="input-group full-width">
                                        <button type="button" class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row" style="height: calc(100% - 50px);"> -->
                        <div id="grid1" class="full-height" style="height: calc(100% - 50px);"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tmstkpiperingkat_2">
                    <table class="osxtable form">
                        <tr>
                            <td width="100px"><label for="kode">Kode</label> </td>
                            <td width="5px">:</td>
                            <td >
                                <input type="text" id="kode" name="kode" class="form-control" placeholder="kode" readonly required>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="keterangan">Keterangan</label> </td>
                            <td>:</td>
                            <td>
                                <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan" required>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="jabatan">Jabatan</label> </td>
                            <td>:</td>
                            <td>
                                <select name="jabatan" id="jabatan" class="form-control scons" style="width:100%"
                                    data-sf="load_jabatan" data-placeholder="Jabatan"></select>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="peringkat">Peringkat</label> </td>
                            <td>:</td>
                            <td>
                                <input  style="width:50px" maxlength="2" type="number" name="peringkat" id="peringkat" class="form-control" value="0">
                            </td>
                        </tr>
                        <tr>
                            <td><label for="nilai_min">Nilai Min</label> </td>
                            <td>:</td>
                            <td>
                                <input  style="width:50px" maxlength="5" type="text" name="nilai_min" id="nilai_min" class="form-control number" value="0">
                            </td>
                        </tr>
                        
                        <tr>
                            <td><label for="nilai_max">Nilai Max</label> </td>
                            <td>:</td>
                            <td>
                                <input  style="width:50px" maxlength="5" type="text" name="nilai_max" id="nilai_max" class="form-control number" value="0">
                            </td>
                        </tr>

                        <tr>
                            <td><label for="nominal">Reward</label> </td>
                            <td>:</td>
                            <td>
                                <input maxlength="20" type="text" name="nominal" id="nominal" class="form-control number" value="0">
                            </td>
                        </tr>
                    </table>

                    
                </div>
            </div>
        </div>

        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>


</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstkpiperingkat.grid1_data    = null ;
    bos.mstkpiperingkat.grid1_loaddata= function(){

        this.grid1_data 		= {
			'skd_jabatan':JSON.stringify(bos.mstkpiperingkat.obj.find('#skd_jabatan').val())
		} ;
    }

    bos.mstkpiperingkat.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.mstkpiperingkat.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false,
            columns: [
                { field: 'kode', caption: 'Kode', size: '50px', sortable: false, style:"text-align:center;",frozen:true},
                { field: 'keterangan', caption: 'Keterangan', size: '250px', sortable: false,frozen:true},
                { field: 'peringkat', caption: 'Peringkat', size: '80px', sortable: false },
                { field: 'jabatan', caption: 'Jabatan', size: '100px', sortable: false },
                { field: 'nilai_min', caption: 'N. Min', size: '80px', sortable: false },
                { field: 'nilai_max', caption: 'N. Max', size: '80px', sortable: false },
                { field: 'nominal', caption: 'Nominal', size: '100px',render:'float:2', sortable: false },
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstkpiperingkat.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstkpiperingkat.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstkpiperingkat.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }
    bos.mstkpiperingkat.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }
    bos.mstkpiperingkat.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }



    bos.mstkpiperingkat.cmdedit 		= function(kode){
        bjs.ajax(this.url + '/editing', 'kode=' + kode);
    }

    bos.mstkpiperingkat.cmddelete 	= function(kode){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + kode);
        }
    }

    bos.mstkpiperingkat.init 			= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#jabatan").sval("") ;
        this.obj.find("#peringkat").val("0") ;
        this.obj.find("#nilai_min").val("0") ;
        this.obj.find("#nilai_max").val("0") ;
        this.obj.find("#nominal").val("0") ;
        bjs.ajax(this.url + '/getkode') ;
        bjs.ajax(this.url + '/init') ; 

    }

    bos.mstkpiperingkat.settab 		= function(n){
        this.obj.find("#tmstkpiperingkat button:eq("+n+")").tab("show") ;
    }

    bos.mstkpiperingkat.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstkpiperingkat.grid1_render() ;
            bos.mstkpiperingkat.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#kode").focus() ;

        }
    }

    bos.mstkpiperingkat.initcomp		= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons",
            clear:false
        }) ;

        bjs.initselect({
            class : "#" + this.id + " .s2",
            clear:true,
            multi: true
        }) ;
        bjs.ajax(this.url + '/getkode') ;
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to dra
    }

    bos.mstkpiperingkat.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstkpiperingkat.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstkpiperingkat.grid1_destroy() ;
        }) ;
    }


    bos.mstkpiperingkat.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.mstkpiperingkat.loadmodelpaket      = function(l){
        this.obj.find("#wrap-paket-d").modal(l) ;
    }

    bos.mstkpiperingkat.objs = bos.mstkpiperingkat.obj.find("#cmdsave") ;
    bos.mstkpiperingkat.initfunc	   = function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bjs.ajax( bos.mstkpiperingkat.url + '/saving',bjs.getdataform(this) , bos.mstkpiperingkat.objs) ;
            }
        });


        this.obj.find("#nominal").on("blur", function(e){
            var nilai = Number(this.value);
            this.value = $.number(nilai,2);
        });

        this.obj.find("#cmdrefresh").on("click", function(e){
            e.preventDefault() ;
            bos.mstkpiperingkat.grid1_reloaddata() ;
        }) ;
    }

    

    $(function(){
        bos.mstkpiperingkat.initcomp() ;
        bos.mstkpiperingkat.initcallback() ;
        bos.mstkpiperingkat.initfunc() ;

    })

</script>
