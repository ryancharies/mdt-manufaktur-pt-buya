<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tptkpjmltanggungan">
                        <button class="btn btn-tab tpel active" href="#tptkpjmltanggungan_1" data-toggle="tab" >Daftar PTKP</button>
                        <button class="btn btn-tab tpel" href="#tptkpjmltanggungan_2" data-toggle="tab">Nilai Tanggungan</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstptkpjmltanggungan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tptkpjmltanggungan_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tptkpjmltanggungan_2">
                    <table>
                        <tr>
                            <td width = "50%" valign ="top">
                                <table  class="osxtable form">
                                    <tr>
                                        <td width="14%"><label for="kode">Kode</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input readonly type="text" id="kode" name="kode" class="form-control" placeholder="Kode" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="keterangan">Keterangan</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Keterangan" required>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign ="top">
                                <table>
                                    <tr>
                                        <td>
                                            <table class="osxtable form">
                                                <tr>
                                                    <td>Tgl</td>
                                                    <td>Nilai Tanggungan / Thn</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td><input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>></td>
                                                    <td><input type="text" id="jmltanggungan" name="jmltanggungan" class="form-control number" placeholder="Jml Tanggungan" required></td>
                                                    <td><button type="button" class="btn btn-primary pull-right" id="cmdsave">Save</button></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height = "200px">
                                            <div id="grid2" class="full-height"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstptkpjmltanggungan.grid1_data 	 = null ;
    bos.mstptkpjmltanggungan.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstptkpjmltanggungan.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstptkpjmltanggungan.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '100px', sortable: false },
            ]
        });
    }

    bos.mstptkpjmltanggungan.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstptkpjmltanggungan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstptkpjmltanggungan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstptkpjmltanggungan.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstptkpjmltanggungan.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //detail mutasi jabatan grid2 
    bos.mstptkpjmltanggungan.grid2_data 	 = null ;
    bos.mstptkpjmltanggungan.grid2_loaddata= function(){
        var kode = bos.mstptkpjmltanggungan.obj.find("#kode").val();
        this.grid2_data 		= {'kode':kode} ;
    }

    bos.mstptkpjmltanggungan.grid2_load    = function(){ 
        this.obj.find("#grid2").w2grid({
            name	 : this.id + '_grid2',
            limit 	 : 100 ,
            url 	 : bos.mstptkpjmltanggungan.base_url + "/loadgrid2",
            postData : this.grid2_data ,
            show 		: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'jmltanggungan', caption: 'Nilai Tanggungan', size: '100px', sortable: false,render:'float:2'},
                { field: 'cmddelete', caption: ' ', size: '100px', sortable: false }
            ]
        });
    }

    bos.mstptkpjmltanggungan.grid2_setdata	= function(){
        w2ui[this.id + '_grid2'].postData 	= this.grid2_data ;
    }
    bos.mstptkpjmltanggungan.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.mstptkpjmltanggungan.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.mstptkpjmltanggungan.grid2_render 	= function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.mstptkpjmltanggungan.grid2_reloaddata	= function(){
        this.grid2_loaddata() ;
        this.grid2_setdata() ;
        this.grid2_reload() ;
    }

    bos.mstptkpjmltanggungan.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.mstptkpjmltanggungan.cmddeletejt		= function(kode,tgl){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + kode + '&tgl=' + tgl);
        }
    }

    bos.mstptkpjmltanggungan.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#jmltanggungan").val("0.00") ;
        w2ui[this.id + '_grid2'].clear();
    }

    bos.mstptkpjmltanggungan.settab 		= function(n){
        this.obj.find("#tptkpjmltanggungan button:eq("+n+")").tab("show") ;
    }

    bos.mstptkpjmltanggungan.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstptkpjmltanggungan.grid1_render() ;
            bos.mstptkpjmltanggungan.init() ;
        }else{
            bos.mstptkpjmltanggungan.grid2_reloaddata() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#tgl").focus() ;
        }
    }

    bos.mstptkpjmltanggungan.initcomp	= function(){
        /*bjs.initselect({
            class : "#" + this.id + " .select2",
            clear : true
        }) ;*/
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;        
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_loaddata() ;
        this.grid2_load() 
    }

    bos.mstptkpjmltanggungan.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstptkpjmltanggungan.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstptkpjmltanggungan.grid1_destroy() ;
            bos.mstptkpjmltanggungan.grid2_destroy() ;
        })
         ;
    }


    bos.mstptkpjmltanggungan.objs = bos.mstptkpjmltanggungan.obj.find("#cmdsave") ;
    bos.mstptkpjmltanggungan.initfunc 		= function(){
        // this.obj.find("form").on("submit", function(e){
        //     e.preventDefault() ;
        //     if(bjs.isvalidform(this)){
        //         bjs.ajax( bos.mstptkpjmltanggungan.url + '/saving', bjs.getdataform(this), bos.mstptkpjmltanggungan.objs) ;
        //     }
        // });
        this.obj.find("#cmdsave").on("click", function(e){
            var confrm = confirm("Data akan disimpan??");
            if(confrm){
                var tgl = bos.mstptkpjmltanggungan.obj.find("#tgl").val();
                var jmltanggungan = bos.mstptkpjmltanggungan.obj.find("#jmltanggungan").val();
                var kode = bos.mstptkpjmltanggungan.obj.find("#kode").val();
                var content = "&kode="+kode+"&tgl="+tgl+"&jmltanggungan="+jmltanggungan;
                bjs.ajax( bos.mstptkpjmltanggungan.base_url + '/saving', bjs.getdataform(this)+content) ; 
            }
        }) ;
        
    }
    

    $(function(){
        bos.mstptkpjmltanggungan.initcomp() ;
        bos.mstptkpjmltanggungan.initcallback() ;
        bos.mstptkpjmltanggungan.initfunc() ;
    }) ;
</script>
