<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tklmstk">
                        <button class="btn btn-tab tklmstk active" href="#tklmstk_1" data-toggle="tab" >Daftar Kelompok Barang</button>
                        <button class="btn btn-tab tklmstk" href="#tklmstk_2" data-toggle="tab">Kelompok Barang</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstklmstock.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tklmstk_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tklmstk_2">
                    <table class="osxtable form">
                        <tr>
                            <td width="150px"><label for="kode">Kode</label> </td>
                            <td width="5px">:</td>
                            <td >
                                <input type="text" id="kode" name="kode" class="form-control" placeholder="kode" readonly required>
                            </td>
                        </tr>
                        <tr>
                            <td width="150px"><label for="keterangan">Nama Kelompok</label> </td>
                            <td width="5px">:</td>
                            <td>
                                <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Nama Kelompok" required>
                            </td>
                        </tr>
                        <tr>
                            <td width="150px"><label for="jenis">Jenis</label> </td>
                            <td width="5px">:</td>
                            <td>
                                <select name="jenis" id="jenis" class="form-control select" style="width:100%" data-placeholder="Jenis Kelompok">
                                    <option value = "B" checked>Bahan Baku</option>
                                    <option value = "P">Produk</option>
                                </select>
                            </td>
                        </tr>
                        
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstklmstock.grid1_data 	 = null ;
    bos.mstklmstock.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstklmstock.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstklmstock.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false},
                { field: 'keterangan', caption: 'Nama Kelompok', size: '300px', sortable: false},
                { field: 'jenis', caption: 'Jenis', size: '300px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstklmstock.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstklmstock.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstklmstock.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstklmstock.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstklmstock.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.mstklmstock.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.mstklmstock.cmddelete		= function(id){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + id);
        }
    }

    bos.mstklmstock.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        // this.obj.find("#jenis").sval("") ;
        bos.mstklmstock.getkode();
        bjs.ajax(this.url + "/init") ;
    }

    bos.mstklmstock.settab 		= function(n){
        this.obj.find("#tklmstk button:eq("+n+")").tab("show") ;
    }

    bos.mstklmstock.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstklmstock.grid1_render() ;
            bos.mstklmstock.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#kode").focus() ;
        }
    }

    bos.mstklmstock.getkode = function(){
        bjs.ajax(bos.mstklmstock.url + '/getkode','','',function(fkt){
            bos.mstklmstock.obj.find("#kode").val(fkt) ;
        }) ;
    }

    bos.mstklmstock.initcomp	= function(){
        /*bjs.initselect({
            class : "#" + this.id + " .select2",
            clear : true
        }) ;*/
        this.obj.find('#jenis').select2();
        bos.mstklmstock.getkode();
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.tklmstk') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.mstklmstock.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstklmstock.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstklmstock.grid1_destroy() ;
        }) ;
    }

    bos.mstklmstock.objs = bos.mstklmstock.obj.find("#cmdsave") ;
    bos.mstklmstock.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bjs.ajax( bos.mstklmstock.url + '/saving', bjs.getdataform(this) , bos.mstklmstock.objs) ;
            }
        });
    }

    $(function(){
        bos.mstklmstock.initcomp() ;
        bos.mstklmstock.initcallback() ;
        bos.mstklmstock.initfunc() ;
    }) ;
</script>
