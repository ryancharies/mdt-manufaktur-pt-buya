<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="thjj">
                        <button class="btn btn-tab thjj active" href="#thjj_1" data-toggle="tab" >Daftar Jenis HJ</button>
                        <button class="btn btn-tab thjj" href="#thjj_2" data-toggle="tab">Jenis HJ</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.msthjjenis.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="thjj_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="thjj_2">
                    <table class="osxtable form">
                        <tr>
                            <td>
                                <table>
                                <tr>
                                    <td width="14%"><label for="kode">Kode</label> </td>
                                    <td width="1%">:</td>
                                    <td >
                                        <input type="text" id="kode" name="kode" class="form-control" placeholder="kode" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="14%"><label for="keterangan">Keterangan</label> </td>
                                    <td width="1%">:</td>
                                    <td>
                                        <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan" required>
                                    </td>
                                </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "400px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.msthjjenis.grid1_data 	 = null ;
    bos.msthjjenis.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.msthjjenis.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.msthjjenis.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false},
                { field: 'keterangan', caption: 'Keterangan', size: '300px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.msthjjenis.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.msthjjenis.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.msthjjenis.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.msthjjenis.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.msthjjenis.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //project
    bos.msthjjenis.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false},
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false},
                { field: 'hj', caption: 'Harga Jual', size: '100px', sortable: false,render:'float:2',editable:{type:'int'}}                
            ],
            onChange: function(event){
                event.onComplete = function () {
                        w2ui[event.target].save();
                }
            }
        });


    }

    bos.msthjjenis.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.msthjjenis.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.msthjjenis.grid2_loaddata		= function(){
        var kode = this.obj.find("#kode").val();
        w2ui['bos-form-msthjjenis_grid2'].clear();
        bjs.ajax( bos.msthjjenis.base_url + '/loadgrid2',"&kode="+kode) ;
    }

    bos.msthjjenis.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.msthjjenis.cmddelete		= function(id){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + id);
        }
    }

    bos.msthjjenis.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        bjs.ajax(this.url + "/init") ;
    }

    bos.msthjjenis.settab 		= function(n){
        this.obj.find("#thjj button:eq("+n+")").tab("show") ;
    }

    bos.msthjjenis.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.msthjjenis.grid1_render() ;
            bos.msthjjenis.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#kode").focus() ;
            bos.msthjjenis.grid2_loaddata();
        }
    }

    bos.msthjjenis.initcomp	= function(){
        /*bjs.initselect({
            class : "#" + this.id + " .select2",
            clear : true
        }) ;*/
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.thjj') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.msthjjenis.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.msthjjenis.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.msthjjenis.grid1_destroy() ;
            bos.msthjjenis.grid2_destroy() ;
        }) ;
    }

    bos.msthjjenis.objs = bos.msthjjenis.obj.find("#cmdsave") ;
    bos.msthjjenis.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;
        this.grid2_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                var datagrid2 =  w2ui['bos-form-msthjjenis_grid2'].records;
                datagrid2 = JSON.stringify(datagrid2);
                bjs.ajax( bos.msthjjenis.url + '/saving', bjs.getdataform(this)+"&grid2="+datagrid2 , bos.msthjjenis.objs) ;
            }
        });
    }

    $(function(){
        bos.msthjjenis.initcomp() ;
        bos.msthjjenis.initcallback() ;
        bos.msthjjenis.initfunc() ;
    }) ;
</script>
