<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tptkpkaryawan">
                        <button class="btn btn-tab tpel active" href="#tptkpkaryawan_1" data-toggle="tab" >Daftar Karyawan</button>
                        <button class="btn btn-tab tpel" href="#tptkpkaryawan_2" data-toggle="tab">PTKP Karyawan</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstptkpkaryawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tptkpkaryawan_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tptkpkaryawan_2">
                    <table class="osxtable form">
                        <tr>
                            <td width = "50%" valign ="top">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="kode">Kode</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input readonly type="text" id="kode" name="kode" class="form-control" placeholder="Kode" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="nama">Nama</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="ktp">No KTP</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" id="ktp" name="ktp" class="form-control" placeholder="No KTP" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="notelepon">No Telp</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" id="notelepon" name="notelepon" class="form-control" placeholder="notelepon" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="alamat">Alamat</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" id="alamat" name="alamat" class="form-control" placeholder="alamat" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="tglmasuk">Tgl Masuk</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input style="width:80px" type="text" class="form-control date" id="tglmasuk" name="tglmasuk" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign ="top">
                                <table class="osxtable form">
                                    <tr>
                                        <td>
                                            <table class="osxtable form">
                                                <tr>
                                                    <td>Tgl</td>
                                                    <td>PTKP</td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td><input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>></td>
                                                    <td><select name="ptkp" id="ptkp" class="form-control select" style="width:100%" data-placeholder="PTKP"></select></td>
                                                    <td><button type="button" class="btn btn-primary pull-right" id="cmdsave">Save</button></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height = "300px">
                                            <div id="grid2" class="full-height"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstptkpkaryawan.grid1_data 	 = null ;
    bos.mstptkpkaryawan.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstptkpkaryawan.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstptkpkaryawan.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,frozen:true,style:"text-align:center;"},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false,frozen:true},
                { field: 'ktp', caption: 'No KTP', size: '100px', sortable: false,frozen:true},
                { field: 'alamat', caption: 'Alamat', size: '200px', sortable: false},
                { field: 'telepon', caption: 'Telepon', size: '100px', sortable: false},
                { field: 'tgl', caption: 'Tgl Masuk', size: '100px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '100px', sortable: false },
            ]
        });
    }

    bos.mstptkpkaryawan.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstptkpkaryawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstptkpkaryawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstptkpkaryawan.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstptkpkaryawan.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //detail mutasi ptkp grid2 
    bos.mstptkpkaryawan.grid2_data 	 = null ;
    bos.mstptkpkaryawan.grid2_loaddata= function(){
        var kode = bos.mstptkpkaryawan.obj.find("#kode").val();
        this.grid2_data 		= {'kode':kode} ;
    }

    bos.mstptkpkaryawan.grid2_load    = function(){ 
        this.obj.find("#grid2").w2grid({
            name	 : this.id + '_grid2',
            limit 	 : 100 ,
            url 	 : bos.mstptkpkaryawan.base_url + "/loadgrid2",
            postData : this.grid2_data ,
            show 		: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false,frozen:true,style:"text-align:center;"},
                { field: 'ptkp', caption: 'PTKP', size: '200px', sortable: false,frozen:true},
                { field: 'cmddelete', caption: ' ', size: '100px', sortable: false }
            ]
        });
    }

    bos.mstptkpkaryawan.grid2_setdata	= function(){
        w2ui[this.id + '_grid2'].postData 	= this.grid2_data ;
    }
    bos.mstptkpkaryawan.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.mstptkpkaryawan.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.mstptkpkaryawan.grid2_render 	= function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.mstptkpkaryawan.grid2_reloaddata	= function(){
        this.grid2_loaddata() ;
        this.grid2_setdata() ;
        this.grid2_reload() ;
    }

    bos.mstptkpkaryawan.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.mstptkpkaryawan.cmddeletemj		= function(nip,tgl){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'nip=' + nip + '&tgl=' + tgl);
        }
    }

    bos.mstptkpkaryawan.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#nama").val("") ;
        this.obj.find("#notelepon").val("") ;
        this.obj.find("#ktp").val("") ;
        this.obj.find("#alamat").val("") ;
        this.obj.find("#ptkp").sval("") ;
        w2ui[this.id + '_grid2'].clear();
    }

    bos.mstptkpkaryawan.settab 		= function(n){
        this.obj.find("#tptkpkaryawan button:eq("+n+")").tab("show") ;
    }

    bos.mstptkpkaryawan.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstptkpkaryawan.grid1_render() ;
            bos.mstptkpkaryawan.init() ;
        }else{
            bos.mstptkpkaryawan.grid2_reloaddata() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#tgl").focus() ;
        }
    }

    bos.mstptkpkaryawan.initcomp	= function(){
        /*bjs.initselect({
            class : "#" + this.id + " .select2",
            clear : true
        }) ;*/
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;        
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_loaddata() ;
        this.grid2_load() 
    }

    bos.mstptkpkaryawan.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstptkpkaryawan.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstptkpkaryawan.grid1_destroy() ;
            bos.mstptkpkaryawan.grid2_destroy() ;
        })
         ;
    }


    bos.mstptkpkaryawan.objs = bos.mstptkpkaryawan.obj.find("#cmdsave") ;
    bos.mstptkpkaryawan.initfunc 		= function(){
        // this.obj.find("form").on("submit", function(e){
        //     e.preventDefault() ;
        //     if(bjs.isvalidform(this)){
        //         bjs.ajax( bos.mstptkpkaryawan.url + '/saving', bjs.getdataform(this), bos.mstptkpkaryawan.objs) ;
        //     }
        // });
        this.obj.find("#cmdsave").on("click", function(e){
            var confrm = confirm("Data akan disimpan??");
            if(confrm){
                var tgl = bos.mstptkpkaryawan.obj.find("#tgl").val();
                var ptkp = bos.mstptkpkaryawan.obj.find("#ptkp").val();
                var kode = bos.mstptkpkaryawan.obj.find("#kode").val();
                var content = "&kode="+kode+"&tgl="+tgl+"&ptkp="+ptkp;
                bjs.ajax( bos.mstptkpkaryawan.base_url + '/saving', bjs.getdataform(this)+content) ; 
            }
        }) ;
        
    }
    
    $('#ptkp').select2({
        ajax: {
            url: bos.mstptkpkaryawan.base_url + '/seekptkp',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    $(function(){
        bos.mstptkpkaryawan.initcomp() ;
        bos.mstptkpkaryawan.initcallback() ;
        bos.mstptkpkaryawan.initfunc() ;
    }) ;
</script>
