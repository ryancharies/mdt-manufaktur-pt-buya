<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Bobot Indikator KPI</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstkpiindikatorbobot.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <table width="100%">
                        <tr>
                            <td  class="osxtable form">
                                <table>
                                    <tr>
                                        <td width="50px"><label for="tgl">Tgl</label> </td>
                                        <td width="3px">:</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" value=<?=date("d-m-Y")?> <?=date_set()?><?=$mintgl?>>
                                        </td>
                                        <td width="120px"><label for="bagian">Bagian - Jabatan :</label> </td>
                                        <td width="100px">
                                            <select name="bagian" id="bagian" class="form-control select" style="width:100%" data-placeholder="Bagian"></select>
                                        </td>
                                        <td width="3px"> - </td>
                                        <td width="100px">
                                            <select name="jabatan" id="jabatan" class="form-control select" style="width:100%" data-placeholder="Jabatan"></select>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "500px" >
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
            </table>               
        </div>
        <div class="footer fix" style="height:32px">
            *) Data akan langsung tersimpan ketika sudah diedit
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    
    bos.mstkpiindikatorbobot.grid1_kolom = <?= $kolomgrid1 ;?>;
    bos.mstkpiindikatorbobot.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false},
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false},
                { field: 'bobot', caption: 'Bobot(%)', size: '100px', sortable: false,render:'float:2',editable:{'type':'int'}}
            ],
            onChange: function(event){
                //console.log(event);
                if(confirm("Apakah perubahan disimpan???")){
                    var kode = this.getCellValue(event.index,0);      
                    //alert(kode);              
                    bos.mstkpiindikatorbobot.savingbobot(kode,event.value_new);    
                }else{
                    //this.set(event.recid, { namefield : event.value_previous });
                    bos.mstkpiindikatorbobot.grid1_loaddata();
                }

                event.onComplete = function () {
                        w2ui[event.target].save();
                }
            }
        });


    }

    bos.mstkpiindikatorbobot.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstkpiindikatorbobot.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.mstkpiindikatorbobot.grid1_loaddata		= function(){
        var tgl = this.obj.find("#tgl").val();
        var jabatan = this.obj.find("#jabatan").val();
        var bagian = this.obj.find("#bagian").val();
        w2ui['bos-form-mstkpiindikatorbobot_grid1'].clear();
        bjs.ajax( bos.mstkpiindikatorbobot.base_url + '/loadgrid1', "tgl="+tgl+"&jabatan="+jabatan+"&bagian="+bagian) ;
    }

    bos.mstkpiindikatorbobot.savingbobot		= function(kode,bobot){
        var tgl = this.obj.find("#tgl").val();
        var jabatan = this.obj.find("#jabatan").val();
        var bagian = this.obj.find("#bagian").val();
        bjs.ajax(this.url + '/saving', 'kode=' + kode + '&tgl=' + tgl + '&jabatan=' + jabatan + "&bagian="+bagian + "&bobot="+bobot);
    }

   
    bos.mstkpiindikatorbobot.init				= function(){
        bjs.ajax(this.url + "/init") ;
    }
    
    bos.mstkpiindikatorbobot.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    

    bos.mstkpiindikatorbobot.initcomp	= function(){
        bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.obj.find('#jabatan').select2({
            ajax: {
                url: bos.mstkpiindikatorbobot.base_url + '/seekjabatan',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.obj.find('#bagian').select2({
            ajax: {
                url: bos.mstkpiindikatorbobot.base_url + '/seekbagian',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
    }

    bos.mstkpiindikatorbobot.initcallback	= function(){


        this.obj.on("remove",function(){
            bos.mstkpiindikatorbobot.grid1_destroy() ;
        }) ;
    }

    bos.mstkpiindikatorbobot.objs = bos.mstkpiindikatorbobot.obj.find("#cmdsave") ;
    bos.mstkpiindikatorbobot.initfunc 		= function(){
        this.init() ;
        this.grid1_load() ;

       

        this.obj.find("#cmdrefresh").on("click",function(){
            bos.mstkpiindikatorbobot.grid1_loaddata();
        });
    }

    $(function(){
        bos.mstkpiindikatorbobot.initcomp() ;
        bos.mstkpiindikatorbobot.initcallback() ;
        bos.mstkpiindikatorbobot.initfunc() ;
    }) ;
</script>
