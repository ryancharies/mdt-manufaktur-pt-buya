<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tmststock">
                        <button class="btn btn-tab tpel active" href="#tmststock_1" data-toggle="tab" >Daftar Stock / Barang</button>
                        <button class="btn btn-tab tpel" href="#tmststock_2" data-toggle="tab">Stock / Barang</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstdatastock.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">

    <div class="bodyfix scrollme" style="height:100%">
        <div class="tab-content full-height">
            <div role="tabpanel" class="tab-pane active full-height" id="tmststock_1" style="padding-top:5px;">
                <div id="grid1" class="full-height"></div>
            </div>
            <div role="tabpanel" class="tab-pane fade full-height" id="tmststock_2">
                <form novalidate>
                    <table width='100%'>
                        <tr>
                            <td>
                                <table class="osxtable form">
                                    <tr>
                                        <td width="14%"><label for="kode">Kode</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input type="text" id="kode" name="kode" class="form-control" placeholder="kode" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="barcode">Barcode</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input type="text" id="barcode" name="barcode" class="form-control" placeholder="Barcode">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="keterangan">Keterangan</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="kelompok">Kelompok</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select name="kelompok" id="kelompok" class="form-control scons" style="width:100%"
                                                    data-placeholder="Kelompok Stock" data-sf="load_klmstock" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="group">Golongan</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select name="group" id="group" class="form-control scons" style="width:100%"
                                                    data-placeholder="Golongan Stock" data-sf="load_golstock" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="satuan">Satuan</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select name="satuan" id="satuan" class="form-control scons" style="width:100%"
                                                    data-placeholder="Satuan Stock" data-sf="load_satuan" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="tampil">Tampil</label> </td>
                                        <td width="1%">:</td>
                                        <td width="50%">
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="tampil" class="tampil" value="B" checked>
                                                    Modul Pembelian & Stock 
                                                </label>
                                                &nbsp;&nbsp;
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="tampil" class="tampil" value="P">
                                                    Modul Penjualan & Stock 
                                                </label>
                                                &nbsp;&nbsp;
                                            </div>
                                            
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="tampil" class="tampil" value="S">
                                                    Semua Modul
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="hargajual">Harga Jual</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input  style="width:200px" maxlength="20" type="text" name="hargajual" id="hargajual" class="form-control number" value="0">

                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                    </table>

                    <div class="footer fix hidden" style="height:32px">
                        <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstdatastock.grid1_data    = null ;
    bos.mstdatastock.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstdatastock.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.mstdatastock.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'kode', caption: 'Kode', size: '150px', sortable: false, style:"text-align:center;",frozen:true},
                { field: 'barcode', caption: 'Barcode', size: '150px', sortable: false,frozen:true},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false },
                { field: 'KetStockGroup', caption: 'Stock Group', size: '150px', sortable: false },
                { field: 'ketkelompok', caption: 'Kelompok', size: '150px', sortable: false },
                { field: 'satuan', caption: 'Satuan', size: '150px', sortable: false ,style:"text-align:center;"},
                { field: 'hargajual', caption: 'Harga Jual', render: 'int', size: '150px', sortable: false, style:"text-align:right"},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstdatastock.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstdatastock.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstdatastock.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }
    bos.mstdatastock.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }
    bos.mstdatastock.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }



    bos.mstdatastock.cmdedit 		= function(kode){
        bjs.ajax(this.url + '/editing', 'kode=' + kode);
    }

    bos.mstdatastock.cmddelete 	= function(kode){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + kode);
        }
    }

    bos.mstdatastock.init 			= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#barcode").val("") ;
        this.obj.find("#hargajual").val("") ;
        this.obj.find("#group").sval("") ;
        this.obj.find("#satuan").sval("") ;
        this.obj.find("#kelompok").sval("") ;
        bjs.ajax(this.url + '/getkode') ;
        bjs.ajax(this.url + '/init') ; 
        bos.mstdatastock.setopt("tampil","B");
        this.obj.find("#ckpilihsemua").checked = false;

    }

    bos.mstdatastock.settab 		= function(n){
        this.obj.find("#tmststock button:eq("+n+")").tab("show") ;
    }

    bos.mstdatastock.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstdatastock.grid1_render() ;
            bos.mstdatastock.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#kode").focus() ;

        }
    }

    bos.mstdatastock.initcomp		= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons"
        }) ;
        bjs.ajax(this.url + '/getkode') ;
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to dra
    }

    bos.mstdatastock.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstdatastock.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstdatastock.grid1_destroy() ;
        }) ;
    }


    bos.mstdatastock.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.mstdatastock.loadmodelpaket      = function(l){
        this.obj.find("#wrap-paket-d").modal(l) ;
    }

    bos.mstdatastock.objs = bos.mstdatastock.obj.find("#cmdsave") ;
    bos.mstdatastock.initfunc	   = function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bjs.ajax( bos.mstdatastock.url + '/saving',bjs.getdataform(this) , bos.mstdatastock.objs) ;
            }
        });



    }

    

    $(function(){
        bos.mstdatastock.initcomp() ;
        bos.mstdatastock.initcallback() ;
        bos.mstdatastock.initfunc() ;

    })

</script>
