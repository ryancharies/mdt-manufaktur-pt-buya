
<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tmstmp">
                        <button class="btn btn-tab tpel active" href="#tmstmp_1" data-toggle="tab" >Daftar Produk</button>
                        <button class="btn btn-tab tpel" href="#tmstmp_2" data-toggle="tab">Material Produk</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstmaterialproduk.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tmstmp_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tmstmp_2">                
                    <table width='100%'>
                        <tr>
                            <td>
                                <table class="osxtable form">
                                    <tr>
                                        <td width="14%"><label for="kode">Kode</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input type="text" readonly id="kode" name="kode" class="form-control" placeholder="kode" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="barcode">Barcode</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input readonly type="text" id="barcode" name="barcode" class="form-control" placeholder="Barcode">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="keterangan">Keterangan</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input readonly type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="group">Golongan</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select readonly name="group" id="group" class="form-control select" style="width:100%"
                                                    data-placeholder="Golongan Stock" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="satuan">Satuan</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select readonly name="satuan" id="satuan" class="form-control select" style="width:100%"
                                                    data-placeholder="Satuan Stock" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="tampil">Tampil</label> </td>
                                        <td width="1%">:</td>
                                        <td width="50%">
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="tampil" class="tampil" value="B" checked>
                                                    Modul Pembelian & Stock 
                                                </label>
                                                &nbsp;&nbsp;
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="tampil" class="tampil" value="P">
                                                    Modul Penjualan & Stock 
                                                </label>
                                                &nbsp;&nbsp;
                                            </div>
                                            
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="tampil" class="tampil" value="S">
                                                    Semua Modul
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="hargajual">Harga Jual</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input readonly style="width:200px" maxlength="20" type="text" name="hargajual" id="hargajual" class="form-control number" value="0">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="bop">BBB</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input style="width:200px" maxlength="20" type="text" name="bbb" id="bbb" class="form-control number" value="0" readonly>
                                        </td>
                                    </tr>
									<tr>
                                        <td width="14%"><label for="btkl">BTKL</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input style="width:200px" maxlength="20" type="text" name="btkl" id="btkl" class="form-control number" value="0">

                                        </td>
                                    </tr>
									<tr>
                                        <td width="14%"><label for="bop">BOP</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input style="width:200px" maxlength="20" type="text" name="bop" id="bop" class="form-control number" value="0">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="bop">HPP Standart</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input style="width:200px" maxlength="20" type="text" name="hpp" id="hpp" class="form-control number" value="0" readonly>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                            <td>
                                <div class="form-group">
                                    <label>Setup bahan baku (*hanya untuk Produk)</label>
                                    <input  type="checkbox" id="ckpilihsemua" onclick ="bos.mstmaterialproduk.grid2_checkheader(this)"> Pilih Semua
                                    <div id="grid2" style="height:300px"></div>

                                </div>
                            </td>
                        </tr>
                    </table>                
                </div>
                
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>

        <div class="modal fade" style="position:absolute;" id="wrap-preview-detail-d" role="dialog" data-backdrop="false" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="wm-title">Preview Detail Material Produk</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Stock</label>
                                    <input type="text" name="d-stock" id="d-stock" class="form-control" placeholder="Stock" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nama Stock</label>
                                    <input type="text" name="d-namastock" id="d-namastock" readonly = true class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Satuan</label>
                                    <input type="text" name="d-satuan" id="d-satuan" readonly = true class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Jenis</label>
                                    <input type="text" name="d-jenis" id="d-jenis" readonly = true class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="grid3" style="height:200px"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>BBB</label>
                                    <input type="text" name="d-bbb" id="d-bbb" readonly = true class="form-control number">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>BTKL</label>
                                    <input type="text" name="d-btkl" id="d-btkl" readonly = true class="form-control number">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>BOP</label>
                                    <input type="text" name="d-bop" id="d-bop" readonly = true class="form-control number">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>HPP</label>
                                    <input type="text" name="d-hpp" id="d-hpp" readonly = true class="form-control number">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <br/>
                                    <button type = "button" class="btn btn-success" id="cmdcetakdetail">Cetak</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstmaterialproduk.grid1_data    = null ;
    bos.mstmaterialproduk.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstmaterialproduk.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.mstmaterialproduk.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'kode', caption: 'Kode', size: '150px', sortable: false, style:"text-align:center;"},
                { field: 'barcode', caption: 'Barcode', size: '150px', sortable: false},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false },
                { field: 'KetStockGroup', caption: 'Stock Group', size: '150px', sortable: false },
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false ,style:"text-align:center;"},
                { field: 'hargajual', caption: 'Harga Jual', render: 'int', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddetail', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstmaterialproduk.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstmaterialproduk.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstmaterialproduk.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }
    bos.mstmaterialproduk.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }
    bos.mstmaterialproduk.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid 2
    bos.mstmaterialproduk.grid2_data    = null ;
    bos.mstmaterialproduk.grid2_lreload    = true ;
    bos.mstmaterialproduk.grid2_loaddata= function(){
        this.grid2_data 		= {'kode':bos.mstmaterialproduk.obj.find("#kode").val()} ;
    }
    bos.mstmaterialproduk.datagrid    = true;
    bos.mstmaterialproduk.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            url 	: bos.mstmaterialproduk.base_url + "/loadgrid2",
            postData: this.grid2_data ,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'ck', caption: '', size: '30px', sortable: false,
                 render: function(record){
                     return '<div style = "text-align:center">'+
                         ' <input type="checkbox"'+(record.ck ? 'checked' : '')+
                         '  onclick="var obj = w2ui[\''+this.name+'\'];obj.set(\''+record.recid+'\',{ck:this.checked});(this.checked) ? obj.set(\''+record.recid+'\',{qty:1}) : obj.set(\''+record.recid+'\',{qty:0});bos.mstmaterialproduk.hitungbbb();">'+
                         '</div>';
                 }
                },
                { field: 'no', caption: 'No', size: '30px', sortable: false,style:"text-align:right"},
                { field: 'kode', caption: 'Stock', size: '80px', sortable: false },
                { field: 'keterangan', caption: 'Nama Stock', size: '250px', sortable: false },
                { field: 'qty', caption: 'Qty', size: '100px', sortable: false, style:"text-align:right",
                 render: function(record){
                     return '<div style = "text-align:center">'+
                         ' <input type="number" style="width:90px" value='+record.qty+
                         '  onChange="var obj = w2ui[\''+this.name+'\'];obj.set(\''+record.recid+'\',{qty:this.value});bos.mstmaterialproduk.hitungbbb();">'+
                         '</div>';
                 }
                },
				{ field: 'hp', caption: 'HP', size: '100px', sortable: false, style:"text-align:right",
                 render: function(record){
                     return '<div style = "text-align:center">'+
                         ' <input type="number" style="width:90px" value='+record.hp+
                         '  onChange="var obj = w2ui[\''+this.name+'\'];obj.set(\''+record.recid+'\',{hp:this.value});bos.mstmaterialproduk.hitungbbb();">'+
                         '</div>';
                 }
                },
                { field: 'satuan', caption: 'Satuan', size: '70px', sortable: false}
            ],
            checkck: function(l){
                this.set({ck:l});
            }     
        });
    }

    bos.mstmaterialproduk.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.mstmaterialproduk.grid2_setdata	= function(){
        w2ui[this.id + '_grid2'].postData 	= this.grid2_data ;
    }

    bos.mstmaterialproduk.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
        
    }

    bos.mstmaterialproduk.grid2_render 	= function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
       
    }

    bos.mstmaterialproduk.grid2_reloaddata	= function(){
        this.grid2_loaddata() ;
        this.grid2_setdata() ;
        this.grid2_reload() ;
        
    }
    bos.mstmaterialproduk.grid2_checkheader 	= function(e){
        w2ui[this.id + '_grid2'].checkck(e.checked);

        $('#ckpilihsemua').checked = e.checked;
    }


    //grid detail
    bos.mstmaterialproduk.grid3_data    = null ;
    bos.mstmaterialproduk.grid3_loaddata= function(){
        this.grid3_data         = {} ;
    }


    bos.mstmaterialproduk.grid3_load    = function(){
        this.obj.find("#grid3").w2grid({
            name    : this.id + '_grid3',
            show: {
                footer      : false,
                toolbar     : false,
                toolbarColumns  : false
            },
            multiSearch     : false,
            columns: [
                { field: 'no', caption: 'No', size: '30px', sortable: false,style:"text-align:right"},
                { field: 'kode', caption: 'Stock', size: '80px', sortable: false },
                { field: 'keterangan', caption: 'Nama Stock', size: '250px', sortable: false },
                { field: 'qty', caption: 'Qty', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'hp', caption: 'HP', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'satuan', caption: 'Satuan', size: '70px', sortable: false}
            ]
        });
    }

    bos.mstmaterialproduk.grid3_reload     = function(){
        w2ui[this.id + '_grid3'].reload() ;
    }
    bos.mstmaterialproduk.grid3_destroy    = function(){
        if(w2ui[this.id + '_grid3'] !== undefined){
            w2ui[this.id + '_grid3'].destroy() ;
        }
    }

    bos.mstmaterialproduk.grid3_render     = function(){
        this.obj.find("#grid3").w2render(this.id + '_grid3') ;
    }

    bos.mstmaterialproduk.grid3_reloaddata = function(){
        this.grid3_reload() ;
    }

    bos.mstmaterialproduk.cmddetail	= function(kode){
        //bos.mstmaterialproduk.initdetil();
        bjs.ajax(this.url + '/loaddetail', 'kode=' + kode);
    }

    bos.mstmaterialproduk.cmdedit 		= function(kode){
        bjs.ajax(this.url + '/editing', 'kode=' + kode);
    }

    bos.mstmaterialproduk.init 			= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#barcode").val("") ;
        this.obj.find("#hargajual").val("0.00") ;
		this.obj.find("#bbb").val("0.00") ;
        this.obj.find("#btkl").val("0.00") ;
		this.obj.find("#bop").val("0.00") ;
        this.obj.find("#hpp").val("0.00") ;
        this.obj.find("#group").sval("") ;
        this.obj.find("#satuan").sval("") ;
        bjs.ajax(this.url + '/init') ; 
        bos.mstmaterialproduk.setopt("jenis","B");
        this.obj.find("#ckpilihsemua").checked = false;
        bos.mstmaterialproduk.grid2_lreload = true;
    }

    bos.mstmaterialproduk.initdetil 			= function(){
        with(bos.mstmaterialproduk.obj){
           find("#d-stock").val("") ;
           find("#bbb").val("0") ;
           find("#d-btkl").val("0") ;
           find("#d-bop").val("0") ;
           find("#d-hpp").val("0") ;
           find("#d-satuan").val("") ;
           find("#d-namastock").val("") ;
        }      
    }

    bos.mstmaterialproduk.settab 		= function(n){
        this.obj.find("#tmstmp button:eq("+n+")").tab("show") ;
    }

    bos.mstmaterialproduk.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstmaterialproduk.grid1_render() ;
            bos.mstmaterialproduk.init() ;
        }else{
            bos.mstmaterialproduk.grid2_reloaddata() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#kode").focus() ;
        }
    }

    bos.mstmaterialproduk.initcomp		= function(){
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

    }

    bos.mstmaterialproduk.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstmaterialproduk.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstmaterialproduk.grid1_destroy() ;
            bos.mstmaterialproduk.grid2_destroy() ;
            bos.mstmaterialproduk.grid3_destroy() ;
        }) ;
    }


    bos.mstmaterialproduk.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.mstmaterialproduk.loadmodeldetail      = function(l){
        this.obj.find("#wrap-preview-detail-d").modal(l) ;
    }

    bos.mstmaterialproduk.hitunghpp      = function(){
        var bbb = string_2n(this.obj.find("#bbb").val());
        var btkl = string_2n(this.obj.find("#btkl").val()) ;
        var bop = string_2n(this.obj.find("#bop").val()) ;
        var hpp = bbb + btkl + bop;
        this.obj.find("#hpp").val($.number(hpp,2))
    }

    bos.mstmaterialproduk.hitungbbb      = function(){
        var nRows = w2ui[this.id + '_grid2'].records.length;
        var bbb = 0 ;
        for(i=0;i< nRows;i++){
            var ck = w2ui[this.id + '_grid2'].getCellValue(i,0);
            if(ck){
                var qty = w2ui[this.id + '_grid2'].getCellValue(i,4);
                var hp = w2ui[this.id + '_grid2'].getCellValue(i,5);
                bbb += Number(qty) * Number(hp);
            }
        }
        this.obj.find("#bbb").val($.number(bbb,2));

        bos.mstmaterialproduk.hitunghpp();
    }

    bos.mstmaterialproduk.initreportdetail  = function(s,e){
        bjs.ajax(this.base_url+ '/initreportdetail', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.mstmaterialproduk.openreportdetil  = function(){
        bjs_os.form_report( this.base_url + '/showreportdetil' ) ;
    }

    bos.mstmaterialproduk.objs = bos.mstmaterialproduk.obj.find("#cmdsave") ;
    bos.mstmaterialproduk.initfunc	   = function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_loaddata() ;
        this.grid2_load() ;

        this.grid3_load() ;


        this.obj.find("btkl").on("blur", function(e){
            bos.mstmaterialproduk.hitungbbb();
        });

        this.obj.find("bop").on("blur", function(e){
            bos.mstmaterialproduk.hitungbbb();
        });

        this.obj.find("#cmdcetakdetail").on("click", function(e){
            e.preventDefault() ;
            bos.mstmaterialproduk.initreportdetail();
        }) ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                var datagrid2 =  w2ui['bos-form-mstmaterialproduk_grid2'].records;
                datagrid2 = JSON.stringify(datagrid2);
                bjs.ajax( bos.mstmaterialproduk.url + '/saving',bjs.getdataform(this)+"&grid2="+datagrid2 , bos.mstmaterialproduk.objs) ;
            }
        });



    }

    $(function(){
        bos.mstmaterialproduk.initcomp() ;
        bos.mstmaterialproduk.initcallback() ;
        bos.mstmaterialproduk.initfunc() ;

    })

</script>