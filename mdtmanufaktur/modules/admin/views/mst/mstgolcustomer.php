<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tgc">
                        <button class="btn btn-tab tpel active" href="#tgc_1" data-toggle="tab" >Daftar Gol. Customer</button>
                        <button class="btn btn-tab tpel" href="#tgc_2" data-toggle="tab">Gol. Customer</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstgolcustomer.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tgc_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tgc_2">
                    <table class="osxtable form">
                        <tr>
                            <td width="14%"><label for="kode">Kode</label> </td>
                            <td width="1%">:</td>
                            <td >
                                <input type="text" id="kode" name="kode" class="form-control" placeholder="kode" required>
                            </td>
                        </tr>
                        <tr>
                            <td width="14%"><label for="keterangan">Keterangan</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan" required>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="rekpersd">Rek. Penjualan</label> </td>
                            <td>:</td>
                            <td>
                                <select name="rekpj" id="rekpj" class="form-control select" style="width:100%"
                                    data-placeholder="Rekening Penjualan"></select>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="rekpenj">Rek. Retur Penjualan</label> </td>
                            <td>:</td>
                            <td>
                                <select name="rekrj" id="rekrj" class="form-control select" style="width:100%"
                                    data-placeholder="Rekening Retur Penjualan"></select>
                            </td>
                        </tr>
                        <tr>
                            <td><label for="rekdisc">Rek. Disc</label> </td>
                            <td>:</td>
                            <td>
                                <select name="rekdisc" id="rekdisc" class="form-control select" style="width:100%"
                                    data-placeholder="Rekening Disc / Pot. Penjualan"></select>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.mstgolcustomer.grid1_data 	 = null ;
    bos.mstgolcustomer.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.mstgolcustomer.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.mstgolcustomer.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false},
                { field: 'keterangan', caption: 'Keterangan', size: '300px', sortable: false},
                { field: 'rekpj', caption: 'Rek. Penjualan', size: '100px', sortable: false},
                { field: 'rekrj', caption: 'Rek. Retur Penjualan', size: '100px', sortable: false},
                { field: 'rekdisc', caption: 'Rek. Disc', size: '100px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.mstgolcustomer.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.mstgolcustomer.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.mstgolcustomer.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstgolcustomer.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.mstgolcustomer.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.mstgolcustomer.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.mstgolcustomer.cmddelete		= function(id){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + id);
        }
    }

    bos.mstgolcustomer.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#rekpj").sval("") ;
        this.obj.find("#rekrj").sval("") ;
        this.obj.find("#rekdisc").sval("") ;
        bjs.ajax(this.url + "/init") ;
    }

    bos.mstgolcustomer.settab 		= function(n){
        this.obj.find("#tgc button:eq("+n+")").tab("show") ;
    }

    bos.mstgolcustomer.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.mstgolcustomer.grid1_render() ;
            bos.mstgolcustomer.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#kode").focus() ;
        }
    }

    bos.mstgolcustomer.initcomp	= function(){
        /*bjs.initselect({
            class : "#" + this.id + " .select2",
            clear : true
        }) ;*/
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.mstgolcustomer.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.mstgolcustomer.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.mstgolcustomer.grid1_destroy() ;
        }) ;
    }

    bos.mstgolcustomer.objs = bos.mstgolcustomer.obj.find("#cmdsave") ;
    bos.mstgolcustomer.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bjs.ajax( bos.mstgolcustomer.url + '/saving', bjs.getdataform(this) , bos.mstgolcustomer.objs) ;
            }
        });
    }
    
    $('#rekpj').select2({
        ajax: {
            url: bos.mstgolcustomer.base_url + '/seekrekening',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
    
    $('#rekrj').select2({
        ajax: {
            url: bos.mstgolcustomer.base_url + '/seekrekening',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
    
    $('#rekdisc').select2({
        ajax: {
            url: bos.mstgolcustomer.base_url + '/seekrekening',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    $(function(){
        bos.mstgolcustomer.initcomp() ;
        bos.mstgolcustomer.initcallback() ;
        bos.mstgolcustomer.initfunc() ;
    }) ;
</script>
