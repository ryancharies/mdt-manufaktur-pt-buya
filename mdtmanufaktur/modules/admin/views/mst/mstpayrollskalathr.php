<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Payroll Skala THR</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.mstpayrollskalathr.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <table width="100%">
                        <tr>
                            <td  class="osxtable form">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="tgl">Tgl</label> </td>
                                        <td width="1%">:</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" value=<?=date("d-m-Y")?> <?=date_set()?><?=$mintgl?>>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "400px" >
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
            </table>               
        </div>
        <div class="footer fix hidden" style="height:32px">
            *) Data akan langsung tersimpan ketika sudah diedit
        </div>

        <div class="modal fade" style="position:absolute;" id="wrap-preview-detail-d" role="dialog" data-backdrop="false" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="wm-title">History Perubahan Nilai Komponen</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Komponen Gaji</label>
                                    <input type="text" name="payroll" id="payroll" class="form-control" placeholder="Payroll" readonly = true>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="grid1" style="height:250px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    
    bos.mstpayrollskalathr.grid1_kolom = <?= $kolomgrid1 ;?>;
    bos.mstpayrollskalathr.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: bos.mstpayrollskalathr.grid1_kolom,
            onChange: function(event){
                var namefield = bos.mstpayrollskalathr.grid1_kolom[event.column].field;
                //console.log(event);
                if(confirm("Apakah perubahan disimpan???")){
                    var kodelamakerja = this.getCellValue(event.index,0);                    
                    bos.mstpayrollskalathr.savingskala(namefield,kodelamakerja,event.value_new);    
                }else{
                    //this.set(event.recid, { namefield : event.value_previous });
                    bos.mstpayrollskalathr.grid1_loaddata();
                }

                event.onComplete = function () {
                        w2ui[event.target].save();
                }
            }
        });


    }

    bos.mstpayrollskalathr.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.mstpayrollskalathr.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.mstpayrollskalathr.grid1_loaddata		= function(){
        var tgl = this.obj.find("#tgl").val();
        w2ui['bos-form-mstpayrollskalathr_grid1'].clear();
        bjs.ajax( bos.mstpayrollskalathr.base_url + '/loadgrid1', "tgl="+tgl) ;
    }


    //grid detail perubahan nominal
    bos.mstpayrollskalathr.grid2_data    = null ;
    bos.mstpayrollskalathr.grid2_loaddata= function(){
        this.grid2_data         = {} ;
    }


    bos.mstpayrollskalathr.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name    : this.id + '_grid2',
            show: {
                footer      : false,
                toolbar     : false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            multiSearch     : false,
            columns: [
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false ,style:'text-align:center'},
                { field: 'lama', caption: 'Lama Kerja ', size: '120px', sortable: false},
                { field: 'prosentase', caption: 'Prosentase', size: '100px', sortable: false,render:'float:2'}
            ]
        });
    }

    bos.mstpayrollskalathr.grid2_reload     = function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.mstpayrollskalathr.grid2_destroy    = function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.mstpayrollskalathr.grid2_render     = function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.mstpayrollskalathr.grid2_reloaddata = function(){
        this.grid2_reload() ;
    }

    bos.mstpayrollskalathr.loadmodalpreview      = function(l){
        this.obj.find("#wrap-preview-detail-d").modal(l) ;
    }

    bos.mstpayrollskalathr.savingskala		= function(payroll,lamakerja,prosentase){
        var tgl = this.obj.find("#tgl").val();
        bjs.ajax(this.url + '/saving', 'payroll=' + payroll + '&lamakerja=' + lamakerja + '&prosentase=' + prosentase + "&tgl="+tgl);
    }

    bos.mstpayrollskalathr.cmddetail		= function(payroll){
        bjs.ajax(this.url + '/detail', 'payroll=' + payroll);
    }

   
    bos.mstpayrollskalathr.init				= function(){
        bjs.ajax(this.url + "/init") ;
    }
    
    bos.mstpayrollskalathr.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    

    bos.mstpayrollskalathr.initcomp	= function(){
        bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.mstpayrollskalathr.initcallback	= function(){


        this.obj.on("remove",function(){
            bos.mstpayrollskalathr.grid1_destroy() ;
            bos.mstpayrollskalathr.grid2_destroy() ;
        }) ;
    }

    bos.mstpayrollskalathr.objs = bos.mstpayrollskalathr.obj.find("#cmdsave") ;
    bos.mstpayrollskalathr.initfunc 		= function(){
        this.init() ;
        this.grid1_load() ;
        this.grid1_loaddata();

        this.grid2_load() ;

       

        this.obj.find("#cmdrefresh").on("click",function(){
            bos.mstpayrollskalathr.grid1_loaddata();
        });
    }

    $(function(){
        bos.mstpayrollskalathr.initcomp() ;
        bos.mstpayrollskalathr.initcallback() ;
        bos.mstpayrollskalathr.initfunc() ;
    }) ;
</script>
