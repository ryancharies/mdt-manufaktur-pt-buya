<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><?=$app_title?></title>
	<link rel="stylesheet" type="text/css" href="<?=base_url('bismillah/bootstrap/css/bootstrap.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('bismillah/icon/css/font-awesome.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('bismillah/icon/css/ionicons.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('bismillah/animate.css')?>">
	<link rel="stylesheet" href="<?=base_url('bismillah/jQueryUI/jquery-ui.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('bismillah/select2/css/select2.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('bismillah/datepicker/bootstrap-datetimepicker.css')?>">
	<link rel="stylesheet" href="<?=base_url('bismillah/w2/w2ui-1.5.rc1.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('bismillah/pnotify/pnotify.custom.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('bismillah/core.desktop.css')?>">
	<link rel="stylesheet" href="<?=base_url('bismillah/yearcalendar/js-year-calendar.min.css')?>">
	<style media="screen">
		body{ background-image: url("./uploads/bg/bg.jpg"); }
	
	</style>
</head>
<body id="osx" data-bismillahauth="">
	<audio id="wrap-audio-notif">
		<source type="audio/mpeg" src="./uploads/notif.mp3"></source>
	</audio>
	
	<div id="topbar">
	<!-- my calendar -->
		<table id="general">
			 <tr>
				  <td width="30%" class="icon" onclick="">
				  		<i onclick="" class="ion-cube" style="margin-right: 5px;"></i>
					  	<strong><?=$app_title?></strong>
				  </td>
				  <td width="30%" class="text-center"><b><?=$fullname?></b></td>
				  <td class="text-right">
						<table id="right">
							<tr>
								<td style="margin-right: 20px ;">
									<i class="fa fa-paper-plane transition" id="oObj" style="color:#3498db; margin-right: 15px;"></i><b><?= "v" . BISMILLAH_APP_VERSION?></b>
								</td>
								<td>
									<i class="fa fa-circle transition" id="oObj" style="color:#3498db"></i>
								</td>
								<td >
									<b><?=$city?> (<span id="texttime"></span>)</b>
								</td>
								<td class="hover transition" id="framemenu_open">
									<i class="fa fa-th-large"></i>
								</td>
								<td class="hover transition" id="framemenu_profile">
									<i class="fa fa-tasks"></i>
								</td>
							</tr>
						</table>
				  </td>
			 </tr>
		</table>
	</div><!-- topbar -->

	<div id="footerbar" class="transition">
		<div class="dock">
		</div>
	</div><!-- footerbar -->



	<!-- sidebar menu -->
	<div class="sidebar menu transition" ng-app="framemenu" ng-controller="framemenu_controller">
		<h4><i class="ion-ios7-keypad-outline"></i>&nbsp;&nbsp;Menus</h4>
		<div class="search">
			 <input class="form-control" name="search" id="search"
			 placeholder="Search ..." ng-model="framemenu_searchstring">
		</div>
		<ul class="detailmenu">
			 <li ng-repeat="dbr in items | framemenu_searchfor:framemenu_searchstring"
			 class="item {{dbr.parent}}" ng-click="form_desktop(dbr.o)">
				  <i class="{{dbr.icon}}"></i>&nbsp;&nbsp;{{dbr.name}}
			 </li>
		</ul>
	</div>
	<!-- end menu -->
	<!-- sidebar profil -->
	<div class="sidebar profile transition">
		<div class="nav">
			 <div class="btn-group">
				  <button class="btn btn-tab active" href="#frame_t1" data-toggle="tab" >Profil</button>
				  <button class="btn btn-tab" href="#frame_t2" data-toggle="tab">Notification</button>
			 </div>
		</div>
		<div class="tab-content">
			 <div role="tabpanel" class="tab-pane fade in active" id="frame_t1">
				  <div class="me">
						<div class="wrap-img">
                             <img style="display:none;" src ='./uploads/header.jpg' height = 50px>
							 <img class="img-responsive img-thumbnail" src="<?=$data_var['ava']?>" id="file-pic">
							 <input type="file" name="file-hidden-pic" class="file-hidden" accept="image/*" id="file-hidden-pic">
						</div>
						<h4><?= $username ?></h4>
						<div class="icon-circle" style="background-color:#1abc9c" data-toggle="tooltip"
						title="Change Background">
							 <i class="ion-ios-monitor"></i>
							 <input type="file" name="bg-hidden-pic" class="file-hidden" accept="image/*" id="bg-hidden-pic">
						</div>
						<div class="icon-circle" style="background-color:#e67e22" data-toggle="tooltip"
						title="Change Password" onclick = "openmenugantipassword()">
							<i class="ion-key"></i>
						</div>
						<div class="icon-circle" style="background-color:#3498db" data-toggle="tooltip"
						title="Setting Printer" onclick = "openmenusettingprinter()">
							<i class="ion-android-print"></i>
						</div>
						<div class="icon-circle" style="background-color:#bf9408" data-toggle="tooltip"
						title="About Application" onclick = "openmenuinfo()">
							<i class="ion-android-apps"></i>
						</div>
						<div class="icon-circle" style="background-color:#cc0099" data-toggle="tooltip"
						title="Support Online" onclick = "openmenusupportonline()">
							<i class="ion-android-notifications-none"></i>
						</div>
						<div class="icon-circle" style="background-color:#000099" data-toggle="tooltip"
						title="www.mdtsolution.com" onclick = "mdtsite()">
							<i class= "ion-information" ></i>
						</div>
						<div class="icon-circle" style="background-color:#00cc66" data-toggle="tooltip"
						title="Status Proses App" onclick = "openmenustatusprocess()">
							<i class= "ion-android-sync" ></i>
						</div>
						<div class="icon-circle" style="background-color:#666666" data-toggle="tooltip"
						title="Logout" id="logout">
							 <i class="ion-ios-locked-outline"></i>
						</div>
				  </div>
				  <div class="item parent">
						<i class="ion-ios-time-outline"></i>&nbsp;&nbsp;&nbsp;Hari Ini
				  </div>
				  <div class="wrap sidebar_tanggal">
						<h3 ><?= date("l") . ",<br />" . date("d M Y") ?></h3>
				  </div>
			 </div>
			 <div role="tabpanel" class="tab-pane fade" id="frame_t2">
				  <div style="text-align:center">
						<div style="margin:auto;">
							 <ul class="notification" id="wrapnotification">
							 </ul>
						</div>
				  </div>
			 </div>
		</div>
	</div>
	<!-- end profil -->

	<script type="text/javascript" src="<?=base_url('bismillah/jQuery/jquery-2.2.3.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bismillah/jQueryUI/jquery-ui.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bismillah/angular/angular.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bismillah/bootstrap/js/bootstrap.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bismillah/select2/js/select2.full.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bismillah/w2/w2ui-1.5.rc1.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bismillah/pnotify/pnotify.custom.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bismillah/datepicker/moment.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bismillah/datepicker/bootstrap-datetimepicker.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bismillah/jQuery/jquery.number.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bismillah/chart/Chart-2.4.0.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bismillah/yearcalendar/js-year-calendar.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bismillah/yearcalendar/js-year-calendar.id.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bismillah/tinymce/js/tinymce/tinymce.min.js')?>"></script>
	<script type="text/javascript">
	<?php
		echo 'var base_url = "'.base_url().'" ;' ;
	?>
	</script>
	<?php
		require_once 'frame.menu.js.php' ;
	?>
	<script type="text/javascript" src="<?=base_url('bismillah/core.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('bismillah/main.js')?>"></script>
	<script type="text/javascript">
		var menit = 0 ;
		bjs_os.dock_init() ;
		bjs_os.frame_init() ;

		bjs.ajax("admin/frame/ping") ;
		bjs.ajax("admin/frame/ceknotif") ;
		
		setInterval(function(){
			bjs.ajax("admin/frame/ping") ;

			//notif setiap 3 menit
			if(++menit == 3){
				bjs.ajax("admin/frame/ceknotif") ;
				menit 	= 0 ;
			}
		}, 60000) ;
        
        function string_2n(nom){
            nom = nom.split(',');
            nom = nom.join('');
            nom = parseFloat(nom);
            return nom;
		}

		function datejs_2s(date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) 
				month = '0' + month;
			if (day.length < 2) 
				day = '0' + day;

			return [year, month, day].join('-');
		}
		
		function openmenugantipassword(){
			var lc = {"module":"","name":"Ganti Password","md5":"","obj":"gantipassword","loc":"admin/config/gantipassword","icon":"ion-filing","size":{"width":400,"height":250},"opt":{"title":false}};
			lc.toString();
			bjs_os.form(lc);
			//form_desktop(lc);
		}
		function openmenusettingprinter(){
			var lc = {"module":"","name":"Printer Setup","md5":"","obj":"settingprinter","loc":"admin/config/settingprinter","icon":"ion-filing","size":{"width":500,"height":300},"opt":{"title":false}};
			lc.toString();
			bjs_os.form(lc);
		}

		function openmenusupportonline(){
			var lc = {"module":"","name":"Support Online","md5":"","obj":"supportonline","loc":"admin/utl/supportonline","icon":"ion-filing","size":{"width":1200,"height":900},"opt":{"title":false}};
			lc.toString();
			bjs_os.form(lc);
		}

		function mdtsite(){
			// var lc = {"module":"","name":"Printer Setup","md5":"","obj":"settingprinter","loc":"admin/utl/utlsupportonline","icon":"ion-filing","size":{"width":1200,"height":700},"opt":{"title":false}};
			// lc.toString();
			// bjs_os.form(lc);
			var win = window.open('http://mdtsolution.com/', '_blank');
			if (win) {
				//Browser has allowed it to be opened
				win.focus();
			} else {
				//Browser has blocked it
				alert('Please allow popups for this website');
			}
		}
		

		function openmenuinfo(){
			var lc = {"module":"","name":"Info Aplikasi","md5":"","obj":"infoaplikasi","loc":"admin/config/infoaplikasi","icon":"ion-filing","size":{"width":500,"height":300},"opt":{"title":false}};
			lc.toString();
			bjs_os.form(lc);
		}

		function openmenustatusprocess(){
			var lc = {"module":"","name":"Status Process App","md5":"","obj":"utlstatusprocess","loc":"admin/utl/utlstatusprocess","icon":"ion-android-sync","size":{"width":500,"height":300},"opt":{"title":false}};
			lc.toString();
			bjs_os.form(lc);
		}

		function openmenuotpengajuandana(){
			var lc = {"module":"","name":"Otorisasi Pengajuan Dana","md5":"","obj":"trotpengajuandana","loc":"admin/tr/trotpengajuandana","icon":"ion-filing","size":{"width":1050,"height":500},"opt":{"title":false}};
			lc.toString();
			bjs_os.form(lc);
		}		
	</script>
</body>
</html>
