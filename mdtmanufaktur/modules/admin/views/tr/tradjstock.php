<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tadjstock">
                        <button class="btn btn-tab tpel active" href="#tadjstock_1" data-toggle="tab" >Daftar Penyesuaian Stock</button>
                        <button class="btn btn-tab tpel" href="#tadjstock_2" data-toggle="tab">Penyesuaian Stock</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.tradjstock.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tadjstock_1" style="padding-top:5px;">
                    <table width="100%">
                        <tr>
                            <td height="25px" width="100%">
                                <table class="osxtable form">
                                    <tr>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="5px">sd</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>

                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="400px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tadjstock_2">
                    <table width="100%">
                        <tr>
                            <td width="100%" class="osxtable form">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="faktur">Faktur</label> </td>
                                        <td width="1%">:</td>
                                        <td width="35%">
                                            <input type="text" id="faktur" name="faktur" class="form-control" placeholder="Faktur" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="tgl">Tanggal</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                                <input caption="sd" style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                               
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="gudang">Gudang</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <select name="gudang" id="gudang" class="form-control scons" style="width:100%"
                                                    data-placeholder="Gudang" data-sf="load_gudang" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="stock">Stock</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <div class="input-group">
                                                <input type="text" id="stock" name="stock" class="form-control" placeholder="Stock">
                                                <span class="input-group-btn">
                                                    <button class="form-control btn btn-info" type="button" id="cmdstock"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="namastock">Nama Stock</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input type="text" id="namastock" readonly name="namastock" class="form-control" placeholder="Nama Stock">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="satuan">Satuan</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input type="text" id="satuan" readonly name="satuan" class="form-control" placeholder="Satuan">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="keterangan">Keterangan</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Keterangan Penyesuaian">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="saldosistem">Saldo Sistem</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input readonly maxlength="20" type="text" name="saldosistem" id="saldosistem" class="form-control number" value="0">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="saldofisik">Saldo Fisik</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input maxlength="20" type="text" name="saldofisik" id="saldofisik" class="form-control number" value="0">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="selisih">Selisih</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input readonly maxlength="20" type="text" name="selisih" id="selisih" class="form-control number" value="0">
                                        </td>
                                    </tr>
                                    
                                    
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    <?=cekbosjs();?>
    //grid daftar pembelian
    bos.tradjstock.grid1_data    = null ;saldosistem
    bos.tradjstock.grid1_loaddata= function(){

        var tglawal = bos.tradjstock.obj.find("#tglawal").val();
        var tglakhir = bos.tradjstock.obj.find("#tglakhir").val();
        this.grid1_data 		= {'tglawal':tglawal,'tglakhir':tglakhir} ;
    }

    bos.tradjstock.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.tradjstock.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center",frozen:true},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'ketgudang', caption: 'Gudang', size: '100px', sortable: false,frozen:true},
                { field: 'stock', caption: 'Stock', size: '100px', sortable: false ,frozen:true},
                { field: 'namastock', caption: 'Nama Stock', size: '150px', sortable: false },
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false},
                { field: 'qtysistem', caption: 'Saldo Sistem', size: '100px',render:'float:2', sortable: false, style:"text-align:right" },
                { field: 'qtyfisik', caption: 'Saldo Fisik', size: '100px',render:'float:2', sortable: false, style:"text-align:right" },
                { field: 'qtyselisih', caption: 'Selisih', size: '100px',render:'float:2', sortable: false, style:"text-align:right" },
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.tradjstock.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.tradjstock.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.tradjstock.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.tradjstock.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.tradjstock.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.tradjstock.cmdedit 		= function(faktur){
        bjs.ajax(this.url + '/editing', 'faktur=' + faktur);
    }

    bos.tradjstock.cmddelete 	= function(faktur){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'faktur=' + faktur);
        }
    }

    bos.tradjstock.init 			= function(){

        this.obj.find("#stock").val("") ;
        this.obj.find("#namastock").val("") ;
        this.obj.find("#keterangan").val("Penyesuaian stock ") ;
        this.obj.find("#satuan").val("") ;
        this.obj.find("#saldosistem").val("0") ;
        this.obj.find("#saldofisik").val("0") ;
        this.obj.find("#selisih").val("0") ;
        this.obj.find("#gudang").sval("") ;
        this.obj.find("#stock").attr("readonly", false);
        this.obj.find("#cmdstock").attr("disabled", false);
        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getfaktur') ;
    }

    bos.tradjstock.settab 		= function(n){
        this.obj.find("#tadjstock button:eq("+n+")").tab("show") ;
    }

    bos.tradjstock.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.tradjstock.grid1_render() ;
            bos.tradjstock.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#faktur").focus() ;
        }
    }
    
    bos.tradjstock.initcomp		= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons"
        }) ;

        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getfaktur') ;
    }

    bos.tradjstock.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.tradjstock.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.tradjstock.grid1_destroy() ;
        }) ;
    }

    bos.tradjstock.hitungselisih 			= function(){
        var sistem = string_2n(this.obj.find("#saldosistem").val());
        var fisik = string_2n(this.obj.find("#saldofisik").val());
        var selisih = sistem - fisik;
        this.obj.find("#saldosistem").val($.number(sistem,2));
        this.obj.find("#saldofisik").val($.number(fisik,2));
        this.obj.find("#selisih").val($.number(selisih,2));
    }
    
    bos.tradjstock.objs = bos.tradjstock.obj.find("#cmdsave") ;
    bos.tradjstock.initfunc	   = function(){
       
        this.obj.find("#cmdstock").on("click", function(e){
            var gudang = bos.tradjstock.obj.find("#gudang").val();
            var tgl = bos.tradjstock.obj.find("#tgl").val();
            if(gudang != "" && gudang != null){
                mdl_barang.open(function(r){
                    r = JSON.parse(r);
                    with(bos.tradjstock.obj){
                        find("#stock").val(r.kode) ;
                        find("#namastock").val(r.keterangan);
                        find("#saldofisik").focus() ;
                        find("#satuan").val(r.satuan);
                        find("#saldosistem").val(r.saldo);
                    }
                    bos.tradjstock.hitungselisih();
                },{'gudang':[gudang],'tgl':tgl});
            }else{
                alert("Gudang harus dipilih terlebih dahulu!!!");
            }
            
        }) ;


        this.obj.find("#saldosistem").on("blur", function(e){
            bos.tradjstock.hitungselisih();
        });
        
        this.obj.find("#saldofisik").on("blur", function(e){
            bos.tradjstock.hitungselisih();
        });

        this.obj.find("#stock").on("blur", function(e){
            var stock = bos.tradjstock.obj.find("#stock").val();
            var gudang = bos.tradjstock.obj.find("#gudang").val();
            var tgl = bos.tradjstock.obj.find("#tgl").val();
            if(gudang != "" && gudang != null){
                var gudang = [bos.tradjstock.obj.find("#gudang").val()];

                bjs.ajax('admin/load/mdl_stock_seek', 'stock=' + stock +'&tgl=' + tgl + "&gudang="+ JSON.stringify(gudang),'',function(r){
                    r = JSON.parse(r);
                    // console.log(r.length);
                    if(r.kode != undefined){
                        with(bos.tradjstock.obj){
                            find("#stock").val(r.kode) ;
                            find("#namastock").val(r.keterangan);
                            find("#saldofisik").focus() ;
                            find("#satuan").val(r.satuan);
                            find("#saldosistem").val(r.saldo);
                        }
                        bos.tradjstock.hitungselisih();
                    }else{
                        alert("data tidak ditemukan!!, Cek data barang pada menu stok / barang");
                    }
                    
                });
            }else{
                alert("Gudang harus dipilih terlebih dahulu!!!");
            }
        });

        this.obj.find('form').on("submit", function(e){
            //this.obj.find("#cmdsave").on("click", function(e){
            e.preventDefault() ;
            if( bjs.isvalidform(this) ){
                bjs.ajax( bos.tradjstock.base_url + '/saving', bjs.getdataform(this) , bos.tradjstock.cmdsave) ;
            }

        }) ;

        

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.tradjstock.grid1_reloaddata();
        });

    }

    $(function(){
        bos.tradjstock.initcomp() ;
        bos.tradjstock.initcallback() ;
        bos.tradjstock.initfunc() ;
    });
</script>