<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tasuransikaryawan">
                        <button class="btn btn-tab tpel active" href="#tasuransikaryawan_1" data-toggle="tab" >Daftar Asuransi Karyawan</button>
                        <button class="btn btn-tab tpel" href="#tasuransikaryawan_2" data-toggle="tab">Asuransi Karyawan</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trasuransikaryawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tasuransikaryawan_1" style="padding-top:5px;">
                    <table width="100%">
                        <tr>
                            <td height="25px" width="100%">
                                <table class="osxtable form">
                                    <tr>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="5px">sd</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="400px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tasuransikaryawan_2">
                    <table width="100%">                        
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td width="100%" class="osxtable form">
                                            <table width="100%">
                                                <tr>
                                                    <td width="14%"><label for="nopendaftaran">No Pendaftaran</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input readonly type="text" id="nopendaftaran" name="nopendaftaran" class="form-control" placeholder="No Pendaftaran" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="tgl">Tanggal</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                                    </td>
                                                </tr>    
                                                <tr>
                                                    <td width="14%"><label for="noasuransi">No Asuransi</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input type="text" id="noasuransi" name="noasuransi" class="form-control" placeholder="No Asuransi" required>
                                                    </td>
                                                </tr> 
                                                <tr>
                                                    <td><label for="asuransi">Asuransi</label> </td>
                                                    <td>:</td>
                                                    <td>
                                                        <select name="asuransi" id="asuransi" class="form-control select" style="width:100%"
                                                            data-placeholder="Asuransi" required></select>
                                                    </td>
                                                </tr>                                           
                                                <tr>
                                                    <td width="14%"><label for="produk">Produk</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input type="text" id="produk" name="produk" class="form-control" placeholder="Produk / Kelas" required>
                                                    </td>
                                                </tr>
                                            <!-- </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width = "50%" valign = "top">
                                <table width="100%">
                                    <tr>
                                        <td width="100%" class="osxtable form">
                                            <table width="100%"> -->
                                                <tr>
                                                    <td width="14%"><label for="nip">NIP</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <div class="input-group">
                                                            <input readonly type="text" id="nip" name="nip" class="form-control" placeholder="NIP">
                                                            <span class="input-group-btn">
                                                                <button class="form-control btn btn-info" type="button" id="cmdnip"><i class="fa fa-search"></i></button>
                                                            </span>
                                                        </div>
                                                            
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="namakaryawan">Nama Karyawan</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input type="text" id="namakaryawan" readonly name="namakaryawan" class="form-control" placeholder="Nama Karyawan">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="jabatan">Jabatan</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input type="text" id="jabatan" readonly name="jabatan" class="form-control" placeholder="Jabatan">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="bagian">Bagian</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input type="text" id="bagian" readonly name="bagian" class="form-control" placeholder="Bagian">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
        <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<div class="modal fade" id="wrap-pencariannip-d" role="dialog" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog" style="width:900px;">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="wm-title">Daftar Karyawan</h4>
            </div>
            <div class="modal-body">
                <div id="grid2" style="height:250px"></div>
            </div>
            <div class="modal-footer">
                *Pilih Karyawan
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    bos.trasuransikaryawan.grid1_data    = null ;
    bos.trasuransikaryawan.grid1_loaddata= function(){

        var tglawal = bos.trasuransikaryawan.obj.find("#tglawal").val();
        var tglakhir = bos.trasuransikaryawan.obj.find("#tglakhir").val();
        this.grid1_data 		= {'tglawal':tglawal,'tglakhir':tglakhir} ;
    }

    bos.trasuransikaryawan.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trasuransikaryawan.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers  : true
            },
            multiSearch		: false,
            columns: [
                { field: 'nopendaftaran', caption: 'Pendaftaran', size: '120px', sortable: false, style:"text-align:center",frozen:true},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'noasuransi', caption: 'No Asuransi', size: '120px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'asuransi', caption: 'Asuransi', size: '100px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'produk', caption: 'Produk', size: '200px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'nip', caption: 'NIP', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'nama', caption: 'Nama', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'jabatan', caption: 'Jabatan', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'bagian', caption: 'Bagian', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trasuransikaryawan.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trasuransikaryawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trasuransikaryawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trasuransikaryawan.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trasuransikaryawan.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    
    //grid2
    bos.trasuransikaryawan.grid2_data    = null ;
    bos.trasuransikaryawan.grid2_loaddata= function(){
        var tgl = bos.trasuransikaryawan.obj.find("#tgl").val();
        var nopendaftaran = bos.trasuransikaryawan.obj.find("#nopendaftaran").val();
        this.grid2_data 		= {'tgl':tgl,'nopendaftaran':nopendaftaran} ;
    }

    bos.trasuransikaryawan.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            limit 	: 100 ,
            url 	: bos.trasuransikaryawan.base_url + "/loadgrid2",
            postData: this.grid2_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'nip', caption: 'NIP', size: '100px', sortable: false,frozen:true},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false ,frozen:true},
                { field: 'jabatan', caption: 'Jabatan', size: '100px', sortable: false },
                { field: 'bagian', caption: 'Bagian', size: '100px', sortable: false },
                { field: 'cmdpilih', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trasuransikaryawan.grid2_setdata	= function(){
        w2ui[this.id + '_grid2'].postData 	= this.grid2_data ;
    }
    bos.trasuransikaryawan.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.trasuransikaryawan.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trasuransikaryawan.grid2_render 	= function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.trasuransikaryawan.grid2_reloaddata	= function(){
        this.grid2_loaddata() ;
        this.grid2_setdata() ;
        this.grid2_reload() ;
    }

    bos.trasuransikaryawan.cmdpilih 		= function(nip){
        var tgl = this.obj.find("#tgl").val() ;
        bjs.ajax(this.url + '/pilihnip','nip=' + nip + '&tgl=' + tgl);
    }


    bos.trasuransikaryawan.cmdedit 		= function(nopendaftaran){
        bjs.ajax(this.url + '/editing', 'nopendaftaran=' + nopendaftaran);
    }

    bos.trasuransikaryawan.cmddelete 	= function(faktur){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'nopendaftaran=' + nopendaftaran);
        }
    }


    bos.trasuransikaryawan.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.trasuransikaryawan.init 			= function(){

        this.obj.find("#nopendaftaran").val("") ;
        this.obj.find("#noasuransi").val("") ;
        this.obj.find("#produk").val("") ;
        this.obj.find("#nip").val("") ;
        this.obj.find("#namakaryawan").val("") ;
        this.obj.find("#jabatan").val("") ;
        this.obj.find("#bagian").val("") ;
        this.obj.find("#asuransi").sval("") ;


        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getnopendaftaran') ;
    }

    bos.trasuransikaryawan.settab 		= function(n){
        this.obj.find("#tasuransikaryawan button:eq("+n+")").tab("show") ;
    }

    bos.trasuransikaryawan.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trasuransikaryawan.grid1_render() ;
            bos.trasuransikaryawan.init() ;
        }else{
            // bos.trasuransikaryawan.grid2_reload() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#faktur").focus() ;
        }
    }
    
    bos.trasuransikaryawan.initcomp		= function(){
        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;


        this.grid2_loaddata() ;
        this.grid2_load() ;

        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getnopendaftaran') ;

        this.obj.find('#asuransi').select2({
            ajax: {
                url: bos.trasuransikaryawan.base_url + '/seekasuransi',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        
    }

    bos.trasuransikaryawan.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trasuransikaryawan.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.trasuransikaryawan.grid1_destroy() ;
            bos.trasuransikaryawan.grid2_destroy() ;
        }) ;
    }

    bos.trasuransikaryawan.loadmodelnip      = function(l){
        this.obj.find("#wrap-pencariannip-d").modal(l) ;
    }

    bos.trasuransikaryawan.objs = bos.trasuransikaryawan.obj.find("#cmdsave") ;
    bos.trasuransikaryawan.initfunc	   = function(){
    
        this.obj.find('form').on("submit", function(e){
            //this.obj.find("#cmdsave").on("click", function(e){
            if(confirm("Data disimpan ??")){
                e.preventDefault() ;
                if( bjs.isvalidform(this) ){
                    bjs.ajax( bos.trasuransikaryawan.base_url + '/saving', bjs.getdataform(this) , bos.trasuransikaryawan.cmdsave) ;
                }
            }

        }) ;

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trasuransikaryawan.grid1_reloaddata();
        });

        this.obj.find("#cmdnip").on("click", function(e){
            bos.trasuransikaryawan.loadmodelnip("show");
            bos.trasuransikaryawan.grid2_reloaddata() ;
        }) ;
    }

    

    $(function(){
        bos.trasuransikaryawan.initcomp() ;
        bos.trasuransikaryawan.initcallback() ;
        bos.trasuransikaryawan.initfunc() ;
    });
</script>