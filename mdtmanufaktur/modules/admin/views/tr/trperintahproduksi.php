<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tproduksi">
                        <button class="btn btn-tab tpel active" href="#tproduksi_1" data-toggle="tab" >Daftar Perintah Produksi</button>
                        <button class="btn btn-tab tpel" href="#tproduksi_2" data-toggle="tab">Perintah Produksi</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trperintahproduksi.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tproduksi_1" style="padding-top:5px;">
                    <div class="row">
                        <div class="col-md-1">
                            <div class="form-group">
                                <label>Tgl Awal</label>
                                <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label>Tgl Akhir</label>
                                <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Cabang</label>
                                <div class="input-group">
                                    <select name="skd_cabang" id="skd_cabang" class="form-control s2" style="width:100%"
                                                            data-placeholder="Pilih Cabang" data-sf="load_cabang"></select>
                                    <span class="input-group-addon">
                                        <span id="cmdcabang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                                    </span>
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group full-width">
                                <label>&nbsp;</label>
                                <div class="input-group full-width">
                                        <button type="button" class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row" style="height: calc(100% - 50px);"> -->
                        <div id="grid1" class="full-height" style="height: calc(100% - 50px);"></div>
                    <!-- </div> -->
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tproduksi_2">
                    <table width="100%">
                        <tr>
                            <td width="100%" class="osxtable form">
                                <table width="100%">
                                    <tr>
                                        <td width="100px"><label for="faktur">Faktur</label> </td>
                                        <td width="5px">:</td>
                                        <td>
                                            <input type="text" id="faktur" name="faktur" class="form-control" placeholder="Faktur" required readonly>
                                        </td>
                                        <td width="100px"><label for="cabang">Cabang</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <select name="cabang" id="cabang" class="form-control scons" style="width:100%"
                                                    data-placeholder="Cabang" data-sf="load_cabang" required>
                                            </select>
                                        </td>

                                        <td width="100px"><label>Perbaikan</label></td>
                                        <td width="5px">:</td>
                                        <td>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="perbaikan" class="perbaikan" value="Y">
                                                    Ya
                                                </label>
                                                &nbsp;&nbsp;
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="perbaikan" class="perbaikan" value="N" checked>
                                                    Tidak
                                                </label>
                                            </div>
                                        </td>

                                        
                                    </tr>
                                    <tr>
                                        <td width="100px"><label for="tgl">Tanggal</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                             
                                        </td>

                                        <td width="100px"><label for="regu">Regu</label> </td>
                                        <td width="5px">:</td>
                                        <td>
                                            <select name="regu" id="regu" class="form-control scons" style="width:100%"
                                                    data-placeholder="Pilih Regu" data-sf="load_produksiregu" required>
                                            </select>
                                        </td>

                                        <td width="100px"><label for="gudang">Gudang</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <select name="gudang" id="gudang" class="form-control scons" style="width:100%"
                                                    data-placeholder="Gudang" data-sf="load_gudang" required>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td height = "20px" class="osxtable form">
                                <table>
                                    <tr>
                                        <td width='150px'><label for="stock">Produk</label> </td>
                                        <td><label>Nama Produk</label> </td>
                                        <td width='80px'><label for="qty">Qty</label> </td>
                                        <td><label>Satuan</label> </td>
                                        <td><label>BB</label> </td>
                                        <td><label for="btkl">BTKL</label> </td>
                                        <td><label for="bop">BOP</label> </td>
                                        <td><label>Harga Pokok</label> </td>
                                        <td><label>Jumlah</label> </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" id="stock" name="stock" class="form-control" placeholder="Produk">
                                                <span class="input-group-btn">
                                                    <button class="form-control btn btn-info" type="button" id="cmdstock"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td><input type="text" id="namastock" readonly name="namastock" class="form-control" placeholder="Nama Produk"></td>
                                        <td><input maxlength="10" type="text" name="qty" id="qty" class="form-control number" value="0"></td>
                                        <td><input type="text" id="satuan" readonly name="satuan" class="form-control" placeholder="Satuan"></td>
                                        <td><input maxlength="20" readonly type="text" name="bb" id="bb" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" type="text" name="btkl" id="btkl" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" type="text" name="bop" id="bop" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" type="text" name="hargapokok" id="hargapokok" class="form-control number" value="0"readonly></td>
                                        <td><input maxlength="20" type="text" readonly name="jumlah" id="jumlah" class="form-control number" value="0" readonly></td>

                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td height = "20px" class="osxtable form">
                                <table>
                                    <tr>
                                        <td width='50px'><label for="nomor">No</label> </td>
                                        <td width='150px'><label for="kodebb">BB</label> </td>
                                        <td><label>Nama BB</label> </td>
                                        <td width='80px'><label for="qtybb">Qty BB</label> </td>
                                        <td><label>Satuan</label> </td>
                                        <td><label for="hpbb">HP</label> </td>
                                        <td><label>Jml HP</label> </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input maxlength="5" type="text" name="nomor" id="nomor" class="form-control number" value="0"></td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" id="kodebb" name="kodebb" class="form-control" placeholder="Bahan Baku">
                                                <span class="input-group-btn">
                                                    <button class="form-control btn btn-info" type="button" id="cmdbb"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td><input type="text" id="namabb" readonly name="namabb" class="form-control" placeholder="Nama Produk"></td>
                                        <td><input maxlength="10" type="text" name="qtybb" id="qtybb" class="form-control number" value="0"></td>
                                        <td><input type="text" id="satbb" readonly name="satbb" class="form-control" placeholder="Satuan"></td>
                                        <td><input maxlength="20" type="text" name="hpbb" id="hpbb" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" type="text" name="jmlhpbb" id="jmlhpbb" class="form-control number" value="0"></td>
                                        <td><button type="button" class="btn btn-primary pull-right" id="cmdok">OK</button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "230px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    <?=cekbosjs();?>
    //grid daftar pembelian
    bos.trperintahproduksi.grid1_data    = null ;
    bos.trperintahproduksi.grid1_loaddata= function(){
        mdl_cabang.vargr.selection = bos.trperintahproduksi.obj.find('#skd_cabang').val();

        this.grid1_data 		= {
			"tglawal"	   : this.obj.find("#tglawal").val(),
            "tglakhir"	   : this.obj.find("#tglakhir").val(),
			'skd_cabang':JSON.stringify(mdl_cabang.vargr.selection)
		} ;
    }

    bos.trperintahproduksi.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trperintahproduksi.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'cabang', caption: 'Cabang', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'regu', caption: 'Regu', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'karu', caption: 'Karu', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'bb', caption: 'BB', size: '100px', sortable: false , style:"text-align:right",render:'int'},
                { field: 'btkl', caption: 'BTKL', size: '100px', sortable: false , style:"text-align:right",render:'int'},
                { field: 'bop', caption: 'BOP', size: '100px', sortable: false , style:"text-align:right",render:'int'},
                { field: 'hargapokok', caption: 'Harga Pokok', size: '100px', sortable: false , style:"text-align:right",render:'int'},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trperintahproduksi.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trperintahproduksi.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trperintahproduksi.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trperintahproduksi.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trperintahproduksi.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail perintah
    bos.trperintahproduksi.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
            multiSelect : false,
            recordHeight : 30,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers : true
            },
            columns: [
                { field: 'kodebb', caption: 'BB / BP', size: '100px', sortable: false },
                { field: 'namabb', caption: 'Nama BB / BP', size: '150px', sortable: false },
                { field: 'qtybb', caption: 'Qty BB', size: '100px', sortable: false, style:'text-align:right',
                 render: function(record){
                     if(record.recid !== "ZZZZ"){
                         return '<div style = "text-align:center">'+
                             ' <input type="number" style="width:90px" value='+record.qtybb+
                             '  onChange=bos.trperintahproduksi.grid2_hitungjumlahbb('+record.recid+',this.value,'+record.hpbb+')>'+
                             '</div>';

                     }
                 }
                },
                { field: 'satbb', caption: 'Sat BB', size: '100px', sortable: false},
                { field: 'hpbb', caption: 'HP BB', size: '100px', sortable: false, style:'text-align:right',
                 render: function(record){
                     if(record.recid !== "ZZZZ"){
                         return '<div style = "text-align:center">'+
                             ' <input type="number" style="width:90px" value='+record.hpbb+
                             '  onChange=bos.trperintahproduksi.grid2_hitungjumlahbb('+record.recid+','+record.qtybb+',this.value)>'+
                             '</div>';

                     }
                 }
                },
                { field: 'jmlhpbb', caption: 'Jml HP BB', size: '100px', sortable: false, style:'text-align:right',render:'float:2'},
                { field: 'totqtybb', caption: 'Tot Qty BB', size: '100px', sortable: false, style:'text-align:right',render:'float:2'},
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });


    }

    bos.trperintahproduksi.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trperintahproduksi.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.trperintahproduksi.grid2_append    = function(no,kodebb,namabb,qtybb,satbb,hpbb,jmlhpbb){
        var datagrid = w2ui[this.id + '_grid2'].records;
        var lnew = true;
        var nQty = 1;
        var recid = "";

        for(i=0;i<datagrid.length;i++){
            recid = datagrid[i]["recid"];
            if(kodebb == recid){
                lnew = false;
                qtybb = string_2n(qtybb) + string_2n(datagrid[i]["qtybb"]);
                hpbb = string_2n(hpbb);
                jmlhpbb = hpbb * qtybb;
                w2ui[this.id + '_grid2'].set(recid,{kodebb: kodebb, namabb: namabb,qtybb: qtybb, satbb:satbb,hpbb:hpbb,jmlhpbb:jmlhpbb});
            }
        }
        if(lnew){
            recid = kodebb;
            qtybb = string_2n(qtybb);
            hpbb = string_2n(hpbb);
            jmlhpbb = hpbb * qtybb;
            var Hapus = "<button type='button' onclick = 'bos.trperintahproduksi.grid2_deleterow("+recid+")' class='btn btn-danger btn-grid'>Delete</button>";
            w2ui[this.id + '_grid2'].add([
                { recid:recid,
                    kodebb: kodebb, namabb: namabb,qtybb: qtybb, satbb:satbb,hpbb:hpbb,jmlhpbb:jmlhpbb,cmddelete:Hapus}
            ]) ;
        }
        bos.trperintahproduksi.initdetail();
        bos.trperintahproduksi.hitungbb();
        bos.trperintahproduksi.obj.find("#kodebb").focus() ;
    }

    bos.trperintahproduksi.grid2_deleterow = function(recid){
        if(confirm("Item di hapus dari detail produksi???"+recid)){
            w2ui[this.id + '_grid2'].select(recid);
            w2ui[this.id + '_grid2'].delete(true);
        }
    }

    bos.trperintahproduksi.grid2_sumtotalpenggunaan = function(){
        var totproduk = string_2n(bos.trperintahproduksi.obj.find("#qty").val());
        var datagrid = w2ui[this.id + '_grid2'].records;
        for(i=0;i<datagrid.length;i++){
            var totpenggunaan= totproduk * datagrid[i]["qtybb"] ;
            w2ui[this.id + '_grid2'].set(datagrid[i]["recid"],{totqtybb : totpenggunaan});
        }
        bos.trperintahproduksi.obj.find("#qty").val($.number(totproduk,2))
    }
    bos.trperintahproduksi.grid2_hitungjumlahbb = function(recid,qty,hpbb){
        //alert(recid+"eer"+qty+"eefef"+harga);
        var jumlah = qty * hpbb;

        w2ui[this.id + '_grid2'].set(recid,{qtybb:qty,hpbb:hpbb,jmlhpbb:jumlah});
        bos.trperintahproduksi.hitungbb();
        bos.trperintahproduksi.grid2_sumtotalpenggunaan();
    }

    bos.trperintahproduksi.cmdedit 		= function(faktur){
        bos.trperintahproduksi.act = 1 ;
        bjs.ajax(this.url + '/editing', 'faktur=' + faktur,"",function(v){
            v = JSON.parse(v);
            console.log(v);
            // $cabang[] = array("id" => $data['cabang'], "text" => $data['ketcabang']);
            // $gudang[] = array("id" => $data['gudangperbaikan'], "text" => $data['ketgudang']);
            
            with(bos.trperintahproduksi.obj){
                find(".nav-tabs li:eq(1) a").tab("show") ;
                find("#faktur").val(v.faktur) ;
                if(v.regu !== null)find("#regu").sval([{id:v.regu.kode,text:v.regu.keterangan + " | " + v.regu.namakaru}]) ;
                find("#tgl").val(v.tgl);
                find("#cabang").sval([{id:v.cabang,text:v.ketcabang}]);
                find("#gudang").sval([{id:v.gudangperbaikan,text:v.ketgudang}]);
                find("#qty").val(v.qty);
                find("#bb").val($.number(v.bb,2));
                find("#btkl").val($.number(v.btkl,2));
                find("#bop").val($.number(v.bop,2));
                find("#hargapokok").val($.number(v.hargapokok,2));
                find("#jumlah").val($.number(v.jumlah,2));
                find("#stock").val(v.stock);
                find("#namastock").val(v.namastock);
                find("#satuan").val(v.satuan);

                find('#regu').attr('data-sp',v.cabang);
                find('#gudang').attr('data-sp',v.cabang);
            }

            $.each(v.detail, function(i, val){
                // console.log(val);
                val._act = "<button type='button' onclick = 'bos.trperintahproduksi.grid2_deleterow(" + i +")' class='btn btn-danger btn-grid'>Delete</button>";

                w2ui['bos-form-trperintahproduksi_grid2'].add([{
                     recid:i,
                     kodebb: i,
                     namabb: val.namabb,
                     qtybb: val.qtybb,
                     satbb:val.satbb,
                     hpbb:val.hpbb,
                     jmlhpbb:val.jmlhpbb,
                     cmddelete:val._act
                    }]) ;
            });

            bos.trperintahproduksi.hitungbb();
            bos.trperintahproduksi.grid2_sumtotalpenggunaan();
            bos.trperintahproduksi.setopt("perbaikan",v.perbaikan);
                
            bos.trperintahproduksi.initdetail();
            bos.trperintahproduksi.settab(1) ;
        });
    }

    bos.trperintahproduksi.cmddelete 	= function(faktur){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'faktur=' + faktur);
        }
    }
    
    bos.trperintahproduksi.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.trperintahproduksi.hitungbb 			= function(){
        var nRows = w2ui[this.id + '_grid2'].records.length;
        var subtotal = 0 ;
        for(i=0;i< nRows;i++){
            var jumlah = w2ui[this.id + '_grid2'].getCellValue(i,5);
            subtotal += jumlah;
        }

        this.obj.find("#bb").val($.number(subtotal,2));
        bos.trperintahproduksi.hitungjumlah();
    }

    bos.trperintahproduksi.init 			= function(){
        this.obj.find("#stock").val("") ;
        this.obj.find("#namastock").val("") ;
        this.obj.find("#qty").val("1") ;
        bos.trperintahproduksi.setopt("perbaikan","N");
        this.obj.find("#bb").val("0") ;
        this.obj.find("#btkl").val("0") ;
        this.obj.find("#bop").val("0") ;
        this.obj.find("#hargapokok").val("0") ;
        this.obj.find("#jumlah").val("0") ;
        this.obj.find("#satuan").val("") ;
        this.obj.find("#regu").sval("") ;
        this.obj.find("#cabang").sval("") ;
        this.obj.find("#gudang").sval("") ;

        bos.trperintahproduksi.obj.find('#regu').attr('data-sp','');


        bos.trperintahproduksi.act = 0 ;


        bjs.ajax(this.url + '/init') ;
        bos.trperintahproduksi.getfaktur();
        w2ui[this.id + '_grid2'].clear();
        bos.trperintahproduksi.initdetail() ;

    }

    bos.trperintahproduksi.settab 		= function(n){
        this.obj.find("#tproduksi button:eq("+n+")").tab("show") ;
    }

    bos.trperintahproduksi.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trperintahproduksi.grid1_render() ;
            bos.trperintahproduksi.init() ;
        }else{
            bos.trperintahproduksi.grid2_reload() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#faktur").focus() ;
        }
    }

    bos.trperintahproduksi.initdetail 			= function(){
        var datagrid = w2ui[this.id + '_grid2'].records;
        this.obj.find("#nomor").val(datagrid.length+1) ;
        this.obj.find("#kodebb").val("") ;
        this.obj.find("#namabb").val("") ;
        this.obj.find("#qtybb").val("1") ;
        this.obj.find("#satbb").val("") ;
        this.obj.find("#hpbb").val("0.00") ;
        this.obj.find("#jmlhpbb").val("0.00") ;
    }

    bos.trperintahproduksi.getfaktur = function(){
        if(bos.trperintahproduksi.act == 0){
            bos.trperintahproduksi.cabang = bos.trperintahproduksi.obj.find("#cabang").val();
            bos.trperintahproduksi.tgl = bos.trperintahproduksi.obj.find("#tgl").val();
            bjs.ajax(this.url + '/getfaktur','cabang='+bos.trperintahproduksi.cabang+"&tgl="+bos.trperintahproduksi.tgl,'',function(hasil){
                bos.trperintahproduksi.obj.find("#faktur").val(hasil);
            }) ;
        }
        
    }
    bos.trperintahproduksi.initcomp		= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons",
            clear :false
        }) ;

        bjs.initselect({
            class : "#" + this.id + " .s2",
            multi:true,
            clear:true
        }) ;

        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;
        this.grid2_load() ;

        bos.trperintahproduksi.init();

    }

    bos.trperintahproduksi.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trperintahproduksi.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.trperintahproduksi.grid1_destroy() ;
            bos.trperintahproduksi.grid2_destroy() ;
        }) ;
    }

    bos.trperintahproduksi.hitungjumlah 			= function(){

        var bb = string_2n(this.obj.find("#bb").val());
        var btkl = string_2n(this.obj.find("#btkl").val());
        var bop = string_2n(this.obj.find("#bop").val());
        //var hp = parseFloat(bb) + parseFloat(btkl) + parseFloat(bop);
        var hp = bb + btkl + bop;
        var jml = string_2n(this.obj.find("#qty").val()) * hp;


        this.obj.find("#bb").val($.number(bb,2));
        this.obj.find("#btkl").val($.number(btkl,2));
        this.obj.find("#bop").val($.number(bop,2));
        this.obj.find("#hargapokok").val($.number(hp,2));
        this.obj.find("#jumlah").val($.number(jml,2));
    }

    bos.trperintahproduksi.hitungjumlahhpbb			= function(){

        var qtybb = string_2n(this.obj.find("#qtybb").val());
        var hpbb = string_2n(this.obj.find("#hpbb").val());
        var jmlhpbb = qtybb * hpbb;


        this.obj.find("#qtybb").val($.number(qtybb,2));
        this.obj.find("#hpbb").val($.number(hpbb,2));
        this.obj.find("#jmlhpbb").val($.number(jmlhpbb,2));
    }

    bos.trperintahproduksi.objs = bos.trperintahproduksi.obj.find("#cmdsave") ;
    bos.trperintahproduksi.initfunc	   = function(){
        this.obj.find('form').on("submit", function(e){
            //this.obj.find("#cmdsave").on("click", function(e){
            e.preventDefault() ;
            if( bjs.isvalidform(this) ){
                var datagrid2 =  w2ui['bos-form-trperintahproduksi_grid2'].records;
                datagrid2 = JSON.stringify(datagrid2);
                bjs.ajax( bos.trperintahproduksi.base_url + '/saving', bjs.getdataform(this)+"&grid2="+datagrid2 , bos.trperintahproduksi.objs,function(hasil){
                    if(hasil == "ok"){
                        bos.trperintahproduksi.settab(0) ;
                    }else{
                        alert(hasil);
                    }
                }) ;
            }

        }) ;

        this.obj.find("#cmdok").on("click", function(e){
            var no = bos.trperintahproduksi.obj.find("#nomor").val();
            var kodebb = bos.trperintahproduksi.obj.find("#kodebb").val();
            var namabb = bos.trperintahproduksi.obj.find("#namabb").val();
            var qtybb = bos.trperintahproduksi.obj.find("#qtybb").val();
            var satbb = bos.trperintahproduksi.obj.find("#satbb").val();
            var hpbb = bos.trperintahproduksi.obj.find("#hpbb").val();
            var jmlhpbb = bos.trperintahproduksi.obj.find("#jmlhpbb").val();
            bos.trperintahproduksi.grid2_append(no,kodebb,namabb,qtybb,satbb,hpbb,jmlhpbb);
        }) ;

        this.obj.find("#cmdstock").on("click", function(e){
            mdl_barang.open(function(r){
                r = JSON.parse(r);
                bos.trperintahproduksi.obj.find("#stock").val(r.kode);
                bos.trperintahproduksi.obj.find("#namastock").val(r.keterangan);
                bos.trperintahproduksi.obj.find("#satuan").val(r.satuan);
                bos.trperintahproduksi.obj.find("#qty").focus();

                bjs.ajax(bos.trperintahproduksi.base_url +'/hitungproduk', 'kode=' + r.kode);

            },{'tampil':['P','S'],'orderby':'tampil asc, keterangan asc'});
        }) ;

        this.obj.find("#stock").on("blur", function(e){
            if(bos.trperintahproduksi.obj.find("#stock").val() !== ""){
                var stock = bos.trperintahproduksi.obj.find("#stock").val();
                var tampil = ['P','S'];
                var qty = 0;
                if(stock.indexOf("*") > 0){
                    res            = stock.split("*");
                    stock = res[1];
                    qty = res[0];
                }
                bjs.ajax('admin/load/mdl_stock_seek', 'stock=' + stock + "&tampil="+ JSON.stringify(tampil),'',function(r){
                        r = JSON.parse(r);
                        // console.log(r.length);
                        if(r.kode != undefined){
                            bos.trperintahproduksi.obj.find("#stock").val(r.kode);
                            bos.trperintahproduksi.obj.find("#namastock").val(r.keterangan);
                            bos.trperintahproduksi.obj.find("#qty").val(qty);
                            bos.trperintahproduksi.obj.find("#satuan").val(r.satuan);
                            
                            bjs.ajax(bos.trperintahproduksi.base_url +'/hitungproduk', 'kode=' + r.kode);

                        }else{
                            alert("data tidak ditemukan!!, Cek data barang pada menu stok / barang");
                        }
                        
                    });
            }
        });

        this.obj.find("#cmdbb").on("click", function(e){
            mdl_barang.open(function(r){
                r = JSON.parse(r);
                bos.trperintahproduksi.obj.find("#kodebb").val(r.kode);
                bos.trperintahproduksi.obj.find("#namabb").val(r.keterangan);
                bos.trperintahproduksi.obj.find("#satbb").val(r.satuan);
                bos.trperintahproduksi.obj.find("#qtybb").focus();
            },{'tampil':['B','S'],'orderby':'tampil asc, keterangan asc'});
        }) ;

        this.obj.find("#kodebb").on("blur", function(e){
            if(bos.trperintahproduksi.obj.find("#kodebb").val() !== ""){
                var stock = bos.trperintahproduksi.obj.find("#kodebb").val();
                var tampil = ['B','S'];
                var qty = 0;
                if(stock.indexOf("*") > 0){
                    res            = stock.split("*");
                    stock = res[1];
                    qty = res[0];
                }
                bjs.ajax('admin/load/mdl_stock_seek', 'stock=' + stock + "&tampil="+ JSON.stringify(tampil),'',function(r){
                        r = JSON.parse(r);
                        // console.log(r.length);
                        if(r.kode != undefined){
                            bos.trperintahproduksi.obj.find("#kodebb").val(r.kode);
                            bos.trperintahproduksi.obj.find("#namabb").val(r.keterangan);
                            bos.trperintahproduksi.obj.find("#satbb").val(r.satuan);
                            bos.trperintahproduksi.obj.find("#qtybb").val(qty);

                        }else{
                            alert("data tidak ditemukan!!, Cek data barang pada menu stok / barang");
                        }
                        
                    });
            }
        });

        this.obj.find("#qty").on("blur", function(e){
            bos.trperintahproduksi.grid2_sumtotalpenggunaan();
        });

        this.obj.find("#nomor").on("blur", function(e){
            var no = bos.trperintahproduksi.obj.find("#nomor").val();
            var datagrid = w2ui['bos-form-trperintahproduksi_grid2'].records;
            if(no <= datagrid.length){
                bos.trperintahproduksi.obj.find("#kodebb").val(w2ui['bos-form-trperintahproduksi_grid2'].getCellValue(no-1,1));
                bos.trperintahproduksi.obj.find("#namabb").val(w2ui['bos-form-trperintahproduksi_grid2'].getCellValue(no-1,2));
                bos.trperintahproduksi.obj.find("#qtybb").val(w2ui['bos-form-trperintahproduksi_grid2'].getCellValue(no-1,3));
                bos.trperintahproduksi.obj.find("#satuanbb").val(w2ui['bos-form-trperintahproduksi_grid2'].getCellValue(no-1,4));
                bos.trperintahproduksi.obj.find("#hpbb").val(w2ui['bos-form-trperintahproduksi_grid2'].getCellValue(no-1,5));
                bos.trperintahproduksi.obj.find("#jmlhpbb").val(w2ui['bos-form-trperintahproduksi_grid2'].getCellValue(no-1,6));
            }else{
                bos.trperintahproduksi.obj.find("#nomor").val(datagrid.length + 1)
            }

        });

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trperintahproduksi.grid1_reloaddata();
        });

        this.obj.find("#qtybb").on("blur", function(e){
            bos.trperintahproduksi.hitungjumlahhpbb();
        });
        this.obj.find("#hpbb").on("blur", function(e){
            bos.trperintahproduksi.hitungjumlahhpbb();
        });

        this.obj.find("#qty").on("blur", function(e){
            bos.trperintahproduksi.hitungjumlah();
        });

        this.obj.find("#bb").on("blur", function(e){
            bos.trperintahproduksi.hitungjumlah();
        });

        this.obj.find("#btkl").on("blur", function(e){
            bos.trperintahproduksi.hitungjumlah();
        });

        this.obj.find("#bop").on("blur", function(e){
            bos.trperintahproduksi.hitungjumlah();
        });

        this.obj.find("#hargapokok").on("blur", function(e){
            bos.trperintahproduksi.hitungjumlah();
        });
        this.obj.find('#cabang').on('select2:selecting', function(e){
            bos.trperintahproduksi.obj.find('#regu').attr('data-sp',e.params.args.data.id);
            bos.trperintahproduksi.obj.find('#gudang').attr('data-sp',e.params.args.data.id);
        });
        
        this.obj.find('#cabang, #tgl').on('change', function(){
            bos.trperintahproduksi.getfaktur();
        });

        this.obj.find('#cmdcabang').on('click', function(){
            mdl_cabang.vargr.selection = bos.trperintahproduksi.obj.find('#skd_cabang').val();
            mdl_cabang.open(function(r){
                r = JSON.parse(r);
                bos.trperintahproduksi.cabang = [];
                $.each(r, function(i, v){
                    bos.trperintahproduksi.cabang.push({'id':v.kode,'text':v.keterangan})                   
                });
                bos.trperintahproduksi.obj.find('#skd_cabang').sval(bos.trperintahproduksi.cabang);
            });
        });
    }

    $(function(){
        bos.trperintahproduksi.initcomp() ;
        bos.trperintahproduksi.initcallback() ;
        bos.trperintahproduksi.initfunc() ;
        bos.trperintahproduksi.initdetail();
    });
</script>