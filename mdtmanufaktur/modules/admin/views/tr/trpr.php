<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpr">
                        <button class="btn btn-tab tpel active" href="#tpr_1" data-toggle="tab" >Daftar SPP</button>
                        <button class="btn btn-tab tpel" href="#tpr_2" data-toggle="tab">SPP</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trpr.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpr_1" style="padding-top:5px;">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Antara Tanggal</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                    </div>
                                    <div class="col-md-6">
                                        <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label>Gudang</label>
                                <div class="input-group">
                                    <select name="skd_gudang" id="skd_gudang" class="form-control s2" style="width:100%"
                                                            data-placeholder="Pilih Gudang" data-sf="load_gudang"></select>
                                    <span class="input-group-addon">
                                        <span id="cmdgudang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                                    </span>
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group full-width">
                                <label>&nbsp;</label>
                                <div class="input-group full-width">
                                        <button type="button" class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="grid1" class="full-height" style="height: calc(100% - 50px);"></div>
                            
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpr_2">
                    <table width="100%">
                        <tr>
                            <td width="100%" class="osxtable form">
                                <table width = '100%'>
                                    <tr>
                                        <td width="100px"><label for="gudang">Gudang</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                             <select name="gudang" id="gudang" class="form-control scons" style="width:100%"
                                                data-placeholder="Gudang" data-sf="load_gudang" required></select>
                                        </td>
                                        <td width="100px"><label for="faktur">Faktur</label> </td>
                                        <td width="5px">:</td>
                                        <td>
                                            <input type="text" id="faktur" name="faktur" class="form-control" placeholder="Faktur" required readonly>
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="tgl">Tanggal</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                        </td>
                                        <td width="14%"></td>
                                        <td width="1%"></td>
                                        <td ></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "20px" class="osxtable form">
                                <table>
                                    <tr>
                                        <td><label for="stock">Bahan Baku</label> </td>
                                        <td><label>Nama BB</label> </td>
                                        <td><label for="qty">Qty</label> </td>
                                        <td><label>Satuan</label> </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" id="stock" name="stock" class="form-control" placeholder="Stock">
                                                <span class="input-group-btn">
                                                    <button class="form-control btn btn-info" type="button" id="cmdstock"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td><input type="text" id="namastock" readonly name="namastock" class="form-control" placeholder="Nama Stock"></td>
                                        <td><input maxlength="10" type="text" name="qty" id="qty" class="form-control number" value="0"></td>
                                        <td><input type="text" id="satuan" readonly name="satuan" class="form-control" placeholder="Satuan"></td>
                                        <td><button type="button" class="btn btn-primary pull-right" id="cmdok">OK</button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "230px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    //grid daftar po
    bos.trpr.grid1_data    = null ;
    bos.trpr.grid1_loaddata= function(){

        var tglawal = bos.trpr.obj.find("#tglawal").val();
        var tglakhir = bos.trpr.obj.find("#tglakhir").val();
        mdl_gudang.vargr.selection = bos.trpr.obj.find('#skd_gudang').val();
        
        this.grid1_data 		= {'tglawal':tglawal,'tglakhir':tglakhir,'skd_gudang':JSON.stringify(mdl_gudang.vargr.selection)} ;
    }

    bos.trpr.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trpr.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'gudang', caption: 'Gudang', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trpr.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trpr.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trpr.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trpr.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trpr.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail pembelian
    bos.trpr.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
            multiSelect : false,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers : true
            },
            columns: [
                { field: 'stock', caption: 'Kode / Barcode', size: '120px', sortable: false },
                { field: 'namastock', caption: 'Nama Stock', size: '150px', sortable: false },
                { field: 'qty',render:'float:2', caption: 'Qty', size: '70px', sortable: false,editable:{type:'float'},style:'text-align:right'},
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false},
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ],
            onChange: function(event){
                event.onComplete = function () {
                    w2ui[event.target].save();
                }
            }
        });
    }

    bos.trpr.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trpr.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.trpr.grid2_append    = function(kode,keterangan,qty,satuan){
        var datagrid  = w2ui[this.id + '_grid2'].records;
        var lnew      = true;
        var nQty      = 1;

        var recid     = kode;
        if( w2ui['bos-form-trpr_grid2'].get(recid) !== null ){
            w2ui[this.id + '_grid2'].set(recid,{stock: kode, namastock: keterangan, qty: qty, satuan:satuan});
        }else{
            var Hapus = "<button type='button' onclick = 'bos.trpr.grid2_deleterow("+recid+")' class='btn btn-danger btn-grid'>Delete</button>";
            w2ui[this.id + '_grid2'].add([
                { recid:recid,
                 stock: kode,
                 namastock: keterangan,
                 qty: qty,
                 satuan:satuan,
                 cmddelete:Hapus}
            ]) ;
        }
        bos.trpr.initdetail();
        bos.trpr.obj.find("#cmdstock").focus() ;
    }

    bos.trpr.grid2_deleterow = function(recid){
        if(confirm("Item di hapus dari detail pembelian???"+recid)){
            w2ui[this.id + '_grid2'].select(recid);
            w2ui[this.id + '_grid2'].delete(true);
            bos.trpr.hitungsubtotal();
        }
    }

    bos.trpr.cmdedit 		= function(faktur){
        bos.trpr.act = 1;
        bjs.ajax(this.url + '/editing', 'faktur=' + faktur,'',function(v){
            v = JSON.parse(v);
            w2ui["bos-form-trpr_grid2"].clear();
            with(bos.trpr.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;

               find("#faktur").val(v.faktur) ;
               find("#tgl").val(v.tgl);
               find("#gudang").sval([{id:v.gudang,text:v.ketgudang}]);

            }

            // each()
            $.each(v.detil, function(i, d){
                d.act = '<button type="button" onClick=bos.trpr.grid2_deleterow("'+i+'") class="btn btn-danger btn-grid">Delete</button>' ;
                w2ui["bos-form-trpr_grid2"].add({recid:i,stock: d.stock, namastock: d.namastock,
                                          qty:d.qty,satuan:d.satuan,
                                          cmddelete:d.act});
            });

            bos.trpr.initdetail();
            bos.trpr.settab(1) ;
        });
    }

    bos.trpr.cmddelete 	= function(faktur){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'faktur=' + faktur);
        }
    }

    bos.trpr.init 			= function(){

        this.obj.find("#gudang").sval("") ;
        bos.trpr.act = 0;



        bjs.ajax(this.url + '/init') ;
        w2ui[this.id + '_grid2'].clear();
        bos.trpr.getfaktur();
        bos.trpr.initdetail();
    }

    bos.trpr.settab 		= function(n){
        this.obj.find("#tpr button:eq("+n+")").tab("show") ;
    }

    bos.trpr.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trpr.grid1_render() ;
            bos.trpr.init() ;
        }else{
            bos.trpr.grid2_reload() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#faktur").focus() ;
        }
    }
    bos.trpr.initdetail 			= function(){
        var datagrid = w2ui[this.id + '_grid2'].records;

        this.obj.find("#nomor").val(datagrid.length+1) ;
        this.obj.find("#stock").val("") ;
        this.obj.find("#namastock").val("") ;
        this.obj.find("#qty").val("1") ;
        this.obj.find("#satuan").val("") ;
    }

    bos.trpr.objf = bos.trpr.obj.find("#faktur") ;
    bos.trpr.getfaktur = function(){
        if(bos.trpr.act == 0 ){
            bos.trpr.gudang = bos.trpr.obj.find("#gudang").val();
            bos.trpr.tgl = bos.trpr.obj.find("#tgl").val();
            bjs.ajax(this.url + '/getfaktur','gudang='+bos.trpr.gudang+"&tgl="+bos.trpr.tgl,'',function(hasil){
                bos.trpr.obj.find("#faktur").val(hasil);
            }) ;
        }        
    }
    
    bos.trpr.initcomp		= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons",
            clear : false
        }) ;

        bjs.initselect({
            class : "#" + this.id + " .s2",
            multi : true
        }) ;

        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;

        bos.trpr.init();
        
    }

    bos.trpr.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trpr.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.trpr.grid1_destroy() ;
            bos.trpr.grid2_destroy() ;
        }) ;
    }

    bos.trpr.loadmodelstock      = function(l){
        this.obj.find("#wrap-pencarianstock-d").modal(l) ;
    }

    bos.trpr.getdatastock = function(cKodeStock){
        bjs.ajax(this.url + '/getdatastock', 'cKodeStock=' + cKodeStock);
    }

    bos.trpr.objs = bos.trpr.obj.find("#cmdsave") ;
    bos.trpr.initfunc	   = function(){
        this.obj.find("#cmdok").on("click", function(e){
            var no          = bos.trpr.obj.find("#nomor").val();
            var stock       = bos.trpr.obj.find("#stock").val();
            var keterangan  = bos.trpr.obj.find("#namastock").val();
            var qty         = bos.trpr.obj.find("#qty").val();
            var satuan      = bos.trpr.obj.find("#satuan").val();
            bos.trpr.grid2_append(stock,keterangan,qty,satuan);
        }) ;

        this.obj.find("#cmdstock").on("click", function(e){
            mdl_barang.open(function(r){
                r = JSON.parse(r);
                bos.trpr.obj.find("#stock").val(r.kode);
                bos.trpr.obj.find("#namastock").val(r.keterangan);
                bos.trpr.obj.find("#qty").val("1").focus();
                bos.trpr.obj.find("#satuan").val(r.satuan);
            },{'tampil':['B','S'],'orderby':'tampil asc, keterangan asc'});
        }) ;

        this.obj.find("#stock").on("blur", function(e){
            var stock = bos.trpr.obj.find("#stock").val();
            var tampil = ['B','S'];
            bjs.ajax('admin/load/mdl_stock_seek', 'stock=' + stock + "&tampil="+ JSON.stringify(tampil),'',function(r){
                r = JSON.parse(r);
                // console.log(r.length);
                if(r.kode != undefined){
                    bos.trpr.obj.find("#stock").val(r.kode);
                    bos.trpr.obj.find("#namastock").val(r.keterangan);
                    bos.trpr.obj.find("#qty").focus();
                    bos.trpr.obj.find("#satuan").val(r.satuan);
                }else{
                    alert("data tidak ditemukan!!, Cek data barang pada menu stok / barang");
                }
                
            });
        });

        this.obj.find('form').on("submit", function(e){
            //this.obj.find("#cmdsave").on("click", function(e){
            e.preventDefault() ;
            if( bjs.isvalidform(this) ){
                var datagrid2 =  w2ui['bos-form-trpr_grid2'].records;
                datagrid2 = JSON.stringify(datagrid2);
                bjs.ajax( bos.trpr.base_url + '/saving', bjs.getdataform(this)+"&grid2="+datagrid2 , bos.trpr.cmdsave,function(hasil){
                    if(hasil == "ok"){
                        alert("Data berhasil disimpan,,");
                        bos.trpr.settab(0) ;
                    }
                }) ;
            }

        }) ;

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trpr.grid1_reloaddata();
        });

        this.obj.find('#cmdgudang').on('click', function(){
            mdl_gudang.vargr.selection = bos.trpr.obj.find('#skd_gudang').val();
            mdl_gudang.open(function(r){
                r = JSON.parse(r);
                bos.trpr.gudang = [];
                $.each(r, function(i, v){
                    bos.trpr.gudang.push({'id':v.kode,'text':v.keterangan})                   
                });
                bos.trpr.obj.find('#skd_gudang').sval(bos.trpr.gudang);
            });
        
            
        });

        this.obj.find('#gudang, #tgl').on('change', function(){
            bos.trpr.getfaktur();
        });
    }

    $(function(){
        bos.trpr.initcomp() ;
        bos.trpr.initcallback() ;
        bos.trpr.initfunc() ;
        bos.trpr.initdetail();
    });
</script>