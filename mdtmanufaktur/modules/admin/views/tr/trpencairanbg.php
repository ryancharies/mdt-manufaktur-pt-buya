<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpbgcek">
                        <button class="btn btn-tab tpel active" href="#tpbgcek_1" data-toggle="tab" >Daftar BG/CEK</button>
                        <button class="btn btn-tab tpel" href="#tpbgcek_2" data-toggle="tab">Pencairan BG/CEK</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trpencairanbg.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpbgcek_1" style="padding-top:5px;">
                    <table width="100%">
                        <tr>
                            <td height="25px" width="100%">
                                <table class="osxtable form">
                                    <tr>
                                        <td width="285px">
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" class="jenis" value="1" checked>
                                                    Belum Cair
                                                </label>
                                                &nbsp;&nbsp;
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" class="jenis" value="2">
                                                    Sudah Cair
                                                </label>
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" class="jenis" value="3">
                                                    Tidak Cair
                                                </label>
                                            </div>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="500px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpbgcek_2">
                    <table  class="osxtable form">
                        <tr>
                            <td width = "50%" style="text-align:center;"><b>:: Data BG ::</b></td>
                            <td style="text-align:center;"><b>:: Proses BG ::</b></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="bank">Bank</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input readonly type="text" maxlength="8" id="bank" name="bank" class="form-control" placeholder="Bank" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="faktur">Faktur</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input readonly type="text" maxlength="8" id="faktur" name="faktur" class="form-control" placeholder="faktur">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="kepada">Kepada</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input readonly type="text" id="kepada" name="kepada" class="form-control" placeholder="kepada" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="cabang">Cabang</label> </td>
                                        <td>:</td>
                                        <td>
                                        <input readonly type="text" id="cabang" name="cabang" class="form-control" placeholder="Cabang" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="bgcek">BG/CEK</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input readonly type="text" id="bgcek" name="bgcek" class="form-control" placeholder="BG / CEK" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="vendor">No Rekening</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input readonly type="text" id="norekening" name="norekening" class="form-control" placeholder="No Rekening" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="vendor">No BG/CEK</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input readonly type="text" id="nobgcek" name="nobgcek" class="form-control" placeholder="No BG/CEK" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="tglkeluar">Tgl Keluar</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input  readonly style="width:80px" type="text" class="form-control date" id="tglkeluar" name="tglkeluar" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td><label for="tgljthtmp">Tgl JthTmp</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input  readonly style="width:80px" type="text" class="form-control date" id="tgljthtmp" name="tgljthtmp" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td width="14%"><label for="nominal">Nominal</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input readonly style="width:200px" maxlength="20" type="text" name="nominal" id="nominal" class="form-control number" value="0">

                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="status">Status</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="status" class="status" value="1" checked>
                                                    Cair
                                                </label>
                                                &nbsp;&nbsp;
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="status" class="status" value="3">
                                                    Tidak dicairkan
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="fakturcair">Fkt Cair</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input readonly type="text" maxlength="8" id="fakturcair" name="fakturcair" class="form-control" placeholder="Faktur Cair" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="tglcair">Tgl. Proses</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input style="width:80px" type="text" class="form-control datetr" id="tglcair" name="tglcair" value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="keterangan">Keterangan</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Keterangan">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.trpencairanbg.grid1_data 	 = null ;
    bos.trpencairanbg.grid1_loaddata= function(){
        var jenis = this.obj.find('input:radio[name=jenis]:checked').val();
        
        this.grid1_data 		= {'jenis':jenis} ;
    }

    bos.trpencairanbg.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.trpencairanbg.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'no', caption: 'No', size: '50px',render:'int', sortable: false,frozen:true},
                { field: 'ketbank', caption: 'Bank', size: '100px', sortable: false,frozen:true},
                { field: 'bgcek', caption: 'BG / Cek', size: '100px', sortable: false, style:'text-align:center',frozen:true},
                { field: 'norekening', caption: 'No Rekening', size: '100px', sortable: false,frozen:true},
                { field: 'nobgcek', caption: 'No BG / Cek', size: '100px', sortable: false,frozen:true},
                { field: 'namasupplier', caption: 'Kepada', size: '100px', sortable: false,frozen:true},
                { field: 'tgl', caption: 'Tgl Keluar', size: '100px', sortable: false, style:'text-align:center'},
                { field: 'tgljthtmp', caption: 'Jatuh Tempo', size: '100px', sortable: false, style:'text-align:center'},
                { field: 'nominal', caption: 'Nominal', size: '100px',render:'float:2', sortable: false, style:'text-align:right'},
                { field: 'status', caption: 'Status', size: '100px',sortable: false, style:'text-align:center'},
                { field: 'cmdact', caption: '', size: '100px',sortable: false, style:'text-align:center'},
                { field: 'cmdreport', caption: '', size: '100px',sortable: false, style:'text-align:center'},
                { field: 'cmddotmatrix', caption: '', size: '100px',sortable: false, style:'text-align:center'},
                { field: 'keterangan', caption: 'Keterangan', size: '200px',sortable: false, style:'text-align:center'}
            ]
        });
    }

    bos.trpencairanbg.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trpencairanbg.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trpencairanbg.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trpencairanbg.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trpencairanbg.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.trpencairanbg.cmdproses		= function(kode,faktur){
        if(confirm("BG diproses?")){
            bjs.ajax(this.url + '/proses', 'nobgcek=' + kode + '&faktur=' + faktur);
        }
    }

    bos.trpencairanbg.cmdopenbg		= function(kode,faktur){
        if(confirm("Data BG dibuka?")){
            bjs.ajax(this.url + '/openbg', 'nobgcek=' + kode + '&faktur=' + faktur);
        }
    }
    
    bos.trpencairanbg.cmdbatalcair		= function(id,fkt){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'nobgcek=' + id + '&faktur=' + fkt);
        }
    }
    
    bos.trpencairanbg.cmdreport		= function(id,faktur){
        if(confirm("Cetak Bukti Pencairan?")){
            bjs.ajax(this.url + '/cetak', 'nobgcek=' + id + '&faktur=' + faktur);
        }
    }

    bos.trpencairanbg.cmddotmatrix		= function(id,faktur){
        if(confirm("Cetak Dot Matrix Bukti Pencairan?")){
            bjs.ajax(this.url + '/cetakdm', 'nobgcek=' + id + '&faktur=' + faktur);
        }
    }

    bos.trpencairanbg.openreport = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }
    bos.trpencairanbg.init				= function(){
        this.obj.find("#bank").val("") ;
		this.obj.find("#faktur").val("") ;
        this.obj.find("#kepada").val("") ;
        this.obj.find("#cabang").val("") ;
        this.obj.find("#bgcek").val("") ;
		this.obj.find("#norekening").val("") ;
		this.obj.find("#keterangan").val("") ;
        this.obj.find("#nobgcek").val("") ;
        this.obj.find("#tglkeluar").val("<?=date("d-m-Y")?>");
        this.obj.find("#tgljthtmp").val("<?=date("d-m-Y")?>");
        this.obj.find("#nominal").val("0") ;

        //proses
        this.obj.find("#fakturcair").val("") ;        
        this.obj.find("#tglcair").val("<?=date("d-m-Y")?>");
        bos.trpencairanbg.setopt("status","1");

        bjs.ajax(this.url + "/init") ;

    }

    bos.trpencairanbg.settab 		= function(n){
        this.obj.find("#tpbgcek button:eq("+n+")").tab("show") ;
    }

    bos.trpencairanbg.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trpencairanbg.grid1_render() ;
            bos.trpencairanbg.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#customer").focus() ;
        }
    }

    bos.trpencairanbg.initcomp	= function(){
        /*bjs.initselect({
            class : "#" + this.id + " .select2",
            clear : true
        }) ;*/
        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag headerreport
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.trpencairanbg.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trpencairanbg.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.trpencairanbg.grid1_destroy() ;
        }) ;
    }
    bos.trpencairanbg.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }
    
    
    bos.trpencairanbg.objs = bos.trpencairanbg.obj.find("#cmdsave") ;
    bos.trpencairanbg.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bjs.ajax( bos.trpencairanbg.url + '/saving', bjs.getdataform(this) , bos.trpencairanbg.objs) ;
            }
        });

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trpencairanbg.grid1_reloaddata();
        });
    }

    $(function(){
        bos.trpencairanbg.initcomp() ;
        bos.trpencairanbg.initcallback() ;
        bos.trpencairanbg.initfunc() ;
    }) ;
    

</script>
