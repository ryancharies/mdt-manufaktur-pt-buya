<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Posting THR</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trthr.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <table width = '100%'>
                <tr>
                    <td>
                        <table class="osxtable form">
                            <tr>
                                <td width="100px"><label for="tgl">Tgl</label> </td>
                                <td width="5px">:</td>
                                <td width="85px">
                                    <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" value=<?=date("d-m-Y")?> <?=date_set()?><?=$mintgl?>>
                                </td>
                                <td width="85px">
                                    <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                </td>
                                
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="osxtable form">
                            <tr>
                                <td width="100px"><label for="faktur">Faktur</label> </td>
                                <td width="5px">:</td>
                                <td width="200px">
                                    <input readonly placeholder="Faktur"  type="text" class="form-control" id="faktur" name="faktur">
                                </td>
                                <td width="300px">
                                    <input readonly placeholder="Status Posting"  type="text" class="form-control" id="status" name="status">
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width = '100%' height = "750px">
                            <tr>
                                <td >
                                    <div id="grid1" class="full-height">Pilih tanggal dan klik refresh!!!</div>
                                </td>
                                <td width = '3px'></td>
                                <td width = "400px" valign = "top">
                                    <table width = "400px">
                                        <tr>
                                            <td valign = "top">
                                                <table class="osxtable form">
                                                    <tr>
                                                        <td width='75px'><label for="nomor">No</label> </td>
                                                        <td width='5px'>:</td>
                                                        <td><input maxlength="5" style="width:50px;" type="text" name="nomor" id="nomor" class="form-control number" value="0"></td>
                                                    </tr>
                                                    <tr>
                                                        <td><label for="rekening">Rekening</label> </td>
                                                        <td width='5px'>:</td>
                                                        <td><select style="width:250px;" name="rekening" id="rekening" class="form-control select rekselect" style="width:100%"
                                                                    data-placeholder="Rekening"></select>  </td>
                                                    </tr>
                                                    <tr>
                                                        <td><label for="nominal">Nominal</label> </td>
                                                        <td width='5px'>:</td>
                                                        <td><input maxlength="20" type="text" name="nominal" id="nominal" class="form-control number" value="0"></td>
                                                        <td><button type="button" class="btn btn-primary pull-right" id="cmdok">OK</button></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height = "600px" >
                                                <div id="grid2" class="full-height"></div>
                                            </td>
                                        </tr>
                                    </table>

                                    
                                </td>
                            </tr>
                        </table>
                    </td>
                <tr>
                

            </table>               
        </div>
        <div class="footer fix" style="height:32px;">
            *) Pastikan data semua benar untuk kemudian diposting
            <button type="button" class="btn btn-danger pull-right" id="cmdbatalposting">Batal Posting</button>
            <button type="button" class="btn btn-primary pull-left" id="cmdposting">Posting</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.trthr.grid1_data    = null ;
    bos.trthr.grid1_loaddata= function(){

        var tgl = bos.trthr.obj.find("#tgl").val();
        this.grid1_data 		= {'tgl':tgl} ;
    }

    bos.trthr.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trthr.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: <?= $kolomgrid1 ; ?> 
        });


    }


    bos.trthr.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trthr.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trthr.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trthr.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    

    bos.trthr.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.trthr.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'no', caption: 'No', size: '50px', sortable: false},
                { field: 'kode', caption: 'Kode', size: '120px', sortable: false },
                { field: 'ketrekening', caption: 'Ket. Rekening', size: '120px', sortable: false },
                { field: 'nominal', caption: 'Nominal', size: '100px', sortable: false, render:'float:2'},
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ],
            summary: [
                { recid: "ZZZZ", no: '', kode: '', ketrekening: 'Total', nominal: 0 }
            ]
        });


    }

    bos.trthr.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trthr.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.trthr.grid2_append    = function(no,kode,ketrekening,nominal){
        var datagrid  = w2ui[this.id + '_grid2'].records;
        var lnew      = true;
        var nQty      = 1;
        var recid     = "";
        if(no <= datagrid.length){
            recid = no;
            w2ui[this.id + '_grid2'].set(recid,{kode: kode,ketrekening:ketrekening,  nominal: nominal});
        }else{
            recid = no;
            var Hapus = "<button type='button' onclick = 'bos.trthr.grid2_deleterow("+recid+")' class='btn btn-danger btn-grid'>Delete</button>";
            w2ui[this.id + '_grid2'].add([
                { recid:recid,
                 no: no,
                 kode: kode,
                 ketrekening: ketrekening,
                 nominal:nominal,
                 cmddelete:Hapus}
            ]) ;
        }
        bos.trthr.initdetail();
        bos.trthr.grid2_jumlah();
    }

    bos.trthr.grid2_deleterow = function(recid){
        if(confirm("Item di hapus dari detail DO???"+recid)){
            w2ui[this.id + '_grid2'].select(recid);
            w2ui[this.id + '_grid2'].delete(true);
            bos.trthr.grid2_urutkan();
        }
    }

    bos.trthr.grid2_urutkan = function(){
        var datagrid = w2ui[this.id + '_grid2'].records;
        w2ui[this.id + '_grid2'].clear();
        var total = 0 ;
        for(i=0;i<datagrid.length;i++){
            var no = i+1;
            datagrid[i]["recid"] = no;
            var recid = no;
            var Hapus = "<button type='button' onclick = 'bos.trthr.grid2_deleterow("+recid+")' class='btn btn-danger btn-grid'>Delete</button>";
            w2ui[this.id + '_grid2'].add({recid:recid,no: no, kode: datagrid[i]["kode"], ketrekening: datagrid[i]["ketrekening"],
                                          nominal:datagrid[i]["nominal"],cmddelete:Hapus});
            total += datagrid[i]["nominal"];
        }
        w2ui[this.id + '_grid2'].add({recid:"ZZZZ",no: "", kode: "", ketrekening: "Total",
                                          nominal:total,w2ui:{summary:true}});
        
        bos.trthr.initdetail();
    }
    
    bos.trthr.grid2_jumlah = function(){
        var datagrid = w2ui[this.id + '_grid2'].records;
        var total = 0 ;
        for(i=0;i<datagrid.length;i++){
            var nominal = w2ui[this.id + '_grid2'].getCellValue(i,3);
            total += Number(nominal);
        }
        w2ui[this.id + '_grid2'].set("ZZZZ",{nominal:total});
    }

   
    bos.trthr.init				= function(){
        bjs.ajax(this.url + "/init") ;
        bos.trthr.initdetail();
    }

    bos.trthr.initdetail 			= function(){
        var datagrid = w2ui[this.id + '_grid2'].records;

        this.obj.find("#nomor").val(datagrid.length+1) ;
        this.obj.find("#rekening").sval("") ;
        this.obj.find("#nominal").val("0") ;



    }
    
    bos.trthr.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    

    bos.trthr.initcomp	= function(){
        // bjs.initselect({
		// 	class : "#" + this.id + " .select"
		// }) ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid2_load();
    }

    bos.trthr.initcallback	= function(){


        this.obj.on("remove",function(){
            bos.trthr.grid1_destroy() ;
            bos.trthr.grid2_destroy() ;
        }) ;
    }

    bos.trthr.loadperiode	= function(){
        var tgl = this.obj.find("#tgl").val();
        bjs.ajax(this.url + '/loadperiode', 'tgl=' + tgl);
    }

    bos.trthr.loadpembayaran	= function(){
        w2ui['bos-form-trthr_grid2'].clear();
        var tgl = this.obj.find("#tgl").val();
        bjs.ajax(this.url + '/loadpembayaran', 'tgl=' + tgl);
    }
    bos.trthr.cmdposting = bos.trthr.obj.find("#cmdposting") ;
    bos.trthr.cmdbatalposting = bos.trthr.obj.find("#cmdbatalposting") ;
    bos.trthr.initfunc 		= function(){
        this.init() ;
        this.loadperiode();

        this.obj.find("#cmdrefresh").on("click",function(){
            // bos.trthr.grid1_loaddata();
            bos.trthr.loadpembayaran();
            bos.trthr.grid1_destroy() ;
            bos.trthr.grid1_loaddata();
            bos.trthr.grid1_load() ;
            
        });

        this.obj.find("#tgl").on("change",function(){
            bos.trthr.loadperiode();
        });

        this.obj.find("#cmdposting").on("click",function(){
            if(confirm("Posting THR akan di proses?? \nPastikan Data telah benar...")){
                //cek jumlah pembayaran
                
                var indxthrbersih = "";
                var nilaitotalthrbersih = "";
                if(w2ui['bos-form-trthr_grid1'] !== undefined){
                    indxthrbersih = w2ui['bos-form-trthr_grid1'].getColumn('thr', true);
                    nilaitotalthrbersih = w2ui['bos-form-trthr_grid1'].getCellValue(0,indxthrbersih,true);
                }

                var indxnominal = w2ui['bos-form-trthr_grid2'].getColumn('nominal', true);
                var nilaitotalnominal = w2ui['bos-form-trthr_grid2'].getCellValue(0,indxnominal,true);
                var error ="";

                if(nilaitotalthrbersih !== nilaitotalnominal)error += "Nilai pembayaran tidak sama dengan total THR... \n";
                if(w2ui['bos-form-trthr_grid1'] == undefined)error += "Data tidak valid... \n";
                if(error == ""){
                    var datagrid2 =  w2ui['bos-form-trthr_grid2'].records;
                    datagrid2 = JSON.stringify(datagrid2);
                    bjs.ajax( bos.trthr.base_url + '/saving', bjs.getdataform(bos.trthr.obj.find("form"))+"&grid2="+datagrid2, bos.trthr.cmdposting) ;
                }else{
                    alert(error);
                }
                
            }
        });

        this.obj.find("#cmdbatalposting").on("click",function(){
            bjs.ajax( bos.trthr.base_url + '/batalposting', bjs.getdataform(bos.trthr.obj.find("form")), bos.trthr.cmdbatalposting) ;
        });

        this.obj.find("#cmdok").on("click", function(e){

            var no          = bos.trthr.obj.find("#nomor").val();
            var kode       = bos.trthr.obj.find("#rekening").val();
            var ketrekening       = bos.trthr.obj.find("#rekening").text();
            var nominal         = bos.trthr.obj.find("#nominal").val();
            bos.trthr.grid2_append(no,kode,ketrekening,nominal);
        }) ;
    }
    

    $('.rekselect').select2({
        ajax: {
            url: bos.trthr.base_url + '/seekrekening',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    $(function(){
        bos.trthr.initcomp() ;
        bos.trthr.initcallback() ;
        bos.trthr.initfunc() ;
    }) ;
</script>
