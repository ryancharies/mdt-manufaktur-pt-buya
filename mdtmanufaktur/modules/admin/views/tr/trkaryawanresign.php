<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tkaryawanresign">
                        <button class="btn btn-tab tpel active" href="#tkaryawanresign_1" data-toggle="tab" >Daftar Karyawan</button>
                        <button class="btn btn-tab tpel" href="#tkaryawanresign_2" data-toggle="tab">Resign</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trkaryawanresign.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tkaryawanresign_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tkaryawanresign_2">
                    <table class="osxtable form">
                        <tr>
                            <td width="14%"><label for="kode">Kode</label> </td>
                            <td width="1%">:</td>
                            <td >
                                <input readonly type="text" id="kode" name="kode" class="form-control" placeholder="Kode" required>
                            </td>
                        
                            <td width="14%"><label for="nama">Nama</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input readonly type="text" id="nama" name="nama" class="form-control" placeholder="Nama">
                            </td>
                        </tr>
                        <tr>
                            <td width="14%"><label for="ktp">No KTP</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input readonly type="text" id="ktp" name="ktp" class="form-control" placeholder="No KTP">
                            </td>
                        
                            <td width="14%"><label for="notelepon">No Telp</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input readonly type="text" id="notelepon" name="notelepon" class="form-control" placeholder="notelepon">
                            </td>
                        </tr>
                        <tr>
                            <td width="14%"><label for="alamat">Alamat</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input readonly type="text" id="alamat" name="alamat" class="form-control" placeholder="alamat">
                            </td>
                        
                            <td width="14%"><label for="tgl">Tgl Masuk</label> </td>
                            <td width="1%">:</td>
                            <td >
                                <input readonly caption="sd" style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                            </td>
                        </tr>
                        <tr>
                            <td width="14%"><label for="rekening">Rekening</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input readonly type="text" id="rekening" name="rekening" class="form-control" placeholder="Rekening">
                            </td>
                        
                            <td width="14%"><label for="anrekening">An Rekening</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input readonly type="text" id="anrekening" name="anrekening" class="form-control" placeholder="Atas Nama Rekening">
                            </td>
                        </tr>
                        <tr>
                            <td width="14%"><label for="tglkeluar">Tgl Resign</label> </td>
                            <td width="1%">:</td>
                            <td >
                                <input style="width:80px" type="text" class="form-control datetr" id="tglkeluar" name="tglkeluar" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                            </td>
                            <td width="14%"><label for="ketkeluar">Ket Resign</label> </td>
                            <td width="1%">:</td>
                            <td>
                                <input type="text" id="ketkeluar" name="ketkeluar" class="form-control" placeholder="Keterangan keluar" required>
                            </td>
                        </tr>
                        <tr>
                            <td colspan = "6">
                                Foto Karyawan <span id="idlfotokaryawan"></span>
                                <table height = "200px" width = "100%">
                                    <tr><td height="180px">
                                        <div class="row" id="fotobrowse" style="height:200px;width:100%;overflow: scroll;">
                                        </div>
                                    </td></tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    *) Proses resign akan menghapus data pin karyawan
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.trkaryawanresign.grid1_data 	 = null ;
    bos.trkaryawanresign.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.trkaryawanresign.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.trkaryawanresign.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,frozen:true,style:"text-align:center;"},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false,frozen:true},
                { field: 'ktp', caption: 'No KTP', size: '100px', sortable: false,frozen:true},
                { field: 'alamat', caption: 'Alamat', size: '200px', sortable: false},
                { field: 'telepon', caption: 'Telepon', size: '100px', sortable: false},
                { field: 'tgl', caption: 'Tgl Masuk', size: '100px', sortable: false},
                { field: 'rekening', caption: 'Rekening', size: '100px', sortable: false},
                { field: 'anrekening', caption: 'AN Rekening', size: '200px', sortable: false},
                { field: 'tglkeluar', caption: 'Tgl Resign', size: '100px', sortable: false},
                { field: 'ketkeluar', caption: 'Ket Resign', size: '200px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }
    bos.trkaryawanresign.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trkaryawanresign.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trkaryawanresign.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trkaryawanresign.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trkaryawanresign.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }
    bos.trkaryawanresign.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.trkaryawanresign.cmdbatalresign		= function(id){
        if(confirm("Apakah karyawan dibatal resign?? anda perlu melakukan setting pin absensi setalah melakukan pembatalan.")){
            bjs.ajax(this.url + '/batalresign', 'kode=' + id);
        }
    }

    bos.trkaryawanresign.cmddelete		= function(id){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + id);
        }
    }

    bos.trkaryawanresign.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#nama").val("") ;
        this.obj.find("#notelepon").val("") ;
        this.obj.find("#ktp").val("") ;
        this.obj.find("#alamat").val("") ;
        this.obj.find("#rekenianrekeningng").val("") ;
        this.obj.find("#anrekening").val("") ;
        this.obj.find("#tglkeluar").val("00-00-0000") ;
        this.obj.find("#ketkeluar").val("") ;
        this.obj.find("#fotobrowse").html("");
    }

    bos.trkaryawanresign.settab 		= function(n){
        this.obj.find("#tkaryawanresign button:eq("+n+")").tab("show") ;
    }

    bos.trkaryawanresign.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trkaryawanresign.grid1_render() ;
            bos.trkaryawanresign.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#tglkeluar").focus() ;
        }
    }

    bos.trkaryawanresign.initcomp	= function(){
        /*bjs.initselect({
            class : "#" + this.id + " .select2",
            clear : true
        }) ;*/
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;        
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.trkaryawanresign.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trkaryawanresign.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.trkaryawanresign.grid1_destroy() ;
        }) ;
    }

    bos.trkaryawanresign.objs = bos.trkaryawanresign.obj.find("#cmdsave") ;
    bos.trkaryawanresign.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bjs.ajax( bos.trkaryawanresign.url + '/saving', bjs.getdataform(this), bos.trkaryawanresign.objs) ;
            }
        });
    }

    $(function(){
        bos.trkaryawanresign.initcomp() ;
        bos.trkaryawanresign.initcallback() ;
        bos.trkaryawanresign.initfunc() ;
    }) ;
</script>
