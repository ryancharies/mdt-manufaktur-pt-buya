<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tkpipenilaian">
                        <button class="btn btn-tab tpel active" href="#tkpipenilaian_1" data-toggle="tab" >Daftar Karyawan</button>
                        <button class="btn btn-tab tpel" href="#tkpipenilaian_2" data-toggle="tab">Penilaian KPI</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trkpipenilaian.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tkpipenilaian_1" >
                    <table width = "100%">                            
                        <tr>
                            <td  class="osxtable form">
                                <table>
                                    <tr>
                                        <td width="100px"><label for="periode">Periode</label> </td>
                                        <td width="5px">:</td>
                                        <td width="400px">
                                            <select name="periode" id="periode" class="form-control select" style="width:100%" data-sf="load_periodepayroll" data-placeholder="Periode"></select>
                                        </td>
                                        <td width="85px">
                                            <input readonly style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="5px">sd</td>
                                        <td width="85px">
                                            <input readonly style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        
                                        <td></td>
                                        <!-- <td width="100px"><label for="cabang">Cabang</label> </td>
                                            <td width="5px">:</td>
                                            <td width="300px">
                                                <select name="cabang" id="cabang" class="form-control select" style="width:100%" data-sf="load_cabang" data-placeholder="Cari Cabang"></select>
                                            </td> -->
                                    </tr>
                                </table>
                            </td>
                        </tr>            
                        <tr>
                            <tr>
                                <td class="osxtable form">
                                    <table>
                                        <tr>
                                            <td width="100px"><label for="jabatan">Jabatan</label> </td>
                                            <td width="5px">:</td>
                                            <td width="400px">
                                                <select name="jabatan" id="jabatan" class="form-control select" style="width:100%" data-sf="load_jabatan" data-placeholder="Cari Jabatan"></select>
                                            </td>
                                            <td></td>
                                            
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tr>
                        <tr>
                            <td height = "500px" >
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                        <tr>
                            <td  class="osxtable form">
                                <table width = '100%'>
                                    <tr>
                                        <td width="300px">
                                            <input readonly placeholder="Periode belum dipilih"  type="text" class="form-control" id="status" name="status">
                                        </td>
                                        <td>
                                        <button type="button" class="btn btn-success pull-left" id="cmdposting">Posting</button>

                                            <button type="button" class="btn btn-danger pull-left" id="cmdbatalposting">Batal Posting</button>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-primary pull-right" id="cmdcetak">Cetak</button>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>                  
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tkpipenilaian_2">
                    <table width = '100%'>
                        <tr>
                            <td>
                                <table width = '100%'>
                                    <tr>
                                        <td>
                                            <table class="osxtable form" width = '100%'>
                                                <tr>
                                                    <td width="100px"><label for="d_nip">NIP</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input type="text" class="form-control" id="d_nip" name="d_nip" readonly>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px"><label for="d_nama">Nama</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input type="text" class="form-control" id="d_nama" name="d_nama" readonly>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table class="osxtable form" width = '100%'>
                                                <tr>
                                                    <td width="100px"><label for="alamat">Alamat</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input type="text" class="form-control" id="d_alamat" name="d_alamat" readonly>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px"><label for="jabatan">Jabatan</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input type="text" class="form-control" id="d_jabatan" name="d_jabatan" readonly>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <table width = '100%'>
                                    <tr>
                                        <td  width = '50%' height = "450px">
                                            <div id="grid2" class="full-height"></div>
                                        </td>
                                    
                                        <td height = "450px">
                                            <div id="grid3" class="full-height"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="osxtable form">
                                <div class="row ">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>No Penilaian</label>
                                            <div id="no_penilaian"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Periode Penilaian</label>
                                            <div id="periode_penilaian"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nilai</label>
                                            <div  id="nilai">Nilai Kosong</div>
                                        </div>
                                    </div>                                    
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            *) Isi nilai antara 0 - 10
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    bos.trkpipenilaian.grid1_data 	 = null ;
    bos.trkpipenilaian.data = {};

    bos.trkpipenilaian.grid1_loaddata= function(){
        var tglawal = bos.trkpipenilaian.obj.find("#tglawal").val();
        var tglakhir = bos.trkpipenilaian.obj.find("#tglakhir").val();
        var periode = bos.trkpipenilaian.obj.find("#periode").val();
        var jabatan = bos.trkpipenilaian.obj.find("#jabatan").val();
        this.grid1_data 		= {'tglawal':tglawal,'tglakhir':tglakhir,'periode':periode,'jabatan':jabatan} ;
    }

    bos.trkpipenilaian.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.trkpipenilaian.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,frozen:true},
                { field: 'nama',caption: 'Nama', size: '150px', sortable: false,frozen:true},
                { field: 'jabatan',caption: 'Jabatan', size: '150px', sortable: false,frozen:true},
                { field: 'alamat',caption: 'Alamat', size: '200px', sortable: false},
                { field: 'telepon',caption: 'Telepon', size: '100px', sortable: false},                
                { field: 'faktur',caption: 'No Penilaian', size: '100px', sortable: false},
                { field: 'nilai',caption: 'Nilai', size: '100px', sortable: false},
                { field: 'peringkat',caption: 'Peringkat', size: '100px', sortable: false},
                { field: 'nominal',caption: 'Nominal', size: '100px',render:'float:2', sortable: false},
                { field: 'cmddetail', caption: ' ', size: '40px', sortable: false },
                { field: 'cmdcetak', caption: ' ', size: '40px', sortable: false }
            ]
        });
    }

    bos.trkpipenilaian.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trkpipenilaian.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trkpipenilaian.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trkpipenilaian.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trkpipenilaian.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid 2 penilaian umum
    bos.trkpipenilaian.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
            multiSelect : false,
            header  : 'Penilaian Umum',
            show: {
                header 		: true,
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'kode', caption: 'Kode', size: '80px', sortable: false ,style:'text-align:center'},
                { field: 'keterangan', caption: 'Indikator', size: '200px', sortable: false},
                { field: 'periode', caption: 'Periode', size: '100px', sortable: false ,style:'text-align:center'},
                { field: 'nilai', caption: 'Nilai', size: '100px', sortable: false,render:'float:1',editable:{type:'float'}},
                { field: 'perhitungan', caption: 'Perhitungan', size: '100px', sortable: false},
            ], 
            summary: [
                {recid:"ZZ1",kode:"",keterangan:"Jumlah",periode:"",nilai:"0.0"},
                {recid:"ZZ2",kode:"",keterangan:"Rata-rata",periode:"",nilai:"0.0"}
            ],
            onChange: function(event){
                event.onComplete = function () {
                    w2ui[event.target].save();
                    if(event.column == 3){ // maka cek nilai
                        if(w2ui[event.target].getCellValue(event.index, 4) == "Custom"){
                            if(event.value_new < 0 || event.value_new > 10){
                                alert("nilai tidak valid!!, masukkan nilai 0 - 10");
                                this.set(event.recid, { nilai : event.value_previous });
                                
                            }
                            bos.trkpipenilaian.grid2_hitungjumlah();
                        }else{
                            alert("Data tidak bisa dirubah,, karena bukan metode perhitungan custom!!");
                            this.set(event.recid, { nilai : event.value_previous });
                        }    
                    }

                    
                }
                
            }
        });


    }

    bos.trkpipenilaian.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trkpipenilaian.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.trkpipenilaian.grid2_loaddata		= function(){
        bos.trkpipenilaian.tglakhir = this.obj.find("#tglakhir").val();
        bos.trkpipenilaian.tglawal = this.obj.find("#tglawal").val();
        bos.trkpipenilaian.nip = this.obj.find("#d_nip").val();
        w2ui['bos-form-trkpipenilaian_grid2'].clear();
        bjs.ajax( bos.trkpipenilaian.base_url + '/loadgridpenilaian', "faktur="+bos.trkpipenilaian.faktur+"&tglawal="+bos.trkpipenilaian.tglawal+"&tglakhir="+bos.trkpipenilaian.tglakhir+"&nip="+bos.trkpipenilaian.nip+"&penilaian=0",'',function(data){
            bos.trkpipenilaian.gr2 = JSON.parse(data);
            $.each(bos.trkpipenilaian.gr2, function(i, v){
                w2ui["bos-form-trkpipenilaian_grid2"].add({recid:v.kode,kode:v.kode,keterangan:v.keterangan,periode:v.periode,nilai:v.nilai,perhitungan:v.perhitungan});
            });
            w2ui["bos-form-trkpipenilaian_grid2"].add({recid:"ZZ1",kode:"",keterangan:"Jumlah",periode:"",nilai:"0.0",w2ui:{summary:true}});
            w2ui["bos-form-trkpipenilaian_grid2"].add({recid:"ZZ2",kode:"",keterangan:"Rata-rata",periode:"",nilai:"0.0",w2ui:{summary:true}});
                          
            //w2ui["bos-form-trkpipenilaian_grid2"].add(' . $vare . ');,w2ui:{summary:true}

            bos.trkpipenilaian.grid2_hitungjumlah();

        }) ;
    }

    bos.trkpipenilaian.grid2_hitungjumlah = function(){
        bos.trkpipenilaian.gr2 = w2ui['bos-form-trkpipenilaian_grid2'].records;
        bos.trkpipenilaian.gr2_jml = 0;
        bos.trkpipenilaian.gr2_count = 0;
        $.each(bos.trkpipenilaian.gr2, function(i, v){
            bos.trkpipenilaian.gr2_count++;
            bos.trkpipenilaian.gr2_jml += v.nilai;
        });

        bos.trkpipenilaian.gr2_rata2 = bos.trkpipenilaian.gr2_jml / bos.trkpipenilaian.gr2_count;

        w2ui['bos-form-trkpipenilaian_grid2'].set("ZZ1", { periode:bos.trkpipenilaian.gr2_count , nilai : bos.trkpipenilaian.gr2_jml });
        w2ui['bos-form-trkpipenilaian_grid2'].set("ZZ2", {nilai : bos.trkpipenilaian.gr2_rata2 });

        bos.trkpipenilaian.hitungtotal();
    }


    //grid 3 penilaian khusus
    bos.trkpipenilaian.grid3_load    = function(){
        this.obj.find("#grid3").w2grid({
            name	: this.id + '_grid3',
            multiSelect : false,
            header  : 'Penilaian Khusus',
            show: {
                header 		: true,
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'kode', caption: 'Kode', size: '80px', sortable: false ,style:'text-align:center'},
                { field: 'keterangan', caption: 'Indikator', size: '200px', sortable: false},
                { field: 'periode', caption: 'Periode', size: '100px', sortable: false ,style:'text-align:center'},
                { field: 'nilai', caption: 'Nilai', size: '100px', sortable: false,render:'float:1',editable:{type:'float'}},
                { field: 'perhitungan', caption: 'Perhitungan', size: '100px', sortable: false ,style:'text-align:left'},
            ], 
            summary: [
                {recid:"ZZ1",kode:"",keterangan:"Jumlah",periode:"",nilai:"0.0"},
                {recid:"ZZ2",kode:"",keterangan:"Rata-rata",periode:"",nilai:"0.0"}
            ],
            onChange: function(event){
                event.onComplete = function () {
                    w2ui[event.target].save();
                    if(event.column == 3){ // maka cek nilai
                        if(w2ui[event.target].getCellValue(event.index, 4) == "Custom"){
                            if(event.value_new < 0 || event.value_new > 10){
                                alert("nilai tidak valid!!, masukkan nilai 0 - 10");
                                this.set(event.recid, { nilai : event.value_previous });
                                
                            }
                            bos.trkpipenilaian.grid3_hitungjumlah();
                        }else{
                            alert("Data tidak bisa dirubah,, karena bukan metode perhitungan custom!!");
                            this.set(event.recid, { nilai : event.value_previous });
                        }                        
                    }
                }
            }
        });


    }

    bos.trkpipenilaian.grid3_destroy 	= function(){
        if(w2ui[this.id + '_grid3'] !== undefined){
            w2ui[this.id + '_grid3'].destroy() ;
        }
    }

    bos.trkpipenilaian.grid3_reload		= function(){
        w2ui[this.id + '_grid3'].reload() ;
    }

    bos.trkpipenilaian.grid3_loaddata		= function(){
        bos.trkpipenilaian.tglakhir = this.obj.find("#tglakhir").val();
        bos.trkpipenilaian.tglawal = this.obj.find("#tglawal").val();
        bos.trkpipenilaian.nip = this.obj.find("#d_nip").val();
        w2ui['bos-form-trkpipenilaian_grid3'].clear();
        bjs.ajax( bos.trkpipenilaian.base_url + '/loadgridpenilaian', "faktur="+bos.trkpipenilaian.faktur+"&tglawal="+bos.trkpipenilaian.tglawal+"&tglakhir="+bos.trkpipenilaian.tglakhir+"&nip="+bos.trkpipenilaian.nip+"&penilaian=1",'',function(data){
            bos.trkpipenilaian.gr3 = JSON.parse(data);
            $.each(bos.trkpipenilaian.gr3, function(i, v){
                w2ui["bos-form-trkpipenilaian_grid3"].add({recid:v.kode,kode:v.kode,keterangan:v.keterangan,periode:v.periode,nilai:v.nilai,perhitungan:v.perhitungan});
            });
            w2ui["bos-form-trkpipenilaian_grid3"].add({recid:"ZZ1",kode:"",keterangan:"Jumlah",periode:"",nilai:"0.0",w2ui:{summary:true}});
            w2ui["bos-form-trkpipenilaian_grid3"].add({recid:"ZZ2",kode:"",keterangan:"Rata-rata",periode:"",nilai:"0.0",w2ui:{summary:true}});
                          
            //w2ui["bos-form-trkpipenilaian_grid3"].add(' . $vare . ');,w2ui:{summary:true}

            
            bos.trkpipenilaian.grid3_hitungjumlah();

        }) ;
    }

    bos.trkpipenilaian.grid3_hitungjumlah = function(){
        bos.trkpipenilaian.gr3 = w2ui['bos-form-trkpipenilaian_grid3'].records;
        console.log(bos.trkpipenilaian.gr3);
        bos.trkpipenilaian.gr3_jml = 0;
        bos.trkpipenilaian.gr3_count = 0;
        $.each(bos.trkpipenilaian.gr3, function(i, v){
            bos.trkpipenilaian.gr3_count++;
            bos.trkpipenilaian.gr3_jml += v.nilai;
        });

        bos.trkpipenilaian.gr3_rata2 = bos.trkpipenilaian.gr3_jml / bos.trkpipenilaian.gr3_count;

        w2ui['bos-form-trkpipenilaian_grid3'].set("ZZ1", {periode:bos.trkpipenilaian.gr3_count, nilai : bos.trkpipenilaian.gr3_jml });
        w2ui['bos-form-trkpipenilaian_grid3'].set("ZZ2", {nilai : bos.trkpipenilaian.gr3_rata2 });

        bos.trkpipenilaian.hitungtotal();
    }
    
    bos.trkpipenilaian.hitungtotal = function(){

        bos.trkpipenilaian.rata2 = (bos.trkpipenilaian.gr2_jml + bos.trkpipenilaian.gr3_jml) / (bos.trkpipenilaian.gr2_count + bos.trkpipenilaian.gr3_count);
        bos.trkpipenilaian.nilai = $.number(bos.trkpipenilaian.rata2,2);
        bos.trkpipenilaian.peringkat = " [Tidak masuk peringkat]";

        //lihat peringkat
        $.each(bos.trkpipenilaian.data['peringkat'], function(i, v){
            if(v.nilai_min <= bos.trkpipenilaian.rata2 && v.nilai_max >= bos.trkpipenilaian.rata2){
                bos.trkpipenilaian.peringkat = " ["+v.keterangan+"]";
            }
        });

        bos.trkpipenilaian.obj.find("#nilai").html(bos.trkpipenilaian.nilai + bos.trkpipenilaian.peringkat) ;
    }
    
    bos.trkpipenilaian.cmddetail		= function(kode){
        bos.trkpipenilaian.tglakhir = bos.trkpipenilaian.obj.find("#tglakhir").val();
        bos.trkpipenilaian.periode = bos.trkpipenilaian.obj.find("#periode").val();
        bos.trkpipenilaian.txtperiode = bos.trkpipenilaian.obj.find("#periode").text();
        bos.trkpipenilaian.peringkat = {};
        bos.trkpipenilaian.faktur = "";
        bjs.ajax(this.url + '/detail', 'kode=' + kode + '&tglakhir='+bos.trkpipenilaian.tglakhir + '&periode='+bos.trkpipenilaian.periode,'',function(data){
            
            bos.trkpipenilaian.data = JSON.parse(data);
            bos.trkpipenilaian.faktur = bos.trkpipenilaian.data['faktur'];

            w2ui["bos-form-trkpipenilaian_grid2"].clear();
            w2ui["bos-form-trkpipenilaian_grid3"].clear();
            with(bos.trkpipenilaian.obj){
                 find("#d_nip").val(bos.trkpipenilaian.data['kode']) ;
                 find("#d_nama").val(bos.trkpipenilaian.data['nama']) ;
                 find("#d_alamat").val(bos.trkpipenilaian.data['alamat']) ;
                 find("#d_jabatan").val(bos.trkpipenilaian.data['jabatan']) ;
                 find("#periode_penilaian").html(bos.trkpipenilaian.txtperiode) ;
                 if(bos.trkpipenilaian.data['faktur'] != null)find("#no_penilaian").html(bos.trkpipenilaian.data['faktur']) ;
            }
           

            bos.trkpipenilaian.settab(1) ;
            
        });
    }

    
    bos.trkpipenilaian.cmdcetak		= function(kode){
        bos.trkpipenilaian.tglakhir = bos.trkpipenilaian.obj.find("#tglakhir").val();
        bos.trkpipenilaian.periode = bos.trkpipenilaian.obj.find("#periode").val();

        bjs.ajax(this.base_url+ '/initreport_detil','kode=' + kode + '&tglakhir='+bos.trkpipenilaian.tglakhir + '&periode='+bos.trkpipenilaian.periode) ;
    
    }

    bos.trkpipenilaian.cmdcetak_showreport = function(){
        bjs_os.form_report(this.base_url + '/showreport_detil' ) ;
    }

    bos.trkpipenilaian.init				= function(){
        bos.trkpipenilaian.faktur = "";
        bos.trkpipenilaian.data = {};

        bjs.ajax(this.url + '/getfaktur',"","",function(hasil){
            console.log(hasil);
            bos.trkpipenilaian.obj.find("#no_penilaian").html(hasil) ;
        }) ;
        
    }
    
    bos.trkpipenilaian.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.trkpipenilaian.settab 		= function(n){
        this.obj.find("#tkpipenilaian button:eq("+n+")").tab("show") ;
    }

    bos.trkpipenilaian.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trkpipenilaian.grid1_render() ;
            bos.trkpipenilaian.init = "";
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            bos.trkpipenilaian.grid2_loaddata();
            bos.trkpipenilaian.grid3_loaddata();
        }
    }

    bos.trkpipenilaian.initcomp	= function(){
        bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.trkpipenilaian.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trkpipenilaian.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.trkpipenilaian.grid1_destroy() ;
            bos.trkpipenilaian.grid2_destroy() ;
            bos.trkpipenilaian.grid3_destroy() ;
        }) ;
    }

    bos.trkpipenilaian.loadperiode	= function(periode){
        bjs.ajax( bos.trkpipenilaian.url + '/loadperiode',"kode="+periode,'',function(data){
            data = JSON.parse(data);
            //console.log(data);
            bos.trkpipenilaian.obj.find("#tglawal").val(data['tglawal']);
            bos.trkpipenilaian.obj.find("#tglakhir").val(data['tglakhir']);
            bos.trkpipenilaian.obj.find("#periode_penilaian").html(data['keterangan']);
            if(data['status'] == "1"){
                data['status'] = "Payroll sudah posting";
                bos.trkpipenilaian.obj.find("#cmdposting").attr("disabled", true);
                bos.trkpipenilaian.obj.find("#cmdbatalposting").attr("disabled", true);
            }else if(data['status'] == "2"){
                data['status'] = "KPI sudah posting";
                bos.trkpipenilaian.obj.find("#cmdposting").attr("disabled", true);
                bos.trkpipenilaian.obj.find("#cmdbatalposting").attr("disabled", false);
            }else{
                data['status'] = "KPI belum posting";
                bos.trkpipenilaian.obj.find("#cmdposting").attr("disabled", false);
                bos.trkpipenilaian.obj.find("#cmdbatalposting").attr("disabled", true);
            }
            bos.trkpipenilaian.obj.find("#status").val(data['status']);
            bos.trkpipenilaian.grid1_destroy() ;
            bos.trkpipenilaian.grid1_loaddata() ;
            bos.trkpipenilaian.grid1_load() ;
        }) ;
    }

    bos.trkpipenilaian.objs = bos.trkpipenilaian.obj.find("#cmdsave") ;
    bos.trkpipenilaian.objp = bos.trkpipenilaian.obj.find("#cmdposting") ;
    bos.trkpipenilaian.objb = bos.trkpipenilaian.obj.find("#cmdbatalposting") ;
    bos.trkpipenilaian.initfunc 		= function(){
        this.init() ;
        

        this.grid2_load() ;

        this.grid3_load() ;

       

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bos.trkpipenilaian.error = "";
                if(bos.trkpipenilaian.gr2.length == 0 && bos.trkpipenilaian.gr3.length == 0){
                    bos.trkpipenilaian.error = "Data KPI tidak valid!!";
                }
                if(bos.trkpipenilaian.error == ""){
                    bjs.ajax( bos.trkpipenilaian.url + '/saving', 
                        bjs.getdataform(this)+"&gr2="+JSON.stringify(bos.trkpipenilaian.gr2)+"&gr3="+JSON.stringify(bos.trkpipenilaian.gr3)+"&faktur="+bos.trkpipenilaian.faktur,
                        bos.trkpipenilaian.objs,function(hasil){
                            if(hasil == "ok"){
                                alert("Penilaian berhasil disimpan !!!");
                                bos.trkpipenilaian.settab(0);
                            }else{
                                alert(hasil);
                            }
                    }) ;
                }else{
                    alert(bos.trkpipenilaian.error);
                }
            }
        });

        this.obj.find('#periode').on("select2:selecting", function(e) { 
            bos.trkpipenilaian.periode = e.params.args.data.id;
            bos.trkpipenilaian.loadperiode(bos.trkpipenilaian.periode);
        });

        this.obj.find('#jabatan').on("change", function(e) {
            bos.trkpipenilaian.grid1_destroy() ;
            bos.trkpipenilaian.grid1_loaddata() ;
            bos.trkpipenilaian.grid1_load() ;
        });

        this.obj.find('#cmdposting').on("click", function(e) {
            if(confirm("Penilaian KPI akan diposting? posting akan memproses semua data KPI pada periode yang dipilih, pastikan data anda benar")){
                bjs.ajax( bos.trkpipenilaian.url + '/posting',bjs.getdataform(bos.trkpipenilaian.obj.find("form")),bos.trkpipenilaian.objp,function(hasil){
                            if(hasil == "ok"){
                                alert("Data KPI berhasil diposting !!!");
                                bos.trkpipenilaian.loadperiode(bos.trkpipenilaian.periode);
                            }else{
                                alert(hasil);
                            }
                    }) ;
            }
        });


        this.obj.find('#cmdbatalposting').on("click", function(e) {
            if(confirm("Penilaian KPI akan dibatal posting? posting akan memproses semua data KPI pada periode yang sudah diposting, pastikan data anda benar")){
                bjs.ajax( bos.trkpipenilaian.url + '/batalposting',bjs.getdataform(bos.trkpipenilaian.obj.find("form")),bos.trkpipenilaian.objb,function(hasil){
                            if(hasil == "ok"){
                                alert("Data KPI berhasil dibatal posting !!!");
                                bos.trkpipenilaian.loadperiode(bos.trkpipenilaian.periode);
                            }else{
                                alert(hasil);
                            }
                    }) ;
            }
        });
    }

    $(function(){
        bos.trkpipenilaian.initcomp() ;
        bos.trkpipenilaian.initcallback() ;
        bos.trkpipenilaian.initfunc() ;
    }) ;
</script>
