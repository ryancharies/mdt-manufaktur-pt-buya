<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Batal Produksi Selesai</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trbatalproduksiselesai.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div>
<div class="body">
    <form novalidate>         
        <div class="bodyfix scrollme" style="height:100%"> 
            <div class="row">
                <div class="col-md-1">
                    <div class="form-group">
                        <label>Tgl Awal</label>
                        <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <label>Tgl Akhir</label>
                        <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Cabang</label>
                        <div class="input-group">
                            <select name="skd_cabang" id="skd_cabang" class="form-control s2" style="width:100%"
                                                    data-placeholder="Pilih Cabang" data-sf="load_cabang"></select>
                            <span class="input-group-addon">
                                <span id="cmdcabang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                            </span>
                        
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group full-width">
                        <label>&nbsp;</label>
                        <div class="input-group full-width">
                                <button type="button" class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="row" style="height: calc(100% - 50px);"> -->
                <div id="grid1" class="full-height" style="height: calc(100% - 50px);"></div>
            <!-- </div> -->
        </div>
        <div class="modal fade" style="position:absolute;" id="wrap-preview-detail-d" role="dialog" data-backdrop="false" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="wm-title">Preview Detail Hasil Produksi</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Faktur</label>
                                    <input type="text" name="faktur" id="faktur" class="form-control" placeholder="faktur" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Faktur Produksi</label>
                                    <input type="text" name="fakturprod" id="fakturprod" class="form-control" placeholder="faktur produksi" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tgl</label>
                                    <input type="text" name="tgl" id="tgl" readonly = true class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="grid2" style="height:250px"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Stock</label>
                                <input type="text" name="stock" id="stock" class="form-control" placeholder="Kode Stock" readonly = true>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Nama Stock</label>
                                <input type="text" name="namastock" id="namastock" class="form-control" placeholder="Nama Stock" readonly = true>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Qty</label>
                                <input maxlength="10" type="text" name="qty" id="qty" class="form-control number" value="0" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Satuan</label>
                                <input type="text" name="satuan" id="satuan" class="form-control" placeholder="Satuan" readonly = true>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>BB</label>
                                <input maxlength="10" type="text" name="bb" id="bb" class="form-control number" value="0" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>BTKL</label>
                                <input maxlength="10" type="text" name="btkl" id="btkl" class="form-control number" value="0" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>BOP</label>
                                <input maxlength="10" type="text" name="bop" id="bop" class="form-control number" value="0" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>HP</label>
                                <input maxlength="10" type="text" name="hp" id="hp" class="form-control number" value="0" readonly>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Jumlah</label>
                                <input maxlength="10" type="text" name="jumlah" id="jumlah" class="form-control number" value="0" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    <?=cekbosjs();?>


    //grid daftar pembelian
    bos.trbatalproduksiselesai.grid1_data    = null ;
    bos.trbatalproduksiselesai.grid1_loaddata= function(){
        mdl_cabang.vargr.selection = bos.trbatalproduksiselesai.obj.find('#skd_cabang').val();

        this.grid1_data 		= {
			"tglawal"	   : this.obj.find("#tglawal").val(),
            "tglakhir"	   : this.obj.find("#tglakhir").val(),
			'skd_cabang':JSON.stringify(mdl_cabang.vargr.selection)
		} ;
    }

    bos.trbatalproduksiselesai.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trbatalproduksiselesai.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'fakturproduksi', caption: 'Faktur Prod', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'regu', caption: 'Regu', size: '100px', sortable: false, style:"text-align:left"},
                { field: 'karu', caption: 'Karu', size: '100px', sortable: false, style:"text-align:left"},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'qtystd', render: 'int' ,caption: 'Qty Standart', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'std', render: 'int' ,caption: 'Nilai Standart', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'qtyaktual', render: 'int' ,caption: 'Qty Aktual', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'aktual', render: 'int' ,caption: 'Nilai Aktual', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'cmdPreview', caption: ' ', size: '120px', sortable: false },
				{ field: 'cmdBatal', caption: ' ', size: '120px', sortable: false }
            ]
        });
    }

    bos.trbatalproduksiselesai.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }

    bos.trbatalproduksiselesai.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.trbatalproduksiselesai.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trbatalproduksiselesai.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trbatalproduksiselesai.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }


    bos.trbatalproduksiselesai.cmdpreviewdetail	= function(faktur){
        bjs.ajax(this.url + '/PreviewDetail', 'faktur=' + faktur);
    }
	bos.trbatalproduksiselesai.cmdbatal	= function(faktur,fakturproduksi){
		var cnfrm = confirm("Apakah produksi akan dibatal proses??");
		if(cnfrm){
			bjs.ajax(this.url + '/deleting', 'faktur=' + faktur + '&fakturproduksi=' + fakturproduksi);
		}
	}

    bos.trbatalproduksiselesai.cmdrefresh          = bos.trbatalproduksiselesai.obj.find("#cmdrefresh") ;
    bos.trbatalproduksiselesai.cmdpreview     = bos.trbatalproduksiselesai.obj.find("#cmdpreview")
    bos.trbatalproduksiselesai.cmdCetakLaporanDetail     = bos.trbatalproduksiselesai.obj.find("#cmdCetakLaporanDetail") ;
    bos.trbatalproduksiselesai.initfunc    = function(){
        this.obj.find("#cmdpreview").on("click", function(e){
            e.preventDefault() ;
            bos.trbatalproduksiselesai.initreportTotal(0,0) ;
        }) ;

        this.obj.find("#cmdrefresh").on("click", function(e){
            e.preventDefault() ;
            bos.trbatalproduksiselesai.grid1_reloaddata() ;
        }) ;
        this.obj.find("#cmdCetakLaporanDetail").on("click",function(e){
            e.preventDefault() ;
            bos.trbatalproduksiselesai.initReportDetailPP(0,0) ;
        });

        this.obj.find('#cmdcabang').on('click', function(){
            mdl_cabang.vargr.selection = bos.trbatalproduksiselesai.obj.find('#skd_cabang').val();
            mdl_cabang.open(function(r){
                r = JSON.parse(r);
                bos.trbatalproduksiselesai.cabang = [];
                $.each(r, function(i, v){
                    bos.trbatalproduksiselesai.cabang.push({'id':v.kode,'text':v.keterangan})                   
                });
                bos.trbatalproduksiselesai.obj.find('#skd_cabang').sval(bos.trbatalproduksiselesai.cabang);
            });
        });
    }

    bos.trbatalproduksiselesai.initReportDetailPP  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.trbatalproduksiselesai.loadmodalpreview      = function(l){
        this.obj.find("#wrap-preview-detail-d").modal(l) ;
    }

    bos.trbatalproduksiselesai.initcomp		= function(){
        bjs.initselect({
            class : "#" + this.id + " .s2",
            multi:true,
            clear:true
        }) ;

        this.grid1_loaddata() ;
        this.grid1_load() ;
        this.grid2_load() ;


        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.trbatalproduksiselesai.initcallback	= function(){
        this.obj.on('remove', function(){
            bos.trbatalproduksiselesai.grid1_destroy() ;
            bos.trbatalproduksiselesai.grid2_destroy() ;
            bos.trbatalproduksiselesai.grid2_destroy() ;
        }) ;
    }

    bos.trbatalproduksiselesai.setPreview      = function(cFaktur){
        bos.trbatalproduksiselesai.initreport(0,0) ;
    }

    bos.trbatalproduksiselesai.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.trbatalproduksiselesai.initreportTotal  = function(s,e){
        bjs.ajax(this.base_url+ '/initreportTotal', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.trbatalproduksiselesai.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }

    bos.trbatalproduksiselesai.openreporttotal  = function(){
        bjs_os.form_report( this.base_url + '/showreporttotal' ) ;
    }

    bos.trbatalproduksiselesai.grid2_data    = null ;
    bos.trbatalproduksiselesai.grid2_loaddata= function(){
        this.grid2_data         = {} ;
    }


    bos.trbatalproduksiselesai.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name    : this.id + '_grid2',
            show: {
                footer      : false,
                toolbar     : false,
                toolbarColumns  : false
            },
            multiSearch     : false,
            columns: [
                { field: 'no', caption: 'No', size: '30px', sortable: false, style:'text-align:center'},
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false ,style:'text-align:center'},
                { field: 'keterangan', caption: 'Nama Stock', size: '150px', sortable: false },
                { field: 'qty', render: 'int', caption: 'Qty', size: '40px', sortable: false, style:'text-align:center'},
                { field: 'satuan', caption: 'Satuan', size: '80px', sortable: false, style:'text-align:left'},
                { field: 'hp', render: 'int', caption: 'HP', size: '100px', sortable: false, style:'text-align:right' },
                { field: 'jmlhp', render: 'int', caption: 'Jml HP', size: '100px', sortable: false, style:'text-align:right' }
            ]
        });
    }

    bos.trbatalproduksiselesai.grid2_reload     = function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.trbatalproduksiselesai.grid2_destroy    = function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trbatalproduksiselesai.grid2_render     = function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.trbatalproduksiselesai.grid2_reloaddata = function(){
        this.grid2_reload() ;
    }

    $(function(){
        bos.trbatalproduksiselesai.initcomp() ;
        bos.trbatalproduksiselesai.initcallback() ;
        bos.trbatalproduksiselesai.initfunc() ;
    });
</script>