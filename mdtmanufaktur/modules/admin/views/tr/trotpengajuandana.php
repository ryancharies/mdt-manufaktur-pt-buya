<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpengdana">
                        <button class="btn btn-tab tpel active" href="#tpengdana_1" data-toggle="tab" >Daftar Pengajuan Dana</button>
                        <button class="btn btn-tab tpel" href="#tpengdana_2" data-toggle="tab">Otorisasi</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trotpengajuandana.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpengdana_1" style="padding-top:5px;">
                    <table width="100%">
                        <tr>
                            <td height="400px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpengdana_2">
                    <table width="100%">
                        <tr>
                            <td width="100%" class="osxtable form">
                                <table width="100%">
                                    <tr>
                                        <td valign="top" width="50%">
                                            <table width="100%">
                                                <tr>
                                                    <td width="20%"><label for="faktur">Faktur</label> </td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <input readonly type="text" id="faktur" name="faktur" class="form-control" placeholder="Faktur" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%"><label for="tglpengajuan">Tanggal</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input readonly style="width:80px" type="text" class="form-control datetr" id="tglpengajuan" name="tglpengajuan" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%"><label for="jumlah">Jumlah</label> </td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <input readonly type="text" name="jumlah" id="jumlah" 
                                                            class="form-control number" value="0" required> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%"><label for="keterangan">Keterangan</label> </td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <input readonly type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%"><label for="jenis">Jenis Pengajuan</label> </td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <div class="radio-inline">
                                                            <label>
                                                                <input type="radio" name="jenis" class="jenis" value="0" checked>
                                                                Rutin
                                                            </label>
                                                            &nbsp;&nbsp;
                                                        </div>
                                                        <div class="radio-inline">
                                                            <label>
                                                                <input type="radio" name="jenis" class="jenis" value="1">
                                                                Insidentil
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%"><label for="yangmengajukan">Yang Mengajukan</label> </td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <input readonly type="text" id="yangmengajukan" name="yangmengajukan" class="form-control" placeholder="Yang Mengajukan" required>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>                            
                                        <td valign="top">
                                            <b>:: Otorisasi</b>
                                            <table width="100%">
                                                <tr>
                                                    <td width="20%"><label for="otorisasi">Otorisai</label> </td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <div class="radio-inline">
                                                            <label>
                                                                <input type="radio" name="statusot" class="statusot" value="1" checked>
                                                                Setuju
                                                            </label>
                                                            &nbsp;&nbsp;
                                                        </div>
                                                        <div class="radio-inline">
                                                            <label>
                                                                <input type="radio" name="statusot" class="statusot" value="2">
                                                                Tidak Setuju
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%"><label for="ketotorisasi">Ket Otorisasi</label> </td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <input type="text" id="ketotorisasi" name="ketotorisasi" class="form-control" placeholder="Ket Otorisasi" required>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "230px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>                                           
</div>

<script type="text/javascript">
    <?=cekbosjs();?>
    //grid daftar po
    bos.trotpengajuandana.grid1_data    = null ;
    bos.trotpengajuandana.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.trotpengajuandana.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trotpengajuandana.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center",frozen:true},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'jumlah', caption: 'Jumlah', size: '100px', sortable: false , render:'float:2'},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'jenis', caption: 'Jenis Pengajuan', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'cmdproses', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trotpengajuandana.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trotpengajuandana.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trotpengajuandana.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trotpengajuandana.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trotpengajuandana.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail
    bos.trotpengajuandana.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'no', caption: 'No', size: '50px', sortable: false},
                { field: 'username', caption: 'Username', size: '120px', sortable: false },
                { field: 'namakaryawan', caption: 'Nama', size: '120px', sortable: false },
                { field: 'jabatan', caption: 'Jabatan', size: '150px', sortable: false },
                { field: 'tahap', caption: 'Tahap', size: '100px', sortable: false},
                { field: 'kettahap', caption: 'Ket Tahap', size: '150px', sortable: false}
            ]
        });


    }

    bos.trotpengajuandana.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trotpengajuandana.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.trotpengajuandana.cmdproses 		= function(faktur){
        bjs.ajax(this.url + '/proses', 'faktur=' + faktur);
    }

    bos.trotpengajuandana.cmddelete 	= function(faktur){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'faktur=' + faktur);
        }
    }

    bos.trotpengajuandana.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.trotpengajuandana.init 			= function(){
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#faktur").val("") ;
        this.obj.find("#yangmengajukan").val("") ;
        this.obj.find("#jumlah").val("0.00") ;
        bos.trotpengajuandana.setopt("jenis","0");

        this.obj.find("#ketotorisasi").val("") ;
        bos.trotpengajuandana.setopt("statusot","1");


        bjs.ajax(this.url + '/init') ;
        w2ui[this.id + '_grid2'].clear();

    }

    bos.trotpengajuandana.settab 		= function(n){
        this.obj.find("#tpengdana button:eq("+n+")").tab("show") ;
    }

    bos.trotpengajuandana.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trotpengajuandana.grid1_render() ;
            bos.trotpengajuandana.init() ;
        }else{
            bos.trotpengajuandana.grid2_reload() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#statusot").focus() ;
        }
    }

    bos.trotpengajuandana.initcomp		= function(){
        bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;
        w2ui[this.id + '_grid2'].hideColumn('username','tahap');

        bjs.ajax(this.url + '/init') ;
    }

    bos.trotpengajuandana.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trotpengajuandana.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.trotpengajuandana.grid1_destroy() ;
            bos.trotpengajuandana.grid2_destroy() ;
        }) ;
    }

    bos.trotpengajuandana.objs = bos.trotpengajuandana.obj.find("#cmdsave") ;
    bos.trotpengajuandana.initfunc	   = function(){
        

        this.obj.find('form').on("submit", function(e){
            //this.obj.find("#cmdsave").on("click", function(e){
            e.preventDefault() ;
            if( bjs.isvalidform(this) ){
                if(confirm("Otorisasi Pengajuan akan di simpan???"))
                bjs.ajax( bos.trotpengajuandana.base_url + '/saving', bjs.getdataform(this), bos.trotpengajuandana.cmdsave) ;
            }

        }) ;

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trotpengajuandana.grid1_reloaddata();
        });
    }

    $(function(){
        bos.trotpengajuandana.initcomp() ;
        bos.trotpengajuandana.initcallback() ;
        bos.trotpengajuandana.initfunc() ;
    });
</script>