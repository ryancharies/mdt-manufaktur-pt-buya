<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tph">
                        <button class="btn btn-tab tpel active" href="#tph_1" data-toggle="tab" >Daftar Pelunasan Hutang</button>
                        <button class="btn btn-tab tpel" href="#tph_2" data-toggle="tab">Pelunasan Hutang</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trpelunasanhutang.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div><!-- end header -->
<form novalidate>
    <div class="body">
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tph_1" style="padding-top:5px;">
                    <table width="100%">
                        <tr>
                            <td height="25px" width="100%">
                                <table class="osxtable form">
                                    <tr>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="5px">sd</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="500px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tph_2">
                    <table width="100%">
                        <tr>
                            <td width="100%" class="osxtable form">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="faktur">Faktur</label> </td>
                                        <td width="1%">:</td>
                                        <td width="35%">
                                            <input type="text" readonly id="faktur" name="faktur" class="form-control" placeholder="Faktur" required>
                                        </td>
                                        <td width="14%"><label for="supplier">Supplier</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <select  name="supplier" id="supplier" class="form-control select" style="width:100%"
                                                data-placeholder="Supplier" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="tgl">Tanggal</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl"  value=<?=date("d-m-Y")?> <?=date_set()?> required <?=$mintgl?>>
                                        </td>
                                        <td width="14%"><label for="keterangan">Keterangan</label></td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Keterangan" value="Angsuran Hutang" required>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "350px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                        <tr>
                            <td height = "50px" class="osxtable form">
                                <table>
                                    <tr>
                                        <td><label>Pembelian</label> </td>
                                        <td><label>Retur</label> </td>
                                        <td><label>Subtotal</label> </td>
                                        <td><label for="prsekot">Persekot</label> </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input maxlength="20" readonly type="text" name="pembelian" id="pembelian" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" readonly type="text" name="retur" id="retur" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" readonly type="text" name="subtotal" id="subtotal" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" type="text" name="persekot" id="persekot" class="form-control number" value="0"></td>
                                        <td>
                                            <select name="kdtrpskt" id="kdtrpskt" class="form-control select" style="width:100%"
                                                data-placeholder="Kode Transaksi Persekot"></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label>BG</label></td>
                                        <td><label for="tfkas">Transfer / Kas</label> </td>
                                        <td></td>
                                        <td><label>Diskon</label> </td>
                                        <td><label>Pembulatan</label> </td>


                                    </tr>
                                    <tr>
                                        <td><div class="input-group">
                                                <input maxlength="20" readonly type="text" name="bg" id="bg" class="form-control number" value="0">
                                                <span class="input-group-btn">
                                                    <button class="form-control btn btn-info" type="button" id="cmdbg"><i class="fa fa-card">...</i></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td><input maxlength="20" type="text" name="tfkas" id="tfkas" class="form-control number" value="0"></td>
                                        <td>
                                            <select name="bankkas" id="bankkas" class="form-control select" style="width:100%"
                                                data-placeholder="Bank Kas"></select>
                                        </td>
                                        <td><input maxlength="20" readonly type="text" name="diskon" id="diskon" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" readonly type="text" name="pembulatan" id="pembulatan" class="form-control number" value="0"></td>

                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button type="button" class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
        <div class="modal fade" style="position:absolute;" id="wrap-bglist-d" role="dialog" data-backdrop="false" data-keyboard="false">
            <div class="modal-dialog" style="width:90%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="wm-title">Daftar BG</h4>
                    </div>
                    <div class="modal-body">
                        <div id="grid3" style="height:250px"></div>
                    </div>
                    <div class="modal-footer">
                        *Data BG
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
</form>

<script type="text/javascript">
    <?=cekbosjs();?>

    bos.trpelunasanhutang.dktrpersekot    = "" ;
    //grid daftar pembelian
    bos.trpelunasanhutang.grid1_data    = null ;
    bos.trpelunasanhutang.grid1_loaddata= function(){

        var tglawal = bos.trpelunasanhutang.obj.find("#tglawal").val();
        var tglakhir = bos.trpelunasanhutang.obj.find("#tglakhir").val();
        this.grid1_data 		= {'tglawal':tglawal,'tglakhir':tglakhir} ;
    }

    bos.trpelunasanhutang.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trpelunasanhutang.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center",frozen:true},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false, style:'text-align:left'},
                { field: 'supplier', caption: 'Supplier', size: '100px', sortable: false,frozen:true},
                { field: 'subtotal', caption: 'Subtotal', size: '100px',render:'float:2', sortable: false, style:"text-align:right" },
                { field: 'diskon', caption: 'Diskon', size: '100px', sortable: false,render:'float:2', style:"text-align:right"},
                { field: 'pembulatan', caption: 'Pembulatan', size: '100px', sortable: false, render:'float:2',style:"text-align:right"},
                { field: 'bgcek', caption: 'BG/CEK', size: '100px', sortable: false, render:'float:2',style:"text-align:right"},
                { field: 'persekot', caption: 'Persekot', size: '100px', sortable: false, render:'float:2',style:"text-align:right"},
                { field: 'kasbank', caption: 'Kas/Bank', size: '100px', sortable: false, render:'float:2',style:"text-align:right"},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trpelunasanhutang.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trpelunasanhutang.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trpelunasanhutang.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trpelunasanhutang.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trpelunasanhutang.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail pembelian
    bos.trpelunasanhutang.fdate = new Date();
    bos.trpelunasanhutang.date =  ("0" + bos.trpelunasanhutang.fdate.getDate()).slice(-2) + "-" +("0" + bos.trpelunasanhutang.fdate.getMonth()+1).slice(-2) + "-" +  bos.trpelunasanhutang.fdate.getFullYear();
    bos.trpelunasanhutang.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'no', caption: 'No', size: '50px', sortable: false},
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:'text-align:center'},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false, style:'text-align:center'},
                { field: 'total', caption: 'Total', size: '100px', sortable: false, style:'text-align:right'},
                { field: 'sisaawal', caption: 'Sisa Awal', size: '100px', sortable: false, style:'text-align:right'},
                { field: 'pelunasan', caption: 'Pelunasan', size: '120px', sortable: false, style:'text-align:right',
                    render: function(record){
                       if(record.no !== ""){
                            return '<div style = "text-align:center">'+
                                ' <input type="text" style="width:110px;text-align:right;" value='+$.number(record.pelunasan,"2",".",",")+
                                '  onBlur="var obj = w2ui[\''+this.name+'\'];obj.set(\''+record.recid+'\',{pelunasan:$.number(this.value,\'2\',\'.\',\',\')});bos.trpelunasanhutang.hitungsisapelunasan(\''+record.recid+'\')">'+
                                '</div>';
                       }
                    }
                },
                { field: 'sisa', caption: 'Sisa', size: '100px', sortable: false, style:'text-align:right'},
                { field: 'jenis', caption: 'Jenis', size: '100px', sortable: false},
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });


    }

    bos.trpelunasanhutang.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trpelunasanhutang.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    //grid detail bg
    bos.trpelunasanhutang.bgcek = <?php echo $bankkas; ?>;

    bos.trpelunasanhutang.bgorcek = [
        { id: 'BG', text: 'BG' },
        { id: 'CEK', text: 'CEK' }
    ];
    
    bos.trpelunasanhutang.recidgrid3 = "";
    bos.trpelunasanhutang.grid3_load    = function(){
        this.obj.find("#grid3").w2grid({
            name	: this.id + '_grid3',
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            toolbar: {
                items: [
                    { type: 'break' },
                    { type: 'button', id: 'clrrow', caption: 'Clear', icon: 'w2ui-icon-cross',disabled:true }
                ],
                onClick: function (event) {
                    event.onComplete = function() {  
                        bos.trpelunasanhutang.grid3_clearrows(bos.trpelunasanhutang.recidgrid3);
                    }
                }
            },
            columns: [
                { field: 'no', caption: 'No', size: '50px',render:'int', sortable: false},
                { field: 'bank', caption: 'Bank', size: '100px', sortable: false, style:'text-align:center',
                    editable: { type: 'select', items: [{ id: '', text: '' }].concat(bos.trpelunasanhutang.bgcek)}, 
                    render: function (record, index, col_index) {
                        var html = '';
                        for (var p in bos.trpelunasanhutang.bgcek) {
                            if (bos.trpelunasanhutang.bgcek[p].id == this.getCellValue(index, col_index)) html = bos.trpelunasanhutang.bgcek[p].text;
                        }
                        return html;
                    }
                },
                { field: 'bgcek', caption: 'BG / Cek', size: '100px', sortable: false, style:'text-align:center',
                    editable: { type: 'select', items: [{ id: '', text: '' }].concat(bos.trpelunasanhutang.bgorcek)}, 
                    render: function (record, index, col_index) {
                        var html = '';
                        for (var p in bos.trpelunasanhutang.bgorcek) {
                            if (bos.trpelunasanhutang.bgorcek[p].id == this.getCellValue(index, col_index)) html = bos.trpelunasanhutang.bgorcek[p].text;
                        }
                        return html;
                    }},
                { field: 'rekening', caption: 'No Rekening', size: '100px', sortable: false, style:'text-align:center',
                    editable: { type: 'text' }
                },
                { field: 'nobg', caption: 'No BG / Cek', size: '100px', sortable: false, style:'text-align:center',
                    editable: { type: 'text' }
                },
                { field: 'jthtmp', caption: 'Jatuh Tempo', size: '100px', sortable: false, style:'text-align:center',
                    editable: { type: 'date',format: 'dd-mm-yyyy'} 
                },
                { field: 'total', caption: 'Total', size: '100px',render:'float:2', sortable: false, style:'text-align:right',
                    editable: { type: 'text' }
                },
                { field: 'status', caption: 'Status', size: '100px',sortable: false, style:'text-align:center'}
            ],
            records: [
                { recid: 1, no: 1,bank:"",bgcek:"",rekening:"",nobg:"",status:"",total:0,jthtmp:bos.trpelunasanhutang.date},
                { recid: 2, no: 2,bank:"",bgcek:"",rekening:"",nobg:"",status:"",total:0,jthtmp:bos.trpelunasanhutang.date},
                { recid: 3, no: 3,bank:"",bgcek:"",rekening:"",nobg:"",status:"",total:0,jthtmp:bos.trpelunasanhutang.date},
                { recid: 4, no: 4,bank:"",bgcek:"",rekening:"",nobg:"",status:"",total:0,jthtmp:bos.trpelunasanhutang.date},
                { recid: 5, no: 5,bank:"",bgcek:"",rekening:"",nobg:"",status:"",total:0,jthtmp:bos.trpelunasanhutang.date},
                { recid: 6, no: 6,bank:"",bgcek:"",rekening:"",nobg:"",status:"",total:0,jthtmp:bos.trpelunasanhutang.date},
                { recid: 7, no: 7,bank:"",bgcek:"",rekening:"",nobg:"",status:"",total:0,jthtmp:bos.trpelunasanhutang.date},
                { recid: 8, no: 8,bank:"",bgcek:"",rekening:"",nobg:"",status:"",total:0,jthtmp:bos.trpelunasanhutang.date},
                { recid: 9, no: 9,bank:"",bgcek:"",rekening:"",nobg:"",status:"",total:0,jthtmp:bos.trpelunasanhutang.date},
                { recid: 10, no: 10,bank:"",bgcek:"",rekening:"",nobg:"",status:"",total:0,jthtmp:bos.trpelunasanhutang.date}
            ],
            onChange: function(event){
               // console.log(event);
                /*if(event.column == 6){
                    this.set(event.recid,{total:event.value_new});
                    
                }*/
                var stt = w2ui[event.target].getCellValue(event.index,7);
                if(stt !== ""){
                    event.preventDefault();
                }else{
                    event.onComplete = function () {
                        w2ui[event.target].save();
                        bos.trpelunasanhutang.grid3_totalbg();
                    }
                }
            },
            onSelect: function(event) {
                var stt = w2ui[event.target].getCellValue(event.recid-1,7);  
                if(stt !== ""){
                    event.preventDefault();
                    this.toolbar.disable('clrrow');
                }else{
                    event.onComplete = function() { 
                        if (event.recid > 0) {
                            this.toolbar.enable('clrrow');
                        } else {
                            this.toolbar.disable('clrrow');
                        }
                        bos.trpelunasanhutang.recidgrid3 = event.recid;   
                    }
                }
            }
        });


    }
    bos.trpelunasanhutang.grid3_clearrows = function(recid){
        w2ui[this.id + '_grid3'].set(recid,{bank:"",bgcek:"",rekening:"",nobg:"",total:0,jthtmp:bos.trpelunasanhutang.date});
    }
    bos.trpelunasanhutang.grid3_totalbg = function(){
        var nRows = w2ui[this.id + '_grid3'].records.length;
        var totbg = 0 
        for(i=0;i< nRows;i++){
            var nominal = w2ui[this.id + '_grid3'].getCellValue(i,6);
            totbg += Number(nominal);
        }
        this.obj.find("#bg").val($.number(totbg,2));
        bos.trpelunasanhutang.hitungtotal();
    }

    bos.trpelunasanhutang.grid3_destroy 	= function(){
        if(w2ui[this.id + '_grid3'] !== undefined){
            w2ui[this.id + '_grid3'].destroy() ;
        }
    }

    bos.trpelunasanhutang.grid3_reload		= function(){
        w2ui[this.id + '_grid3'].reload() ;
    }

    bos.trpelunasanhutang.hitungsubtotal 			= function(){

        var nRows = w2ui[this.id + '_grid2'].records.length;
        var pembelian = 0 ;
        var retur = 0 ;
        for(i=0;i< nRows;i++){
            var pelunasan = w2ui[this.id + '_grid2'].getCellValue(i,5);
            if(w2ui[this.id + '_grid2'].getCellValue(i,7) == "Pembelian") pembelian += string_2n(pelunasan);
            if(w2ui[this.id + '_grid2'].getCellValue(i,7) == "Retur Pembelian") retur += string_2n(pelunasan);
        }
        this.obj.find("#pembelian").val($.number(pembelian,2));
        this.obj.find("#retur").val($.number(retur,2));
        bos.trpelunasanhutang.hitungtotal();

    }

    bos.trpelunasanhutang.cmdedit 		= function(faktur){
        bjs.ajax(this.url + '/editing', 'faktur=' + faktur);
    }

    bos.trpelunasanhutang.cmddelete 	= function(faktur){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'faktur=' + faktur);
        }
    }

    bos.trpelunasanhutang.hitungsisapelunasan 		= function(recid){
        var obj = w2ui['bos-form-trpelunasanhutang_grid2'];
        var sisa = string_2n(obj.getCellValue(recid,4)) - string_2n(obj.getCellValue(recid,5));
        //alert(sisa);
        obj.set(recid,{sisa:$.number(sisa,2)});
        bos.trpelunasanhutang.hitungsubtotal();
    }


    bos.trpelunasanhutang.init 			= function(){

        this.obj.find("#subtotal").val("0") ;
        this.obj.find("#keterangan").val("Angsuran Hutang") ;
        this.obj.find("#diskon").val("0") ;
        this.obj.find("#pembulatan").val("0") ;
        this.obj.find("#tfkas").val("0") ;
        this.obj.find("#retur").val("0") ;
        this.obj.find("#tgl").val("<?=date('d-m-Y')?>") ;
        this.obj.find("#pembelian").val("0") ;
        this.obj.find("#bankkas").sval("") ;
        this.obj.find("#supplier").sval("") ;
        this.obj.find("#kdtrpskt").sval("") ;
        this.obj.find("#retur").val("0") ;
        this.obj.find("#persekot").val("0") ;
        this.obj.find("#bg").val("0") ;


        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getfaktur') ;
        w2ui[this.id + '_grid2'].clear();
        w2ui[this.id + '_grid3'].clear();

    }

    bos.trpelunasanhutang.settab 		= function(n){
        this.obj.find("#tph button:eq("+n+")").tab("show") ;
    }

    bos.trpelunasanhutang.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trpelunasanhutang.grid1_render() ;
            bos.trpelunasanhutang.init() ;
        }else{
            bos.trpelunasanhutang.grid2_reload() ;
            w2ui[this.id + '_grid3'].reset();
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#faktur").focus() ;
        }
    }
    bos.trpelunasanhutang.initcomp		= function(){
        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;
        this.grid3_load() ;


        this.obj.find('#supplier').select2({
            ajax: {
                url: bos.trpelunasanhutang.base_url + '/seeksupplier',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.obj.find('#bankkas').select2({
            ajax: {
                url: bos.trpelunasanhutang.base_url + '/seekbankkas',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.obj.find('#kdtrpskt').select2({
            ajax: {
                url: bos.trpelunasanhutang.base_url + '/seekkodetransaksi',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getfaktur') ;
    }

    bos.trpelunasanhutang.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trpelunasanhutang.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.trpelunasanhutang.grid1_destroy() ;
            bos.trpelunasanhutang.grid2_destroy() ;
            bos.trpelunasanhutang.grid3_destroy() ;
            bos.trpelunasanhutang.grid4_destroy() ;
        }) ;
    }

    bos.trpelunasanhutang.hitungtotal 			= function(){
        var subtotal = string_2n(this.obj.find("#pembelian").val()) - string_2n(this.obj.find("#retur").val());
        var kas = subtotal - string_2n(this.obj.find("#bg").val());
        if(kas <= 0) kas = 0;
        this.obj.find("#subtotal").val($.number(subtotal,2));
        this.obj.find("#tfkas").val($.number(kas,2));
        bos.trpelunasanhutang.hitungselisih();
    }

    bos.trpelunasanhutang.hitungselisih 			= function(){
        var subtotal = string_2n(this.obj.find("#pembelian").val()) - string_2n(this.obj.find("#retur").val());
        var tfkas = string_2n(this.obj.find("#tfkas").val());
        var persekot = string_2n(this.obj.find("#persekot").val());
        var bg = string_2n(this.obj.find("#bg").val());
        var txttrpersekot = bos.trpelunasanhutang.obj.find("#kdtrpskt option:selected").text();
        var jenispskt = txttrpersekot.substr(1,1);

        var selisih = subtotal - tfkas - bg;
        if(jenispskt == 'K'){
            selisih -= persekot;
        }else if(jenispskt == 'D'){
            selisih += persekot;
        }

        var diskon = 0 ;
        var pembulatan = 0 ;

        if(selisih > 0) diskon = selisih;
        if(selisih < 0) pembulatan = -1 * selisih;
        this.obj.find("#diskon").val($.number(diskon,2));
        this.obj.find("#pembulatan").val($.number(pembulatan,2));
        this.obj.find("#tfkas").val($.number(tfkas,2));
        this.obj.find("#persekot").val($.number(persekot,2));

    }

    bos.trpelunasanhutang.loadmodelbg      = function(l){
        this.obj.find("#wrap-bglist-d").modal(l) ;
        bos.trpelunasanhutang.grid3_reload() ;        
    }

    bos.trpelunasanhutang.objs = bos.trpelunasanhutang.obj.find("#cmdsave") ;
    bos.trpelunasanhutang.initfunc	   = function(){

        //this.obj.find('form').on("submit", function(e){
        this.obj.find("#cmdsave").on("click", function(e){
            var cf = confirm("Data akan disimpan ??");
            if(cf){
                if(bjs.isvalidform(bos.trpelunasanhutang.obj.find("form"))){
                //e.preventDefault() ;
                //if( bjs.isvalidform(this) ){
                    var datagrid2 =  w2ui['bos-form-trpelunasanhutang_grid2'].records;
                    var datagrid3 =  w2ui['bos-form-trpelunasanhutang_grid3'].records;
                    datagrid2 = JSON.stringify(datagrid2);
                    datagrid3 = JSON.stringify(datagrid3);
                    //bjs.ajax( bos.trpelunasanhutang.base_url + '/saving', bjs.getdataform(this)+"&grid2="+datagrid2 , bos.trpelunasanhutang.cmdsave) ;
                    bjs.ajax( bos.trpelunasanhutang.base_url + '/saving', bjs.getdataform(bos.trpelunasanhutang.obj.find("form"))+"&grid2="+datagrid2+"&grid3="+datagrid3 , bos.trpelunasanhutang.cmdsave) ;
                }
            }
        }) ;

        this.obj.find("#supplier, #tgl").on("change", function(e){
            var tgl = bos.trpelunasanhutang.obj.find("#tgl").val();
            var faktur = bos.trpelunasanhutang.obj.find("#faktur").val();
            w2ui['bos-form-trpelunasanhutang_grid2'].clear();
            //bos.trpelunasanhutang.grid2_reload() ;
            e.preventDefault() ;
            bjs.ajax( bos.trpelunasanhutang.base_url + '/loadgrid2', bjs.getdataform(this)+"&tgl="+tgl+"&faktur="+faktur) ;
        });

        this.obj.find("#kdtrpskt").on("change", function(e){
            bos.trpelunasanhutang.hitungselisih();
        });

        this.obj.find("#persekot").on("blur", function(e){
            bos.trpelunasanhutang.hitungselisih();
        });

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trpelunasanhutang.grid1_reloaddata();
        });

        this.obj.find("#tfkas").on("blur", function(e){
            bos.trpelunasanhutang.hitungselisih();
        });

        this.obj.find("#cmdbg").on("click", function(e){
            bos.trpelunasanhutang.loadmodelbg("show");
        }) ;
    }

    

    $(function(){
        bos.trpelunasanhutang.initcomp() ;
        bos.trpelunasanhutang.initcallback() ;
        bos.trpelunasanhutang.initfunc() ;
    });
</script>