<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpja">
                        <button class="btn btn-tab tpel active" href="#tpja_1" data-toggle="tab" >Daftar Aset & Inventaris</button>
                        <button class="btn btn-tab tpel" href="#tpja_2" data-toggle="tab">Penj. Aset & Inventaris</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trpenjualanaset.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpja_1" style="padding-top:5px;">
                    <table width="100%">
                        <tr>
                            <td height="25px" width="100%">
                                <table class="osxtable form">
                                    <tr>
                                        <td width="285px">
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="status" class="status" value="1" checked>
                                                    Belum dijual
                                                </label>
                                                &nbsp;&nbsp;
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="status" class="status" value="2">
                                                    Sudah dijual
                                                </label>
                                            </div>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="500px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpja_2">
                    <table  class="osxtable form">
                        <tr>
                            <td width = "50%" style="text-align:center;"><b>:: Data Aset ::</b></td>
                            <td style="text-align:center;"><b>:: Data Penjualan ::</b></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="kode">Kode</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input readonly type="text" maxlength="8" id="kode" name="kode" class="form-control" placeholder="kode" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="faktur">Fkt PB</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input readonly type="text" maxlength="8" id="fktpb" name="fktpb" class="form-control" placeholder="faktur">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="keterangan">Keterangan</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input readonly type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="cabang">Cabang</label> </td>
                                        <td>:</td>
                                        <td>
                                        <input readonly type="text" id="cabang" name="cabang" class="form-control" placeholder="Cabang" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="golaset">Gol. Aset</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input readonly type="text" id="golaset" name="golaset" class="form-control" placeholder="Golongan Aset" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="vendor">Vendor</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input readonly type="text" id="vendor" name="vendor" class="form-control" placeholder="Vendor" >
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="tglperolehan">Tgl. Perolehan</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input  readonly style="width:80px" type="text" class="form-control date" id="tglperolehan" name="tglperolehan" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td width="14%"><label for="lama">Lama (Tahun)</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input  readonly style="width:50px" maxlength="5" type="text" name="lama" id="lama" class="form-control number" value="0">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="hp">Harga Perolehan</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input readonly style="width:200px" maxlength="20" type="text" name="hp" id="hp" class="form-control number" value="0">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="unit">Unit</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input readonly style="width:50px" maxlength="7" type="text" name="unit" id="unit" class="form-control number" value="0" required>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="jenis">Jenis Peny</label> </td>
                                        <td width="1%">:</td>
                                        <td width="50%">
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" class="jenis" value="1" checked>
                                                    Garis Lurus
                                                </label>
                                                &nbsp;&nbsp;
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" class="jenis" value="2">
                                                    Saldo Menurun
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="tarifpeny">Tarif Peny (%)</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input readonly  style="width:50px" maxlength="7" type="text" name="tarifpeny" id="tarifpeny" class="form-control number" value="0">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="residu">Nilai Residu</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input readonly  style="width:200px" maxlength="20" type="text" name="residu" id="residu" class="form-control number" value="0">

                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="faktur">Fkt PJ</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input readonly type="text" maxlength="8" id="fktpj" name="fktpj" class="form-control" placeholder="faktur" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><label for="tglpj">Tgl. PJ</label> </td>
                                        <td>:</td>
                                        <td>
                                            <input style="width:80px" type="text" class="form-control datetr" id="tglpj" name="tglpj" value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td><label for="customer">Customer</label> </td>
                                        <td>:</td>
                                        <td>
                                            <select name="customer" id="customer" class="form-control select" style="width:100%"
                                                    data-placeholder="Customer" required></select>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <td width="14%"><label for="nilaibuku">Nilai Buku</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input  readonly style="width:200px" maxlength="20" type="text" name="nilaibuku" id="nilaibuku" class="form-control number" value="0" required>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="hargajual">Harga Jual</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input  style="width:200px" maxlength="20" type="text" name="hargajual" id="hargajual" class="form-control number" value="0" required>

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.trpenjualanaset.grid1_data 	 = null ;
    bos.trpenjualanaset.grid1_loaddata= function(){
        var status = this.obj.find('input:radio[name=status]:checked').val();
        
        this.grid1_data 		= {'status':status} ;
    }

    bos.trpenjualanaset.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.trpenjualanaset.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '120px', sortable: false,frozen:true},
                { field: 'keterangan', caption: 'Keterangan', size: '150px', sortable: false,frozen:true},
                { field: 'cabang', caption: 'Cabang', size: '80px', sortable: false},
                { field: 'golongan', caption: 'Gol. Aset', size: '100px', sortable: false},
                { field: 'tglperolehan', caption: 'Tgl Perolehan', size: '80px', sortable: false},                
				{ field: 'fakturpj', caption: 'Faktur Jual', size: '120px', sortable: false},
                { field: 'tglpj', caption: 'Tgl Jual', size: '80px', sortable: false},
                { field: 'customer', caption: 'Customer', size: '100px', sortable: false},
                { field: 'lama', caption: 'Lama', size: '80px', sortable: false,render:"float:2"},
                { field: 'hargaperolehan', caption: 'Harga Perolehan', size: '100px', sortable: false,render:"float:2"},
                { field: 'hargajual', caption: 'Harga Jual', size: '100px', sortable: false,render:"float:2"},
                { field: 'nilaibukujual', caption: 'Nilai Buku', size: '100px', sortable: false,render:"float:2"},
                { field: 'cmdact', caption: ' ', size: '80px', sortable: false },
                { field: 'cmdreport', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trpenjualanaset.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trpenjualanaset.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trpenjualanaset.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trpenjualanaset.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trpenjualanaset.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.trpenjualanaset.cmdjual		= function(kode){
        bjs.ajax(this.url + '/jual', 'kode=' + kode);
    }

    bos.trpenjualanaset.cmdbataljual		= function(id,fkt){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'kode=' + id + '&faktur=' + fkt);
        }
    }

    bos.trpenjualanaset.cmdreport		= function(id){
        if(confirm("Cetak Penjualan?")){
            bjs.ajax(this.url + '/cetak', 'kode=' + id );
        }
    }
    bos.trpenjualanaset.openreport = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }
    bos.trpenjualanaset.init				= function(){
        this.obj.find("#kode").val("") ;
		this.obj.find("#fktpb").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#cabang").val("") ;
        this.obj.find("#golaset").val("") ;
		this.obj.find("#aset").val("") ;
        this.obj.find("#tglperolehan").val("<?=date("d-m-Y")?>");
        this.obj.find("#lama").val("0") ;
        this.obj.find("#hp").val("0") ;
        this.obj.find("#unit").val("1") ;
        //this.obj.find("#jenis").val("1") ;
        this.obj.find("#tarifpeny").val("0") ;
        this.obj.find("#residu").val("0") ;

        //penjualan
        this.obj.find("#fktpj").val("") ;        
        this.obj.find("#tglpj").val("<?=date("d-m-Y")?>");
        this.obj.find("#customer").sval("") ;
        this.obj.find("#nilaibuku").val("0") ;
        this.obj.find("#hargajual").val("0") ;
        
        bjs.ajax(this.url + "/init") ;

    }

    bos.trpenjualanaset.settab 		= function(n){
        this.obj.find("#tpja button:eq("+n+")").tab("show") ;
    }

    bos.trpenjualanaset.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trpenjualanaset.grid1_render() ;
            bos.trpenjualanaset.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#customer").focus() ;
        }
    }

    bos.trpenjualanaset.initcomp	= function(){
        /*bjs.initselect({
            class : "#" + this.id + " .select2",
            clear : true
        }) ;*/
        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.trpenjualanaset.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trpenjualanaset.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.trpenjualanaset.grid1_destroy() ;
        }) ;
    }
    bos.trpenjualanaset.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }
    
    bos.trpenjualanaset.seekaset = function(){
        var kode = this.obj.find('#kode').val();
        var tglpj = this.obj.find('#tglpj').val();
        bjs.ajax(this.url + '/seekaset', 'kode=' + kode + '&tglpj=' + tglpj);
    }
    bos.trpenjualanaset.objs = bos.trpenjualanaset.obj.find("#cmdsave") ;
    bos.trpenjualanaset.initfunc 		= function(){
        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bjs.ajax( bos.trpenjualanaset.url + '/saving', bjs.getdataform(this) , bos.trpenjualanaset.objs) ;
            }
        });

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trpenjualanaset.grid1_reloaddata();
        });

        this.obj.find("#tglpj").on("change", function(e){
            bos.trpenjualanaset.seekaset();
        });

        this.obj.find("#hargajual").on("blur", function(e){
            var nilai = Number(this.value);
            this.value = $.number(nilai,2);
        });
    }

	
	$('#customer').select2({
        ajax: {
            url: bos.trpenjualanaset.base_url + '/seekcustomer',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    $(function(){
        bos.trpenjualanaset.initcomp() ;
        bos.trpenjualanaset.initcallback() ;
        bos.trpenjualanaset.initfunc() ;
    }) ;
    

</script>
