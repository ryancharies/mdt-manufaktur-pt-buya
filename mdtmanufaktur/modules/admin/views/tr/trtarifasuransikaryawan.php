<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Tarif Asuransi Karyawan</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trtarifasuransikaryawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <table width="100%">
                        <tr>
                            <td class="osxtable form" height="25px" width="100%">
                                <table width="100%" >
                                    <tr>
                                        <td width="100px">Tgl</td>
                                        <td width="5px">:</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td>
                                            <select name="asuransi" id="asuransi" class="form-control select" style="width:100%"
                                                            data-placeholder="Asuransi" required></select>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="400px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                </table>
            
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    bos.trtarifasuransikaryawan.grid1_data    = null ;
    bos.trtarifasuransikaryawan.grid1_loaddata= function(){

        var asuransi = bos.trtarifasuransikaryawan.obj.find("#asuransi").val();
        var tglakhir = bos.trtarifasuransikaryawan.obj.find("#tglakhir").val();
        this.grid1_data 		= {'tglakhir':tglakhir,'asuransi':asuransi} ;
    }

    bos.trtarifasuransikaryawan.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            url 	: bos.trtarifasuransikaryawan.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: false,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers  : true
            },
            multiSearch		: false,
            columns: [                
                { field: 'nip', caption: 'NIP', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'nama', caption: 'Nama', size: '150px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'jabatan', caption: 'Jabatan', size: '150px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'bagian', caption: 'Bagian', size: '150px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'nopendaftaran', caption: 'Pendaftaran', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'noasuransi', caption: 'No Asuransi', size: '120px', sortable: false , style:"text-align:center"},
                { field: 'asuransi', caption: 'Asuransi', size: '100px', sortable: false , style:"text-align:left"},
                { field: 'produk', caption: 'Produk', size: '200px', sortable: false , style:"text-align:left"},
                { field: 'tarifkaryawan', caption: 'Trf. Karyawan', size: '100px',render:"float:2",editable:{type:'text'}, sortable: false},
                { field: 'tarifperusahaan', caption: 'Trf. Perusahaan', size: '100px',render:"float:2",editable:{type:'text'}, sortable: false}
            ],
            onChange: function(event){
                //var namefield = bos.mstpayrollskalathr.grid1_kolom[event.column].field;
                console.log(event);
                if(confirm("Apakah perubahan disimpan???")){
                     var nopendaftaran = this.getCellValue(event.index,4);   
                     var karyawan =  this.getCellValue(event.index,8);  
                     var perusahaan =  this.getCellValue(event.index,9);  

                     if(event.column == 8) karyawan = event.value_new;
                     if(event.column == 9) perusahaan = event.value_new;
                     bos.trtarifasuransikaryawan.savingtarif(nopendaftaran,karyawan,perusahaan);    
                }else{
                //     //this.set(event.recid, { namefield : event.value_previous });
                //     bos.mstpayrollskalathr.grid1_loaddata();
                }
            }
        });
    }

    bos.trtarifasuransikaryawan.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trtarifasuransikaryawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trtarifasuransikaryawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trtarifasuransikaryawan.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trtarifasuransikaryawan.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.trtarifasuransikaryawan.savingtarif = function(nopendaftaran,karyawan,perusahaan){
        var tgl = this.obj.find("#tglakhir").val();
        bjs.ajax(this.url + '/saving', 'nopendaftaran=' + nopendaftaran + '&karyawan=' + karyawan + '&perusahaan=' + perusahaan + "&tgl="+tgl);
    }
    
    bos.trtarifasuransikaryawan.initcomp		= function(){
        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;


        this.obj.find('#asuransi').select2({
            ajax: {
                url: bos.trtarifasuransikaryawan.base_url + '/seekasuransi',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        
    }

    bos.trtarifasuransikaryawan.initcallback	= function(){
        this.obj.on('remove', function(){
            bos.trtarifasuransikaryawan.grid1_destroy() ;
        }) ;
    }

    bos.trtarifasuransikaryawan.initfunc	   = function(){
    

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trtarifasuransikaryawan.grid1_reloaddata();
        });

       
    }

    

    $(function(){
        bos.trtarifasuransikaryawan.initcomp() ;
        bos.trtarifasuransikaryawan.initcallback() ;
        bos.trtarifasuransikaryawan.initfunc() ;
    });
</script>