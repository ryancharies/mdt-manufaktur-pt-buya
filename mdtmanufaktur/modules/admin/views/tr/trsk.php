<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tsk">
                        <button class="btn btn-tab tpel active" href="#tsk_1" data-toggle="tab" >Daftar Stock Keluar</button>
                        <button class="btn btn-tab tpel" href="#tsk_2" data-toggle="tab">Stock Keluar</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trsk.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tsk_1" style="padding-top:5px;">
                    <table width="100%">
                        <tr>
                            <td height="25px" width="100%">
                                <table class="osxtable form">
                                    <tr>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="5px">sd</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="400px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tsk_2">
                    <table width="100%">
                        <tr>
                            <td width="100%" class="osxtable form">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="faktur">Faktur</label> </td>
                                        <td width="1%">:</td>
                                        <td width="35%">
                                            <input type="text" id="faktur" name="faktur" class="form-control" placeholder="Faktur" readonly>
                                        </td>
                                        <td width="14%"><label for="gudang">Gudang</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                             <select name="gudang" id="gudang" class="form-control scons" style="width:100%"
                                                data-placeholder="Gudang" data-sf="load_gudang" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="tgl">Tanggal</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                        </td>
                                        <td width="14%"><label for="Jenis">Jenis</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <select name="jenis" id="jenis" class="form-control select" style="width:100%"
                                                data-placeholder="Jenis" required>
                                                    <option value="1">Prive (Pemakaian Sendiri)</option>
                                                    <option value="2">Kadaluarsa</option>
                                                    <option value="3">Rusak</option>
                                                    <option value="4">Sample</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="keterangan">Keterangan</label> </td>
                                        <td width="1%">:</td>
                                        <td colspan="4">
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Keterangan" required>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "20px" class="osxtable form">
                                <table>
                                    <tr>
                                        <td width='50px'><label for="nomor">No</label> </td>
                                        <td><label for="stock">Stock</label> </td>
                                        <td><label>Nama Stock</label> </td>
                                        <td><label for="qty">Qty</label> </td>
                                        <td><label>Satuan</label> </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input maxlength="5" type="text" name="nomor" id="nomor" class="form-control number" value="0"></td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" id="stock" name="stock" class="form-control" placeholder="Stock">
                                                <span class="input-group-btn">
                                                    <button class="form-control btn btn-info" type="button" id="cmdstock"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td><input type="text" id="namastock" readonly name="namastock" class="form-control" placeholder="Nama Stock"></td>
                                        <td><input maxlength="20" type="text" name="qty" id="qty" class="form-control number" value="0"></td>
                                        <td><input type="text" id="satuan" readonly name="satuan" class="form-control" placeholder="Satuan"></td>
                                        <td><button type="button" class="btn btn-primary pull-right" id="cmdok">OK</button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "300px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:62px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    <?=cekbosjs();?>
    //grid daftar po
    bos.trsk.grid1_data    = null ;
    bos.trsk.grid1_loaddata= function(){

        var tglawal = bos.trsk.obj.find("#tglawal").val();
        var tglakhir = bos.trsk.obj.find("#tglakhir").val();
        this.grid1_data 		= {'tglawal':tglawal,'tglakhir':tglakhir} ;
    }

    bos.trsk.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trsk.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center",frozen:true},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'keterangan', caption: 'Keterangan', size: '250px', sortable: false},
                { field: 'gudang', caption: 'Gudang', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'jenis', caption: 'Jenis', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'cmdcetak', caption: ' ', size: '80px', sortable: false },
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trsk.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trsk.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trsk.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trsk.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trsk.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail sk
    bos.trsk.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'no', caption: 'No', size: '50px', sortable: false},
                { field: 'stock', caption: 'Kode / Barcode', size: '120px', sortable: false },
                { field: 'namastock', caption: 'Nama Stock', size: '150px', sortable: false },
                { field: 'qty', caption: 'Qty', size: '100px', sortable: false, style:'text-align:right',render:'float:2'},
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false},
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });


    }

    bos.trsk.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trsk.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.trsk.grid2_append    = function(no,kode,keterangan,qty,satuan){
        var datagrid  = w2ui[this.id + '_grid2'].records;
        var lnew      = true;
        var nQty      = 1;
        var recid     = "";
        if(no <= datagrid.length){
            recid = no;
            w2ui[this.id + '_grid2'].set(recid,{stock: kode, namastock: keterangan,  qty:qty, satuan:satuan});
        }else{
            recid = no;
            var Hapus = "<button type='button' onclick = 'bos.trsk.grid2_deleterow("+recid+")' class='btn btn-danger btn-grid'>Delete</button>";
            w2ui[this.id + '_grid2'].add([
                { recid:recid,
                 no: no,
                 stock: kode,
                 namastock: keterangan,
                 qty: qty,
                 satuan:satuan,
                 cmddelete:Hapus}
            ]) ;
        }
        bos.trsk.initdetail();
        bos.trsk.obj.find("#cmdstock").focus() ;
    }

    bos.trsk.grid2_deleterow = function(recid){
        if(confirm("Item di hapus dari detail stok keluar???"+recid)){
            w2ui[this.id + '_grid2'].select(recid);
            w2ui[this.id + '_grid2'].delete(true);
            bos.trsk.grid2_urutkan();
            bos.trsk.hitungsubtotal();
        }
    }

    bos.trsk.grid2_urutkan = function(){
        var datagrid = w2ui[this.id + '_grid2'].records;
        w2ui[this.id + '_grid2'].clear();
        for(i=0;i<datagrid.length;i++){
            var no = i+1;
            datagrid[i]["recid"] = no;
            var recid = no;
            var Hapus = "<button type='button' onclick = 'bos.trsk.grid2_deleterow("+recid+")' class='btn btn-danger btn-grid'>Delete</button>";
            w2ui[this.id + '_grid2'].add({recid:recid,no: no, stock: datagrid[i]["stock"], namastock: datagrid[i]["namastock"],
                                          qty:datagrid[i]["qty"],satuan:datagrid[i]["satuan"],
                                          cmddelete:Hapus});
        }
    }


    bos.trsk.cmdpilih 		= function(kode){
        bjs.ajax(this.url + '/pilihstock', bjs.getdataform(this.obj.find('form'))+'&kode=' + kode);
    }

    bos.trsk.cmdedit 		= function(faktur){
        bjs.ajax(this.url + '/editing', 'faktur=' + faktur);
    }

    bos.trsk.cmddelete 	= function(faktur){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'faktur=' + faktur);
        }
    }

    bos.trsk.cmdcetak	= function(faktur){
        bjs_os.form_report( this.base_url + '/showreport?faktur=' + faktur) ;
    }

    bos.trsk.init 			= function(){

        this.obj.find("#gudang").sval("") ;
        this.obj.find("#keterangan").val("") ;
        // this.obj.find("#jenis").sval("");

        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getfaktur') ;
        w2ui[this.id + '_grid2'].clear();

        bos.trsk.initdetail();
    }

    bos.trsk.settab 		= function(n){
        this.obj.find("#tsk button:eq("+n+")").tab("show") ;
    }

    bos.trsk.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trsk.grid1_render() ;
            bos.trsk.init() ;
        }else{
            bos.trsk.grid2_reload() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#faktur").focus() ;
        }
    }
    bos.trsk.initdetail 			= function(){
        var datagrid = w2ui[this.id + '_grid2'].records;

        this.obj.find("#nomor").val(datagrid.length+1) ;
        this.obj.find("#stock").val("") ;
        this.obj.find("#namastock").val("") ;
        this.obj.find("#qty").val("0") ;
        this.obj.find("#satuan").val("") ;
    }

    bos.trsk.initcomp		= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons"
        }) ;
        this.obj.find('.select').select2();

        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;

        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getfaktur') ;
    }

    bos.trsk.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trsk.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.trsk.grid1_destroy() ;
            bos.trsk.grid2_destroy() ;
        }) ;
    }

    bos.trsk.objs = bos.trsk.obj.find("#cmdsave") ;
    bos.trsk.initfunc	   = function(){
        this.obj.find("#cmdok").on("click", function(e){
            var no          = bos.trsk.obj.find("#nomor").val();
            var stock       = bos.trsk.obj.find("#stock").val();
            var keterangan  = bos.trsk.obj.find("#namastock").val();
            var qty   = string_2n(bos.trsk.obj.find("#qty").val());
            var satuan      = bos.trsk.obj.find("#satuan").val();
            bos.trsk.grid2_append(no,stock,keterangan,qty,satuan);
        }) ;

        this.obj.find("#cmdstock").on("click", function(e){
            mdl_barang.open(function(r){
                r = JSON.parse(r);
                bos.trsk.obj.find("#stock").val(r.kode);
                bos.trsk.obj.find("#namastock").val(r.keterangan);
                bos.trsk.obj.find("#qty").val("1").focus();
                bos.trsk.obj.find("#satuan").val(r.satuan);
            });
        }) ;

        this.obj.find("#nomor").on("blur", function(e){
            var no = bos.trsk.obj.find("#nomor").val();
            var datagrid = w2ui['bos-form-trsk_grid2'].records;
            if(no <= datagrid.length){
                bos.trsk.obj.find("#stock").val(w2ui['bos-form-trsk_grid2'].getCellValue(no-1,1));
                bos.trsk.obj.find("#namastock").val(w2ui['bos-form-trsk_grid2'].getCellValue(no-1,2));
                bos.trsk.obj.find("#qty").val(w2ui['bos-form-trsk_grid2'].getCellValue(no-1,3));
                bos.trsk.obj.find("#satuan").val(w2ui['bos-form-trsk_grid2'].getCellValue(no-1,4));
            }else{
                bos.trsk.obj.find("#nomor").val(datagrid.length + 1)
            }

        });

        this.obj.find("#stock").on("blur", function(e){
            var stock = bos.trsk.obj.find("#stock").val();

            bjs.ajax('admin/load/mdl_stock_seek', 'stock=' + stock ,'',function(r){
                r = JSON.parse(r);
                if(r.kode != undefined){
                    bos.trsk.obj.find("#stock").val(r.kode);
                    bos.trsk.obj.find("#namastock").val(r.keterangan);
                    bos.trsk.obj.find("#qty").val("1").focus();
                    bos.trsk.obj.find("#satuan").val(r.satuan);
                }else{
                    alert("data tidak ditemukan!!, Cek data barang pada menu stok / barang");

                }
            });
        });

        this.obj.find('form').on("submit", function(e){
            //this.obj.find("#cmdsave").on("click", function(e){
            e.preventDefault() ;
            if( bjs.isvalidform(this) ){
                var datagrid2 =  w2ui['bos-form-trsk_grid2'].records;
                datagrid2 = JSON.stringify(datagrid2);
                bjs.ajax( bos.trsk.base_url + '/saving', bjs.getdataform(this)+"&grid2="+datagrid2 , bos.trsk.cmdsave) ;
            }

        }) ;

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trsk.grid1_reloaddata();
        });
    }

    $(function(){
        bos.trsk.initcomp() ;
        bos.trsk.initcallback() ;
        bos.trsk.initfunc() ;
        bos.trsk.initdetail();
    });
</script>