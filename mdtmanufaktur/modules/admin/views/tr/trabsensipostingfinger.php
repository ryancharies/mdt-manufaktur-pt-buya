<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Posting Absensi Finger</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trabsensipostingfinger.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <table width="100%">
                        <tr>
                            <td class="osxtable form" height="25px" width="100%">
                                <table >
                                    <tr>
                                        <td width="100px">Tgl</td>
                                        <td width="5px">:</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tgl" name="tgl" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="400px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                </table>            
        </div>
        <div class="footer fix" style="height:32px">
            <button type="button" class="btn btn-primary pull-right" id="cmdposting">Posting</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    bos.trabsensipostingfinger.grid1_data    = null ;
    bos.trabsensipostingfinger.grid1_loaddata= function(){
        var tgl = bos.trabsensipostingfinger.obj.find("#tgl").val();
        this.grid1_data 		= {'tgl':tgl} ;
    }

    bos.trabsensipostingfinger.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            url 	: bos.trabsensipostingfinger.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers  : true
            },
            multiSearch		: false,
            columns: [                
                { field: 'nip', caption: 'NIP', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'nama', caption: 'Nama', size: '150px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'jabatan', caption: 'Jabatan', size: '150px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'bagian', caption: 'Bagian', size: '150px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'tgl', caption: 'Tgl Jw', size: '100px', sortable: false, style:"text-align:center"},
                { field: 'jenis', caption: 'Jenis', size: '100px', sortable: false, style:"text-align:center"},
                { field: 'jadwal', caption: 'Jadwal', size: '150px', sortable: false, style:"text-align:center"},
                { field: 'toleransi', caption: 'Toleransi', size: '50px', sortable: false, style:"text-align:center"},
                { field: 'absensi', caption: 'Absensi', size: '150px', sortable: false , style:"text-align:center"},
                { field: 'selisih', caption: 'Selisih', size: '150px', sortable: false , style:"text-align:center"},
                { field: 'keterangan', caption: 'Keterangan', size: '150px', sortable: false , style:"text-align:center"},
                { field: 'mesin', caption: 'Mesin', size: '60px', sortable: false , style:"text-align:center"},
                { field: 'ketmesin', caption: 'Nama Mesin', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'cabang', caption: 'Cabang', size: '60px', sortable: false , style:"text-align:center"},
                { field: 'namacabang', caption: 'Nama Cabang', size: '150px', sortable: false , style:"text-align:left"},
            ]
        });
    }

    bos.trabsensipostingfinger.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trabsensipostingfinger.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trabsensipostingfinger.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trabsensipostingfinger.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trabsensipostingfinger.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }
    
    bos.trabsensipostingfinger.initcomp		= function(){
        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        w2ui[this.id + '_grid1'].hideColumn('selisih');

        this.obj.find('#asuransi').select2({
            ajax: {
                url: bos.trabsensipostingfinger.base_url + '/seekasuransi',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        
    }

    bos.trabsensipostingfinger.initcallback	= function(){
        this.obj.on('remove', function(){
            bos.trabsensipostingfinger.grid1_destroy() ;
        }) ;
    }
    
    bos.trabsensipostingfinger.cmdposting = bos.trabsensipostingfinger.obj.find("#cmdposting") ;
    bos.trabsensipostingfinger.initfunc	   = function(){
    

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trabsensipostingfinger.grid1_reloaddata();
        });


        this.obj.find("#cmdposting").on("click", function(e){
            if(confirm("Apakah data absensi melalui finger akan di posting ke riwayat absensi karyawan??\n"+
                        "data ini akan menimpa data yang sudah ada sebelumnya yang sama")){
                var datagrid =  w2ui['bos-form-trabsensipostingfinger_grid1'].records;
                var datacount =  w2ui['bos-form-trabsensipostingfinger_grid1'].records.length;
                datagrid = JSON.stringify(datagrid);
                if(datacount > 0){
                    bjs.ajax( bos.trabsensipostingfinger.base_url + '/posting', bjs.getdataform(bos.trabsensipostingfinger.obj.find("form"))+"&grid="+datagrid, bos.trabsensipostingfinger.cmdposting) ;
                }else{
                    alert("Data tidak valid!!");
                }
            }
        });

       
    }

    

    $(function(){
        bos.trabsensipostingfinger.initcomp() ;
        bos.trabsensipostingfinger.initcallback() ;
        bos.trabsensipostingfinger.initfunc() ;
    });
</script>