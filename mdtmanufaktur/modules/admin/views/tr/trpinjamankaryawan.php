<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpinjamankaryawan">
                        <button class="btn btn-tab tpel active" href="#tpinjamankaryawan_1" data-toggle="tab" >Daftar Pinjaman Karyawan</button>
                        <button class="btn btn-tab tpel" href="#tpinjamankaryawan_2" data-toggle="tab">Pinjaman Karyawan</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trpinjamankaryawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpinjamankaryawan_1" style="padding-top:5px;">
                    <table width="100%">
                        <tr>
                            <td height="25px" width="100%">
                                <table class="osxtable form">
                                    <tr>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="5px">sd</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>

                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="400px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpinjamankaryawan_2">
                    <table width="100%">
                        <tr>
                            <td width="100%" class="osxtable form">
                                <table  width="100%">
                                    <tr>
                                        <td width="100px"><label for="nip">NIP</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <div class="input-group">
                                                <input readonly type="text" id="nip" name="nip" class="form-control" placeholder="NIP">
                                                <span class="input-group-btn">
                                                    <button class="form-control btn btn-info" type="button" id="cmdnip"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                    
                                        <td width="100px"><label for="namakaryawan">Nama Karyawan</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input type="text" id="namakaryawan" readonly name="namakaryawan" class="form-control" placeholder="Nama Keryawan">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px"><label for="jabatan">Jabatan</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input type="text" id="jabatan" readonly name="jabatan" class="form-control" placeholder="Jabatan">
                                        </td>
                                    
                                        <td width="100px"><label for="bagian">Bagian</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input type="text" id="bagian" readonly name="bagian" class="form-control" placeholder="Bagian">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr><td><hr/></td></tr>
                        <tr>
                            <td width="100%" class="osxtable form">
                                <table width="100%">
                                    <tr>
                                        <td width="100px"><label for="nopinjaman">No Pinjaman</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input readonly type="text" id="nopinjaman" name="nopinjaman" class="form-control" placeholder="No Pinjaman" required>
                                        </td>

                                        <td width="100px"><label for="fktrealisasi">Fkt Realisasi</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input readonly type="text" id="fktrealisasi" name="fktrealisasi" class="form-control" placeholder="Faktur Realisasi" required>
                                        </td>
                                    
                                        <td width="100px"><label for="tgl">Tanggal</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input caption="sd" style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                        </td>
                                    </tr>
                                   
                                    <tr>
                                        <td width="100px"><label for="plafond">Plafond</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input maxlength="20" type="text" name="plafond" id="plafond" class="form-control number" value="0">
                                        </td>
                                    
                                        <td width="100px"><label for="lama">Lama (Bln)</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input maxlength="20" type="text" name="lama" id="lama" class="form-control number" value="0" cap>
                                                
                                        </td>

                                        <td width="100px"><label for="tglawalags">Tgl Awal Ags</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input caption="sd" style="width:80px" type="text" class="form-control datetr" id="tglawalags" name="tglawalags" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                        </td>
                                    </tr>
                                   
                                   <tr>
                                        
                                        <td width="100px"><label for="keterangan">Keterangan</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Keterangan" required>
                                        </td>
                                    
                                        <td width="100px"><label for="bank">Bank</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <select name="bank" id="bank" class="form-control select" style="width:100%"
                                                    data-placeholder="Pencairan Ke" required></select>
                                        </td>
                                    </tr>
                                    
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<div class="modal fade" id="wrap-pencariankaryawan-d" role="dialog" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="wm-title">Daftar Karyawan</h4>
            </div>
            <div class="modal-body">
                <div id="grid3" style="height:250px"></div>
            </div>
            <div class="modal-footer">
                *Pilih Karyawan
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    <?=cekbosjs();?>
    //grid daftar pembelian
    bos.trpinjamankaryawan.grid1_data    = null ;
    bos.trpinjamankaryawan.grid1_loaddata= function(){

        var tglawal = bos.trpinjamankaryawan.obj.find("#tglawal").val();
        var tglakhir = bos.trpinjamankaryawan.obj.find("#tglakhir").val();
        this.grid1_data 		= {'tglawal':tglawal,'tglakhir':tglakhir} ;
    }

    bos.trpinjamankaryawan.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trpinjamankaryawan.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers		: true
            },
            multiSearch		: false,
            columns: [
                { field: 'nopinjaman', caption: 'No Pinjaman', size: '120px', sortable: false, style:"text-align:center",frozen:true},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'nip', caption: 'NIP', size: '100px', sortable: false,frozen:true},
                { field: 'nama', caption: 'Nama', size: '150px', sortable: false ,frozen:true},
                { field: 'jabatan', caption: 'Jabatan', size: '100px', sortable: false ,frozen:true},
                { field: 'keterangan', caption: 'Keterangan', size: '250px', sortable: false},
                { field: 'plafond', caption: 'Plafond', size: '100px',render:'float:2', sortable: false},
                { field: 'lama', caption: 'Lama (Bln)', size: '100px',render:'float:2', sortable: false},
                { field: 'fakturrealisasi', caption: 'Fkt Realisasi', size: '120px', sortable: false},
                { field: 'bank', caption: 'Bank', size: '130px', sortable: false},
                { field: 'tglawalags', caption: 'Tgl Awal Angs', size: '100px', sortable: false},
                { field: 'cmdcetak', caption: ' ', size: '80px', sortable: false },
                { field: 'cmdjadwal', caption: ' ', size: '80px', sortable: false },
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trpinjamankaryawan.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trpinjamankaryawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trpinjamankaryawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trpinjamankaryawan.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trpinjamankaryawan.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    
    //grid3 daftar karyawan
    bos.trpinjamankaryawan.grid3_data    = null ;
    bos.trpinjamankaryawan.grid3_loaddata= function(){
        this.grid3_data 		= {} ;
    }

    bos.trpinjamankaryawan.grid3_load    = function(){
        this.obj.find("#grid3").w2grid({
            name	: this.id + '_grid3',
            limit 	: 100 ,
            url 	: bos.trpinjamankaryawan.base_url + "/loadgrid3",
            postData: this.grid3_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'kode', caption: 'NIP', size: '100px', sortable: false},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false },
                { field: 'jabatan', caption: 'Jabatan', size: '100px', sortable: false },
                { field: 'bagian', caption: 'Bagian', size: '100px', sortable: false },
                { field: 'cmdpilih', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trpinjamankaryawan.grid3_setdata	= function(){
        w2ui[this.id + '_grid3'].postData 	= this.grid3_data ;
    }
    bos.trpinjamankaryawan.grid3_reload		= function(){
        w2ui[this.id + '_grid3'].reload() ;
    }
    bos.trpinjamankaryawan.grid3_destroy 	= function(){
        if(w2ui[this.id + '_grid3'] !== undefined){
            w2ui[this.id + '_grid3'].destroy() ;
        }
    }

    bos.trpinjamankaryawan.grid3_render 	= function(){
        this.obj.find("#grid3").w2render(this.id + '_grid3') ;
    }

    bos.trpinjamankaryawan.grid3_reloaddata	= function(){
        this.grid3_loaddata() ;
        this.grid3_setdata() ;
        this.grid3_reload() ;
    }


    bos.trpinjamankaryawan.cmdpilih 		= function(kode){
        bjs.ajax(this.url + '/pilihkaryawan',bjs.getdataform(bos.trpinjamankaryawan.obj.find("form"))+'&kode=' + kode);
    }

    bos.trpinjamankaryawan.getnopinjaman = function(){
        bjs.ajax(this.url + '/getnopinjaman',bjs.getdataform(bos.trpinjamankaryawan.obj.find("form"))) ;
    }

    bos.trpinjamankaryawan.cmdedit 		= function(nopinjaman){
       
        bjs.ajax(this.url + '/editing', 'nopinjaman=' + nopinjaman);
    }

    bos.trpinjamankaryawan.cmddelete 	= function(nopinjaman){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'nopinjaman=' + nopinjaman);
        }
    }

    bos.trpinjamankaryawan.cmdjadwal 	= function(nopinjaman){
        if(confirm("Cetak Jadwal?")){
            bjs_os.form_report( this.base_url + '/cetkjadwal?nopinjaman=' + nopinjaman) ;
        }
    }

    bos.trpinjamankaryawan.cmdcetak 	= function(nopinjaman){
        if(confirm("Cetak Bukti?")){
            bjs_os.form_report( this.base_url + '/cetakbukti?nopinjaman=' + nopinjaman) ;
        }
    }

    bos.trpinjamankaryawan.init 			= function(){

        this.obj.find("#nip").val("") ;
        this.obj.find("#namakaryawan").val("") ;
        this.obj.find("#jabatan").val("") ;
        this.obj.find("#bagian").val("") ;
        this.obj.find("#nopinjaman").val("") ;
        this.obj.find("#fktrealisasi").val("") ;
        

        this.obj.find("#plafond").val("0") ;
        this.obj.find("#lama").val("0") ;
        this.obj.find("#keterangan").val("") ;
        
        this.obj.find("#bank").sval("") ;
        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getfaktur') ;
        this.obj.find("#cmdnip").attr("disabled", false);
       
    }

    bos.trpinjamankaryawan.settab 		= function(n){
        this.obj.find("#tpinjamankaryawan button:eq("+n+")").tab("show") ;
    }

    bos.trpinjamankaryawan.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trpinjamankaryawan.grid1_render() ;
            bos.trpinjamankaryawan.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#faktur").focus() ;
        }
    }
    
    bos.trpinjamankaryawan.initcomp		= function(){
        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

       

        this.grid3_load() ;
        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getfaktur') ;
       // bjs.ajax(this.url + '/getnopinjaman') ;

       this.obj.find('#bank').select2({
            ajax: {
                url: bos.trpinjamankaryawan.base_url + '/seekbank',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
    }

    bos.trpinjamankaryawan.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trpinjamankaryawan.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.trpinjamankaryawan.grid1_destroy() ;
            bos.trpinjamankaryawan.grid3_destroy() ;
        }) ;
    }

    bos.trpinjamankaryawan.loadmodelnip      = function(l){
        this.obj.find("#wrap-pencariankaryawan-d").modal(l) ;
    }
    
    bos.trpinjamankaryawan.objs = bos.trpinjamankaryawan.obj.find("#cmdsave") ;
    bos.trpinjamankaryawan.initfunc	   = function(){
       
        this.obj.find("#cmdnip").on("click", function(e){
            bos.trpinjamankaryawan.loadmodelnip("show");
            bos.trpinjamankaryawan.grid3_reloaddata() ;
        }) ;

        this.obj.find('form').on("submit", function(e){
            //this.obj.find("#cmdsave").on("click", function(e){
            if(confirm("Data disimpan??")){
                e.preventDefault() ;
                if( bjs.isvalidform(this) ){
                    bjs.ajax( bos.trpinjamankaryawan.base_url + '/saving', bjs.getdataform(this) , bos.trpinjamankaryawan.cmdsave) ;
                }
            }
        }) ;

        this.obj.find("#plafond").on("blur", function(){
            var nilai = string_2n(bos.trpinjamankaryawan.obj.find("#plafond").val());
            bos.trpinjamankaryawan.obj.find("#plafond").val($.number(nilai,2));
        });

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trpinjamankaryawan.grid1_reloaddata();
        });

    }


    

    $(function(){
        bos.trpinjamankaryawan.initcomp() ;
        bos.trpinjamankaryawan.initcallback() ;
        bos.trpinjamankaryawan.initfunc() ;
    });
</script>