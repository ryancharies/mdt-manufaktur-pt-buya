<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tjwgolabsensi">
                        <button class="btn btn-tab tpel active" href="#tjwgolabsensi_1" data-toggle="tab" >Daftar Gol Absensi</button>
                        <button class="btn btn-tab tpel" href="#tjwgolabsensi_2" data-toggle="tab">Jadwal Absensi</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trjwgolabsensi.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tjwgolabsensi_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tjwgolabsensi_2">
                    <table class="osxtable form">
                        <tr>
                            <td width = "100%" valign ="top">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="kode">Kode</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input readonly type="text" id="kode" name="kode" class="form-control" placeholder="Kode" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="keterangan">keterangan</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Keterangan" required>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign ="top">
                                <table class="osxtable form">
                                    <tr>
                                        <td>
                                            <table class="osxtable form">
                                                <tr>
                                                    <td width='80px'>Tgl</td>
                                                    <td width='200px'>Jadwal</td>
                                                    <td width='80px'>Mulai Hari ke</td>
                                                    <td width='80px'></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td><input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>></td>
                                                    <td><select name="jadwal" id="jadwal" class="form-control select" style="width:100%" data-placeholder="Golongan"></select></td>
                                                    <td><input type="number" id="mulaihari" name="mulaihari" class="form-control" placeholder="hari ke"></td>
                                                    <td><button type="button" class="btn btn-primary pull-right" id="cmdsave">Save</button></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height = "300px">
                                            <div id="grid2" class="full-height"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.trjwgolabsensi.grid1_data 	 = null ;
    bos.trjwgolabsensi.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.trjwgolabsensi.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.trjwgolabsensi.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'keterangan', caption: 'Nama', size: '200px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '100px', sortable: false },
            ]
        });
    }

    bos.trjwgolabsensi.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trjwgolabsensi.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trjwgolabsensi.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trjwgolabsensi.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trjwgolabsensi.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //detail mutasi golongan grid2 
    bos.trjwgolabsensi.grid2_data 	 = null ;
    bos.trjwgolabsensi.grid2_loaddata= function(){
        var kode = bos.trjwgolabsensi.obj.find("#kode").val();
        this.grid2_data 		= {'kode':kode} ;
    }

    bos.trjwgolabsensi.grid2_load    = function(){ 
        this.obj.find("#grid2").w2grid({
            name	 : this.id + '_grid2',
            limit 	 : 100 ,
            url 	 : bos.trjwgolabsensi.base_url + "/loadgrid2",
            postData : this.grid2_data ,
            show 		: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false,frozen:true,style:"text-align:center;"},
                { field: 'jadwal', caption: 'Jadwal', size: '200px', sortable: false,frozen:true},
                { field: 'mulaihari', caption: 'Mulai Hari', size: '100px', sortable: false},
                { field: 'cmddelete', caption: ' ', size: '100px', sortable: false }
            ]
        });
    }

    bos.trjwgolabsensi.grid2_setdata	= function(){
        w2ui[this.id + '_grid2'].postData 	= this.grid2_data ;
    }
    bos.trjwgolabsensi.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.trjwgolabsensi.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trjwgolabsensi.grid2_render 	= function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.trjwgolabsensi.grid2_reloaddata	= function(){
        this.grid2_loaddata() ;
        this.grid2_setdata() ;
        this.grid2_reload() ;
    }

    bos.trjwgolabsensi.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.trjwgolabsensi.cmddeletemj		= function(golongan,tgl){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'golongan=' + golongan + '&tgl=' + tgl);
        }
    }

    bos.trjwgolabsensi.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#mulaihari").val("1") ;
        this.obj.find("#jadwal").sval("") ;
        w2ui[this.id + '_grid2'].clear();
    }

    bos.trjwgolabsensi.settab 		= function(n){
        this.obj.find("#tjwgolabsensi button:eq("+n+")").tab("show") ;
    }

    bos.trjwgolabsensi.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trjwgolabsensi.grid1_render() ;
            bos.trjwgolabsensi.init() ;
        }else{
            bos.trjwgolabsensi.grid2_reloaddata() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#tgl").focus() ;
        }
    }

    bos.trjwgolabsensi.initcomp	= function(){
        /*bjs.initselect({
            class : "#" + this.id + " .select2",
            clear : true
        }) ;*/
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;        
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_loaddata() ;
        this.grid2_load() ;

        this.obj.find('#jadwal').select2({
            ajax: {
                url: bos.trjwgolabsensi.base_url + '/seekjadwal',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
    }

    bos.trjwgolabsensi.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trjwgolabsensi.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.trjwgolabsensi.grid1_destroy() ;
            bos.trjwgolabsensi.grid2_destroy() ;
        })
         ;
    }


    bos.trjwgolabsensi.objs = bos.trjwgolabsensi.obj.find("#cmdsave") ;
    bos.trjwgolabsensi.initfunc 		= function(){
        // this.obj.find("form").on("submit", function(e){
        //     e.preventDefault() ;
        //     if(bjs.isvalidform(this)){
        //         bjs.ajax( bos.trjwgolabsensi.url + '/saving', bjs.getdataform(this), bos.trjwgolabsensi.objs) ;
        //     }
        // });
        this.obj.find("#cmdsave").on("click", function(e){
            var confrm = confirm("Data akan disimpan??");
            if(confrm){
                var tgl = bos.trjwgolabsensi.obj.find("#tgl").val();
                var jadwal = bos.trjwgolabsensi.obj.find("#jadwal").val();
                var kode = bos.trjwgolabsensi.obj.find("#kode").val();
                bjs.ajax( bos.trjwgolabsensi.base_url + '/saving', bjs.getdataform(bos.trjwgolabsensi.obj.find("form"))) ; 
            }
        }) ;
        
    }
    
    

    $(function(){
        bos.trjwgolabsensi.initcomp() ;
        bos.trjwgolabsensi.initcallback() ;
        bos.trjwgolabsensi.initfunc() ;
    }) ;
</script>
