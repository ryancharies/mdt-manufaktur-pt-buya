<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="thasilproduksi">
                        <button class="btn btn-tab tpel active" href="#thasilproduksi_1" data-toggle="tab" >Daftar Produksi</button>
                        <button class="btn btn-tab tpel" href="#thasilproduksi_2" data-toggle="tab">Hasil Produksi</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trhasilproduksi.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="thasilproduksi_1" style="padding-top:5px;">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label>Cabang</label>
                                <div class="input-group">
                                    <select name="skd_cabang" id="skd_cabang" class="form-control s2" style="width:100%"
                                                            data-placeholder="Pilih Cabang" data-sf="load_cabang"></select>
                                    <span class="input-group-addon">
                                        <span id="cmdcabang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                                    </span>
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group full-width">
                                <label>&nbsp;</label>
                                <div class="input-group full-width">
                                        <button type="button" class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="row" style="height: calc(100% - 50px);"> -->
                    <div id="grid1" class="full-height" style="height: calc(100% - 50px);"></div>
                    <!-- </div> -->
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="thasilproduksi_2">
                    <table width="100%" height='100%'>
                        <tr>
                            <td width="100%">
                                <table width="100%" height="100%">
                                    <tr>
                                        <td width = "50%" >
                                            <table class="osxtable form">
                                                <tr>
                                                    <td width="14%"><label for="fakturprod">Faktur</label> </td>
                                                    <td width="1%">:</td>
                                                    <td width="35%">
                                                        <input type="text" readonly id="fakturprod" name="fakturprod" class="form-control" placeholder="Faktur" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="cabang">Cabang</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <select name="cabang" readonly id="cabang" class="form-control select" style="width:100%"
                                                                data-placeholder="Cabang" required></select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="tglprod">Tanggal</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input style="width:80px" readonly type="text" class="form-control datetr" id="tglprod" name="tglprod" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="perbaikan">Perbaikan</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input style="width:40px" readonly type="text" class="form-control" id="perbaikan" name="perbaikan" required >
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="petugas">Karu</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input readonly type="text" class="form-control" id="petugas" name="petugas" >
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width = "50%" height="100%">
                                            <div id="grid4" class="full-height"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "20px" class="osxtable form">
                                <table>
                                    <tr>
                                        <td><label for="faktur">Faktur</label> </td>
                                        <td><label for="tgl">Tgl</label> </td>
                                        <td><label for="gudang">Gudang</label> </td>
                                        <td><label for="stock">Produk</label> </td>
                                        <td><label>Nama Produk</label> </td>
                                        <td><label for="qty">Qty</label> </td>
                                        <td><label>Satuan</label> </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" readonly id="faktur" name="faktur" class="form-control" placeholder="Faktur" ></td>
                                        <td><input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl"  value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>></td>
                                        <td><select name="gudang" id="gudang" class="form-control scons" style="width:100%"
                                                    data-placeholder="Gudang" data-sf="load_gudang" required>
                                            </select>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" id="stock" name="stock" class="form-control" placeholder="Produk">
                                                <span class="input-group-btn">
                                                    <button class="form-control btn btn-info" type="button" id="cmdstock"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td><input type="text" id="namastock" readonly name="namastock" class="form-control" placeholder="Nama Produk"></td>
                                        <td><input maxlength="10" type="text" name="qty" id="qty" class="form-control number" value="0"></td>
                                        <td><input type="text" id="satuan" readonly name="satuan" class="form-control" placeholder="Satuan"></td>
                                        <td><button type="button" class="btn btn-primary pull-right" id="cmdok">OK</button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "230px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <!--
<button class="btn btn-primary pull-right" id="cmdsave">Simpan</button> -->
        </div>
    </form>
</div>
<div class="modal fade" id="wrap-pencarianstock-d" role="dialog" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="wm-title">Daftar Produk</h4>
            </div>
            <div class="modal-body">
                <div id="grid3" style="height:250px"></div>
            </div>
            <div class="modal-footer">
                *Pilih Stock
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    bos.trhasilproduksi.grid1_data    = null ;
    bos.trhasilproduksi.grid1_loaddata= function(){
        mdl_cabang.vargr.selection = bos.trhasilproduksi.obj.find('#skd_cabang').val();
        this.grid1_data 		= {
			'skd_cabang':JSON.stringify(mdl_cabang.vargr.selection)
		} ;
    }

    bos.trhasilproduksi.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trhasilproduksi.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'cabang', caption: 'Cabang', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'regu', caption: 'Regu', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'karu', caption: 'Karu', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'stt', caption: 'Status', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
            ]
        });
    }

    bos.trhasilproduksi.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trhasilproduksi.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trhasilproduksi.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trhasilproduksi.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trhasilproduksi.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid BB
    bos.trhasilproduksi.grid2_data    = null ;
    bos.trhasilproduksi.grid2_loaddata= function(){

        var fakturproduksi = bos.trhasilproduksi.obj.find("#fakturprod").val();
        this.grid2_data 		= {'fakturproduksi':fakturproduksi} ;
    }

    bos.trhasilproduksi.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            url 	: bos.trhasilproduksi.base_url + "/loadgrid2",
            postData: this.grid2_data ,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'no', caption: 'No', size: '50px', sortable: false},
                { field: 'faktur', caption: 'Faktur', size: '150px', sortable: false },
                { field: 'tgl', caption: 'Tanggal', size: '100px', sortable: false },
                { field: 'gudang', caption: 'Gudang', size: '100px', sortable: false },
                { field: 'stock', caption: 'Stock', size: '100px', sortable: false },
                { field: 'namastock', caption: 'Nama Stock', size: '150px', sortable: false },
                { field: 'qty', caption: 'Qty', size: '70px', sortable: false, style:'text-align:right'},
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false}
            ]
        });
        //,        { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }

    }

    bos.trhasilproduksi.grid2_reloaddata	= function(){
        this.grid2_loaddata() ;
        this.grid2_setdata() ;
        this.grid2_reload() ;
    }

    bos.trhasilproduksi.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trhasilproduksi.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.trhasilproduksi.grid2_setdata	= function(){
        w2ui[this.id + '_grid2'].postData 	= this.grid2_data ;
    }

    bos.trhasilproduksi.grid2_deleterow = function(faktur){
        if(confirm("Penggunaan BB di hapus dari detail produksi???"+faktur)){
            bjs.ajax( bos.trhasilproduksi.base_url + '/deleteprod', bjs.getdataform(this)+"&faktur="+faktur) ;
        }
    }

    //grid3 daftarstock
    bos.trhasilproduksi.grid3_data    = null ;
    bos.trhasilproduksi.grid3_loaddata= function(){
        var fakturproduksi = bos.trhasilproduksi.obj.find("#fakturprod").val();
        this.grid3_data 		= {'fakturproduksi':fakturproduksi} ;
    }

    bos.trhasilproduksi.grid3_load    = function(){
        this.obj.find("#grid3").w2grid({
            name	: this.id + '_grid3',
            limit 	: 100 ,
            url 	: bos.trhasilproduksi.base_url + "/loadgrid3",
            postData: this.grid3_data ,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false },
                { field: 'qty', caption: 'Qty', size: '100px', sortable: false },
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false },
                { field: 'cmdpilih', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trhasilproduksi.grid3_setdata	= function(){
        w2ui[this.id + '_grid3'].postData 	= this.grid3_data ;
    }
    bos.trhasilproduksi.grid3_reload		= function(){
        w2ui[this.id + '_grid3'].reload() ;
    }
    bos.trhasilproduksi.grid3_destroy 	= function(){
        if(w2ui[this.id + '_grid3'] !== undefined){
            w2ui[this.id + '_grid3'].destroy() ;
        }
    }

    bos.trhasilproduksi.grid3_render 	= function(){
        this.obj.find("#grid3").w2render(this.id + '_grid3') ;
    }

    bos.trhasilproduksi.grid3_reloaddata	= function(){
        this.grid3_loaddata() ;
        this.grid3_setdata() ;
        this.grid3_reload() ;
    }

    //grid4 daftarstock
    bos.trhasilproduksi.grid4_load    = function(){
        this.obj.find("#grid4").w2grid({
            name	: this.id + '_grid4',
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'stock', caption: 'Kode', size: '100px', sortable: false},
                { field: 'namastock', caption: 'Keterangan', size: '200px', sortable: false },
                { field: 'qty', caption: 'Qty', size: '70px', sortable: false, style:'text-align:right'},
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false }
            ]
        });
    }

    bos.trhasilproduksi.grid4_reload		= function(){
        w2ui[this.id + '_grid4'].reload() ;
    }
    bos.trhasilproduksi.grid4_destroy 	= function(){
        if(w2ui[this.id + '_grid4'] !== undefined){
            w2ui[this.id + '_grid4'].destroy() ;
        }
    }
    bos.trhasilproduksi.cmdpilih 		= function(kode,faktur){
        bjs.ajax(this.url + '/pilihstock', 'stock=' + kode + '&faktur=' + faktur);
    }

    bos.trhasilproduksi.cmdedit 		= function(faktur){

        bjs.ajax(this.url + '/editing', 'faktur=' + faktur,'',function(v){
            v = JSON.parse(v);
            with(bos.trhasilproduksi.obj){
                find(".nav-tabs li:eq(1) a").tab("show") ;
                find("#fakturprod").val(v.faktur) ;
                find("#petugas").val(v.karu) ;
                find("#perbaikan").val(v.perbaikan) ;
                find("#tglprod").val(v.tgl);
                find("#tgl").val(v.tgl);               
                find("#cabang").sval([{id:v.cabang,text:v.ketcabang}]);
                find('#gudang').attr('data-sp',v.cabang);
            }
            console.log(v.detil);
            n = 0;
            $.each(v.detil, function(i, val){
                
                w2ui['bos-form-trhasilproduksi_grid4'].add([{
                        recid: n++,
                        stock: val.stock,
                        namastock: val.namastock,
                        qty: val.qty,
                        satuan: val.satuan
                    }]) ;
            });
            bos.trhasilproduksi.initdetail();
            bos.trhasilproduksi.settab(1) ;
            bos.trhasilproduksi.grid2_reloaddata() ;
            bos.trhasilproduksi.grid2_reload() ;

        });
    }

    bos.trhasilproduksi.cmddelete 	= function(faktur){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'faktur=' + faktur);
        }
    }

    bos.trhasilproduksi.init 			= function(){
        this.obj.find("#cabang").sval("") ;
        this.obj.find("#fakturprod").val("") ;
        this.obj.find("#petugas").val("") ;

        bjs.ajax(this.url + '/init') ;
        w2ui[this.id + '_grid3'].clear();
        w2ui[this.id + '_grid2'].clear();
        w2ui[this.id + '_grid4'].clear();

    }

    bos.trhasilproduksi.settab 		= function(n){
        this.obj.find("#thasilproduksi button:eq("+n+")").tab("show") ;
    }

    bos.trhasilproduksi.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trhasilproduksi.grid1_render() ;
            bos.trhasilproduksi.init() ;
        }else{
            bos.trhasilproduksi.grid3_reloaddata() ;
            bos.trhasilproduksi.grid2_reloaddata() ;
            bos.trhasilproduksi.grid4_reload() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#faktur").focus() ;
        }
    }

    bos.trhasilproduksi.initdetail 			= function(){
        var datagrid = w2ui[this.id + '_grid2'].records;
        this.obj.find("#nomor").val(datagrid.length+1) ;
        this.obj.find("#stock").val("") ;
        this.obj.find("#namastock").val("") ;
        this.obj.find("#qty").val("1") ;
        this.obj.find("#satuan").val("") ;
        //this.obj.find("#gudang").sval("") ;
        bjs.ajax(this.url + '/getfaktur') ;
    }
    bos.trhasilproduksi.initcomp		= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons",
            clear:false
        }) ;

        bjs.initselect({
            class : "#" + this.id + " .s2",
            multi:true,
            clear:true
        }) ;

        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;
        this.grid2_loaddata() ;
        this.grid2_load() ;

        this.grid3_loaddata() ;
        this.grid3_load() ;
        this.grid4_load() ;
        bjs.ajax(this.url + '/init') ;
    }

    bos.trhasilproduksi.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trhasilproduksi.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.trhasilproduksi.grid1_destroy() ;
            bos.trhasilproduksi.grid2_destroy() ;
            bos.trhasilproduksi.grid3_destroy() ;
            bos.trhasilproduksi.grid4_destroy() ;
        }) ;
    }

    bos.trhasilproduksi.loadmodelstock      = function(l){
        this.obj.find("#wrap-pencarianstock-d").modal(l) ;
    }

    bos.trhasilproduksi.objs = bos.trhasilproduksi.obj.find("#cmdsave") ;
    bos.trhasilproduksi.objok = bos.trhasilproduksi.obj.find("#cmdok") ;
    bos.trhasilproduksi.initfunc	   = function(){
        this.obj.find('form').on("submit", function(e){
            //this.obj.find("#cmdsave").on("click", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                var datagrid2 =  w2ui['bos-form-trhasilproduksi_grid2'].records;
                datagrid2 = JSON.stringify(datagrid2);
                bjs.ajax( bos.trhasilproduksi.base_url + '/saving', bjs.getdataform(this)+"&grid2="+datagrid2 , bos.trhasilproduksi.objs) ;
            }

        }) ;

        this.obj.find("#cmdok").on("click", function(e){
            var datagrid2 =  w2ui['bos-form-trhasilproduksi_grid2'].records;
            var stock = bos.trhasilproduksi.obj.find("#stock").val();
            var gudang = bos.trhasilproduksi.obj.find("#gudang").val();

            if(stock !== "" && gudang !== ""){
                if(datagrid2.length == 0){
                    var tgl = bos.trhasilproduksi.obj.find("#tgl").val();
                    var stock = bos.trhasilproduksi.obj.find("#stock").val();
                    var qty = bos.trhasilproduksi.obj.find("#qty").val();
                    var fakturprod = bos.trhasilproduksi.obj.find("#fakturprod").val();
                    var gudang = bos.trhasilproduksi.obj.find("#gudang").val();
                    var content = "&tgl="+tgl+"&stock="+stock+"&qty="+qty+"&fakturprod="+fakturprod+"&gudang="+gudang;
                    bjs.ajax( bos.trhasilproduksi.base_url + '/savingprod', bjs.getdataform(this)+content, bos.trhasilproduksi.objok) ;
                }else{
                    alert("hapus dulu hasil produksi yag sudah ter entry sebelumnya, karena hasil tidak bisa lebih dari 1 item..");
                }
            }else{
                alert("Data tidak valid!!");
            }            
        }) ;

        this.obj.find("#cmdstock").on("click", function(e){
            bos.trhasilproduksi.loadmodelstock("show");
            bos.trhasilproduksi.grid3_reloaddata();
        }) ;

        this.obj.find("#stock").on("blur", function(e){
            if(bos.trhasilproduksi.obj.find("#stock").val() !== ""){
                var stock = bos.trhasilproduksi.obj.find("#stock").val();
                var tampil = ['P','S'];
                var qty = 0;
                if(stock.indexOf("*") > 0){
                    res            = stock.split("*");
                    bos.trhasilproduksi.obj.find("#qty").val(res[0]);
                    bos.trhasilproduksi.obj.find("#stock").val(res[1]);
                }
                bjs.ajax( bos.trhasilproduksi.base_url + '/seekstock', bjs.getdataform(this)) ;
            }
        });

        this.obj.find("#nomor").on("blur", function(e){
            var no = bos.trhasilproduksi.obj.find("#nomor").val();
            var datagrid = w2ui['bos-form-trhasilproduksi_grid2'].records;
            if(no <= datagrid.length){
                bos.trhasilproduksi.obj.find("#stock").val(w2ui['bos-form-trhasilproduksi_grid2'].getCellValue(no-1,1));
                bos.trhasilproduksi.obj.find("#namastock").val(w2ui['bos-form-trhasilproduksi_grid2'].getCellValue(no-1,2));
                bos.trhasilproduksi.obj.find("#qty").val(w2ui['bos-form-trhasilproduksi_grid2'].getCellValue(no-1,3));
                bos.trhasilproduksi.obj.find("#satuan").val(w2ui['bos-form-trhasilproduksi_grid2'].getCellValue(no-1,4));
            }else{
                bos.trhasilproduksi.obj.find("#nomor").val(datagrid.length + 1)
            }

        });

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trhasilproduksi.grid1_reloaddata();
        });

        this.obj.find('#cmdcabang').on('click', function(){
            mdl_cabang.vargr.selection = bos.trhasilproduksi.obj.find('#skd_cabang').val();
            mdl_cabang.open(function(r){
                r = JSON.parse(r);
                bos.trhasilproduksi.cabang = [];
                $.each(r, function(i, v){
                    bos.trhasilproduksi.cabang.push({'id':v.kode,'text':v.keterangan})                   
                });
                bos.trhasilproduksi.obj.find('#skd_cabang').sval(bos.trhasilproduksi.cabang);
            });
        });

    }

    $(function(){
        bos.trhasilproduksi.initcomp() ;
        bos.trhasilproduksi.initcallback() ;
        bos.trhasilproduksi.initfunc() ;
        bos.trhasilproduksi.initdetail();
    });
</script>