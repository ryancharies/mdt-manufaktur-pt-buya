<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tapk">
                        <button class="btn btn-tab tpel active" href="#tapk_1" data-toggle="tab" >Daftar Angs. Pinj. Karyawan</button>
                        <button class="btn btn-tab tpel" href="#tapk_2" data-toggle="tab">Angs. Pinj. Karyawan</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.tragspinjamankaryawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tapk_1" style="padding-top:5px;">
                    <table width="100%">
                        <tr>
                            <td height="25px" width="100%">
                                <table class="osxtable form">
                                    <tr>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="5px">sd</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="400px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tapk_2">
                    <table width="100%">                        
                        <tr>
                            <td width = "50%" valign = "top">
                                <table width="100%">
                                    <tr>
                                        <td width="100%" class="osxtable form">
                                            <table width="100%">
                                                <tr>
                                                    <td width="14%"><label for="nopinjaman">No Pinjaman</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <div class="input-group">
                                                            <input readonly type="text" id="nopinjaman" name="nopinjaman" class="form-control" placeholder="No Pinjaman">
                                                            <span class="input-group-btn">
                                                                <button class="form-control btn btn-info" type="button" id="cmdnopinjaman"><i class="fa fa-search"></i></button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="nip">NIP</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input readonly type="text" id="nip" name="nip" class="form-control" placeholder="NIP">
                                                            
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="namakaryawan">Nama Karyawan</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input type="text" id="namakaryawan" readonly name="namakaryawan" class="form-control" placeholder="Nama Karyawan">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="jabatan">Jabatan</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input type="text" id="jabatan" readonly name="jabatan" class="form-control" placeholder="Jabatan">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="bagian">Bagian</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input type="text" id="bagian" readonly name="bagian" class="form-control" placeholder="Bagian">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="tglrealisasi">Tgl. Realisasi</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input readonly style="width:80px" type="text" class="form-control date" id="tglrealisasi" name="tglrealisasi" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="tglawalags">Tgl. Ags Awal</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input readonly style="width:80px" type="text" class="form-control date" id="tglawalags" name="tglawalags" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="plafond">Plafond</label> </td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <input type="text" name="plafond" id="plafond" readonly
                                                            class="form-control number" value="0" required> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="lama">Lama (Bln)</label> </td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <input type="text" name="lama" id="lama" readonly 
                                                            class="form-control number" value="0" required> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="bakidebetawal">Bakidebet Awal</label> </td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <input type="text" name="bakidebetawal" id="bakidebetawal" readonly
                                                            class="form-control number" value="0"> 
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table width="100%">
                                    <tr>
                                        <td width="100%" class="osxtable form">
                                            <table width="100%">
                                                <tr>
                                                    <td width="14%"><label for="faktur">Faktur</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input readonly type="text" id="faktur" name="faktur" class="form-control" placeholder="Faktur" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="tgl">Tanggal</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="pokok">Pokok</label> </td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <input type="text" name="pokok" id="pokok" readonly
                                                            class="form-control number" value="0" required> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="bakidebetakhir">Bakidebet Akhir</label> </td>
                                                    <td width="1%">:</td>
                                                    <td>
                                                        <input type="text" name="bakidebetakhir" id="bakidebetakhir" readonly
                                                            class="form-control number" value="0"> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="keterangan">Keterangan</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Keterangan Angsuran" required>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height = "20px" class="osxtable form">
                                            <table>
                                                <tr>
                                                    <td width='50px'><label for="nomor">No</label> </td>
                                                    <td><label for="rekening">Rekening</label> </td>
                                                    <td><label for="nominal">Nominal</label> </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td><input maxlength="5" type="text" name="nomor" id="nomor" class="form-control number" value="0"></td>
                                                    <td>
                                                        <select name="rekening" id="rekening" class="form-control select rekselect" style="width:100%"
                                                                data-placeholder="Rekening"></select>  
                                                    </td>
                                                    <td><input maxlength="20" type="text" name="nominal" id="nominal" class="form-control number" value="0"></td>
                                                    <td><button type="button" class="btn btn-primary pull-right" id="cmdok">OK</button></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height = "230px" >
                                            <div id="grid2" class="full-height"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<div class="modal fade" id="wrap-pencariannopinjaman-d" role="dialog" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog" style="width:900px;">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="wm-title">Daftar No Pinjaman</h4>
            </div>
            <div class="modal-body">
                <div id="grid3" style="height:250px"></div>
            </div>
            <div class="modal-footer">
                *Pilih No Pinjaman
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    //grid daftar po
    bos.tragspinjamankaryawan.grid1_data    = null ;
    bos.tragspinjamankaryawan.grid1_loaddata= function(){

        var tglawal = bos.tragspinjamankaryawan.obj.find("#tglawal").val();
        var tglakhir = bos.tragspinjamankaryawan.obj.find("#tglakhir").val();
        this.grid1_data 		= {'tglawal':tglawal,'tglakhir':tglakhir} ;
    }

    bos.tragspinjamankaryawan.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.tragspinjamankaryawan.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers  : true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center",frozen:true},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'nopinjaman', caption: 'No Pinjaman', size: '120px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'nip', caption: 'NIP', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'jabatan', caption: 'Jabatan', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'bagian', caption: 'Bagian', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'pokok', caption: 'Jumlah', size: '100px', sortable: false , render:'float:2'},
                { field: 'keterangan', caption: 'Keterangan', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'cmdcetak', caption: ' ', size: '80px', sortable: false },
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.tragspinjamankaryawan.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.tragspinjamankaryawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.tragspinjamankaryawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.tragspinjamankaryawan.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.tragspinjamankaryawan.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail
    bos.tragspinjamankaryawan.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'no', caption: 'No', size: '50px', sortable: false},
                { field: 'kode', caption: 'Kode', size: '120px', sortable: false },
                { field: 'ketrekening', caption: 'Ket. Rekening', size: '120px', sortable: false },
                { field: 'nominal', caption: 'Nominal', size: '100px', sortable: false, render:'int'},
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });


    }

    bos.tragspinjamankaryawan.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.tragspinjamankaryawan.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.tragspinjamankaryawan.grid2_append    = function(no,kode,ketrekening,nominal){
        var datagrid  = w2ui[this.id + '_grid2'].records;
        var lnew      = true;
        var nQty      = 1;
        var recid     = "";
        if(no <= datagrid.length){
            recid = no;
            w2ui[this.id + '_grid2'].set(recid,{kode: kode,ketrekening:ketrekening, nominal: nominal});
        }else{
            recid = no;
            var Hapus = "<button type='button' onclick = 'bos.tragspinjamankaryawan.grid2_deleterow("+recid+")' class='btn btn-danger btn-grid'>Delete</button>";
            w2ui[this.id + '_grid2'].add([
                { recid:recid,
                 no: no,
                 kode: kode,
                 ketrekening: ketrekening,
                 nominal:nominal,
                 cmddelete:Hapus}
            ]) ;
        }
        bos.tragspinjamankaryawan.initdetail();
        bos.tragspinjamankaryawan.hitungjumlah();
    }

    bos.tragspinjamankaryawan.grid2_deleterow = function(recid){
        if(confirm("Item di hapus dari detail???"+recid)){
            w2ui[this.id + '_grid2'].select(recid);
            w2ui[this.id + '_grid2'].delete(true);
            bos.tragspinjamankaryawan.grid2_urutkan();
        }
    }

    bos.tragspinjamankaryawan.grid2_urutkan = function(){
        var datagrid = w2ui[this.id + '_grid2'].records;
        w2ui[this.id + '_grid2'].clear();
        for(i=0;i<datagrid.length;i++){
            var no = i+1;
            datagrid[i]["recid"] = no;
            var recid = no;
            var Hapus = "<button type='button' onclick = 'bos.tragspinjamankaryawan.grid2_deleterow("+recid+")' class='btn btn-danger btn-grid'>Delete</button>";
            w2ui[this.id + '_grid2'].add({recid:recid,no: no, kode: datagrid[i]["kode"], ketrekening: datagrid[i]["ketrekening"],
                                          nominal:datagrid[i]["nominal"],cmddelete:Hapus});
        }
    }

    //grid3
    bos.tragspinjamankaryawan.grid3_data    = null ;
    bos.tragspinjamankaryawan.grid3_loaddata= function(){
        var tgl = bos.tragspinjamankaryawan.obj.find("#tgl").val();
        var faktur = bos.tragspinjamankaryawan.obj.find("#faktur").val();
        this.grid3_data 		= {'tgl':tgl,'faktur':faktur} ;
    }

    bos.tragspinjamankaryawan.grid3_load    = function(){
        this.obj.find("#grid3").w2grid({
            name	: this.id + '_grid3',
            limit 	: 100 ,
            url 	: bos.tragspinjamankaryawan.base_url + "/loadgrid3",
            postData: this.grid3_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'nopinjaman', caption: 'No Pinjaman', size: '120px', sortable: false,frozen:true},
                { field: 'nip', caption: 'NIP', size: '100px', sortable: false,frozen:true},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false ,frozen:true},
                { field: 'jabatan', caption: 'Jabatan', size: '100px', sortable: false },
                { field: 'bagian', caption: 'Bagian', size: '100px', sortable: false },
                { field: 'plafond', caption: 'Plafond', size: '100px', sortable: false, render:'float:2'},
                { field: 'bakidebet', caption: 'Bakidebet', size: '100px', sortable: false, render:'float:2'},
                { field: 'cmdpilih', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.tragspinjamankaryawan.grid3_setdata	= function(){
        w2ui[this.id + '_grid3'].postData 	= this.grid3_data ;
    }
    bos.tragspinjamankaryawan.grid3_reload		= function(){
        w2ui[this.id + '_grid3'].reload() ;
    }
    bos.tragspinjamankaryawan.grid3_destroy 	= function(){
        if(w2ui[this.id + '_grid3'] !== undefined){
            w2ui[this.id + '_grid3'].destroy() ;
        }
    }

    bos.tragspinjamankaryawan.grid3_render 	= function(){
        this.obj.find("#grid3").w2render(this.id + '_grid3') ;
    }

    bos.tragspinjamankaryawan.grid3_reloaddata	= function(){
        this.grid3_loaddata() ;
        this.grid3_setdata() ;
        this.grid3_reload() ;
    }

    bos.tragspinjamankaryawan.cmdpilih 		= function(nopinjaman){
        var tgl = this.obj.find("#tgl").val() ;
        bjs.ajax(this.url + '/pilihnopinjaman','nopinjaman=' + nopinjaman + '&tgl=' + tgl);
    }


    bos.tragspinjamankaryawan.cmdedit 		= function(faktur){
        bjs.ajax(this.url + '/editing', 'faktur=' + faktur);
    }

    bos.tragspinjamankaryawan.cmddelete 	= function(faktur){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'faktur=' + faktur);
        }
    }

    bos.tragspinjamankaryawan.cmdcetak	= function(faktur){
        bjs_os.form_report( this.base_url + '/showreport?faktur=' + faktur) ;
    }

    bos.tragspinjamankaryawan.cmdcetakdm	= function(faktur){
        if(confirm("Cetak Data?")){
            bjs.ajax(this.url + '/printbuktidm', 'faktur='+faktur);
        }
    }

    bos.tragspinjamankaryawan.printbukti = function (contenthtml) {
        var mywindow = window.open('', 'Print', 'height=600,width=800');

        mywindow.document.write(contenthtml);

        mywindow.document.close();
        mywindow.focus()
        mywindow.print();
        mywindow.close();
    }

    bos.tragspinjamankaryawan.hitungjumlah 			= function(){

        var nRows = w2ui[this.id + '_grid2'].records.length;
        var subtotal = 0 ;
        for(i=0;i< nRows;i++){
            var jumlah = w2ui[this.id + '_grid2'].getCellValue(i,3);
            subtotal += Number(jumlah);
        }
        this.obj.find("#pokok").val($.number(subtotal,2));

        var bdawal = string_2n(this.obj.find("#bakidebetawal").val());
        var bdakhir = bdawal - subtotal;
        this.obj.find("#bakidebetakhir").val($.number(bdakhir,2));
    }


    bos.tragspinjamankaryawan.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.tragspinjamankaryawan.init 			= function(){

        this.obj.find("#nopinjaman").val("") ;
        this.obj.find("#nip").val("") ;
        this.obj.find("#namakaryawan").val("") ;
        this.obj.find("#jabatan").val("") ;
        this.obj.find("#bagian").val("") ;
        this.obj.find("#plafond").val("0") ;
        this.obj.find("#lama").val("0") ;
        this.obj.find("#bakidebetawal").val("0") ;
        this.obj.find("#pokok").val("0") ;
        this.obj.find("#bakidebetakhir").val("0") ;
        this.obj.find("#keterangan").val("Angsuran Pinjaman") ;


        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getfaktur') ;
        w2ui[this.id + '_grid2'].clear();

        bos.tragspinjamankaryawan.initdetail();
    }

    bos.tragspinjamankaryawan.settab 		= function(n){
        this.obj.find("#tapk button:eq("+n+")").tab("show") ;
    }

    bos.tragspinjamankaryawan.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.tragspinjamankaryawan.grid1_render() ;
            bos.tragspinjamankaryawan.init() ;
        }else{
            bos.tragspinjamankaryawan.grid2_reload() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#faktur").focus() ;
        }
    }
    bos.tragspinjamankaryawan.initdetail 			= function(){
        var datagrid = w2ui[this.id + '_grid2'].records;
        this.obj.find("#nomor").val(datagrid.length+1) ;
        this.obj.find("#rekening").sval("") ;
        this.obj.find("#nominal").val("0") ;
    }

    bos.tragspinjamankaryawan.initcomp		= function(){
        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.obj.find('.rekselect').select2({
            ajax: {
                url: bos.tragspinjamankaryawan.base_url + '/seekrekening',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;

        this.grid3_loaddata() ;
        this.grid3_load() ;

        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getfaktur') ;
    }

    bos.tragspinjamankaryawan.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.tragspinjamankaryawan.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.tragspinjamankaryawan.grid1_destroy() ;
            bos.tragspinjamankaryawan.grid2_destroy() ;
            bos.tragspinjamankaryawan.grid3_destroy() ;
        }) ;
    }

    bos.tragspinjamankaryawan.loadmodelnopinjaman      = function(l){
        this.obj.find("#wrap-pencariannopinjaman-d").modal(l) ;
    }

    bos.tragspinjamankaryawan.objs = bos.tragspinjamankaryawan.obj.find("#cmdsave") ;
    bos.tragspinjamankaryawan.initfunc	   = function(){
        this.obj.find("#cmdok").on("click", function(e){

            var no          = bos.tragspinjamankaryawan.obj.find("#nomor").val();
            var kode       = bos.tragspinjamankaryawan.obj.find("#rekening").val();
            var ketrekening       = bos.tragspinjamankaryawan.obj.find("#rekening").text();
            var nominal         = string_2n(bos.tragspinjamankaryawan.obj.find("#nominal").val());
            bos.tragspinjamankaryawan.grid2_append(no,kode,ketrekening,nominal);
        }) ;


        this.obj.find("#nomor").on("blur", function(e){
            var no = bos.tragspinjamankaryawan.obj.find("#nomor").val();
            var datagrid = w2ui['bos-form-tragspinjamankaryawan_grid2'].records;
            if(no <= datagrid.length){
                bos.tragspinjamankaryawan.obj.find("#rekening").sval([{id:w2ui['bos-form-tragspinjamankaryawan_grid2'].getCellValue(no-1,1),text:w2ui['bos-form-tragspinjamankaryawan_grid2'].getCellValue(no-1,2)}]);
                bos.tragspinjamankaryawan.obj.find("#nominal").val(w2ui['bos-form-tragspinjamankaryawan_grid2'].getCellValue(no-1,3));
            }else{
                bos.tragspinjamankaryawan.obj.find("#nomor").val(datagrid.length + 1)
            }

        });

        this.obj.find('form').on("submit", function(e){
            //this.obj.find("#cmdsave").on("click", function(e){
            if(confirm("Data disimpan ??")){
                e.preventDefault() ;
                if( bjs.isvalidform(this) ){
                    var datagrid2 =  w2ui['bos-form-tragspinjamankaryawan_grid2'].records;
                    datagrid2 = JSON.stringify(datagrid2);
                    bjs.ajax( bos.tragspinjamankaryawan.base_url + '/saving', bjs.getdataform(this)+"&grid2="+datagrid2 , bos.tragspinjamankaryawan.cmdsave) ;
                }
            }

        }) ;

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.tragspinjamankaryawan.grid1_reloaddata();
        });

        this.obj.find("#cmdnopinjaman").on("click", function(e){
            bos.tragspinjamankaryawan.loadmodelnopinjaman("show");
            bos.tragspinjamankaryawan.grid3_reloaddata() ;
        }) ;
    }

    

    $(function(){
        bos.tragspinjamankaryawan.initcomp() ;
        bos.tragspinjamankaryawan.initcallback() ;
        bos.tragspinjamankaryawan.initfunc() ;
        bos.tragspinjamankaryawan.initdetail();
    });
</script>