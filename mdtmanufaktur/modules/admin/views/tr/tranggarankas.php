<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tagnkas">
                        <button class="btn btn-tab tpel active" href="#tagnkas_1" data-toggle="tab" >Daftar Anggaran Kas</button>
                        <button class="btn btn-tab tpel" href="#tagnkas_2" data-toggle="tab">Anggaran Kas</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.tranggarankas.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tagnkas_1" style="padding-top:5px;">
                    <table width="100%">
                        <tr>
                            <td height="25px" width="100%">
                                <table class="osxtable form">
                                    <tr>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="periodeawal" name="periodeawal" value=<?=date("Y")?> <?=date_periodset(false)?>>
                                        </td>
                                        <td width="5px">sd</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="periodeakhir" name="periodeakhir" value=<?=date("Y")?> <?=date_periodset(false)?>>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="400px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tagnkas_2">
                    <table width="100%">
                        <tr>
                            <td width="100%" class="osxtable form">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="periode">Periode</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                        <input style="width:80px" type="text" class="form-control datetr" id="periode" name="periode" required value=<?=date("Y")?> <?=date_periodset(false)?> <?=$mintgl?>>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "430px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                        <tr>
                            <td height = "20px" class="osxtable form">
                                <table>
                                    <tr>
                                        <td><label>Oprasional</label> </td>
                                        <td><label>Pendanaan</label> </td>
                                        <td><label>Investasi</label> </td>
                                        <td><label>Total</label> </td>
                                    </tr>
                                    <tr>
                                        <td><input maxlength="20" readonly type="text" name="oprasional" id="oprasional" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" readonly type="text" name="pendanaan" id="pendanaan" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" readonly type="text" name="investasi" id="investasi" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" readonly type="text" name="total" id="total" class="form-control number" value="0"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    //grid daftar po
    bos.tranggarankas.grid1_data    = null ;
    bos.tranggarankas.grid1_loaddata= function(){

        var periodeawal = bos.tranggarankas.obj.find("#periodeawal").val();
        var periodeakhir = bos.tranggarankas.obj.find("#periodeakhir").val();
        this.grid1_data 		= {'periodeawal':periodeawal,'periodeakhir':periodeakhir} ;
    }

    bos.tranggarankas.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.tranggarankas.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'periode', caption: 'Periode', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'oprasional', caption: 'Oprasional', size: '100px', sortable: false , render:'float:2'},
                { field: 'investasi', caption: 'Investasi', size: '100px', sortable: false , render:'float:2'},
                { field: 'pendanaan', caption: 'Pendanaan', size: '100px', sortable: false , render:'float:2'},
                { field: 'nominal', caption: 'Nominal', size: '100px', sortable: false , render:'float:2'},
                { field: 'username', caption: 'username', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'datetime', caption: 'Nominal', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'cmdcetak', caption: ' ', size: '80px', sortable: false },
                { field: 'cmdcetak2', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.tranggarankas.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.tranggarankas.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.tranggarankas.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.tranggarankas.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.tranggarankas.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail
    bos.tranggarankas.grid2_data    = null ;
    bos.tranggarankas.grid2_loaddata= function(){
        var periode = bos.tranggarankas.obj.find("#periode").val();
        this.grid2_data 		= {'periode':periode} ;
    }
    bos.tranggarankas.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            limit 	: 100 ,
            url 	: bos.tranggarankas.base_url + "/loadgrid2",
            postData: this.grid2_data ,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'no', caption: 'No', size: '50px', sortable: false,frozen:true},
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,frozen:true},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false,frozen:true},
                { field: 'aruskas', caption: 'Arus Kas', size: '50px', sortable: false,frozen:true},
                { field: 'b1', caption: 'Januari', size: '100px', sortable: false , render:'float:2',editable:{type:'text'}},
                { field: 'b2', caption: 'Februari', size: '100px', sortable: false , render:'float:2',editable:{type:'text'}},
                { field: 'b3', caption: 'Maret', size: '100px', sortable: false , render:'float:2',editable:{type:'int'}},
                { field: 'b4', caption: 'April', size: '100px', sortable: false , render:'float:2',editable:{type:'int'}},
                { field: 'b5', caption: 'Mei', size: '100px', sortable: false , render:'float:2',editable:{type:'int'}},
                { field: 'b6', caption: 'Juni', size: '100px', sortable: false , render:'float:2',editable:{type:'int'}},
                { field: 'b7', caption: 'Juli', size: '100px', sortable: false , render:'float:2',editable:{type:'int'}},
                { field: 'b8', caption: 'Agustus', size: '100px', sortable: false , render:'float:2',editable:{type:'int'}},
                { field: 'b9', caption: 'September', size: '100px', sortable: false , render:'float:2',editable:{type:'int'}},
                { field: 'b10', caption: 'Oktober', size: '100px', sortable: false , render:'float:2',editable:{type:'int'}},
                { field: 'b11', caption: 'November', size: '100px', sortable: false , render:'float:2',editable:{type:'int'}},
                { field: 'b12', caption: 'Desember', size: '100px', sortable: false , render:'float:2',editable:{type:'int'}},
                { field: 'total', caption: 'Total', size: '100px', sortable: false , render:'float:2'},
            ],
            onChange: function(event){
                var total = 0 ;
                var n = 0;
                var arr = [];
                for(var i = 4; i<=15 ; i++){
                    n++;
                    var nilai = this.getCellValue(event.index,i);
                    if(event.column == i){
                        nilai = event.value_new;
                    }
                    total += Number(nilai);
                }
                
                if(event.column == 4)this.set(event.recid,{total:total,b1:event.value_new});
                if(event.column == 5)this.set(event.recid,{total:total,b2:event.value_new});
                if(event.column == 6)this.set(event.recid,{total:total,b3:event.value_new});
                if(event.column == 7)this.set(event.recid,{total:total,b4:event.value_new});
                if(event.column == 8)this.set(event.recid,{total:total,b5:event.value_new});
                if(event.column == 9)this.set(event.recid,{total:total,b6:event.value_new});
                if(event.column == 10)this.set(event.recid,{total:total,b7:event.value_new});
                if(event.column == 11)this.set(event.recid,{total:total,b8:event.value_new});
                if(event.column == 12)this.set(event.recid,{total:total,b9:event.value_new});
                if(event.column == 13)this.set(event.recid,{total:total,b10:event.value_new});
                if(event.column == 14)this.set(event.recid,{total:total,b11:event.value_new});
                if(event.column == 15)this.set(event.recid,{total:total,b12:event.value_new});
                bos.tranggarankas.grid2_hitungtotal();
            },
            onLoad: function(event){
                var datagrid2 = event.xhr.responseText;
                datagrid2 = JSON.parse(datagrid2);
                var listdata = datagrid2.records;
                var opr = 0 ;
                var pend = 0 ;
                var inv = 0 ;
                var tot = 0;
                for (var key in listdata) {
                    var jumlah = listdata[key].total;
                    var aruskas = listdata[key].aruskas;
                    if(aruskas == "o") opr += Number(jumlah);
                    if(aruskas == "i") inv += Number(jumlah);
                    if(aruskas == "p") pend += Number(jumlah);
                    tot += Number(jumlah);
                }
                bos.tranggarankas.obj.find("#oprasional").val($.number(opr,2));
                bos.tranggarankas.obj.find("#pendanaan").val($.number(pend,2));
                bos.tranggarankas.obj.find("#investasi").val($.number(inv,2));
                bos.tranggarankas.obj.find("#total").val($.number(tot,2));
            }

        });


    }

    bos.tranggarankas.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.tranggarankas.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    
    bos.tranggarankas.grid2_setdata	= function(){
        w2ui[this.id + '_grid2'].postData 	= this.grid2_data ;
    }
    
    bos.tranggarankas.grid2_reloaddata	= function(){
        this.grid2_loaddata() ;
        this.grid2_setdata() ;
        this.grid2_reload() ;
    }

    
    bos.tranggarankas.grid2_hitungtotal = function(){
        var nRows = w2ui[this.id + '_grid2'].records.length;
        var opr = 0 ;
        var pend = 0 ;
        var inv = 0 ;
        var tot = 0;
        for(i=0;i< nRows;i++){
            var jumlah = w2ui[this.id + '_grid2'].getCellValue(i,16);
            var aruskas = w2ui[this.id + '_grid2'].getCellValue(i,3);
            if(aruskas == "o") opr += Number(jumlah);
            if(aruskas == "i") inv += Number(jumlah);
            if(aruskas == "p") pend += Number(jumlah);
            tot += Number(jumlah);
        }
        this.obj.find("#oprasional").val($.number(opr,2));
        this.obj.find("#pendanaan").val($.number(pend,2));
        this.obj.find("#investasi").val($.number(inv,2));
        this.obj.find("#total").val($.number(tot,2));
    }

    bos.tranggarankas.cmdcetak	= function(tahun){
        bjs_os.form_report( this.base_url + '/showreport?tahun=' + tahun) ;
    }

    bos.tranggarankas.cmdcetak2	= function(tahun){
        bjs_os.form_report( this.base_url + '/showreport2?tahun=' + tahun) ;
    }


    bos.tranggarankas.init 			= function(){
        this.obj.find("#oprasional").val("0") ;
        this.obj.find("#pendanaan").val("0") ;
        this.obj.find("#investasi").val("0") ;
        this.obj.find("#total").val("0") ;
        bjs.ajax(this.url + '/init') ;
        w2ui[this.id + '_grid2'].clear();


    }

    bos.tranggarankas.settab 		= function(n){
        this.obj.find("#tagnkas button:eq("+n+")").tab("show") ;
    }

    bos.tranggarankas.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.tranggarankas.grid1_render() ;
            bos.tranggarankas.init() ;
        }else{
            bos.tranggarankas.grid2_reloaddata() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            
        }
    }

    bos.tranggarankas.initcomp		= function(){
        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;
        this.grid2_loaddata() ;
        this.grid2_load() ;
    }

    bos.tranggarankas.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.tranggarankas.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.tranggarankas.grid1_destroy() ;
            bos.tranggarankas.grid2_destroy() ;
        }) ;
    }

    bos.tranggarankas.objs = bos.tranggarankas.obj.find("#cmdsave") ;
    bos.tranggarankas.initfunc	   = function(){

        this.obj.find('form').on("submit", function(e){
            //this.obj.find("#cmdsave").on("click", function(e){
            e.preventDefault() ;
            if( bjs.isvalidform(this) ){
                var datagrid2 =  w2ui['bos-form-tranggarankas_grid2'].records;
                datagrid2 = JSON.stringify(datagrid2);
                bjs.ajax( bos.tranggarankas.base_url + '/saving', bjs.getdataform(this)+"&grid2="+datagrid2 , bos.tranggarankas.cmdsave) ;
            }

        }) ;

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.tranggarankas.grid1_reloaddata();
        });

        this.obj.find("#periode").on("change", function(e){
            bos.tranggarankas.grid2_reloaddata();
        });
    }
    $(function(){
        bos.tranggarankas.initcomp() ;
        bos.tranggarankas.initcallback() ;
        bos.tranggarankas.initfunc() ;
    });
</script>