<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Import Absensi Karyawan</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trimportabsensikaryawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%;">
            <table width = '100%'>
                <tr>
                    <td class="osxtable form">
                        <table>
                            <tr>
                                <td width="100px"><label for="periode">Periode</label> </td>
                                <td width="5px">:</td>
                                <td width="400px">
                                    <select name="periode" id="periode" class="form-control select" style="width:100%" data-placeholder="Periode"></select>
                                </td>
                                <td width="85px">
                                    <input readonly style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                </td>
                                <td width="5px">sd</td>
                                <td width="85px">
                                    <input readonly style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                </td>
                                <td>*) Pilih periode dan mesin terlebih dahulu sebelum melakukan upload</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="osxtable form">
                        <table>
                            <tr>
                                <td width="100px"><label for="mesin">Mesin</label> </td>
                                <td width="5px">:</td>
                                <td width="400px">
                                    <select name="mesin" id="mesin" class="form-control select" style="width:100%" data-placeholder="Mesin"></select>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="osxtable form">
                        Upload CSV <span id="idlimportabsensi"></span>
                        <table width = "100%">
                            <tr><td height = '20px'><input type="file" id="importabsensi" accept="csv/*" class="fupload"></td></tr>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td height = "500px" >
                        <div id="grid1" class="full-height"></div>
                    </td>
                </tr>
                <tr>
                    <td class="osxtable form">
                        <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
                    </td>
                </tr>
            </table>                         
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    bos.trimportabsensikaryawan.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false,style:'text-align:center',frozen:true},
                { field: 'nip', caption: 'NIP', size: '100px', sortable: false,style:'text-align:center',frozen:true},
                { field: 'nama', caption: 'Nama', size: '220px', sortable: false,frozen:true},
                { field: 'bagian', caption: 'Bagian', size: '120px', sortable: false,frozen:true},
                { field: 'jabatan', caption: 'Jabatan', size: '120px', sortable: false,frozen:true},
                { field: 'pin', caption: 'PIN', size: '50px', sortable: false,style:'text-align:center'},
                { field: 'namamesin', caption: 'Nama Finger', size: '220px', sortable: false},
                { field: 'jwmasuk', caption: 'Jw Masuk', size: '100px', sortable: false,style:'text-align:center'},
                { field: 'masuk', caption: 'Jam Masuk', size: '100px', sortable: false,style:'text-align:center'},
                { field: 'terlambat', caption: 'Terlambat', size: '100px', sortable: false,style:'text-align:center'},
                { field: 'jwpulang', caption: 'Jw Pulang', size: '100px', sortable: false,style:'text-align:center'},
                { field: 'pulang', caption: 'Jam Pulang', size: '100px', sortable: false,style:'text-align:center'},
                { field: 'plgcepat', caption: 'Plg. Cepat', size: '100px', sortable: false,style:'text-align:center'},
                { field: 'lembur', caption: 'Lembur', size: '100px', sortable: false,style:'text-align:center'},
                { field: 'status', caption: 'Status', size: '100px', sortable: false,style:'text-align:center'}
            ]
        });


    }

    bos.trimportabsensikaryawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trimportabsensikaryawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
   
    bos.trimportabsensikaryawan.init				= function(){
        bjs.ajax(this.url + "/init") ;
    }
    
    bos.trimportabsensikaryawan.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    

    bos.trimportabsensikaryawan.initcomp	= function(){
        // bjs.initselect({
		// 	class : "#" + this.id + " .select"
		// }) ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.obj.find('#periode').select2({
            ajax: {
                url: bos.trimportabsensikaryawan.base_url + '/seekperiode',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.obj.find('#mesin').select2({
            ajax: {
                url: bos.trimportabsensikaryawan.base_url + '/seekmesin',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
    }

    bos.trimportabsensikaryawan.initcallback	= function(){


        this.obj.on("remove",function(){
            bos.trimportabsensikaryawan.grid1_destroy() ;
        }) ;
    }

    bos.trimportabsensikaryawan.objs = bos.trimportabsensikaryawan.obj.find("#cmdsave") ;
    bos.trimportabsensikaryawan.initfunc 		= function(){
        this.init() ;
        this.grid1_load() ;
        //this.grid1_loaddata();
       

        this.obj.find("#periode").on("change",function(){
            var periode = bos.trimportabsensikaryawan.obj.find("#periode").val();
            bjs.ajax(bos.trimportabsensikaryawan.base_url + '/loadperiode', 'periode=' + periode);
        });

        this.obj.find(".fupload").on("change", function(e){
            w2ui['bos-form-trimportabsensikaryawan_grid1'].clear();

			bos.trimportabsensikaryawan.uname	= $(this).attr("id") ;
			e.preventDefault() ;

            bos.trimportabsensikaryawan.fal    = e.target.files ;
            bos.trimportabsensikaryawan.gfal   = new FormData() ;
            $.each(bos.trimportabsensikaryawan.fal, function(key,val){
              bos.trimportabsensikaryawan.gfal.append(key,val) ;
            }) ;

            var periode = bos.trimportabsensikaryawan.obj.find("#periode").val();
            var mesin = bos.trimportabsensikaryawan.obj.find("#mesin").val();
            var tglawal = bos.trimportabsensikaryawan.obj.find("#tglawal").val();
            var tglakhir = bos.trimportabsensikaryawan.obj.find("#tglakhir").val();
            bos.trimportabsensikaryawan.gfal.append("periode",periode);
            bos.trimportabsensikaryawan.gfal.append("mesin",mesin);
            bos.trimportabsensikaryawan.gfal.append("tglawal",tglawal);
            bos.trimportabsensikaryawan.gfal.append("tglakhir",tglakhir);

            bos.trimportabsensikaryawan.obj.find("#idl" + bos.trimportabsensikaryawan.uname).html("sys_periode_mesin<i class='fa fa-spinner fa-pulse'></i>");
            bos.trimportabsensikaryawan.obj.find("#id" + bos.trimportabsensikaryawan.uname).html("") ;
            
            bjs.ajaxfile(bos.trimportabsensikaryawan.base_url + "/uploadimport/" + bos.trimportabsensikaryawan.uname , bos.trimportabsensikaryawan.gfal, this) ;
		}) ;

        this.obj.find('form').on("submit", function(e){
            //this.obj.find("#cmdsave").on("click", function(e){
            e.preventDefault() ;
            if(confirm("Data absensi diimport??")){
                if( bjs.isvalidform(this) ){
                    var datagrid1 =  w2ui['bos-form-trimportabsensikaryawan_grid1'].records;
                    datagrid1 = JSON.stringify(datagrid1);
                    bjs.ajax( bos.trimportabsensikaryawan.base_url + '/saving', bjs.getdataform(this)+"&grid1="+datagrid1 , bos.trimportabsensikaryawan.cmdsave) ;
                }
            }

        }) ;
    }
    
    

    $(function(){
        bos.trimportabsensikaryawan.initcomp() ;
        bos.trimportabsensikaryawan.initcallback() ;
        bos.trimportabsensikaryawan.initfunc() ;
    }) ;
</script>
