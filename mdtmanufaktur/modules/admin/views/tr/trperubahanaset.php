<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpel">
                        <button class="btn btn-tab tpel active" href="#tpel_1" data-toggle="tab" >Daftar Perubahan Aset & Inventaris</button>
                        <button class="btn btn-tab tpel" href="#tpel_2" data-toggle="tab">Perubahan Aset & Inventaris</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trperubahanaset.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpel_1" style="padding-top:5px;">
                    <div class="row">
                        <div class="col-md-1">
                            <div class="form-group">
                                <label>Tgl Awal</label>
                                <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label>Tgl Akhir</label>
                                <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Cabang</label>
                                <div class="input-group">
                                    <select name="skd_cabang" id="skd_cabang" class="form-control s2" style="width:100%"
                                                            data-placeholder="Pilih Cabang" data-sf="load_cabang"></select>
                                    <span class="input-group-addon">
                                        <span id="cmdcabang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                                    </span>
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group full-width">
                                <label>&nbsp;</label>
                                <div class="input-group full-width">
                                        <button type="button" class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="grid1" class="full-height" style="height: calc(100% - 50px);"></div>
                    
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpel_2">
                    <table width='100%'>
                        <tr>
                            <td valign = "top" width='50%'>
                                <table width='100%'>
                                    <tr>
                                        <td width='100%'>
                                            <b> :: Data Aset :: </b>
                                            <table class="osxtable form">
                                                <tr>
                                                    <td width="150px"><label for="cabang">Cabang</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <select name="cabang" id="cabang" class="form-control scons" style="width:100%"
                                                                data-placeholder="Cabang / Kantor" data-sf="load_cabang" required></select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="150px"><label for="kode">Aset</label> </td>
                                                    <td width="5px">:</td>
                                                    <td >
                                                        <select name="kode" id="kode" class="form-control scons" style="width:100%"
                                                                data-placeholder="Aset" data-sf="load_aset" required></select>
                                                    </td>
                                                </tr>  
                                                <tr>
                                                    <td><label for="faktur">Faktur</label> </td>
                                                    <td>:</td>
                                                    <td >
                                                        <input readonly type="text" maxlength="8" id="faktur" name="faktur" class="form-control" placeholder="faktur" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="tglperolehan">Tgl</label> </td>
                                                    <td>:</td>
                                                    <td>
                                                        <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                                    </td>
                                                </tr> 
                                                                                  
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width='100%'>
                                            <b> :: Data Awal :: </b>(Data Sebelum Perubahan)
                                            <div class="row">
                                                <div class="col-md-11 bg-warning">                                                    
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Golongan</label>
                                                            <div id="da_golongan">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Kelompok</label>
                                                            <div id="da_kelompok">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Harga Perolehan</label>
                                                            <div id="da_hp">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Lama Penyusutan (Bulan)</label>
                                                            <div id="da_lama">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Penyusutan</label>
                                                            <div id="da_penyakhir">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Nilai Buku</label>
                                                            <div id="da_nb">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                        <label>Nilai Residu</label>
                                                            <div id="da_residu">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br/>
                                            <b>:: Data Akhir :: </b>(Data Setelah Perubahan)
                                            <div class="row">
                                                <div class="col-md-11 bg-info">
                                                <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Harga Perolehan</label>
                                                            <div id="dz_hp">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Lama Penyusutan (Bulan)</label>
                                                            <div id="dz_lama">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Nilai Buku</label>
                                                            <div id="dz_nb">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                
                            </td>
                            <td width='50%' valign='top'>
                                <b>:: Data Perubahan</b>
                                <table width='100%'>
                                    <tr>
                                        <td width='100%'>
                                            <table class="osxtable form"> 
                                                <tr>
                                                    <td><label for="kelaset">Kelompok Aset</label> </td>
                                                    <td>:</td>
                                                    <td>
                                                        <select name="kelaset" id="kelaset" class="form-control scons" style="width:100%"
                                                                data-placeholder="Kelompok Aset" data-sf="load_kelaset" required></select>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td width="150px"><label for="lama">Lama Bulan (+/-)</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input maxlength="5" type="text" name="lama" id="lama" class="form-control number" value="0">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="hp">Nilai Tambah </label> </td>
                                                    <td>:</td>
                                                    <td>
                                                        <input  maxlength="20" type="text" name="hp_tambah" id="hp_tambah" class="form-control number" value="0">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="jenis">Jenis Peny</label> </td>
                                                    <td>:</td>
                                                    <td>
                                                        <div class="radio-inline">
                                                            <label>
                                                                <input type="radio" name="jenis" class="jenis" value="1" checked>
                                                                Garis Lurus
                                                            </label>
                                                            &nbsp;&nbsp;
                                                        </div>
                                                        <div class="radio-inline">
                                                            <label>
                                                                <input type="radio" name="jenis" class="jenis" value="2">
                                                                Saldo Menurun
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="tarifpeny">Tarif Peny (%)</label> </td>
                                                    <td>:</td>
                                                    <td>
                                                        <input  style="width:50px" maxlength="7" type="text" name="tarifpeny" id="tarifpeny" class="form-control number" value="0">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><label for="residu">Nilai Residu</label> </td>
                                                    <td>:</td>
                                                    <td>
                                                        <input  style="width:200px" maxlength="20" type="text" name="residu" id="residu" class="form-control number" value="0">

                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width='100%'>
                                            
                                            <table class="osxtable form">                                    
                                                <tr>
                                                    <td width = '300px' ><label for="vendor">Vendor / Supplier</label> </td>
                                                    <td><label for="hutang">Hutang</label> </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td><select name="vendor" id="vendor" class="form-control scons" style="width:100%"
                                                        data-placeholder="Vendor / Supplier" data-sf="load_supplier"></select></td>
                                                    <td><input maxlength="20" type="text" name="hutang" id="hutang" class="form-control number" value="0"></td>
                                                    <td><button type="button" class="btn btn-primary pull-right" id="cmdok">OK</button></td>
                                                </tr>
                                                
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height = '300px'>
                                            <div id="grid2" class="full-height"></div>
                                        </td>
                                    </tr>
                                </table>
                                
                            </td>
                        </tr>
                    </table>                    
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.trperubahanaset.aset = {};
    bos.trperubahanaset.grid1_data 	 = null ;
    bos.trperubahanaset.grid1_loaddata= function(){

        mdl_cabang.vargr.selection = bos.trperubahanaset.obj.find('#skd_cabang').val();

        this.grid1_data 		= {
            "tglawal"	   : this.obj.find("#tglawal").val(),
            "tglakhir"	   : this.obj.find("#tglakhir").val(),
            'skd_cabang':JSON.stringify(mdl_cabang.vargr.selection)
        } ;

    }

    bos.trperubahanaset.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.trperubahanaset.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false, 
            columns: [
				{ field: 'faktur', caption: 'Faktur', size: '120px', sortable: false,frozen:true},
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,frozen:true},
                { field: 'tgl', caption: 'Tgl Perubahan', size: '100px', sortable: false,frozen:true},
                { field: 'keterangan', caption: 'Keterangan', size: '150px', sortable: false,frozen:true},
                { field: 'cabang', caption: 'Cabang', size: '80px', sortable: false},
				{ field: 'vendors', caption: 'Vendor', size: '80px', sortable: false},
                { field: 'golongan', caption: 'Golongan', size: '100px', sortable: false},
                { field: 'kelompok', caption: 'Kelompok', size: '100px', sortable: false},
                { field: 'lama', caption: 'Lama', size: '80px', sortable: false},
                { field: 'hargaperolehan', caption: 'Harga Perolehan', size: '100px',render:'float:2', sortable: false},
                { field: 'unit', caption: 'Unit', size: '80px',render:'float:2', sortable: false},
                { field: 'jenispenyusutan', caption: 'Jenis Penyusutan', size: '100px', sortable: false},
                { field: 'tarifpenyusutan', caption: 'Tarif Penyusutan', size: '100px',render:'float:2', sortable: false},
                { field: 'residu', caption: 'Nilai Residu', size: '100px',render:'float:2', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trperubahanaset.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trperubahanaset.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trperubahanaset.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trperubahanaset.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trperubahanaset.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail pembelian
    bos.trperubahanaset.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
            multiSelect : false,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'vendor', caption: 'Vendor / Supplier', size: '100px', sortable: false },
                { field: 'namavendor', caption: 'Nama Vendor / Supplier', size: '200px', sortable: false },
                { field: 'hutang',render:'float:2', caption: 'Hutang', size: '100px', editable:{type:'int'},sortable: false, style:'text-align:right'},
                { field: 'act', caption: ' ', size: '80px', sortable: false }
            ],
            summary: [
                { recid: "ZZZZ", vendor: '', namavendor: 'TOTAL', hutang: 0 }
            ],
            onChange: function(event){
                event.onComplete = function () {
                        w2ui[event.target].save();
                        bos.trperubahanaset.grid2_hitungtotal();
                }
            }
        });
    }

    bos.trperubahanaset.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trperubahanaset.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.trperubahanaset.grid2_append    = function(vendor,hutang){
        var datagrid  = w2ui[this.id + '_grid2'].records;
        var lnew      = true;
        var nQty      = 1;
        var recid     = "";
        if(no <= datagrid.length){
            recid = no;
            w2ui[this.id + '_grid2'].set(recid,{stock: kode, namastock: keterangan, qty: qty, satuan:satuan});
        }else{
            recid = no;
            var Hapus = "<button type='button' onclick = 'bos.trperubahanaset.grid2_deleterow("+recid+")' class='btn btn-danger btn-grid'>Delete</button>";
            w2ui[this.id + '_grid2'].add([
                { recid:recid,
                 no: no,
                 stock: kode,
                 namastock: keterangan,
                 qty: qty,
                 satuan:satuan,
                 cmddelete:Hapus}
            ]) ;
        }
        bos.trperubahanaset.initdetail();
        bos.trperubahanaset.obj.find("#vendor").focus() ;
    }

    bos.trperubahanaset.grid2_deleterow = function(recid){
        if(confirm("Item di hapus dari detail vendor???"+recid)){
            w2ui[this.id + '_grid2'].select(recid);
            w2ui[this.id + '_grid2'].delete(true);
            bos.trperubahanaset.grid2_hitungtotal();
        }
    }

    bos.trperubahanaset.grid2_hitungtotal 			= function(){
        bos.trperubahanaset.gr2 = w2ui[this.id + '_grid2'].records;
        bos.trperubahanaset.tothtg = 0 ;
        $.each(bos.trperubahanaset.gr2, function(i, v){
            bos.trperubahanaset.tothtg += v.hutang;
        });
        w2ui[this.id + '_grid2'].set("ZZZZ",{hutang:bos.trperubahanaset.tothtg});
    }

    bos.trperubahanaset.hitung_perubahan = function(){
            console.log(this.aset);

            lama = string_2n(this.obj.find("#lama").val());
            hp = string_2n(this.obj.find("#hp_tambah").val());

            lamatotal = lama + this.aset.lama;
            lamasisa = Number(this.aset.sisa_lama) + Number(lama);
            hptotal = Number(hp) + Number(this.aset.hp);
            nbtotal = Number(hp) + Number(this.aset.nilaibuku);

            console.log(nbtotal);
            this.obj.find("#dz_lama").html(lamatotal + " sisa "+ lamasisa);
            this.obj.find("#dz_hp").html($.number(hptotal,2));
            this.obj.find("#dz_nb").html($.number(nbtotal,2));
        
    }

    bos.trperubahanaset.cmdedit		= function(faktur){
        bos.trperubahanaset.act = 1;

        bjs.ajax(this.url + '/editing', 'faktur=' + faktur);
    }

    bos.trperubahanaset.cmddelete		= function(faktur){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'faktur=' + faktur);
        }
    }

    bos.trperubahanaset.init				= function(){
        with(this.obj){
            find("#cabang").sval("") ;
            find("#kode").sval("") ;
		    find("#faktur").val("") ;
            find("#kelaset").sval("") ;
            find("#tgl").val("<?=date("d-m-Y")?>");
            find("#lama").val("0") ;
            find("#hp_tambah").val("0") ;
            find("#tarifpeny").val("0") ;
            find("#residu").val("0") ;
            
            
            find("#da_golongan").html("&nbsp;");
            find("#da_kelompok").html("&nbsp;");
            find("#da_hp").html("&nbsp;");
            find("#da_lama").html("&nbsp;");
            find("#da_nb").html("&nbsp;");
            find("#da_penyakhir").html("&nbsp;");
            find("#da_residu").html("&nbsp;");
        }

        bos.trperubahanaset.setopt("jenis","1");
        bos.trperubahanaset.act = 0;


        bjs.ajax(this.url + "/init") ;
        bos.trperubahanaset.getfaktur();
        bos.trperubahanaset.initdetail();

        w2ui[this.id + '_grid2'].clear();
        w2ui[this.id + '_grid2'].add({recid: "ZZZZ", vendor: '', namavendor: 'TOTAL', hutang: 0,w2ui:{summary:true}});

    }

    bos.trperubahanaset.getfaktur 			= function(){
        console.log(bos.trperubahanaset.act);
        if(bos.trperubahanaset.act == 0){
            bos.trperubahanaset.cabang = bos.trperubahanaset.obj.find("#cabang").val();
            bos.trperubahanaset.tgl = bos.trperubahanaset.obj.find("#tgl").val();
            bjs.ajax(this.url + '/getfaktur','cabang='+bos.trperubahanaset.cabang+"&tgl="+bos.trperubahanaset.tgl,'',function(hasil){
                bos.trperubahanaset.obj.find("#faktur").val(hasil);
            }) ;
        }        
    }

    bos.trperubahanaset.dataaset_edit 			= function(aset,tgl){
        bjs.ajax( bos.trperubahanaset.url + '/dataaset',"kode="+aset+"&tgl="+tgl,'',function(hasil){
            hasil = JSON.parse(hasil);
            //bos.trperubahanaset.obj.find("#da_golongan").html(data.gol);
            bos.trperubahanaset.aset = hasil;
            with(bos.trperubahanaset.obj){
                find("#da_golongan").html(hasil.gol);
                find("#da_kelompok").html(hasil.kel);
                find("#da_hp").html($.number(hasil.hp,2));
                find("#da_lama").html(hasil.lama + " sisa " + hasil.sisa_lama);
                find("#da_nb").html($.number(hasil.nilaibuku,2));
                find("#da_penyakhir").html($.number(hasil.penyakhir,2));
                find("#da_residu").html($.number(hasil.residu,2));
            }
            bos.trperubahanaset.hitung_perubahan();
        }) ;
    }

    bos.trperubahanaset.dataaset 			= function(aset,tgl){
        console.log(tgl);
        bjs.ajax( bos.trperubahanaset.url + '/dataaset',"kode="+aset+"&tgl="+tgl,'',function(hasil){
            hasil = JSON.parse(hasil);
            //bos.trperubahanaset.obj.find("#da_golongan").html(data.gol);
            bos.trperubahanaset.aset = hasil;
            with(bos.trperubahanaset.obj){
                find("#da_golongan").html(hasil.gol);
                find("#da_kelompok").html(hasil.kel);
                find("#da_hp").html($.number(hasil.hp,2));
                find("#da_lama").html(hasil.lama + " sisa " + hasil.sisa_lama);
                find("#da_nb").html($.number(hasil.nilaibuku,2));
                find("#da_penyakhir").html($.number(hasil.penyakhir,2));
                find("#da_residu").html($.number(hasil.residu,2));
                find("#tarifpeny").val($.number(hasil.tarifpeny,2)) ;
                find("#residu").val($.number(hasil.residu,2)) ;
                find("#kelaset").sval([{"id" : hasil.kelompok, "text" : hasil.kel}]) ;
            }
            bos.trperubahanaset.hitung_perubahan();
        }) ;
    }

    bos.trperubahanaset.initdetail 			= function(){
        this.obj.find("#vendor").sval("") ;
        this.obj.find("#hutang").val("") ;
    }

    bos.trperubahanaset.settab 		= function(n){
        this.obj.find("#tpel button:eq("+n+")").tab("show") ;
    }

    bos.trperubahanaset.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trperubahanaset.grid1_render() ;
            bos.trperubahanaset.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            bos.trperubahanaset.grid2_reload() ;

            this.obj.find("#kode").focus() ;
        }
    }

    bos.trperubahanaset.initcomp	= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons",
            clear : false
        }) ;

        bjs.initselect({
            class : "#" + this.id + " .s2",
            multi : true
        }) ;

        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag


        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;
    }

    bos.trperubahanaset.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trperubahanaset.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.trperubahanaset.grid1_destroy() ;
            bos.trperubahanaset.grid2_destroy() ;
        }) ;
    }
    bos.trperubahanaset.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }
    bos.trperubahanaset.objs = bos.trperubahanaset.obj.find("#cmdsave") ;
    bos.trperubahanaset.initfunc 		= function(){
        this.init() ;
        this.obj.find("#cmdrefresh").on("click", function(e){
            e.preventDefault() ;
            bos.trperubahanaset.grid1_reloaddata() ;
        }) ;

        this.obj.find("form").on("submit", function(e){
            e.preventDefault() ;
            if(bjs.isvalidform(this)){
                bos.trperubahanaset.hp = string_2n(bos.trperubahanaset.obj.find("#hp_tambah").val());

                if(bos.trperubahanaset.tothtg == bos.trperubahanaset.hp){
                    bos.trperubahanaset.gr2 =  w2ui['bos-form-trperubahanaset_grid2'].records;
                    bos.trperubahanaset.gr2 = JSON.stringify(bos.trperubahanaset.gr2);
                    bjs.ajax( bos.trperubahanaset.url + '/saving', bjs.getdataform(this)+"&grid2="+bos.trperubahanaset.gr2 , bos.trperubahanaset.objs,function(hasil){
                        if(hasil == "ok"){
                            bos.trperubahanaset.settab(0) ;
                            alert("perubahan berhasil disimpan,,");

                        }else{
                            alert(hasil);
                        }
                    }) ;
                }else{
                    alert("Nilai harga perolehan dan total hutang tidak sama!!");
                }                
            }
        });

        this.obj.find("#hutang").on("blur", function(e){
            bos.trperubahanaset.htg = string_2n(bos.trperubahanaset.obj.find("#hutang").val());
            bos.trperubahanaset.obj.find("#hutang").val($.number(bos.trperubahanaset.htg,2));
        });

        this.obj.find("#lama, #hp_tambah").on("blur", function(e){
            //alert("sff");
            bos.trperubahanaset.hitung_perubahan();
        });

        this.obj.find('#cabang').on("select2:selecting", function(e) { 
            bos.trperubahanaset.obj.find('#kode').attr('data-sp',e.params.args.data.id);
            bos.trperubahanaset.getfaktur();

        });
        this.obj.find('#kode').on("select2:selecting", function(e) { 
            bos.trperubahanaset.tgl = bos.trperubahanaset.obj.find("#tgl").val();
            bos.trperubahanaset.dataaset(e.params.args.data.id,bos.trperubahanaset.tgl);
        });

        this.obj.find('#tgl').on("change", function(e) { 
            bos.trperubahanaset.tgl = bos.trperubahanaset.obj.find("#tgl").val();
            bos.trperubahanaset.kode = bos.trperubahanaset.obj.find("#kode").val();
            if(bos.trperubahanaset.kode !== ""){
                bos.trperubahanaset.dataaset(bos.trperubahanaset.kode,bos.trperubahanaset.tgl);
            }
        });

        this.obj.find('#kelaset').on("select2:selecting", function(e) { 
            bos.trperubahanaset.kelaset = bos.trperubahanaset.obj.find("#kelaset").val();
            bjs.ajax( bos.trperubahanaset.url + '/getkelaset',"kode="+e.params.args.data.id,'',function(data){
                data = JSON.parse(data);
                bos.trperubahanaset.lama = Number(data['lama']) * 12;
                bos.trperubahanaset.obj.find("#lama").val(bos.trperubahanaset.lama);
                bos.trperubahanaset.hitung_perubahan();
            }) ;
        });

        this.obj.find("#cmdok").on("click", function(e){
            bos.trperubahanaset.vendor = bos.trperubahanaset.obj.find("#vendor").val();
            bos.trperubahanaset.namavendor = bos.trperubahanaset.obj.find("#vendor").text();
            bos.trperubahanaset.hutang = string_2n(bos.trperubahanaset.obj.find("#hutang").val());
            if(bos.trperubahanaset.vendor !== "" && bos.trperubahanaset.hutang > 0){
                if( w2ui['bos-form-trperubahanaset_grid2'].get(bos.trperubahanaset.vendor) !== null ){
                    w2ui['bos-form-trperubahanaset_grid2'].set(bos.trperubahanaset.vendor,{
                        hutang: bos.trperubahanaset.hutang
                    });
                }else{
                    bos.trperubahanaset._act = "<button type='button' onclick = 'bos.trperubahanaset.grid2_deleterow("+bos.trperubahanaset.vendor+")' class='btn btn-danger btn-grid'>Delete</button>";
                    w2ui['bos-form-trperubahanaset_grid2'].add([{ 
                        recid: bos.trperubahanaset.vendor,
                        vendor: bos.trperubahanaset.vendor,
                        namavendor: bos.trperubahanaset.namavendor,
                        hutang: bos.trperubahanaset.hutang,
                        act: bos.trperubahanaset._act,
                    }]) ;
                }
                bos.trperubahanaset.initdetail();
                bos.trperubahanaset.grid2_hitungtotal();

            }else{
                alert("Data hutang vendor tidak valid!!");
            }
        }) ;

        this.obj.find('#cabang').on('change', function(){
            bos.trperubahanaset.getfaktur();
        });

        this.obj.find('#cmdcabang').on('click', function(){
            mdl_cabang.vargr.selection = bos.trperubahanaset.obj.find('#skd_cabang').val();
            mdl_cabang.open(function(r){
                r = JSON.parse(r);
                bos.trperubahanaset.cabang = [];
                $.each(r, function(i, v){
                    bos.trperubahanaset.cabang.push({'id':v.kode,'text':v.keterangan})                   
                });
                bos.trperubahanaset.obj.find('#skd_cabang').sval(bos.trperubahanaset.cabang);
            });
        });
    }
    $(function(){
        bos.trperubahanaset.initcomp() ;
        bos.trperubahanaset.initcallback() ;
        bos.trperubahanaset.initfunc() ;
        bos.trperubahanaset.init() ;
    }) ;
    

</script>
