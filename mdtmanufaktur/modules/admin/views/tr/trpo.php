<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpo">
                        <button class="btn btn-tab tpel active" href="#tpo_1" data-toggle="tab" >Daftar Purchase Order</button>
                        <button class="btn btn-tab tpel" href="#tpo_2" data-toggle="tab">Purchase Order</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trpo.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpo_1" style="padding-top:5px;">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Antara Tanggal</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                    </div>
                                    <div class="col-md-6">
                                        <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label>Gudang</label>
                                <div class="input-group">
                                    <select name="skd_gudang" id="skd_gudang" class="form-control s2" style="width:100%"
                                                            data-placeholder="Pilih Gudang" data-sf="load_gudang"></select>
                                    <span class="input-group-addon">
                                        <span id="cmdgudang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                                    </span>
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group full-width">
                                <label>&nbsp;</label>
                                <div class="input-group full-width">
                                        <button type="button" class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="grid1" class="full-height" style="height: calc(100% - 50px);"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpo_2">
                    <table width="100%">
                        <tr>
                            <td width="100%" class="osxtable form">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="faktur">Faktur</label> </td>
                                        <td width="1%">:</td>
                                        <td width="35%">
                                            <input type="text" id="faktur" name="faktur" class="form-control" placeholder="Faktur" required readonly>
                                        </td>
                                        <td width="14%"><label for="gudang">Gudang</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <select name="gudang" id="gudang" class="form-control scons" style="width:100%"
                                                    data-placeholder="Gudang"  data-sf="load_gudang" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="tgl">Tanggal</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                        </td>
                                        <td width="14%"><label for="supplier">Supplier</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <select name="supplier" id="supplier" class="form-control scons" style="width:100%"
                                                    data-placeholder="Supplier" data-sf="load_supplier" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"></td>
                                        <td width="1%"></td>
                                        <td ></td>
                                        <td width="14%"><label for="fktpr">Faktur SPP</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <div width = '100%' class="input-group">
                                                <input type="text" id="fktpr" name="fktpr" class="form-control" placeholder="Faktur SPP">
                                                <span class="input-group-btn">
                                                    <button class="form-control btn btn-info" type="button" id="cmdpr"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "20px" class="osxtable form">
                                <table>
                                    <tr>
                                        <td><label for="stock">Bahan Baku</label> </td>
                                        <td><label>Nama BB</label> </td>
                                        <td><label>Spesifikasi</label> </td>
                                        <td><label for="harga">Harga</label> </td>
                                        <td><label for="qty">Qty</label> </td>
                                        <td><label>Satuan</label> </td>
                                        <td><label>Jumlah</label></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" id="stock" name="stock" class="form-control" placeholder="Stock">
                                                <span class="input-group-btn">
                                                    <button class="form-control btn btn-info" type="button" id="cmdstock"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td><input type="text" id="namastock" readonly name="namastock" class="form-control" placeholder="Nama Stock"></td>
                                        <td><input type="text" id="spesifikasi" name="spesifikasi" class="form-control" placeholder="Spesifikasi"></td>
                                        <td><input maxlength="20" type="text" name="harga" id="harga" class="form-control number" value="0"></td>
                                        <td><input maxlength="10" type="text" name="qty" id="qty" class="form-control number" value="0"></td>
                                        <td><input type="text" id="satuan" readonly name="satuan" class="form-control" placeholder="Satuan"></td>
                                        <td><input maxlength="20" readonly type="text" name="jumlah" id="jumlah" class="form-control number" value="0"></td>
                                        <td><button type="button" class="btn btn-primary pull-right" id="cmdok">OK</button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "210px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                        <tr>
                            <td height = "20px" class="osxtable form">
                                <table>
                                    <tr>
                                        <td><label>Subtotal</label> </td>
                                        <td><label>Total</label> </td>
                                    </tr>
                                    <tr>
                                        <td><input maxlength="20" readonly type="text" name="subtotal" id="subtotal" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" readonly type="text" name="total" id="total" class="form-control number" value="0"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<div class="modal fade" id="wrap-pencarianpr-d" role="dialog" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="wm-title">Daftar Faktur SPP</h4>
            </div>
            <div class="modal-body">
                <div id="grid4" style="height:250px"></div>
            </div>
            <div class="modal-footer">
                *Pilih Faktur SPP
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    //grid daftar po
    bos.trpo.grid1_data    = null ;
    bos.trpo.grid1_loaddata= function(){
        mdl_gudang.vargr.selection = bos.trpo.obj.find('#skd_gudang').val();        
        this.grid1_data 		= {'tglawal':bos.trpo.obj.find("#tglawal").val(),
                                    'tglakhir':bos.trpo.obj.find("#tglakhir").val(),
                                    'skd_gudang':JSON.stringify(mdl_gudang.vargr.selection)} ;
    }

    bos.trpo.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trpo.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers  : true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'fktpr', caption: 'Fkt PR', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'supplier', caption: 'Supplier', size: '300px', sortable: false},
                { field: 'total', caption: 'Total', size: '100px', sortable: false, render:'int',style:"text-align:right"},
                { field: 'gudang', caption: 'Gudang', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trpo.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trpo.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trpo.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trpo.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trpo.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail po
    bos.trpo.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
            multiSelect : false,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers :true
            },
            columns: [
                { field: 'stock', caption: 'Kode / Barcode', size: '120px', sortable: false },
                { field: 'namastock', caption: 'Nama Stock', size: '150px', sortable: false },
                { field: 'spesifikasi', caption: 'Spesifikasi', size: '300px',editable:{type:'text'}, sortable: false },
                { field: 'harga',render:'float:2', caption: 'Harga', size: '100px', sortable: false,editable:{type:'float'}, style:'text-align:right'},
                { field: 'qty',render:'float:2', caption: 'Qty', size: '70px', sortable: false,editable:{type:'float'}, style:'text-align:right'},
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false},
                { field: 'jumlah',render:'float:2', caption: 'Jumlah', size: '100px', sortable: false, style:'text-align:right'},
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ],
            onChange: function(event){
                

                bos.trpo.vbarang.kode = this.getCellValue(event.index,0);  
                bos.trpo.vbarang.spesifikasi = this.getCellValue(event.index,2);

                bos.trpo.vbarang._harga = this.getCellValue(event.index,3);
                bos.trpo.vbarang._qty = this.getCellValue(event.index,4);
                

                if(event.column == 2){
                    bos.trpo.vbarang.spesifikasi = event.value_new;
                }else if(event.column == 3){
                    bos.trpo.vbarang._harga = event.value_new;
                }else if(event.column == 4){
                    bos.trpo.vbarang._qty = event.value_new;
                }

                bos.trpo.vbarang._jml = bos.trpo.vbarang._harga * bos.trpo.vbarang._qty;
                
                event.onComplete = function () {
                    w2ui[event.target].save();
                    w2ui[event.target].set(event.recid, { jumlah :  bos.trpo.vbarang._jml });
                    w2ui[event.target].refresh();
                    bos.trpo.hitungsubtotal();
                }
            }
        });


    }

    bos.trpo.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trpo.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.trpo.grid2_append    = function(kode,keterangan,spesifikasi,harga,qty,satuan,jumlah){
        var datagrid  = w2ui[this.id + '_grid2'].records;
        var lnew      = true;
        var nQty      = 1;
        var nJumlah   = nQty * harga;
        var recid     = kode;
        if( w2ui[this.id + '_grid2'].get(recid) !== null){
            w2ui[this.id + '_grid2'].set(recid,{stock: kode, namastock: keterangan, spesifikasi: spesifikasi, harga: harga, qty: qty, satuan:satuan,jumlah:jumlah});
        }else{
            var Hapus = "<button type='button' onclick = 'bos.trpo.grid2_deleterow("+recid+")' class='btn btn-danger btn-grid'>Delete</button>";
            w2ui[this.id + '_grid2'].add([
                { recid:recid,
                 stock: kode,
                 namastock: keterangan,
                 spesifikasi: spesifikasi,
                 harga: harga,
                 qty: qty,
                 satuan:satuan,
                 jumlah:jumlah,
                 cmddelete:Hapus}
            ]) ;
        }
        bos.trpo.initdetail();
        bos.trpo.obj.find("#cmdstock").focus() ;
        bos.trpo.hitungsubtotal();
    }

    bos.trpo.grid2_deleterow = function(recid){
        if(confirm("Item di hapus dari detail po???"+recid)){
            w2ui[this.id + '_grid2'].select(recid);
            w2ui[this.id + '_grid2'].delete(true);
            bos.trpo.hitungsubtotal();
        }
    }

    //grid4 daftarpr
    bos.trpo.grid4_data    = null ;
    bos.trpo.grid4_loaddata= function(){
        this.grid4_data 		= {} ;
    }

    bos.trpo.grid4_load    = function(){
        this.obj.find("#grid4").w2grid({
            name	: this.id + '_grid4',
            limit 	: 100 ,
            url 	: bos.trpo.base_url + "/loadgrid4",
            postData: this.grid4_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers : true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '100px', sortable: false},
                { field: 'tgl', caption: 'Tanggal', size: '100px', sortable: false},
                { field: 'gudang', caption: 'Gudang', size: '200px', sortable: false },
                { field: 'cmdpilih', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trpo.grid4_setdata	= function(){
        w2ui[this.id + '_grid4'].postData 	= this.grid4_data ;
    }

    bos.trpo.grid4_reload		= function(){
        w2ui[this.id + '_grid4'].reload() ;
    }

    bos.trpo.grid4_destroy 	= function(){
        if(w2ui[this.id + '_grid4'] !== undefined){
            w2ui[this.id + '_grid4'].destroy() ;
        }
    }

    bos.trpo.grid4_render 	= function(){
        this.obj.find("#grid4").w2render(this.id + '_grid4') ;
    }

    bos.trpo.grid4_reloaddata	= function(){
        this.grid4_loaddata() ;
        this.grid4_setdata() ;
        this.grid4_reload() ;
    }

    bos.trpo.cmdpilihpr 		= function(fktpr){
        w2ui['bos-form-trpo_grid2'].clear();
        bjs.ajax(this.url + '/pilihpr', 'fktpr=' + fktpr,"",function(v){
            v = JSON.parse(v);
            with(bos.trpo.obj){
               find("#fktpr").val(v.faktur) ;
               find("#gudang").sval([{id:v.gudang,text:v.ketgudang}]);
               bos.trpo.loadmodelpr("hide");
            }

            $.each(v.detil, function(i, d){
                d.act = '<button type="button" onClick=bos.trpo.grid2_deleterow("'+i+'") class="btn btn-danger btn-grid">Delete</button>' ;
                w2ui["bos-form-trpo_grid2"].add({recid:i,
                                            stock: d.stock,
                                            namastock: d.namastock,
                                            spesifikasi: "",
                                            harga: 0,
                                            qty: d.qty,
                                            satuan:d.satuan,
                                            jumlah:0,
                                            cmddelete:d.act});
            });

            bos.trpo.initdetail();
            bos.trpo.hitungsubtotal();
        });
    }

    bos.trpo.cmdpilih 		= function(kode){
        bjs.ajax(this.url + '/pilihstock', 'kode=' + kode);
    }

    bos.trpo.cmdedit 		= function(faktur){
        bos.trpo.act = 1;
        bjs.ajax(this.url + '/editing', 'faktur=' + faktur,"",function(v){
            v = JSON.parse(v);
            w2ui["bos-form-trpo_grid2"].clear();
            with(bos.trpo.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#faktur").val(v.faktur) ;
               find("#fktpr").val(v.fktpr) ;
               find("#tgl").val(v.tgl);
               find("#gudang").sval([{id:v.gudang,text:v.ketgudang}]);
               find("#supplier").sval([{id:v.supplier,text:v.namasupplier}]);
               find("#subtotal").val($.number(v.total,2)) ;
               find("#total").val($.number(v.total,2)) ;
            }

            // each()
            $.each(v.detil, function(i, d){
                d.act = '<button type="button" onClick=bos.trpo.grid2_deleterow("'+i+'") class="btn btn-danger btn-grid">Delete</button>' ;
                w2ui["bos-form-trpo_grid2"].add({recid:i,
                                            stock: d.stock,
                                            namastock: d.namastock,
                                            spesifikasi: d.spesifikasi,
                                            harga: d.harga,
                                            qty: d.qty,
                                            satuan:d.satuan,
                                            jumlah:d.jumlah,
                                            cmddelete:d.act});
            });

            bos.trpo.initdetail();
            bos.trpo.settab(1) ;
        });
    }

    bos.trpo.cmddelete 	= function(faktur){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'faktur=' + faktur,"",function(hasil){
                if(hasil == "ok"){
                    alert("Data berhasil dihapus!!");
                    bos.trpo.grid1_reloaddata() ;
                }
            });
        }
    }

    bos.trpo.vbarang = {};

    bos.trpo.init 			= function(){
        bos.trpo.act = 0;

        this.obj.find("#subtotal").val("") ;
        this.obj.find("#total").val("") ;
        this.obj.find("#gudang").sval("") ;
        this.obj.find("#supplier").sval("") ;
        this.obj.find("#fktpr").val("") ;

        bjs.ajax(this.url + '/init') ;
        bos.trpo.getfaktur();
        w2ui[this.id + '_grid2'].clear();

        bos.trpo.initdetail();
    }

    bos.trpo.settab 		= function(n){
        this.obj.find("#tpo button:eq("+n+")").tab("show") ;
    }

    bos.trpo.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trpo.grid1_render() ;
            bos.trpo.init() ;
        }else{
            bos.trpo.grid2_reload() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#faktur").focus() ;
        }
    }
    bos.trpo.initdetail 			= function(){
        var datagrid = w2ui[this.id + '_grid2'].records;

        this.obj.find("#stock").val("") ;
        this.obj.find("#namastock").val("") ;
        this.obj.find("#spesifikasi").val("") ;
        this.obj.find("#harga").val("") ;
        this.obj.find("#qty").val("1") ;
        this.obj.find("#satuan").val("") ;
        this.obj.find("#jumlah").val("") ;



    }

    bos.trpo.getfaktur = function(){
        if(bos.trpo.act == 0 ){
            bos.trpo.gudang = bos.trpo.obj.find("#gudang").val();
            bos.trpo.tgl = bos.trpo.obj.find("#tgl").val();
            bjs.ajax(this.url + '/getfaktur','gudang='+bos.trpo.gudang+"&tgl="+bos.trpo.tgl,'',function(hasil){
                bos.trpo.obj.find("#faktur").val(hasil);
            }) ;
        }        
    }

    bos.trpo.initcomp		= function(){

        bjs.initselect({
            class : "#" + this.id + " .scons"
        }) ;

        bjs.initselect({
            class : "#" + this.id + " .s2",
            multi : true
        }) ;

        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;
        this.grid4_load() ;

        bos.trpo.init();

    }

    bos.trpo.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trpo.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.trpo.grid1_destroy() ;
            bos.trpo.grid2_destroy() ;
            bos.trpo.grid4_destroy() ;
        }) ;
    }

    bos.trpo.loadmodelstock      = function(l){
        this.obj.find("#wrap-pencarianstock-d").modal(l) ;
    }

    bos.trpo.loadmodelpr      = function(l){
        this.obj.find("#wrap-pencarianpr-d").modal(l) ;
    }

    bos.trpo.hitungjumlah 			= function(){
        var qty = string_2n(this.obj.find("#qty").val());
        var harga = string_2n(this.obj.find("#harga").val());
        var jml = qty * harga;
        this.obj.find("#jumlah").val($.number(jml,2));
        this.obj.find("#qty").val($.number(qty,2));
        this.obj.find("#harga").val($.number(harga,2));
    }

    bos.trpo.hitungsubtotal 			= function(){

        var nRows = w2ui[this.id + '_grid2'].records.length;
        var subtotal = 0 ;
        for(i=0;i< nRows;i++){
            var jumlah = w2ui[this.id + '_grid2'].getCellValue(i,6);
            subtotal += Number(jumlah);
        }
        this.obj.find("#subtotal").val($.number(subtotal,2));
        bos.trpo.hitungtotal();
    }

    bos.trpo.hitungtotal 			= function(){
        var total = string_2n(this.obj.find("#subtotal").val());
        this.obj.find("#total").val($.number(total,2));
    }

    bos.trpo.objs = bos.trpo.obj.find("#cmdsave") ;
    bos.trpo.initfunc	   = function(){
        this.obj.find("#cmdok").on("click", function(e){
            var stock       = bos.trpo.obj.find("#stock").val();
            var keterangan  = bos.trpo.obj.find("#namastock").val();
            var spesifikasi  = bos.trpo.obj.find("#spesifikasi").val();
            var harga       = string_2n(bos.trpo.obj.find("#harga").val());
            var qty         = string_2n(bos.trpo.obj.find("#qty").val());
            var satuan      = bos.trpo.obj.find("#satuan").val();
            var jumlah      = string_2n(bos.trpo.obj.find("#jumlah").val());
            bos.trpo.grid2_append(stock,keterangan,spesifikasi,harga,qty,satuan,jumlah);
        }) ;

        this.obj.find("#cmdstock").on("click", function(e){
            mdl_barang.open(function(r){
                r = JSON.parse(r);
                bos.trpo.obj.find("#stock").val(r.kode);
                bos.trpo.obj.find("#namastock").val(r.keterangan);
                bos.trpo.obj.find("#qty").val("1").focus();
                bos.trpo.obj.find("#satuan").val(r.satuan);
                bos.trpo.obj.find("#spesifikasi").focus() ;
            },{'tampil':['B','S'],'orderby':'tampil asc, keterangan asc'});
        }) ;

        this.obj.find("#cmdpr").on("click", function(e){
            bos.trpo.loadmodelpr("show");
            bos.trpo.grid4_reloaddata() ;
        }) ;

        this.obj.find("#stock").on("blur", function(e){
            var stock = bos.trpo.obj.find("#stock").val();
            var tampil = ['B','S'];
            bjs.ajax('admin/load/mdl_stock_seek', 'stock=' + stock + "&tampil="+ JSON.stringify(tampil),'',function(r){
                r = JSON.parse(r);
                // console.log(r.length);
                if(r.kode != undefined){
                    bos.trpo.obj.find("#stock").val(r.kode);
                    bos.trpo.obj.find("#namastock").val(r.keterangan);
                    bos.trpo.obj.find("#qty").focus();
                    bos.trpo.obj.find("#satuan").val(r.satuan);
                }else{
                    alert("data tidak ditemukan!!, Cek data barang pada menu stok / barang");
                }
                
            });
        });

        this.obj.find('form').on("submit", function(e){
            e.preventDefault() ;
            if( bjs.isvalidform(this) ){
                var datagrid2 =  w2ui['bos-form-trpo_grid2'].records;
                datagrid2 = JSON.stringify(datagrid2);
                bjs.ajax(bos.trpo.base_url + '/saving', bjs.getdataform(this)+"&grid2="+datagrid2 , bos.trpo.cmdsave,function(hasil){
                    if(hasil == "ok"){
                        alert("Data berhasil disimpan!!");
                        bos.trpo.settab(0) ;
                    }
                }) ;
            }
            //});
        }) ;

        this.obj.find("#harga").on("blur", function(e){
            bos.trpo.hitungjumlah();
        });
        this.obj.find("#qty").on("blur", function(e){
            bos.trpo.hitungjumlah();
        });

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trpo.grid1_reloaddata();
        });

        this.obj.find('#cmdgudang').on('click', function(){
            mdl_gudang.vargr.selection = bos.trpo.obj.find('#skd_gudang').val();
            mdl_gudang.open(function(r){
                r = JSON.parse(r);
                bos.trpo.gudang = [];
                $.each(r, function(i, v){
                    bos.trpo.gudang.push({'id':v.kode,'text':v.keterangan})                   
                });
                bos.trpo.obj.find('#skd_gudang').sval(bos.trpo.gudang);
            });
        
            
        });

        this.obj.find('#gudang, #tgl').on('change', function(){
            bos.trpo.getfaktur();
        });
    }

    $(function(){
        bos.trpo.initcomp() ;
        bos.trpo.initcallback() ;
        bos.trpo.initfunc() ;
        bos.trpo.initdetail();
    });
</script>