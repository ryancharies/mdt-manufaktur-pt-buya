<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Potongan Gaji Pinjaman Karyawan</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trpotgajipinjamankaryawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <table class="osxtable form">
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td width="100px"><label for="periode">Periode</label> </td>
                                        <td width="5px">:</td>
                                        <td width="400px">
                                            <select name="periode" id="periode" class="form-control select" style="width:100%" data-placeholder="Periode"></select>
                                        </td>
                                        <td width="85px">
                                            <input readonly style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="5px">sd</td>
                                        <td width="85px">
                                            <input readonly style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td width="100px"><label for="tgl">Tgl</label> </td>
                                        <td width="5px">:</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" value=<?=date("d-m-Y")?> <?=date_set()?><?=$mintgl?>>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "550px" >
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
            </table>               
        </div>
        <div class="footer fix hidden" style="height:32px">
            *) Data akan langsung tersimpan ketika sudah diedit
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    bos.trpotgajipinjamankaryawan.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'nopinjaman', caption: 'NoPinjaman', size: '120px', sortable: false ,style:'text-align:center',frozen:true},
                { field: 'nip', caption: 'NIP', size: '100px', sortable: false,style:'text-align:center',frozen:true},
                { field: 'nama', caption: 'Nama', size: '220px', sortable: false},
                { field: 'bagian', caption: 'Bagian', size: '120px', sortable: false},
                { field: 'jabatan', caption: 'Jabatan', size: '120px', sortable: false},
                { field: 'tglrealisasi', caption: 'Tgl Realisasi', size: '100px', sortable: false,style:'text-align:center'},
                { field: 'tglawalags', caption: 'Tgl Awal Ags', size: '100px', sortable: false,style:'text-align:center'},
                { field: 'plafond', caption: 'Plafond', size: '100px', sortable: false,render:'float:2'},
                { field: 'lama', caption: 'Lama (Bln)', size: '100px', sortable: false,render:'float:2'},
                { field: 'tunggakan', caption: 'Tunggakan', size: '100px', sortable: false,render:'float:2'},
                { field: 'bakidebet', caption: 'Bakidebet Awal', size: '100px', sortable: false,render:'float:2'},
                { field: 'angsuran', caption: 'Angsuran', size: '100px', sortable: false,render:'float:2', editable: { type: 'text' }},
                { field: 'bakidebetakhir', caption: 'Bakidebet Akhir', size: '100px', sortable: false,render:'float:2'}
            ],
            onChange: function(event){
                if(confirm("Apakah perubahan disimpan???")){
                    var nopinjaman = this.getCellValue(event.index,0);  
                    bos.trpotgajipinjamankaryawan.savingangsuran(nopinjaman,event.value_new);    
                }else{
                    this.set(event.recid, { namefield : event.value_previous });
                    //bos.trpotgajipinjamankaryawan.grid1_loaddata();
                }
                
                event.onComplete = function () {
                        w2ui[event.target].save();
                }
            }
        });


    }

    bos.trpotgajipinjamankaryawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trpotgajipinjamankaryawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.trpotgajipinjamankaryawan.grid1_loaddata		= function(){
        var tgl = this.obj.find("#tgl").val();
        var periode = this.obj.find("#periode").val();
        w2ui['bos-form-trpotgajipinjamankaryawan_grid1'].clear();
        bjs.ajax( bos.trpotgajipinjamankaryawan.base_url + '/loadgrid1', "tgl="+tgl+"&periode="+periode) ;
    }


    

    bos.trpotgajipinjamankaryawan.savingangsuran		= function(nopinjaman,nominal){
        var tgl = this.obj.find("#tgl").val();
        var periode = this.obj.find("#periode").val();
        bjs.ajax(this.url + '/saving', 'periode=' + periode + '&nominal=' + nominal + "&tgl="+tgl + "&nopinjaman="+nopinjaman);
    }

   
    bos.trpotgajipinjamankaryawan.init				= function(){
        bjs.ajax(this.url + "/init") ;
    }
    
    bos.trpotgajipinjamankaryawan.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    

    bos.trpotgajipinjamankaryawan.initcomp	= function(){
        // bjs.initselect({
		// 	class : "#" + this.id + " .select"
		// }) ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.trpotgajipinjamankaryawan.initcallback	= function(){


        this.obj.on("remove",function(){
            bos.trpotgajipinjamankaryawan.grid1_destroy() ;
        }) ;
    }

    bos.trpotgajipinjamankaryawan.objs = bos.trpotgajipinjamankaryawan.obj.find("#cmdsave") ;
    bos.trpotgajipinjamankaryawan.initfunc 		= function(){
        this.init() ;
        this.grid1_load() ;
        //this.grid1_loaddata();
       

        this.obj.find("#cmdrefresh").on("click",function(){
            bos.trpotgajipinjamankaryawan.grid1_loaddata();
        });

        this.obj.find("#periode").on("change",function(){
            var periode = bos.trpotgajipinjamankaryawan.obj.find("#periode").val();
            bjs.ajax(bos.trpotgajipinjamankaryawan.base_url + '/loadperiode', 'periode=' + periode);
            bos.trpotgajipinjamankaryawan.grid1_loaddata();
        });
    }
    
    $('#periode').select2({
        ajax: {
            url: bos.trpotgajipinjamankaryawan.base_url + '/seekperiode',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    $(function(){
        bos.trpotgajipinjamankaryawan.initcomp() ;
        bos.trpotgajipinjamankaryawan.initcallback() ;
        bos.trpotgajipinjamankaryawan.initfunc() ;
    }) ;
</script>
