<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpel">
                        <button class="btn btn-tab tpel active" href="#tpel_1" data-toggle="tab" >Daftar Retur Pembelian</button>
                        <button class="btn btn-tab tpel" href="#tpel_2" data-toggle="tab">Retur Pembelian</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trreturpembelian.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpel_1" style="padding-top:5px;">
                <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Antara Tanggal</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                    </div>
                                    <div class="col-md-6">
                                        <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label>Gudang</label>
                                <div class="input-group">
                                    <select name="skd_gudang" id="skd_gudang" class="form-control s2" style="width:100%"
                                                            data-placeholder="Pilih Gudang" data-sf="load_gudang"></select>
                                    <span class="input-group-addon">
                                        <span id="cmdgudang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                                    </span>
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group full-width">
                                <label>&nbsp;</label>
                                <div class="input-group full-width">
                                        <button type="button" class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="grid1" class="full-height" style="height: calc(100% - 50px);"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpel_2">
                    <table width="100%">
                        <tr>
                            <td width="100%" class="osxtable form">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="faktur">Faktur</label> </td>
                                        <td width="1%">:</td>
                                        <td width="35%">
                                            <input type="text" id="faktur" name="faktur" class="form-control" placeholder="Faktur" readonly required>
                                        </td>
                                        <td width="14%"><label for="gudang">Gudang</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                             <select name="gudang" id="gudang" class="form-control scons" style="width:100%"
                                                data-placeholder="Gudang" data-sf="load_gudang" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="tgl">Tanggal</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                        </td>
                                        <td width="14%"><label for="supplier">Supplier</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <select name="supplier" id="supplier" class="form-control scons" style="width:100%"
                                                data-placeholder="Supplier" data-sf="load_supplier" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="fktpb">Faktur PB</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <div width = '100%' class="input-group">
                                                <input type="text" id="fktpb" name="fktpb" class="form-control" placeholder="Faktur PB" readonly>
                                                <span class="input-group-btn">
                                                    <button class="form-control btn btn-info" type="button" id="cmdpb"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td width="14%"><label for="persppn">PPn (%)</label></td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input maxlength="6" width="7" type="text" name="persppn" id="persppn" class="form-control number" value="0">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <!-- <tr>
                            <td height = "20px" class="osxtable form">
                                <table>
                                    <tr>
                                        <td><label for="stock">Bahan Baku</label> </td>
                                        <td><label>Nama BB</label> </td>
                                        <td><label for="harga">Harga</label> </td>
                                        <td><label for="qty">Qty</label> </td>
                                        <td><label>Satuan</label> </td>
                                        <td><label>Jumlah</label></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" id="stock" name="stock" class="form-control" placeholder="Stock">
                                                <span class="input-group-btn">
                                                    <button class="form-control btn btn-info" type="button" id="cmdstock"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td><input type="text" id="namastock" readonly name="namastock" class="form-control" placeholder="Nama Stock"></td>
                                        <td><input maxlength="20" type="text" name="harga" id="harga" class="form-control number" value="0"></td>
                                        <td><input maxlength="10" type="text" name="qty" id="qty" class="form-control number" value="0"></td>
                                        <td><input type="text" id="satuan" readonly name="satuan" class="form-control" placeholder="Satuan"></td>
                                        <td><input maxlength="20" readonly type="text" name="jumlah" id="jumlah" class="form-control number" value="0"></td>
                                        <td><button type="button" class="btn btn-primary pull-right" id="cmdok">OK</button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr> -->
                        <tr>
                            <td height = "300px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                        <tr>
                            <td height = "20px" class="osxtable form">
                                <table>
                                    <tr>
                                        <td><label>Subtotal</label> </td>
                                        <td><label for="diskontotal">Diskon</label> </td>
                                        <td><label for="pembulatantotal">Pembulatan</label> </td>
                                        <td><label for="ppntotal">Ppn</label> </td>
                                        <td><label>Total</label> </td>
                                    </tr>
                                    <tr>
                                        <td><input maxlength="20" readonly type="text" name="subtotal" id="subtotal" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" type="text" name="diskontotal" id="diskontotal" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" type="text" name="pembulatantotal" id="pembulatantotal" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" readonly type="text" name="ppntotal" id="ppntotal" class="form-control number" value="0"></td>
                                        <td><input maxlength="20" readonly type="text" name="total" id="total" class="form-control number" value="0"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>

<div class="modal fade" id="wrap-pencarianpb-d" role="dialog" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="wm-title">Daftar Faktur PB</h4>
            </div>
            <div class="modal-body">
                <div id="grid4" style="height:250px"></div>
            </div>
            <div class="modal-footer">
                *Pilih Faktur PB
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    <?=cekbosjs();?>
    //grid daftar pembelian
    bos.trreturpembelian.grid1_data    = null ;
    bos.trreturpembelian.grid1_loaddata= function(){
        mdl_gudang.vargr.selection = bos.trreturpembelian.obj.find('#skd_gudang').val();        
        this.grid1_data 		= {'tglawal':bos.trreturpembelian.obj.find("#tglawal").val(),
                                    'tglakhir':bos.trreturpembelian.obj.find("#tglakhir").val(),
                                    'skd_gudang':JSON.stringify(mdl_gudang.vargr.selection)} ;
    }

    bos.trreturpembelian.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trreturpembelian.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers : true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'supplier', caption: 'Supplier', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'subtotal', caption: 'Subtotal', size: '100px',render:'int', sortable: false, style:"text-align:right" },
                { field: 'diskon', caption: 'Diskon', size: '100px', sortable: false,render:'int', style:"text-align:right"},
                { field: 'pembulatan', caption: 'Pembulatan', size: '100px', sortable: false,render:'int', style:"text-align:right"},
                { field: 'ppn', caption: 'PPn', size: '100px', sortable: false, render:'int',style:"text-align:right"},
                { field: 'total', caption: 'Total', size: '100px', sortable: false, render:'int',style:"text-align:right"},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trreturpembelian.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trreturpembelian.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trreturpembelian.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trreturpembelian.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trreturpembelian.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail pembelian
    bos.trreturpembelian.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
            multiSelect : false,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers:true
            },
            columns: [
                { field: 'stock', caption: 'Kode / Barcode', size: '120px', sortable: false },
                { field: 'namastock', caption: 'Nama Stock', size: '250px', sortable: false },
                { field: 'harga', render:'float:2',caption: 'Harga', size: '100px', sortable: false,style:'text-align:right'},
                { field: 'qtybeli', render:'float:2',caption: 'Qty Beli', size: '70px', sortable: false, style:'text-align:right'},
                { field: 'qty', render:'float:2',caption: 'Qty Retur', size: '70px', sortable: false,editable:{type:'float'},  style:'text-align:right'},
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false},
                { field: 'jumlah', render:'float:2',caption: 'Jumlah', size: '100px', sortable: false, style:'text-align:right'}
            ],
            onChange: function(event){
                bos.trreturpembelian.vbarang.kode = this.getCellValue(event.index,0);  

                bos.trreturpembelian.vbarang._harga = this.getCellValue(event.index,2);
                bos.trreturpembelian.vbarang._qtybeli = this.getCellValue(event.index,3);
                bos.trreturpembelian.vbarang._qty = this.getCellValue(event.index,4);
                

                if(event.column == 4){
                    if(bos.trreturpembelian.vbarang._qtybeli >= event.value_new){
                        bos.trreturpembelian.vbarang._qty = event.value_new;
                    }else{
                        bos.trreturpembelian.vbarang._qty = event.value_previous;
                    }
                }

                bos.trreturpembelian.vbarang._jml = bos.trreturpembelian.vbarang._harga * bos.trreturpembelian.vbarang._qty;
                
                event.onComplete = function () {
                    w2ui[event.target].save();
                    w2ui[event.target].set(event.recid, { jumlah :  bos.trreturpembelian.vbarang._jml, qty :  bos.trreturpembelian.vbarang._qty  });
                    w2ui[event.target].refresh();
                    bos.trreturpembelian.hitungsubtotal();
                }
            }
        });


    }

    bos.trreturpembelian.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trreturpembelian.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    // bos.trreturpembelian.grid2_append    = function(kode,keterangan,harga,qty,satuan,jumlah){
    //     var datagrid  = w2ui[this.id + '_grid2'].records;
    //     var lnew      = true;
    //     var nQty      = 1;
    //     var nJumlah   = nQty * harga;
    //     var recid     = kode;
    //     if( w2ui[this.id + '_grid2'].get(recid) !== null ){
    //         w2ui[this.id + '_grid2'].set(recid,{stock: kode, namastock: keterangan, harga: harga, qty: qty, satuan:satuan,jumlah:jumlah});
    //     }else{
    //         var Hapus = "<button type='button' onclick = 'bos.trreturpembelian.grid2_deleterow("+recid+")' class='btn btn-danger btn-grid'>Delete</button>";
    //         w2ui[this.id + '_grid2'].add([
    //             { recid:recid,
    //              stock: kode,
    //              namastock: keterangan,
    //              harga: harga,
    //              qty: qty,
    //              satuan:satuan,
    //              jumlah:jumlah,
    //              cmddelete:Hapus}
    //         ]) ;
    //     }
    //     bos.trreturpembelian.initdetail();
    //     bos.trreturpembelian.obj.find("#cmdstock").focus() ;
    //     bos.trreturpembelian.hitungsubtotal();
    // }

    // bos.trreturpembelian.grid2_deleterow = function(recid){
    //     if(confirm("Item di hapus dari detail pembelian???"+recid)){
    //         w2ui[this.id + '_grid2'].select(recid);
    //         w2ui[this.id + '_grid2'].delete(true);
    //         bos.trreturpembelian.hitungsubtotal();
    //     }
    // }

    //grid4 daftarpb
    bos.trreturpembelian.grid4_data    = null ;
    bos.trreturpembelian.grid4_loaddata= function(){
        this.grid4_data 		= {} ;
    }

    bos.trreturpembelian.grid4_load    = function(){
        this.obj.find("#grid4").w2grid({
            name	: this.id + '_grid4',
            limit 	: 100 ,
            url 	: bos.trreturpembelian.base_url + "/loadgrid4",
            pbstData: this.grid4_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false},
                { field: 'tgl', caption: 'Tanggal', size: '100px', sortable: false},
                { field: 'supplier', caption: 'Supplier', size: '200px', sortable: false },
                { field: 'total', render:'float:2', caption: 'Total', size: '100px', sortable: false},
                { field: 'cmdpilih', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trreturpembelian.grid4_setdata	= function(){
        w2ui[this.id + '_grid4'].pbstData 	= this.grid4_data ;
    }
    bos.trreturpembelian.grid4_reload		= function(){
        w2ui[this.id + '_grid4'].reload() ;
    }
    bos.trreturpembelian.grid4_destroy 	= function(){
        if(w2ui[this.id + '_grid4'] !== undefined){
            w2ui[this.id + '_grid4'].destroy() ;
        }
    }

    bos.trreturpembelian.grid4_render 	= function(){
        this.obj.find("#grid4").w2render(this.id + '_grid4') ;
    }

    bos.trreturpembelian.grid4_reloaddata	= function(){
        this.grid4_loaddata() ;
        this.grid4_setdata() ;
        this.grid4_reload() ;
    }

    bos.trreturpembelian.cmdpilihpb 		= function(fktpb){
        w2ui['bos-form-trreturpembelian_grid2'].clear();
        bjs.ajax(this.url + '/pilihpb', 'fktpb=' + fktpb,'',function(v){
            v = JSON.parse(v);

            w2ui["bos-form-trreturpembelian_grid2"].clear();
            with(bos.trreturpembelian.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#faktur").val(v.faktur) ;
               find("#fktpb").val(fktpb) ;
               find("#tgl").val(v.tgl);
               find("#jthtmp").val(v.jthtmp);
               find("#persppn").val(v.persppn);
               find("#gudang").sval([{id:v.gudang,text:v.ketgudang}]);
               find("#supplier").sval([{id:v.supplier,text:v.namasupplier}]);

               find("#subtotal").val($.number(v.subtotal,2)) ;
               find("#diskontotal").val($.number(v.diskon,2));
               find("#pembulatantotal").val($.number(v.pembulatan,2));
               find("#ppntotal").val($.number(v.ppn,2));
               find("#total").val($.number(v.total,2)) ;
            }

            // each()
            $.each(v.detil, function(i, d){
                // d.act = '<button type="button" onClick=bos.trreturpembelian.grid2_deleterow("'+i+'") class="btn btn-danger btn-grid">Delete</button>' ;
                w2ui["bos-form-trreturpembelian_grid2"].add({recid:i,
                    stock: d.stock,
                    namastock: d.namastock,
                    harga: d.harga,
                    qtybeli: d.qty,
                    qty: d.qty,
                    satuan:d.satuan,
                    jumlah:d.jumlah});
            });

            bos.trreturpembelian.loadmodelpb("hide");
            bos.trreturpembelian.hitungsubtotal();

        });
    }

    bos.trreturpembelian.cmdedit 		= function(faktur){
        bjs.ajax(this.url + '/editing', 'faktur=' + faktur,'',function(v){
            v = JSON.parse(v);

            w2ui["bos-form-trreturpembelian_grid2"].clear();
            with(bos.trreturpembelian.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#faktur").val(v.faktur) ;
               find("#fktpo").val(v.fktpo) ;
               find("#tgl").val(v.tgl);
               find("#jthtmp").val(v.jthtmp);
               find("#gudang").sval([{id:v.gudang,text:v.ketgudang}]);
               find("#supplier").sval([{id:v.supplier,text:v.namasupplier}]);
               find("#subtotal").val($.number(v.subtotal,2)) ;
               find("#total").val($.number(v.total,2)) ;
            }

            // each()
            $.each(v.detil, function(i, d){
                d.act = '<button type="button" onClick=bos.trreturpembelian.grid2_deleterow("'+i+'") class="btn btn-danger btn-grid">Delete</button>' ;
                w2ui["bos-form-trreturpembelian_grid2"].add({recid:i,
                    stock: d.stock,
                    namastock: d.namastock,
                    harga: d.harga,
                    qtybeli: d.qtybeli,
                    qty: d.qty,
                    satuan:d.satuan,
                    jumlah:d.jumlah});
            });

            bos.trreturpembelian.initdetail();
            bos.trreturpembelian.settab(1) ;

        });
    }

    bos.trreturpembelian.cmddelete 	= function(faktur){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'faktur=' + faktur);
        }
    }

    bos.trreturpembelian.vbarang = {};

    bos.trreturpembelian.init 			= function(){
        bos.trreturpembelian.act = 0;

        this.obj.find("#subtotal").val("0") ;
        this.obj.find("#diskontotal").val("0") ;
        this.obj.find("#pembulatantotal").val("0") ;
        this.obj.find("#ppntotal").val("0") ;
        this.obj.find("#persppn").val("0") ;
        this.obj.find("#fktpo").val("") ;
        this.obj.find("#total").val("0") ;
        this.obj.find("#gudang").sval("") ;
        this.obj.find("#supplier").sval("") ;

        bjs.ajax(this.url + '/init') ;
        bos.trreturpembelian.getfaktur();

        w2ui[this.id + '_grid2'].clear();

        bos.trreturpembelian.initdetail();
    }

    bos.trreturpembelian.settab 		= function(n){
        this.obj.find("#tpel button:eq("+n+")").tab("show") ;
    }

    bos.trreturpembelian.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trreturpembelian.grid1_render() ;
            bos.trreturpembelian.init() ;
        }else{
            bos.trreturpembelian.grid2_reload() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#faktur").focus() ;
        }
    }
    bos.trreturpembelian.initdetail 			= function(){
        var datagrid = w2ui[this.id + '_grid2'].records;

        this.obj.find("#nomor").val(datagrid.length+1) ;
        this.obj.find("#stock").val("") ;
        this.obj.find("#namastock").val("") ;
        this.obj.find("#harga").val("") ;
        this.obj.find("#qty").val("1") ;
        this.obj.find("#satuan").val("") ;
        this.obj.find("#jumlah").val("") ;

    }

    bos.trreturpembelian.getfaktur = function(){
        if(bos.trreturpembelian.act == 0 ){
            bos.trreturpembelian.gudang = bos.trreturpembelian.obj.find("#gudang").val();
            bos.trreturpembelian.tgl = bos.trreturpembelian.obj.find("#tgl").val();
            bjs.ajax(this.url + '/getfaktur','gudang='+bos.trreturpembelian.gudang+"&tgl="+bos.trreturpembelian.tgl,'',function(hasil){
                bos.trreturpembelian.obj.find("#faktur").val(hasil);
            }) ;
        }        
    }

    bos.trreturpembelian.initcomp		= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons"
        }) ;

        bjs.initselect({
            class : "#" + this.id + " .s2",
            multi : true
        }) ;


        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;

        bos.trreturpembelian.init();

    }

    bos.trreturpembelian.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trreturpembelian.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.trreturpembelian.grid1_destroy() ;
            bos.trreturpembelian.grid2_destroy() ;
        }) ;
    }

    bos.trreturpembelian.loadmodelpb      = function(l){
        this.obj.find("#wrap-pencarianpb-d").modal(l) ;
    }

    // bos.trreturpembelian.hitungjumlah 			= function(){
    //     var qty = string_2n(this.obj.find("#qty").val());
    //     var harga = string_2n(this.obj.find("#harga").val());
    //     var jml = qty * harga;
    //     this.obj.find("#jumlah").val($.number(jml,2));
    //     this.obj.find("#qty").val($.number(qty,2));
    //     this.obj.find("#harga").val($.number(harga,2));
    // }

    bos.trreturpembelian.hitungsubtotal 			= function(){

        var nRows = w2ui[this.id + '_grid2'].records.length;
        var subtotal = 0 ;
        for(i=0;i< nRows;i++){
            var jumlah = w2ui[this.id + '_grid2'].getCellValue(i,6);
            subtotal += Number(jumlah);
        }
        this.obj.find("#subtotal").val($.number(subtotal,2));
        bos.trreturpembelian.hitungtotal();
    }

    bos.trreturpembelian.hitungtotal 			= function(){
        var persppn = string_2n(this.obj.find("#persppn").val());
        var subtotal = string_2n(this.obj.find("#subtotal").val());
        var diskontotal = string_2n(this.obj.find("#diskontotal").val());
        var pembulatantotal = string_2n(this.obj.find("#pembulatantotal").val());
        var ppntotal = persppn * subtotal / 100;
        var total = subtotal - diskontotal + pembulatantotal + ppntotal;
        this.obj.find("#ppntotal").val($.number(ppntotal,2));
        this.obj.find("#total").val($.number(total,2));
        this.obj.find("#diskontotal").val($.number(diskontotal,2));
        this.obj.find("#pembulatantotal").val($.number(pembulatantotal,2));
    }

    bos.trreturpembelian.objs = bos.trreturpembelian.obj.find("#cmdsave") ;
    bos.trreturpembelian.initfunc	   = function(){
        // this.obj.find("#cmdok").on("click", function(e){
        //     var stock       = bos.trreturpembelian.obj.find("#stock").val();
        //     var keterangan  = bos.trreturpembelian.obj.find("#namastock").val();
        //     var harga       = string_2n(bos.trreturpembelian.obj.find("#harga").val());
        //     var qty         = string_2n(bos.trreturpembelian.obj.find("#qty").val());
        //     var satuan      = bos.trreturpembelian.obj.find("#satuan").val();
        //     var jumlah      = string_2n(bos.trreturpembelian.obj.find("#jumlah").val());
        //     bos.trreturpembelian.grid2_append(stock,keterangan,harga,qty,satuan,jumlah);
        // }) ;

        // this.obj.find("#cmdstock").on("click", function(e){
        //     mdl_barang.open(function(r){
        //         r = JSON.parse(r);
        //         bos.trreturpembelian.obj.find("#stock").val(r.kode);
        //         bos.trreturpembelian.obj.find("#namastock").val(r.keterangan);
        //         bos.trreturpembelian.obj.find("#qty").val("1");
        //         bos.trreturpembelian.obj.find("#satuan").val(r.satuan);
        //         bos.trreturpembelian.obj.find("#harga").val('0');
        //         bos.trreturpembelian.obj.find("#harga").focus();
        //     },{'tampil':['B','S'],'orderby':'tampil asc, keterangan asc'});
        // }) ;

        // this.obj.find("#stock").on("blur", function(e){
        //     var stock = bos.trreturpembelian.obj.find("#stock").val();
        //     var tampil = ['B','S'];
        //     bjs.ajax('admin/load/mdl_stock_seek', 'stock=' + stock + "&tampil="+ JSON.stringify(tampil),'',function(r){
        //         r = JSON.parse(r);
        //         // console.log(r.length);
        //         if(r.kode != undefined){
        //             bos.trreturpembelian.obj.find("#stock").val(r.kode);
        //             bos.trreturpembelian.obj.find("#namastock").val(r.keterangan);
        //             bos.trreturpembelian.obj.find("#qty").focus();
        //             bos.trreturpembelian.obj.find("#satuan").val(r.satuan);
        //         }else{
        //             alert("data tidak ditemukan!!, Cek data barang pada menu stok / barang");
        //         }
                
        //     });
        // });
        this.obj.find('form').on("submit", function(e){
            //this.obj.find("#cmdsave").on("click", function(e){
            e.preventDefault() ;
            if( bjs.isvalidform(this) ){
                var datagrid2 =  w2ui['bos-form-trreturpembelian_grid2'].records;
                datagrid2 = JSON.stringify(datagrid2);
                bjs.ajax( bos.trreturpembelian.base_url + '/saving', bjs.getdataform(this)+"&grid2="+datagrid2 , bos.trreturpembelian.cmdsave) ;
            }

        }) ;

        this.obj.find("#harga").on("blur", function(e){
            bos.trreturpembelian.hitungjumlah();
        });
        this.obj.find("#qty").on("blur", function(e){
            bos.trreturpembelian.hitungjumlah();
        });

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trreturpembelian.grid1_reloaddata();
        });

        this.obj.find("#diskontotal").on("blur", function(e){
            bos.trreturpembelian.hitungtotal();
        });
        
        this.obj.find("#pembulatantotal").on("blur", function(e){
            bos.trreturpembelian.hitungtotal();
        });

        this.obj.find("#persppn").on("blur", function(e){
            bos.trreturpembelian.hitungtotal();
        });

        this.obj.find('#gudang, #tgl').on('change', function(){
            bos.trreturpembelian.getfaktur();
        });

        this.obj.find('#cmdgudang').on('click', function(){
            mdl_gudang.vargr.selection = bos.trreturpembelian.obj.find('#skd_gudang').val();
            mdl_gudang.open(function(r){
                r = JSON.parse(r);
                bos.trreturpembelian.gudang = [];
                $.each(r, function(i, v){
                    bos.trreturpembelian.gudang.push({'id':v.kode,'text':v.keterangan})                   
                });
                bos.trreturpembelian.obj.find('#skd_gudang').sval(bos.trreturpembelian.gudang);
            });
        });

        this.obj.find("#cmdpb").on("click", function(e){
            bos.trreturpembelian.loadmodelpb("show");
            bos.trreturpembelian.grid4_destroy() ;
            bos.trreturpembelian.grid4_load() ;
        }) ;
    }

    $(function(){
        bos.trreturpembelian.initcomp() ;
        bos.trreturpembelian.initcallback() ;
        bos.trreturpembelian.initfunc() ;
        bos.trreturpembelian.initdetail();
    });
</script>