<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tabsensikaryawan">
                        <button class="btn btn-tab tpel active" href="#tabsensikaryawan_1" data-toggle="tab" >Daftar Karyawan</button>
                        <button class="btn btn-tab tpel" href="#tabsensikaryawan_2" data-toggle="tab">Absensi</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trabsensikaryawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tabsensikaryawan_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tabsensikaryawan_2">
                    <table width = "100%">
                        <tr>
                            <td width = "100%" valign ="top">
                                <table width = "100%">
                                    <tr>
                                        <td width = '100%' class="osxtable form">
                                            <table width = '100%'>
                                                <tr>
                                                    <td width="75px"><label for="kode">Kode</label> </td>
                                                    <td width="5px">:</td>
                                                    <td >
                                                        <input readonly type="text" id="kode" name="kode" class="form-control" placeholder="Kode" required>
                                                    </td>
                                                <!-- </tr>
                                                <tr> -->
                                                    <td width="75px"><label for="nama">Nama</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama" required>
                                                    </td>
                                                <!-- </tr>
                                                <tr> -->
                                                    <td width="75px"><label for="ktp">No KTP</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input type="text" id="ktp" name="ktp" class="form-control" placeholder="No KTP" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="75px"><label for="notelepon">NoTelp</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input type="text" id="notelepon" name="notelepon" class="form-control" placeholder="notelepon" required>
                                                    </td>
                                                <!-- </tr>
                                                <tr> -->
                                                    <td width="75px"><label for="alamat">Alamat</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input type="text" id="alamat" name="alamat" class="form-control" placeholder="alamat" required>
                                                    </td>
                                                <!-- </tr>
                                                <tr> -->
                                                    <td width="75px"><label for="tglmasuk">Tgl Masuk</label> </td>
                                                    <td width="5px">:</td>
                                                    <td >
                                                        <input style="width:80px" type="text" class="form-control date" id="tglmasuk" name="tglmasuk" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <hr/>
                                        </td>
                                    </tr>
                                <!-- </table>
                            </td>
                            <td valign ="top">
                                <table class="osxtable form"> -->
                                    <tr>
                                        <td class="osxtable form">
                                            <table>
                                                <tr>
                                                    <td width="100px"><label for="periode">Periode</label> </td>
                                                    <td width="5px">:</td>
                                                    <td width="400px">
                                                        <select name="periode" id="periode" class="form-control select" style="width:100%" data-placeholder="Periode"></select>
                                                    </td>
                                                    <td width="85px">
                                                        <input readonly style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                                    </td>
                                                    <td width="5px">sd</td>
                                                    <td width="85px">
                                                        <input readonly style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height = "450px">
                                            <div id="grid2" class="full-height"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
                </div>
                <div class="modal fade" style="position:absolute;padding-top:100px;" id="wrap-absensi-d" role="dialog" data-backdrop="false" data-keyboard="false">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="wm-title">Absensi Karyawan</h4>
                                </div>
                                <div class="modal-body">
                                    <table width = '100%' class="osxtable form">
                                        <tr>
                                            <td width="75px"><label for="tglabsensi">Tgl Absensi</label> </td>
                                            <td width="5px">:</td>
                                            <td width="100px">
                                                <input readonly type="text" id="tglabsensi" name="tglabsensi" class="form-control date" value=<?=date("d-m-Y")?> <?=date_set()?> required>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="75px"><label for="masuk">Masuk</label> </td>
                                            <td width="5px">:</td>
                                            <td width="100px">
                                                <input type="text" id="masuk" name="masuk" class="form-control" placeholder="JJ:MM:DD" value = "--:--:--" required>
                                            </td>
                                            <td>
                                                <select name="kodeabsensim" id="kodeabsensim" class="form-control select" style="width:100%" data-placeholder="Kode Absensi Masuk"></select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="75px"><label for="pulang">Pulang</label> </td>
                                            <td width="5px">:</td>
                                            <td width="100px">
                                                <input type="text" id="pulang" name="pulang" class="form-control" placeholder="JJ:MM:DD" value = "--:--:--" required>
                                            </td>
                                            <td>
                                                <select name="kodeabsensip" id="kodeabsensip" class="form-control select" style="width:100%" data-placeholder="Kode Absensi Pulang"></select>
                                            </td>
                                        </tr>
                                    <table>
                                </div>
                                <div class="modal-footer">
                                    <button type = "button" class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.trabsensikaryawan.grid1_data 	 = null ;
    bos.trabsensikaryawan.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.trabsensikaryawan.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.trabsensikaryawan.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,frozen:true,style:"text-align:center;"},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false,frozen:true},
                { field: 'ktp', caption: 'No KTP', size: '100px', sortable: false,frozen:true},
                { field: 'alamat', caption: 'Alamat', size: '200px', sortable: false},
                { field: 'telepon', caption: 'Telepon', size: '100px', sortable: false},
                { field: 'tgl', caption: 'Tgl Masuk', size: '100px', sortable: false},
                { field: 'golabsensi', caption: 'Gol. Absensi', size: '100px', sortable: false},
                { field: 'cmdedit', caption: ' ', size: '100px', sortable: false },
            ]
        });
    }

    bos.trabsensikaryawan.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trabsensikaryawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trabsensikaryawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trabsensikaryawan.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trabsensikaryawan.grid1_reloadData	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //detail mutasi golongan grid2 
    bos.trabsensikaryawan.grid2_data 	 = null ;

    bos.trabsensikaryawan.grid2_load    = function(){ 
        this.obj.find("#grid2").w2grid({
            name	 : this.id + '_grid2',
            show 		: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'no', caption: 'No', size: '40px', sortable: false,style:"text-align:right;"},
                { field: 'hari', caption: 'Hari', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'jadwalmasuk', caption: 'JW. Masuk', size: '80px', sortable: false},
                { field: 'jadwalpulang', caption: 'JW. Pulang', size: '80px', sortable: false},
                { field: 'toleransi', caption: 'Toleransi (Mnt)', size: '100px', sortable: false},
                { field: 'absmasuk', caption: 'Abs. Masuk', size: '80px', sortable: false},
                { field: 'kodeabsensim', caption: 'Kode Abs Masuk', size: '150px', sortable: false, style:"text-align:center"},
                { field: 'terlambat', caption: 'Terlambat', size: '80px', sortable: false},
                { field: 'abspulang', caption: 'Abs. Pulang', size: '80px', sortable: false},
                { field: 'kodeabsensip', caption: 'Kode Abs Pulang', size: '150px', sortable: false, style:"text-align:center"},
                { field: 'plgcepat', caption: 'Plg Cepat', size: '80px', sortable: false},
                { field: 'btn', caption: '', size: '40px', style:"text-align:center",sortable: false},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false},
                { field: 'golabsensi', caption: 'Gol. Absensi', size: '150px', sortable: false},
                { field: 'jadwal', caption: 'Jadwal', size: '150px', sortable: false}
            ]
        });
    }

    bos.trabsensikaryawan.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trabsensikaryawan.grid2_render 	= function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.trabsensikaryawan.grid2_reloaddata	= function(){
        // this.grid2_loaddata() ;
        // this.grid2_setdata() ;
        // this.grid2_reload() ;
        w2ui[this.id + '_grid2'].clear();
        var periode = this.obj.find("#periode").val();
        var nip = this.obj.find("#kode").val();
        bjs.ajax(this.url + '/loadabsensi', 'periode=' + periode + '&nip=' + nip);
    }

    bos.trabsensikaryawan.cmdedit		= function(id){
        bjs.ajax(this.url + '/editing', 'kode=' + id);
    }

    bos.trabsensikaryawan.cmdabsensi 		= function(nip,tgl){
        //bjs.getdataform(bos.tradjstock.obj.find("form"))+
        bjs.ajax(this.url + '/absensi','nip=' + nip + '&tgl=' + tgl);
    }

    bos.trabsensikaryawan.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#nama").val("") ;
        this.obj.find("#notelepon").val("") ;
        this.obj.find("#ktp").val("") ;
        this.obj.find("#alamat").val("") ;
        this.obj.find("#golongan").sval("") ;
        w2ui[this.id + '_grid2'].clear();
    }

    bos.trabsensikaryawan.settab 		= function(n){
        this.obj.find("#tabsensikaryawan button:eq("+n+")").tab("show") ;
    }

    bos.trabsensikaryawan.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trabsensikaryawan.grid1_render() ;
            bos.trabsensikaryawan.init() ;
        }else{
            bos.trabsensikaryawan.grid2_reloaddata() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#tgl").focus() ;
        }
    }

    bos.trabsensikaryawan.initcomp	= function(){
        /*bjs.initselect({
            class : "#" + this.id + " .select2",
            clear : true
        }) ;*/
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;      
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        // this.grid2_loaddata() ;
        this.grid2_load() ;

        this.obj.find('#kodeabsensim').select2({
            ajax: {
                url: bos.trabsensikaryawan.base_url + '/seekkodeabsensim',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
        this.obj.find('#kodeabsensip').select2({
            ajax: {
                url: bos.trabsensikaryawan.base_url + '/seekkodeabsensip',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.obj.find('#periode').select2({
            ajax: {
                url: bos.trabsensikaryawan.base_url + '/seekperiode',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });


    }

    bos.trabsensikaryawan.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trabsensikaryawan.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.trabsensikaryawan.grid1_destroy() ;
            bos.trabsensikaryawan.grid2_destroy() ;
        })
         ;
    }

    bos.trabsensikaryawan.loadmodelabsensi      = function(l){
        this.obj.find("#wrap-absensi-d").modal(l) ;
    }

    bos.trabsensikaryawan.objs = bos.trabsensikaryawan.obj.find("#cmdsave") ;
    bos.trabsensikaryawan.initfunc 		= function(){
        // this.obj.find("form").on("submit", function(e){
        //     e.preventDefault() ;
        //     if(bjs.isvalidform(this)){
        //         bjs.ajax( bos.trabsensikaryawan.url + '/saving', bjs.getdataform(this), bos.trabsensikaryawan.objs) ;
        //     }
        // });
        this.obj.find("#periode").on("change",function(){
            
            // w2ui['bos-form-trabsensikaryawan_grid2'].clear();
            // var periode = bos.trabsensikaryawan.obj.find("#periode").val();
            // var nip = bos.trabsensikaryawan.obj.find("#kode").val();
            // bjs.ajax(bos.trabsensikaryawan.base_url + '/loadabsensi', 'periode=' + periode + '&nip=' + nip);
            bos.trabsensikaryawan.grid2_reloaddata();
        });

        this.obj.find("#cmdsave").on("click", function(e){
            var confrm = confirm("Data akan disimpan??");
            if(confrm){
                bjs.ajax( bos.trabsensikaryawan.base_url + '/saving', bjs.getdataform(bos.trabsensikaryawan.obj.find("form"))) ; 
            }
        }) ;
        
    }
    

    $(function(){
        bos.trabsensikaryawan.initcomp() ;
        bos.trabsensikaryawan.initcallback() ;
        bos.trabsensikaryawan.initfunc() ;
    }) ;
</script>
