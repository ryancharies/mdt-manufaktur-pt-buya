<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpengdana">
                        <button class="btn btn-tab tpel active" href="#tpengdana_1" data-toggle="tab" >Daftar Pengajuan Dana</button>
                        <button class="btn btn-tab tpel" href="#tpengdana_2" data-toggle="tab">Pengajuan Dana</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trpengajuandana.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpengdana_1" style="padding-top:5px;">
                    <table width="100%">
                        <tr>
                            <td height="25px" width="100%">
                                <table class="osxtable form">
                                    <tr>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="5px">sd</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="400px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpengdana_2">
                    <table width="100%">
                        <tr>
                            <td width="100%" class="osxtable form">
                                <table>
                                    <tr>
                                        <td width="14%"><label for="faktur">Faktur</label> </td>
                                        <td width="1%">:</td>
                                        <td width="35%">
                                            <input type="text" id="faktur" name="faktur" class="form-control" placeholder="Faktur" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="tgl">Tanggal</label> </td>
                                        <td width="1%">:</td>
                                        <td >
                                            <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                        </td>
                                        <td width="14%"><label for=""></label> </td>
                                        <td width="1%"></td>
                                        <td >

                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="jumlah">Jumlah</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" name="jumlah" id="jumlah" 
                                                   class="form-control number" value="0" required> 
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="keterangan">Keterangan</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="keterangan" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="jenis">Jenis Pengajuan</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" class="jenis" value="0" checked>
                                                    Rutin
                                                </label>
                                                &nbsp;&nbsp;
                                            </div>
                                            <div class="radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" class="jenis" value="1">
                                                    Insidentil
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="14%"><label for="yangmengajukan">Yang Mengajukan</label> </td>
                                        <td width="1%">:</td>
                                        <td>
                                            <input type="text" id="yangmengajukan" name="yangmengajukan" class="form-control" placeholder="Yang Mengajukan" required>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "20px" class="osxtable form">
                                *) List Otorisasi
                                <table>
                                    <tr>
                                        <td width='50px'><label for="nomor">No</label> </td>
                                        <td><label for="otorisasi">Otorisasi</label> </td>
                                        <td><label for="jabatan">Jabatan</label> </td>
                                        <td><label for="peran">Tahap</label> </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input maxlength="5" type="text" name="nomor" id="nomor" class="form-control number" value="0"></td>
                                        <td>
                                            <select name="otorisasi" id="otorisasi" class="form-control otorisasiselect" style="width:300px"
                                                    data-placeholder="Karyawan"></select>  
                                        </td>
                                        <td><input type="text" id="jabatan" name="jabatan" class="form-control" placeholder="Jabatan"></td>
                                        <td><select name="tahap" id="tahap" class="form-control select"
                                                data-sf="load_tahap" data-placeholder="Tahap"></select> </td>
                                        <td><button type="button" class="btn btn-primary pull-right" id="cmdok">OK</button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "230px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>                                           
</div>

<script type="text/javascript">
    <?=cekbosjs();?>
    //grid daftar po
    bos.trpengajuandana.grid1_data    = null ;
    bos.trpengajuandana.grid1_loaddata= function(){

        var tglawal = bos.trpengajuandana.obj.find("#tglawal").val();
        var tglakhir = bos.trpengajuandana.obj.find("#tglakhir").val();
        this.grid1_data 		= {'tglawal':tglawal,'tglakhir':tglakhir} ;
    }

    bos.trpengajuandana.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trpengajuandana.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center",frozen:true},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'jumlah', caption: 'Jumlah', size: '100px', sortable: false , render:'float:2'},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'jenis', caption: 'Jenis Pengajuan', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'cmdcetak', caption: ' ', size: '80px', sortable: false },
				{ field: 'cmdcetakdm', caption: ' ', size: '80px', sortable: false },
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trpengajuandana.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trpengajuandana.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trpengajuandana.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trpengajuandana.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trpengajuandana.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail
    bos.trpengajuandana.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'no', caption: 'No', size: '50px', sortable: false},
                { field: 'username', caption: 'Username', size: '120px', sortable: false },
                { field: 'namakaryawan', caption: 'Nama', size: '120px', sortable: false },
                { field: 'jabatan', caption: 'Jabatan', size: '150px', sortable: false },
                { field: 'tahap', caption: 'Tahap', size: '100px', sortable: false},
                { field: 'kettahap', caption: 'Ket Tahap', size: '150px', sortable: false},
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });


    }

    bos.trpengajuandana.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trpengajuandana.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

    bos.trpengajuandana.grid2_append    = function(no,username,namakaryawan,jabatan,tahap,kettahap){
        var datagrid  = w2ui[this.id + '_grid2'].records;
        var lnew      = true;
        var nQty      = 1;
        var recid     = "";
        if(no <= datagrid.length){
            recid = no;
            w2ui[this.id + '_grid2'].set(recid,{username: username,namakaryawan:namakaryawan,jabatan:jabatan,tahap:tahap,kettahap:kettahap});
        }else{
            recid = no;
            var Hapus = "<button type='button' onclick = 'bos.trpengajuandana.grid2_deleterow("+recid+")' class='btn btn-danger btn-grid'>Delete</button>";
            w2ui[this.id + '_grid2'].add([
                { recid:recid,
                 no: no,
                 username: username,
                 namakaryawan:namakaryawan,
                 jabatan:jabatan,
                 tahap:tahap,
                 kettahap:kettahap,
                 cmddelete:Hapus}
            ]) ;
        }
        bos.trpengajuandana.initdetail();
    }

    bos.trpengajuandana.grid2_deleterow = function(recid){
        if(confirm("Otorisasi di hapus dari detail???"+recid)){
            w2ui[this.id + '_grid2'].select(recid);
            w2ui[this.id + '_grid2'].delete(true);
            bos.trpengajuandana.grid2_urutkan();
        }
    }

    bos.trpengajuandana.grid2_urutkan = function(){
        var datagrid = w2ui[this.id + '_grid2'].records;
        w2ui[this.id + '_grid2'].clear();
        for(i=0;i<datagrid.length;i++){
            var no = i+1;
            datagrid[i]["recid"] = no;
            var recid = no;
            var Hapus = "<button type='button' onclick = 'bos.trpengajuandana.grid2_deleterow("+recid+")' class='btn btn-danger btn-grid'>Delete</button>";
            w2ui[this.id + '_grid2'].add({recid:recid,no: no, username: datagrid[i]["username"], namakaryawan: datagrid[i]["namakaryawan"],
                                          jabatan: datagrid[i]["jabatan"], tahap:datagrid[i]["tahap"], kettahap:datagrid[i]["kettahap"],cmddelete:Hapus});
        }
    }

    bos.trpengajuandana.cmdedit 		= function(faktur){
        bjs.ajax(this.url + '/editing', 'faktur=' + faktur);
    }

    bos.trpengajuandana.cmddelete 	= function(faktur){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'faktur=' + faktur);
        }
    }

    bos.trpengajuandana.cmdcetak	= function(faktur){
        bjs.ajax(this.url + '/cetak', 'faktur='+faktur);
    }

    bos.trpengajuandana.showReport = function(urlfile){
        bjs_os.form_report(urlfile) ;
    }
    
	bos.trpengajuandana.cmdcetakdm	= function(faktur){
        bjs.ajax(this.url + '/cetakdm', 'faktur='+faktur);
    }
	
    bos.trpengajuandana.printbukti = function (contenthtml) {
        var mywindow = window.open('', 'Print', 'height=600,width=800');

        mywindow.document.write(contenthtml);

        mywindow.document.close();
        mywindow.focus()
        mywindow.print();
        mywindow.close();
    }


    bos.trpengajuandana.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.trpengajuandana.init 			= function(){
        this.obj.find("#keterangan").val("") ;
        this.obj.find("#yangmengajukan").val("") ;
        this.obj.find("#jumlah").val("0.00") ;
        bos.trpengajuandana.setopt("jenis","0");


        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getfaktur') ;
        w2ui[this.id + '_grid2'].clear();

        bos.trpengajuandana.initdetail();
    }

    bos.trpengajuandana.settab 		= function(n){
        this.obj.find("#tpengdana button:eq("+n+")").tab("show") ;
    }

    bos.trpengajuandana.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trpengajuandana.grid1_render() ;
            bos.trpengajuandana.init() ;
        }else{
            bos.trpengajuandana.grid2_reload() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#faktur").focus() ;
        }
    }
    bos.trpengajuandana.initdetail 			= function(){
        var datagrid = w2ui[this.id + '_grid2'].records;

        this.obj.find("#nomor").val(datagrid.length+1) ;
        this.obj.find("#otorisasi").sval("") ;
        this.obj.find("#jabatan").val("") ;
        this.obj.find("#tahap").sval("") ;
    }

    bos.trpengajuandana.initcomp		= function(){
        bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        //slecet2
        this.obj.find('.otorisasiselect').select2({
            ajax: {
                url: bos.trpengajuandana.base_url + '/seekotorisasi',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;
        w2ui[this.id + '_grid2'].hideColumn('username','tahap');

        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getfaktur') ;
    }

    bos.trpengajuandana.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trpengajuandana.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.trpengajuandana.grid1_destroy() ;
            bos.trpengajuandana.grid2_destroy() ;
        }) ;
    }

    bos.trpengajuandana.objs = bos.trpengajuandana.obj.find("#cmdsave") ;
    bos.trpengajuandana.initfunc	   = function(){
        this.obj.find("#cmdok").on("click", function(e){

            var no              = bos.trpengajuandana.obj.find("#nomor").val();
            var username             = bos.trpengajuandana.obj.find("#otorisasi").val();
            var namakaryawan    = bos.trpengajuandana.obj.find("#otorisasi").text();
            var jabatan         = bos.trpengajuandana.obj.find("#jabatan").val();
            var tahap      = bos.trpengajuandana.obj.find("#tahap").val();
            var kettahap      = bos.trpengajuandana.obj.find("#tahap").text();
            bos.trpengajuandana.grid2_append(no,username,namakaryawan,jabatan,tahap,kettahap);
        }) ;


        this.obj.find("#nomor").on("blur", function(e){
            var no = bos.trpengajuandana.obj.find("#nomor").val();
            var datagrid = w2ui['bos-form-trpengajuandana_grid2'].records;
            if(no <= datagrid.length){
                bos.trpengajuandana.obj.find("#otorisasi").sval([{id:w2ui['bos-form-trpengajuandana_grid2'].getCellValue(no-1,1),text:w2ui['bos-form-trpengajuandana_grid2'].getCellValue(no-1,2)}]);
                bos.trpengajuandana.obj.find("#jabatan").val(w2ui['bos-form-trpengajuandana_grid2'].getCellValue(no-1,3));
                bos.trpengajuandana.obj.find("#tahap").sval([{id:w2ui['bos-form-trpengajuandana_grid2'].getCellValue(no-1,4),text:w2ui['bos-form-trpengajuandana_grid2'].getCellValue(no-1,5)}]);
            }else{
                bos.trpengajuandana.obj.find("#nomor").val(datagrid.length + 1);
            }

        });

        this.obj.find('form').on("submit", function(e){
            //this.obj.find("#cmdsave").on("click", function(e){
            e.preventDefault() ;
            if( bjs.isvalidform(this) ){
                var datagrid2 =  w2ui['bos-form-trpengajuandana_grid2'].records;
                datagrid2 = JSON.stringify(datagrid2);
                bjs.ajax( bos.trpengajuandana.base_url + '/saving', bjs.getdataform(this)+"&grid2="+datagrid2 , bos.trpengajuandana.cmdsave) ;
            }

        }) ;

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trpengajuandana.grid1_reloaddata();
        });

        this.obj.find("#otorisasi").on("change", function(e){
            var otorisasi =  bos.trpengajuandana.obj.find("#otorisasi").val();
            var tgl =  bos.trpengajuandana.obj.find("#tgl").val();
            bjs.ajax( bos.trpengajuandana.base_url + '/getdataotorisasi', "username="+otorisasi+"&tgl="+tgl) ;
        });
    }

    $(function(){
        bos.trpengajuandana.initcomp() ;
        bos.trpengajuandana.initcallback() ;
        bos.trpengajuandana.initfunc() ;
        bos.trpengajuandana.initdetail();
    });
</script>