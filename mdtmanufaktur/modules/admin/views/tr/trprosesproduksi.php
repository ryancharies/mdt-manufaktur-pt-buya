<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tprosesproduksi">
                        <button class="btn btn-tab tpel active" href="#tprosesproduksi_1" data-toggle="tab" >Daftar Produksi</button>
                        <button class="btn btn-tab tpel" href="#tprosesproduksi_2" data-toggle="tab">Proses Produksi</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trprosesproduksi.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tprosesproduksi_1" style="padding-top:5px;">
                    <div class="row">

                        <div class="col-md-10">
                            <div class="form-group">
                                <label>Cabang</label>
                                <div class="input-group">
                                    <select name="skd_cabang" id="skd_cabang" class="form-control s2" style="width:100%"
                                                            data-placeholder="Pilih Cabang" data-sf="load_cabang"></select>
                                    <span class="input-group-addon">
                                        <span id="cmdcabang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                                    </span>
                                
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group full-width">
                                <label>&nbsp;</label>
                                <div class="input-group full-width">
                                        <button type="button" class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="row" style="height: calc(100% - 50px);"> -->
                        <div id="grid1" class="full-height" style="height: calc(100% - 50px);"></div>
                    <!-- </div> -->
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tprosesproduksi_2">
                    <table width="100%" height='100%'>
                        <tr>
                            <td width="100%">
                                <table width="100%" height="100%">
                                    <tr>
                                        <td width = "50%" >
                                            <table class="osxtable form">
                                                <tr>
                                                    <td width="14%"><label for="fakturprod">Faktur SPP</label> </td>
                                                    <td width="1%">:</td>
                                                    <td width="35%">
                                                        <input type="text" readonly id="fakturprod" name="fakturprod" class="form-control" placeholder="Faktur" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="cabang">Cabang</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <select name="cabang" readonly id="cabang" class="form-control select" style="width:100%"
                                                                data-placeholder="Cabang" required></select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="tglprod">Tanggal</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input style="width:80px" readonly type="text" class="form-control datetr" id="tglprod" name="tglprod" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="perbaikan">Perbaikan</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input style="width:40px" readonly type="text" class="form-control" id="perbaikan" name="perbaikan" required >
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="14%"><label for="petugas">Karu</label> </td>
                                                    <td width="1%">:</td>
                                                    <td >
                                                        <input readonly type="text" class="form-control" id="petugas" name="petugas" required >
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td width = "50%" height="100%">
                                            <div id="grid4" class="full-height"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "20px" class="osxtable form">
                                <table>
                                    <tr>
                                        <td><label for="faktur">Faktur</label> </td>
                                        <td><label for="tgl">Tgl</label> </td>
                                        <td><label for="gudang">Gudang</label> </td>
                                        <td></td>
                                        <td><label for="stock">Bahan Baku</label> </td>
                                        <td><label>Nama BB</label> </td>
                                        <td><label for="qty">Qty</label> </td>
                                        <td><label>Satuan</label> </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><input type="text" readonly id="faktur" name="faktur" class="form-control" placeholder="Faktur" ></td>
                                        <td><input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl"  value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>></td>
                                        <td><select name="gudang" id="gudang" class="form-control scons" style="width:100%"
                                                                data-placeholder="Gudang" data-sf="load_gudang"  required>
                                            </select></td>
                                        <td><button class="form-control btn btn-warning pull-right" type="button"  id="cmdautobb">BB Auto</button></td>
                                        <td>
                                            <div class="input-group">
                                                <input type="text" id="stock" name="stock" class="form-control" placeholder="Produk">
                                                <span class="input-group-btn">
                                                    <button class="form-control btn btn-info" type="button" id="cmdstock"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </td>
                                        <td><input type="text" id="namastock" readonly name="namastock" class="form-control" placeholder="Nama Produk"></td>
                                        <td><input maxlength="10" type="text" name="qty" id="qty" class="form-control number" value="0"></td>
                                        <td><input type="text" id="satuan" readonly name="satuan" class="form-control" placeholder="Satuan"></td>
                                        <td><button class="form-control btn btn-primary pull-right" type="button"  id="cmdok">OK</button></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "230px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    bos.trprosesproduksi.grid1_data    = null ;
    bos.trprosesproduksi.grid1_loaddata= function(){
        mdl_cabang.vargr.selection = bos.trprosesproduksi.obj.find('#skd_cabang').val();
        this.grid1_data 		= {
			'skd_cabang':JSON.stringify(mdl_cabang.vargr.selection)
		} ;
    }

    bos.trprosesproduksi.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trprosesproduksi.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers: true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'cabang', caption: 'Cabang', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'regu', caption: 'Regu', size: '100px', sortable: false , style:"text-align:left"},
                { field: 'karu', caption: 'Karu', size: '120px', sortable: false, style:"text-align:left"},
                { field: 'stt', caption: 'Status', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
            ]
        });
    }

    bos.trprosesproduksi.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trprosesproduksi.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trprosesproduksi.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trprosesproduksi.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trprosesproduksi.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }
    
    //grid BB
    bos.trprosesproduksi.grid2_data    = null ;
    bos.trprosesproduksi.grid2_loaddata= function(){

        var fakturproduksi = bos.trprosesproduksi.obj.find("#fakturprod").val();
        this.grid2_data 		= {'fakturproduksi':fakturproduksi} ;
    }

    bos.trprosesproduksi.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
            multiSelect : false,
            url 	: bos.trprosesproduksi.base_url + "/loadgrid2",
            postData: this.grid2_data ,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers: true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '150px', sortable: false },
                { field: 'tgl', caption: 'Tanggal', size: '100px', sortable: false },
                { field: 'gudang', caption: 'Gudang', size: '100px', sortable: false },
                { field: 'stock', caption: 'Stock', size: '100px', sortable: false },
                { field: 'namastock', caption: 'Nama Stock', size: '150px', sortable: false },
                { field: 'qty', caption: 'Qty', size: '70px', sortable: false, style:'text-align:right'},
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false},
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });


    }

    bos.trprosesproduksi.grid2_reloaddata	= function(){
        this.grid2_loaddata() ;
        this.grid2_setdata() ;
        this.grid2_reload() ;
    }

    bos.trprosesproduksi.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trprosesproduksi.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
     bos.trprosesproduksi.grid2_setdata	= function(){
        w2ui[this.id + '_grid2'].postData 	= this.grid2_data ;
    }

    bos.trprosesproduksi.grid2_deleterow = function(faktur){
        if(confirm("Penggunaan BB di hapus dari detail produksi???"+faktur)){
            bjs.ajax( bos.trprosesproduksi.base_url + '/deletebb', bjs.getdataform(this)+"&faktur="+faktur) ;
        }
    }


    //grid4 daftarstock
    bos.trprosesproduksi.grid4_load    = function(){
        this.obj.find("#grid4").w2grid({
            name	: this.id + '_grid4',
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers: true
            },
            columns: [
                { field: 'stock', caption: 'Kode', size: '100px', sortable: false},
                { field: 'namastock', caption: 'Keterangan', size: '200px', sortable: false },
                { field: 'qty', caption: 'Qty', size: '70px', sortable: false, style:'text-align:right'},
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false }
            ]
        });
    }

    bos.trprosesproduksi.grid4_reload		= function(){
        w2ui[this.id + '_grid4'].reload() ;
    }
    bos.trprosesproduksi.grid4_destroy 	= function(){
        if(w2ui[this.id + '_grid4'] !== undefined){
            w2ui[this.id + '_grid4'].destroy() ;
        }
    }

    bos.trprosesproduksi.cmdedit 		= function(faktur){
        bjs.ajax(this.url + '/editing', 'faktur=' + faktur,'',function(v){
            v = JSON.parse(v);
            with(bos.trprosesproduksi.obj){
                find(".nav-tabs li:eq(1) a").tab("show") ;
                find("#fakturprod").val(v.faktur) ;
                find("#petugas").val(v.karu) ;
                find("#perbaikan").val(v.perbaikan) ;
                find("#tglprod").val(v.tgl);
                find("#tgl").val(v.tgl);               
                find("#cabang").sval([{id:v.cabang,text:v.ketcabang}]);
                find('#gudang').attr('data-sp',v.cabang);
            }
            console.log(v.detil);
            n = 0;
            $.each(v.detil, function(i, val){
                
                w2ui['bos-form-trprosesproduksi_grid4'].add([{
                        recid: n++,
                        stock: val.stock,
                        namastock: val.namastock,
                        qty: val.qty,
                        satuan: val.satuan
                    }]) ;
            });
            bos.trprosesproduksi.initdetail();
            bos.trprosesproduksi.settab(1) ;
            bos.trprosesproduksi.grid2_reloaddata() ;
            bos.trprosesproduksi.grid2_reload() ;

        });
    }

    bos.trprosesproduksi.cmddelete 	= function(faktur){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'faktur=' + faktur);
        }
    }

    bos.trprosesproduksi.init 			= function(){
        this.obj.find("#cabang").sval("") ;
        this.obj.find("#fakturprod").val("") ;
        this.obj.find("#petugas").val("") ;

        bjs.ajax(this.url + '/init') ;
        w2ui[this.id + '_grid2'].clear();
        w2ui[this.id + '_grid4'].clear();

    }

    bos.trprosesproduksi.settab 		= function(n){
        this.obj.find("#tprosesproduksi button:eq("+n+")").tab("show") ;
    }

    bos.trprosesproduksi.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trprosesproduksi.grid1_render() ;
            bos.trprosesproduksi.init() ;
        }else{
            bos.trprosesproduksi.grid2_reloaddata() ;
            bos.trprosesproduksi.grid4_reload() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#faktur").focus() ;
        }
    }

    bos.trprosesproduksi.initdetail 			= function(){
        var datagrid = w2ui[this.id + '_grid2'].records;
        this.obj.find("#nomor").val(datagrid.length+1) ;
        this.obj.find("#stock").val("") ;
        this.obj.find("#namastock").val("") ;
        this.obj.find("#qty").val("1") ;
        this.obj.find("#satuan").val("") ;
        bjs.ajax(this.url + '/getfaktur') ;
    }
    bos.trprosesproduksi.initcomp		= function(){
        bjs.initselect({
            class : "#" + this.id + " .scons",
            clear:false
        }) ;

        bjs.initselect({
            class : "#" + this.id + " .s2",
            multi:true,
            clear:true
        }) ;


        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_loaddata() ;
        this.grid2_load() ;

        this.grid4_load() ;
        bjs.ajax(this.url + '/init') ;
    }

    bos.trprosesproduksi.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trprosesproduksi.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.trprosesproduksi.grid1_destroy() ;
            bos.trprosesproduksi.grid2_destroy() ;
            bos.trprosesproduksi.grid4_destroy() ;
        }) ;
    }


    //bos.trprosesproduksi.objs = bos.trprosesproduksi.obj.find("#cmdsave") ;
    bos.trprosesproduksi.initfunc	   = function(){
        this.obj.find("#cmdok").on("click", function(e){
            //alert("dnfksdf");
            bos.trprosesproduksi.gudang = bos.trprosesproduksi.obj.find("#gudang").val();
            bos.trprosesproduksi.stock = bos.trprosesproduksi.obj.find("#stock").val();

            if(bos.trprosesproduksi.gudang !== "" && bos.trprosesproduksi.stock !== ""){
                var tgl = bos.trprosesproduksi.obj.find("#tgl").val();
                var stock = bos.trprosesproduksi.obj.find("#stock").val();
                var qty = bos.trprosesproduksi.obj.find("#qty").val();
                var fakturprod = bos.trprosesproduksi.obj.find("#fakturprod").val();
                var gudang = bos.trprosesproduksi.obj.find("#gudang").val();
                var content = "&tgl="+tgl+"&stock="+stock+"&qty="+qty+"&fakturprod="+fakturprod+"&gudang="+gudang;
                bjs.ajax( bos.trprosesproduksi.base_url + '/savingbb', bjs.getdataform(this)+content) ; 
            }else{
                alert("Data tidak valid!!");
            }
            
            //bos.trprosesproduksi.grid2_append(tgl,stock,qty);
        }) ;

        this.obj.find("#cmdstock").on("click", function(e){
            mdl_barang.open(function(r){
                r = JSON.parse(r);
                bos.trprosesproduksi.obj.find("#stock").val(r.kode);
                bos.trprosesproduksi.obj.find("#namastock").val(r.keterangan);
                bos.trprosesproduksi.obj.find("#satuan").val(r.satuan);
                bos.trprosesproduksi.obj.find("#qty").focus();

            },{'tampil':['B','S'],'orderby':'tampil asc, keterangan asc'});
        }) ;

        this.obj.find("#stock").on("blur", function(e){
            if(bos.trprosesproduksi.obj.find("#stock").val() !== ""){
                var stock = bos.trprosesproduksi.obj.find("#stock").val();
                var tampil = ['B','S'];
                var qty = 0;
                if(stock.indexOf("*") > 0){
                    res            = stock.split("*");
                    stock = res[1];
                    qty = res[0];
                }
                bjs.ajax('admin/load/mdl_stock_seek', 'stock=' + stock + "&tampil="+ JSON.stringify(tampil),'',function(r){
                    r = JSON.parse(r);
                    // console.log(r.length);
                    if(r.kode != undefined){
                        bos.trprosesproduksi.obj.find("#stock").val(r.kode);
                        bos.trprosesproduksi.obj.find("#namastock").val(r.keterangan);
                        bos.trprosesproduksi.obj.find("#qty").val(qty);
                        bos.trprosesproduksi.obj.find("#satuan").val(r.satuan);
                    }else{
                        alert("data tidak ditemukan!!, Cek data barang pada menu stok / barang");
                    }
                });
            }
        });

        this.obj.find("#cmdautobb").on("click", function(e){
            //bos.trprosesproduksi.loadmodelstock("show");
            
            //bos.trprosesproduksi.grid3_reloaddata() ;
            var lcnfm = confirm("Apakah semua bahan baku pada perintah produksi akan dikeluarkan pada proses saat ini?? pastikan data benar seperti faktur SPP,tgl dan gudang");
            if(lcnfm){
                var tgl = bos.trprosesproduksi.obj.find("#tgl").val();
                var fakturprod = bos.trprosesproduksi.obj.find("#fakturprod").val();
                var gudang = bos.trprosesproduksi.obj.find("#gudang").val();
                if(tgl == '' || fakturprod == '' || gudang == null){
                    alert("Data tidak valid...");
                }else{
                    var content = "&tgl="+tgl+"&fakturprod="+fakturprod+"&gudang="+gudang;
                    bjs.ajax( bos.trprosesproduksi.base_url + '/autoprosesbb', bjs.getdataform(this)+content) ;
                }
            }
        }) ;

        this.obj.find("#nomor").on("blur", function(e){
            var no = bos.trprosesproduksi.obj.find("#nomor").val();
            var datagrid = w2ui['bos-form-trprosesproduksi_grid2'].records;
            if(no <= datagrid.length){
                bos.trprosesproduksi.obj.find("#stock").val(w2ui['bos-form-trprosesproduksi_grid2'].getCellValue(no-1,1));
                bos.trprosesproduksi.obj.find("#namastock").val(w2ui['bos-form-trprosesproduksi_grid2'].getCellValue(no-1,2));
                bos.trprosesproduksi.obj.find("#qty").val(w2ui['bos-form-trprosesproduksi_grid2'].getCellValue(no-1,3));
                bos.trprosesproduksi.obj.find("#satuan").val(w2ui['bos-form-trprosesproduksi_grid2'].getCellValue(no-1,4));
            }else{
                bos.trprosesproduksi.obj.find("#nomor").val(datagrid.length + 1)
            }

        });

        this.obj.find('#cmdcabang').on('click', function(){
            mdl_cabang.vargr.selection = bos.trprosesproduksi.obj.find('#skd_cabang').val();
            mdl_cabang.open(function(r){
                r = JSON.parse(r);
                bos.trprosesproduksi.cabang = [];
                $.each(r, function(i, v){
                    bos.trprosesproduksi.cabang.push({'id':v.kode,'text':v.keterangan})                   
                });
                bos.trprosesproduksi.obj.find('#skd_cabang').sval(bos.trprosesproduksi.cabang);
            });
        });

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trprosesproduksi.grid1_reloaddata();
        });
    }

    $(function(){
        bos.trprosesproduksi.initcomp() ;
        bos.trprosesproduksi.initcallback() ;
        bos.trprosesproduksi.initfunc() ;
        bos.trprosesproduksi.initdetail();
    });
</script>