<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tkaryawankontrak">
                        <button class="btn btn-tab tpel active" href="#tkaryawankontrak_1" data-toggle="tab" >Daftar Karyawan</button>
                        <button class="btn btn-tab tpel" href="#tkaryawankontrak_2" data-toggle="tab">Kontrak</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trkaryawankontrak.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<form novalidate>
<div class="body">
    
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tkaryawankontrak_1" style="padding-top:5px;">
                    <table width="100%" height="100%">
                        <tr>
                            <td height="25px" width="100%">
                                <table class="osxtable form">
                                    <tr>
                                        <td width="50px">
                                            Tgl : 
                                        </td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="5px">sd</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <!-- <td width="85px">
                                            <button type="button" class="btn btn-danger pull-right" id="cmdcetak">Cetak</button>
                                        </td> -->
                                        <td></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tkaryawankontrak_2">
                    <table class="osxtable form">
                        <tr>
                            <td width="100%">
                                <table>
                                    <tr>
                                        <td valign="top">
                                            <table>
                                                <tr>
                                                    <td width="100px"><label for="kode">Kode</label> </td>
                                                    <td width="5px">:</td>
                                                    <td >
                                                        <div class="input-group">
                                                            <input readonly type="text" id="kode" name="kode" class="form-control" placeholder="Kode" required>
                                                            <span class="input-group-btn">
                                                                <button class="form-control btn btn-info" type="button" id="cmdnip"><i class="fa fa-search"></i></button>
                                                            </span>
                                                        </div>
                                                    </td>
                                                
                                                    <td width="100px"><label for="nama">Nama</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input readonly type="text" id="nama" name="nama" class="form-control" placeholder="Nama">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px"><label for="ktp">No KTP</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input readonly type="text" id="ktp" name="ktp" class="form-control" placeholder="No KTP">
                                                    </td>
                                                
                                                    <td width="100px"><label for="notelepon">No Telp</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input readonly type="text" id="notelepon" name="notelepon" class="form-control" placeholder="notelepon">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px"><label for="alamat">Alamat</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input readonly type="text" id="alamat" name="alamat" class="form-control" placeholder="alamat">
                                                    </td>
                                                    <td width="100px"><label for="bagjab">Bagian - Jabatan</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input readonly type="text" id="bagjab" name="jabatan" class="form-control" placeholder="Bagian - Jabatan">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td height = "100%" style="border:1px solid;" valign="top">
                                            <table width = "100%" height = "100%">
                                                <tr>
                                                    <td height = "100%" style="height:100%;width:100%;">
                                                        Foto Karyawan <span id="idlfotokaryawan"></span>
                                                        <table width = "100%" height = "100%">
                                                            <tr>
                                                                <td height = "100%">
                                                                    <div class="row" id="fotobrowse" style="height:100%;width:100%;overflow: scroll;">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td width="100px"><label for="noperjanjian">No Perjanjian</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input readonly type="text" id="noperjanjian" name="noperjanjian" class="form-control" placeholder="No Perjanjian" required>
                                        </td>   

                                        <td width="100px"><label for="nomor">Nomor</label> </td>
                                        <td width="5px">:</td>
                                        <td>
                                            <input type="text" id="nomor" name="nomor" class="form-control" placeholder="Nomor">
                                        </td>
                                        
                                        <td width="100px"><label for="tgl">Tgl</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input style="width:80px" type="text" class="form-control date" id="tgl" name="tgl" value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px"><label for="lama">Lama</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <div class="input-group">
                                                <input type="text" style="width:80px" id="lama" name="lama" class="form-control number" placeholder="Lama" required>
                                                <span class="input-group-select">
                                                <select name="periode" id="periode" class="form-control select" style="width:100px"
                                                data-sf="load_periode" data-placeholder="Periode" required></select>
                                                </span>
                                            </div>
                                        </td>   

                                        <td width="100px"><label for="tglpkawal">Tgl Awal</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input style="width:80px" type="text" class="form-control datetr" id="tglpkawal" name="tglpkawal" value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                        </td>
                                        
                                        <td width="100px"><label for="tglpkakhir">Tgl Akhir</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input readonly style="width:80px" type="text" class="form-control datetr" id="tglpkakhir" name="tglpkakhir" value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px"><label for="bagian">Bagian</label> </td>
                                        <td width="5px">:</td>
                                        <td>
                                            <select name="bagian" id="bagian" class="form-control select2" style="width:100px"
                                                data-placeholder="Bagian" required></select>
                                        </td>
                                        <td width="100px"><label for="jabatan">Jabatan</label> </td>
                                        <td width="5px">:</td>
                                        <td>
                                            <select name="jabatan" id="jabatan" class="form-control select2" style="width:100px"
                                                data-placeholder="Jabatan" required></select>
                                        </td>
                                    </tr>
                                    
                                </table>
                            </td>
                        <tr>
                        
                        
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button type="button" class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>


        
    
</div>
<div class="modal fade" id="wrap-cetakpk-d" role="dialog" data-backdrop="false" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="wm-title">Cetak Kontrak Kerja Karyawan</h4>
                    </div>
                    <div class="modal-body">
                        <table class="osxtable form">
                            <tr>
                                    <td width="100px">No Perjanjian</td>
                                    <td width="5px">:</td>
                                    <td>
                                        <input readonly type="text" id="noperjanjianctk" name="noperjanjianctk" class="form-control" placeholder="No Perjanjian Cetak">
                                    </td>
                            </tr>
                            <tr>
                                <td width="100px">Surat</td>
                                <td width="5px">:</td>
                                <td>
                                    <select name="surat" id="surat" class="form-control select2" style="width:100px"
                                                data-placeholder="Pilih Surat" required></select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <table class="osxtable form" border="0">
                            <tr>
                                <td><a id="linkdownlaodfile" href=""></a></td>
                                <td>
                                            *Pilih Surat Yang akan dicetak &nbsp;&nbsp;&nbsp;
                                    <button type="button" class="btn btn-primary pull-right" id="cmdcetak">Cetak</button>
                                </td>
                            </tr>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
<div class="modal fade" id="wrap-pencariankaryawan-d" role="dialog" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="wm-title">Daftar Karyawan</h4>
            </div>
            <div class="modal-body">
                <div id="grid2" style="height:250px"></div>
            </div>
            <div class="modal-footer">
                *Pilih Karyawan
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.trkaryawankontrak.grid1_data 	 = null ;
    bos.trkaryawankontrak.grid1_loaddata= function(){
        var tglawal = bos.trkaryawankontrak.obj.find("#tglawal").val();
        var tglakhir = bos.trkaryawankontrak.obj.find("#tglakhir").val();
        this.grid1_data 		= {'tglawal':tglawal,'tglakhir':tglakhir} ;
    }

    bos.trkaryawankontrak.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.trkaryawankontrak.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false, 
            columns: [
                { field: 'noperjanjian', caption: 'No Perjanjian', size: '150px', sortable: false,style:"text-align:center;",frozen:true},
                { field: 'nomor', caption: 'Nomor', size: '100px', sortable: false,style:"text-align:center;",frozen:true},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false,style:"text-align:center;",frozen:true},
                { field: 'lamaperiode', caption: 'Lama', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'tglawal', caption: 'Tgl Awal', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'tglakhir', caption: 'Tgl Akhir', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'kode', caption: 'NIP', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false},
                { field: 'ktp', caption: 'No KTP', size: '100px', sortable: false},
                { field: 'alamat', caption: 'Alamat', size: '200px', sortable: false},
                { field: 'telepon', caption: 'Telepon', size: '100px', sortable: false},
                { field: 'cmdcetakpk', caption: ' ', size: '100px', sortable: false },
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }
    bos.trkaryawankontrak.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trkaryawankontrak.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trkaryawankontrak.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trkaryawankontrak.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trkaryawankontrak.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid2 daftar karyawan
    bos.trkaryawankontrak.grid2_data    = null ;
    bos.trkaryawankontrak.grid2_loaddata= function(){
        this.grid2_data 		= {} ;
    }

    bos.trkaryawankontrak.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            limit 	: 100 ,
            url 	: bos.trkaryawankontrak.base_url + "/loadgrid2",
            postData: this.grid2_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'kode', caption: 'NIP', size: '100px', sortable: false},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false },
                { field: 'jabatan', caption: 'Jabatan', size: '100px', sortable: false },
                { field: 'bagian', caption: 'Bagian', size: '100px', sortable: false },
                { field: 'cmdpilih', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trkaryawankontrak.grid2_setdata	= function(){
        w2ui[this.id + '_grid2'].postData 	= this.grid2_data ;
    }
    bos.trkaryawankontrak.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.trkaryawankontrak.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trkaryawankontrak.grid2_render 	= function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.trkaryawankontrak.grid2_reloaddata	= function(){
        this.grid2_loaddata() ;
        this.grid2_setdata() ;
        this.grid2_reload() ;
    }

    bos.trkaryawankontrak.cmdpilih 		= function(kode){
        bjs.ajax(this.url + '/pilihkaryawan',bjs.getdataform(bos.trkaryawankontrak.obj.find("form"))+'&kode=' + kode);
    }

    bos.trkaryawankontrak.cmdedit		= function(noperjanjian){
        bjs.ajax(this.url + '/editing', 'noperjanjian=' + noperjanjian);
    }

    bos.trkaryawankontrak.cmdcetakpk		= function(noperjanjian){
        if(confirm("Apakah kontrak akan dicetak???")){
            bjs.ajax(this.url + '/cetakpk', 'noperjanjian=' + noperjanjian);
        }
    }

    bos.trkaryawankontrak.cmddelete		= function(noperjanjian){
        if(confirm("Hapus Data?")){
            bjs.ajax(this.url + '/deleting', 'noperjanjian=' + noperjanjian);
        }
    }

    bos.trkaryawankontrak.getnoperjanjian		= function(){
        var tgl  = this.obj.find("#tgl").val();
        bjs.ajax(this.url + '/getnoperjanjian', 'tgl=' + tgl);
    }

    bos.trkaryawankontrak.gettglakhirkontrak		= function(){
        var tglawal  = this.obj.find("#tglpkawal").val();
        var lama  = this.obj.find("#lama").val();
        var periode  = this.obj.find("#periode option:selected").val();
        bjs.ajax(this.url + '/gettglakhirkontrak', 'tglawal=' + tglawal + '&lama=' + lama + '&periode=' + periode);
    }

    bos.trkaryawankontrak.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#nama").val("") ;
        this.obj.find("#notelepon").val("") ;
        this.obj.find("#ktp").val("") ;
        this.obj.find("#alamat").val("") ;
        this.obj.find("#fotobrowse").html("");
        this.obj.find("#noperjanjian").val("") ;
        this.obj.find("#nomor").val("") ;
        this.obj.find("#lama").val("") ;
        this.obj.find("#periode").sval("") ;
        this.obj.find("#bagian").sval("") ;
        this.obj.find("#jabatan").sval("") ;
    }

    bos.trkaryawankontrak.settab 		= function(n){
        this.obj.find("#tkaryawankontrak button:eq("+n+")").tab("show") ;
    }

    bos.trkaryawankontrak.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trkaryawankontrak.grid1_render() ;
            bos.trkaryawankontrak.init() ;
        }else{
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#tglkeluar").focus() ;
        }
    }

    bos.trkaryawankontrak.initcomp	= function(){
        bjs.initselect({
            class : "#" + this.id + " .select",
            clear : true
        }) ;
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;        
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.init() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;

        bos.trkaryawankontrak.getnoperjanjian();

        this.obj.find('#bagian').select2({
            ajax: {
                url: bos.trkaryawankontrak.base_url + '/seekbagian',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.obj.find('#jabatan').select2({
            ajax: {
                url: bos.trkaryawankontrak.base_url + '/seekjabatan',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.obj.find('#surat').select2({
            ajax: {
                url: bos.trkaryawankontrak.base_url + '/seeksurat',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
    }

    bos.trkaryawankontrak.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trkaryawankontrak.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.trkaryawankontrak.grid1_destroy() ;
            bos.trkaryawankontrak.grid2_destroy() ;
        }) ;
    }

    bos.trkaryawankontrak.loadmodelnip      = function(l){
        this.obj.find("#wrap-pencariankaryawan-d").modal(l) ;
    }

    bos.trkaryawankontrak.loadmodelcetakpk      = function(l){
        this.obj.find("#wrap-cetakpk-d").modal(l) ;
    }

    bos.trkaryawankontrak.objs = bos.trkaryawankontrak.obj.find("#cmdsave") ;
    bos.trkaryawankontrak.objc = bos.trkaryawankontrak.obj.find("#cmdcetak") ;
    bos.trkaryawankontrak.initfunc 		= function(){
        
        this.obj.find("#cmdnip").on("click", function(e){
            bos.trkaryawankontrak.loadmodelnip("show");
            bos.trkaryawankontrak.grid2_reloaddata() ;
        }) ;

        this.obj.find("#tglpkawal").on("change", function(e){
            bos.trkaryawankontrak.gettglakhirkontrak() ;
        }) ;

        this.obj.find("#lama").on("change", function(e){
            bos.trkaryawankontrak.gettglakhirkontrak() ;
        }) ;

        this.obj.find("#periode").on("change", function(e){
            bos.trkaryawankontrak.gettglakhirkontrak() ;
        }) ;
        
        this.obj.find("#cmdsave").on("click",function(){
            if(confirm("Kontrak kerja karyawanakan disimpan?? \n Pastikan Data telah benar...")){
                bjs.ajax( bos.trkaryawankontrak.base_url + '/saving', bjs.getdataform(bos.trkaryawankontrak.obj.find("form")), bos.trkaryawankontrak.objs) ;               
            }
        });

        this.obj.find("#cmdcetak").on("click",function(){
            var noperjanjianctk = bos.trkaryawankontrak.obj.find("#noperjanjianctk").val();
            var surat  = bos.trkaryawankontrak.obj.find("#surat option:selected").val();
            bjs.ajax( bos.trkaryawankontrak.base_url + '/cetakpktodocx', "&noperjanjianctk="+noperjanjianctk+"&surat="+surat, bos.trkaryawankontrak.objc) ;               
            
        });

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trkaryawankontrak.grid1_reloaddata();
        });
    }

    $(function(){
        bos.trkaryawankontrak.initcomp() ;
        bos.trkaryawankontrak.initcallback() ;
        bos.trkaryawankontrak.initfunc() ;
    }) ;
</script>
