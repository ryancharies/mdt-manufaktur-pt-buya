<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="taspay">
                        <button class="btn btn-tab tpel active" href="#taspay_1" data-toggle="tab" >Daftar Pembayaran Asuransi</button>
                        <button class="btn btn-tab tpel" href="#taspay_2" data-toggle="tab">Pembayaranasuransi</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.trasuransipembayaran.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="taspay_1" style="padding-top:5px;">
                    <table width="100%">
                        <tr>
                            <td height="25px" width="100%">
                                <table class="osxtable form">
                                    <tr>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="5px">sd</td>
                                        <td width="85px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="85px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td></td>
                                    </tr>

                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="400px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="taspay_2">
                    <table width="100%">                        
                        <tr>
                            
                            <td width="100%" class="osxtable form">
                                <table width="100%">
                                    <tr>
                                        <td width="100px"><label for="faktur">Faktur</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input readonly type="text" id="faktur" name="faktur" class="form-control" placeholder="Faktur" required>
                                        </td>
                                        <td width="100px"><label for="rekkasbank">Rek. Kas</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                        <select name="rekkasbank" id="rekkasbank" class="form-control rekselect select" style="width:100%"
                                                            data-placeholder="Rek. Kas Bank" required></select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px"><label for="tgl">Tanggal</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?> <?=$mintgl?>>
                                        </td>
                                        <td width="100px"><label for="keterangan">Keterangan</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <input type="text" id="keterangan" name="keterangan" class="form-control" placeholder="Keterangan" required>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px"><label for="asuransi">Asuransi</label> </td>
                                        <td width="5px">:</td>
                                        <td >
                                            <select name="asuransi" id="asuransi" class="form-control select" style="width:100%"
                                                            data-placeholder="Asuransi" required></select>
                                        </td>
                                        
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height = "330px" >
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>       
                    </table>
                </div>
            </div>
        </div>
        <div class="footer fix hidden" style="height:32px">
            <button class="btn btn-primary pull-right" id="cmdsave">Simpan</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>
    //grid daftar po
    bos.trasuransipembayaran.grid1_data    = null ;
    bos.trasuransipembayaran.grid1_loaddata= function(){

        var tglawal = bos.trasuransipembayaran.obj.find("#tglawal").val();
        var tglakhir = bos.trasuransipembayaran.obj.find("#tglakhir").val();
        this.grid1_data 		= {'tglawal':tglawal,'tglakhir':tglakhir} ;
    }

    bos.trasuransipembayaran.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.trasuransipembayaran.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers  : true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center",frozen:true},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'asuransi', caption: 'Asuransi', size: '120px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'rekkas', caption: 'Rek. Kas', size: '150px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'karyawan', caption: 'Karyawan', size: '100px', sortable: false , render:'float:2'},
                { field: 'perusahaan', caption: 'Perusahaan', size: '100px', sortable: false , render:'float:2'},
                { field: 'total', caption: 'Total', size: '100px', sortable: false , render:'float:2'},
                { field: 'cmdcetak', caption: ' ', size: '80px', sortable: false },
                { field: 'cmdedit', caption: ' ', size: '80px', sortable: false },
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.trasuransipembayaran.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.trasuransipembayaran.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.trasuransipembayaran.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.trasuransipembayaran.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.trasuransipembayaran.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid detail
    bos.trasuransipembayaran.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            columns: [
                { field: 'nip', caption: 'NIP', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'nama', caption: 'Nama', size: '150px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'jabatan', caption: 'Jabatan', size: '150px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'bagian', caption: 'Bagian', size: '150px', sortable: false , style:"text-align:left",frozen:true},
                { field: 'nopendaftaran', caption: 'Pendaftaran', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'noasuransi', caption: 'No Asuransi', size: '120px', sortable: false , style:"text-align:center"},
                { field: 'asuransi', caption: 'Asuransi', size: '100px', sortable: false , style:"text-align:left"},
                { field: 'produk', caption: 'Produk', size: '200px', sortable: false , style:"text-align:left"},
                { field: 'tarifkaryawan', caption: 'Trf. Karyawan', size: '100px',render:"float:2",sortable: false},
                { field: 'tarifperusahaan', caption: 'Trf. Perusahaan', size: '100px',render:"float:2",sortable: false},
                { field: 'total', caption: 'Total', size: '100px',render:"float:2",sortable: false}

            ]
        });


    }

    bos.trasuransipembayaran.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.trasuransipembayaran.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }


    bos.trasuransipembayaran.cmdedit 		= function(faktur){
        bjs.ajax(this.url + '/editing', 'faktur=' + faktur);
    }

    bos.trasuransipembayaran.cmddelete 	= function(faktur){
        if(confirm("Delete Data?")){
            bjs.ajax(this.url + '/deleting', 'faktur=' + faktur);
        }
    }

    bos.trasuransipembayaran.cmdcetak	= function(faktur){
        bjs_os.form_report( this.base_url + '/showreport?faktur=' + faktur) ;
    }

    bos.trasuransipembayaran.cmdcetakdm	= function(faktur){
        if(confirm("Cetak Data?")){
            bjs.ajax(this.url + '/printbuktidm', 'faktur='+faktur);
        }
    }

    bos.trasuransipembayaran.hitungjumlah 			= function(){

        var nRows = w2ui[this.id + '_grid2'].records.length;
        var subtotal = 0 ;
        for(i=0;i< nRows;i++){
            var jumlah = w2ui[this.id + '_grid2'].getCellValue(i,3);
            subtotal += Number(jumlah);
        }
        this.obj.find("#pokok").val($.number(subtotal,2));

        var bdawal = string_2n(this.obj.find("#bakidebetawal").val());
        var bdakhir = bdawal - subtotal;
        this.obj.find("#bakidebetakhir").val($.number(bdakhir,2));
    }


    bos.trasuransipembayaran.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.trasuransipembayaran.init 			= function(){

        this.obj.find("#asuransi").sval("") ;
        this.obj.find("#rekkasbank").sval("") ;
        this.obj.find("#faktur").val("") ;
        this.obj.find("#keterangan").val("") ;

        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getfaktur') ;
       // w2ui[this.id + '_grid2'].clear();
        w2ui['bos-form-trasuransipembayaran_grid2'].clear();
    }

    bos.trasuransipembayaran.settab 		= function(n){
        this.obj.find("#taspay button:eq("+n+")").tab("show") ;
    }

    bos.trasuransipembayaran.tabsaction	= function(n){
        if(n == 0){
            bos.trasuransipembayaran.init() ;
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.trasuransipembayaran.grid1_render() ;
        }else{
            bos.trasuransipembayaran.grid2_reload() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#faktur").focus() ;
        }
    }

    bos.trasuransipembayaran.initcomp		= function(){
        bjs.initdate("#" + this.id + " .date") ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.obj.find('.rekselect').select2({
            ajax: {
                url: bos.trasuransipembayaran.base_url + '/seekrekening',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.obj.find('#asuransi').select2({
            ajax: {
                url: bos.trasuransipembayaran.base_url + '/seekasuransi',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;

        bjs.ajax(this.url + '/init') ;
        bjs.ajax(this.url + '/getfaktur') ;
    }

    bos.trasuransipembayaran.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.trasuransipembayaran.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.trasuransipembayaran.grid1_destroy() ;
            bos.trasuransipembayaran.grid2_destroy() ;
        }) ;
    }

    bos.trasuransipembayaran.loadmodelnopinjaman      = function(l){
        this.obj.find("#wrap-pencariannopinjaman-d").modal(l) ;
    }

    bos.trasuransipembayaran.objs = bos.trasuransipembayaran.obj.find("#cmdsave") ;
    bos.trasuransipembayaran.initfunc	   = function(){
        
        this.obj.find('form').on("submit", function(e){
            //this.obj.find("#cmdsave").on("click", function(e){
            if(confirm("Data disimpan ??")){
                e.preventDefault() ;
                if( bjs.isvalidform(this) ){
                    var datagrid2 =  w2ui['bos-form-trasuransipembayaran_grid2'].records;
                    datagrid2 = JSON.stringify(datagrid2);

                    var totkaryawan = "";
                    var totperusahaan = "";
                    if(w2ui['bos-form-trasuransipembayaran_grid2'] !== undefined){
                        indxtotkaryawan = w2ui['bos-form-trasuransipembayaran_grid2'].getColumn('tarifkaryawan', true);
                        totkaryawan = w2ui['bos-form-trasuransipembayaran_grid2'].getCellValue(0,indxtotkaryawan,true);

                        indxtotperusahaan = w2ui['bos-form-trasuransipembayaran_grid2'].getColumn('tarifperusahaan', true);
                        totperusahaan = w2ui['bos-form-trasuransipembayaran_grid2'].getCellValue(0,indxtotperusahaan,true);
                    }

                    bjs.ajax( bos.trasuransipembayaran.base_url + '/saving', bjs.getdataform(this)+"&grid2="+datagrid2+"&totkaryawan="+totkaryawan+"&totperusahaan="+totperusahaan, bos.trasuransipembayaran.cmdsave) ;
                }
            }

        }) ;

        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.trasuransipembayaran.grid1_reloaddata();
        });

        this.obj.find('#asuransi').on("change", function(e){
            w2ui['bos-form-trasuransipembayaran_grid2'].clear();
            var tgl = bos.trasuransipembayaran.obj.find("#tgl").val();
            var faktur = bos.trasuransipembayaran.obj.find("#faktur").val();
            var asuransi = bos.trasuransipembayaran.obj.find("#asuransi").val();
            
            
            //bos.trpelunasanhutang.grid2_reload() ;
            //e.preventDefault() ;
            
            bjs.ajax( bos.trasuransipembayaran.base_url + '/loadgrid2', bjs.getdataform(this)+"&tgl="+tgl+"&faktur="+faktur+"&asuransi="+asuransi) ;
        });

    
    }

    

    $(function(){
        bos.trasuransipembayaran.initcomp() ;
        bos.trasuransipembayaran.initcallback() ;
        bos.trasuransipembayaran.initfunc() ;
    });
</script>