<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Laporan Purchase Order</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptsaldopo.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div>
<div class="body">
    <form novalidate>         
        <div class="bodyfix scrollme" style="height:100%"> 
            <table class="osxtable form" border="0">
                <tr>
                    <td width="80px"><label for="tgl"> Sampai Tgl</label> </td>
                    <td width="20px">:</td>
                    <td width="100px">
                        <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                    </td> 

                    <td width="100px">
                        <button class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                    </td>
                    <td width="100px">	
                        <button class="btn btn-primary pull-right" id="cmdpreview">Preview</button>  
                    </td>
                    <td></td>
                </tr>  
                <tr><td colspan="6"><hr class="no-margin no-padding"></td></tr>
            </table> 
            <div class="row" style="height: calc(100% - 50px);"> 
                <div class="col-sm-12 full-height">
                    <div id="grid1" class="full-height"></div>
                </div>  
            </div> 
        </div>
        <div class="modal fade" style="position:absolute;" id="wrap-preview-detail-d" role="dialog" data-backdrop="false" data-keyboard="false">
            <div class="modal-dialog" style = "width:90%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="wm-title">Preview Detail Purchase Order</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Faktur</label>
                                    <input type="text" name="cFaktur" id="faktur" class="form-control" placeholder="faktur" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Supplier</label>
                                    <input type="text" name="supplier" id="supplier" readonly = true class="form-control">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Tgl</label>
                                    <input type="text" name="tgl" id="tgl" readonly = true class="form-control">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Faktur PR</label>
                                    <input type="text" name="fktpr" id="fktpr" class="form-control" placeholder="faktur" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Termin Hari</label>
                                    <input type="text" name="terminhari" id="terminhari" class="form-control" placeholder="Termin Hari" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Nama Bank</label>
                                    <input type="text" name="namabank" id="namabank" class="form-control" placeholder="Nama Bank" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Rekening</label>
                                    <input type="text" name="rekening" id="rekening" class="form-control" placeholder="Rekening" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>An. Rekening</label>
                                    <input type="text" name="atasnamarekening" id="atasnamarekening" class="form-control" placeholder="Atas Nama Rekening" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Telp</label>
                                    <input type="text" name="notelepon" id="notelepon" class="form-control" placeholder="No Telp" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Kontak Pers</label>
                                    <input type="text" name="kontakpersonal" id="kontakpersonal" class="form-control" placeholder="No Telp" readonly = true>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="grid2" style="height:250px"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Sub Total</label>
                                    <input type="text" style = "text-align:right" name="subtotal" id="subtotal" class="form-control number" placeholder="0.00" readonly = true required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Total</label>
                                    <input type="text" style = "text-align:right" name="total" id="total" class="form-control number" placeholder="0.00" readonly = true required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <br/>
                                    <button type = "button" class="btn btn-success" id="cmdCetakLaporanDetail">Print Report</button>
                                </div>
                            </div>
                            <!-- <div class="col-md-2">
                                <div class="form-group">
                                    <br/>
                                    <button type = "button" class="btn btn-primary" id="cmdcetakdm">Print Dot Matrix</button>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    <?=cekbosjs();?>


    //grid daftar pembelian
    bos.rptsaldopo.grid1_data    = null ;
    bos.rptsaldopo.grid1_loaddata= function(){
        var tglakhir = bos.rptsaldopo.obj.find("#tglakhir").val();
        this.grid1_data 		= {'tglakhir':tglakhir} ;
    }

    bos.rptsaldopo.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.rptsaldopo.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'no', caption: 'No', size: '30px', sortable: false, style:"text-align:right"},
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'fktpr', caption: 'Fkt PR', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'supplier', caption: 'Supplier', size: '300px', sortable: false , style:"text-align:left"},
                { field: 'tglpo', caption: 'Tgl PO', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'qty', render: 'float:2' ,caption: 'Saldo Qty', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'nominal', render: 'float:2' ,caption: 'Saldo Nominal', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'cmdPreview', caption: ' ', size: '120px', sortable: false }
            ]
        });
    }

    bos.rptsaldopo.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }

    bos.rptsaldopo.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.rptsaldopo.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptsaldopo.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rptsaldopo.grid1_reloadData	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }


    bos.rptsaldopo.cmdpreviewdetail	= function(faktur,tgl){
        bjs.ajax(this.url + '/PreviewDetail', 'faktur=' + faktur + '&tgl=' + tgl);
    }
	bos.rptsaldopo.cmdcetakdm 	= function(){
        if(confirm("Cetak data?")){
            var faktur = bos.rptsaldopo.obj.find("#faktur").val();
            bjs.ajax(this.url + '/cetakdm', 'faktur=' + faktur);
        }
    }

    bos.rptsaldopo.cmdcetaklpbdm 	= function(){
        if(confirm("Cetak LPB?")){
            var faktur = bos.rptsaldopo.obj.find("#faktur").val();
            bjs.ajax(this.url + '/cetaklpbdm', 'faktur=' + faktur);
        }
    }

    bos.rptsaldopo.cmdrefresh          = bos.rptsaldopo.obj.find("#cmdrefresh") ;
    bos.rptsaldopo.cmdpreview     = bos.rptsaldopo.obj.find("#cmdpreview")
    bos.rptsaldopo.cmdCetakLaporanDetail     = bos.rptsaldopo.obj.find("#cmdCetakLaporanDetail") ;
    bos.rptsaldopo.initfunc    = function(){
        this.obj.find("#cmdpreview").on("click", function(e){
            e.preventDefault() ;
            bos.rptsaldopo.initreportTotal(0,0) ;
        }) ;

        this.obj.find("#cmdrefresh").on("click", function(e){
            e.preventDefault() ;
            bos.rptsaldopo.grid1_reloadData() ;
        }) ;
        this.obj.find("#cmdCetakLaporanDetail").on("click",function(e){
            e.preventDefault() ;
            bos.rptsaldopo.initReportDetailPembelian(0,0) ;
		});
			
		this.obj.find("#cmdcetakdm").on("click", function(e){
            bos.rptsaldopo.cmdcetakdm();
		});

        this.obj.find("#cmdcetaklpbdm").on("click", function(e){
            bos.rptsaldopo.cmdcetaklpbdm();
		});
    }

    bos.rptsaldopo.initReportDetailPembelian  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.rptsaldopo.loadmodalpreview      = function(l){
        this.obj.find("#wrap-preview-detail-d").modal(l) ;
    }

    bos.rptsaldopo.initcomp		= function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;
        this.grid2_load() ;

        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drags
    }

    bos.rptsaldopo.initcallback	= function(){
        this.obj.on('remove', function(){
            bos.rptsaldopo.grid1_destroy() ;
            bos.rptsaldopo.grid2_destroy() ;
        }) ;
    }

    bos.rptsaldopo.setPreview      = function(cFaktur){
        bos.rptsaldopo.initreport(0,0) ;
    }

    bos.rptsaldopo.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.rptsaldopo.initreportTotal  = function(s,e){
        bjs.ajax(this.base_url+ '/initreportTotal', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.rptsaldopo.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }

    bos.rptsaldopo.openreporttotal  = function(){
        bjs_os.form_report( this.base_url + '/showreporttotal' ) ;
    }
	
	

    bos.rptsaldopo.grid2_data    = null ;
    bos.rptsaldopo.grid2_loaddata= function(){
        this.grid2_data         = {} ;
    }


    bos.rptsaldopo.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name    : this.id + '_grid2',
            show: {
                footer      : false,
                toolbar     : false,
                toolbarColumns  : false
            },
            multiSearch     : false,
            columns: [
                { field: 'no', caption: 'No', size: '30px', sortable: false, style:'text-align:center',frozen:true},
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false ,style:'text-align:center',frozen:true},
                { field: 'keterangan', caption: 'Nama Stock', size: '150px', sortable: false,frozen:true},
                { field: 'spesifikasi', caption: 'Spesifikasi', size: '200px', sortable: false },
                { field: 'satuan', caption: 'Satuan', size: '80px', sortable: false, style:'text-align:center'},
                { field: 'harga', render: 'float:2', caption: 'Harga Satuan', size: '100px', sortable: false, style:'text-align:right' },
                { field: 'qty', render: 'float:2', caption: 'Qty', size: '100px', sortable: false},
                { field: 'jumlah', render: 'float:2' , caption: 'Total', size: '90px', sortable: false,style:'text-align:right'},
                { field: 'qtysisa', render: 'float:2', caption: 'Saldo Qty', size: '100px', sortable: false},
                { field: 'nomsisa', render: 'float:2', caption: 'Saldo Nominal', size: '100px', sortable: false}                
            ]
        });
    }

    bos.rptsaldopo.grid2_reload     = function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.rptsaldopo.grid2_destroy    = function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.rptsaldopo.grid2_render     = function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.rptsaldopo.grid2_reloaddata = function(){
        this.grid2_reload() ;
    }

    $(function(){
        bos.rptsaldopo.initcomp() ;
        bos.rptsaldopo.initcallback() ;
        bos.rptsaldopo.initfunc() ;
    });
</script>