<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Laporan Hasil Produksi Karu</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rpthasilproduksikaru.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div>
<div class="body">
    <form novalidate>         
        <div class="bodyfix scrollme" style="height:100%"> 
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Antara Tanggal</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                            </div>
                            <div class="col-md-6">
                                <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Cabang</label>
                        <div class="input-group">
                            <select name="skd_cabang" id="skd_cabang" class="form-control s2" style="width:100%"
                                                    data-placeholder="Pilih cabang" data-sf="load_cabang"></select>
                            <span class="input-group-addon">
                                <span id="cmdcabang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                            </span>
                        
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group full-width">
                        <label>&nbsp;</label>
                        <div class="input-group full-width">
                            <button type="button" class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                        </div>
                    </div>
                </div>
                <!-- <div class="col-md-2">
                    <div class="form-group">
                        <label>Cetak</label>
                        <div class="input-group">
                            <select name="export" id="export" class="form-control select" style="width:100%"
                                data-sf="load_export" data-placeholder="PDF" required></select>
                            <span class="input-group-addon">
                                <span id="cmdpreview" style="cursor:pointer">Cetak <i class="fa fa-print"></i></span>
                            </span>
                        
                        </div>
                    </div>
                </div> -->
            </div>

            <div id="grid1" class="full-height" style="height: calc(100% - 50px);"></div> 
        </div>
        <div class="modal fade" style="position:absolute;" id="wrap-preview-detail-d" role="dialog" data-backdrop="false" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="wm-title">Preview Detail Hasil Produksi Karu</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>NIP Karu</label>
                                    <input type="text" name="karu" id="karu" class="form-control" placeholder="Karu" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Nama Karu</label>
                                    <input type="text" name="namakaru" id="namakaru" class="form-control" placeholder="Karu" readonly = true>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="grid2" style="height:350px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <button type = "button" class="btn btn-success" id="cmdcetaklapdetail">Print Report</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    <?=cekbosjs();?>


    //grid daftar pembelian<div class="row">
    
    bos.rpthasilproduksikaru.grid1_data    = null ;
    bos.rpthasilproduksikaru.grid1_loaddata= function(){

        mdl_cabang.vargr.selection = bos.rpthasilproduksikaru.obj.find('#skd_cabang').val();
        this.grid1_data 		= {
			"tglawal"	   : this.obj.find("#tglawal").val(),
            "tglakhir"	   : this.obj.find("#tglakhir").val(),
			'skd_cabang'   : JSON.stringify(mdl_cabang.vargr.selection)
		} ;
    }

    bos.rpthasilproduksikaru.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.rpthasilproduksikaru.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            multiSearch		: false,
            columns: [
                { field: 'regu', caption: 'Regu', size: '100px', sortable: false , style:"text-align:left"},
                { field: 'karu', caption: 'Karu', size: '100px', sortable: false , style:"text-align:left"},
                { field: 'prodbaru', caption: 'HP. Baru', size: '100px', sortable: false, render:"float:2"},
                { field: 'prodperbaikan', caption: 'HP. Perbaikan', size: '100px', sortable: false, render:"float:2"},
                { field: 'prodproses', caption: 'Prod. Proses', size: '100px', sortable: false, render:"float:2"},
                { field: 'prodselesai', caption: 'Prod. Selesai', size: '100px', sortable: false, render:"float:2"},
                { field: 'cmdpreview', caption: ' ', size: '120px', sortable: false }
            ]
        });
    }

    bos.rpthasilproduksikaru.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }

    bos.rpthasilproduksikaru.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.rpthasilproduksikaru.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rpthasilproduksikaru.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rpthasilproduksikaru.grid1_reloadData	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }


    bos.rpthasilproduksikaru.cmdpreviewdetail	= function(karu){

        bjs.ajax(this.url + '/PreviewDetail', bjs.getdataform(this.obj.find("form"))+'&karu=' + karu);
    }

    bos.rpthasilproduksikaru.cmdcetakdm 	= function(){
        if(confirm("Cetak data?")){
            var faktur = bos.rpthasilproduksikaru.obj.find("#faktur").val();
            bjs.ajax(this.url + '/cetakdm', 'faktur=' + faktur);
        }
    }

    bos.rpthasilproduksikaru.cmdrefresh          = bos.rpthasilproduksikaru.obj.find("#cmdrefresh") ;
    bos.rpthasilproduksikaru.cmdpreview     = bos.rpthasilproduksikaru.obj.find("#cmdpreview")
    bos.rpthasilproduksikaru.cmdcetaklapdetail     = bos.rpthasilproduksikaru.obj.find("#cmdcetaklapdetail") ;
    bos.rpthasilproduksikaru.initfunc    = function(){
        this.obj.find("#cmdpreview").on("click", function(e){
            e.preventDefault() ;
            bos.rpthasilproduksikaru.initreportTotal(0,0) ;
        }) ;

        this.obj.find("#cmdrefresh").on("click", function(e){
            e.preventDefault() ;
            bos.rpthasilproduksikaru.grid1_reloadData() ;
        }) ;
        this.obj.find("#cmdcetaklapdetail").on("click",function(e){
            e.preventDefault() ;
            bos.rpthasilproduksikaru.initreportdetail(0,0) ;
        });
        this.obj.find("#cmdcetakdm").on("click", function(e){
            bos.rpthasilproduksikaru.cmdcetakdm();
		});
    }

    bos.rpthasilproduksikaru.initreportdetail  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.rpthasilproduksikaru.loadmodalpreview      = function(l){
        this.obj.find("#wrap-preview-detail-d").modal(l) ;
    }

    bos.rpthasilproduksikaru.initcomp		= function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;
        this.grid2_load() ;

        bjs.initselect({
            class : "#" + this.id + " .s2",
            multi : true
        }) ;

        bjs.initselect({
            class : "#" + this.id + " .select",
            clear :false
        }) ;

        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drags
    }

    bos.rpthasilproduksikaru.initcallback	= function(){
        this.obj.on('remove', function(){
            bos.rpthasilproduksikaru.grid1_destroy() ;
            bos.rpthasilproduksikaru.grid2_destroy() ;
            bos.rpthasilproduksikaru.grid2_destroy() ;
        }) ;
    }

    bos.rpthasilproduksikaru.setPreview      = function(cFaktur){
        bos.rpthasilproduksikaru.initreport(0,0) ;
    }

    bos.rpthasilproduksikaru.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.rpthasilproduksikaru.initreportTotal  = function(s,e){
        bjs.ajax(this.base_url+ '/initreportTotal', bjs.getdataform(this.obj.find("form"))+"&skd_cabang="+JSON.stringify(mdl_cabang.vargr.selection)) ;
    }

    bos.rpthasilproduksikaru.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }

    bos.rpthasilproduksikaru.openreporttotal  = function(){
        bjs_os.form_report( this.base_url + '/showreporttotal' ) ;
    }

    bos.rpthasilproduksikaru.grid2_data    = null ;
    bos.rpthasilproduksikaru.grid2_loaddata= function(){
        this.grid2_data         = {} ;
    }


    bos.rpthasilproduksikaru.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name    : this.id + '_grid2',
            show: {
                footer      : false,
                toolbar     : false,
                toolbarColumns  : false,
                lineNumbers    : true
            },
            multiSearch     : false,
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false ,style:'text-align:center'},
                { field: 'keterangan', caption: 'Nama Stock', size: '150px', sortable: false },
                { field: 'prodselesai', render: 'float:2', caption: 'Prod. Selesai', size: '100px', sortable: false, style:'text-align:right'},
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false }
            ]
        });
    }

    bos.rpthasilproduksikaru.grid2_reload     = function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.rpthasilproduksikaru.grid2_destroy    = function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.rpthasilproduksikaru.grid2_render     = function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.rpthasilproduksikaru.grid2_reloaddata = function(){
        this.grid2_reload() ;
    }

    $(function(){
        bos.rpthasilproduksikaru.initcomp() ;
        bos.rpthasilproduksikaru.initcallback() ;
        bos.rpthasilproduksikaru.initfunc() ;
    });
</script>