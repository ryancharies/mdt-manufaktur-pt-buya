<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Laporan Kartu Piutang</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptkartupiutang.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div>
<div class="body">
    <form novalidate>         
        <div class="bodyfix scrollme" style="height:100%"> 
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>Customer</label>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="input-group">
                                    <input type="text" id="customer" name="customer" class="form-control" placeholder="Customer" readonly>
                                    <span class="input-group-btn">
                                        <button class="form-control btn btn-info" type="button" id="cmdcustomer"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <input type="text" id="namacustomer" readonly name="namacustomer" class="form-control" placeholder="Nama Customer">  
                            </div>
                        </div>                       
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Antara Tanggal</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                            </div>
                            <div class="col-md-6">
                                <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group full-width">
                        <label>Fkt (Faktur Induk)</label>
                        <input type="text" id="fkt" name="fkt" class="form-control" placeholder="No (Faktur / Bukti) Induk">  
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group full-width">
                        <label>&nbsp;</label>
                        <div class="input-group full-width">
                                <button class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Cetak</label>
                        <div class="input-group">
                            <select name="export" id="export" class="form-control select" style="width:100%"
                                data-sf="load_export" data-placeholder="PDF" required></select>
                            <span class="input-group-addon">
                                <span id="cmdview" style="cursor:pointer">Cetak <i class="fa fa-print"></i></span>
                            </span>
                        
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="row" style="height: calc(100% - 50px);"> -->
                <div id="grid1" class="full-height" style="height: calc(100% - 100px);"></div>
            <!-- </div> -->
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>


    //grid kartu
    bos.rptkartupiutang.grid1_data    = null ;
    bos.rptkartupiutang.grid1_loaddata= function(){
        this.grid1_data 		= {'tglawal':bos.rptkartupiutang.obj.find("#tglawal").val(),
                        'tglakhir':bos.rptkartupiutang.obj.find("#tglakhir").val(),
                        'customer':bos.rptkartupiutang.obj.find("#customer").val(),
                        'fkt':bos.rptkartupiutang.obj.find("#fkt").val()} ;
    }

    bos.rptkartupiutang.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            header  : "Data Kartu Piutang",
            limit 	: 100 ,
            url 	: bos.rptkartupiutang.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                header 		: true,
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false , style:"text-align:left"},
                { field: 'debet', render: 'float:2' ,caption: 'Debet', size: '100px', sortable: false, style:"text-align:right" },
                { field: 'kredit', render: 'float:2' ,caption: 'Kredit', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'saldo', render: 'float:2' ,caption: 'Saldo', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'fkt', caption: 'Fkt', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'cabang', caption: 'Cabang', size: '80px', sortable: false, style:"text-align:center"}
            ]
        });
    }

    bos.rptkartupiutang.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }

    bos.rptkartupiutang.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.rptkartupiutang.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptkartupiutang.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rptkartupiutang.grid1_reloadData	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

	bos.rptkartupiutang.obj.find("#cmdview").on("click", function(){
		bos.rptkartupiutang.initreport();
	}) ;

    bos.rptkartupiutang.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }
    bos.rptkartupiutang.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }
	
    bos.rptkartupiutang.cmdrefresh          = bos.rptkartupiutang.obj.find("#cmdrefresh") ;
    bos.rptkartupiutang.initfunc    = function(){
        this.obj.find("#cmdrefresh").on("click", function(e){
            e.preventDefault() ;
            bos.rptkartupiutang.grid1_reloadData() ;
        }) ;

        this.obj.find("#cmdcustomer").on("click", function(e){
            mdl_customer.open(function(r){
                r = JSON.parse(r);
                bos.rptkartupiutang.obj.find("#customer").val(r.kode);
                bos.rptkartupiutang.obj.find("#namacustomer").val(r.nama);
                bos.rptkartupiutang.obj.find("#tglawal").focus();
            });
        }) ;


    }
    bos.rptkartupiutang.initcomp		= function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;
        bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;
		this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.rptkartupiutang.initcallback	= function(){
        this.obj.on('remove', function(){
            bos.rptkartupiutang.grid1_destroy() ;
            bos.rptkartupiutang.grid2_destroy() ;
        }) ;
    }

    $(function(){
        bos.rptkartupiutang.initcomp() ;
        bos.rptkartupiutang.initcallback() ;
        bos.rptkartupiutang.initfunc() ;
    });
</script>