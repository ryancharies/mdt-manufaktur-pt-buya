<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="trwytabsensikaryawan">
                        <button class="btn btn-tab tpel active" href="#trwytabsensikaryawan_1" data-toggle="tab" >Daftar Karyawan</button>
                        <button class="btn btn-tab tpel" href="#trwytabsensikaryawan_2" data-toggle="tab">Riwayat Absensi</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptriwayatabsensikaryawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="trwytabsensikaryawan_1" style="padding-top:5px;">
                    <div id="grid1" class="full-height"></div>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="trwytabsensikaryawan_2">
                    <table width = "100%">
                        <tr>
                            <td width = "100%" valign ="top">
                                <table width = "100%">
                                    <tr>
                                        <td width = '100%' class="osxtable form">
                                            <table width = '100%'>
                                                <tr>
                                                    <td width="75px"><label for="kode">Kode</label> </td>
                                                    <td width="5px">:</td>
                                                    <td >
                                                        <input readonly type="text" id="kode" name="kode" class="form-control" placeholder="Kode" required>
                                                    </td>
                                                <!-- </tr>
                                                <tr> -->
                                                    <td width="75px"><label for="nama">Nama</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama" required>
                                                    </td>
                                                <!-- </tr>
                                                <tr> -->
                                                    <td width="75px"><label for="ktp">No KTP</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input type="text" id="ktp" name="ktp" class="form-control" placeholder="No KTP" required>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="75px"><label for="notelepon">NoTelp</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input type="text" id="notelepon" name="notelepon" class="form-control" placeholder="notelepon" required>
                                                    </td>
                                                <!-- </tr>
                                                <tr> -->
                                                    <td width="75px"><label for="alamat">Alamat</label> </td>
                                                    <td width="5px">:</td>
                                                    <td>
                                                        <input type="text" id="alamat" name="alamat" class="form-control" placeholder="alamat" required>
                                                    </td>
                                                <!-- </tr>
                                                <tr> -->
                                                    <td width="75px"><label for="tglmasuk">Tgl Masuk</label> </td>
                                                    <td width="5px">:</td>
                                                    <td >
                                                        <input style="width:80px" type="text" class="form-control date" id="tglmasuk" name="tglmasuk" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <hr/>
                                        </td>
                                    </tr>
                                <!-- </table>
                            </td>
                            <td valign ="top">
                                <table class="osxtable form"> -->
                                    <tr>
                                        <td class="osxtable form">
                                            <table>
                                                <tr>
                                                    <td width="100px"><label for="periode">Periode</label> </td>
                                                    <td width="5px">:</td>
                                                    <td width="400px">
                                                        <select name="periode" id="periode" class="form-control select" style="width:100%" data-placeholder="Periode"></select>
                                                    </td>
                                                    <td width="85px">
                                                        <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                                    </td>
                                                    <td width="5px">sd</td>
                                                    <td width="85px">
                                                        <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                                    </td>
                                                    <td>
                                                        <button type = "button" class="btn btn-primary" id="cmdpreview">Preview</button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height = "450px">
                                            <div id="grid2" class="full-height"></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.rptriwayatabsensikaryawan.grid1_data 	 = null ;
    bos.rptriwayatabsensikaryawan.grid1_loaddata= function(){
        this.grid1_data 		= {} ;
    }

    bos.rptriwayatabsensikaryawan.grid1_load    = function(){ 
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.rptriwayatabsensikaryawan.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false, 
            columns: [
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false,frozen:true,style:"text-align:center;"},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false,frozen:true},
                { field: 'ktp', caption: 'No KTP', size: '100px', sortable: false,frozen:true},
                { field: 'alamat', caption: 'Alamat', size: '200px', sortable: false},
                { field: 'telepon', caption: 'Telepon', size: '100px', sortable: false},
                { field: 'tgl', caption: 'Tgl Masuk', size: '100px', sortable: false},
                { field: 'golabsensi', caption: 'Gol. Absensi', size: '100px', sortable: false},
                { field: 'cmdkaryawan', caption: ' ', size: '100px', sortable: false },
            ]
        });
    }

    bos.rptriwayatabsensikaryawan.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.rptriwayatabsensikaryawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.rptriwayatabsensikaryawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptriwayatabsensikaryawan.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rptriwayatabsensikaryawan.grid1_reloadData	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //detail mutasi golongan grid2 
    bos.rptriwayatabsensikaryawan.grid2_data 	 = null ;

    bos.rptriwayatabsensikaryawan.grid2_load    = function(){ 
        this.obj.find("#grid2").w2grid({
            name	 : this.id + '_grid2',
            show 		: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'no', caption: 'No', size: '40px', sortable: false,style:"text-align:right;"},
                { field: 'hari', caption: 'Hari', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'jadwalmasuk', caption: 'JW. Masuk', size: '80px', sortable: false},
                { field: 'jadwalpulang', caption: 'JW. Pulang', size: '80px', sortable: false},
                { field: 'toleransi', caption: 'Toleransi (Mnt)', size: '100px', sortable: false},
                { field: 'absmasuk', caption: 'Abs. Masuk', size: '80px', sortable: false},
                { field: 'kodeabsensim', caption: 'Kode Abs Masuk', size: '150px', sortable: false, style:"text-align:center"},
                { field: 'terlambat', caption: 'Terlambat', size: '80px', sortable: false},
                { field: 'abspulang', caption: 'Abs. Pulang', size: '80px', sortable: false},
                { field: 'kodeabsensip', caption: 'Kode Abs Pulang', size: '150px', sortable: false, style:"text-align:center"},
                { field: 'plgcepat', caption: 'Plg Cepat', size: '80px', sortable: false},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false},
                { field: 'golabsensi', caption: 'Gol. Absensi', size: '150px', sortable: false},
                { field: 'jadwal', caption: 'Jadwal', size: '150px', sortable: false},
                { field: 'lembur', caption: 'Lembur', size: '150px', sortable: false},
            ]
        });
    }

    bos.rptriwayatabsensikaryawan.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.rptriwayatabsensikaryawan.grid2_render 	= function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.rptriwayatabsensikaryawan.grid2_reloaddata	= function(){
        // this.grid2_loaddata() ;
        // this.grid2_setdata() ;
        // this.grid2_reload() ;
        w2ui[this.id + '_grid2'].clear();
        var periode = this.obj.find("#periode").val();
        var nip = this.obj.find("#kode").val();
        bjs.ajax(this.url + '/loadabsensi', 'periode=' + periode + '&nip=' + nip);
    }

    bos.rptriwayatabsensikaryawan.cmdkaryawan		= function(id){
        bjs.ajax(this.url + '/karyawan', 'kode=' + id);
    }

    bos.rptriwayatabsensikaryawan.init				= function(){
        this.obj.find("#kode").val("") ;
        this.obj.find("#nama").val("") ;
        this.obj.find("#notelepon").val("") ;
        this.obj.find("#ktp").val("") ;
        this.obj.find("#alamat").val("") ;
        this.obj.find("#golongan").sval("") ;
        w2ui[this.id + '_grid2'].clear();
    }

    bos.rptriwayatabsensikaryawan.settab 		= function(n){
        this.obj.find("#trwytabsensikaryawan button:eq("+n+")").tab("show") ;
    }

    bos.rptriwayatabsensikaryawan.tabsaction	= function(n){
        if(n == 0){
            this.obj.find(".bodyfix").css("height","100%") ;
            this.obj.find(".footer").addClass("hidden") ;
            bos.rptriwayatabsensikaryawan.grid1_render() ;
            bos.rptriwayatabsensikaryawan.init() ;
        }else{
            bos.rptriwayatabsensikaryawan.grid2_reloaddata() ;
            this.obj.find(".bodyfix").css("height","calc(100% - 32px)") ;
            this.obj.find(".footer").removeClass("hidden") ;
            this.obj.find("#tgl").focus() ;
        }
    }

    bos.rptriwayatabsensikaryawan.initcomp	= function(){
        /*bjs.initselect({
            class : "#" + this.id + " .select2",
            clear : true
        }) ;*/
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;      
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        // this.grid2_loaddata() ;
        this.grid2_load() ;

        this.obj.find('#kodeabsensim').select2({
            ajax: {
                url: bos.rptriwayatabsensikaryawan.base_url + '/seekkodeabsensim',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });
        this.obj.find('#kodeabsensip').select2({
            ajax: {
                url: bos.rptriwayatabsensikaryawan.base_url + '/seekkodeabsensip',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.obj.find('#periode').select2({
            ajax: {
                url: bos.rptriwayatabsensikaryawan.base_url + '/seekperiode',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });


    }

    bos.rptriwayatabsensikaryawan.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.rptriwayatabsensikaryawan.tabsaction( e.i )  ;
        });

        this.obj.on("remove",function(){
            bos.rptriwayatabsensikaryawan.grid1_destroy() ;
            bos.rptriwayatabsensikaryawan.grid2_destroy() ;
        })
         ;
    }

    bos.rptriwayatabsensikaryawan.objs = bos.rptriwayatabsensikaryawan.obj.find("#cmdsave") ;
    bos.rptriwayatabsensikaryawan.initfunc 		= function(){
        // this.obj.find("form").on("submit", function(e){
        //     e.preventDefault() ;
        //     if(bjs.isvalidform(this)){
        //         bjs.ajax( bos.rptriwayatabsensikaryawan.url + '/saving', bjs.getdataform(this), bos.rptriwayatabsensikaryawan.objs) ;
        //     }
        // });
        this.obj.find("#periode").on("change",function(){
            
            // w2ui['bos-form-rptriwayatabsensikaryawan_grid2'].clear();
            // var periode = bos.rptriwayatabsensikaryawan.obj.find("#periode").val();
            // var nip = bos.rptriwayatabsensikaryawan.obj.find("#kode").val();
            // bjs.ajax(bos.rptriwayatabsensikaryawan.base_url + '/loadabsensi', 'periode=' + periode + '&nip=' + nip);
            bos.rptriwayatabsensikaryawan.grid2_reloaddata();
        });    

        this.obj.find("#cmdpreview").on("click",function(){
            var periode = bos.rptriwayatabsensikaryawan.obj.find("#periode").val();
            var nip = bos.rptriwayatabsensikaryawan.obj.find("#kode").val();
            bjs_os.form_report(bos.rptriwayatabsensikaryawan.base_url + '/preview?'+ bjs.getdataform(bos.rptriwayatabsensikaryawan.obj.find("form")));
        }); 
    }
    

    $(function(){
        bos.rptriwayatabsensikaryawan.initcomp() ;
        bos.rptriwayatabsensikaryawan.initcallback() ;
        bos.rptriwayatabsensikaryawan.initfunc() ;
    }) ;
</script>
