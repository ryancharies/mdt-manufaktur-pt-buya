<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon" ><i class="fa fa-building"></i></td>
            <td class="title">
                <div class="nav ">
                    <div class="btn-group" id="tpjprod">
                        <button class="btn btn-tab tpel active" href="#tpjprod_1" data-toggle="tab" >Produk</button>
                        <button class="btn btn-tab tpel" href="#tpjprod_2" data-toggle="tab" >Bulan</button>
                        <button class="btn btn-tab tpel" href="#tpjprod_3" data-toggle="tab">Grafik Penjualan</button>
                    </div>
                </div>
            </td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptpenjualanproduk.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div><!-- end header -->
<div class="body">
    
        <div class="bodyfix scrollme" style="height:100%">
            <div class="tab-content full-height">
                <div role="tabpanel" class="tab-pane active full-height" id="tpjprod_1" style="padding-top:5px;">
                    <table width="100%">
                        <tr>
                            <td height="25px">
                            <form id="pjproduk" novalidate>
                                <table class="osxtable form">
                                    <tr>
                                        <td width="100px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td width="20px">sd</td>
                                        <td width="100px">
                                            <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                        </td>
                                        <td >
                                            <select name="jenis" id="jenis" class="form-control select" style="width:100%"
                                                data-placeholder="Jenis" required>
                                                    <option value="customer">Customer</option>
                                                    <option value="dati_2">Wilayah</option>
                                            </select>
                                        </td>
                                        <td>
                                                <select name="customer_gol" id="customer_gol" class="form-control scons" style="width:100%"
                                                    data-sf="load_customer_gol" data-placeholder="Golongan Customer" required></select>
                                            </td>
                                        <td width="100px">
                                            <button type="button"  class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                        </td>
                                        <td width="100px">
                                            <select name="export" id="export" class="form-control scons" style="width:100%"
                                                data-sf="load_export" data-placeholder="PDF" required></select>
                                        </td>
                                        <td width="100px">
                                            <button type="button" class="btn btn-primary pull-right" id="cmdview">Export</button> 
                                        </td>
                                    </tr>

                                </table>
                            </form>
                            </td>
                        </tr>
                        <tr>
                            <td height="700px">
                                <div id="grid1" class="full-height"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpjprod_2">
                    <table width = "100%" >
                        <tr>
                            <td height="25px">
                                <form id="pjprodukbln" novalidate>
                                    <table class="osxtable form">
                                        <tr>
                                            <td width="85px">
                                                <input style="width:100px" type="text" class="form-control date" id="periodeawal" name="periodeawal" required value=<?=date("m-Y")?> <?=date_periodset(true)?>>
                                            </td>
                                            <td width="20px">sd</td>
                                            <td width="85px">
                                                <input style="width:100px" type="text" class="form-control date" id="periodeakhir" name="periodeakhir" required value=<?=date("m-Y")?> <?=date_periodset(true)?>>
                                            </td>
                                            <td width="100px">
                                                <select name="stock_kelompok" id="stock_kelompok" class="form-control scons" style="width:100%"
                                                    data-sf="load_klmstock" data-placeholder="Kelompok Stock" required></select>
                                            </td>
                                            <td>
                                                <select name="customer_gol" id="customer_gol" class="form-control scons" style="width:100%"
                                                    data-sf="load_customer_gol" data-placeholder="Golongan Customer" required></select>
                                            </td>
                                            <td width="100px">
                                                <button type="button"  class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                            </td>
                                            <td width="100px">
                                                <select name="export" id="export" class="form-control scons" style="width:100%"
                                                    data-sf="load_export" data-placeholder="PDF" required></select>
                                            </td>
                                            <td width="100px">
                                                <button type="button" class="btn btn-primary pull-right" id="cmdview">Export</button> 
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td height="700px">
                                <div id="grid2" class="full-height"></div>
                            </td>
                        </tr>
                    </table>

                </div>
                <div role="tabpanel" class="tab-pane fade full-height" id="tpjprod_3">
                    <table width = "100%" >
                        <tr>
                            <td height="25px">
                                <form id="pjprodukblngrafik" novalidate>
                                    <table class="osxtable form">
                                        <tr>
                                            <td width="85px">
                                                <input style="width:100px" type="text" class="form-control date" id="periodeawal" name="periodeawal" required value=<?=date("m-Y")?> <?=date_periodset(true)?>>
                                            </td>
                                            <td width="20px">sd</td>
                                            <td width="85px">
                                                <input style="width:100px" type="text" class="form-control date" id="periodeakhir" name="periodeakhir" required value=<?=date("m-Y")?> <?=date_periodset(true)?>>
                                            </td>
                                            <td>
                                                <select name="customer_gol" id="customer_gol" class="form-control scons" style="width:100%"
                                                    data-sf="load_customer_gol" data-placeholder="Golongan Customer" required></select>
                                            </td>
                                            <td width="100px">
                                                <button type="button"  class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td height="700px">
                                <table style="height:100%;width:100%">
                                    <tr>
                                        <td>
                                        <div class="box-header with-border">
                                            <h3 class="box-title" >Grafik Penjualan Bulanan</h3>
                                        </div>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td heig>
                                            
                                            <div class="box-body chart-responsive">
                                                <canvas id="chartgrfkpjbulan"></canvas>
                                            </div>
                                                        <!-- /.box-body -->
                                                    
                                        </td>
                                    </tr>                                
                                </table>
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    /* grid1 */
    bos.rptpenjualanproduk.grid1_data 	 = null ;
    bos.rptpenjualanproduk.grid1_loaddata= function(){
        this.grid1_data 		= {
            "tglawal"	   : this.obj.find("#pjproduk #tglawal").val(),
            "tglakhir"	   : this.obj.find("#pjproduk #tglakhir").val(),
            "jenis"	   : this.obj.find("#pjproduk #jenis").val(),
            "customer_gol"	   : this.obj.find("#pjproduk #customer_gol").val()
        } ;
    }

    bos.rptpenjualanproduk.grid1_load    = function(){        
        bjs.ajax(bos.rptpenjualanproduk.base_url + '/initgrid1',bjs.getdataform(this.obj.find("#pjproduk")),'',function(data){
            // console.log(data);  
            data = JSON.parse(data);
            bos.rptpenjualanproduk.obj.find("#grid1").w2grid({
                name		: bos.rptpenjualanproduk.id + '_grid1',
                url 		: bos.rptpenjualanproduk.base_url + "/loadgrid1",
                postData : bos.rptpenjualanproduk.grid1_data ,
                header : 'Daftar Penjualan Produk Per '+data.judul,
                show 		: {
                    footer 		: true,
                    header 		: true,
                    toolbar		: false,
                    toolbarColumns  : false,
                    lineNumbers : true
                },
                multiSearch		: false,                 
                columnGroups: data.kolomgroup,
                columns: data.kolom
            });
        });            
        
        
    }

    bos.rptpenjualanproduk.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.rptpenjualanproduk.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.rptpenjualanproduk.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptpenjualanproduk.grid1_initcolumn 	= function(){
        w2ui[this.id + '_grid1'].destroy() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;
    }

    bos.rptpenjualanproduk.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rptpenjualanproduk.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }
    /*end grid1*/
    
    bos.rptpenjualanproduk.grid2_data 	 = null ;
    bos.rptpenjualanproduk.grid2_loaddata= function(){
        this.grid2_data 		= {
            "periodeawal"	   : this.obj.find("#pjprodukbln #periodeawal").val(),
            "periodeakhir"	   : this.obj.find("#pjprodukbln #periodeakhir").val(),
            "stock_kelompok"   : this.obj.find("#pjprodukbln #stock_kelompok").val(),
            "customer_gol"	   : this.obj.find("#pjprodukbln #customer_gol").val()
        } ;
    }

    bos.rptpenjualanproduk.grid2_load    = function(){
        bos.rptpenjualanproduk.judul = 'Daftar Penjualan '+ this.obj.find("#pjprodukbln #stock_kelompok").text() +' '+ this.obj.find("#pjprodukbln #customer_gol").text();
        bjs.ajax(bos.rptpenjualanproduk.base_url + '/initgrid2',bjs.getdataform(this.obj.find("#pjprodukbln")),'',function(data){
            //  console.log(data);  
            data = JSON.parse(data);
            bos.rptpenjualanproduk.obj.find("#grid2").w2grid({
                name		: bos.rptpenjualanproduk.id + '_grid2',
                url 		: bos.rptpenjualanproduk.base_url + "/loadgrid2",
                postData : bos.rptpenjualanproduk.grid2_data ,
                header : bos.rptpenjualanproduk.judul,
                show 		: {
                    footer 		: true,
                    header 		: true,
                    toolbar		: false,
                    toolbarColumns  : false,
                    lineNumbers : true
                },
                multiSearch		: false,                 
                columnGroups: data.kolomgroup,
                columns: data.kolom
            });
        });        
    }

    bos.rptpenjualanproduk.grid2_setdata	= function(){
        w2ui[this.id + '_grid2'].postData 	= this.grid2_data ;
    }
    bos.rptpenjualanproduk.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.rptpenjualanproduk.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.rptpenjualanproduk.grid2_initcolumn 	= function(){
        w2ui[this.id + '_grid2'].destroy() ;
        this.grid2_loaddata() ;
        this.grid2_load() ;
    }

    bos.rptpenjualanproduk.grid2_render 	= function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.rptpenjualanproduk.grid2_reloaddata	= function(){
        this.grid2_loaddata() ;
        this.grid2_setdata() ;
        this.grid2_reload() ;
    }

    bos.rptpenjualanproduk.settab 		= function(n){
        this.obj.find("#tpjprod button:eq("+n+")").tab("show") ;
    }

    bos.rptpenjualanproduk.tabsaction	= function(n){
        this.obj.find(".bodyfix").css("height","100%") ;
        if(n == 0){
            bos.rptpenjualanproduk.grid1_render() ;
        }else if(n == 1){
            bos.rptpenjualanproduk.grid2_render() ;
        }else{
            // bos.rptpenjualanproduk.grid3_render() ;
        }
    }

    bos.rptpenjualanproduk.initreport1  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport1', bjs.getdataform(this.obj.find("#pjproduk"))) ;
    }

    bos.rptpenjualanproduk.openreport1  = function(){

        bjs_os.form_report( this.base_url + '/showreport1' ) ;
    }

    bos.rptpenjualanproduk.initreport2  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport2', bjs.getdataform(this.obj.find("#pjprodukbln"))) ;
    }
    
    bos.rptpenjualanproduk.openreport2  = function(){

        bjs_os.form_report( this.base_url + '/showreport2' ) ;
    }

    bos.rptpenjualanproduk.cmdrefrshgrafik                        = bos.rptpenjualanproduk.obj.find("#pjprodukblngrafik #cmdrefresh") ;

    bos.rptpenjualanproduk.reloadgrafik      = function(){
        bjs.ajax(this.url + '/reloadgrafik', bjs.getdataform(this.obj.find("#pjprodukblngrafik")),bos.rptpenjualanproduk.cmdrefrshgrafik,function(resp){
            resp = JSON.parse(resp);
            var labels = resp.labels;
            var ctx = document.getElementById("chartgrfkpjbulan").getContext("2d");
            var myChart = new Chart(ctx, {
                type: "bar",
                data: {
                    labels: labels,                
                    datasets: resp.datas
                    
                },
                options: {
                    showTooltips: true,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true,
                                userCallback: function(value, index, values) {
                                    // Convert the number to a string and splite the string every 3 charaters from the end
                                    value = value.toString();
                                    value = value.split(/(?=(?:...)*$)/);
                        
                                    // Convert the array to a string and format the output
                                    value = value.join(".");
                                    return value;
                                }
                            },
                            scaleLabel: {
                                display: true,
                                labelString: "Qty"
                            }
                        }],
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: "Periode"
                            }
                        }]
                    },
                    title: {
                        display: true,
                        text:"Progress Penjualan " + resp.judul,
                        fontSize:20
                    },
                    showTooltips: true,

                    onAnimationComplete: function() {
                        this.showTooltip(this.datasets[0].points, true);
                    },          
                    tooltips: {
                        mode: "index",
                        podition: "nearest",
                        callbacks: {
                            label: function(tooltipItem, data) {
                                var nominal = tooltipItem.yLabel.toString();
                                nominal = nominal.split(/(?=(?:...)*$)/);
                        
                                    // Convert the array to a string and format the output
                                nominal = nominal.join(".");
                                
                                var isi = data.datasets[tooltipItem.datasetIndex].label + " : Rp. " + nominal;
                                return isi ;
                            }
                        }
                    },
                    animation: {
                        onComplete: function() {
                            const chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                            // ctx.rotate(-Math.PI / 4);
                            ctx.font = Chart.helpers.fontString(
                            10,
                            Chart.defaults.global.defaultFontStyle,
                            Chart.defaults.global.defaultFontFamily
                            );
                            ctx.textAlign = "center";
                            ctx.textBaseline = "bottom";
        
                            this.data.datasets.forEach(function(dataset, i) {
                            const meta = chartInstance.controller.getDatasetMeta(i);
                            
                            meta.data.forEach(function(bar, index) {
                                const data = dataset.data[index];
    
                                var nominal = data.toString();
                                nominal = nominal.split(/(?=(?:...)*$)/);
                        
                                    // Convert the array to a string and format the output
                                nominal = nominal.join(".");
    
                                ctx.fillStyle = "#000";
                                ctx.fillText(nominal, bar._model.x-20, bar._model.y - 2);
                            });
                            });
                        }
                    }
                }
            });
        }) ;
    }  

    bos.rptpenjualanproduk.initcomp		= function(){
        this.obj.find('.select').select2();

        bjs.initselect({
            class : "#" + this.id + " .scons"
        }) ;

        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_loaddata() ;
        this.grid2_load() ;

    }

    bos.rptpenjualanproduk.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.rptpenjualanproduk.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.rptpenjualanproduk.grid1_destroy() ;
            bos.rptpenjualanproduk.grid2_destroy() ;
        }) ;
    }

    bos.rptpenjualanproduk.objs = bos.rptpenjualanproduk.obj.find("#cmdsave") ;
    bos.rptpenjualanproduk.initfunc	   = function(){
        // console.log(this.id);
        // this.obj.find('#pjproduk').on("submit", function(e){
        //     bos.rptpenjualanproduk.grid1_initcolumn();
        //     e.preventDefault() ;

        // }) ;

        this.obj.find("#pjproduk #cmdrefresh").on("click", function(e){
            bos.rptpenjualanproduk.grid1_initcolumn();
        });

        this.obj.find("#pjproduk #cmdview").on("click", function(e){
            bos.rptpenjualanproduk.initreport1();
        });


        this.obj.find("#pjprodukbln #cmdrefresh").on("click", function(e){
            //alert("daadaj");
            bos.rptpenjualanproduk.grid2_initcolumn();
        });

        this.obj.find("#pjprodukbln #cmdview").on("click", function(e){
            bos.rptpenjualanproduk.initreport2();
        });

        this.obj.find("#pjprodukblngrafik #cmdrefresh").on("click", function(e){
            bos.rptpenjualanproduk.reloadgrafik() ;
        }) ;
    }

    $(function(){
        bos.rptpenjualanproduk.initcomp() ;
        bos.rptpenjualanproduk.initcallback() ;
        bos.rptpenjualanproduk.initfunc() ;
    });
</script>
