<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Laporan THR</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptthr.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <table width = '100%'>
                <tr>
                    <td>
                        <table class="osxtable form">
                            <tr>
                                <td width="100px"><label for="tgl">Tgl</label> </td>
                                <td width="5px">:</td>
                                <td width="85px">
                                    <input style="width:80px" type="text" class="form-control datetr" id="tgl" name="tgl" value=<?=date("d-m-Y")?> <?=date_set()?><?=$mintgl?>>
                                </td>
                                <td width="85px">
                                    <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                </td>
                                
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="osxtable form">
                            <tr>
                                <td width="100px"><label for="faktur">Faktur</label> </td>
                                <td width="5px">:</td>
                                <td width="200px">
                                    <input readonly placeholder="Faktur"  type="text" class="form-control" id="faktur" name="faktur">
                                </td>
                                <td width="300px">
                                    <input readonly placeholder="Status Posting"  type="text" class="form-control" id="status" name="status">
                                </td>
                                <td width="85px">
                                    <button type="button" class="btn btn-primary pull-right" id="cmdreport">Report</button>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <table width = '100%' height = "750px">
                            <tr>
                                <td >
                                    <div id="grid1" class="full-height">Pilih tanggal dan klik refresh!!!</div>
                                </td>
                                <td width = '3px'></td>
                                <td width = "400px" valign = "top">
                                    <table width = "400px">
                                        <tr>
                                            <td height = "735px" >
                                                Detail Pembayaran
                                                <div id="grid2" class="full-height"></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                <tr>
            </table>               
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.rptthr.grid1_data    = null ;
    bos.rptthr.grid1_loaddata= function(){

        var tgl = bos.rptthr.obj.find("#tgl").val();
        this.grid1_data 		= {'tgl':tgl} ;
    }

    bos.rptthr.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            recordHeight : 30,
            limit 	: 100 ,
            url 	: bos.rptthr.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true,
                multiSelect:true
            },
            columns: <?= $kolomgrid1 ; ?> 
        });


    }


    bos.rptthr.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.rptthr.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.rptthr.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptthr.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    

    bos.rptthr.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.rptthr.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'no', caption: 'No', size: '50px', sortable: false},
                { field: 'kode', caption: 'Kode', size: '120px', sortable: false },
                { field: 'ketrekening', caption: 'Ket. Rekening', size: '120px', sortable: false },
                { field: 'nominal', caption: 'Nominal', size: '100px', sortable: false, render:'float:2'},
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ],
            summary: [
                { recid: "ZZZZ", no: '', kode: '', ketrekening: 'Total', nominal: 0 }
            ]
        });


    }

    bos.rptthr.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.rptthr.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

   
    bos.rptthr.init				= function(){
        bjs.ajax(this.url + "/init") ;
        bos.rptthr.initdetail();
    }

    bos.rptthr.initdetail 			= function(){
        var datagrid = w2ui[this.id + '_grid2'].records;

        this.obj.find("#nomor").val(datagrid.length+1) ;
        this.obj.find("#rekening").sval("") ;
        this.obj.find("#nominal").val("0") ;



    }
    
    bos.rptthr.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    

    bos.rptthr.initcomp	= function(){
         bjs.initselect({
		 	class : "#" + this.id + " .selectauto"
		 }) ;
        bjs.initdatetr("#" + this.id + " .datetr") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

        this.grid2_load();
    }

    bos.rptthr.initcallback	= function(){


        this.obj.on("remove",function(){
            bos.rptthr.grid1_destroy() ;
            bos.rptthr.grid2_destroy() ;
                

        }) ;
    }

    bos.rptthr.loadperiode	= function(){
        var tgl = this.obj.find("#tgl").val();
        bjs.ajax(this.url + '/loadperiode', 'tgl=' + tgl);
    }

    bos.rptthr.loadpembayaran	= function(){
        w2ui['bos-form-rptthr_grid2'].clear();
        var tgl = this.obj.find("#tgl").val();
        bjs.ajax(this.url + '/loadpembayaran', 'tgl=' + tgl);
    }

    bos.rptthr.obj.find("#cmdreport").on("click", function(){
        bos.rptthr.initreport();
	}) ;

    bos.rptthr.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }
    bos.rptthr.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }

    bos.rptthr.initfunc 		= function(){
        this.init() ;
       

        this.obj.find("#cmdrefresh").on("click",function(){
            // bos.rptthr.grid1_loaddata();
            bos.rptthr.loadpembayaran();
            bos.rptthr.grid1_destroy() ;
            bos.rptthr.grid1_loaddata();
            bos.rptthr.grid1_load() ;
            
        });

        this.obj.find("#tgl").on("change",function(){
            
            bos.rptthr.loadperiode();
        });
    }

    $('.rekselect').select2({
        ajax: {
            url: bos.rptthr.base_url + '/seekrekening',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    $(function(){
        bos.rptthr.initcomp() ;
        bos.rptthr.initcallback() ;
        bos.rptthr.initfunc() ;
    }) ;
</script>
