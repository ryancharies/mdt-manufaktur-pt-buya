<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Payroll</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptkpikaryawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <table width = '100%'>
                <tr>
                    <td>
                        <table class="osxtable form">
                            <tr>
                                <td width="100px"><label for="tgl">Antara Tgl</label> </td>
                                <td width="5px">:</td>
                                <td width="85px">
                                    <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                </td>
                                <td width="5px">sd</td>
                                <td width="85px">
                                    <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                </td>
                                <td width="85px">
                                    <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                </td>
                                <td width="85px">
                                    <button type="button" class="btn btn-success pull-right" id="cmdcetak">Cetak</button>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width = '100%' height = "750px">
                            <tr>
                                <td >
                                    <div id="grid1" class="full-height"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                <tr>
            </table>               
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.rptkpikaryawan.grid1_data    = null ;
    bos.rptkpikaryawan.grid1_loaddata= function(){
        var tglawal = bos.rptkpikaryawan.obj.find("#tglawal").val();
        var tglakhir = bos.rptkpikaryawan.obj.find("#tglakhir").val();
        this.grid1_data 		= {'tglawal':tglawal,'tglakhir':tglakhir} ;
    }

    bos.rptkpikaryawan.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            recordHeight : 30,
            limit 	: 100 ,
            url 	: bos.rptkpikaryawan.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true,
                selectColumn: true,
                multiSelect:true
            },
            columns: [
                { field: 'kode', caption: 'NIP', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false},
                { field: 'bagian', caption: 'Bagian', size: '100px', sortable: false},
                { field: 'jabatan', caption: 'Jabatan', size: '100px', sortable: false},
                { field: 'cetak', caption: 'Cetak', size: '100px', sortable: false}
            ],
            onSelect: function(event) {
                event.onComplete = function () {
                    this.set(event.recid,{cetak:"Ya"});
                   // w2ui[event.target].save();
                }
                //this.set(event.recid,{cetakslip:"Cetak Slip"});
            },
            onUnselect: function(event) {
                event.onComplete = function () {
                    this.set(event.recid,{cetak:"Tidak"});
                    //w2ui[event.target].save();
                
                }
                //this.set(event.recid,{cetakslip:"Tidak Cetak Slip"});
            },  
        });


    }


    bos.rptkpikaryawan.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.rptkpikaryawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.rptkpikaryawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptkpikaryawan.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }
    

    bos.rptkpikaryawan.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }
    

    bos.rptkpikaryawan.initcomp	= function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;
        
        bjs.initselect({
		    class : "#" + this.id + " .selectauto"
		}) ;
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
        
        
    }

    bos.rptkpikaryawan.initcallback	= function(){


        this.obj.on("remove",function(){
            bos.rptkpikaryawan.grid1_destroy() ;
                

        }) ;
    }

    bos.rptkpikaryawan.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }
    bos.rptkpikaryawan.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }

    bos.rptkpikaryawan.cmdcetak = bos.rptkpikaryawan.obj.find("#cmdcetak") ;
    bos.rptkpikaryawan.initreportkpi  = function(){
       // bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
        var datagrid = w2ui[this.id + '_grid1'].records;
        datagrid = JSON.stringify(datagrid);
        bjs.ajax(this.base_url  + '/printkpikaryawan', bjs.getdataform(this.obj.find("form"))+"&grid1="+datagrid,bos.rptkpikaryawan.cmdcetak);

        //bjs_os.form_report( this.base_url + '/printkpikaryawan' , bjs.getdataform(this.obj.find("form"))+"&grid1="+datagrid,bos.rptkpikaryawan.cmdcetak) ;
    }

    bos.rptkpikaryawan.openreportkpi  = function(data){
        bjs_os.form_report( this.base_url + '/showreporkpikaryawan?data='+data+"&"+bjs.getdataform(this.obj.find("form")) ) ;
    }

    bos.rptkpikaryawan.obj.find("#cmdcetak").on("click", function(){
        if(confirm("Apakah KPI akan di cetak?, KPI akan dicetak untuk data yang dipilih...")){
            bos.rptkpikaryawan.initreportkpi();
        }
	}) ;

    bos.rptkpikaryawan.showReport = function(urlfile){
        bjs_os.form_report(urlfile) ;
    }

    bos.rptkpikaryawan.initfunc 		= function(){
        this.obj.find("#cmdrefresh").on("click",function(){
            // bos.rptkpikaryawan.grid1_loaddata();
            bos.rptkpikaryawan.grid1_loaddata();
            bos.rptkpikaryawan.grid1_load() ;
            
        });

    }

    $(function(){
        bos.rptkpikaryawan.initcomp() ;
        bos.rptkpikaryawan.initcallback() ;
        bos.rptkpikaryawan.initfunc() ;
    }) ;
</script>
