<style media="screen">
   #bos-form-rptbukubesar-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
   #bos-form-rptbukubesar-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<form novalidate>         
<div class="bodyfix scrollme" style="height:100%"> 
    <table class="osxtable form" border="0">
		<tr>
			<td width="80px"><label for="tgl">Tgl</label> </td>
			<td width="20px">:</td>
			<td width="100px">
				<input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
			</td>
			<td width="40px">s/d</td>
			<td width="100px">
				<input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
			</td>   
			<td>
				<button type = "button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
			</td>
			<td width="100px">	
				<button type = "button" class="btn btn-primary pull-right" id="cmdview">Preview</button>
			</td>
		</tr> 
		
      <tr><td colspan="6"><hr class="no-margin no-padding"></td></tr>
   </table> 
   <div class="row" style="height: calc(100% - 50px);"> 
      <div class="col-sm-12 full-height">
         <div id="grid1" class="full-height"></div>
      </div> 
   </div> 
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.rptrealisasikas.grid1_data 	 = null ;
    bos.rptrealisasikas.grid1_loaddata= function(){
        this.grid1_data 		= {
            "tglawal"	   : this.obj.find("#tglawal").val(),
            "tglakhir"	   : this.obj.find("#tglakhir").val(),
			"stock"	   : this.obj.find("#stock").val()
        } ;
    }

    bos.rptrealisasikas.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.rptrealisasikas.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'no', caption: 'No', size: '50px', sortable: false,style:'text-align:right'},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false,style:'text-align:center'},
                { field: 'keterangan', caption: 'Keterangan', size: '450px', sortable: false},
                { field: 'penerimaan', caption: 'Penerimaan', size: '120px',render:'float:2',style:'text-align:right', sortable: false},
                { field: 'pengeluaran', caption: 'Pengeluaran', size: '120px',render:'float:2',style:'text-align:right', sortable: false},
                { field: 'jumlah', caption: 'Jumlah', size: '120px',render:'float:2',style:'text-align:right', sortable: false}
            ]
        });
    }

    bos.rptrealisasikas.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.rptrealisasikas.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.rptrealisasikas.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptrealisasikas.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rptrealisasikas.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.rptrealisasikas.obj.find("#cmdview").on("click", function(){
        bos.rptrealisasikas.initreport();
	}) ;
    bos.rptrealisasikas.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }
    
    bos.rptrealisasikas.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }


    bos.rptrealisasikas.initcomp		= function(){
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag


    }

    bos.rptrealisasikas.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.rptrealisasikas.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.rptrealisasikas.grid1_destroy() ;
        }) ;
    }

    bos.rptrealisasikas.initfunc	   = function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;
        


        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.rptrealisasikas.grid1_reloaddata();
        });
    }

    $('#stock').select2({
        ajax: {
            url: bos.rptrealisasikas.base_url + '/seekstock',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    $(function(){
        bos.rptrealisasikas.initcomp() ;
        bos.rptrealisasikas.initcallback() ;
        bos.rptrealisasikas.initfunc() ;
    });
</script>