<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 

<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Grafik Penjualan</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptpenjualangrafik.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div>
<div class="body">
    <form novalidate>         
        <div class="bodyfix scrollme" style="height:100%;width:100%"> 
            <table class="osxtable form" border="0" style="width:100%">
                <tr>
                    <td width="100%" height="20px">
                        <table width="100%" height="20px">
                            <tr>
                                <td width="80px"><label for="periode">Periode</label> </td>
                                <td width="20px">:</td>
                                <td width="100px"> 
                                    <select name="blnawal" id="blnawal" class="form-control select" style="width:100%"
                                    data-sf="load_bulan" data-placeholder="Bln Awal" required></select>
                                </td>
                                <td width="10px">sd</td>
                                <td width="100px"> 
                                    <select name="blnakhir" id="blnakhir" class="form-control select" style="width:100%"
                                    data-sf="load_bulan" data-placeholder="Bln Akhir" required></select>
                                </td>
                                <td width="20px">Tahun</td>
                                <td width="100px"> 
                                    <select name="thn" id="thn" class="form-control select" style="width:100%"
                                    data-sf="load_tahun" data-placeholder="Tahun" required></select>
                                </td>

                                <td width="100px">
                                    <button type = "button" class="btn btn-primary" id="cmdrefresh">Refresh</button>
                                </td>
                                <td></td>
                            </tr>
                            
                        </table>
                    </td>
                </tr> 
                <tr>
                    <td>
                        <table style="height:100%;width:100%">
                            <tr>
                                <td>
                                <div class="box-header with-border">
                                    <h3 class="box-title">Progress Omset Bulanan</h3>
                                </div>

                                </td>
                            </tr>
                            <tr>
                                <td heig>
                                    
                                    <div class="box-body chart-responsive">
                                        <canvas id="chartgrfkpenjualan"></canvas>
                                    </div>
                                                <!-- /.box-body -->
                                            
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
            </table> 
        </div>
    </form>
</div>

<script type="text/javascript">
<?=cekbosjs();?>
    bos.rptpenjualangrafik.reloadgrafik      = function(){
        bjs.ajax(this.url + '/reloadgrafik', bjs.getdataform(this.obj.find("form"))) ;
    }    

    bos.rptpenjualangrafik.initcomp      = function(){
        bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.rptpenjualangrafik.cmdRefresh                        = bos.rptpenjualangrafik.obj.find("#cmdrefresh") ;
    bos.rptpenjualangrafik.initfunc    = function(){
        this.obj.find("#cmdrefresh").on("click", function(e){
            e.preventDefault() ;
            bos.rptpenjualangrafik.cmdRefresh.html("Loading..");
            bos.rptpenjualangrafik.cmdRefresh.prop("disabled",true);
            bos.rptpenjualangrafik.reloadgrafik() ;
        }) ;

        
    }

    
    $(function(){
        bos.rptpenjualangrafik.initcomp() ;
        // bos.rptpenjualangrafik.initcallback() ;
        bos.rptpenjualangrafik.initfunc() ;
    })

</script>