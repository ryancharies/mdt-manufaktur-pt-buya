<style media="screen">
   #bos-form-rptperbandingannrcthn-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
   #bos-form-rptperbandingannrcthn-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<form novalidate>          
<div class="bodyfix scrollme" style="height:100%"> 
   <table class="osxtable form" border="0">
		<tr>
			<td width="80px"><label for="periode">Tahun</label> </td> 
			<td width="20px">:</td>
			<td width="100px">
			<input style="width:80px" type="text" class="form-control date" id="periode" name="periode" required value=<?=date("Y")?> <?=date_periodset(false)?>></td>
			</td>
			<td></td>
			<td width="100px"> 
				<button class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
			</td>
			<td width="100px">	
				<button class="btn btn-primary pull-right" id="cmdview">Preview</button> 
			</td>
		</tr>  
      <tr><td colspan="6"><hr class="no-margin no-padding"></td></tr>
   </table> 
   <div class="row" style="height: calc(100% - 50px);"> 
      <div class="col-sm-12 full-height">
         <div id="grid1" class="full-height"></div>
      </div>  
   </div> 
</div>
</form>
<script type="text/javascript">
	<?=cekbosjs();?>

	bos.rptperbandingannrcthn.grid1_data 	 = null ;
	bos.rptperbandingannrcthn.grid1_loaddata= function(){
		this.grid1_data 		= {
			"periode"	   : this.obj.find("#periode").val()
		} ;
	}

	bos.rptperbandingannrcthn.grid1_load    = function(){
		var thn = this.obj.find("#periode").val();
        var kolom = [];
        kolom.push({ field: 'kode', caption: 'Rekening', size: '150px', sortable: false});
        kolom.push({ field: 'keterangan', caption: 'Keterangan', size: '350px', sortable: false});
        //console.log(thn-4+"eeee");
        //var btsakhir = thn -4;
        for(t=thn;t>=thn-1;t--){
            kolom.push({ field: t,caption:t,  size: '130px', style:'text-align:right',sortable: false});
        }
        

      	this.obj.find("#grid1").w2grid({
			name		: this.id + '_grid1',
			limit 	: 100 ,
			url 		: bos.rptperbandingannrcthn.base_url + "/loadgrid",
			postData : this.grid1_data ,
			show 		: {
				footer 		: true,
				toolbar		: false,
				toolbarColumns  : false
			},
			multiSearch		: false, 
			columns: kolom
		});
   }

   bos.rptperbandingannrcthn.grid1_setdata	= function(){
		w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
	}
	bos.rptperbandingannrcthn.grid1_reload		= function(){
		w2ui[this.id + '_grid1'].reload() ;
	}
	bos.rptperbandingannrcthn.grid1_destroy 	= function(){
		if(w2ui[this.id + '_grid1'] !== undefined){
			w2ui[this.id + '_grid1'].destroy() ;
		}
	}

	bos.rptperbandingannrcthn.grid1_initcolumn 	= function(){
        //bos.rptanalisaratiokeu.grid1_removeallcolumn();
        //w2ui[this.id + '_grid1'].addColumn([{ field: 'keterangan1', caption: 'Keterangan1', size: '50px', sortable: false}]) ;
        w2ui[this.id + '_grid1'].destroy() ;
        this.grid1_loaddata() ;
        this.grid1_load() ;
    }

	bos.rptperbandingannrcthn.grid1_render 	= function(){
		this.obj.find("#grid1").w2render(this.id + '_grid1') ;
	}

	bos.rptperbandingannrcthn.grid1_reloaddata	= function(){
		this.grid1_loaddata() ;
		this.grid1_setdata() ;
		this.grid1_reload() ;
	}

	bos.rptperbandingannrcthn.cmdedit		= function(id){
		bjs.ajax(this.url + '/editing', 'id=' + id);
	}

	bos.rptperbandingannrcthn.cmddelete		= function(id){
		if(confirm("Hapus Data?")){
			bjs.ajax(this.url + '/deleting', 'id=' + id);
		}
	}

	bos.rptperbandingannrcthn.init				= function(){
		bjs.ajax(this.url + "/init") ;
	}

	bos.rptperbandingannrcthn.obj.find("#cmdrefresh").on("click", function(){ 
		bos.rptperbandingannrcthn.grid1_initcolumn(); ; 
	}) ;

	bos.rptperbandingannrcthn.obj.find("#cmdview").on("click", function(){
        bos.rptperbandingannrcthn.initreport();
	}) ;
    
    bos.rptperbandingannrcthn.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form")),bos.rptperbandingannrcthn.obj.find("#cmdview")) ;
    }
    bos.rptperbandingannrcthn.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }


	bos.rptperbandingannrcthn.initcomp	= function(){
		bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
		bjs.initdate("#" + this.id + " .date") ;
		bjs_os.inittab(this.obj, '.tpel') ;
		bjs_os._header(this.id) ; //drag header
		this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
	}

	bos.rptperbandingannrcthn.initcallback	= function(){
		this.obj.on("bos:tab", function(e){
			bos.rptperbandingannrcthn.tabsaction( e.i )  ;
		});  

		this.obj.on("remove",function(){
			bos.rptperbandingannrcthn.grid1_destroy() ;
		}) ;   	
      
	}

	bos.rptperbandingannrcthn.initfunc 		= function(){
		this.init() ;
		this.grid1_loaddata() ;
		this.grid1_load() ;

		this.obj.find("form").on("submit", function(e){ 
         e.preventDefault() ;
      	});
	}


	$(function(){
		bos.rptperbandingannrcthn.initcomp() ;
		bos.rptperbandingannrcthn.initcallback() ;
		bos.rptperbandingannrcthn.initfunc() ;
	}) ;
</script>