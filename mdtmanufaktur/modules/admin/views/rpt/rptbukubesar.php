<style media="screen">
   #bos-form-rptbukubesar-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
   #bos-form-rptbukubesar-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<form novalidate>         
<div class="bodyfix scrollme" style="height:100%"> 
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<label>Antara Tanggal</label>
				<div class="row">
					<div class="col-md-6">
						<input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
					</div>
					<div class="col-md-6">
						<input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="form-group">
				<label>Cabang</label>
				<div class="input-group">
					<select name="skd_cabang" id="skd_cabang" class="form-control s2" style="width:100%"
											data-placeholder="Pilih Cabang" data-sf="load_cabang"></select>
					<span class="input-group-addon">
						<span id="cmdcabang" style="cursor:pointer"><i class="fa fa-search"></i></span>
					</span>
				
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-5">
			<div class="form-group">
				<label>Rekening</label>
				<select name="rekening" id="rekening" class="form-control select" style="width:100%"
            		data-sf="load_rekening" data-placeholder="Rekening" required></select>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<label>Faktur</label>
				<select name="skd_faktur" id="skd_faktur" class="form-control s2" style="width:100%"
            		data-sf="load_kodefaktur" data-placeholder="Faktur" required></select>
			</div>
		</div>
		<div class="col-md-2">
			<div class="form-group full-width">
                <label>&nbsp;</label>
				<div class="input-group full-width">
						<button class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
				</div>
            </div>
		</div>
		<div class="col-md-2">
			<div class="form-group">
                <label>Cetak</label>
				<div class="input-group">
                    <select name="export" id="export" class="form-control select" style="width:100%"
                    	data-sf="load_export" data-placeholder="PDF" required></select>
                    <span class="input-group-addon">
                        <span id="cmdview" style="cursor:pointer">Cetak <i class="fa fa-print"></i></span>
                    </span>
                
                </div>
            </div>
		</div>
	</div>
   
   <div class="row" style="height: calc(100% - 100px);"> 
      <div class="col-sm-12 full-height">
         <div id="grid1" class="full-height"></div>
      </div> 
   </div> 
</div>
</form>
<script type="text/javascript">
	<?=cekbosjs();?>

	bos.rptbukubesar.grid1_data 	 = null ;
	bos.rptbukubesar.grid1_loaddata= function(){
		mdl_cabang.vargr.selection = bos.rptbukubesar.obj.find('#skd_cabang').val();
		this.grid1_data 		= {
			"tglawal"	   : this.obj.find("#tglawal").val(),
			"tglakhir"	   : this.obj.find("#tglakhir").val(),
			"rekening"	   : this.obj.find("#rekening").val(),
			'skd_cabang':JSON.stringify(mdl_cabang.vargr.selection),
			'skd_faktur':JSON.stringify(bos.rptbukubesar.obj.find('#skd_faktur').val())
		} ;
	}

	bos.rptbukubesar.grid1_load    = function(){
      this.obj.find("#grid1").w2grid({
			name		: this.id + '_grid1',
			limit 	: 100 ,
			url 		: bos.rptbukubesar.base_url + "/loadgrid",
			postData : this.grid1_data ,
			show 		: {
				footer 		: true,
				toolbar		: false,
				toolbarColumns  : false,
				lineNumbers : true
			},
			multiSearch		: false, 
			columns: [
				{ field: 'tgl', caption: 'Tgl', size: '100px', sortable: false},
				{ field: 'faktur', caption: 'Faktur', size: '100px', sortable: false},
				{ field: 'keterangan', caption: 'keterangan', size: '200px', sortable: false},
				{ field: 'debet',render:'float:2', caption: 'Debet', size: '100px',style:'text-align:right', sortable: false},
				{ field: 'kredit',render:'float:2', caption: 'Kredit', size: '100px',style:'text-align:right', sortable: false},
				{ field: 'total',render:'float:2', caption: 'Total', size: '120px',style:'text-align:right', sortable: false},
				{ field: 'username', caption: 'Username', size: '100px', sortable: false}
			]
		});
   }

   bos.rptbukubesar.grid1_setdata	= function(){
		w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
	}
	bos.rptbukubesar.grid1_reload		= function(){
		w2ui[this.id + '_grid1'].reload() ;
	}
	bos.rptbukubesar.grid1_destroy 	= function(){
		if(w2ui[this.id + '_grid1'] !== undefined){
			w2ui[this.id + '_grid1'].destroy() ;
		}
	}

	bos.rptbukubesar.grid1_render 	= function(){
		this.obj.find("#grid1").w2render(this.id + '_grid1') ;
	}

	bos.rptbukubesar.grid1_reloaddata	= function(){
		this.grid1_loaddata() ;
		this.grid1_setdata() ;
		this.grid1_reload() ;
	}

	bos.rptbukubesar.cmdedit		= function(id){
		bjs.ajax(this.url + '/editing', 'id=' + id);
	}

	bos.rptbukubesar.cmddelete		= function(id){
		if(confirm("Hapus Data?")){
			bjs.ajax(this.url + '/deleting', 'id=' + id);
		}
	}


	bos.rptbukubesar.obj.find("#cmdrefresh").on("click", function(){ 
   		bos.rptbukubesar.grid1_reloaddata() ; 
	}) ;

	bos.rptbukubesar.obj.find("#cmdview").on("click", function(){
		bos.rptbukubesar.initreport();
	}) ;
	bos.rptbukubesar.objv = bos.rptbukubesar.obj.find("#cmdview");
    bos.rptbukubesar.initreport  = function(s,e){
		mdl_cabang.vargr.selection = bos.rptbukubesar.obj.find('#skd_cabang').val();
		bos.rptbukubesar.skd_faktur = bos.rptbukubesar.obj.find('#skd_faktur').val();
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))+"&skd_cabang="+JSON.stringify(mdl_cabang.vargr.selection)+"&skd_faktur="+JSON.stringify(bos.rptbukubesar.skd_faktur),bos.rptbukubesar.objv) ;
        //bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }
    bos.rptbukubesar.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }

	bos.rptbukubesar.initcomp	= function(){
		bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
		bjs.initselect({
            class : "#" + this.id + " .s2",
            multi : true
        }) ;
		bjs.initdate("#" + this.id + " .date") ;
		bjs_os.inittab(this.obj, '.tpel') ;
		bjs_os._header(this.id) ; //drag header
		this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
	}

	bos.rptbukubesar.initcallback	= function(){
		this.obj.on("bos:tab", function(e){
			bos.rptbukubesar.tabsaction( e.i )  ;
		});  

		this.obj.on("remove",function(){
			bos.rptbukubesar.grid1_destroy() ;
		}) ;   	

		// this.obj.find("#rekening").on("select2:select", function(e){ 
        //  	bjs.ajax(bos.rptbukubesar.url+"/refresh", "rekening=" + $(this).val()) ; 
      	// }) ; 
      
	}

	bos.rptbukubesar.initfunc 		= function(){
		this.grid1_loaddata() ;
		this.grid1_load() ;

		this.obj.find("form").on("submit", function(e){ 
         	e.preventDefault() ; 
      	});

	  	this.obj.find('#cmdcabang').on('click', function(){
            mdl_cabang.vargr.selection = bos.rptbukubesar.obj.find('#skd_cabang').val();
            mdl_cabang.open(function(r){
                r = JSON.parse(r);
                bos.rptbukubesar.cabang = [];
                $.each(r, function(i, v){
                    bos.rptbukubesar.cabang.push({'id':v.kode,'text':v.keterangan})                   
                });
                bos.rptbukubesar.obj.find('#skd_cabang').sval(bos.rptbukubesar.cabang);
            });
        
            
        });
	}


	$(function(){
		bos.rptbukubesar.initcomp() ;
		bos.rptbukubesar.initcallback() ;
		bos.rptbukubesar.initfunc() ;
	}) ;
</script>
