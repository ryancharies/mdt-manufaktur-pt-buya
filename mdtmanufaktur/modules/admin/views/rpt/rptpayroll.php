<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Payroll</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptpayroll.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <table width = '100%'>
                <tr>
                    <td>
                        <table class="osxtable form">
                            <tr>
                                <td width="100px"><label for="periode">Periode</label> </td>
                                <td width="5px">:</td>
                                <td width="400px">
                                    <select name="periode" id="periode" class="form-control select" style="width:100%" data-placeholder="Periode"></select>
                                </td>
                                <td width="85px">
                                    <input readonly style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                </td>
                                <td width="5px">sd</td>
                                <td width="85px">
                                    <input readonly style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" value=<?=date("d-m-Y")?> <?=date_set()?>>
                                </td>
                                <td width="300px">
                                    <input readonly placeholder="Status Posting"  type="text" class="form-control" id="status" name="status">
                                </td>
                                <td width="200px">
                                    <input readonly placeholder="Faktur"  type="text" class="form-control" id="faktur" name="faktur">
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="osxtable form">
                            <tr>
                                <td width="100px"><label for="bagian">Bagian</label> </td>
                                <td width="5px">:</td>
                                <td width="200px">
                                    <select name="bagian" id="bagian" class="form-control select" style="width:100%" data-placeholder="Bagian"></select>
                                </td>
                                <td width="300px">
                                    <input style="width:100%" type="text" class="form-control" placeholder="Masukkan NIP / Nama / Rekening" id="cari" name="cari">
                                </td>
                                <td></td>
                            </tr>
                        </table>
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="osxtable form">
                            <tr>
                                <td width="100px"></td>
                                <td width="5px"></td>
                                <td width="85px">
                                    <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                </td>
                                <td width="100px">
                                    <select name="export" id="export" class="form-control selectauto" style="width:100%"
                                        data-sf="load_export" data-placeholder="PDF" required></select>
                                </td>
                                <td width="85px">
                                    <button type="button" class="btn btn-primary pull-right" id="cmdreport">Report</button>
                                </td>
                                <td width="110px">
                                    <button type="button" class="btn btn-success pull-right" id="cmdslip">Cetak Slip</button>
                                </td>
                                <td width="110px">
                                    <button type="button" class="btn btn-danger pull-right" id="cmddaftargaji">Daftar Gaji</button>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width = '100%' height = "680px">
                            <tr>
                                <td >
                                    <div id="grid1" class="full-height">Pilih Periode dan klik Refresh!!!</div>
                                </td>
                                <td width = '3px'></td>
                                <td width = "400px" valign = "top">
                                    <table width = "400px">
                                        <tr>
                                            <td height = "650px" >
                                                Detail Pembayaran
                                                <div id="grid2" class="full-height"></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                <tr>
            </table>               
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.rptpayroll.grid1_col    = <?= $kolomgrid1 ; ?> ;
    bos.rptpayroll.grid1_data    = null ;
    bos.rptpayroll.grid1_loaddata = function(){

        var tglawal = bos.rptpayroll.obj.find("#tglawal").val();
        var tglakhir = bos.rptpayroll.obj.find("#tglakhir").val();
        var periode = bos.rptpayroll.obj.find("#periode").val();
        var bagian = bos.rptpayroll.obj.find("#bagian").val();
        var cari = bos.rptpayroll.obj.find("#cari").val();
        this.grid1_data 		= {'tglawal':tglawal,'tglakhir':tglakhir,'periode':periode,'bagian':bagian,'cari':cari} ;
    }

    bos.rptpayroll.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            recordHeight : 30,
            limit 	: 100 ,
            url 	: bos.rptpayroll.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true,
                selectColumn: true,
                multiSelect:true
            },
            columns: this.grid1_col ,
            onSelect: function(event) {
                event.onComplete = function () {
                    this.set(event.recid,{cetakslip:"Ya"});
                   // w2ui[event.target].save();
                }
                //this.set(event.recid,{cetakslip:"Cetak Slip"});
            },
            onUnselect: function(event) {
                event.onComplete = function () {
                    this.set(event.recid,{cetakslip:"Tidak"});
                    //w2ui[event.target].save();
                
                }
                //this.set(event.recid,{cetakslip:"Tidak Cetak Slip"});
            },  
        });


    }


    bos.rptpayroll.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.rptpayroll.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.rptpayroll.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptpayroll.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    

    bos.rptpayroll.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.rptpayroll.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false
            },
            columns: [
                { field: 'no', caption: 'No', size: '50px', sortable: false},
                { field: 'kode', caption: 'Kode', size: '120px', sortable: false },
                { field: 'ketrekening', caption: 'Ket. Rekening', size: '120px', sortable: false },
                { field: 'nominal', caption: 'Nominal', size: '100px', sortable: false, render:'float:2'},
                { field: 'cmddelete', caption: ' ', size: '80px', sortable: false }
            ],
            summary: [
                { recid: "ZZZZ", no: '', kode: '', ketrekening: 'Total', nominal: 0 }
            ]
        });


    }

    bos.rptpayroll.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.rptpayroll.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }

   
    bos.rptpayroll.init				= function(){
        bjs.ajax(this.url + "/init") ;
        bos.rptpayroll.initdetail();
    }

    bos.rptpayroll.initdetail 			= function(){
        var datagrid = w2ui[this.id + '_grid2'].records;

        this.obj.find("#nomor").val(datagrid.length+1) ;
        this.obj.find("#rekening").sval("") ;
        this.obj.find("#nominal").val("0") ;



    }
    
    bos.rptpayroll.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    

    bos.rptpayroll.initcomp	= function(){
         bjs.initselect({
		 	class : "#" + this.id + " .selectauto"
		 }) ;
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
        
        this.obj.find('#periode').select2({
            ajax: {
                url: bos.rptpayroll.base_url + '/seekperiode',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.obj.find('#bagian').select2({
            ajax: {
                url: bos.rptpayroll.base_url + '/seekbagian',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            }
        });

        this.grid2_load();
    }

    bos.rptpayroll.initcallback	= function(){


        this.obj.on("remove",function(){
            bos.rptpayroll.grid1_destroy() ;
            bos.rptpayroll.grid2_destroy() ;
                

        }) ;
    }

    bos.rptpayroll.loadperiode	= function(){
        var periode = this.obj.find("#periode").val();
            bjs.ajax(this.url + '/loadperiode', 'periode=' + periode);
    }

    bos.rptpayroll.loadpembayaran	= function(){
        w2ui['bos-form-rptpayroll_grid2'].clear();
        var periode = this.obj.find("#periode").val();
        bjs.ajax(this.url + '/loadpembayaran', 'periode=' + periode);
    }

    bos.rptpayroll.obj.find("#cmdreport").on("click", function(){
        bos.rptpayroll.initreport();
	}) ;

    bos.rptpayroll.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }
    bos.rptpayroll.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }

    bos.rptpayroll.cmdslip = bos.rptpayroll.obj.find("#cmdslip") ;
    bos.rptpayroll.cmdrefresh = bos.rptpayroll.obj.find("#cmdrefresh") ;
    bos.rptpayroll.cmddaftargaji = bos.rptpayroll.obj.find("#cmddaftargaji") ;
    bos.rptpayroll.initreportslip  = function(){
       // bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
        var datagrid = w2ui[this.id + '_grid1'].records;
        datagrid = JSON.stringify(datagrid);
        bjs.ajax(this.base_url  + '/printslipa5', bjs.getdataform(this.obj.find("form"))+"&grid1="+datagrid,bos.rptpayroll.cmdslip);
    }

    bos.rptpayroll.obj.find("#cmdslip").on("click", function(){
        if(confirm("Apakah slip akan di cetak?, slip akan dicetak untuk data yang dipilih...")){
            bos.rptpayroll.initreportslip();
        }
	}) ;
	
    bos.rptpayroll.showReport = function(urlfile){
        bjs_os.form_report(urlfile) ;
    }

    bos.rptpayroll.obj.find("#cmddaftargaji").on("click", function(){
        if(confirm("Apakah daftar gaji akan dicetak?")){
            bos.rptpayroll.initreportdaftargaji();
        }
	}) ;

    bos.rptpayroll.initreportdaftargaji  = function(s,e){
        bjs.ajax(this.base_url+ '/initreportdaftargaji', bjs.getdataform(this.obj.find("form")),bos.rptpayroll.cmddaftargaji) ;
    }
    bos.rptpayroll.openreportdaftargaji  = function(){
        bjs_os.form_report( this.base_url + '/showreportdaftargaji' ) ;
    }   
    
    bos.rptpayroll.initfunc 		= function(){
        this.init() ;
       

        this.obj.find("#cmdrefresh").on("click",function(){
            // bos.rptpayroll.grid1_loaddata();
            var periode = bos.rptpayroll.obj.find("#periode").val();
            bjs.ajax(bos.rptpayroll.base_url + '/initgrid1', 'periode=' + periode,bos.rptpayroll.cmdrefresh);     
            
        });

        this.obj.find("#periode").on("change",function(){
            
            bos.rptpayroll.loadperiode();
        });
    }
    
    

    $(function(){
        bos.rptpayroll.initcomp() ;
        bos.rptpayroll.initcallback() ;
        bos.rptpayroll.initfunc() ;
    }) ;
</script>
