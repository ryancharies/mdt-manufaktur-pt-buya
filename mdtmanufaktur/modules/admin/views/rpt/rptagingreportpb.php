<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style>
<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Laporan Aging Report</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptagingreportpb.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <table class="osxtable form" border="0">
                <tr>
                    <td width="80px"><label for="tglakhir">Sampai Tgl</label> </td>
                    <td width="20px">:</td>
                    <td width="100px">
                        <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                    </td>
                    <td width="100px">
                        <button class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                    </td>
                    <td width="100px">
                        <button class="btn btn-primary pull-right" id="cmdpreview">Preview</button>
                    </td>
                    <td></td>
                </tr>
                <tr><td colspan="6"><hr class="no-margin no-padding"></td></tr>
            </table>
            <div class="row" style="height: calc(100% - 50px);">
                <div class="col-sm-12 full-height">
                    <div id="grid1" class="full-height"></div>
                </div>
            </div>
        </div>
        <div class="modal fade" style="position:absolute;" id="wrap-preview-detail-d" role="dialog" data-backdrop="false" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="wm-title">Preview Detail Pembelian</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Faktur</label>
                                    <input type="text" name="cFaktur" id="faktur" class="form-control" placeholder="faktur" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Supplier</label>
                                    <input type="text" name="supplier" id="supplier" readonly = true class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tgl</label>
                                    <input type="text" name="tgl" id="tgl" readonly = true class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Jatuh Tempo</label>
                                    <input type="text" name="jthtmp" id="jthtmp" readonly = true class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Fkt PO</label>
                                    <input type="text" name="fktpo" id="fktpo" class="form-control" placeholder="faktur PO" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>%PPn</label>
                                    <input type="text" style = "text-align:right" name="persppn" id="persppn" class="form-control number" placeholder="0.00" readonly = true required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="grid2" style="height:250px"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Sub Total</label>
                                    <input type="text" style = "text-align:right" name="subtotal" id="subtotal" class="form-control number" placeholder="0.00" readonly = true required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Diskon</label>
                                    <input type="text" style = "text-align:right" name="diskon" id="diskon" class="form-control number" placeholder="0.00" readonly = true required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>PPn</label>
                                    <input type="text" style = "text-align:right" name="ppn" id="ppn" class="form-control number" placeholder="0.00" readonly = true required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Total</label>
                                    <input type="text" style = "text-align:right" name="total" id="total" class="form-control number" placeholder="0.00" readonly = true required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <br/>
                                    <button type = "button" class="btn btn-success" id="cmdCetakLaporanDetail">Print Report</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" style="position:absolute;" id="wrap-preview-detailaset-d" role="dialog" data-backdrop="false" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="wm-title">Preview Detail Aset</h4>
                    </div>
                    <div class="modal-body">
                        <table class="osxtable form">
                            <tr>
                                <td width="14%"><label for="kode">Kode</label> </td>
                                <td width="1%">:</td>
                                <td >
                                    <input readonly type="text" maxlength="8" id="kodeaset" name="kodeaset" class="form-control" placeholder="kode" required>
                                </td>
                            </tr>
                            <tr>
                                <td width="14%"><label for="faktur">faktur</label> </td>
                                <td width="1%">:</td>
                                <td >
                                    <input readonly type="text" maxlength="8" id="fakturaset" name="fakturaset" class="form-control" placeholder="faktur" required>
                                </td>
                            </tr>
                            <tr>
                                <td width="14%"><label for="keterangan">Keterangan</label> </td>
                                <td width="1%">:</td>
                                <td>
                                    <input readonly type="text" id="keteranganaset" name="keteranganaset" class="form-control" placeholder="keterangan" required>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="cabang">Cabang</label> </td>
                                <td>:</td>
                                <td>
                                    <select readonly name="cabangaset" id="cabangaset" class="form-control select" style="width:100%"
                                            data-placeholder="Cabang / Kantor" required></select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="golaset">Gol. Aset</label> </td>
                                <td>:</td>
                                <td>
                                    <select readonly name="golasetaset" id="golasetaset" class="form-control select" style="width:100%"
                                            data-placeholder="Golongan Aset" required></select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="vendor">Vendor</label> </td>
                                <td>:</td>
                                <td>
                                    <select readonly name="vendoraset" id="vendoraset" class="form-control select" style="width:100%"
                                            data-placeholder="Vendor" required></select>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="tglperolehan">Tgl. Perolehan</label> </td>
                                <td>:</td>
                                <td>
                                    <input readonly style="width:80px" type="text" class="form-control date" id="tglperolehanaset" name="tglperolehanaset" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                </td>
                            </tr>
                            <tr>
                                <td width="14%"><label for="lama">Lama (Tahun)</label> </td>
                                <td width="1%">:</td>
                                <td>
                                    <input  readonly style="width:50px" maxlength="5" type="text" name="lamaaset" id="lamaaset" class="form-control number" value="0">

                                </td>
                            </tr>
                            <tr>
                                <td width="14%"><label for="hp">Harga Perolehan</label> </td>
                                <td width="1%">:</td>
                                <td>
                                    <input readonly  style="width:200px" maxlength="20" type="text" name="hpaset" id="hpaset" class="form-control number" value="0">

                                </td>
                            </tr>
                            <tr>
                                <td width="14%"><label for="unit">Unit</label> </td>
                                <td width="1%">:</td>
                                <td>
                                    <input  readonly style="width:50px" maxlength="7" type="text" name="unitaset" id="unitaset" class="form-control number" value="0" required>

                                </td>
                            </tr>
                            <tr>
                                <td width="14%"><label for="jenis">Jenis Peny</label> </td>
                                <td width="1%">:</td>
                                <td width="50%">
                                    <div class="radio-inline">
                                        <label>
                                            <input type="radio" name="jenisaset" class="jenisaset" value="1" checked>
                                            Garis Lurus
                                        </label>
                                        &nbsp;&nbsp;
                                    </div>
                                    <div class="radio-inline">
                                        <label>
                                            <input type="radio" name="jenisaset" class="jenisaset" value="2">
                                            Saldo Menurun
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td width="14%"><label for="tarifpenyaset">Tarif Peny (%)</label> </td>
                                <td width="1%">:</td>
                                <td>
                                    <input  style="width:50px" maxlength="7" type="text" name="tarifpenyaset" id="tarifpenyaset" class="form-control number" value="0">

                                </td>
                            </tr>
                            <tr>
                                <td width="14%"><label for="residu">Nilai Residu</label> </td>
                                <td width="1%">:</td>
                                <td>
                                    <input  style="width:200px" maxlength="20" type="text" name="residu" id="residu" class="form-control number" value="0">

                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    <?=cekbosjs();?>


    //grid daftar pembelian
    bos.rptagingreportpb.grid1_data    = null ;
    bos.rptagingreportpb.grid1_loaddata= function(){
        var tglAkhir = bos.rptagingreportpb.obj.find("#tglakhir").val();
        this.grid1_data 		= {'tglAkhir':tglAkhir} ;
    }

    bos.rptagingreportpb.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.rptagingreportpb.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'supplier', caption: 'Supplier', size: '200px', sortable: false },
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'jthtmp', caption: 'Jatuh Tempo', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'total', render: 'int' ,caption: 'Total', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'pelunasan', render: 'int' ,caption: 'Pelunasan', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'saldo', render: 'int' ,caption: 'Saldo', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'cmdPreview', caption: ' ', size: '120px', sortable: false }
            ]
        });
    }

    bos.rptagingreportpb.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }

    bos.rptagingreportpb.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.rptagingreportpb.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptagingreportpb.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rptagingreportpb.grid1_reloadData	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }


    bos.rptagingreportpb.cmdpreviewdetail	= function(faktur){
        //$("#modal-preview").html("");
        //alert("lasjdlasd");
        //bjs.ajax(bos.rptagingreportpb.url + '/PreviewDetailPembelianStock/'+ faktur);
        bjs.ajax(this.url + '/PreviewDetailPembelianStock', 'faktur=' + faktur);
    }

    bos.rptagingreportpb.cmdpreviewdetailaset	= function(kode){
        //$("#modal-preview").html("");
        //alert("lasjdlasd");
        //bjs.ajax(bos.rptagingreportpb.url + '/PreviewDetailPembelianStock/'+ faktur);
        bjs.ajax(this.url + '/PreviewDetailPembelianaset', 'kode=' + kode);
    }
    bos.rptagingreportpb.setopt = function(nama,isi){
        this.obj.find('input:radio[name='+nama+'][value='+isi+']').prop('checked',true);
    }

    bos.rptagingreportpb.cmdrefresh          = bos.rptagingreportpb.obj.find("#cmdrefresh") ;
    bos.rptagingreportpb.cmdpreview     = bos.rptagingreportpb.obj.find("#cmdpreview")
    bos.rptagingreportpb.cmdCetakLaporanDetail     = bos.rptagingreportpb.obj.find("#cmdCetakLaporanDetail") ;
    bos.rptagingreportpb.initfunc    = function(){
        this.obj.find("#cmdpreview").on("click", function(e){
            e.preventDefault() ;
            bos.rptagingreportpb.initreportTotal(0,0) ;
        }) ;

        this.obj.find("#cmdrefresh").on("click", function(e){
            e.preventDefault() ;
            bos.rptagingreportpb.grid1_reloadData() ;
        }) ;
        this.obj.find("#cmdCetakLaporanDetail").on("click",function(e){
            e.preventDefault() ;
            bos.rptagingreportpb.initReportDetailPembelian(0,0) ;
        });
    }

    bos.rptagingreportpb.initReportDetailPembelian  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.rptagingreportpb.loadmodalpreview      = function(l){
        this.obj.find("#wrap-preview-detail-d").modal(l) ;
    }

    bos.rptagingreportpb.loadmodalpreviewaset      = function(l){
        this.obj.find("#wrap-preview-detailaset-d").modal(l) ;
    } 

    bos.rptagingreportpb.initcomp		= function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;
        this.grid2_load() ;

        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.rptagingreportpb.initcallback	= function(){
        this.obj.on('remove', function(){
            bos.rptagingreportpb.grid1_destroy() ;
            bos.rptagingreportpb.grid2_destroy() ;
            bos.rptagingreportpb.grid2_destroy() ;
        }) ;
    }

    bos.rptagingreportpb.setPreview      = function(cFaktur){
        //e.preventDefault() ;
        //bos.rptagingreportpb.cmdview.button('loading') ;
        bos.rptagingreportpb.initreport(0,0) ;
    }

    bos.rptagingreportpb.initreport  = function(s,e){
        //bjs.initprogress(this.obj.find("#progress_1"),s,e) ;
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.rptagingreportpb.initreportTotal  = function(s,e){
        //bjs.initprogress(this.obj.find("#progress_1"),s,e) ;
        bjs.ajax(this.base_url+ '/initreportTotal', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.rptagingreportpb.openreport  = function(){
        // bos.rptkartustock.cmdview.button('reset') ;
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }

    bos.rptagingreportpb.openreporttotal  = function(){
        // bos.rptkartustock.cmdview.button('reset') ;
        bjs_os.form_report( this.base_url + '/showreporttotal' ) ;
    }

    bos.rptagingreportpb.grid2_data    = null ;
    bos.rptagingreportpb.grid2_loaddata= function(){
        this.grid2_data         = {} ;
    }


    bos.rptagingreportpb.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name    : this.id + '_grid2',
            show: {
                footer      : false,
                toolbar     : false,
                toolbarColumns  : false
            },
            multiSearch     : false,
            columns: [
                { field: 'no', caption: 'No', size: '30px', sortable: false, style:'text-align:center'},
                { field: 'keterangan', caption: 'Nama Stock', size: '150px', sortable: false },
                { field: 'HargaSatuan', render: 'int', caption: 'Harga Satuan', size: '100px', sortable: false, style:'text-align:right' },
                { field: 'qty', render: 'int', caption: 'Qty', size: '40px', sortable: false, style:'text-align:center'},
                { field: 'satuan', caption: 'Satuan', size: '80px', sortable: false, style:'text-align:center'},
                { field: 'pembelian', render: 'int', caption: 'Pembelian', size: '100px', sortable: false },
                { field: 'hargadiskon', render: 'int',caption: 'Harga Diskon', size: '100px', sortable: false,style:'text-align:right'},
                { field: 'total', render: 'int' , caption: 'Total', size: '90px', sortable: false,style:'text-align:right'}
            ]
        });
    }

    bos.rptagingreportpb.grid2_reload     = function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.rptagingreportpb.grid2_destroy    = function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.rptagingreportpb.grid2_render     = function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.rptagingreportpb.grid2_reloaddata = function(){
        this.grid2_reload() ;
    }

    $(function(){
        bos.rptagingreportpb.initcomp() ;
        bos.rptagingreportpb.initcallback() ;
        bos.rptagingreportpb.initfunc() ;
    });
</script>