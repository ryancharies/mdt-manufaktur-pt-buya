<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Laporan Kontrak Karyawan Jatuh Tempo</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptkontrakjthtmp.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div>
<div class="body">
    <form novalidate>         
        <div class="bodyfix scrollme" style="height:100%"> 
            <table class="osxtable form" border="0">

                <tr>
                    <td width="80px"><label for="tglawal">Antara Tgl</label> </td>
                    <td width="20px">:</td>
                    <td width="100px"> 
                        <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                    </td>
                    <td width="100px"> 
                        <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                    </td>
                    <td width="100px">
                        <button class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                    </td>
					<td width="100px">	
						<button type= "button" class="btn btn-primary pull-right" id="cmdview">Preview</button>
					</td>
                    <td></td>
                </tr>  
                <tr><td colspan="6"><hr class="no-margin no-padding"></td></tr>
            </table> 
            <div class="row" style="height: calc(100% - 50px);"> 
                <div class="col-sm-12 full-height">
                    <div id="grid1" class="full-height"></div>
                </div>  
            </div> 
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>


    //grid daftar piutang
    bos.rptkontrakjthtmp.grid1_data    = null ;
    bos.rptkontrakjthtmp.grid1_loaddata= function(){
        var tglawal = bos.rptkontrakjthtmp.obj.find("#tglawal").val();
        var tglakhir = bos.rptkontrakjthtmp.obj.find("#tglakhir").val();
        this.grid1_data 		= {'tglawal':tglawal,'tglakhir':tglakhir} ;
    }

    bos.rptkontrakjthtmp.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.rptkontrakjthtmp.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers : true
            },
            multiSearch		: false,
            columns: [
                { field: 'noperjanjian', caption: 'No Perjanjian', size: '150px', sortable: false,style:"text-align:center;",frozen:true},
                { field: 'nomor', caption: 'Nomor', size: '100px', sortable: false,style:"text-align:center;",frozen:true},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false,style:"text-align:center;",frozen:true},
                { field: 'lamaperiode', caption: 'Lama', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'tglawal', caption: 'Tgl Awal', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'tglakhir', caption: 'Tgl Akhir', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'kode', caption: 'NIP', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false},
                { field: 'ktp', caption: 'No KTP', size: '100px', sortable: false},
                { field: 'alamat', caption: 'Alamat', size: '200px', sortable: false},
                { field: 'telepon', caption: 'Telepon', size: '100px', sortable: false},
                { field: 'bagian', caption: 'Bagian', size: '100px', sortable: false},
                { field: 'jabatan', caption: 'Jabatan', size: '100px', sortable: false}
            ]
        });
    }

    bos.rptkontrakjthtmp.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }

    bos.rptkontrakjthtmp.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.rptkontrakjthtmp.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptkontrakjthtmp.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rptkontrakjthtmp.grid1_reloadData	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }
	
	bos.rptkontrakjthtmp.obj.find("#cmdview").on("click", function(){
		bos.rptkontrakjthtmp.initreport();
	}) ;

    bos.rptkontrakjthtmp.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }
    bos.rptkontrakjthtmp.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }
	
    bos.rptkontrakjthtmp.cmdrefresh          = bos.rptkontrakjthtmp.obj.find("#cmdrefresh") ;
    bos.rptkontrakjthtmp.initfunc    = function(){
        this.obj.find("#cmdrefresh").on("click", function(e){
            e.preventDefault() ;
            bos.rptkontrakjthtmp.grid1_reloadData() ;
        }) ;


    }

    bos.rptkontrakjthtmp.initcomp		= function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;

        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.rptkontrakjthtmp.initcallback	= function(){
        this.obj.on('remove', function(){
            bos.rptkontrakjthtmp.grid1_destroy() ;

        }) ;
    }

    $(function(){
        bos.rptkontrakjthtmp.initcomp() ;
        bos.rptkontrakjthtmp.initcallback() ;
        bos.rptkontrakjthtmp.initfunc() ;
    });
</script>