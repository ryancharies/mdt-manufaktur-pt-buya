<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<form novalidate>         
    <div class="bodyfix scrollme" style="height:100%"> 
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Antara Tanggal</label>
                    <div class="row">
                        <div class="col-md-6">
                            <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                        </div>
                        <div class="col-md-6">
                            <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Cabang</label>
                    <div class="input-group">
                        <select name="skd_cabang" id="skd_cabang" class="form-control s2" style="width:100%"
                                                data-placeholder="Pilih Cabang" data-sf="load_cabang"></select>
                        <span class="input-group-addon">
                            <span id="cmdcabang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                        </span>
                    
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group full-width">
                    <label>&nbsp;</label>
                    <div class="input-group full-width">
                            <button class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Cetak</label>
                    <div class="input-group">
                        <select name="export" id="export" class="form-control select" style="width:100%"
                            data-sf="load_export" data-placeholder="PDF" required></select>
                        <span class="input-group-addon">
                            <span id="cmdview" style="cursor:pointer">Cetak <i class="fa fa-print"></i></span>
                        </span>
                    
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="height: calc(100% - 50px);"> 
            <div class="col-sm-12 full-height">
                <div id="grid1" class="full-height"></div>
            </div>  
        </div>
    </div>
</form>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.rptjurnal.grid1_data 	 = null ;
    bos.rptjurnal.grid1_loaddata= function(){
        mdl_cabang.vargr.selection = bos.rptjurnal.obj.find('#skd_cabang').val();
        this.grid1_data 		= {
            "tglawal"	    : this.obj.find("#tglawal").val(),
            "tglakhir"	    : this.obj.find("#tglakhir").val(),
            "skd_cabang"    :JSON.stringify(mdl_cabang.vargr.selection)
        } ;
    }

    bos.rptjurnal.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.rptjurnal.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false, 
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false,frozen:true},
                { field: 'tgl', caption: 'Tgl', size: '70px', sortable: false,frozen:true},
                { field: 'rekening', caption: 'Rekening', size: '100px', sortable: false},
				{ field: 'ketrekening', caption: 'Ket Rekening', size: '150px', sortable: false},
                { field: 'keterangan', caption: 'keterangan', size: '240px', sortable: false},
                { field: 'debet', caption: 'Debet', size: '120px',render:'int', sortable: false},
                { field: 'kredit', caption: 'Kredit', size: '120px',render:'int', sortable: false},
                { field: 'username', caption: 'Username', size: '100px', sortable: false}
            ]
        });
    }

    bos.rptjurnal.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.rptjurnal.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.rptjurnal.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptjurnal.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rptjurnal.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }


    bos.rptjurnal.obj.find("#cmdrefresh").on("click", function(){ 
        bos.rptjurnal.grid1_reloaddata() ;  
    }) ; 

    bos.rptjurnal.obj.find("#cmdview").on("click", function(){
        bos.rptjurnal.initreport();
    }) ;

    bos.rptjurnal.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }
    bos.rptjurnal.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }
    


    bos.rptjurnal.initcomp	= function(){

        bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
		bjs.initselect({
            class : "#" + this.id + " .s2",
            multi : true
        }) ;
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }  

    bos.rptjurnal.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.rptjurnal.tabsaction( e.i )  ;
        });  

        this.obj.on("remove",function(){
            bos.rptjurnal.grid1_destroy() ;
        }) ;   	

    }

    bos.rptjurnal.initfunc 		= function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.obj.find("form").on("submit", function(e){ 
            e.preventDefault() ;
        });

        this.obj.find('#cmdcabang').on('click', function(){
            mdl_cabang.vargr.selection = bos.rptjurnal.obj.find('#skd_cabang').val();
            mdl_cabang.open(function(r){
                r = JSON.parse(r);
                bos.rptjurnal.cabang = [];
                $.each(r, function(i, v){
                    bos.rptjurnal.cabang.push({'id':v.kode,'text':v.keterangan})                   
                });
                bos.rptjurnal.obj.find('#skd_cabang').sval(bos.rptjurnal.cabang);
            });
        
            
        });
    }

    $(function(){
        bos.rptjurnal.initcomp() ;
        bos.rptjurnal.initcallback() ;
        bos.rptjurnal.initfunc() ;
    }) ;
</script>