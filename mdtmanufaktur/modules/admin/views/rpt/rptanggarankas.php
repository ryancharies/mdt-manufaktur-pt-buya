<style media="screen">
   #bos-form-rptbukubesar-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
   #bos-form-rptbukubesar-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<form novalidate>         
<div class="bodyfix scrollme" style="height:100%"> 
    <table class="osxtable form" border="0">
		<tr>
			<td width="80px"><label for="tgl">Tgl</label> </td>
			<td width="20px">:</td>
			<td width="100px">
				<input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
			</td>
			<td width="40px">s/d</td>
			<td width="100px">
				<input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
			</td>   
			<td>
				<button type = "button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
			</td>
			<td width="100px">	
				<button type = "button" class="btn btn-primary pull-right" id="cmdview">Preview</button>
			</td>
		</tr> 
		
      <tr><td colspan="6"><hr class="no-margin no-padding"></td></tr>
   </table> 
   <div class="row" style="height: calc(100% - 50px);"> 
      <div class="col-sm-12 full-height">
         <div id="grid1" class="full-height"></div>
      </div> 
   </div> 
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.rptanggarankas.grid1_data 	 = null ;
    bos.rptanggarankas.grid1_loaddata= function(){
        this.grid1_data 		= {
            "tglawal"	   : this.obj.find("#tglawal").val(),
            "tglakhir"	   : this.obj.find("#tglakhir").val(),
			"stock"	   : this.obj.find("#stock").val()
        } ;
    }

    bos.rptanggarankas.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name		: this.id + '_grid1',
            limit 	: 100 ,
            url 		: bos.rptanggarankas.base_url + "/loadgrid",
            postData : this.grid1_data ,
            show 		: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            multiSearch		: false, 
            columns: [
                { field: 'no', caption: 'No', size: '50px', sortable: false,style:'text-align:right'},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false,style:'text-align:center'},
                { field: 'keterangan', caption: 'Keterangan', size: '450px', sortable: false},
                { field: 'penerimaan', caption: 'Penerimaan', size: '120px',render:'float:2',style:'text-align:right', sortable: false},
                { field: 'pengeluaran', caption: 'Pengeluaran', size: '120px',render:'float:2',style:'text-align:right', sortable: false},
                { field: 'jumlah', caption: 'Jumlah', size: '120px',render:'float:2',style:'text-align:right', sortable: false}
            ]
        });
    }

    bos.rptanggarankas.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.rptanggarankas.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.rptanggarankas.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptanggarankas.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rptanggarankas.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.rptanggarankas.obj.find("#cmdview").on("click", function(){
        bos.rptanggarankas.initreport();
	}) ;
    bos.rptanggarankas.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }
    
    bos.rptanggarankas.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }


    bos.rptanggarankas.initcomp		= function(){
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os.inittab(this.obj, '.tpel') ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag


    }

    bos.rptanggarankas.initcallback	= function(){
        this.obj.on("bos:tab", function(e){
            bos.rptanggarankas.tabsaction( e.i )  ;
        });
        this.obj.on('remove', function(){
            bos.rptanggarankas.grid1_destroy() ;
        }) ;
    }

    bos.rptanggarankas.initfunc	   = function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;
        


        this.obj.find("#cmdrefresh").on("click", function(e){
            bos.rptanggarankas.grid1_reloaddata();
        });
    }

    $('#stock').select2({
        ajax: {
            url: bos.rptanggarankas.base_url + '/seekstock',
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });

    $(function(){
        bos.rptanggarankas.initcomp() ;
        bos.rptanggarankas.initcallback() ;
        bos.rptanggarankas.initfunc() ;
    });
</script>