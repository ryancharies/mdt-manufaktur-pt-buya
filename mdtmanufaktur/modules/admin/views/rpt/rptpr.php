<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Laporan SPP</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptpr.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div>
<div class="body">
    <form novalidate>         
        <div class="bodyfix scrollme" style="height:100%"> 
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Antara Tanggal</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                            </div>
                            <div class="col-md-6">
                                <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label>Gudang</label>
                        <div class="input-group">
                            <select name="skd_gudang" id="skd_gudang" class="form-control s2" style="width:100%"
                                                    data-placeholder="Pilih Gudang" data-sf="load_gudang"></select>
                            <span class="input-group-addon">
                                <span id="cmdgudang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                            </span>
                        
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group full-width">
                        <label>&nbsp;</label>
                        <div class="input-group full-width">
                            <button type="button" class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Cetak</label>
                        <div class="input-group">
                            <select name="export" id="export" class="form-control select" style="width:100%"
                                data-sf="load_export" data-placeholder="PDF" required></select>
                            <span class="input-group-addon">
                                <span id="cmdpreview" style="cursor:pointer">Cetak <i class="fa fa-print"></i></span>
                            </span>
                        
                        </div>
                    </div>
                </div>
            </div>

            <div id="grid1" class="full-height" style="height: calc(100% - 50px);"></div> 
        </div>
        <div class="modal fade" style="position:absolute;" id="wrap-preview-detail-d" role="dialog" data-backdrop="false" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="wm-title">Preview Detail SPP</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Faktur</label>
                                    <input type="text" name="cFaktur" id="faktur" class="form-control" placeholder="faktur" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Gudang</label>
                                    <input type="text" name="gudang" id="gudang" readonly = true class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tgl</label>
                                    <input type="text" name="tgl" id="tgl" readonly = true class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="grid2" style="height:250px"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <br/>
                                    <button type = "button" class="btn btn-success" id="cmdCetakLaporanDetail">Print Report</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    <?=cekbosjs();?>


    //grid daftar pembelian
    bos.rptpr.grid1_data    = null ;
    bos.rptpr.grid1_loaddata= function(){

        mdl_gudang.vargr.selection = bos.rptpr.obj.find('#skd_gudang').val();

		this.grid1_data 		= {
			"tglawal"	   : this.obj.find("#tglawal").val(),
            "tglakhir"	   : this.obj.find("#tglakhir").val(),
			'skd_gudang':JSON.stringify(mdl_gudang.vargr.selection)
		} ;
    }

    bos.rptpr.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.rptpr.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'gudang', caption: 'Gudang', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'cmdPreview', caption: ' ', size: '120px', sortable: false }
            ]
        });
    }

    bos.rptpr.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }

    bos.rptpr.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.rptpr.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptpr.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rptpr.grid1_reloadData	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }


    bos.rptpr.cmdpreviewdetail	= function(faktur){
        bjs.ajax(this.url + '/PreviewDetail', 'faktur=' + faktur);
    }

    bos.rptpr.cmdrefresh          = bos.rptpr.obj.find("#cmdrefresh") ;
    bos.rptpr.cmdpreview     = bos.rptpr.obj.find("#cmdpreview")
    bos.rptpr.cmdCetakLaporanDetail     = bos.rptpr.obj.find("#cmdCetakLaporanDetail") ;
    bos.rptpr.initfunc    = function(){
        this.obj.find("#cmdpreview").on("click", function(e){
            e.preventDefault() ;
            bos.rptpr.initreportTotal(0,0) ;
        }) ;

        this.obj.find("#cmdrefresh").on("click", function(e){
            e.preventDefault() ;
            bos.rptpr.grid1_reloadData() ;
        }) ;
        this.obj.find("#cmdCetakLaporanDetail").on("click",function(e){
            e.preventDefault() ;
            bos.rptpr.initReportDetailPembelian(0,0) ;
        });

        this.obj.find('#cmdgudang').on('click', function(){
            mdl_gudang.vargr.selection = bos.rptpr.obj.find('#skd_gudang').val();
            mdl_gudang.open(function(r){
                r = JSON.parse(r);
                bos.rptpr.gudang = [];
                $.each(r, function(i, v){
                    bos.rptpr.gudang.push({'id':v.kode,'text':v.keterangan})                   
                });
                bos.rptpr.obj.find('#skd_gudang').sval(bos.rptpr.gudang);
            });
        
            
        });
    }

    bos.rptpr.initReportDetailPembelian  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.rptpr.loadmodalpreview      = function(l){
        this.obj.find("#wrap-preview-detail-d").modal(l) ;
    }

    bos.rptpr.initcomp		= function(){
        bjs.initselect({
            class : "#" + this.id + " .s2",
            multi : true
        }) ;

        bjs.initselect({
            class : "#" + this.id + " .select",
            clear : false
        }) ;

        this.grid1_loaddata() ;
        this.grid1_load() ;
        this.grid2_load() ;       

        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag

    }

    bos.rptpr.initcallback	= function(){
        this.obj.on('remove', function(){
            bos.rptpr.grid1_destroy() ;
            bos.rptpr.grid2_destroy() ;
            bos.rptpr.grid2_destroy() ;
        }) ;
    }

    bos.rptpr.setPreview      = function(cFaktur){
        bos.rptpr.initreport(0,0) ;
    }

    bos.rptpr.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.rptpr.initreportTotal  = function(s,e){
        mdl_gudang.vargr.selection = bos.rptpr.obj.find('#skd_gudang').val();

        bjs.ajax(this.base_url+ '/initreportTotal', bjs.getdataform(this.obj.find("form"))+"&skd_gudang="+JSON.stringify(mdl_gudang.vargr.selection),bos.rptpr.cmdpreview) ;
    }

    bos.rptpr.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }

    bos.rptpr.openreporttotal  = function(){
        bjs_os.form_report( this.base_url + '/showreporttotal' ) ;
    }

    bos.rptpr.grid2_data    = null ;
    bos.rptpr.grid2_loaddata= function(){
        this.grid2_data         = {} ;
    }


    bos.rptpr.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name    : this.id + '_grid2',
            show: {
                footer      : false,
                toolbar     : false,
                toolbarColumns  : false
            },
            multiSearch     : false,
            columns: [
                { field: 'no', caption: 'No', size: '30px', sortable: false, style:'text-align:center'},
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false ,style:'text-align:center'},
                { field: 'keterangan', caption: 'Nama Stock', size: '150px', sortable: false },
                { field: 'qty', render: 'int', caption: 'Qty', size: '40px', sortable: false, style:'text-align:center'},
                { field: 'satuan', caption: 'Satuan', size: '80px', sortable: false, style:'text-align:center'}
            ]
        });
    }

    bos.rptpr.grid2_reload     = function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.rptpr.grid2_destroy    = function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.rptpr.grid2_render     = function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.rptpr.grid2_reloaddata = function(){
        this.grid2_reload() ;
    }

    $(function(){
        bos.rptpr.initcomp() ;
        bos.rptpr.initcallback() ;
        bos.rptpr.initfunc() ;
    });
</script>