<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Laporan Kartu Pinjaman Karyawan</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptkartupinjamankaryawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div>
<div class="body">
    <form novalidate>         
        <div class="bodyfix scrollme" style="height:100%"> 
            <table class="osxtable form" border="0">
                <tr>
                    <td>
                        <table width = "100%">
                            <tr>
                                <td width="14%"><label for="nopinjaman">No Pinjaman</label> </td>
                                <td width="1%">:</td>
                                <td >
                                    <div class="input-group">
                                        <input readonly type="text" id="nopinjaman" name="nopinjaman" class="form-control" placeholder="No Pinjaman">
                                        <span class="input-group-btn">
                                            <button class="form-control btn btn-info" type="button" id="cmdnopinjaman"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                </td>
                            
                                <td width="14%"><label for="nip">NIP</label> </td>
                                <td width="1%">:</td>
                                <td >
                                    <input readonly type="text" id="nip" name="nip" class="form-control" placeholder="NIP">
                                </td>
                            </tr>
                            <tr>
                                <td width="14%"><label for="namakaryawan">Nama Karyawan</label> </td>
                                <td width="1%">:</td>
                                <td >
                                    <input type="text" id="namakaryawan" readonly name="namakaryawan" class="form-control" placeholder="Nama Karyawan">
                                </td>
                                
                                <td width="14%"><label for="plafond">Plafond</label> </td>
                                <td width="1%">:</td>
                                <td>
                                    <input type="text" name="plafond" id="plafond" readonly
                                        class="form-control number" value="0" required> 
                                </td>
                                
                            </tr>
                            <tr>
                                <td width="14%"><label for="bagian">Bagian</label> </td>
                                <td width="1%">:</td>
                                <td >
                                    <input type="text" id="bagian" readonly name="bagian" class="form-control" placeholder="Bagian">
                                </td>

                                <td width="14%"><label for="lama">Lama (Bln)</label> </td>
                                <td width="1%">:</td>
                                <td>
                                    <input type="text" name="lama" id="lama" readonly 
                                        class="form-control number" value="0" required> 
                                </td>                        
                                
                            </tr>
                            <tr>
                                
                                <td width="14%"><label for="jabatan">Jabatan</label> </td>
                                <td width="1%">:</td>
                                <td >
                                    <input type="text" id="jabatan" readonly name="jabatan" class="form-control" placeholder="Jabatan">
                                </td>   
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width = "100%">
                            <tr>
                                <td width="80px"><label for="tgl">Tgl</label> </td>
                                <td width="20px">:</td>
                                <td width="100px"> 
                                    <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                </td>
                                <td width="40px">s/d</td>
                                <td width="100px">
                                    <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                </td> 

                                <td width="100px">
                                    <button class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                </td>
                                <td width="100px">
                                    <select name="export" id="export" class="form-control select" style="width:100%"
                                        data-sf="load_export" data-placeholder="PDF" required></select>
                                </td>
                                <td width="100px">	
                                    <button type= "button" class="btn btn-primary pull-right" id="cmdview">Preview</button>
                                </td>
                            </tr> 
                        </table>
                    </td>
                </tr>                
                <tr><td><hr class="no-margin no-padding"></td></tr>
            </table> 
            <div class="row" style="height: calc(100% - 200px);"> 
                <div class="col-sm-12 full-height">
                    <div id="grid1" class="full-height"></div>
                </div>  
            </div> 
        </div>
    </form>
</div>
<div class="modal fade" id="wrap-pencariannopinjaman-d" role="dialog" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="wm-title">Daftar Pinjaman Karyawan</h4>
            </div>
            <div class="modal-body">
                <div id="grid2" style="height:250px"></div>
            </div>
            <div class="modal-footer">
                *Pilih No Pinjaman
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>


    //grid kartu
    bos.rptkartupinjamankaryawan.grid1_data    = null ;
    bos.rptkartupinjamankaryawan.grid1_loaddata= function(){
        var tglAwal = bos.rptkartupinjamankaryawan.obj.find("#tglawal").val();
        var tglAkhir = bos.rptkartupinjamankaryawan.obj.find("#tglakhir").val();
        var nopinjaman = bos.rptkartupinjamankaryawan.obj.find("#nopinjaman").val();
        this.grid1_data 		= {'tglAwal':tglAwal,'tglAkhir':tglAkhir,'nopinjaman':nopinjaman} ;
    }

    bos.rptkartupinjamankaryawan.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.rptkartupinjamankaryawan.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers:true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false , style:"text-align:left"},
                { field: 'debet', render: 'float:2' ,caption: 'Debet', size: '100px', sortable: false, style:"text-align:right" },
                { field: 'kredit', render: 'float:2' ,caption: 'Kredit', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'saldo', render: 'float:2' ,caption: 'Saldo', size: '100px', sortable: false, style:"text-align:right"}
            ]
        });
    }

    bos.rptkartupinjamankaryawan.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }

    bos.rptkartupinjamankaryawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.rptkartupinjamankaryawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptkartupinjamankaryawan.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rptkartupinjamankaryawan.grid1_reloadData	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //grid2 daftarstock
    bos.rptkartupinjamankaryawan.grid2_data    = null ;
    bos.rptkartupinjamankaryawan.grid2_loaddata= function(){
        this.grid2_data 		= {} ;
    }

    bos.rptkartupinjamankaryawan.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name	: this.id + '_grid2',
multiSelect : false,
            limit 	: 100 ,
            url 	: bos.rptkartupinjamankaryawan.base_url + "/loadgrid2",
            postData: this.grid3_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'nopinjaman', caption: 'No Pinjaman', size: '120px', sortable: false,frozen:true},
                { field: 'nip', caption: 'NIP', size: '100px', sortable: false,frozen:true},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false ,frozen:true},
                { field: 'jabatan', caption: 'Jabatan', size: '100px', sortable: false },
                { field: 'bagian', caption: 'Bagian', size: '100px', sortable: false },
                { field: 'plafond', caption: 'Plafond', size: '100px', sortable: false, render:'float:2'},
                { field: 'lama', caption: 'Lama', size: '100px', sortable: false, render:'float:2'},
                { field: 'cmdpilih', caption: ' ', size: '80px', sortable: false }
            ]
        });
    }

    bos.rptkartupinjamankaryawan.grid2_setdata	= function(){
        w2ui[this.id + '_grid2'].postData 	= this.grid2_data ;
    }
    bos.rptkartupinjamankaryawan.grid2_reload		= function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.rptkartupinjamankaryawan.grid2_destroy 	= function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.rptkartupinjamankaryawan.grid2_render 	= function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.rptkartupinjamankaryawan.grid2_reloaddata	= function(){
        this.grid2_loaddata() ;
        this.grid2_setdata() ;
        this.grid2_reload() ;
    }

    bos.rptkartupinjamankaryawan.cmdpilih 		= function(nopinjaman){
        bjs.ajax(this.url + '/pilihnopinjaman','nopinjaman=' + nopinjaman);
    }
	
	bos.rptkartupinjamankaryawan.obj.find("#cmdview").on("click", function(){
		bos.rptkartupinjamankaryawan.initreport();
	}) ;

    bos.rptkartupinjamankaryawan.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }
    bos.rptkartupinjamankaryawan.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }
	
    bos.rptkartupinjamankaryawan.cmdrefresh          = bos.rptkartupinjamankaryawan.obj.find("#cmdrefresh") ;
    bos.rptkartupinjamankaryawan.initfunc    = function(){
        this.obj.find("#cmdrefresh").on("click", function(e){
            e.preventDefault() ;
            bos.rptkartupinjamankaryawan.grid1_reloadData() ;
        }) ;

        this.obj.find("#cmdnopinjaman").on("click", function(e){
            bos.rptkartupinjamankaryawan.loadmodelnopinjaman("show");
            bos.rptkartupinjamankaryawan.grid2_reloaddata() ;
        }) ;


    }
    bos.rptkartupinjamankaryawan.initcomp		= function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;
        this.grid2_load() ;
        bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;
		this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.rptkartupinjamankaryawan.loadmodelnopinjaman      = function(l){
        this.obj.find("#wrap-pencariannopinjaman-d").modal(l) ;
    }
    bos.rptkartupinjamankaryawan.initcallback	= function(){
        this.obj.on('remove', function(){
            bos.rptkartupinjamankaryawan.grid1_destroy() ;
            bos.rptkartupinjamankaryawan.grid2_destroy() ;
        }) ;
    }

    $(function(){
        bos.rptkartupinjamankaryawan.initcomp() ;
        bos.rptkartupinjamankaryawan.initcallback() ;
        bos.rptkartupinjamankaryawan.initfunc() ;
    });
</script>