<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Laporan Perintah Produksi</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptperintahproduksi.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div>
<div class="body">
    <form novalidate>         
        <div class="bodyfix scrollme" style="height:100%"> 
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Antara Tanggal</label>
                        <div class="row">
                            <div class="col-md-6">
                                <input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                            </div>
                            <div class="col-md-6">
                                <input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Cabang</label>
                        <div class="input-group">
                            <select name="skd_cabang" id="skd_cabang" class="form-control s2" style="width:100%"
                                                    data-placeholder="Pilih cabang" data-sf="load_cabang"></select>
                            <span class="input-group-addon">
                                <span id="cmdcabang" style="cursor:pointer"><i class="fa fa-search"></i></span>
                            </span>
                        
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group full-width">
                        <label>&nbsp;</label>
                        <div class="input-group full-width">
                            <button type="button" class="btn btn-primary full-width" id="cmdrefresh">Refresh <i class="fa fa-refresh"></i></button>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Cetak</label>
                        <div class="input-group">
                            <select name="export" id="export" class="form-control select" style="width:100%"
                                data-sf="load_export" data-placeholder="PDF" required></select>
                            <span class="input-group-addon">
                                <span id="cmdpreview" style="cursor:pointer">Cetak <i class="fa fa-print"></i></span>
                            </span>
                        
                        </div>
                    </div>
                </div>
            </div>

            <div id="grid1" class="full-height" style="height: calc(100% - 50px);"></div> 

        </div>
        <div class="modal fade" style="position:absolute;" id="wrap-preview-detail-d" role="dialog" data-backdrop="false" data-keyboard="false">
            <div class="modal-dialog  modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="wm-title">Preview Detail Perintah Produksi</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Faktur</label>
                                        <input type="text" name="faktur" id="faktur" class="form-control" placeholder="faktur" readonly = true>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Tgl</label>
                                        <input type="text" name="tgl" id="tgl" readonly = true class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Perbaikan</label>
                                        <input type="text" name="perbaikan" id="perbaikan" readonly = true class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Karu</label>
                                        <input type="text" name="petugas" id="petugas" readonly = true class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="grid2" style="height:250px"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Stock</label>
                                        <input type="text" name="stock" id="stock" class="form-control" placeholder="Kode Stock" readonly = true>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nama Stock</label>
                                        <input type="text" name="namastock" id="namastock" class="form-control" placeholder="Nama Stock" readonly = true>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Qty</label>
                                        <input maxlength="10" type="text" name="qty" id="qty" class="form-control number" value="0" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Satuan</label>
                                        <input type="text" name="satuan" id="satuan" class="form-control" placeholder="Satuan" readonly = true>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>BB</label>
                                        <input maxlength="10" type="text" name="bb" id="bb" class="form-control number" value="0" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>BTKL</label>
                                        <input maxlength="10" type="text" name="btkl" id="btkl" class="form-control number" value="0" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>BOP</label>
                                        <input maxlength="10" type="text" name="bop" id="bop" class="form-control number" value="0" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>HP. Pebaikan</label>
                                        <input maxlength="10" type="text" name="hpperbaikan" id="hpperbaikan" class="form-control number" value="0" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Jml. Perbaikan</label>
                                        <input maxlength="10" type="text" name="jmlperbaikan" id="jmlperbaikan" class="form-control number" value="0" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>HP</label>
                                        <input maxlength="10" type="text" name="hp" id="hp" class="form-control number" value="0" readonly>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Jumlah</label>
                                        <input maxlength="10" type="text" name="jumlah" id="jumlah" class="form-control number" value="0" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="grid3" style="height:500px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button type = "button" class="btn btn-success" id="cmdCetakLaporanDetail">Print Report</button>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button type = "button" class="btn btn-primary" id="cmdcetakdm">Print Dot Matrix</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    <?=cekbosjs();?>


    //grid daftar pembelian
    bos.rptperintahproduksi.grid1_data    = null ;
    bos.rptperintahproduksi.grid1_loaddata= function(){
        mdl_cabang.vargr.selection = bos.rptperintahproduksi.obj.find('#skd_cabang').val();

        this.grid1_data 		= {
			"tglawal"	   : this.obj.find("#tglawal").val(),
            "tglakhir"	   : this.obj.find("#tglakhir").val(),
			'skd_cabang'   : JSON.stringify(mdl_cabang.vargr.selection)
		} ;
    }

    bos.rptperintahproduksi.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.rptperintahproduksi.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: true,
                toolbarColumns  : false,
                lineNumbers : true
            },
            multiSearch		: false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'regu', caption: 'Regu', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'karu', caption: 'Karu', size: '150px', sortable: false , style:"text-align:left"},
                { field: 'bb', render: 'int' ,caption: 'BB', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'btkl', render: 'int' ,caption: 'BTKL', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'bop', render: 'int' ,caption: 'BOP', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'hargapokok', render: 'int' ,caption: 'Harga Pokok', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'cabang', caption: 'Cabang', size: '80px', sortable: false},
                { field: 'cmdpreview', caption: ' ', size: '120px', sortable: false }
            ]
        });
    }

    bos.rptperintahproduksi.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }

    bos.rptperintahproduksi.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.rptperintahproduksi.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptperintahproduksi.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rptperintahproduksi.grid1_reloadData	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }


    bos.rptperintahproduksi.cmdpreviewdetail	= function(faktur){
        bjs.ajax(this.url + '/PreviewDetail', 'faktur=' + faktur);
    }

    bos.rptperintahproduksi.cmdcetakdm 	= function(){
        if(confirm("Cetak data?")){
            var faktur = bos.rptperintahproduksi.obj.find("#faktur").val();
            bjs.ajax(this.url + '/cetakdm', 'faktur=' + faktur);
        }
    }

    bos.rptperintahproduksi.cmdrefresh          = bos.rptperintahproduksi.obj.find("#cmdrefresh") ;
    bos.rptperintahproduksi.cmdpreview     = bos.rptperintahproduksi.obj.find("#cmdpreview")
    bos.rptperintahproduksi.cmdCetakLaporanDetail     = bos.rptperintahproduksi.obj.find("#cmdCetakLaporanDetail") ;
    bos.rptperintahproduksi.initfunc    = function(){
        this.obj.find("#cmdpreview").on("click", function(e){
            e.preventDefault() ;
            bos.rptperintahproduksi.initreportTotal(0,0) ;
        }) ;

        this.obj.find("#cmdrefresh").on("click", function(e){
            e.preventDefault() ;
            bos.rptperintahproduksi.grid1_reloadData() ;
        }) ;
        this.obj.find("#cmdCetakLaporanDetail").on("click",function(e){
            e.preventDefault() ;
            bos.rptperintahproduksi.initReportDetailPP(0,0) ;
        });

        this.obj.find("#cmdcetakdm").on("click", function(e){
            bos.rptperintahproduksi.cmdcetakdm();
		});

        this.obj.find('#cmdcabang').on('click', function(){
            mdl_cabang.vargr.selection = bos.rptperintahproduksi.obj.find('#skd_cabang').val();
            mdl_cabang.open(function(r){
                r = JSON.parse(r);
                bos.rptperintahproduksi.cabang = [];
                $.each(r, function(i, v){
                    bos.rptperintahproduksi.cabang.push({'id':v.kode,'text':v.keterangan})                   
                });
                bos.rptperintahproduksi.obj.find('#skd_cabang').sval(bos.rptperintahproduksi.cabang);
            });
        
            
        });
    }

    bos.rptperintahproduksi.initReportDetailPP  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.rptperintahproduksi.loadmodalpreview      = function(l){
        this.obj.find("#wrap-preview-detail-d").modal(l) ;
    }

    bos.rptperintahproduksi.initcomp		= function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;
        this.grid2_load() ;
        this.grid3_load() ;

        bjs.initselect({
            class : "#" + this.id + " .s2",
            multi : true
        }) ;

        bjs.initselect({
            class : "#" + this.id + " .select",
            clear :false
        }) ;

        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;

        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.rptperintahproduksi.initcallback	= function(){
        this.obj.on('remove', function(){
            bos.rptperintahproduksi.grid1_destroy() ;
            bos.rptperintahproduksi.grid2_destroy() ;
            bos.rptperintahproduksi.grid3_destroy() ;
        }) ;
    }

    bos.rptperintahproduksi.setPreview      = function(cFaktur){
        bos.rptperintahproduksi.initreport(0,0) ;
    }

    bos.rptperintahproduksi.initreport  = function(s,e){
        mdl_cabang.vargr.selection = bos.rptperintahproduksi.obj.find('#skd_cabang').val();

        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.rptperintahproduksi.initreportTotal  = function(s,e){
        mdl_cabang.vargr.selection = bos.rptperintahproduksi.obj.find('#skd_cabang').val();

        bjs.ajax(this.base_url+ '/initreportTotal', bjs.getdataform(this.obj.find("form"))+"&skd_cabang="+JSON.stringify(mdl_cabang.vargr.selection)) ;
    }

    bos.rptperintahproduksi.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }

    bos.rptperintahproduksi.openreporttotal  = function(){
        bjs_os.form_report( this.base_url + '/showreporttotal' ) ;
    }

    bos.rptperintahproduksi.grid2_data    = null ;
    bos.rptperintahproduksi.grid2_loaddata= function(){
        this.grid2_data         = {} ;
    }


    bos.rptperintahproduksi.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name    : this.id + '_grid2',
            show: {
                footer      : false,
                toolbar     : false,
                toolbarColumns  : false
            },
            multiSearch     : false,
            columns: [
                { field: 'no', caption: 'No', size: '30px', sortable: false, style:'text-align:center'},
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false ,style:'text-align:center'},
                { field: 'keterangan', caption: 'Nama Stock', size: '150px', sortable: false },
                { field: 'qty', render: 'float:2', caption: 'Qty', size: '40px', sortable: false, style:'text-align:center'},
                { field: 'satuan', caption: 'Satuan', size: '80px', sortable: false, style:'text-align:left'},
                { field: 'hp', render: 'float:2', caption: 'HP', size: '100px', sortable: false, style:'text-align:right' },
                { field: 'jmlhp', render: 'float:2', caption: 'Jml HP', size: '100px', sortable: false, style:'text-align:right' },
                { field: 'totqty', render: 'float:2', caption: 'Tot Qty', size: '100px', sortable: false, style:'text-align:right'}
            ]
        });
    }

    bos.rptperintahproduksi.grid2_reload     = function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.rptperintahproduksi.grid2_destroy    = function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.rptperintahproduksi.grid2_render     = function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.rptperintahproduksi.grid2_reloaddata = function(){
        this.grid2_reload() ;
    }

    //riwayat
    bos.rptperintahproduksi.grid3_load    = function(){
        this.obj.find("#grid3").w2grid({
            name    : this.id + '_grid3',
            header : "Riwayat Produksi",
            show: {
                header      : true,
                footer      : false,
                toolbar     : false,
                toolbarColumns  : false,                
                lineNumbers:true
            },
            multiSearch     : false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false ,style:'text-align:center',frozen:true},
                { field: 'tgl', caption: 'Tgl', size: '80px', sortable: false ,style:'text-align:center'},
                { field: 'kode', caption: 'Kode', size: '80px', sortable: false ,style:'text-align:center'},
                { field: 'keterangan', caption: 'Nama Stock', size: '150px', sortable: false},
                { field: 'qty', render: 'float:2', caption: 'Qty', size: '100px', sortable: false, style:'text-align:right'},
                { field: 'satuan', caption: 'Satuan', size: '80px', sortable: false, style:'text-align:left'},
                { field: 'hp', render: 'float:2', caption: 'HP', size: '100px', sortable: false, style:'text-align:right' },
                { field: 'jmlhp', render: 'float:2', caption: 'Jml HP', size: '100px', sortable: false, style:'text-align:right' }
            ]
        });
    }

    bos.rptperintahproduksi.grid3_reload     = function(){
        w2ui[this.id + '_grid3'].reload() ;
    }
    bos.rptperintahproduksi.grid3_destroy    = function(){
        if(w2ui[this.id + '_grid3'] !== undefined){
            w2ui[this.id + '_grid3'].destroy() ;
        }
    }

    bos.rptperintahproduksi.grid3_render     = function(){
        this.obj.find("#grid3").w2render(this.id + '_grid3') ;
    }

    bos.rptperintahproduksi.grid3_reloaddata = function(){
        this.grid3_reload() ;
    }

    $(function(){
        bos.rptperintahproduksi.initcomp() ;
        bos.rptperintahproduksi.initcallback() ;
        bos.rptperintahproduksi.initfunc() ;
    });
</script>