
<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Laporan Kartu Stock PO</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptpokartu.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<form novalidate>
<div class="body">
    
        <div class="bodyfix scrollme" style="height:100%">
            <table width="100%">
                <tr>
                    <td width="100%" class="osxtable form">
                        <table>
                            <tr>
                                <td width="14%"><label for="faktur">Faktur</label> </td>
                                <td width="1%">:</td>
                                <td width="20%">
                                    <input type="text" id="faktur" name="faktur" class="form-control" placeholder="Faktur PO">
                                </td>
                                <td width="10%"><label for="tgl"> Sampai Tgl</label> </td>
                                <td width="1%">:</td>
                                <td width="10%">
                                    <input style="width:80px" type="text" class="form-control date" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?>> 
                                </td>
                                <td width="10%">
                                    <button class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                </td>
                                <td >
                                    <button class="btn btn-primary pull-left" id="cmdpreview">Preview</button>
                                </td>
                            </tr>
                        </table>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td width="100%" class="osxtable form">
                        <table>
                            <tr>
                                <td width="14%"><label for="tglpo">Tanggal PO</label> </td>
                                <td width="1%">:</td>
                                <td >
                                    <input readonly style="width:80px" type="text" class="form-control date" id="tglpo" name="tglpo" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                                </td>
                                <td width="14%"><label for="supplier">Supplier</label> </td>
                                <td width="1%">:</td>
                                <td >
                                    <input type="text" id="supplier" name="supplier" class="form-control" placeholder="Supplier" readonly>
                                </td>
                            </tr>
                            <tr>
                            <td width="14%"><label for="gudang">Gudang</label> </td>
                                <td width="1%">:</td>
                                <td >
                                    <input readonly type="text" id="gudang" name="gudang" class="form-control" placeholder="Gudang">
                                </td>
                                <td width="14%"><label for="fktpr">Faktur SPP</label> </td>
                                <td width="1%">:</td>
                                <td >
                                    <input type="text" id="fktpr" name="fktpr" class="form-control" placeholder="Faktur SPP" readonly>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height = "410px" >
                        <div id="grid1" class="full-height"></div>
                    </td>
                </tr>
            </table>
        </div>

        
    
</div>
<div class="modal fade" id="wrap-detailpostockkartu-d" role="dialog" data-backdrop="false" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="wm-title">Detail Kartu PO Stock</h4>
                    </div>                    
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Stock</label>
                                    <input type="text" name="stock" id="stock" class="form-control" placeholder="stock" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nama Stock</label>
                                    <input type="text" name="namastock" id="namastock" class="form-control" placeholder="namastock" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Satuan</label>
                                    <input type="text" name="satuan" id="satuan" class="form-control" placeholder="satuan" readonly = true>
                                </div>
                            </div>
                        </div>
                        <div id="grid2" style="height:250px"></div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <br/>
                                    <button type = "button" class="btn btn-success" id="cmdcetakdetail">Preview</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
<script type="text/javascript">
    <?=cekbosjs();?>


    //grid daftar pembelian
    bos.rptpokartu.grid1_data    = null ;
    bos.rptpokartu.grid1_loaddata= function(){
        var tgl = bos.rptpokartu.obj.find("#tgl").val();
        var faktur = bos.rptpokartu.obj.find("#faktur").val();
        this.grid1_data 		= {'faktur':faktur,'tgl':tgl} ;
    }

    bos.rptpokartu.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.rptpokartu.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'no', caption: 'No', size: '50px', sortable: false, style:"text-align:right"},
                { field: 'stock', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center"},
                { field: 'namastock', caption: 'Nama Stock', size: '200px', sortable: false , style:"text-align:left"},
                { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false , style:"text-align:left"},
                { field: 'qtypo', render: 'float:2' ,caption: 'Qty PO', size: '100px', sortable: false, style:"text-align:right" },
                { field: 'qtypb', render: 'float:2' ,caption: 'Qty Pembelian', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'saldo', render: 'float:2' ,caption: 'Saldo', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'cmddetail', caption: ' ', size: '100px', sortable: false }
            ]
        });
    }

    bos.rptpokartu.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }

    bos.rptpokartu.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.rptpokartu.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptpokartu.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rptpokartu.grid1_reloadData	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    bos.rptpokartu.obj.find("#cmdview").on("click", function(){
		bos.rptpokartu.initreport();
	}) ;

    bos.rptpokartu.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.rptpokartu.initreportdetil  = function(s,e){
        bjs.ajax(this.base_url+ '/initreportdetil', bjs.getdataform(this.obj.find("form"))) ;
    }

    bos.rptpokartu.showreport = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }

    bos.rptpokartu.showreportdetil = function(){
        bjs_os.form_report( this.base_url + '/showreportdetil' ) ;
    }

    bos.rptpokartu.cmdrefresh          = bos.rptpokartu.obj.find("#cmdrefresh") ;
    bos.rptpokartu.initfunc    = function(){
        this.obj.find("#cmdrefresh").on("click", function(e){
            e.preventDefault() ;
            bos.rptpokartu.seekfaktur() ;
        }) ;
        this.obj.find("#cmdpreview").on("click", function(e){
            e.preventDefault() ;
            bos.rptpokartu.initreport();
        }) ;
        this.obj.find("#cmdcetakdetail").on("click", function(e){
            e.preventDefault() ;
            bos.rptpokartu.initreportdetil();
        }) ;

    }
    bos.rptpokartu.seekfaktur  = function(){
        bjs.ajax(this.base_url+ '/seekfaktur', bjs.getdataform(this.obj.find("form"))) ;
    }
    bos.rptpokartu.initcomp		= function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;
        this.grid2_load() ;
        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drags
    }
    
    bos.rptpokartu.initcallback	= function(){
        this.obj.on('remove', function(){
            bos.rptpokartu.grid1_destroy() ;
            bos.rptpokartu.grid2_destroy() ;
        }) ;
    }

    bos.rptpokartu.loadmodaldetail      = function(l){
        this.obj.find("#wrap-detailpostockkartu-d").modal(l) ;
    }

    bos.rptpokartu.cmddetail	= function(faktur,stock,tgl){
        //alert(faktur);
        bjs.ajax(this.url + '/loaddetail', 'faktur=' + faktur + '&stock=' + stock + '&tgl=' + tgl);
    }

    bos.rptpokartu.grid2_data    = null ;
    bos.rptpokartu.grid2_loaddata= function(){
        this.grid2_data         = {} ;
    }


    bos.rptpokartu.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name    : this.id + '_grid2',
            show: {
                footer      : false,
                toolbar     : false,
                toolbarColumns  : false
            },
            multiSearch     : false,
            columns: [
                { field: 'no', caption: 'No', size: '30px', sortable: false, style:'text-align:center'},
                { field: 'faktur', caption: 'Faktur', size: '100px', sortable: false ,style:'text-align:center'},
                { field: 'tgl', caption: 'Kode', size: '100px', sortable: false ,style:'text-align:center'},
                { field: 'keterangan', caption: 'Nama Stock', size: '150px', sortable: false },
                { field: 'debet', render: 'int', caption: 'PO', size: '100px', sortable: false, style:'text-align:right' },
                { field: 'kredit', render: 'int', caption: 'Pembelian', size: '100px', sortable: false, style:'text-align:right' },
                { field: 'saldo', render: 'int', caption: 'Sisa', size: '100px', sortable: false, style:'text-align:right' },
                { field: 'username', caption: 'Username', size: '100px', sortable: false },
                { field: 'datetime', caption: 'Datetime', size: '100px', sortable: false,style:'text-align:center' }
            ]
        });
    }

    bos.rptpokartu.grid2_reload     = function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.rptpokartu.grid2_destroy    = function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.rptpokartu.grid2_render     = function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.rptpokartu.grid2_reloaddata = function(){
        this.grid2_reload() ;
    }

    $(function(){
        bjs.initenter($("form")) ;
        bos.rptpokartu.initcomp() ;
        bos.rptpokartu.initcallback() ;
        bos.rptpokartu.initfunc() ;
    });
</script>