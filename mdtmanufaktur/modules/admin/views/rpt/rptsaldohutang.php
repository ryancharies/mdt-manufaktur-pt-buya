<style media="screen">
    #bos-form-rptjurnal-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
    #bos-form-rptjurnal-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Laporan Saldo Hutang</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr> 
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptsaldohutang.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td> 
        </tr>
    </table> 
</div>
<div class="body">
    <form novalidate>         
        <div class="bodyfix scrollme" style="height:100%"> 
            <table class="osxtable form" border="0">

                <tr>
                    <td width="80px"><label for="tgl">Tgl</label> </td>
                    <td width="20px">:</td>
                    <td width="100px"> 
                        <input style="width:80px" type="text" class="form-control date" id="tgl" name="tgl" required value=<?=date("d-m-Y")?> <?=date_set()?>>
                    </td>

                    <td width="285px">
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="jenis" class="jenis" value="" checked>
                                Semua
                            </label>
                            &nbsp;&nbsp;
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="jenis" class="jenis" value="H">
                                Hutang Dagang
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="jenis" class="jenis" value="A">
                                Aset
                            </label>
                        </div>
                    </td>

                    <td width="100px">
                        <button class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                    </td>
					<td width="100px">	
						<button type= "button" class="btn btn-primary pull-right" id="cmdview">Preview</button>
					</td>
                    <td></td>
                </tr>  
                <tr><td colspan="6"><hr class="no-margin no-padding"></td></tr>
            </table> 
            <div class="row" style="height: calc(100% - 50px);"> 
                <div class="col-sm-12 full-height">
                    <div id="grid1" class="full-height"></div>
                </div>  
            </div> 
        </div>

        <div class="modal fade" style="position:absolute;padding-top:40px;" id="wrap-preview-detail-d" role="dialog" data-backdrop="false" data-keyboard="false">
            <div class="modal-dialog" style="width:100%;">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="wm-title">Preview Detail Saldo Hutang</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Supplier</label>
                                    <input type="text" name="supplier" id="supplier" class="form-control" placeholder="Supplier" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nama Supplier</label>
                                    <input type="text" name="namasupplier" id="namasupplier" class="form-control" placeholder="Nama Supplier" readonly = true>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat" readonly = true>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="grid2" style="height:250px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
						<button type= "button" class="btn btn-primary pull-right" id="cmdviewd">Preview</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>


    //grid daftar hutang
    bos.rptsaldohutang.grid1_data    = null ;
    bos.rptsaldohutang.grid1_loaddata= function(){
        var tgl = bos.rptsaldohutang.obj.find("#tgl").val();
        var jenis = this.obj.find('input:radio[name=jenis]:checked').val();
        this.grid1_data 		= {'tgl':tgl,'jenis':jenis} ;
    }

    bos.rptsaldohutang.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            limit 	: 100 ,
            url 	: bos.rptsaldohutang.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: true,
                toolbar		: false,
                toolbarColumns  : false
            },
            multiSearch		: false,
            columns: [
                { field: 'no', caption: 'No', size: '50px', sortable: false, style:"text-align:right"},
                { field: 'kode', caption: 'Kode', size: '100px', sortable: false, style:"text-align:center"},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false , style:"text-align:left"},
                { field: 'alamat', caption: 'Alamat', size: '200px', sortable: false , style:"text-align:left"},
                { field: 'saldo', render: 'float:2' ,caption: 'Saldo', size: '100px', sortable: false, style:"text-align:right"},
                { field: 'cmddetail', caption: '', size: '100px', sortable: false, style:"text-align:center"},
                { field: 'cmddetail2', caption: '', size: '100px', sortable: false, style:"text-align:center"}
            ]
        });
    }

    bos.rptsaldohutang.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }

    bos.rptsaldohutang.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }

    bos.rptsaldohutang.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptsaldohutang.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }

    bos.rptsaldohutang.grid1_reloadData	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }

    //detail
    bos.rptsaldohutang.grid2_data    = null ;
    bos.rptsaldohutang.grid2_loaddata= function(){
        this.grid2_data         = {} ;
    }


    bos.rptsaldohutang.grid2_load    = function(){
        this.obj.find("#grid2").w2grid({
            name    : this.id + '_grid2',
            show: {
                footer      : false,
                toolbar     : false,
                toolbarColumns  : false,
                lineNumbers: true
            },
            multiSearch     : false,
            columns: [
                { field: 'faktur', caption: 'Faktur', size: '120px', sortable: false, style:"text-align:center",frozen:true},
                { field: 'tgl', caption: 'Tgl', size: '100px', sortable: false , style:"text-align:center",frozen:true},
                { field: 'jthtmp', caption: 'Jth Tmp', size: '100px', sortable: false , style:"text-align:center"},
                { field: 'fktpo', caption: 'PO', size: '120px', sortable: false, style:"text-align:center",frozen:true},
                { field: 'subtotal', render: 'float:2' ,caption: 'Subtotal', size: '100px', sortable: false},
                { field: 'diskon', render: 'float:2' ,caption: 'Diskon', size: '100px', sortable: false},
                { field: 'ppn', render: 'float:2' ,caption: 'PPn', size: '100px', sortable: false},
                { field: 'total', render: 'float:2' ,caption: 'Total', size: '100px', sortable: false},
                { field: 'pembayaran', render: 'float:2' ,caption: 'Pembayaran', size: '100px', sortable: false},
                { field: 'sisa', render: 'float:2' ,caption: 'Saldo', size: '100px', sortable: false}
            ]
        });
    }

    bos.rptsaldohutang.grid2_reload     = function(){
        w2ui[this.id + '_grid2'].reload() ;
    }
    bos.rptsaldohutang.grid2_destroy    = function(){
        if(w2ui[this.id + '_grid2'] !== undefined){
            w2ui[this.id + '_grid2'].destroy() ;
        }
    }

    bos.rptsaldohutang.grid2_render     = function(){
        this.obj.find("#grid2").w2render(this.id + '_grid2') ;
    }

    bos.rptsaldohutang.grid2_reloaddata = function(){
        this.grid2_reload() ;
    }

    bos.rptsaldohutang.cmddetail	= function(kode,tgl){

        bjs.ajax(this.url + '/detailhutang', 'kode=' + kode +'&tgl=' + tgl);
    }

    bos.rptsaldohutang.loadmodalpreview      = function(l){
        this.obj.find("#wrap-preview-detail-d").modal(l) ;
    }
	
	bos.rptsaldohutang.obj.find("#cmdview").on("click", function(){
		bos.rptsaldohutang.initreport();
	}) ;

    bos.rptsaldohutang.obj.find("#cmdviewd").on("click", function(){
		bos.rptsaldohutang.initreportd();
	}) ;

    bos.rptsaldohutang.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }
    bos.rptsaldohutang.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }

    bos.rptsaldohutang.initreportd  = function(s,e){
        bjs.ajax(this.base_url+ '/initreportd', bjs.getdataform(this.obj.find("form"))) ;
    }
    bos.rptsaldohutang.openreportd  = function(){
        bjs_os.form_report( this.base_url + '/showreportd' ) ;
    }

    bos.rptsaldohutang.cmdrefresh          = bos.rptsaldohutang.obj.find("#cmdrefresh") ;
    bos.rptsaldohutang.initfunc    = function(){
        this.obj.find("#cmdrefresh").on("click", function(e){
            e.preventDefault() ;
            bos.rptsaldohutang.grid1_reloadData() ;
        }) ;


    }

    bos.rptsaldohutang.initcomp		= function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;

        this.grid2_load() ;

        bjs.initenter(this.obj.find("form")) ;
        bjs.initdate("#" + this.id + " .date") ;
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
    }

    bos.rptsaldohutang.initcallback	= function(){
        this.obj.on('remove', function(){
            bos.rptsaldohutang.grid1_destroy() ;
            bos.rptsaldohutang.grid2_destroy() ;

        }) ;
    }

    $(function(){
        bos.rptsaldohutang.initcomp() ;
        bos.rptsaldohutang.initcallback() ;
        bos.rptsaldohutang.initfunc() ;
    });
</script>