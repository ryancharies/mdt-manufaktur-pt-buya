<style media="screen">
   #bos-form-rptneraca-wrapper .text-number{font-size: 16px; font-weight: bold; text-align: right;}
   #bos-form-rptneraca-wrapper .info{border-radius: 4px; margin-right: 20px}
</style> 
<form novalidate>          
<div class="bodyfix scrollme" style="height:100%"> 
   <table class="osxtable form" border="0">
		<tr>
			<td width="80px"><label for="tgl">Tgl</label> </td> 
			<td width="20px">:</td>
			<td width="100px">
				<input style="width:80px" type="text" class="form-control date" id="tglawal" name="tglawal" required value=<?=date("d-m-Y")?> <?=date_set()?>>
			</td>
            <td width="20px">sd</td>
            <td width="100px">
				<input style="width:80px" type="text" class="form-control date" id="tglakhir" name="tglakhir" required value=<?=date("d-m-Y")?> <?=date_set()?>>
			</td>
            <td width="40px">Level&nbsp;: </td>
			<td width="50px">
                 <input type="text" id="level" name="level" value = "4" class="form-control" placeholder="level" required>
            </td>
            <td></td>
			<td width="100px">
				<button class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
			</td>
            <td width="100px">
                <select name="export" id="export" class="form-control select" style="width:100%"
                    data-sf="load_export" data-placeholder="PDF" required></select>
            </td>
			<td width="100px">
				<button class="btn btn-primary pull-right" id="cmdview">Export</button> 
			</td>
		</tr>  
      <tr><td colspan="6"><hr class="no-margin no-padding"></td></tr>
   </table> 
   <div class="row" style="height: calc(100% - 50px);"> 
      <div class="col-sm-12 full-height">
         <div id="grid1" class="full-height"></div>
      </div>
   </div> 
</div>
</form>
<script type="text/javascript">
	<?=cekbosjs();?>

	bos.rptneracalajur.grid1_data 	 = null ;
	bos.rptneracalajur.grid1_loaddata= function(){
		this.grid1_data 		= {
			"tglawal"	   : this.obj.find("#tglawal").val(),
            "tglakhir"	   : this.obj.find("#tglakhir").val(),
            "level"	   : this.obj.find("#level").val()
		} ;
	}

	bos.rptneracalajur.grid1_load    = function(){
      this.obj.find("#grid1").w2grid({
			name		: this.id + '_grid1',
			url 		: bos.rptneracalajur.base_url + "/loadgrid",
			postData : this.grid1_data ,
			show 		: {
				footer 		: true,
				toolbar		: false,
				toolbarColumns  : false
			},
			multiSearch		: false,
			columns: [
				{ field: 'kode', caption: 'Rekening', size: '80px', sortable: false,frozen:true},
				{ field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false,frozen:true},
				{ field: 'sad',render:'float:2', caption: 'SA Debet', size: '120px', sortable: false},
                { field: 'sak',render:'float:2', caption: 'SA Kredit', size: '120px', sortable: false},
				{ field: 'jrd',render:'float:2', caption: 'JR Debet', size: '120px', sortable: false},
                { field: 'jrk',render:'float:2', caption: 'JR Kredit', size: '120px', sortable: false},
				{ field: 'nsd',render:'float:2', caption: 'NS Debet', size: '120px', sortable: false},
                { field: 'nsk',render:'float:2', caption: 'NS Kredit', size: '120px', sortable: false},
				{ field: 'ajpd',render:'float:2', caption: 'Peny Debet', size: '120px', sortable: false},
                { field: 'ajpk',render:'float:2', caption: 'Peny Kredit', size: '120px', sortable: false},
				{ field: 'nspd',render:'float:2', caption: 'NSP Debet', size: '120px', sortable: false},
                { field: 'nspk',render:'float:2', caption: 'NSP Kredit', size: '120px', sortable: false},
				{ field: 'lrd',render:'float:2', caption: 'LR Debet', size: '120px', sortable: false},
                { field: 'lrk',render:'float:2', caption: 'LR Kredit', size: '120px', sortable: false},
				{ field: 'nrcd',render:'float:2', caption: 'NRC Debet', size: '120px', sortable: false},
                { field: 'nrck',render:'float:2', caption: 'NRC Kredit', size: '120px', sortable: false},
			]
		});
   }

   bos.rptneracalajur.grid1_setdata	= function(){
		w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
	}
	bos.rptneracalajur.grid1_reload		= function(){
		w2ui[this.id + '_grid1'].reload() ;
	}
	bos.rptneracalajur.grid1_destroy 	= function(){
		if(w2ui[this.id + '_grid1'] !== undefined){
			w2ui[this.id + '_grid1'].destroy() ;
		}
	}

	bos.rptneracalajur.grid1_render 	= function(){
		this.obj.find("#grid1").w2render(this.id + '_grid1') ;
	}

	bos.rptneracalajur.grid1_reloaddata	= function(){
		this.grid1_loaddata() ;
		this.grid1_setdata() ;
		this.grid1_reload() ;
	}

	bos.rptneracalajur.cmdedit		= function(id){
		bjs.ajax(this.url + '/editing', 'id=' + id);
	}

	bos.rptneracalajur.cmddelete		= function(id){
		if(confirm("Hapus Data?")){
			bjs.ajax(this.url + '/deleting', 'id=' + id);
		}
	}

	bos.rptneracalajur.init				= function(){
		bjs.ajax(this.url + "/init") ;
	}

	bos.rptneracalajur.obj.find("#cmdrefresh").on("click", function(){ 
   		bos.rptneracalajur.grid1_reloaddata() ; 
	}) ;

	bos.rptneracalajur.obj.find("#cmdview").on("click", function(){
        bos.rptneracalajur.initreport();
	}) ;

    bos.rptneracalajur.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }
    bos.rptneracalajur.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }


	bos.rptneracalajur.initcomp	= function(){
		bjs.initselect({
			class : "#" + this.id + " .select"
		}) ;
		bjs.initdate("#" + this.id + " .date") ;
		bjs_os.inittab(this.obj, '.tpel') ;
		bjs_os._header(this.id) ; //drag header
		this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
	}

	bos.rptneracalajur.initcallback	= function(){
		this.obj.on("bos:tab", function(e){
			bos.rptneracalajur.tabsaction( e.i )  ;
		});  

		this.obj.on("remove",function(){
			bos.rptneracalajur.grid1_destroy() ;
		}) ;   	

	}

	bos.rptneracalajur.initfunc 		= function(){
		this.init() ;
		this.grid1_loaddata() ;
		this.grid1_load() ;

		this.obj.find("form").on("submit", function(e){ 
         e.preventDefault() ;
      	});
	}


	$(function(){
		bos.rptneracalajur.initcomp() ;
		bos.rptneracalajur.initcallback() ;
		bos.rptneracalajur.initfunc() ;
	}) ;
</script>