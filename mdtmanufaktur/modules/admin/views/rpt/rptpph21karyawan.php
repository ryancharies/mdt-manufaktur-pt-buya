<div class="header active">
    <table class="header-table">
        <tr>
            <td class="icon"  align="left" ><i class="fa fa-building"></i></td>
            <td class="title">Payroll</td>
            <td class="button">
                <table class="header-button" align="right">
                    <tr>
                        <td>
                            <div class="btn-circle btn-close transition" onclick="bos.rptpph21karyawan.close()">
                                <img src="./uploads/titlebar/close.png">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div class="body">
    <form novalidate>
        <div class="bodyfix scrollme" style="height:100%">
            <table width = '100%'>
                <tr>
                    <td>
                        <table class="osxtable form">
                            <tr>
                                <td width="100px"><label for="tahun">Tahun</label> </td>
                                <td width="5px">:</td>
                                <td width="85px">
                                    <select name="thn" id="thn" class="form-control selectauto" style="width:100%"
                                    data-sf="load_tahun" data-placeholder="Tahun" required>
                                    <option value = '<?= date("Y") ?>' selected><?= date("Y") ?></option>
                                    </select>
                                </td>
                                <td width="85px">
                                    <button type="button" class="btn btn-primary pull-right" id="cmdrefresh">Refresh</button>
                                </td>
                                <td width="85px">
                                    <button type="button" class="btn btn-success pull-right" id="cmdcetakdetail">Cetak Detail</button>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width = '100%' height = "750px">
                            <tr>
                                <td >
                                    <div id="grid1" class="full-height"></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                <tr>
            </table>               
        </div>
    </form>
</div>
<script type="text/javascript">
    <?=cekbosjs();?>

    bos.rptpph21karyawan.grid1_data    = null ;
    bos.rptpph21karyawan.grid1_loaddata= function(){
        var thn = bos.rptpph21karyawan.obj.find("#thn").val();
        this.grid1_data 		= {'thn':thn} ;
    }

    bos.rptpph21karyawan.grid1_load    = function(){
        this.obj.find("#grid1").w2grid({
            name	: this.id + '_grid1',
            recordHeight : 30,
            limit 	: 100 ,
            url 	: bos.rptpph21karyawan.base_url + "/loadgrid",
            postData: this.grid1_data ,
            show: {
                footer 		: false,
                toolbar		: false,
                toolbarColumns  : false,
                lineNumbers    : true,
                selectColumn: true,
                multiSelect:true
            },
            columns: [
                { field: 'kode', caption: 'NIP', size: '100px', sortable: false,style:"text-align:center;"},
                { field: 'nama', caption: 'Nama', size: '200px', sortable: false},
                { field: 'bagian', caption: 'Bagian', size: '100px', sortable: false},
                { field: 'jabatan', caption: 'Jabatan', size: '100px', sortable: false},
                { field: 'kodeptkp', caption: 'Kode PTKP', size: '100px', sortable: false},
                { field: 'penambah', caption: 'Penambah', size: '100px', sortable: false,style:'text-align:right',render:'float:2'},
                { field: 'pengurang', caption: 'Pengurang', size: '100px', sortable: false,style:'text-align:right',render:'float:2'},
                { field: 'neto', caption: 'Neto', size: '100px', sortable: false,style:'text-align:right',render:'float:2'},
                { field: 'ptkp', caption: 'PTKP', size: '100px', sortable: false,style:'text-align:right',render:'float:2'},
                { field: 'pkp', caption: 'PKP', size: '100px', sortable: false,style:'text-align:right',render:'float:2'},
                { field: 'pph21', caption: 'PPh21', size: '100px', sortable: false,style:'text-align:right',render:'float:2'},
                { field: 'cetak', caption: 'Cetak', size: '100px', sortable: false}
            ],
            onSelect: function(event) {
                event.onComplete = function () {
                    this.set(event.recid,{cetak:"Ya"});
                   // w2ui[event.target].save();
                }
                //this.set(event.recid,{cetakslip:"Cetak Slip"});
            },
            onUnselect: function(event) {
                event.onComplete = function () {
                    this.set(event.recid,{cetak:"Tidak"});
                    //w2ui[event.target].save();
                
                }
                //this.set(event.recid,{cetakslip:"Tidak Cetak Slip"});
            },  
        });


    }


    bos.rptpph21karyawan.grid1_setdata	= function(){
        w2ui[this.id + '_grid1'].postData 	= this.grid1_data ;
    }
    bos.rptpph21karyawan.grid1_reload		= function(){
        w2ui[this.id + '_grid1'].reload() ;
    }
    bos.rptpph21karyawan.grid1_destroy 	= function(){
        if(w2ui[this.id + '_grid1'] !== undefined){
            w2ui[this.id + '_grid1'].destroy() ;
        }
    }

    bos.rptpph21karyawan.grid1_render 	= function(){
        this.obj.find("#grid1").w2render(this.id + '_grid1') ;
    }
    

    bos.rptpph21karyawan.grid1_reloaddata	= function(){
        this.grid1_loaddata() ;
        this.grid1_setdata() ;
        this.grid1_reload() ;
    }
    

    bos.rptpph21karyawan.initcomp	= function(){
        this.grid1_loaddata() ;
        this.grid1_load() ;
        
        bjs.initselect({
		    class : "#" + this.id + " .selectauto"
		}) ;
        bjs.initdate("#" + this.id + " .date") ;
        bjs_os._header(this.id) ; //drag header
        this.obj.find(".header").attr("id",this.id + "-title") ; //set to drag
        
        
    }

    bos.rptpph21karyawan.initcallback	= function(){


        this.obj.on("remove",function(){
            bos.rptpph21karyawan.grid1_destroy() ;
                

        }) ;
    }

    bos.rptpph21karyawan.initreport  = function(s,e){
        bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
    }
    bos.rptpph21karyawan.openreport  = function(){
        bjs_os.form_report( this.base_url + '/showreport' ) ;
    }

    bos.rptpph21karyawan.cmdcetakdetail = bos.rptpph21karyawan.obj.find("#cmdcetakdetail") ;
    bos.rptpph21karyawan.initreportpph21detail  = function(){
       // bjs.ajax(this.base_url+ '/initreport', bjs.getdataform(this.obj.find("form"))) ;
        var datagrid = w2ui[this.id + '_grid1'].records;
        datagrid = JSON.stringify(datagrid);
        bjs.ajax(this.base_url  + '/printpph21karyawan', bjs.getdataform(this.obj.find("form"))+"&grid1="+datagrid,bos.rptpph21karyawan.cmdcetakdetail);

        //bjs_os.form_report( this.base_url + '/printkpikaryawan' , bjs.getdataform(this.obj.find("form"))+"&grid1="+datagrid,bos.rptpph21karyawan.cmdcetak) ;
    }

    bos.rptpph21karyawan.openreportpph21karyawan  = function(data){
        //console.log(data);
        bjs_os.form_report( this.base_url + '/showreporpph21karyawan?data='+data+"&"+bjs.getdataform(this.obj.find("form")) ) ;
    }

    bos.rptpph21karyawan.obj.find("#cmdcetakdetail").on("click", function(){
        if(confirm("Apakah detail akan dicetak?, Data akan dicetak untuk data yang dipilih...")){
            bos.rptpph21karyawan.initreportpph21detail();
        }
	}) ;

    bos.rptpph21karyawan.showReport = function(urlfile){
        bjs_os.form_report(urlfile) ;
    }

    bos.rptpph21karyawan.initfunc 		= function(){
        this.obj.find("#cmdrefresh").on("click",function(){
            // bos.rptpph21karyawan.grid1_loaddata();
            bos.rptpph21karyawan.grid1_reloaddata() ;
            
        });

    }

    $(function(){
        bos.rptpph21karyawan.initcomp() ;
        bos.rptpph21karyawan.initcallback() ;
        bos.rptpph21karyawan.initfunc() ;
    }) ;
</script>
