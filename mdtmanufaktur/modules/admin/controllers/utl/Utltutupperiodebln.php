<?php
class Utltutupperiodebln extends Bismillah_Controller
{
    protected $bdb;
    public function __construct()
    {
        parent::__construct();
        // $this->load->model("func/perhitungan_m") ;
        // $this->load->model("func/updtransaksi_m") ;
        $this->load->model("utl/utltutupperiodebln_m");
        $this->bdb = $this->utltutupperiodebln_m;
    }

    public function index()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi", "", "", "tgl asc");
        $d['mintgl'] = "minimdate = '" . date("m/d/Y", strtotime($tglmin)) . "'";
        $this->load->view("utl/utltutupperiodebln", $d);

    }

    public function loadgrid()
    {
        $va = json_decode($this->input->post('request'), true);
        $vare = array();
        $n = 0;
        
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi", "", "", "tgl asc");
        $periodmin = date("Y-m",strtotime($tglmin));
        for ($bln = 1; $bln <= 12; $bln++) {
            $tgl = date('Y-m-d', mktime(0, 0, 0, $bln + 1, 0, $va['periode']));
            $period = date('Y-m', mktime(0, 0, 0, $bln + 1, 0, $va['periode']));
            $tglawal = date('Y-m-d', mktime(0, 0, 0, $bln, 1, $va['periode']));
            $status = $this->bdb->getval("status", "tgl like '$period%'", "tgl_transaksi", "", "", "tgl asc");
            $ketstatus = "Tidak bisa digunakan";
            $btnopen = "";
            $btnclose = "";
            $ltutup = true;
            if ($status == "0") {
                $ketstatus = "Periode Sudah ditutup";
                $btnopen = '<button type="button" onClick="bos.utltutupperiodebln.cmdopen(\'' . $period . '\')"
                                        class="btn btn-success btn-grid">Buka</button>';
                $btnopen = html_entity_decode($btnopen);
            } else if ($status == "1") {
                if($periodmin == $period){
                    $ltutup = false;
                }
                $ketstatus = "Periode Masih Aktif ";
                if (!$ltutup) {
                    $btnclose = '<button type="button" onClick="bos.utltutupperiodebln.cmdclose(\'' . $period . '\')"
                                        class="btn btn-danger btn-grid">Tutup</button>';
                    $btnclose = html_entity_decode($btnclose);
                }
                
            }
            $vare[] = array("no" => $bln, "bulan" => date_month($bln - 1) . " " . $va['periode'], "status" => $ketstatus,
                "btnopen" => $btnopen, "btnclose" => $btnclose);
        }

        $data = array("total" => count($vare), "records" => $vare);
        echo (json_encode($data));
    }

    public function closeperiode(){
        $va = $this->input->post();
        if(date("Y-m") <= $va['periode']){
            echo('
                alert("Gagal, periode belum bisa ditutup karena masih belum melewati periode ini !!");
            ');
        }else{
            $this->bdb->edit("tgl_transaksi",array("status"=>"0"),"tgl like '{$va['periode']}%'");
            echo('
                alert("Periode berhasil ditutup !!");
                bos.utltutupperiodebln.grid1_reloaddata();
            ');
        }
        
    }

    public function openperiode(){
        $va = $this->input->post();
        $tgl = $va['periode']."-01";
        $this->bdb->edit("tgl_transaksi",array("status"=>"1"),"tgl >= '$tgl%'");
        echo('
            alert("Periode berhasil dibuka !!");
            bos.utltutupperiodebln.grid1_reloaddata();
        ');
    }

}
