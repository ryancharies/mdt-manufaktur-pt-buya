<?php

class Utlupdsys extends Bismillah_Controller
{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper('url');
        $this->load->model("utl/utlupdsys_m") ;
    } 

    public function index(){
        $this->load->view("utl/utlupdsys") ; 
    }

    function file_upload(){
        $config	= array("upload_path"=>"./files_patch/","overwrite"=>true,"allowed_types"=>"zip") ;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload(0)){
            echo('
				alert("Hello User : '. $this->upload->display_errors('','') .'") ;
			') ;
        }else{
            $data = array('upload_data' => $this->upload->data());
            //if (stristr(PHP_OS, 'WIN')) {
            # UPDATE
            $zip    = new ZipArchive;
            $file   = $data['upload_data']['full_path'];
            chmod($file,0777);
            if ($zip->open($file) === TRUE) {
                    $zip->extractTo('./');
                    $zip->close();
                    @unlink($config['upload_path'].'/'.$data['upload_data']['file_name']);
                    
                    //update user info app
                    $this->utlupdsys_m->edit("sys_username",array("infoupdate"=>'0'));

                    echo('
                        alert("Aplikasi berhasil update, untuk melihat list update bisa anda buka di menu info aplikasi pada toolbar, anda harus logout dan login ulang...");
                        bjs.ajax("admin/frame/logout") ;
                    ') ;
            } else {
                echo('
                    alert("Patching Error !!") ;
                ') ;
            }
           /* # LINUX
            } else {
               $this->unZipOnLinux($data['upload_data']['file_name'] , './modules/admin/');
            }*/
 
        }
    }

    function unZipOnLinux($sourceFileName,$destinationPath){
        $directoryPos   = strrpos($sourceFileName,'/');
        $directory      = substr($sourceFileName,0,$directoryPos+1);
        $dir            = opendir( $directory );
        $info           = pathinfo($sourceFileName);
        if ( strtolower($info['extension']) == 'zip' ) {
            system('unzip -q '.$sourceFileName .'  -d '. $destinationPath);
            echo('
                alert("System Updated on Linux Server !!") ;
            ') ;
        }
        closedir( $dir );
    }

}


?>

