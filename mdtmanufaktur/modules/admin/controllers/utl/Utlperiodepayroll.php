<?php
class Utlperiodepayroll extends Bismillah_Controller{
   public function __construct(){
		parent::__construct() ;
      $this->load->model('utl/utlperiodepayroll_m') ;
      $this->load->helper('bdate') ;
      $this->load->helper('bdir') ;
	}

   public function index(){
      $tglmin = $this->utlperiodepayroll_m->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
      $data = array();
      $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
      $this->load->view('utl/utlperiodepayroll',$data) ;
   }

   public function loadgrid(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
      $vdb    = $this->utlperiodepayroll_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->utlperiodepayroll_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['tglawal'] = date_2d($vaset['tglawal']);
         $vaset['tglakhir'] = date_2d($vaset['tglakhir']);
         
         $vaset['cmdedit']    = '<button type="button" onClick="bos.utlperiodepayroll.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Edit</button>' ;
         $vaset['cmddelete']  = '<button type="button" onClick="bos.utlperiodepayroll.cmdnonaktif(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Non Aktifkan</button>' ;
         if($vaset['status'] == '2'){
            $vaset['cmddelete']  = '<button type="button" onClick="bos.utlperiodepayroll.cmdaktif(\''.$dbr['kode'].'\')"
                           class="btn btn-success btn-grid">Aktifkan</button>' ;
         }
         $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;
         $vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function init(){
      savesession($this, "ssperiodepayroll_kode", "") ;
   }

   public function saving(){
      $va 	= $this->input->post() ;
      $kode 	= getsession($this, "ssperiodepayroll_kode") ;
      $this->utlperiodepayroll_m->saving($kode, $va) ;
      echo(' bos.utlperiodepayroll.settab(0) ;  ') ;
   }

   public function editing(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->utlperiodepayroll_m->getdata($kode) ;
      if(!empty($data)){
         savesession($this, "ssperiodepayroll_kode", $kode) ;
         echo('
            with(bos.utlperiodepayroll.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$data['kode'].'") ;
               find("#keterangan").val("'.$data['keterangan'].'").focus() ;
               find("#tglawal").val("'.date_2d($data['tglawal']).'") ;
               find("#tglakhir").val("'.date_2d($data['tglakhir']).'") ;               
            }

             bos.utlperiodepayroll.settab(1) ;
             ');

      }
   }

   public function nonaktif(){
      $va 	= $this->input->post() ;
      $error = $this->utlperiodepayroll_m->nonaktif($va['kode']) ;
      if($error == "ok"){
         echo(' bos.utlperiodepayroll.grid1_reloaddata() ; ') ;
      }else{
         echo('alert("'.$error.'");');
      }
   }

   public function aktif(){
      $va 	= $this->input->post() ;
      $error = $this->utlperiodepayroll_m->aktif($va['kode']) ;
      if($error == "ok"){
         echo(' bos.utlperiodepayroll.grid1_reloaddata() ; ') ;
      }else{
         echo('alert("'.$error.'");');
      }
   }
    
  
   public function getkode(){
      $n  = $this->utlperiodepayroll_m->getkode(FALSE) ;
      $y = substr($n,0,4);
      $b = substr($n,4,2);
      $b = $b - 1;
      echo($b);
      $bln = date_month($b);
      $keterangan = "PERIODE ".$bln." ".$y;

      $dbd = $this->utlperiodepayroll_m->select("sys_periode_payroll","*","","","","kode desc");
      if($dbr = $this->utlperiodepayroll_m->getrow($dbd)){
         $tglawal = strtotime($dbr['tglakhir']) + (24*60*60);
         $tglawal = date("d-m-Y",$tglawal);
         $tglakhir = date("d-m-Y",nextmonth(strtotime($dbr['tglakhir']),1));
         $tgleom = date_eom($dbr['tglakhir']);
         if(date_2s($tgleom) == $dbr['tglakhir']){
            $tglakhir = date_eom($tglakhir);
         }
      }

      echo('
        bos.utlperiodepayroll.obj.find("#kode").val("'.$n.'") ;
        bos.utlperiodepayroll.obj.find("#keterangan").val("'.$keterangan.'") ;
        bos.utlperiodepayroll.obj.find("#tglawal").val("'.$tglawal.'") ;
        bos.utlperiodepayroll.obj.find("#tglakhir").val("'.$tglakhir.'") ;
        ') ;
   }
}
?>
