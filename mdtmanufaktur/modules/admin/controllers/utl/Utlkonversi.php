<?php
class Utlkonversi extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model("func/perhitungan_m") ;
        $this->load->model("func/updtransaksi_m") ;
        $this->load->model("utl/utlkonversi_m") ;
        $this->bdb 	= $this->utlkonversi_m ;
    } 

    public function index(){
        $this->load->view("utl/utlkonversi") ; 

    }   

    public function loadgrid(){
        $vare = array();
        $n = 0 ;
        /*$vare[] = array("ck"=>1,"no"=>++$n,"kode"=>'0',"keterangan"=>"Reset Data");
        $vare[] = array("ck"=>1,"no"=>++$n,"kode"=>'1',"keterangan"=>"Saldo Stock");
        $vare[] = array("ck"=>1,"no"=>++$n,"kode"=>'2',"keterangan"=>"Saldo Hutang");
        $vare[] = array("ck"=>1,"no"=>++$n,"kode"=>'3',"keterangan"=>"Saldo Piutang");
        $vare[] = array("ck"=>1,"no"=>++$n,"kode"=>'4',"keterangan"=>"Saldo Neraca Laba rugi");
        $vare[] = array("ck"=>1,"no"=>++$n,"kode"=>'5',"keterangan"=>"Saldo BG");
        $vare[] = array("ck"=>1,"no"=>++$n,"kode"=>'6',"keterangan"=>"Pembenahan Kode Transaksi Porsekot");
        $vare[] = array("ck"=>1,"no"=>++$n,"kode"=>'7',"keterangan"=>"Pembenahan Kode Transaksi Uang Muka");
        $vare[] = array("ck"=>1,"no"=>++$n,"kode"=>'8',"keterangan"=>"Konversi Data Karyawan");
        $vare[] = array("ck"=>1,"no"=>++$n,"kode"=>'9',"keterangan"=>"Tgl Lunas Penjualan, Retur Penjualan, Penjualan Aset, Pembelian, Retur Pembelian");*/
        $vare[] = array("ck"=>1,"no"=>++$n,"kode"=>'10',"keterangan"=>"Penjualan free 2023 dirubah ke distributor sebenarnya");
        $vare 	= array("total"=>count($vare), "records"=>$vare ) ;
        echo(json_encode($vare)) ; 
    }

    public function posting(){
        $va 	= $this->input->post() ;
        $vaGrid = json_decode($va['grid1']);
        $tgl = '2019-12-31';
        foreach($vaGrid as $key => $val){
            if($val->ck){
               // if($val->kode == "0")$this->utlkonversi_m->resetdata($tgl);
              // if($val->kode == "1")$this->utlkonversi_m->saldostock($tgl);
                //if($val->kode == "2")$this->utlkonversi_m->saldohutang($tgl);
                //if($val->kode == "3")$this->utlkonversi_m->saldopiutang($tgl);
                //if($val->kode == "4")$this->utlkonversi_m->saldonlr($tgl);
                //if($val->kode == "5")$this->utlkonversi_m->saldobg($tgl);
                //if($val->kode == "6")$this->utlkonversi_m->fixkodetrporsekot($tgl);
                //if($val->kode == "7")$this->utlkonversi_m->fixkodetruangmuka($tgl);
                //if($val->kode == "8")$this->utlkonversi_m->datakaryawan();
                //if($val->kode == "9")$this->utlkonversi_m->penjualanlunas();
                if($val->kode == "10")$this->utlkonversi_m->penjualanfree2023();
            }
        }
        //$this->bdb->posting($va) ;
        echo('alert("Data Sudah diposting...");') ;
    }
}
?>

