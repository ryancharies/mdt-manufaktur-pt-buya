<?php
class Utlstatusprocess extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model("func/perhitungan_m") ;
        $this->load->model("func/updtransaksi_m") ;
        $this->load->model("utl/utlstatusprocess_m") ;
        $this->bdb 	= $this->utlstatusprocess_m ;
    } 

    public function index(){
        $this->load->view("utl/utlstatusprocess") ; 

    }   

    public function loadgrid(){
        $vare = array();
        $n = 0 ;

        //service finger
        $recid = "service-finger-solution";
        $service = $this->utlstatusprocess_m->getdataservice($recid);
        
        //status service
        $timenow = time();
        $timeaktifmesin = $service['terakhiraktif'];
        $selisihtime = $timenow - $timeaktifmesin;
        $limamenittime = 5 * 60;
        if($selisihtime < $limamenittime){
            $status = "<font color='green'>Running</font>";
        }else{
            $status = "<font color='red'>Not Running</font>";
        }

        $vare[] = array("recid"=>$recid,"no"=>"1","list"=>$service['keterangan'],"status"=>$status);

        $dbd = $this->utlstatusprocess_m->getmesin();
        while($dbr = $this->utlstatusprocess_m->getrow($dbd)){
            
            //cek time aktif jika selisih lebih dari 5 menit maka di anggap tidak aktif
            $timenow = time();
            $timeaktifmesin = strtotime($dbr['terakhiraktif']);
            $selisihtime = $timenow - $timeaktifmesin;
            $limamenittime = 5 * 60;
            if($selisihtime < $limamenittime){
                $status = "<font color='green'>Connect</font>";
            }else{
                $status = "<font color='red'>Not Connect</font>";
            }

            $recid = "absensi-mesin-".$dbr['kode'];
            $vare[] = array("recid"=>$recid,"no"=>"","list"=>$dbr['keterangan'],"status"=>$status);
        }        
        $vare 	= array("total"=>count($vare), "records"=>$vare ) ;
        echo(json_encode($vare)) ; 
    }

    
}
?>

