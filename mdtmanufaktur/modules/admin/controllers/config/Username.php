<?php
class Username extends Bismillah_Controller{
   private $bdb;
	public function __construct(){
		parent::__construct() ;
      $this->load->model("config/username_m") ;
      $this->bdb = $this->username_m;
	}

	public function index(){
		$this->load->view("config/username") ;
	}

	public function loadgrid(){
		$va	 	= json_decode($this->input->post('request'), true) ;
		$db      = $this->bdb->loadgrid($va) ;
		$level = array();
		while( $dbrow	= $this->bdb->getrow($db['db']) ){
			$vaset 		= $dbrow ;
			$vaset['recid']		= $dbrow['username'] ;
			$vaset['level']		= substr($dbrow['password'],-4);
			if(!isset($level[$vaset['level']]))$level[$vaset['level']] = $this->bdb->getval("name","code = '{$vaset['level']}'" , "sys_username_level");
			$vaset['level'] =$level[$vaset['level']];
			
            $data_var	= ($dbrow['data_var'] !== "") ? json_decode($dbrow['data_var'], true) : array() ;
			if(isset($data_var['cabang']))$vaset['cabang'] = json_encode($data_var['cabang']);
			if(isset($data_var['gudang']))$vaset['gudang'] = json_encode($data_var['gudang']);

			$vaset['cmdedit'] 	= '<button type="button" onClick="bos.username.cmdedit(\''.$dbrow['username'].'\')"
									class="btn btn-default btn-grid">Koreksi</button>' ;
			$vaset['cmddelete'] = '<button type="button" onClick="bos.username.cmddelete(\''.$dbrow['username'].'\')"
									class="btn btn-danger btn-grid">Hapus</button>' ;
			$vaset['cmdedit']	= html_entity_decode($vaset['cmdedit']) ;
			$vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

			$vare[]		= $vaset ;
		}

		$vare 	= array("total"=> $db['rows'], "records"=>$vare ) ;
		echo(json_encode($vare)) ;
	}

	public function init(){
      savesession($this, "ssusername_image", "") ;
	}

	public function seekusername(){
		$va 	= $this->input->post() ;
      $w    = "username = ".$this->bdb->escape($va['username']) ;
      if(getsession($this, "ssusername_username") == ""){
         if($data= $this->bdb->getval("username",$w , "sys_username")){
   			echo(' bos.username.obj.find("#username").val("").focus() ;  ') ;
   		}
      }
	}

	public function saving(){
		$va 		= $this->input->post() ;
		// print_r($va);
		$username 	= $va['username'] ;
      	$w = "username = ".$this->bdb->escape($va['username']) ;
		if( $dblast = $this->bdb->getval("*", $w, "sys_username") ){
			$dblast['data_var']	= ($dblast['data_var'] !== "") ? json_decode($dblast['data_var'], true) : array() ;
		}
		if(empty($dblast)){
			$dblast = array("password"=>"", "data_var"=>array('ava'=>"")) ;
		}

		if(!isset($va['rekkas'])) $va['rekkas'] = "";
		//if(!isset($va['gudang'])) $va['gudang'] = "";
		if(!isset($va['nip'])) $va['nip'] = "";

		$data 		= array("username"=>$va['username'], "fullname"=>$va['fullname'],"cabang"=>$va['cabang'],
					"rekkas"=>$va['rekkas'],"gudang"=>$va['gudang'],"nip"=>$va['nip']) ;
		$data['data_var']	= array("ava"=>$dblast['data_var']['ava'],"cabang"=>json_decode($va['cabang']),"gudang"=>json_decode($va['gudang'])) ;

		if($va['password'] !== ""){
			$data['password']	= pass_crypt($va['password']) . $va['level'] ;
		}else{
			$data['password']	= substr($dblast['password'], 0, strlen($dblast['password'])-4) . $va['level'];
		}

		$vimage 		= json_decode(getsession($this, "ssusername_image", "{}"), true) ;

		if(!empty($vimage)){
			$adir 	= $this->config->item('bcore_uploads') . "users/";
            @mkdir($adir, 0777, true) ;
            foreach ($vimage as $key => $img) {
				$vi	= pathinfo($img) ;
				$dir 	= $adir ;
				$dir .= $va['username'] . "." . $vi['extension'] ;
				if(is_file($dir)) @unlink($dir) ;
				if(@copy($img,$dir)){
					@unlink($img) ;
					@unlink($dblast['data_var']['ava']) ;
					$data['data_var']['ava']	= $dir;
				}
			}
		}
		$data['data_var']	= json_encode($data['data_var']) ;


		$this->bdb->update("sys_username", $data, $w, "username") ;
		echo('
              bos.username.init() ; bos.username.settab(0) ; ') ;
	}

	public function saving_image(){
		$fcfg	= array("upload_path"=>"./tmp/", "allowed_types"=>"jpg|jpeg|png", "overwrite"=>true) ;

		savesession($this, "ssusername_image", "") ;
		$this->load->library('upload', $fcfg) ;
		if ( ! $this->upload->do_upload(0) ){
			echo('
				alert("'. $this->upload->display_errors('','') .'") ;
				bos.username.obj.find("#idlimage").html("") ;
			') ;
		}else{
			$data 	= $this->upload->data() ;
			$tname 	= str_replace($data['file_ext'], "", $data['client_name']) ;
			$vimage	= array( $tname => $data['full_path']) ;
			savesession($this, "ssusername_image", json_encode($vimage) ) ;

			echo('
				bos.username.obj.find("#idlimage").html("") ;
				bos.username.obj.find("#idimage").html("<img src=\"'.base_url("./tmp/" . $data['client_name'] . "?time=". time()).'\" class=\"img-responsive\" />") ;
			') ;
		}
	}

	public function editing(){
		$va 	= $this->input->post() ;
      	$w    = "username = ".$this->bdb->escape($va['username']) ;
		$data = $this->bdb->getval("*", $w, "sys_username") ;
		if(!empty($data)){
         savesession($this, "ssusername_username", $va['username']) ;
			$image 		= "" ;
			$slevel 	= array() ;
			$scabang    = array();
			$sgudang    = array();
            $srekkas    = array();
            $data_var	= ($data['data_var'] !== "") ? json_decode($data['data_var'], true) : array() ;
			if(isset($data_var['ava'])){
				$image 	= '<img src=\"'.base_url($data_var['ava']).'\" class=\"img-responsive\"/>' ;
			}

			//cabang
			if(isset($data_var['cabang'])){
				foreach($data_var['cabang'] as $key => $val){
					$scabang[] 	= array("id"=>$val, "text"=>$this->bdb->getval("keterangan", "kode = '{$val}'", "cabang")) ;
				}
			}else{
				$scabang[] 	= array("id"=>$data['cabang'], "text"=>$this->bdb->getval("keterangan", "kode = '{$data['cabang']}'", "cabang")) ;
			}

			//gudang
			if(isset($data_var['gudang'])){
				foreach($data_var['gudang'] as $key => $val){
					$sgudang[] 	= array("id"=>$val, "text"=>$this->bdb->getval("keterangan", "kode = '{$val}'", "gudang")) ;
				}
			}else{
				$sgudang[] 	= array("id"=>$data['gudang'], "text"=>$this->bdb->getval("keterangan", "kode = '{$data['gudang']}'", "gudang")) ;
			}

			$level 		= substr($data['password'], -4) ;
			$slevel[] 	= array("id"=>$level, "text"=> $level . " - " . $this->bdb->getval("name", "code = '{$level}'", "sys_username_level")) ;
            $srekkas[] 	= array("id"=>$data['rekkas'], "text"=> $data['rekkas'] . " - " . $this->bdb->getval("keterangan", "kode = '{$data['rekkas']}'", "keuangan_rekening")) ;
            $sgudang[] 	= array("id"=>$data['gudang'], "text"=> $data['gudang'] . " - " . $this->bdb->getval("keterangan", "kode = '{$data['gudang']}'", "gudang")) ;
			$snip[] 	= array("id"=>$data['nip'], "text"=> $data['nip'] . " - " . $this->bdb->getval("nama", "kode = '{$data['nip']}'", "karyawan")) ;

				echo('
				with(bos.username.obj){
					find("#username").val("'.$data['username'].'").attr("readonly", true) ;
					find("#fullname").val("'.$data['fullname'].'").focus() ;
					find("#level").sval('.json_encode($slevel).') ;
                    find("#cabang").sval('.json_encode($scabang).') ;
                    find("#rekkas").sval('.json_encode($srekkas).') ;
                    find("#gudang").sval('.json_encode($sgudang).') ;
                    find("#nip").sval('.json_encode($snip).') ;
					find("#idimage").html("'.$image.'")
				}
            	bos.username.settab(1) ;
			') ;
		}
	}

	public function deleting(){
		$va 	= $this->input->post() ;
      	$w    = "username = ".$this->bdb->escape($va['username']) ;
		$this->bdb->delete("sys_username", $w ) ;
		echo(' bos.username.tabsaction(0) ; ') ;
	}
    
    public function seekcabang(){
        $search     = $this->input->get('q');
        $vdb    = $this->username_m->seekcabang($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->username_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ".$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }
    public function seeklevel(){
        $search     = $this->input->get('q');
        $vdb    = $this->username_m->seeklevel($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->username_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ".$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }
    public function seekrekening(){
        $search     = $this->input->get('q');
        $vdb    = $this->username_m->seekrekening($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->username_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ".$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }
    public function seekgudang(){
        $search     = $this->input->get('q');
        $vdb    = $this->username_m->seekgudang($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->username_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ".$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
	}
	
	public function seekkaryawan(){
        $search     = $this->input->get('q');
        $vdb    = $this->username_m->seekkaryawan($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->username_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ".$dbr['nama']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }
}
?>
