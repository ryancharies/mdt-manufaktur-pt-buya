<?php
class Gantipassword extends Bismillah_Controller{
   private $bdb;
	public function __construct(){
		parent::__construct() ;
      $this->load->model("config/gantipassword_m") ;
      $this->bdb = $this->gantipassword_m;
	}

	public function index(){
		$this->load->view("config/gantipassword") ;
	}

	public function cekpassskrg(){
		$va 	= $this->input->post() ;
		$username = getsession($this, "username");
		$w = "username = '$username'";
		if( $dblast = $this->bdb->getval("*", $w, "sys_username") ){
			$dblast['data_var']	= ($dblast['data_var'] !== "") ? json_decode($dblast['data_var'], true) : array() ;
			$passnow = substr($dblast['password'], 0, strlen($dblast['password'])-4);
			if(pass_crypt($va['passwordskrg']) <> $passnow){
				echo('
					bos.gantipassword.obj.find("#lblpasswordskrg").css("color", "red");
				');
			}else{
				echo('
					bos.gantipassword.obj.find("#lblpasswordskrg").css("color", "black");
				');
			}
		}
		
	}

	public function saving(){
		$va 		= $this->input->post() ;
		$username = getsession($this, "username");
		$w = "username = '$username'";
		$error = "";
		if( $dblast = $this->bdb->getval("*", $w, "sys_username") ){
			$dblast['data_var']	= ($dblast['data_var'] !== "") ? json_decode($dblast['data_var'], true) : array() ;
			$passnow = substr($dblast['password'], 0, strlen($dblast['password'])-4);
			if(pass_crypt($va['passwordskrg']) <> $passnow){
				$error .= "Password lama tidak valid !!! \\n";
			}

			if($va['passwordbru'] <> $va['passwordbru2']){
				$error .= "Password baru tidak valid !!!";			
			}

			if($error == ""){
				$level = substr($dblast['password'],strlen($dblast['password'])-4);
				$passwordbaru = pass_crypt($va['passwordbru']).$level;
				$data = array("password"=>$passwordbaru);
				$this->bdb->edit("sys_username", $data, $w, "username") ;
				echo('
					alert("Password berhasil dirubah, anda harus logout dan login ulang...");
					bjs.ajax("admin/frame/logout") ;
				');
			}else{
				echo('
					alert("'.$error.'");
				');
			}//d154c23962918b23239809a675ef3b8ad860bff50000
		}
	}
}
?>
