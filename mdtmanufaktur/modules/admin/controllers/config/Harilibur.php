<?php
class Harilibur extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model("config/harilibur_m") ;
        $this->bdb 	= $this->harilibur_m ;
    } 

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $d['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $thn = date("Y");
        $harilibur = array();
        $vdb = $this->bdb->select("sys_jadwal","tgl","libur = '1' and tgl like '$thn%' and status = '1'");
        while($dbr = $this->bdb->getrow($vdb)){
            $arrtgl = explode("-",$dbr['tgl']);
            $harilibur[] = array("tahun"=>$arrtgl[0],"bulan"=>$arrtgl[1],"hari"=>$arrtgl[2],"tgl"=>$dbr['tgl']);
        }

        $d['harilibur'] = json_encode($harilibur);
        $this->load->view("config/harilibur",$d) ; 

    }

    public function saving(){
        $va 	= $this->input->post() ;
        $this->harilibur_m->saving($va);
        $thn = date("Y",strtotime($va['tgl']));
        echo('
            alert("Data telah disimpan...");
            bos.harilibur.setdatalibur("'.$thn.'");
        ');
    }

    public function updatecalender(){
        $va 	= $this->input->post() ;
        $thn = $va['thn'];
        $harilibur = array();
        $vdb = $this->bdb->select("sys_jadwal","tgl","libur = '1' and tgl like '$thn%' and status = '1'");
        while($dbr = $this->bdb->getrow($vdb)){
            $arrtgl = explode("-",$dbr['tgl']);
            $harilibur[] = array("tahun"=>$arrtgl[0],"bulan"=>$arrtgl[1],"hari"=>$arrtgl[2],"tgl"=>$dbr['tgl']);
        }

        $dataharilibur = json_encode($harilibur);
        echo('
            bos.harilibur.setharilibur('.$dataharilibur.');
        ');
    }
}
?>

