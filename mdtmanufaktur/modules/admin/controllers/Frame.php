<?php
class Frame extends Bismillah_Controller{
	private $bdb ;
	private $vakor 		= array() ;
	private $id_daerah 	= array() ;
	public function __construct(){
		parent::__construct() ;
		$this->load->helper('bmenu') ;
		$this->load->helper('bdir') ;
		$this->load->model('frame_m') ;
		$this->bdb = $this->frame_m ;
	}

	public function index(){
		$data 	= array("app_title"	=> getsession($this, "app_title"),
								"fullname"	=> getsession($this, "fullname"),
								"data_var"	=> getsession($this, "data_var"),
                                "username"	=> getsession($this, "username"),
								"city"		=> getsession($this, "city")) ;
		$this->load->view("frame", $data) ;
		file_tmp_delete();
	}

	public function menu_angular(){
		$arrmenu= menu_get($this, APPPATH . "../modules/admin/menu.php", "bmenu", "Administrator") ;
		$vm  	  = array() ;
		$this->menu_generate($vm,$arrmenu) ;
		echo json_encode($vm) ;
	}

	//private function for menu adminlte
	private function menu_generate(&$vm, $arrmenu){
		$level_code 	= getsession($this, "level_code") ;
		$level_value 	= getsession($this, "level_value") ;
		$level_input 	= getsession($this, "level_input") ;

		foreach ($arrmenu as $key => $value) {
			$v 	= true ;
			if($level_code !== "0000"){
				$v 	= false;
				if( strpos($level_value, $value['md5']) > -1){
					$v = true;
					if( strpos($value['name'], ".") > -1 ){
						//simpan ke input
						$li 	= explode(",", $level_input) ;
						$li[] = $value['md5'] ;
						savesession($this, "level_input", implode(",", $li)) ;
					}
				}
			}
			if( strpos($value['obj'], ".") > -1 ) $v = false ;
			if($v){
				$s	 = array("name"=>$value['name'], "icon"=>$value['icon'], "o"=>array()) ;
	        	if($value['loc'] !== ""){
            	$s['o']  	= $value ;
            }else{
            	$s['parent']= "parent" ;
            }

	        	$vm[] = $s ;
		    	if(isset($value['children'])){
		    		$this->menu_generate($vm, $value['children']) ;
		    	}
			}
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		echo('window.location.href = "'.base_url().'" ;') ;
	}

	public function ping(){
		$vd 	= date_2b(date("Y-m-d")) ;
		echo(' $("#osx #texttime").html("'.$vd['day']. ", " . $vd['d'] . " " . $vd['m'] . " "  . $vd['y'] .'") ; ') ;
	}

   public function ceknotif(){
		$this->ceknotif_pengajuandana() ;
		$this->ceknotif_updaplikasi() ;
   }

	private function ceknotif_pengajuandana(){
		$username = getsession($this, "username");
		$jml = 0 ;
		$w 	= "p.status = '1' and o.username = '$username' and o.tahap <> 0 and o.status = '0'" ;
		$j 	= "left join keuangan_pengajuan p on p.faktur = o.faktur";
		$f	= "p.faktur,o.tahap,
			(select b.tahap from keuangan_pengajuan_otorisasi b where b.faktur = p.faktur and b.tahap <> '0' and b.status = '0' order by b.tahap asc limit 1) tahapakhir";
		$db = $this->bdb->select("keuangan_pengajuan_otorisasi o",$f, $w,$j) ;
		while($r = $this->bdb->getrow($db)){
			if($r['tahap'] == $r['tahapakhir'])$jml++;
		}

		
			

			if($jml > 0){
				$content = "Terdapat " . $jml . " pengajuan dana yang perlu di otorisasi!<br /> &#8594; <a style='color: white;' onClick='openmenuotpengajuandana()'> Klik Disini Untuk Info Lengkap </a> &#8592;";
				setnotif("Pengajuan Dana",$content, "ion-cash") ;
			}
	}

	private function ceknotif_updaplikasi(){
		$username = getsession($this, "username");
		$jml = 0 ;
		$w 	= "username = '$username' and infoupdate = '0'" ;
		$j 	= "";
		$f	= "username";
		$db = $this->bdb->select("sys_username",$f, $w,$j) ;
		if($r = $this->bdb->getrow($db)){
			$content = "Update aplikasi versi ". BISMILLAH_APP_VERSION .", buka info aplikasi untuk melihat detail ";
			setnotif("Penting",$content, "ion-information") ;
			$this->bdb->edit("sys_username",array("infoupdate"=>'1'),"username = '$username'");
		}
	}

}
?>
