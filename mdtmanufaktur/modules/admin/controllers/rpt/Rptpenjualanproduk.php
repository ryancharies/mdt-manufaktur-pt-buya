
<?php
class Rptpenjualanproduk extends Bismillah_Controller{
	protected $bdb ;
	public function __construct(){
		parent::__construct() ;
		$this->load->model("rpt/rptpenjualanproduk_m") ;
        $this->load->model("func/perhitungan_m") ;
		$this->bdb 	= $this->rptpenjualanproduk_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptpenjualanproduk_" ;
	}  

	public function index(){ 

       

		$this->load->view("rpt/rptpenjualanproduk") ; 

	}

    public function initgrid1(){

        $va = $this->input->post() ;
        $judul = ($va['jenis'] == 'customer')? 'Customer' : 'Wilayah';
        $custgol = "";
        if(isset($va['customer_gol']))$custgol = " " . $this->bdb->getval("keterangan", "kode = '{$va['customer_gol']}'", "customer_golongan");

        $arrreturn = array("kolom"=>array(),"kolomgroup"=>array(),"judul"=>$judul.$custgol);
        $arrreturn['kolom'][] = array('field'=>"kode",'caption'=>"Kode",'size'=> '80px','style'=>'text-align:center','sortable'=>false,'frozen'=>true);
        if($va['jenis'] == 'customer')$arrreturn['kolom'][] = array('field'=>"nama",'caption'=>"Nama",'size'=> '200px','sortable'=>false,'frozen'=>true);
        $arrreturn['kolom'][] = array('field'=>"dati_2",'caption'=>"Kota / Kab",'size'=> '150px','sortable'=>false,'frozen'=>true);

        //group
        $arrreturn['kolomgroup'][] = array('span' => ($va['jenis'] == 'customer')? 3 : 2, 'caption'=> $judul);

        //cek status sudah posting atau belum
        $arrkel = array();
        $spankel = 0;
        $cabang = getsession($this, "cabang");
        $dbd = $this->bdb->select("stock_kelompok","kode,keterangan","jenis = 'P'");
        while($dbr = $this->bdb->getrow($dbd)){
            $spankel++;
            $dbd1  = $this->bdb->select("stock","kode,keterangan","stock_kelompok = '{$dbr['kode']}'");
            $span = 0 ;
            while($dbr1 = $this->bdb->getrow($dbd1)){
                $span++;
                $ketprod = str_replace($dbr['keterangan'],'',$dbr1['keterangan']);
                $ketprod = trim($ketprod);
                $arrreturn['kolom'][] = array('field'=>$dbr1['kode'],'caption'=>$ketprod,'size'=> '100px','style'=>'text-align:right','sortable'=>false,'render'=>'float:2');
            }
            if($span > 0){
                $arrreturn['kolomgroup'][] = array('span' => $span, 'caption'=> $dbr['keterangan']);
                $arrkel[$dbr['kode']]=$dbr;
            }
        }

        foreach($arrkel as $key => $val){
            $arrreturn['kolom'][] = array('field'=>"jml-".$val['kode'],'caption'=>$val['keterangan'],'size'=> '100px','style'=>'text-align:right','sortable'=>false,'render'=>'float:2');
            
        }
        $arrreturn['kolomgroup'][] = array('span' => $spankel, 'caption'=> "Jumlah");

        foreach($arrkel as $key => $val){
            $arrreturn['kolom'][] = array('field'=>"pers-".$val['kode'],'caption'=>$val['keterangan'] ." (%)",'size'=> '80px','style'=>'text-align:right','sortable'=>false,'render'=>'float:2');
            
        }
        $arrreturn['kolomgroup'][] = array('span' => $spankel, 'caption'=> "Persentase");



        $return = json_encode($arrreturn);
        echo($return);
    }
    
    public function loadgrid1(){ 
        $va     = json_decode($this->input->post('request'), true) ; 

        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $jenis = ($va['jenis'] == 'customer')? 't.customer' : 'c.dati_2';
        

        $vare0 = array();
        $arrtot = array();
        $arrtot['nama'] = "Total";
        $arrtot['w2ui'] = array("summary"=>true);

        $arrkel = array();
        $dbd_kel = $this->bdb->select("stock_kelompok","kode","jenis='P'");
        while($dbr_kel = $this->bdb->getrow($dbd_kel)){
            $arrkel[] = $dbr_kel['kode'];
            $arrtot["jml-".$dbr_kel['kode']] = 0 ;
            $arrtot["pers-".$dbr_kel['kode']] = 0 ;
        }

        //load data produk;
        $cabang = getsession($this, "cabang");

        $join = "left join customer c on c.kode = t.customer left join dati_2 d on d.kode = c.dati_2";
        $wh = "t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}' and t.cabang = '$cabang' and t.status = '1'";
        if(isset($va['customer_gol']))$wh .= " and c.golongan = '{$va['customer_gol']}'";

        $dbd = $this->bdb->select("penjualan_total t","t.faktur,$jenis as kode,c.nama,d.keterangan as dati_2",$wh,$join,$jenis,$jenis);
        while($dbr = $this->bdb->getrow($dbd)){
            $data = array();
            $data['recid'] = $dbr['kode'];
            $data['kode']= $dbr['kode'];
            if($va['jenis'] == 'customer') $data['nama'] = $dbr['nama'];
            $data['dati_2'] = $dbr['dati_2'];

            foreach($arrkel as $key2 => $val_kl){
                $data["jml-".$val_kl] = 0 ;
                $data["pers-".$val_kl] = 0 ;
            }

           
            
            $vare0[$dbr['kode']] = $data;
                
        }

         //penjualan
         $join_pj = "left join penjualan_total t on t.faktur = d.faktur left join customer c on c.kode = t.customer";

         $wh_pj = "t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}' and t.status = '1' and d.jenis_kelompok = 'P'";
        if(isset($va['customer_gol']))$wh_pj .= " and c.golongan = '{$va['customer_gol']}'";
         
         $dbd_pj = $this->bdb->select("penjualan_detail d","$jenis as kode,d.stock,d.stock_kelompok,ifnull(sum(d.qty),0) as qtypj",$wh_pj,$join_pj,"concat($jenis,'##',d.stock)");
         while($dbr_pj = $this->bdb->getrow($dbd_pj)){
             if(!isset($vare0[$dbr_pj['kode']][$dbr_pj['stock']]))$vare0[$dbr_pj['kode']][$dbr_pj['stock']] = 0 ;
             if(!isset($vare0[$dbr_pj['kode']]["jml-".$dbr_pj['stock_kelompok']])){
                 $vare0[$dbr_pj['kode']]["jml-".$dbr_pj['stock_kelompok']] = 0 ;
                 $vare0[$dbr_pj['kode']]["pers-".$dbr_pj['stock_kelompok']] = 0 ;
             }

             if(!isset($arrtot[$dbr_pj['stock']]))$arrtot[$dbr_pj['stock']] = 0 ;
             if(!isset($arrtot["jml-".$dbr_pj['stock_kelompok']])){
                 $arrtot["jml-".$dbr_pj['stock_kelompok']] = 0 ;
                 $arrtot["pers-".$dbr_pj['stock_kelompok']] = 0 ;
             }

             $vare0[$dbr_pj['kode']][$dbr_pj['stock']] += $dbr_pj['qtypj'];
             $vare0[$dbr_pj['kode']]["jml-".$dbr_pj['stock_kelompok']] += $dbr_pj['qtypj'];

             $arrtot[$dbr_pj['stock']] += $dbr_pj['qtypj'];
             $arrtot["jml-".$dbr_pj['stock_kelompok']] += $dbr_pj['qtypj'];
         }

         //retur
         $join_pj = "left join penjualan_retur_total t on t.faktur = d.faktur left join customer c on c.kode = t.customer";
         $wh_pj = "t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}' and t.status = '1' and d.jenis_kelompok = 'P'";
        if(isset($va['customer_gol']))$wh_pj .= " and c.golongan = '{$va['customer_gol']}'";         
// echo($wh_pj);

         $dbd_pj = $this->bdb->select("penjualan_retur_detail d","$jenis as kode,d.stock,d.stock_kelompok,ifnull(sum(d.qty),0) as qtypjrt",$wh_pj,$join_pj,"concat($jenis,'##',d.stock)");
         while($dbr_pj = $this->bdb->getrow($dbd_pj)){
             if(!isset($vare0[$dbr_pj['kode']][$dbr_pj['stock']]))$vare0[$dbr_pj['kode']][$dbr_pj['stock']] = 0 ;                
             if(!isset($vare0[$dbr_pj['kode']]["jml-".$dbr_pj['stock_kelompok']])){
                $vare0[$dbr_pj['kode']]["jml-".$dbr_pj['stock_kelompok']] = 0 ;
                $vare0[$dbr_pj['kode']]["pers-".$dbr_pj['stock_kelompok']] = 0 ;
             }

             if(!isset($arrtot[$dbr_pj['stock']]))$arrtot[$dbr_pj['stock']] = 0 ;
             if(!isset($arrtot["jml-".$dbr_pj['stock_kelompok']])){
                 $arrtot["jml-".$dbr_pj['stock_kelompok']] = 0 ;
                 $arrtot["pers-".$dbr_pj['stock_kelompok']] = 0 ;
             }

             $vare0[$dbr_pj['kode']][$dbr_pj['stock']] -= $dbr_pj['qtypjrt'];
             $vare0[$dbr_pj['kode']]["jml-".$dbr_pj['stock_kelompok']] -= $dbr_pj['qtypjrt'];

             $arrtot[$dbr_pj['stock']] -= $dbr_pj['qtypjrt'];
             $arrtot["jml-".$dbr_pj['stock_kelompok']] -= $dbr_pj['qtypjrt'];

         }

        
        foreach($vare0 as $key => $val){
            foreach($arrkel as $key2 => $val_kl){
                $val["pers-".$val_kl] = devide($val["jml-".$val_kl],$arrtot["jml-".$val_kl])*100;
                $arrtot["pers-".$val_kl] += $val["pers-".$val_kl];
            }            
            $vare[] = $val;
        }

        $vare[] = $arrtot;
        

        $vare 	= array("total"=>count($vare)-1, "records"=>$vare ) ; 
        echo(json_encode($vare)) ; 
    }

    public function initreport1(){
        $va      = $this->input->post() ;
        
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $jenis = ($va['jenis'] == 'customer')? 't.customer' : 'c.dati_2';
        

        $file   = setfile($this, "rpt_pjproduk", __FILE__ , $va) ;
        savesession($this, $this->ss . "file1", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;

        $vare = array();
        $vare_s = array();
        $arrtot = array();

        $arrkel = array();

        

        $dbd = $this->bdb->select("stock_kelompok","kode,keterangan","jenis = 'P'");
        while($dbr = $this->bdb->getrow($dbd)){
            $arrtot["jml-".$dbr['kode']] = 0 ;

            $dbd1  = $this->bdb->select("stock","kode,keterangan","stock_kelompok = '{$dbr['kode']}'");
            $span = 0 ;                
            while($dbr1 = $this->bdb->getrow($dbd1)){
                $span++ ;
                $ketprod = str_replace($dbr['keterangan'],'',$dbr1['keterangan']);
                $ketprod = trim($ketprod);
                $vare_s["stk-".$dbr1['kode']]   = 0;
            }
            if($span > 0)$arrkel[$dbr['kode']]=$dbr;
        }

        foreach($arrkel as $key => $val){
            $vare_s["jml-".$val['kode']] = 0;
        }
        foreach($arrkel as $key => $val){
            $vare_s["pers-".$val['kode']] = 0;
            
        }    
        
        // print_r($vare_s);

        //load data produk;
        $cabang = getsession($this, "cabang");
        $n = 0 ;
        $join = "left join customer c on c.kode = t.customer left join dati_2 d on d.kode = c.dati_2";
        $wh = "t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}' and t.cabang = '$cabang' and t.status = '1'";
        if(isset($va['customer_gol']))$wh .= " and c.golongan = '{$va['customer_gol']}'";         

        $dbd = $this->bdb->select("penjualan_total t","t.faktur,$jenis as kode,c.nama,d.keterangan as dati_2",$wh,$join,$jenis,$jenis);
        while($dbr = $this->bdb->getrow($dbd)){
            $data = array();
            $n++;
            $data['no'] = $n;
            $data['kode'] = $dbr['kode'];
            if($va['jenis'] == 'customer') $data['nama'] = $dbr['nama'];
            $data['dati_2'] = $dbr['dati_2'];
           
            $data2 = array_merge($data,$vare_s);

            $vare[$data['kode']] = $data2;
                
        }

        //penjualan
        $join_pj = "left join penjualan_total t on t.faktur = d.faktur left join customer c on c.kode = t.customer";
        $wh_pj = "t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}' and t.cabang = '$cabang' and t.status = '1' and d.jenis_kelompok = 'P'";
        if(isset($va['customer_gol']))$wh_pj .= " and c.golongan = '{$va['customer_gol']}'";         

        $dbd_pj = $this->bdb->select("penjualan_detail d","$jenis as kode,d.stock,d.stock_kelompok,ifnull(sum(d.qty),0) as qtypj",$wh_pj,$join_pj,"concat($jenis,'##',d.stock)");
        while($dbr_pj = $this->bdb->getrow($dbd_pj)){
            $vare[$dbr_pj['kode']]["stk-".$dbr_pj['stock']] += $dbr_pj['qtypj'];
            $vare[$dbr_pj['kode']]["jml-".$dbr_pj['stock_kelompok']] += $dbr_pj['qtypj'];    
            
            if(!isset($arrtot["jml-".$dbr_pj['stock_kelompok']]))$arrtot["jml-".$dbr_pj['stock_kelompok']] = 0 ;
            $arrtot["jml-".$dbr_pj['stock_kelompok']] += $dbr_pj['qtypj'];  
        }

        //retur
        $join_pj = "left join penjualan_retur_total t on t.faktur = d.faktur left join customer c on c.kode = t.customer";
        $wh_pj = "t.tgl >= '{$va['tglawal']}' and t.tgl <= '{$va['tglakhir']}' and t.status = '1' and d.jenis_kelompok = 'P'";
        if(isset($va['customer_gol']))$wh_pj .= " and c.golongan = '{$va['customer_gol']}'";         
// echo($wh_pj);
        $dbd_pj = $this->bdb->select("penjualan_retur_detail d","$jenis as kode,d.stock,d.stock_kelompok,ifnull(sum(d.qty),0) as qtypjrt",$wh_pj,$join_pj,"concat($jenis,'##',d.stock)");
        while($dbr_pj = $this->bdb->getrow($dbd_pj)){
            $vare[$dbr_pj['kode']]["stk-".$dbr_pj['stock']] -= $dbr_pj['qtypjrt'];
            $vare[$dbr_pj['kode']]["jml-".$dbr_pj['stock_kelompok']] -= $dbr_pj['qtypjrt'];

            if(!isset($arrtot["jml-".$dbr_pj['stock_kelompok']]))$arrtot["jml-".$dbr_pj['stock_kelompok']] = 0 ;
            $arrtot["jml-".$dbr_pj['stock_kelompok']] -= $dbr_pj['qtypjrt'];  
        }

        foreach($vare as $key => $val){
            foreach($arrkel as $key2 => $val_kl){
                $val["pers-".$val_kl['kode']] = devide($val["jml-".$val_kl['kode']],$arrtot["jml-".$val_kl['kode']])*100;
                // $val["pers-".$val_kl['kode']] = round($val["pers-".$val_kl['kode']],2);
            }            
            $vare[$key] = $val;
        }

        file_put_contents($file, json_encode($vare) ) ;
        echo(' bos.rptpenjualanproduk.openreport1() ; ') ;
    }


    public function showreport1(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;

        $judul = ($va['jenis'] == 'customer')? 'CUSTOMER' : 'WILAYAH';
        $custgol = "";
        if(isset($va['customer_gol']))$custgol = " " . $this->bdb->getval("keterangan", "kode = '{$va['customer_gol']}'", "customer_golongan");
        $custgol = strtoupper($custgol);

        $file = getsession($this, $this->ss . "file1") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        if(!empty($data)){
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> $kota ,"5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"Mengetahui,","3"=>"","4"=>"Menyetujui,","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"(.........................)","3"=>"","4"=>"(.........................)","5"=>"") ;

            // $nTotalTotal      = 0 ;
            // foreach ($data as $key => $value) {
            //     $nTotal           = str_replace(",", "", $value['total']) ;
            //     $nTotalTotal  += $nTotal;
            // }

            // $nTotalTotal      = number_format($nTotalTotal) ;
            $arrtot_col = array();
            $arrtot_col['keterangan'] = array("justification"=>"center");
            // $total[] = array("Ket"=>"<b>Total",
            //                 "JumlahTotalTotal"=>$nTotalTotal,
            //                 "Ket2"=>"</b>");


            $arrcol = array();
            $arrcol['no']       = array("caption"=>"No","width"=>2,"wrap"=>1,"justification"=>"right");
            $arrcol['kode']     = array("caption"=>"Kode","width"=>4,"justification"=>"center");
            if($va['jenis'] == 'customer')$arrcol['nama']     = array("caption"=>"Nama","wrap"=>1,"justification"=>"left");
            $arrcol['dati_2']   = array("caption"=>"Wilayah Kota / Kab","wrap"=>1,"justification"=>"left");
            
            $arrkel = array();

            $cabang = getsession($this, "cabang");
            $dbd = $this->bdb->select("stock_kelompok","kode,keterangan","jenis = 'P'");
            while($dbr = $this->bdb->getrow($dbd)){
                $dbd1  = $this->bdb->select("stock","kode,keterangan","stock_kelompok = '{$dbr['kode']}'");
                $span = 0 ;                
                while($dbr1 = $this->bdb->getrow($dbd1)){
                    $span++ ;
                    $ketprod = str_replace($dbr['keterangan'],'',$dbr1['keterangan']);
                    $ketprod = trim($ketprod);
                    $arrcol["stk-".$dbr1['kode']]   = array("caption"=>$dbr['keterangan'].";".$ketprod,"width"=>5,"wrap"=>1,"justification"=>"right");
                    $arrtot_col["stk-".$dbr1['kode']] = $arrcol["stk-".$dbr1['kode']];
                }

                if($span > 0)$arrkel[$dbr['kode']]=$dbr;
            }


            foreach($arrkel as $key => $val){
                $arrcol["jml-".$val['kode']] = array("caption"=>"Jumlah;".$val['keterangan'],"width"=>5,"wrap"=>1,"justification"=>"right");
                $arrtot_col["jml-".$val['kode']] = $arrcol["jml-".$val['kode']];
            }

            foreach($arrkel as $key => $val){
                $arrcol["pers-".$val['kode']] = array("caption"=>"Persentase;".$val['keterangan']." (%)","width"=>3,"wrap"=>1,"justification"=>"right");
                $arrtot_col["pers-".$val['kode']] = $arrcol["pers-".$val['kode']];
                
            }


            //Hitung Jumlah
            $total = array();
            $total[0]=array();
            $total[0]['keterangan'] = "<b>Total</b>";
            foreach($data as $key => $val){
                foreach($val as $key2 => $val2){
                    if(substr($key2,0,4) == "stk-" || substr($key2,0,4) == "jml-" || substr($key2,0,5) == "pers-"){  
                        
                        // if(substr($key2,0,5) == "pers-")$data[$key][$key2] = round($val2,2);
                        
                        if(!isset($total[0][$key2])) $total[0][$key2] = 0;
                        $total[0][$key2] += $val2;

                        $data[$key][$key2] = string_2s($val2,0);
                    }
                }
            }

            foreach($total as $key => $val){
                foreach($val as $key2 => $val2){
                    if(substr($key2,0,4) == "stk-" || substr($key2,0,4) == "jml-" || substr($key2,0,5) == "pers-"){  
                        
                        // if(substr($key2,0,5) == "pers-")$data[$key][$key2] = round($val2,2);

                        $total[$key][$key2] = "<b>".string_2s($val2,0)."</b>";
                    }
                }
            }

            $font = 7 ;
            $o    = array('paper'=>'A4', 'orientation'=>'L', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                        'opt'=>array('export_name'=>'LAPORAN PENJUALAN PRODUK PER '.$judul.$custgol) ) ;
            $this->load->library('bospdf', $o) ;
            $this->bospdf->ezText("<b>LAPORAN PENJUALAN PRODUK PER ".$judul.$custgol."</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("<b>Periode : " .date_2d($va['tglawal']). " s/d " . date_2d($va['tglakhir']) . "</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($data,"","",
                                array("fontSize"=>$font,
                                        "cols"=> $arrcol)
                                ) ;
            $this->bospdf->ezTable($total,"","",
                                array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>1,
                                        "cols"=> $arrtot_col
                                        )
                                ) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vttd,"","",
                                array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                        "cols"=> array(
                                            "1"=>array("justification"=>"right"),
                                            "2"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                            "3"=>array("width"=>40,"wrap"=>1),
                                            "4"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                            "5"=>array("wrap"=>1,"justification"=>"center"))
                                        )
                                ) ;
            $this->bospdf->ezStream() ;
        }else{
            echo('kosong') ;
        }
    }


    /******************************************** tab2 *******************************************************/
    public function initgrid2(){

        $va = $this->input->post() ;
        //print_r($va);
        $arrreturn = array("kolom"=>array(),"kolomgroup"=>array());
        $arrreturn['kolom'][] = array('field'=>"kode",'caption'=>"KODE",'size'=> '80px','style'=>'text-align:center','sortable'=>false,'frozen'=>true);
        $arrreturn['kolom'][] = array('field'=>"keterangan",'caption'=>"WILAYAH",'size'=> '200px','sortable'=>false,'frozen'=>true);
        
        $arrperiodawal = explode("-",$va['periodeawal']);
        $arrperiodakhir = explode("-",$va['periodeakhir']);
        $blnawal = $arrperiodawal[0];
        $thnawal = $arrperiodawal[1];

        $blnakhir = $arrperiodakhir[0];
        $thnakhir = $arrperiodakhir[1];
        
        $timeawal = mktime(0,0,0,$blnawal+1,0,$thnawal);//"01-".$val['bln']."-".$thn;
        $timeakhir = mktime(0,0,0,$blnakhir+1,0,$thnakhir);//"01-".$val['bln']."-".$thn;
        $n = 1;
        while($timeawal <= $timeakhir){
            $periode_bln = date("m",$timeawal);
            $periode_thn = date("Y",$timeawal);
            $arrreturn['kolom'][] = array('field'=>$periode_thn.'-'.$periode_bln,'caption'=>substr(date_month($periode_bln-1),0,3)." ".$periode_thn,'size'=> '100px','render'=>'float:2','sortable'=>false);
            
            $n++;
            $timeawal = mktime(0,0,0,$blnawal+$n,0,$thnawal);//"01-".$val['bln']."-".$thn;

        }
        
        $arrreturn['kolom'][] = array('field'=>"jumlah",'caption'=>"JUMLAH",'size'=> '100px','render'=>'float:2','sortable'=>false);
        $arrreturn['kolom'][] = array('field'=>"persentase",'caption'=>"PERSENTASE",'size'=> '100px','render'=>'float:2','sortable'=>false);

        


        $return = json_encode($arrreturn);
        echo($return);
    }

    public function loadgrid2(){ 
        $va     = json_decode($this->input->post('request'), true) ; 

        $arrperiodawal = explode("-",$va['periodeawal']);
        $arrperiodakhir = explode("-",$va['periodeakhir']);
        $blnawal = $arrperiodawal[0];
        $thnawal = $arrperiodawal[1];

        $blnakhir = $arrperiodakhir[0];
        $thnakhir = $arrperiodakhir[1];
        
        $timeawal = mktime(0,0,0,$blnawal,1,$thnawal);//"01-".$val['bln']."-".$thn;
        $timeakhir = mktime(0,0,0,$blnakhir+1,0,$thnakhir);//"01-".$val['bln']."-".$thn;

        $tglawal = date("Y-m-d",$timeawal);
        $tglakhir = date("Y-m-d",$timeakhir);

        $arrbln = array();
        while($timeawal <= $timeakhir){
            $blnawal++;
            $periode_bln = date("m",$timeawal);
            $periode_thn = date("Y",$timeawal);
            $arrbln[] = $periode_thn.'-'.$periode_bln;
            $timeawal = mktime(0,0,0,$blnawal,1,$thnawal);
        }

        // print_r($arrbln);

        $vare = array();
        $vare0 = array();
        $arrtot = array();
        $arrtot['jumlah'] = 0;
        $arrtot['persentase'] = 0;
        $arrtot['keterangan'] = "Total";
        $arrtot['w2ui'] = array("summary"=>true);
        
        //penjualan
        $join_pj = "left join penjualan_total t on t.faktur = d.faktur left join customer c on c.kode = t.customer left join dati_2 k on k.kode = c.dati_2";
        $wh_pj = "t.tgl >= '$tglawal' and t.tgl <= '$tglakhir'  and t.status = '1' and d.jenis_kelompok = 'P'";        
        if(isset($va['customer_gol']))$wh_pj .= " and c.golongan = '{$va['customer_gol']}'";
        if(isset($va['stock_kelompok']))$wh_pj .= " and d.stock_kelompok = '{$va['stock_kelompok']}'";
        $dbd_pj = $this->bdb->select("penjualan_detail d","left(t.tgl,7) periode,c.dati_2,ifnull(sum(d.qty),0) as qtypj,k.keterangan",$wh_pj,$join_pj,"concat(c.dati_2,left(t.tgl,7))");
        while($dbr_pj = $this->bdb->getrow($dbd_pj)){
            if(!isset($vare0[$dbr_pj['dati_2']])){
                $vare0[$dbr_pj['dati_2']] = array() ;
                $vare0[$dbr_pj['dati_2']]['recid'] = $dbr_pj['dati_2'];
                $vare0[$dbr_pj['dati_2']]['kode'] = $dbr_pj['dati_2'];
                $vare0[$dbr_pj['dati_2']]['keterangan'] = $dbr_pj['keterangan'];
                foreach($arrbln as $key => $val){
                    $vare0[$dbr_pj['dati_2']][$val] = 0;
                    
                    
                }

                $vare0[$dbr_pj['dati_2']]["jumlah"] = 0 ;
                $vare0[$dbr_pj['dati_2']]["persentase"] = 0 ;
            }

            $vare0[$dbr_pj['dati_2']][$dbr_pj['periode']] += $dbr_pj['qtypj'];
            $vare0[$dbr_pj['dati_2']]["jumlah"] += $dbr_pj['qtypj'];

            if(!isset($arrtot[$dbr_pj['periode']]))$arrtot[$dbr_pj['periode']] = 0;
            $arrtot[$dbr_pj['periode']] += $dbr_pj['qtypj'];
            $arrtot["jumlah"] += $dbr_pj['qtypj'];
        

        }

        //retur penjualan
        $join_pj = "left join penjualan_retur_total t on t.faktur = d.faktur left join customer c on c.kode = t.customer left join dati_2 k on k.kode = c.dati_2";
        $wh_pj = "t.tgl >= '$tglawal' and t.tgl <= '$tglakhir'  and t.status = '1' and d.jenis_kelompok = 'P'";        
        if(isset($va['customer_gol']))$wh_pj .= " and c.golongan = '{$va['customer_gol']}'";
        if(isset($va['stock_kelompok']))$wh_pj .= " and d.stock_kelompok = '{$va['stock_kelompok']}'";
        $dbd_pj = $this->bdb->select("penjualan_retur_detail d","left(t.tgl,7) periode,c.dati_2,ifnull(sum(d.qty),0) as qtypj,k.keterangan",$wh_pj,$join_pj,"concat(c.dati_2,left(t.tgl,7))");
        while($dbr_pj = $this->bdb->getrow($dbd_pj)){
            if(!isset($vare0[$dbr_pj['dati_2']])){
                $vare0[$dbr_pj['dati_2']] = array() ;
                $vare0[$dbr_pj['dati_2']]['recid'] = $dbr_pj['dati_2'];
                $vare0[$dbr_pj['dati_2']]['kode'] = $dbr_pj['dati_2'];
                $vare0[$dbr_pj['dati_2']]['keterangan'] = $dbr_pj['keterangan'];
                foreach($arrbln as $key => $val){
                    $vare0[$dbr_pj['dati_2']][$val] = 0;
                    
                    
                }

                $vare0[$dbr_pj['dati_2']]["jumlah"] = 0 ;
                $vare0[$dbr_pj['dati_2']]["persentase"] = 0 ;
            }

            $vare0[$dbr_pj['dati_2']][$dbr_pj['periode']] -= $dbr_pj['qtypj'];
            $vare0[$dbr_pj['dati_2']]["jumlah"] -= $dbr_pj['qtypj'];

            if(!isset($arrtot[$dbr_pj['periode']]))$arrtot[$dbr_pj['periode']] = 0;
            $arrtot[$dbr_pj['periode']] -= $dbr_pj['qtypj'];
            $arrtot["jumlah"] -= $dbr_pj['qtypj'];
        }

        // print_r($vare);

        foreach($vare0 as $key => $val){    
            $val["persentase"] = devide($val["jumlah"],$arrtot["jumlah"])*100;
            $arrtot["persentase"] += $val["persentase"];
            $vare[] = $val;
        }

        $vare[] = $arrtot;

        $vare 	= array("total"=>count($vare)-1, "records"=>$vare ) ; 
        echo(json_encode($vare)) ; 
    }
    
    public function initreport2(){
        $va      = $this->input->post() ;

        $file   = setfile($this, "rpt_pjbulan", __FILE__ , $va) ;
        savesession($this, $this->ss . "file2", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;

        $arrperiodawal = explode("-",$va['periodeawal']);
        $arrperiodakhir = explode("-",$va['periodeakhir']);
        $blnawal = $arrperiodawal[0];
        $thnawal = $arrperiodawal[1];

        $blnakhir = $arrperiodakhir[0];
        $thnakhir = $arrperiodakhir[1];
        
        $timeawal = mktime(0,0,0,$blnawal,1,$thnawal);//"01-".$val['bln']."-".$thn;
        $timeakhir = mktime(0,0,0,$blnakhir+1,0,$thnakhir);//"01-".$val['bln']."-".$thn;

        $tglawal = date("Y-m-d",$timeawal);
        $tglakhir = date("Y-m-d",$timeakhir);

        $arrbln = array();
        while($timeawal <= $timeakhir){
            $blnawal++;
            $periode_bln = date("m",$timeawal);
            $periode_thn = date("Y",$timeawal);
            $arrbln[] = $periode_thn.'-'.$periode_bln;
            $timeawal = mktime(0,0,0,$blnawal,1,$thnawal);
        }

        $vare = array();
        $vare0 = array();
        $arrtot = array();
        $arrtot['jumlah'] = 0;
        $arrtot['persentase'] = 0;

        

        //penjualan
        $join_pj = "left join penjualan_total t on t.faktur = d.faktur left join customer c on c.kode = t.customer left join dati_2 k on k.kode = c.dati_2";
        $wh_pj = "t.tgl >= '$tglawal' and t.tgl <= '$tglakhir'  and t.status = '1' and d.jenis_kelompok = 'P'";        
        if(isset($va['customer_gol']))$wh_pj .= " and c.golongan = '{$va['customer_gol']}'";
        if(isset($va['stock_kelompok']))$wh_pj .= " and d.stock_kelompok = '{$va['stock_kelompok']}'";
        $dbd_pj = $this->bdb->select("penjualan_detail d","left(t.tgl,7) periode,c.dati_2,ifnull(sum(d.qty),0) as qtypj,k.keterangan",$wh_pj,$join_pj,"concat(c.dati_2,left(t.tgl,7))");
        while($dbr_pj = $this->bdb->getrow($dbd_pj)){
            if(!isset($vare0[$dbr_pj['dati_2']])){
                $vare0[$dbr_pj['dati_2']] = array() ;
                $vare0[$dbr_pj['dati_2']]['no'] = 0;
                $vare0[$dbr_pj['dati_2']]['kode'] = $dbr_pj['dati_2'];
                $vare0[$dbr_pj['dati_2']]['keterangan'] = $dbr_pj['keterangan'];
                foreach($arrbln as $key => $val){
                    $vare0[$dbr_pj['dati_2']][$val] = 0;
                    
                    
                }

                $vare0[$dbr_pj['dati_2']]["jumlah"] = 0 ;
                $vare0[$dbr_pj['dati_2']]["persentase"] = 0 ;
            }

            $vare0[$dbr_pj['dati_2']][$dbr_pj['periode']] += $dbr_pj['qtypj'];
            $vare0[$dbr_pj['dati_2']]["jumlah"] += $dbr_pj['qtypj'];

            if(!isset($arrtot[$dbr_pj['periode']]))$arrtot[$dbr_pj['periode']] = 0;
            $arrtot[$dbr_pj['periode']] += $dbr_pj['qtypj'];
            $arrtot["jumlah"] += $dbr_pj['qtypj'];
        

        }

        //retur penjualan
        $join_pj = "left join penjualan_retur_total t on t.faktur = d.faktur left join customer c on c.kode = t.customer left join dati_2 k on k.kode = c.dati_2";
        $wh_pj = "t.tgl >= '$tglawal' and t.tgl <= '$tglakhir'  and t.status = '1' and d.jenis_kelompok = 'P'";        
        if(isset($va['customer_gol']))$wh_pj .= " and c.golongan = '{$va['customer_gol']}'";
        if(isset($va['stock_kelompok']))$wh_pj .= " and d.stock_kelompok = '{$va['stock_kelompok']}'";
        $dbd_pj = $this->bdb->select("penjualan_retur_detail d","left(t.tgl,7) periode,c.dati_2,ifnull(sum(d.qty),0) as qtypj,k.keterangan",$wh_pj,$join_pj,"concat(c.dati_2,left(t.tgl,7))");
        while($dbr_pj = $this->bdb->getrow($dbd_pj)){
            if(!isset($vare0[$dbr_pj['dati_2']])){
                $vare0[$dbr_pj['dati_2']] = array() ;
                $vare0[$dbr_pj['dati_2']]['no'] = 0;
                $vare0[$dbr_pj['dati_2']]['kode'] = $dbr_pj['dati_2'];
                $vare0[$dbr_pj['dati_2']]['keterangan'] = $dbr_pj['keterangan'];
                foreach($arrbln as $key => $val){
                    $vare0[$dbr_pj['dati_2']][$val] = 0;
                    
                    
                }

                $vare0[$dbr_pj['dati_2']]["jumlah"] = 0 ;
                $vare0[$dbr_pj['dati_2']]["persentase"] = 0 ;
            }

            $vare0[$dbr_pj['dati_2']][$dbr_pj['periode']] -= $dbr_pj['qtypj'];
            $vare0[$dbr_pj['dati_2']]["jumlah"] -= $dbr_pj['qtypj'];

            if(!isset($arrtot[$dbr_pj['periode']]))$arrtot[$dbr_pj['periode']] = 0;
            $arrtot[$dbr_pj['periode']] -= $dbr_pj['qtypj'];
            $arrtot["jumlah"] -= $dbr_pj['qtypj'];
        }

        foreach($vare0 as $key => $val){    
            $val["persentase"] = devide($val["jumlah"],$arrtot["jumlah"])*100;
            $arrtot["persentase"] += $val["persentase"];
            $vare[] = $val;
        }

        file_put_contents($file, json_encode($vare) ) ;
        echo(' bos.rptpenjualanproduk.openreport2() ; ') ;
    }

    public function showreport2(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $file = getsession($this, $this->ss . "file2") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        if(!empty($data)){
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> $kota ,"5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"Mengetahui,","3"=>"","4"=>"Menyetujui,","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"(.........................)","3"=>"","4"=>"(.........................)","5"=>"") ;

            // $nTotalTotal      = 0 ;
            // foreach ($data as $key => $value) {
            //     $nTotal           = str_replace(",", "", $value['total']) ;
            //     $nTotalTotal  += $nTotal;
            // }

            // $nTotalTotal      = number_format($nTotalTotal) ;
            $arrtot_col = array();
            $arrtot_col['keterangan'] = array("justification"=>"center");
            // $total[] = array("Ket"=>"<b>Total",
            //                 "JumlahTotalTotal"=>$nTotalTotal,
            //                 "Ket2"=>"</b>");


            $arrcol = array();
            $arrcol['no']       = array("caption"=>"No","width"=>2,"wrap"=>1,"justification"=>"right");
            $arrcol['kode']     = array("caption"=>"Kode","width"=>4,"justification"=>"center");
            $arrcol['keterangan']     = array("caption"=>"Wilayah","wrap"=>1,"justification"=>"left");
            
            $arrperiodawal = explode("-",$va['periodeawal']);
            $arrperiodakhir = explode("-",$va['periodeakhir']);
            $blnawal = $arrperiodawal[0];
            $thnawal = $arrperiodawal[1];

            $blnakhir = $arrperiodakhir[0];
            $thnakhir = $arrperiodakhir[1];
            
            $timeawal = mktime(0,0,0,$blnawal+1,0,$thnawal);//"01-".$val['bln']."-".$thn;
            $timeakhir = mktime(0,0,0,$blnakhir+1,0,$thnakhir);//"01-".$val['bln']."-".$thn;
            $n = 1;
            $arrbln = array();
            while($timeawal <= $timeakhir){
                $periode_bln = date("m",$timeawal);
                $periode_thn = date("Y",$timeawal);

                $arrcol[$periode_thn.'-'.$periode_bln]   = array("caption"=>substr(date_month($periode_bln-1),0,3)." ".$periode_thn,"width"=>5,"wrap"=>1,"justification"=>"right");
                $arrtot_col[$periode_thn.'-'.$periode_bln] = $arrcol[$periode_thn.'-'.$periode_bln];
                $n++;

                $arrbln[] = $periode_thn.'-'.$periode_bln;
                $timeawal = mktime(0,0,0,$blnawal+$n,0,$thnawal);//"01-".$val['bln']."-".$thn;

            }


            $arrcol["jumlah"] = array("caption"=>"Jumlah","width"=>5,"wrap"=>1,"justification"=>"right");
            $arrtot_col["jumlah"] = $arrcol["jumlah"];

            $arrcol["persentase"] = array("caption"=>"Persentase (%)","width"=>3,"wrap"=>1,"justification"=>"right");
            $arrtot_col["persentase"] = $arrcol["persentase"];


            //Hitung Jumlah
            $total = array();
            $total[0]=array();
            $total[0]['keterangan'] = "<b>Total</b>";
            $n = 0 ;
            foreach($data as $key => $val){
                $data[$key]['no'] = ++$n;
                foreach($val as $key2 => $val2){
                    if($key2 <> "no" && $key2 <> "kode" && $key2 <> "keterangan" ){  
                        
                        if(!isset($total[0][$key2])) $total[0][$key2] = 0;
                        $total[0][$key2] += $val2;

                        $data[$key][$key2] = string_2s($val2,0);
                    }
                }
            }

            foreach($total as $key => $val){
                foreach($val as $key2 => $val2){
                    if($key2 <> "no" && $key2 <> "kode" && $key2 <> "keterangan" ){  
                        
                        // if(substr($key2,0,5) == "pers-")$data[$key][$key2] = round($val2,2);

                        $total[$key][$key2] = "<b>". string_2s($val2,0) ."</b>";
                    }
                }
            }

            $font = 7 ;
            $o    = array('paper'=>'A4', 'orientation'=>'L', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                        'opt'=>array('export_name'=>'LAPORAN PENJUALAN PRODUK PER CUSTOMER') ) ;
            $this->load->library('bospdf', $o) ;
            $stock_kelompok = "";
            if(isset($va['stock_kelompok']))$stock_kelompok = $this->bdb->getval("keterangan", "kode = '{$va['stock_kelompok']}'", "stock_kelompok");
            $custgol = "";
            if(isset($va['stock_kelompok']))$custgol = $this->bdb->getval("keterangan", "kode = '{$va['customer_gol']}'", "customer_golongan");
            $this->bospdf->ezText("<b>LAPORAN PENJUALAN PRODUK ".$stock_kelompok." ".$custgol."</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("<b>Periode : " .$va['periodeawal']. " s/d " . $va['periodeakhir'] . "</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($data,"","",
                                array("fontSize"=>$font,
                                        "cols"=> $arrcol)
                                ) ;
            $this->bospdf->ezTable($total,"","",
                                array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>1,
                                        "cols"=> $arrtot_col
                                        )
                                ) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vttd,"","",
                                array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                        "cols"=> array(
                                            "1"=>array("justification"=>"right"),
                                            "2"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                            "3"=>array("width"=>40,"wrap"=>1),
                                            "4"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                            "5"=>array("wrap"=>1,"justification"=>"center"))
                                        )
                                ) ;
            $this->bospdf->ezStream() ;
        }else{
            echo('kosong') ;
        }
    }

    public function reloadgrafik(){
        $va     = $this->input->post() ;
        //print_r($va);
        $judul = $this->bdb->getval("keterangan", "kode = '{$va['customer_gol']}'", "customer_golongan");
        $return = array("datas"=>array(),"labels"=>array(),"judul"=>$judul." Periode : ".$va['periodeawal']." sd ".$va['periodeakhir']);
        
        $arrperiodawal = explode("-",$va['periodeawal']);
        $arrperiodakhir = explode("-",$va['periodeakhir']);
        $blnawal = $arrperiodawal[0];
        $thnawal = $arrperiodawal[1];

        $blnakhir = $arrperiodakhir[0];
        $thnakhir = $arrperiodakhir[1];
        
        $timeawal = mktime(0,0,0,$blnawal,1,$thnawal);//"01-".$val['bln']."-".$thn;
        $timeakhir = mktime(0,0,0,$blnakhir+1,0,$thnakhir);//"01-".$val['bln']."-".$thn;

        $tglawal = date("Y-m-d",$timeawal);
        $tglakhir = date("Y-m-d",$timeakhir);
        $arrbln = array();
        while($timeawal <= $timeakhir){
            $blnawal++;
            $periode_bln = date("m",$timeawal);
            $periode_thn = date("Y",$timeawal);
            $timeawal = mktime(0,0,0,$blnawal,1,$thnawal);
            $arrbln[] = $periode_thn.'-'.$periode_bln;

            $return['labels'][] = $periode_bln.'-'.$periode_thn;
        }
        $n=0;
        $arrwarna = array("#21ab60","#36A2EB","#fcba03","#fc03f8","#03f0fc");
        $dbd = $this->bdb->select("stock_kelompok","kode,keterangan","jenis = 'P'");
        while($dbr = $this->bdb->getrow($dbd)){
            //penjualan
            $omset = array();
            foreach($arrbln as $key => $val){
                $omset[$val] = 0;
            }
            
            $join_pj = "left join penjualan_total t on t.faktur = d.faktur left join customer c on c.kode = t.customer";
            $wh_pj = "c.golongan = '{$va['customer_gol']}' and t.tgl >= '$tglawal' and t.tgl <= '$tglakhir'  and t.status = '1' and d.stock_kelompok = '{$dbr['kode']}'";        
            $dbd_pj = $this->bdb->select("penjualan_detail d","left(t.tgl,7) periode,ifnull(sum(d.qty),0) as qtypj",$wh_pj,$join_pj,"left(t.tgl,7)");
            while($dbr_pj = $this->bdb->getrow($dbd_pj)){
                $omset[$dbr_pj['periode']] = $dbr_pj['qtypj'];
            }

            $data = array();
            $i=0;
            foreach($omset as $key => $val){
                $data[$i] = (int)$val;
                $i++;
            }

            if(!isset($arrwarna[$n]))$arrwarna[$n] = "#fc0324";
            $return['datas'][] = array("label"=>$dbr['keterangan'],"data"=>$data,"backgroundColor"=>$arrwarna[$n],
                    "borderColor"=>$arrwarna[$n],"borderWidth"=>1);
            $n++;

        }

       
        /*$data[] = array("label"=>$thn-1,"data"=>$omsetthnkemarin,"backgroundColor"=>"transparent",
                       "borderColor"=>"#36A2EB","borderWidth"=>1);
        */
        
        //                $data[] = array("label"=>$thn,"data"=>$omsetthnini,"backgroundColor"=>array("rgba(255, 99, 132, 0)"),
        //                "borderColor"=>array("rgba(255,99,132,1)"),"borderWidth"=>1);
        // $data[] = array("label"=>$thn-1,"data"=>$omsetthnkemarin,"backgroundColor"=>array("rgba(54, 162, 235, 0)"),
        //                "borderColor"=>array("rgba(54, 162, 235, 1)"),"borderWidth"=>1);
        //               print_r($data);
        $return = json_encode($return);    
        echo($return);
    }
}


?>
