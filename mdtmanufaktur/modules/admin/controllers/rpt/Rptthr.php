<?php
class Rptthr extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("rpt/rptthr_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->load->model("func/updtransaksi_m") ;
        $this->load->model("func/func_m") ;
        
        $this->bdb 	= $this->rptthr_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptthr2_" ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";


        $arrkolom = array();
        $arrkolom[] = array("field"=> "kode", "caption"=> "NIP", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "nama", "caption"=> "Nama", "size"=> "200px", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "bagian", "caption"=> "Bagian", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "jabatan", "caption"=> "Jabatan", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        
        $arrkolom[] = array("field"=> "thr", "caption"=> "THR", "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        $arrkolom[] = array("field"=> "cetakslip", "caption"=> "", "size"=> "100px", "sortable"=>false);

        $data['kolomgrid1'] = json_encode($arrkolom);
        $this->load->view("rpt/rptthr",$data) ;

    } 

   
    public function loadgrid(){
        $va = json_decode($this->input->post('request'), true);
        //print_r($va);
        $vare   = array() ;
        $vdb    = $this->rptthr_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;
        
        //cek status sudah posting atau belum
        $sudahposting = false;
        $faktur = "";
        $cabang = getsession($this, "cabang");
        $va['tgl'] = date_2s($va['tgl']);
        $dbd2 = $this->rptthr_m->select("thr_posting","faktur","tgl = '{$va['tgl']}' and cabang = '$cabang' and status = '1'");
        if($dbr2 = $this->rptthr_m->getrow($dbd2)){
            $sudahposting = true;
            $faktur = $dbr2['faktur'];
        }

        $total = array();
        $total['recid'] = "ZZZZZ" ;
        $total['nama'] = "Total" ;
        $total['thr'] = 0 ;
        
        while( $dbr = $this->rptthr_m->getrow($dbd) ){
            $n++;

            //jab bag
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$va['tgl']);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['kode'],$va['tgl']);
            
            $vaset = $dbr;
            $gajikotor = 0 ;
            $vaset['jabatan'] = $jabatan['keterangan'];
            $vaset['bagian'] = $bagian['keterangan'];
            if($sudahposting){
                $thr = $this->perhitunganhrd_m->getthrkaryawanposting($dbr['kode'],$va['tgl']);
            }else{
                $thr = $this->perhitunganhrd_m->getthrkaryawan($dbr['kode'],$va['tgl']);
            }

           // print_r($thr);
            $vaset['thr'] = $thr['total'];
            $vare[]		= $vaset ;

            $total['thr'] += $vaset['thr'];
        }
        $total['w2ui']= array("summary"=> true);
        $vare[] = $total;

        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function init(){
        savesession($this, "ssrptthr_kode", "") ;
    }

   public function loadperiode(){
        $va 	= $this->input->post() ;
        $vdb    = $this->rptthr_m->getperiode($va) ;
        if( $dbr = $this->rptthr_m->getrow($vdb['db']) ){
            $status = "-- Belum diposting --";
            $faktur = "";
            $va['tgl'] = date_2s($va['tgl']);
            $cabang = getsession($this, "cabang");
            $dbd2 = $this->rptthr_m->select("thr_posting","faktur","tgl = '{$va['tgl']}' and cabang = '$cabang' and status = '1'");
            if($this->rptthr_m->rows($dbd2) > 0){
                $status = "Sudah diposting";
                if($dbr2 = $this->rptthr_m->getrow($dbd2)){
                    $faktur = $dbr2['faktur'];
                }
            }
            echo('
                with(bos.rptthr.obj){\
                    find("#status").val("'.$status.'") ;
                    find("#faktur").val("'.$faktur.'") ;
                }
            ') ;
            
            
             
        }
   }

    public function loadpembayaran(){
        $va 	= $this->input->post() ;
        $cabang = getsession($this, "cabang");
        $faktur = "";
        $total = 0;
        $va['tgl'] = date_2s($va['tgl']);
        $dbd2 = $this->rptthr_m->select("thr_posting","faktur","tgl = '{$va['tgl']}' and cabang = '$cabang' and status = '1'");
        if($dbr2 = $this->rptthr_m->getrow($dbd2)){
            $faktur = $dbr2['faktur'];

            //loadgrid pembayaran
            $vare = array();
            $dbd = $this->rptthr_m->getpembayaran($faktur) ;
            $n = 0 ;
            
            while( $dbr = $this->rptthr_m->getrow($dbd) ){
                $n++;
                $vaset   = $dbr ;
                $vaset['recid'] = $n;
                $vaset['no'] = $n;

                $vare[]             = $vaset ;
                $total += $dbr['nominal'];
            }

            
        }
        $vare[] = array("recid"=>"ZZZZ","no"=>"","kode"=>"","ketketerangan"=>"Total","nominal"=>$total,"w2ui"=>array("summary"=>true));

        $vare = json_encode($vare);
            echo('
                w2ui["bos-form-rptthr_grid2"].add('.$vare.');
                
        ');
    }

    public function initreport(){
        $n=0;
        $va     = $this->input->post() ;
        
        savesession($this, $this->ss . "va", json_encode($va) ) ;

        //print_r($va);
        $vare   = array() ;
        $vdb    = $this->rptthr_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;
        
        //cek status sudah posting atau belum
        $sudahposting = false;
        $faktur = "";
        $cabang = getsession($this, "cabang");
        $va['tgl'] = date_2s($va['tgl']);
        $dbd2 = $this->rptthr_m->select("thr_posting","faktur","tgl = '{$va['tgl']}' and cabang = '$cabang' and status = '1'");
        if($dbr2 = $this->rptthr_m->getrow($dbd2)){
            $sudahposting = true;
            $faktur = $dbr2['faktur'];
        }

        while( $dbr = $this->rptthr_m->getrow($dbd) ){
            $n++;

            //jab bag
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$va['tgl']);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['kode'],$va['tgl']);
            $vaset = array();
            $gajikotor = 0 ;
            $vaset['no'] = $n;
            $vaset['nip'] = $dbr['kode'];
            $vaset['nama'] = $dbr['nama'];            
            $vaset['jabatan'] = $jabatan['keterangan'];
            $vaset['bagian'] = $bagian['keterangan'];
            if($sudahposting){
                $thr = $this->perhitunganhrd_m->getthrkaryawanposting($dbr['kode'],$va['tgl']);
            }else{
                $thr = $this->perhitunganhrd_m->getthrkaryawan($dbr['kode'],$va['tgl']);
            }

           // print_r($thr);
            $vaset['thr'] = $thr['total'];
            $vare[]		= $vaset ;   
        }
        savesession($this, "rptthr_rpt", json_encode($vare)) ; 
        echo(' bos.rptthr.openreport() ; ') ;
    }

    public function showreport(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        //print_r($va);
        $data = getsession($this,"rptthr_rpt") ;      
        $data = json_decode($data,true) ;

        //menyusun total
        $arrtot = array();
        $arrtot['ket'] = "<b>Total";
        $arrtot['thr'] = 0 ;
        foreach($data as $key => $val){
            $arrtot['thr'] += string_2n($val['thr']);
            $data[$key]['thr'] = string_2s($val['thr']);
        }

        //number 2 string total
        foreach($arrtot as $key => $val){
            if($key <> "ket")$arrtot[$key] = string_2s($val);
        }
        $total[] = $arrtot;

        //menyusun kolom
        $arrcoltot = array();
        $arrcoltot['ket'] = array("caption"=>"Nama","justification"=>"center");

        $arrcoldata = array();
        $arrcoldata['no'] = array("caption"=>"No","width"=>4,"justification"=>"right");
        $arrcoldata['nip'] = array("caption"=>"NIP","width"=>8,"justification"=>"center");
        $arrcoldata['nama'] = array("caption"=>"Nama","justification"=>"left");
        $arrcoldata['jabatan'] = array("caption"=>"Jabatan","justification"=>"left");
        $arrcoldata['bagian'] = array("caption"=>"Bagian","justification"=>"left");
        $arrcoldata["thr"] = array("caption"=>"THR","width"=>12,"justification"=>"right");

        $arrcoltot["thr"] = array("caption"=>"THR","width"=>12,"justification"=>"right");


        if(!empty($data)){ 
            $font = 6 ;
            if(!isset($va['export']) || $va['export'] == "")$va['export'] = 0 ;
            $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>$va['export'],
                          'opt'=>array('export_name'=>'DaftarTHR_' . getsession($this, "username") ) ) ;
            $this->load->library('bospdf', $o) ;   
            $cabang = getsession($this,"cabang");
            $arrcab = $this->func_m->GetDataCabang($cabang);
            $this->bospdf->ezText($arrcab['kode'] ." - ".$arrcab['nama'],$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText($arrcab['alamat'] . " / ". $arrcab['telp'],$font,array("justification"=>"center")) ;
            $this->bospdf->ezText("<b>Daftar Tunjangan Hari Raya (THR)</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("Tgl : ".date_2d($va['tgl']),$font+2,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ; 
            $this->bospdf->ezTable($data,"","",array("showLines"=>2,"showHeadings"=>"1","fontSize"=>$font,"cols"=> $arrcoldata)) ;   
            $this->bospdf->ezTable($total,"","",array("showLines"=>2,"showHeadings"=>"","fontSize"=>$font,"cols"=> $arrcoltot)) ;   
            //print_r($data) ;    
            $this->bospdf->ezStream() ; 
        }else{
            echo('data kosong') ;
        }
    }
}
?>
