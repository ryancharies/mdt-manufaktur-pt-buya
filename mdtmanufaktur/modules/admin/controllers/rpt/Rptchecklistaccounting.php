<?php

class Rptchecklistaccounting extends Bismillah_Controller
{
    protected $bdb;
    protected $ss;
    protected $abc;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('bdate');
        $this->load->model('rpt/rptchecklistaccounting_m');
        $this->load->model('func/perhitungan_m');
        $this->load->model('func/perhitunganhrd_m');
        $this->bdb = $this->rptchecklistaccounting_m;
        $this->ss = "ssrptchecklistaccounting_";
    }

    public function index()
    {
        $d = array("setdate" => date_set());
        $this->load->view('rpt/rptchecklistaccounting', $d);
    }

    public function loadgrid()
    {
        $va = json_decode($this->input->post('request'), true);
        $data = array();
        $data[] = array("no" => "", "keterangan" => "<b>:: Persediaan ::</b>", "neraca" => "", "nominatif" => "", "status" => "");

        //load persediaan
        $arrpersd = $this->rptchecklistaccounting_m->checkpersediaan($va['tglakhir']);
        $n = 0;
        foreach ($arrpersd as $key => $val) {
            $n++;
            $selisih = $val['neraca'] - $val['nominatif'];
            $status = "";
            if ($selisih != 0) {
                $status .= "Selisih " . string_2s($selisih);
            }
            $data[] = array("no" => $n, "keterangan" => $val['rekening'] . "-" . $val['keterangan'], "neraca" => $val['neraca'],
                "nominatif" => $val['nominatif'], "status" => $status);
        }
        
        $data[] = array("no" => "", "keterangan" => "", "neraca" => "", "nominatif" => "", "status" => "");
        $data[] = array("no" => "", "keterangan" => "<b>:: Saldo Hutang Pembelian ::</b>", "neraca" => "", "nominatif" => "", "status" => "");
        //hutang stock
        $arrhut = $this->rptchecklistaccounting_m->checksaldohutangpb($va['tglakhir']);
        $selisih = $arrhut['neraca'] - $arrhut['nominatif'];
        $status = "";
        if ($selisih != 0) {
            $status .= "Selisih " . string_2s($selisih);
        }
        $data[] = array("no" => ++$n, "keterangan" => "Saldo Hutang Pembelian Stock", "neraca" => $arrhut['neraca'], 
                "nominatif" => $arrhut['nominatif'], "status" => $status);

        //hutang aset
        $data[] = array("no" => "", "keterangan" => "", "neraca" => "", "nominatif" => "", "status" => "");
        $arrhutas = $this->rptchecklistaccounting_m->checksaldohutangas($va['tglakhir']);
        foreach ($arrhutas as $key => $val) {
            $n++;
            $data[] = array("no" => $n, "keterangan" =>$val['ketrekening'], "neraca" => "",
                "nominatif" => "", "status" => "");

            foreach($val['detail'] as $keyd => $vald){
                $data[] = array("no" => "", "keterangan" =>$keyd ." - ". $vald['keterangan'], "neraca" => "",
                "nominatif" => $vald['nominatif'], "status" => "");
            }


            $selisih = $val['neraca'] - $val['nominatif'];
            $status = "";
            if ($selisih != 0) {
                $status .= "Selisih " . string_2s($selisih);
            }

            $data[] = array("no" => "", "keterangan" =>"<b>Total ".$val['ketrekening']."</b>", "neraca" => $val['neraca'],
                "nominatif" => $val['nominatif'], "status" => $status);
            $data[] = array("no" => "", "keterangan" => "", "neraca" => "", "nominatif" => "", "status" => "");
            
        }

        

        //porsekot
        $data[] = array("no" => "", "keterangan" => "<b>:: Porsekot ::</b>", "neraca" => "", "nominatif" => "", "status" => "");

        
        $arrpersd = $this->rptchecklistaccounting_m->checksaldoporsekot($va['tglakhir']);
        
        foreach ($arrpersd as $key => $val) {
            $n++;
            $selisih = $val['neraca'] - $val['nominatif'];
            $status = "";
            if ($selisih != 0) {
                $status .= "Selisih " . string_2s($selisih);
            }
            $data[] = array("no" => $n, "keterangan" => $val['rekening'] . "-" . $val['ketrekening'], "neraca" => $val['neraca'],
                "nominatif" => $val['nominatif'], "status" => $status);
        }
        $data[] = array("no" => "", "keterangan" => "", "neraca" => "", "nominatif" => "", "status" => "");
        
        //piutang
        $arrpiut = $this->rptchecklistaccounting_m->checksaldopiutangpj($va['tglakhir']);
        $selisih = $arrpiut['neraca'] - $arrpiut['nominatif'];
        $status = "";
        if ($selisih != 0) {
            $status .= "Selisih " . string_2s($selisih);
        }
        $data[] = array("no" => ++$n, "keterangan" => "Saldo Piutang", "neraca" => $arrpiut['neraca'], 
                "nominatif" => $arrpiut['nominatif'], "status" => $status);

        //Uang Muka
        $arrum = $this->rptchecklistaccounting_m->checksaldoum($va['tglakhir']);
        $selisih = $arrum['neraca'] - $arrum['nominatif'];
        $status = "";
        if ($selisih != 0) {
            $status .= "Selisih " . string_2s($selisih);
        }
        $data[] = array("no" => ++$n, "keterangan" => "Saldo Uang Muka", "neraca" => $arrum['neraca'], 
        "nominatif" => $arrum['nominatif'], "status" => $status);

        //Deposit
        $arrum = $this->rptchecklistaccounting_m->checksaldodp($va['tglakhir']);
        $selisih = $arrum['neraca'] - $arrum['nominatif'];
        $status = "";
        if ($selisih != 0) {
            $status .= "Selisih " . string_2s($selisih);
        }
        $data[] = array("no" => ++$n, "keterangan" => "Saldo Deposit", "neraca" => $arrum['neraca'], 
        "nominatif" => $arrum['nominatif'], "status" => $status);

        //Hutang BG
        $arrbg = $this->rptchecklistaccounting_m->checksaldohutbg($va['tglakhir']);
        $selisih = $arrbg['neraca'] - $arrbg['nominatif'];
        $status = "";
        if ($selisih != 0) {
            $status .= "Selisih " . string_2s($selisih);
        }
        $data[] = array("no" => ++$n, "keterangan" => "Saldo Hutang BG", "neraca" => $arrbg['neraca'], 
        "nominatif" => $arrbg['nominatif'], "status" => $status);

        //Saldo Kasbon / piutang karyawan
        $arrbg = $this->rptchecklistaccounting_m->checksaldokasbonkaryawan($va['tglakhir']);
        //print_r($arrbg);
        foreach($arrbg as $key => $val){
            $n++;
            $selisih = $val['neraca'] - $val['nominatif'];
            $status = "";
            if ($selisih != 0) {
                $status .= "Selisih " . string_2s($selisih);
            }
            $data[] = array("no" => $n, "keterangan" => $val['rekening'] . " - " . $val['keterangan'], "neraca" => $val['neraca'],
                "nominatif" => $val['nominatif'], "status" => $status);
        }
        

        $vare = array("total" => count($data), "records" => $data);
        echo (json_encode($vare));
    }

    public function initreporttotal(){
        $va         = $this->input->post() ;
        //print_r($va);
        $file       = setfile($this, "rpt_checklistaccounting", __FILE__ , $va) ;
        savesession($this, $this->ss . "file", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        file_put_contents($file, json_encode(array()) ) ;

        $file    = getsession($this, $this->ss . "file") ;
        $data    = @file_get_contents($file) ;
        $data    = json_decode($data,true) ;

        $data[] = array("no" => "<b>", "keterangan" => ":: Persediaan ::", "neraca" => "", "nominatif" => "", "status" => "</b>");

        //load persediaan
        $arrpersd = $this->rptchecklistaccounting_m->checkpersediaan($va['tglakhir']);
        $n = 0;
        foreach ($arrpersd as $key => $val) {
            $n++;
            $selisih = $val['neraca'] - $val['nominatif'];
            $status = "";
            if ($selisih != 0) {
                $status .= "Selisih " . string_2s($selisih);
            }
            $data[] = array("no" => $n, "keterangan" => $val['rekening'] . "-" . $val['keterangan'], "neraca" => string_2s($val['neraca']),
                "nominatif" => string_2s($val['nominatif']), "status" => $status);
        }
        
        $data[] = array("no" => "", "keterangan" => "", "neraca" => "", "nominatif" => "", "status" => "");
        $data[] = array("no" => "<b>", "keterangan" => ":: Saldo Hutang Pembelian ::", "neraca" => "", "nominatif" => "", "status" => "</b>");
        //hutang stock
        $arrhut = $this->rptchecklistaccounting_m->checksaldohutangpb($va['tglakhir']);
        $selisih = $arrhut['neraca'] - $arrhut['nominatif'];
        $status = "";
        if ($selisih != 0) {
            $status .= "Selisih " . string_2s($selisih);
        }
        $data[] = array("no" => ++$n, "keterangan" => "Saldo Hutang Pembelian Stock", "neraca" => string_2s($arrhut['neraca']), 
                "nominatif" => string_2s($arrhut['nominatif']), "status" => $status);

        //hutang aset
        $data[] = array("no" => "", "keterangan" => "", "neraca" => "", "nominatif" => "", "status" => "");
        $arrhutas = $this->rptchecklistaccounting_m->checksaldohutangas($va['tglakhir']);
        foreach ($arrhutas as $key => $val) {
            $n++;
            $data[] = array("no" => $n, "keterangan" =>$val['ketrekening'], "neraca" => "",
                "nominatif" => "", "status" => "");

            foreach($val['detail'] as $keyd => $vald){
                $data[] = array("no" => "", "keterangan" =>$keyd ." - ". $vald['keterangan'], "neraca" => "",
                "nominatif" => string_2s($vald['nominatif']), "status" => "");
            }


            $selisih = $val['neraca'] - $val['nominatif'];
            $status = "";
            if ($selisih != 0) {
                $status .= "Selisih " . string_2s($selisih);
            }

            $data[] = array("no" => "<b>", "keterangan" =>"Total ".$val['ketrekening'],"neraca" => string_2s($val['neraca']),
                "nominatif" => string_2s($val['nominatif']), "status" => $status."</b>");
            $data[] = array("no" => "", "keterangan" => "", "neraca" => "", "nominatif" => "", "status" => "");
            
        }

        

        //porsekot
        $data[] = array("no" => "<b>", "keterangan" => ":: Porsekot ::", "neraca" => "", "nominatif" => "", "status" => "</b>");

        
        $arrpersd = $this->rptchecklistaccounting_m->checksaldoporsekot($va['tglakhir']);
        
        foreach ($arrpersd as $key => $val) {
            $n++;
            $selisih = $val['neraca'] - $val['nominatif'];
            $status = "";
            if ($selisih != 0) {
                $status .= "Selisih " . string_2s($selisih);
            }
            $data[] = array("no" => $n, "keterangan" => $val['rekening'] . "-" . $val['ketrekening'], "neraca" => string_2s($val['neraca']),
                "nominatif" => string_2s($val['nominatif']), "status" => $status);
        }
        $data[] = array("no" => "", "keterangan" => "", "neraca" => "", "nominatif" => "", "status" => "");
        
        //piutang
        $arrpiut = $this->rptchecklistaccounting_m->checksaldopiutangpj($va['tglakhir']);
        $selisih = $arrpiut['neraca'] - $arrpiut['nominatif'];
        $status = "";
        if ($selisih != 0) {
            $status .= "Selisih " . string_2s($selisih);
        }
        $data[] = array("no" => ++$n, "keterangan" => "Saldo Piutang", "neraca" => string_2s($arrpiut['neraca']), 
                "nominatif" => string_2s($arrpiut['nominatif']), "status" => $status);

        //Uang Muka
        $arrum = $this->rptchecklistaccounting_m->checksaldoum($va['tglakhir']);
        $selisih = $arrum['neraca'] - $arrum['nominatif'];
        $status = "";
        if ($selisih != 0) {
            $status .= "Selisih " . string_2s($selisih);
        }
        $data[] = array("no" => ++$n, "keterangan" => "Saldo Uang Muka", "neraca" => string_2s($arrum['neraca']), 
        "nominatif" => string_2s($arrum['nominatif']), "status" => $status);

        //Deposit
        $arrum = $this->rptchecklistaccounting_m->checksaldodp($va['tglakhir']);
        $selisih = $arrum['neraca'] - $arrum['nominatif'];
        $status = "";
        if ($selisih != 0) {
            $status .= "Selisih " . string_2s($selisih);
        }
        $data[] = array("no" => ++$n, "keterangan" => "Saldo Deposit", "neraca" => string_2s($arrum['neraca']), 
        "nominatif" => string_2s($arrum['nominatif']), "status" => $status);

        //Hutang BG
        $arrbg = $this->rptchecklistaccounting_m->checksaldohutbg($va['tglakhir']);
        $selisih = $arrbg['neraca'] - $arrbg['nominatif'];
        $status = "";
        if ($selisih != 0) {
            $status .= "Selisih " . string_2s($selisih);
        }
        $data[] = array("no" => ++$n, "keterangan" => "Saldo Hutang BG", "neraca" => string_2s($arrbg['neraca']), 
        "nominatif" => string_2s($arrbg['nominatif']), "status" => $status);


        //Saldo Kasbon / piutang karyawan
        $arrbg = $this->rptchecklistaccounting_m->checksaldokasbonkaryawan($va['tglakhir']);
        //print_r($arrbg);
        foreach($arrbg as $key => $val){
            $n++;
            $selisih = $val['neraca'] - $val['nominatif'];
            $status = "";
            if ($selisih != 0) {
                $status .= "Selisih " . string_2s($selisih);
            }
            $data[] = array("no" => $n, "keterangan" => $val['rekening'] . " - " . $val['keterangan'], "neraca" => string_2s($val['neraca']),
                "nominatif" => string_2s($val['nominatif']), "status" => $status);
        }

        file_put_contents($file, json_encode($data) ) ;
        echo(' bos.rptchecklistaccounting.openreporttotal() ; ') ;
    }

    public function showreporttotal(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $file = getsession($this, $this->ss . "file") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        if(!empty($data)){
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> $kota ,"5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"Mengetahui,","3"=>"","4"=>"Menyetujui,","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"(.........................)","3"=>"","4"=>"(.........................)","5"=>"") ;
          


            $font = 8 ;
            $o    = array('paper'=>'A4', 'orientation'=>'P', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                          'opt'=>array('export_name'=>'Kartu Stock') ) ;
            $this->load->library('bospdf', $o) ;
            $this->bospdf->ezText("<b>Cheklist Accounting</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("<b>Periode : " .$va['tglakhir'] . "</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($data,"","",
                                   array("fontSize"=>$font,
                                         "cols"=> array(
                                             "no"=>array("caption"=>"No","width"=>4,"justification"=>"right"),
                                             "keterangan"=>array("caption"=>"Keterangan","wrap"=>1),
                                             "neraca"=>array("caption"=>"Neraca","width"=>14,"justification"=>"right"),
                                             "nominatif"=>array("caption"=>"Nominatif","width"=>14,"justification"=>"right"),
                                             "status"=>array("caption"=>"status","wrap"=>1,"width"=>20)
                                         ))
                                  ) ;

            
            $this->bospdf->ezStream() ;
        }else{
            echo('kosong') ;
        }
    }
}
