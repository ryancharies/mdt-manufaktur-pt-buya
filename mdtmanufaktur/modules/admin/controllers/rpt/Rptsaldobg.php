<?php

class Rptsaldobg extends Bismillah_Controller{
    protected $bdb ;
    protected $ss ;
    protected $abc ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper('bdate');
        $this->load->model('rpt/rptsaldobg_m') ;
        $this->load->model('func/perhitungan_m') ;
        $this->bdb = $this->rptsaldobg_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptsaldobg_" ;
    }

    public function index(){
        $d    = array("setdate"=>date_set()) ;
        $this->load->view('rpt/rptsaldobg', $d) ;
    }

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $va['tgl'] = date_2s($va['tgl']);
        $vdb    = $this->rptsaldobg_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;
        $totsaldo = 0;
        $key = "";
        $totalgol = 0;
        while( $dbr = $this->rptsaldobg_m->getrow($dbd) ){
            $n++;
            $vaset = $dbr;


            if($key <> $dbr['kpddari']){
				if($n > 1){
					$vare[] = array("no"=>"",'ketbank'=>'Sub Total',"nominal"=>$totalgol);
					$totalgol = 0 ;
					$vare[] = array();
                }
                
                $vare[] = array('ketbank'=>$dbr['namasupplier']);
				
			}

            $vaset['no'] = $n;
            $totsaldo += $vaset['nominal'];
            $totalgol += $vaset['nominal'];
            $vare[]     = $vaset ;

            $key = $dbr['kpddari'];
        }
        $vare[] = array("no"=>"",'ketbank'=>'Sub Total',"nominal"=>$totalgol);
		$vare[] = array("recid"=>'ZZZZ','ketbank'=>'TOTAL',"nominal"=>$totsaldo,"w2ui"=>array("summary"=> true));

        $vare    = array("total"=>count($vare)-1, "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }
	
	public function initreport(){
	  $va     = $this->input->post() ;
      savesession($this, $this->ss . "va", json_encode($va) ) ;
      $vare   = array() ;
      $va['tgl'] = date_2s($va['tgl']);
      $vdb    = $this->rptsaldobg_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      $n = 0 ;
      
      while( $dbr = $this->rptsaldobg_m->getrow($dbd) ){
		$n++;
        $key = $dbr['kpddari'];
        $vare[$key]['header'][0] = array("Ket"=>$dbr['namasupplier']);
        $vare[$key]['body'][]  = array("No"=>$n,"Bank"=>$dbr['ketbank'],"BG/CEK"=>$dbr['bgcek'],"Rekening"=>$dbr['norekening'],
                    "No BG/CEK"=>$dbr['nobgcek'],"Tgl Keluar"=>date_2d($dbr['tgl']),
                    "Tgl JthTmp"=>date_2d($dbr['tgljthtmp']),"Nominal"=>string_2s($dbr['nominal']));
        $vare[$key]['footer'][0] = array("Ket"=>"Subtotal","Nominal"=>0);
      }
	  
      savesession($this, "rptsaldobg_rpt", json_encode($vare)) ;
      echo(' bos.rptsaldobg.openreport() ; ') ;
	}

	public function showreport(){
      $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
      $data = getsession($this,"rptsaldobg_rpt") ;      
      $data = json_decode($data,true) ;
	  $totsaldo = 0 ;
      $n = 0 ;
      
      $totnom = 0 ;
	  foreach($data as $key => $arrval){
        $subtotnom = 0 ;
        $n = 0 ;
        foreach($arrval['body'] as $key2 => $val){
            $n++;
            $data[$key]['body'][$key2]['No'] = $n;
            $subtotnom += string_2n($val['Nominal']);
        }
        $totnom += $subtotnom;
        $data[$key]['footer'][0]['Nominal'] = string_2s($subtotnom);
	  }
	  $total = array();
	  $total[] = array("Ket"=>"Total","Nominal"=>string_2s($totnom));
      if(!empty($data)){ 
      	$font = 8 ;
        $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>"",
                        'opt'=>array('export_name'=>'saldobg_' . getsession($this, "username") ) ) ;
		
        $this->load->library('bospdf', $o) ;   
        $this->bospdf->ezText("<b>LAPORAN SALDO BG/CEK</b>",$font+4,array("justification"=>"center")) ;
        $this->bospdf->ezText("Sampai Tanggal : ". $va['tgl'],$font+2,array("justification"=>"center")) ;
        
        
        foreach($data as $key => $val){
            $this->bospdf->ezText("") ; 
            $this->bospdf->ezTable($val['header'],"","",  
								array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>"","cols"=> array( 
                                "Ket"=>array("justification"=>"left")))) ;
            $this->bospdf->ezTable($val['body'],"","",  
								array("fontSize"=>$font,"showHeadings"=>1,"showLines"=>2,"cols"=> array( 
                                "No"=>array("width"=>3,"justification"=>"right"),
                                "BG/CEK"=>array("width"=>4,"justification"=>"center"),
                                "Tgl Keluar"=>array("width"=>10,"justification"=>"center"),
                                "Tgl JthTmp"=>array("width"=>10,"justification"=>"center"),
                                "Ket"=>array("justification"=>"center"),
                                "Nominal"=>array("width"=>12,"justification"=>"right")
                                ))) ;
            $this->bospdf->ezTable($val['footer'],"","",  
								array("fontSize"=>$font,"showHeadings"=>0,"cols"=> array( 
                                "Ket"=>array("justification"=>"center"),
                                "Nominal"=>array("width"=>12,"justification"=>"right")
                                ))) ;

        }

		$this->bospdf->ezTable($total,"","",  
								array("fontSize"=>$font,"showHeadings"=>0,"cols"=> array( 
                                "Ket"=>array("justification"=>"center"),
                                "Nominal"=>array("width"=>12,"justification"=>"right")
                                ))) ;								 
        //print_r($data) ;    
         $this->bospdf->ezStream() ; 
      }else{
         echo('data kosong') ;
      }
   }
}

?>
