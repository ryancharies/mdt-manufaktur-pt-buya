<?php

class Rptkartupinjamankaryawan extends Bismillah_Controller{
    protected $bdb ;
    protected $ss ;
    protected $abc ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper('bdate');
        $this->load->model('rpt/rptkartupinjamankaryawan_m') ;
        // $this->load->model('func/perhitungan_m') ;
        $this->load->model('func/perhitunganhrd_m') ;
        $this->bdb = $this->rptkartupinjamankaryawan_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptkartupinjamankaryawan_" ;
    }

    public function index(){
        $d    = array("setdate"=>date_set()) ;
        $this->load->view('rpt/rptkartupinjamankaryawan', $d) ;
    }

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $va['tglAwal'] = date_2s($va['tglAwal']);
        $va['tglAkhir'] = date_2s($va['tglAkhir']);
        $vdb    = $this->rptkartupinjamankaryawan_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $tglKemarin = date("Y-m-d",strtotime($va['tglAwal'])-(60*60*24));
        $saldo =  $this->perhitunganhrd_m->getsaldopinjamankaryawan($va['nopinjaman'],$tglKemarin) ; ;
        $n = 0 ;
        $vare[] = array("no"=>"","faktur"=>"","tgl"=>"","keterangan"=>"Saldo Awal","debet"=>"","kredit"=>"","saldo"=>$saldo);
        $totdebet = 0 ;
        $totkredit = 0 ;
        while( $dbr = $this->rptkartupinjamankaryawan_m->getrow($dbd) ){
            $n++;
            $saldo += $dbr['debet'] - $dbr['kredit'];
            $vaset   = $dbr ;
            $vaset['no'] = $n;
            $vaset['saldo'] = $saldo;
            $vaset['tgl'] = date_2d($vaset['tgl']);
            $vare[]     = $vaset ;
            $totdebet += $dbr['debet'];
            $totkredit += $dbr['kredit'];
        }
        $vare[] = array("recid"=>'ZZZZ',"faktur"=> '',"tgl"=> '', 'keterangan'=>'TOTAL',
                        "debet"=>$totdebet,"kredit"=>$totkredit,"saldo"=>"","w2ui"=>array("summary"=> true));

        $vare    = array("total"=>count($vare)-1, "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function pilihnopinjaman(){
        $va 	= $this->input->post() ;
        $dbd = $this->rptkartupinjamankaryawan_m->getdatanopinjaman($va) ;
        if ($dbr = $this->bdb->getrow($dbd)) {
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['nip'],$dbr['tglrealisasi']);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['nip'],$dbr['tglrealisasi']);
            echo('
            with(bos.rptkartupinjamankaryawan.obj){
                find("#nopinjaman").val("' . $dbr['nopinjaman'] . '") ;
                find("#nip").val("' . $dbr['nip'] . '") ;
                find("#namakaryawan").val("' . $dbr['nama'] . '");
                find("#jabatan").val("' . $jabatan['keterangan'] . '");
                find("#bagian").val("' . $bagian['keterangan'] . '");
                find("#plafond").val("' . string_2s($dbr['plafond']) . '") ;
                find("#lama").val("' . string_2s($dbr['lama']) . '") ;
                
               bos.rptkartupinjamankaryawan.loadmodelnopinjaman("hide");
            }

         ') ;
        }
    }

    public function seekcustomer(){
        $va 	= $this->input->post() ;
        $kode 	= $va['Customer'] ;
        $data = $this->rptkartupinjamankaryawan_m->getdata($kode) ;
        if(!empty($data)){
            echo('
            with(bos.rptkartupinjamankaryawan.obj){
               find("#customer").val("'.$data['kode'].'") ;
               find("#namacustomer").val("'.$data['nama'].'");
            }

         ') ;
        }else{
            echo('
                alert("data tidak ditemukan !!!");
                with(bos.rptkartupinjamankaryawan.obj){
                    find("#customer").val("") ;
                    find("#customer").focus() ;
                }
            ');
        }
    }

    public function loadgrid2(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->rptkartupinjamankaryawan_m->loadgrid2($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->rptkartupinjamankaryawan_m->getrow($dbd) ){
            $vaset   = $dbr ;
            $vaset['cmdpilih']    = '<button type="button" onClick="bos.rptkartupinjamankaryawan.cmdpilih(\''.$dbr['nopinjaman'].'\')"
                           class="btn btn-success btn-grid">Pilih</button>' ;
            $vaset['cmdpilih']	   = html_entity_decode($vaset['cmdpilih']) ;
            $vare[]		= $vaset ;
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }
	
	public function initreport(){
        $va     = $this->input->post() ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        $vare   = array() ;
        $va['tglAwal'] = date_2s($va['tglawal']);
        $va['tglAkhir'] = date_2s($va['tglakhir']);
        $vdb    = $this->rptkartupinjamankaryawan_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $tglKemarin = date("Y-m-d",strtotime($va['tglAwal'])-(60*60*24));
        $saldo = $this->perhitunganhrd_m->getsaldopinjamankaryawan($va['nopinjaman'],$tglKemarin) ; ;
        $n = 0 ;
        $vare[] = array("no"=>"","faktur"=>"","tgl"=>"","keterangan"=>"Saldo Awal","debet"=>"","kredit"=>"","saldo"=>string_2s($saldo));
        $totdebet = 0 ;
        $totkredit = 0 ;
        while( $dbr = $this->rptkartupinjamankaryawan_m->getrow($dbd) ){
            $n++;
            $saldo += $dbr['debet'] - $dbr['kredit'];
            $vaset   = $dbr ;
            $vaset['no'] = $n;
            $vaset['saldo'] = string_2s($saldo);
            $vaset['tgl'] = date_2d($vaset['tgl']);
            $vaset['debet'] = string_2s($vaset['debet']);
            $vaset['kredit'] = string_2s($vaset['kredit']);
            $vare[]     = $vaset ;
        }
        
        savesession($this, "rptkartupinjamankaryawan_rpt", json_encode($vare)) ;
        echo(' bos.rptkartupinjamankaryawan.openreport() ; ') ;
	}

	public function showreport(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $data = getsession($this,"rptkartupinjamankaryawan_rpt") ;      
        $data = json_decode($data,true) ;
        $totdebet = 0 ;
        $totkredit = 0 ;
        if (!isset($va['export'])) {
            $va['export'] = 0;
        }
        foreach($data as $key => $val){
            if($val['debet'] == "")$val['debet'] = 0 ;
            if($val['kredit'] == "")$val['kredit'] = 0 ;
            $totdebet += string_2n($val['debet']) ;
            $totkredit += string_2n($val['kredit']) ;
        }
        $total = array();
        $total[] = array("keterangan"=>"<b>Total","debet"=>string_2s($totdebet),"kredit"=>string_2s($totkredit),"ket2"=>"</b>");

        $jabatan = $this->perhitunganhrd_m->getjabatan($va['nip'],$va['tglakhir']);
            $bagian = $this->perhitunganhrd_m->getbagian($va['nip'],$va['tglakhir']);
        
        $vDetail = array() ;
        $vDetail[] = array("1"=>"No Pinjaman","2"=>":","3"=>$va['nopinjaman'],"4"=>"","5"=>"NIP","6"=>":","7"=>$va['nip']);
        $vDetail[] = array("1"=>"Nama","2"=>":","3"=>$va['namakaryawan'],"4"=>"","5"=>"Plafond","6"=>":","7"=>string_2s($va['plafond']));
        $vDetail[] = array("1"=>"Bagian","2"=>":","3"=>$bagian['keterangan'],"4"=>"","5"=>"Lama","6"=>":","7"=>$va['lama']);
        $vDetail[] = array("1"=>"Jabatan","2"=>":","3"=>$jabatan['keterangan'],"4"=>"","5"=>"","6"=>"","7"=>"");
        
        
        if(!empty($data)){ 
            $font = 8 ;
            $o    = array('paper'=>'A4', 'orientation'=>'portrait', 'export'=>$va['export'],
                            'opt'=>array('export_name'=>'Kartuhutang_' . getsession($this, "username") ) ) ;
            $this->load->library('bospdf', $o) ;   
            $this->bospdf->ezText("<b>LAPORAN KARTU PINJAMAN KARYAWAN</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("Periode : ". $va['tglawal'] . " sd " . $va['tglakhir'],$font+2,array("justification"=>"center")) ;
            
            $this->bospdf->ezTable($vDetail,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "1"=>array("width"=>15,"justification"=>"left"),
                                             "2"=>array("width"=>5,"justification"=>"left"),
                                             "3"=>array("justification"=>"left"),
                                             "4"=>array("width"=>10,"justification"=>"left"),
                                             "5"=>array("width"=>15,"justification"=>"left"),
                                             "6"=>array("width"=>5,"justification"=>"left","wrap"=>1),
                                             "7"=>array("justification"=>"left","wrap"=>1))
                                        )
                                  ) ;

            $this->bospdf->ezText("") ; 


            $this->bospdf->ezTable($data,"","",  
                                    array("fontSize"=>$font,"cols"=> array( 
                                    "no"=>array("caption"=>"No","width"=>5,"justification"=>"right"),
                                    "tgl"=>array("caption"=>"Tgl","width"=>10,"justification"=>"center"),
                                    "faktur"=>array("caption"=>"Faktur","width"=>16,"justification"=>"center"),
                                    "keterangan"=>array("caption"=>"Keterangan","wrap"=>1),
                                    "debet"=>array("caption"=>"Debet","width"=>13,"justification"=>"right"),
                                    "kredit"=>array("caption"=>"Kredit","width"=>13,"justification"=>"right"),
                                    "saldo"=>array("caption"=>"Saldo","width"=>15,"justification"=>"right")))) ;  
            $this->bospdf->ezTable($total,"","",  
                                    array("fontSize"=>$font,"showHeadings"=>0,"cols"=> array( 
                                    "keterangan"=>array("caption"=>"Keterangan","wrap"=>1,"justification"=>"center"),
                                    "debet"=>array("caption"=>"Debet","width"=>13,"justification"=>"right"),
                                    "kredit"=>array("caption"=>"Kredit","width"=>13,"justification"=>"right"),
                                    "ket2"=>array("caption"=>"Saldo","width"=>15,"justification"=>"right")))) ;  								 
            //print_r($data) ;    
            $this->bospdf->ezStream() ; 
        }else{
            echo('data kosong') ;
        }
	}
}

?>
