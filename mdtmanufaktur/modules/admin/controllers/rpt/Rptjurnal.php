
<?php
class Rptjurnal extends Bismillah_Controller{
	protected $bdb ;
	public function __construct(){
		parent::__construct() ;
		$this->load->model("rpt/rptjurnal_m") ;
		$this->bdb 	= $this->rptjurnal_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptjurnal_" ;
	} 

	public function index(){
		$this->load->view("rpt/rptjurnal") ; 

	}

   public function loadgrid_where($bs, $s=''){
      $this->duser();

      $bs['tglawal'] = date_2s($bs['tglawal']);
      $bs['tglakhir'] = date_2s($bs['tglakhir']);

      $this->db->where('b.tgl >=', $bs['tglawal']);
      $this->db->where('b.tgl <=', $bs['tglakhir']);

      // print_r($bs);

      if($bs['skd_cabang'] !== 'null'){
          $bs['skd_cabang'] = json_decode($bs['skd_cabang'],true);
          $this->db->group_start();
          foreach($bs['skd_cabang'] as $kdc){
              $this->db->or_where('b.cabang',$kdc);
          }
          $this->db->group_end();

      }else{
          if($this->aruser['level'] !== '0000'){
              if(is_array($this->aruser['data_var']['cabang'])){
                  $this->db->group_start();
                  foreach($this->aruser['data_var']['cabang'] as $kdc){
                      $this->db->or_where('b.cabang', $kdc);
                  }
                  $this->db->group_end();
              }
          }
      }

      if($s !== ''){
         $this->db->like('b.faktur', $s,'after');
      }
   }

   public function loadgrid(){
      $this->duser();

      $va         = json_decode($this->input->post('request'), true) ;
      $search	    = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search     = $this->db->escape_like_str($search) ;
      $va['tglawal'] = date_2s($va['tglawal']);
      $va['tglakhir'] = date_2s($va['tglakhir']);
      $vare  = array() ;
      
      $totdebet = 0 ;
      $totkredit = 0 ;
      $n = 0 ;
      $faktur = "";
      $this->loadgrid_where($va, $search);
      $f2 = "b.id no,b.tgl,b.faktur,concat('[',r.jenis,'] ',b.rekening) as rekening,r.keterangan as ketrekening,b.keterangan,b.debet,b.kredit,b.username";
      $dbd2 = $this->db->select($f2)
          ->from("keuangan_bukubesar b")
          ->join("keuangan_rekening r","r.kode = b.rekening","left")
          ->order_by("b.faktur asc, b.datetime asc")
          ->get();
      foreach($dbd2->result_array() as $dbr){
         $vs = $dbr;
         if($faktur <> $vs['faktur']){
            $faktur = $vs['faktur'];
         }else{
            $vs['faktur'] = "";
         }
         $vs['recid'] = ++$n ;
         $vs['tgl'] = date_2d($vs['tgl']) ;  
         $totdebet += $vs['debet'];
         $totkredit += $vs['kredit'];
         $vare[]		= $vs ;
      }
      $vare[] = array("recid"=>'ZZZZ',"tgl"=> '', "faktur"=> '',"ketrekening"=>"",'keterangan'=>'TOTAL',
                        "debet"=>$totdebet,"kredit"=>$totkredit,"username"=>"","w2ui"=>array("summary"=> true));
      $vare    = array("total"=>count($vare) - 1, "records"=>$vare ) ;

      echo(json_encode($vare)) ;
   }

	public function loadgrid_(){
	  $va     = json_decode($this->input->post('request'), true) ; 
      $vare   = array() ;
      $vdb    = $this->bdb->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      $n = 0 ;
      $debet = 0;
      $kredit = 0;
      while( $dbr = $this->bdb->getrow($dbd) ){
         $vs = $dbr;
         $debet += $dbr['debet'];
         $kredit += $dbr['kredit'];
         $vs['recid'] = ++$n ;
         $vs['tgl'] = date_2d($vs['tgl']) ;  
         $vs['debet'] = $vs['debet'];
         $vs['kredit'] = $vs['kredit'];
         $vare[]		= $vs ;
     }
     $vare[] = array("recid"=>"ZZZ","no"=>"","rekening"=>"","faktur"=>"","ketrekening"=>"Total","keterangan"=>"","debet"=>$debet,"kredit"=>$kredit,"username"=>"","w2ui"=>array("summary"=>true));

      $vare 	= array("total"=>count($vare)-1, "records"=>$vare ) ;
      $varpt = $vare['records'] ;
      echo(json_encode($vare)) ; 
	}
    
    public function initreport(){
      $va     = $this->input->post() ;
      savesession($this, $this->ss . "va", json_encode($va) ) ;
      $vare   = array() ;
      $vdb    = $this->bdb->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      $n = 0 ;
      while( $dbr = $this->bdb->getrow($dbd) ){
         $vs = $dbr;
         $vs['no'] = ++$n ;
         $vs['tgl'] = date_2d($vs['tgl']) ;  
         $vs['debet'] = string_2s($vs['debet']);
         $vs['kredit'] = string_2s($vs['kredit']);
         $vare[]		= $vs ;
	  }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      $varpt = $vare['records'] ;
      savesession($this, "rptjurnal_rpt", json_encode($varpt)) ; 
      echo(' bos.rptjurnal.openreport() ; ') ;
	}



	public function showreport(){
      $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
      $data = getsession($this,"rptjurnal_rpt") ;      
      $data = json_decode($data,true) ;      
      if(!empty($data)){  
      	$font = 8 ;
        $o    = array('paper'=>'A4', 'orientation'=>'portrait', 'export'=>"",
                        'opt'=>array('export_name'=>'DaftarJurnal_' . getsession($this, "username") ) ) ;
        $this->load->library('bospdf', $o) ;   
        $this->bospdf->ezText("<b>LAPORAN JURNAL UMUM</b>",$font+4,array("justification"=>"center")) ;
        $this->bospdf->ezText("Periode : ". $va['tglawal'] . " sd " . $va['tglakhir'],$font+2,array("justification"=>"center")) ;
        $this->bospdf->ezText("") ; 
		$this->bospdf->ezTable($data,"","",  
								array("fontSize"=>$font,"cols"=> array( 
			                     "no"=>array("caption"=>"No","width"=>5,"justification"=>"right"),
			                     "tgl"=>array("caption"=>"Tgl","width"=>10,"justification"=>"center"),
			                     "faktur"=>array("caption"=>"Faktur","width"=>14,"justification"=>"center"),
			                     "rekening"=>array("caption"=>"Rekening","width"=>12),
			                     "keterangan"=>array("caption"=>"Keterangan","wrap"=>1),
			                     "debet"=>array("caption"=>"Debet","width"=>13,"justification"=>"right"),
			                     "kredit"=>array("caption"=>"Kredit","width"=>13,"justification"=>"right"),
			                     "username"=>array("caption"=>"Username","width"=>8)))) ;   
        //print_r($data) ;    
        $this->bospdf->ezStream() ; 
      }else{
         echo('data kosong') ;
      }
   }

}
?>
