
<?php
class Rptneracalajur extends Bismillah_Controller
{
    protected $bdb;
    public function __construct()
    {
        parent::__construct();
        $this->load->model("rpt/rptneracalajur_m");
        $this->load->model("func/perhitungan_m");
        $this->load->model("func/func_m");
        $this->bdb = $this->rptneracalajur_m;
        $this->ss = "ssrptneracalajur_";
    }

    public function index()
    {
        $this->load->view("rpt/rptneracalajur");

    }

    public function loadgrid()
    {

        $va = json_decode($this->input->post('request'), true);
        $tglawal = $va['tglawal']; //date("d-m-Y") ;
        $tglakhir = $va['tglakhir']; //date("d-m-Y") ;
        $tglkemarin = date("d-m-Y", strtotime($tglawal) - (24 * 60 * 60));
        $vare = array();

        $vsjml = array("recid"=>"zzzz1","kode" => "", "keterangan" => "<b>Jumlah</b>", "sad" => 0, "sak" => 0, "jrd" => 0, "jrk" => 0,
                "nsd" => 0, "nsk" => 0, "ajpd" => 0, "ajpk" => 0, "nspd" => 0, "nspk" => 0, "lrd" => 0, "lrk" => 0,
                "nrcd" => 0, "nrck" => 0,"w2ui"=>array("summary"=>true));
        $vslr = array("recid"=>"zzzz2","kode" => "", "keterangan" => "<b>L/R Usaha</b>", "sad" => "", "sak" => "", "jrd" => "", "jrk" => "",
                "nsd" => "", "nsk" => "", "ajpd" => "",  "ajpk" => "", "nspd" => "", "nspk" => "", "lrd" => 0, "lrk" => 0,
                "nrcd" => 0, "nrck" => 0,"w2ui"=>array("summary"=>true));
        $vsnrc = array("recid"=>"zzzz3","kode" => "", "keterangan" => "<b>Balance</b>", "sad" => "", "sak" => "", "jrd" => "", "jrk" => "",
                "nsd" => "", "nsk" => "", "ajpd" => "",  "ajpk" => "", "nspd" => "", "nspk" => "", "lrd" => 0, "lrk" => 0,
                "nrcd" => 0, "nrck" => 0,"w2ui"=>array("summary"=>true));

        // AKTIVA
        $n = 0;
        $vdb = $this->perhitungan_m->loadrekening("1", "1.9999");
        while ($dbr = $this->bdb->getrow($vdb)) {
            $vs = array("kode" => $dbr['kode'], "keterangan" => $dbr['keterangan'], "sad" => 0, "sak" => 0, "jrd" => 0, "jrk" => 0,
                "nsd" => 0, "nsk" => 0,  "ajpd" => 0, "ajpk" => 0, "nspd" => 0, "nspk" => 0, "lrd" => 0, "lrk" => 0,
                "nrcd" => 0, "nrck" => 0);

            $sa = $this->perhitungan_m->getsaldoawal($tglawal, $dbr['kode']);
            if ($sa > 0) {
                $vs['sad'] = $sa;
            } else {
                $vs['sak'] = ($sa * -1);
            }
            
            $py_d = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'SP', '', "T", false);
            $py_d += $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'PS', '', "T", false);
            
            $py_k = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'SP', '', "T", false);
            $py_k += $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'PS', '', "T", false);

            //if( $dbr['kode'] >= $this->bdb->getconfig("rekhppbbbawal") && $dbr['kode'] <= $this->bdb->getconfig("rekhppbbbakhir")){
                $py_d += $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'RJ', '', "T", false);
                $py_k += $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'RJ', '', "T", false);
          //  }

            $vs['jrd'] = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", true);
            $vs['jrd'] -= $py_d;

            $vs['jrk'] = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", true);
            $vs['jrk'] -= $py_k;

            $ns = ($vs['sad'] + $vs['jrd']) - ($vs['sak'] + $vs['jrk']);
            if ($ns > 0) {
                $vs['nsd'] = $ns;
            } else {
                $vs['nsk'] = ($ns * -1);
            }

            $vs['ajpd'] = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", false) + $py_d;
            $vs['ajpk'] = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", false) + $py_k;

            $nsp = ($vs['nsd'] + $vs['ajpd']) - ($vs['ajpk'] + $vs['nsk']);
            if ($nsp > 0) {
                $vs['nspd'] = $nsp;
            } else {
                $vs['nspk'] = ($nsp * -1);
            }
            
            $vs['nrcd'] = $vs['lrd'] + $vs['nspd'];
            $vs['nrck'] = $vs['lrk'] + $vs['nspk'];

            //jumlah
            if ($dbr['jenis'] == "D"){
                $vsjml['sad'] += $vs['sad'];
                $vsjml['sak'] += $vs['sak'];
                $vsjml['jrd'] += $vs['jrd'];
                $vsjml['jrk'] += $vs['jrk'];
                $vsjml['nsd'] += $vs['nsd'];
                $vsjml['nsk'] += $vs['nsk'];
                $vsjml['ajpd'] += $vs['ajpd'];
                $vsjml['ajpk'] += $vs['ajpk'];
                $vsjml['nspd'] += $vs['nspd'];
                $vsjml['nspk'] += $vs['nspk'];
                $vsjml['lrd'] += $vs['lrd'];
                $vsjml['lrk'] += $vs['lrk'];
                $vsjml['nrcd'] += $vs['nrcd'];
                $vsjml['nrck'] += $vs['nrck'];
            }

            // $vs['sad'] = string_2sz($vs['sad']);
            // $vs['sak'] = string_2sz($vs['sak']);
            // $vs['jrd'] = string_2sz($vs['jrd']);
            // $vs['jrk'] = string_2sz($vs['jrk']);
            // $vs['nsd'] = string_2sz($vs['nsd']);
            // $vs['nsk'] = string_2sz($vs['nsk']);
            // $vs['ajpd'] = string_2sz($vs['ajpd']);
            // $vs['ajpk'] = string_2sz($vs['ajpk']);
            // $vs['nspd'] = string_2sz($vs['nspd']);
            // $vs['nspk'] = string_2sz($vs['nspk']);
            // $vs['lrd'] = string_2sz($vs['lrd']);
            // $vs['lrk'] = string_2sz($vs['lrk']);
            // $vs['nrcd'] = string_2sz($vs['nrcd']);
            // $vs['nrck'] = string_2sz($vs['nrck']);
                

            $arrkd = explode(".", $dbr['kode']);
            $level = count($arrkd);
            //bold text
            if ($dbr['jenis'] == "I" and $va['level'] > $level) {
                foreach ($vs as $key => $val) {
                    if ($key == "kode" || $key == "keterangan") {
                        $vs[$key] = "<b>" . $val . "</b>";
                    }

                }
            }

            //susun array untuk struktur
            if ($va['level'] >= $level) {
                $vare[] = $vs;
            }

        }

        // pasiva
        $n = 0;
        $arrlr = $this->perhitungan_m->getlr($tglawal, $tglakhir,6,"Y");
        $reklrthnberjalan = $this->bdb->getconfig("reklrthberjalan");
        $reklrblnberjalan = $this->bdb->getconfig("reklrblnberjalan");
        $vdb = $this->perhitungan_m->loadrekening("2", "3.9999");
        while ($dbr = $this->bdb->getrow($vdb)) {
            $vs = array("kode" => $dbr['kode'], "keterangan" => $dbr['keterangan'], "sad" => 0, "sak" => 0, "jrd" => 0, "jrk" => 0,
                "nsd" => 0, "nsk" => 0, "ajpd" => 0, "ajpk" => 0, "nspd" => 0, "nspk" => 0, "lrd" => 0, "lrk" => 0,
                "nrcd" => 0, "nrck" => 0);

            $sa = $this->perhitungan_m->getsaldoawal($tglawal, $dbr['kode']);

            if (substr($reklrthnberjalan, 0, strlen($dbr['kode'])) == substr($dbr['kode'], 0, strlen($dbr['kode']))) {
                $sa += $arrlr['lrstlhpjk']['saldoawal'];
            }

            if ($sa > 0) {
                $vs['sak'] = $sa;
            } else {
                $vs['sad'] = ($sa * -1);
            }

            $py_d = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'SP', '', "T", false);
            $py_d += $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'PS', '', "T", false);
            
            $py_k = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'SP', '', "T", false);
            $py_k += $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'PS', '', "T", false);

            //if( $dbr['kode'] >= $this->bdb->getconfig("rekhppbbbawal") && $dbr['kode'] <= $this->bdb->getconfig("rekhppbbbakhir")){
                $py_d += $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'RJ', '', "T", false);
                $py_k += $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'RJ', '', "T", false);
          //  }

            $vs['jrd'] = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", true) - $py_d;
            $vs['jrk'] = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", true) - $py_k;


            $ns = ($vs['sak'] + $vs['jrk']) - ($vs['sad'] + $vs['jrd']);
            if ($ns > 0) {
                $vs['nsk'] = $ns;
            } else {
                $vs['nsd'] = ($ns * -1);
            }

            $vs['ajpd'] = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", false) + $py_d;
            $vs['ajpk'] = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", false) + $py_k;

            $nsp = ($vs['ajpk'] + $vs['nsk']) - ($vs['nsd'] + $vs['ajpd']);
            if ($nsp > 0) {
                $vs['nspk'] = $nsp;
            } else {
                $vs['nspd'] = ($nsp * -1);
            }

            $vs['nrcd'] = $vs['lrd'] + $vs['nspd'];
            $vs['nrck'] = $vs['lrk'] + $vs['nspk'];


            //jumlah
            if ($dbr['jenis'] == "D"){
                $vsjml['sad'] += $vs['sad'];
                $vsjml['sak'] += $vs['sak'];
                $vsjml['jrd'] += $vs['jrd'];
                $vsjml['jrk'] += $vs['jrk'];
                $vsjml['nsd'] += $vs['nsd'];
                $vsjml['nsk'] += $vs['nsk'];
                $vsjml['ajpd'] += $vs['ajpd'];
                $vsjml['ajpk'] += $vs['ajpk'];
                $vsjml['nspd'] += $vs['nspd'];
                $vsjml['nspk'] += $vs['nspk'];
                $vsjml['lrd'] += $vs['lrd'];
                $vsjml['lrk'] += $vs['lrk'];
                $vsjml['nrcd'] += $vs['nrcd'];
                $vsjml['nrck'] += $vs['nrck'];
            }

            // $vs['sad'] = string_2sz($vs['sad']);
            // $vs['sak'] = string_2sz($vs['sak']);
            // $vs['jrd'] = string_2sz($vs['jrd']);
            // $vs['jrk'] = string_2sz($vs['jrk']);
            // $vs['nsd'] = string_2sz($vs['nsd']);
            // $vs['nsk'] = string_2sz($vs['nsk']);
            // $vs['ajpd'] = string_2sz($vs['ajpd']);
            // $vs['ajpk'] = string_2sz($vs['ajpk']);
            // $vs['nspd'] = string_2sz($vs['nspd']);
            // $vs['nspk'] = string_2sz($vs['nspk']);
            // $vs['lrd'] = string_2sz($vs['lrd']);
            // $vs['lrk'] = string_2sz($vs['lrk']);
            // $vs['nrcd'] = string_2sz($vs['nrcd']);
            // $vs['nrck'] = string_2sz($vs['nrck']);

            $arrkd = explode(".", $dbr['kode']);
            $level = count($arrkd);
            //bold text
            if ($dbr['jenis'] == "I" and $va['level'] > $level) {
                foreach ($vs as $key => $val) {
                    if ($key == "kode" || $key == "keterangan") {
                        $vs[$key] = "<b>" . $val . "</b>";
                    }

                }
            }

            //susun array untuk struktur
            if ($va['level'] >= $level) {
                $vare[] = $vs;
            }

        }

        // LR
        $n = 0;
        $vdb = $this->perhitungan_m->loadrekening("4", "9.9999");
        while ($dbr = $this->bdb->getrow($vdb)) {
            $vs = array("kode" => $dbr['kode'], "keterangan" => $dbr['keterangan'], "sad" => 0, "sak" => 0, "jrd" => 0, "jrk" => 0,
                "nsd" => 0, "nsk" => 0, "ajpd" => 0,  "ajpk" => 0, "nspd" => 0, "nspk" => 0, "lrd" => 0, "lrk" => 0,
                "nrcd" => 0, "nrck" => 0);

            $sa = 0;//$this->perhitungan_m->getsaldoawal($tglawal, $dbr['kode']);

            $c = substr($dbr['kode'], 0, 1);
            if ($c == "5" || $c == "6" || $c == "8" || $c == "9") {
                if($sa > 0){
                    $vs['sad'] = $sa;
                }else{
                    $vs['sak'] = ($sa * -1);
                }
            } else {

                if ($sa > 0) {
                    $vs['sak'] = $sa;
                } else {
                    $vs['sad'] = ($sa * -1);
                }
            }
            
            $py_d = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'SP', '', "T", false);
            $py_d += $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'PS', '', "T", false);
            
            $py_k = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'SP', '', "T", false);
            $py_k += $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'PS', '', "T", false);

            //if( $dbr['kode'] >= $this->bdb->getconfig("rekhppbbbawal") && $dbr['kode'] <= $this->bdb->getconfig("rekhppbbbakhir")){
                $py_d += $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'RJ', '', "T", false);
                $py_k += $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'RJ', '', "T", false);
          //  }

            
            $vs['jrd'] = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", true) - $py_d;
            $vs['jrk'] = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", true) - $py_k;

            

            $c = substr($dbr['kode'], 0, 1);
            if ($c == "5" || $c == "6" || $c == "8" || $c == "9") {
                $ns = ($vs['sad'] + $vs['jrd']) - ($vs['sak'] + $vs['jrk']);
                if($ns > 0){
                    $vs['nsd'] = $ns;
                }else{
                    $vs['nsk'] = ($ns * -1);
                }
            } else {
                $ns = ($vs['sak'] + $vs['jrk']) - ($vs['sad'] + $vs['jrd']);
                if ($ns > 0) {
                    $vs['nsk'] = $ns;
                } else {
                    $vs['nsd'] = ($ns * -1);
                }
            }
            
            // $vs['nsd'] = $vs['sad'] + $vs['jrd'];
            // $vs['nsk'] = $vs['sak'] + $vs['jrk'];

            $vs['ajpd'] = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", false) + $py_d;
            $vs['ajpk'] = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", false) + $py_k;


            
            if ($c == "5" || $c == "6" || $c == "8" || $c == "9") {
                $nsp = ($vs['nsd'] + $vs['ajpd']) - ($vs['ajpk'] + $vs['nsk']);
                if($nsp > 0){
                    $vs['nspd'] = $nsp;
                }else{
                    $vs['nspk'] = ($nsp * -1);
                }
            } else {
                $nsp = ($vs['ajpk'] + $vs['nsk']) - ($vs['nsd'] + $vs['ajpd']);
                if ($nsp > 0) {
                    $vs['nspk'] = $nsp;
                } else {
                    $vs['nspd'] = ($nsp * -1);
                }
            }
            // $vs['nspd'] = $vs['ajpd'] + $vs['nsd'];
            // $vs['nspk'] = $vs['ajpk'] + $vs['nsk'];

            $vs['lrd'] = $vs['lrd'] + $vs['nspd'];
            $vs['lrk'] = $vs['lrk'] + $vs['nspk'];


            //jumlah
            if ($dbr['jenis'] == "D"){
                $vsjml['sad'] += $vs['sad'];
                $vsjml['sak'] += $vs['sak'];
                $vsjml['jrd'] += $vs['jrd'];
                $vsjml['jrk'] += $vs['jrk'];
                $vsjml['nsd'] += $vs['nsd'];
                $vsjml['nsk'] += $vs['nsk'];
                $vsjml['ajpd'] += $vs['ajpd'];
                $vsjml['ajpk'] += $vs['ajpk'];
                $vsjml['nspd'] += $vs['nspd'];
                $vsjml['nspk'] += $vs['nspk'];
                $vsjml['lrd'] += $vs['lrd'];
                $vsjml['lrk'] += $vs['lrk'];
                $vsjml['nrcd'] += $vs['nrcd'];
                $vsjml['nrck'] += $vs['nrck'];
            }

            // $vs['sad'] = string_2sz($vs['sad']);
            // $vs['sak'] = string_2sz($vs['sak']);
            // $vs['jrd'] = string_2sz($vs['jrd']);
            // $vs['jrk'] = string_2sz($vs['jrk']);
            // $vs['nsd'] = string_2sz($vs['nsd']);
            // $vs['nsk'] = string_2sz($vs['nsk']);
            // $vs['ajpd'] = string_2sz($vs['ajpd']);
            // $vs['ajpk'] = string_2sz($vs['ajpk']);
            // $vs['nspd'] = string_2sz($vs['nspd']);
            // $vs['nspk'] = string_2sz($vs['nspk']);
            // $vs['lrd'] = string_2sz($vs['lrd']);
            // $vs['lrk'] = string_2sz($vs['lrk']);
            // $vs['nrcd'] = string_2sz($vs['nrcd']);
            // $vs['nrck'] = string_2sz($vs['nrck']);

            $arrkd = explode(".", $dbr['kode']);
            $level = count($arrkd);
            //bold text
            if ($dbr['jenis'] == "I" and $va['level'] > $level) {
                foreach ($vs as $key => $val) {
                    if ($key == "kode" || $key == "keterangan") {
                        $vs[$key] = "<b>" . $val . "</b>";
                    }

                }
            }

            //susun array untuk struktur
            if ($va['level'] >= $level) {
                $vare[] = $vs;
            }

        }
        // print_r($vare);
        // $vare = array();
        // foreach($vare as $key => $val){
        //     $vare2[] = $val;
        // }
        $vare[] = $vsjml;
        $lr = $vsjml['lrd'] - $vsjml['lrk'];
        if($lr > 0){
            $vslr['lrk'] = $lr;
        }else{
            $vslr['lrd'] = ($lr * -1);
        }
        $vslr['nrcd'] = $vslr['lrk'];
        $vslr['nrck'] = $vslr['lrd'];

        $vare[] = $vslr;
        $vsnrc['lrd'] = $vslr['lrd'] + $vsjml['lrd'];
        $vsnrc['lrk'] = $vslr['lrk'] + $vsjml['lrk'];
        $vsnrc['nrcd'] = $vslr['nrcd'] + $vsjml['nrcd'];
        $vsnrc['nrck'] = $vslr['nrck'] + $vsjml['nrck'];
        $vare[] = $vsnrc;
        $vare = array("total" => count($vare)-3, "records" => $vare);
        echo (json_encode($vare));
    }

    public function init()
    {
        savesession($this, "ssrptneraca_id", "");
    }

    public function initreport()
    {
        $n = 0;
        $va = $this->input->post();
        savesession($this, $this->ss . "va", json_encode($va));

        $tglawal = $va['tglawal']; //date("d-m-Y") ;
        $tglakhir = $va['tglakhir']; //date("d-m-Y") ;
        $tglkemarin = date("d-m-Y", strtotime($tglawal) - (24 * 60 * 60));
        $vare = array();


        $vsjml = array("keterangan" => "<b>Jumlah", "sad" => 0, "sak" => 0, "jrd" => 0, "jrk" => 0,
                "nsd" => 0, "nsk" => 0, "ajpd" => 0,  "ajpk" => 0, "nspd" => 0, "nspk" => 0, "lrd" => 0, "lrk" => 0,
                "nrcd" => 0, "nrck" => 0);
        $vslr = array("keterangan" => "<b>L/R Usaha", "lrd" => 0, "lrk" => 0,"nrcd" => 0, "nrck" => 0);
        $vsnrc = array("keterangan" => "<b>Balance", "lrd" => 0, "lrk" => 0,"nrcd" => 0, "nrck" => 0);

        // AKTIVA
        $n = 0;
        $vdb = $this->perhitungan_m->loadrekening("1", "1.9999");
        while ($dbr = $this->bdb->getrow($vdb)) {
            $vs = array("kode" => $dbr['kode'], "keterangan" => $dbr['keterangan'], "sad" => 0, "sak" => 0, "jrd" => 0, "jrk" => 0,
                "nsd" => 0, "nsk" => 0,  "ajpd" => 0, "ajpk" => 0, "nspd" => 0, "nspk" => 0, "lrd" => 0, "lrk" => 0,
                "nrcd" => 0, "nrck" => 0);

            $sa = $this->perhitungan_m->getsaldoawal($tglawal, $dbr['kode']);
            if ($sa > 0) {
                $vs['sad'] = $sa;
            } else {
                $vs['sak'] = ($sa * -1);
            }
            
            $py_d = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'SP', '', "T", false);
            $py_d += $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'PS', '', "T", false);
            
            $py_k = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'SP', '', "T", false);
            $py_k += $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'PS', '', "T", false);

            //if( $dbr['kode'] >= $this->bdb->getconfig("rekhppbbbawal") && $dbr['kode'] <= $this->bdb->getconfig("rekhppbbbakhir")){
                $py_d += $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'RJ', '', "T", false);
                $py_k += $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'RJ', '', "T", false);
          //  }


            $vs['jrd'] = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", true) - $py_d;
            $vs['jrk'] = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", true) - $py_k;

            
            $ns = ($vs['sad'] + $vs['jrd']) - ($vs['sak'] + $vs['jrk']);
            if ($ns > 0) {
                $vs['nsd'] = $ns;
            } else {
                $vs['nsk'] = ($ns * -1);
            }

            $vs['ajpd'] = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", false) + $py_d;
            $vs['ajpk'] = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", false) + $py_k;

            $nsp = ($vs['nsd'] + $vs['ajpd']) - ($vs['ajpk'] + $vs['nsk']);
            if ($nsp > 0) {
                $vs['nspd'] = $nsp;
            } else {
                $vs['nspk'] = ($nsp * -1);
            }
            
            $vs['nrcd'] = $vs['lrd'] + $vs['nspd'];
            $vs['nrck'] = $vs['lrk'] + $vs['nspk'];

            //jumlah
            if ($dbr['jenis'] == "D"){
                $vsjml['sad'] += $vs['sad'];
                $vsjml['sak'] += $vs['sak'];
                $vsjml['jrd'] += $vs['jrd'];
                $vsjml['jrk'] += $vs['jrk'];
                $vsjml['nsd'] += $vs['nsd'];
                $vsjml['nsk'] += $vs['nsk'];
                $vsjml['ajpd'] += $vs['ajpd'];
                $vsjml['ajpk'] += $vs['ajpk'];
                $vsjml['nspd'] += $vs['nspd'];
                $vsjml['nspk'] += $vs['nspk'];
                $vsjml['lrd'] += $vs['lrd'];
                $vsjml['lrk'] += $vs['lrk'];
                $vsjml['nrcd'] += $vs['nrcd'];
                $vsjml['nrck'] += $vs['nrck'];
            }

        

            $arrkd = explode(".", $dbr['kode']);
            $level = count($arrkd);

            foreach ($vs as $key => $val) {
                if ($key !== "kode" && $key !== "keterangan") {
                    $vs[$key] = string_2sz($val);
                }

            }
            //bold text
            if ($dbr['jenis'] == "I" and $va['level'] > $level) {
                $vs["kode"] = "<b>" . $vs["kode"];
                $vs["nrck"] = $vs["nrck"] . "</b>";
            }

            //susun array untuk struktur
            if ($va['level'] >= $level) {
                $vare[] = $vs;
            }

        }

        // pasiva
        $n = 0;
        $arrlr = $this->perhitungan_m->getlr($tglawal, $tglakhir);
        $reklrthnberjalan = $this->bdb->getconfig("reklrthberjalan");
        $reklrblnberjalan = $this->bdb->getconfig("reklrblnberjalan");
        $vdb = $this->perhitungan_m->loadrekening("2", "3.9999");
        while ($dbr = $this->bdb->getrow($vdb)) {
            $vs = array("kode" => $dbr['kode'], "keterangan" => $dbr['keterangan'], "sad" => 0, "sak" => 0, "jrd" => 0, "jrk" => 0,
                "nsd" => 0, "nsk" => 0, "ajpd" => 0, "ajpk" => 0, "nspd" => 0, "nspk" => 0, "lrd" => 0, "lrk" => 0,
                "nrcd" => 0, "nrck" => 0);

            $sa = $this->perhitungan_m->getsaldoawal($tglawal, $dbr['kode']);

            if (substr($reklrthnberjalan, 0, strlen($dbr['kode'])) == substr($dbr['kode'], 0, strlen($dbr['kode']))) {
                $sa += $arrlr['lrstlhpjk']['saldoawal'];
                //$vs['sak'] += $arrlr['lrstlhpjk']['saldoawal']; 
            }


            if ($sa > 0) {
                $vs['sak'] = $sa;
            } else {
                $vs['sad'] = ($sa * -1);
            }
            
            $py_d = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'SP', '', "T", false);
            $py_d += $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'PS', '', "T", false);
            
            $py_k = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'SP', '', "T", false);
            $py_k += $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'PS', '', "T", false);

            //if( $dbr['kode'] >= $this->bdb->getconfig("rekhppbbbawal") && $dbr['kode'] <= $this->bdb->getconfig("rekhppbbbakhir")){
                $py_d += $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'RJ', '', "T", false);
                $py_k += $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'RJ', '', "T", false);
          //  }

            $vs['jrd'] = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", true) - $py_d;
            $vs['jrk'] = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", true) - $py_k;


            $ns = ($vs['sak'] + $vs['jrk']) - ($vs['sad'] + $vs['jrd']);
            if ($ns > 0) {
                $vs['nsk'] = $ns;
            } else {
                $vs['nsd'] = ($ns * -1);
            }

            $vs['ajpd'] = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", false) + $py_d;
            $vs['ajpk'] = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", false) + $py_k;

            $nsp = ($vs['ajpk'] + $vs['nsk']) - ($vs['nsd'] + $vs['ajpd']);
            if ($nsp > 0) {
                $vs['nspk'] = $nsp;
            } else {
                $vs['nspd'] = ($nsp * -1);
            }

            $vs['nrcd'] = $vs['lrd'] + $vs['nspd'];
            $vs['nrck'] = $vs['lrk'] + $vs['nspk'];


            //jumlah
            if ($dbr['jenis'] == "D"){
                $vsjml['sad'] += $vs['sad'];
                $vsjml['sak'] += $vs['sak'];
                $vsjml['jrd'] += $vs['jrd'];
                $vsjml['jrk'] += $vs['jrk'];
                $vsjml['nsd'] += $vs['nsd'];
                $vsjml['nsk'] += $vs['nsk'];
                $vsjml['ajpd'] += $vs['ajpd'];
                $vsjml['ajpk'] += $vs['ajpk'];
                $vsjml['nspd'] += $vs['nspd'];
                $vsjml['nspk'] += $vs['nspk'];
                $vsjml['lrd'] += $vs['lrd'];
                $vsjml['lrk'] += $vs['lrk'];
                $vsjml['nrcd'] += $vs['nrcd'];
                $vsjml['nrck'] += $vs['nrck'];
            }

            

            $arrkd = explode(".", $dbr['kode']);
            $level = count($arrkd);
            foreach ($vs as $key => $val) {
                if ($key !== "kode" && $key !== "keterangan") {
                    $vs[$key] = string_2sz($val);
                }

            }
            //bold text
            if ($dbr['jenis'] == "I" and $va['level'] > $level) {
                $vs["kode"] = "<b>" . $vs["kode"];
                $vs["nrck"] = $vs["nrck"] . "</b>";
            }

            //susun array untuk struktur
            if ($va['level'] >= $level) {
                $vare[] = $vs;
            }

        }

        // LR
        $n = 0;
        $vdb = $this->perhitungan_m->loadrekening("4", "9.9999");
        while ($dbr = $this->bdb->getrow($vdb)) {
            $vs = array("kode" => $dbr['kode'], "keterangan" => $dbr['keterangan'], "sad" => 0, "sak" => 0, "jrd" => 0, "jrk" => 0,
                "nsd" => 0, "nsk" => 0, "ajpd" => 0,  "ajpk" => 0, "nspd" => 0, "nspk" => 0, "lrd" => 0, "lrk" => 0,
                "nrcd" => 0, "nrck" => 0);

            $sa = 0;//$this->perhitungan_m->getsaldoawal($tglawal, $dbr['kode']);

            $c = substr($dbr['kode'], 0, 1);
            if ($c == "5" || $c == "6" || $c == "8" || $c == "9") {
                if($sa > 0){
                    $vs['sad'] = $sa;
                }else{
                    $vs['sak'] = ($sa * -1);
                }
            } else {

                if ($sa > 0) {
                    $vs['sak'] = $sa;
                } else {
                    $vs['sad'] = ($sa * -1);
                }
            }
            
            $py_d = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'SP', '', "T", false);
            $py_d += $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'PS', '', "T", false);
            
            $py_k = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'SP', '', "T", false);
            $py_k += $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'PS', '', "T", false);

            //if( $dbr['kode'] >= $this->bdb->getconfig("rekhppbbbawal") && $dbr['kode'] <= $this->bdb->getconfig("rekhppbbbakhir")){
                $py_d += $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'RJ', '', "T", false);
                $py_k += $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'RJ', '', "T", false);
          //  }

            $vs['jrd'] = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", true) - $py_d;
            $vs['jrk'] = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", true) - $py_k;

            

            $c = substr($dbr['kode'], 0, 1);
            if ($c == "5" || $c == "6" || $c == "8" || $c == "9") {
                $ns = ($vs['sad'] + $vs['jrd']) - ($vs['sak'] + $vs['jrk']);
                if($ns > 0){
                    $vs['nsd'] = $ns;
                }else{
                    $vs['nsk'] = ($ns * -1);
                }
            } else {
                $ns = ($vs['sak'] + $vs['jrk']) - ($vs['sad'] + $vs['jrd']);
                if ($ns > 0) {
                    $vs['nsk'] = $ns;
                } else {
                    $vs['nsd'] = ($ns * -1);
                }
            }
            
            // $vs['nsd'] = $vs['sad'] + $vs['jrd'];
            // $vs['nsk'] = $vs['sak'] + $vs['jrk'];

            $vs['ajpd'] = $this->perhitungan_m->getdebet($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", false) + $py_d;
            $vs['ajpk'] = $this->perhitungan_m->getkredit($tglawal, $tglakhir, $dbr['kode'], 'JR', '', "T", false) + $py_k;


            
            if ($c == "5" || $c == "6" || $c == "8" || $c == "9") {
                $nsp = ($vs['nsd'] + $vs['ajpd']) - ($vs['ajpk'] + $vs['nsk']);
                if($nsp > 0){
                    $vs['nspd'] = $nsp;
                }else{
                    $vs['nspk'] = ($nsp * -1);
                }
            } else {
                $nsp = ($vs['ajpk'] + $vs['nsk']) - ($vs['nsd'] + $vs['ajpd']);
                if ($nsp > 0) {
                    $vs['nspk'] = $nsp;
                } else {
                    $vs['nspd'] = ($nsp * -1);
                }
            }
            // $vs['nspd'] = $vs['ajpd'] + $vs['nsd'];
            // $vs['nspk'] = $vs['ajpk'] + $vs['nsk'];

            $vs['lrd'] = $vs['lrd'] + $vs['nspd'];
            $vs['lrk'] = $vs['lrk'] + $vs['nspk'];


            //jumlah
            if ($dbr['jenis'] == "D"){
                $vsjml['sad'] += $vs['sad'];
                $vsjml['sak'] += $vs['sak'];
                $vsjml['jrd'] += $vs['jrd'];
                $vsjml['jrk'] += $vs['jrk'];
                $vsjml['nsd'] += $vs['nsd'];
                $vsjml['nsk'] += $vs['nsk'];
                $vsjml['ajpd'] += $vs['ajpd'];
                $vsjml['ajpk'] += $vs['ajpk'];
                $vsjml['nspd'] += $vs['nspd'];
                $vsjml['nspk'] += $vs['nspk'];
                $vsjml['lrd'] += $vs['lrd'];
                $vsjml['lrk'] += $vs['lrk'];
                $vsjml['nrcd'] += $vs['nrcd'];
                $vsjml['nrck'] += $vs['nrck'];
            }

           

            $arrkd = explode(".", $dbr['kode']);
            $level = count($arrkd);
            foreach ($vs as $key => $val) {
                if ($key !== "kode" && $key !== "keterangan") {
                    $vs[$key] = string_2sz($val);
                }

            }
            //bold text
            if ($dbr['jenis'] == "I" and $va['level'] > $level) {
                $vs["kode"] = "<b>" . $vs["kode"];
                $vs["nrck"] = $vs["nrck"] . "</b>";
            }

            //susun array untuk struktur
            if ($va['level'] >= $level) {
                $vare[] = $vs;
            }

        }
        $data['body'] = $vare;

        
        $lr = $vsjml['lrd'] - $vsjml['lrk'];
        if($lr > 0){
            $vslr['lrk'] = $lr;
        }else{
            $vslr['lrd'] = ($lr * -1);
        }
        $vslr['nrcd'] = $vslr['lrk'];
        $vslr['nrck'] = $vslr['lrd'];

        
        $vsnrc['lrd'] = $vslr['lrd'] + $vsjml['lrd'];
        $vsnrc['lrk'] = $vslr['lrk'] + $vsjml['lrk'];
        $vsnrc['nrcd'] = $vslr['nrcd'] + $vsjml['nrcd'];
        $vsnrc['nrck'] = $vslr['nrck'] + $vsjml['nrck'];
        
        
        foreach ($vsjml as $key => $val) {
            if ($key !== "kode" && $key !== "keterangan") {
                $vsjml[$key] = string_2sz($val);
            }
        }
        $vsjml['nrck'] = $vsjml['nrck'] . "</b>";


        foreach ($vslr as $key => $val) {
            if ($key !== "kode" && $key !== "keterangan") {
                $vslr[$key] = string_2sz($val);
            }
        }
        $vslr['nrck'] = $vslr['nrck'] . "</b>";

        foreach ($vsnrc as $key => $val) {
            if ($key !== "kode" && $key !== "keterangan") {
                $vsnrc[$key] = string_2sz($val);
            }
        }
        $vsnrc['nrck'] = $vsnrc['nrck'] . "</b>";


        $data['footerjml'][0] = $vsjml;
        $data['footerlr'][0] = $vslr;
        $data['footernrc'][0] = $vsnrc;
        savesession($this, "rptneracalajur_rpt", json_encode($data));
        echo (' bos.rptneracalajur.openreport() ; ');
    }

    public function showreport()
    {
        $va = json_decode(getsession($this, $this->ss . "va", "{}"), true);
        $data = getsession($this, "rptneracalajur_rpt");
        $data = json_decode($data, true);
        
        if (!empty($data)) {
            $font = 5;
            if (!isset($va['export'])) {
                $va['export'] = 0;
            }

            $o = array('paper' => 'A4', 'orientation' => 'landscape', 'export' => $va['export'],
                'opt' => array('export_name' => 'DaftarNeracalajur_' . getsession($this, "username")));
            $this->load->library('bospdf', $o);
            $cabang = getsession($this, "cabang");
            $arrcab = $this->func_m->GetDataCabang($cabang);
            $this->bospdf->ezText($arrcab['kode'] . " - " . $arrcab['nama'], $font + 4, array("justification" => "center"));
            $this->bospdf->ezText($arrcab['alamat'] . " / " . $arrcab['telp'], $font, array("justification" => "center"));
            $this->bospdf->ezText("<b>NERACA LAJUR</b>", $font + 4, array("justification" => "center"));
            $this->bospdf->ezText("Tgl : " . $va['tglakhir'], $font + 2, array("justification" => "center"));
            $this->bospdf->ezText("");
            $this->bospdf->ezTable($data['body'], "", "",
                array("showLines" => 2, "showHeadings" => 1, "fontSize" => $font, "cols" => array(
                    "kode"       => array("caption" => "Kode", "width" => 5),
                    "keterangan" => array("caption" => "Keterangan", "wrap" => 1),
                    "sad"        => array("caption" => "Saldo Awal;Debet", "width" => 6, "justification" => "right"),
                    "sak"        => array("caption" => "Saldo Awal;Kredit", "width" => 6, "justification" => "right"),
                    "jrd"        => array("caption" => "Jurnal;Debet", "width" => 6, "justification" => "right"),
                    "jrk"        => array("caption" => "Jurnal;Kredit", "width" => 6, "justification" => "right"),
                    "nsd"        => array("caption" => "Neraca Saldo;Debet", "width" => 6, "justification" => "right"),
                    "nsk"        => array("caption" => "Neraca Saldo;Kredit", "width" => 6, "justification" => "right"),
                    "ajpd"       => array("caption" => "Penyesuaian;Debet", "width" => 6, "justification" => "right"),
                    "ajpk"       => array("caption" => "Penyesuaian;Kredit", "width" => 6, "justification" => "right"),
                    "nspd"       => array("caption" => "NS Disesuaikan;Debit", "width" => 6, "justification" => "right"),
                    "nspk"       => array("caption" => "NS Disesuaikan;Kredit", "width" => 6, "justification" => "right"),
                    "lrd"        => array("caption" => "Laba Rugi;Debet", "width" => 6, "justification" => "right"),
                    "lrk"        => array("caption" => "Laba Rugi;Kredit", "width" => 6, "justification" => "right"),
                    "nrcd"        => array("caption" => "Neraca;Debet", "width" => 6, "justification" => "right"),
                    "nrck"        => array("caption" => "Neraca;Kredit", "width" => 6, "justification" => "right")
                )));
            $this->bospdf->ezTable($data['footerjml'], "", "",
                array("showLines" => 2, "showHeadings" => 0, "fontSize" => $font, "cols" => array(
                    "keterangan" => array("caption" => "Keterangan", "wrap" => 1,"justification" => "center"),
                    "sad"        => array("caption" => "Saldo Awal;Debet", "width" => 6, "justification" => "right"),
                    "sak"        => array("caption" => "Saldo Awal;Kredit", "width" => 6, "justification" => "right"),
                    "jrd"        => array("caption" => "Jurnal;Debet", "width" => 6, "justification" => "right"),
                    "jrk"        => array("caption" => "Jurnal;Kredit", "width" => 6, "justification" => "right"),
                    "nsd"        => array("caption" => "Neraca Saldo;Debet", "width" => 6, "justification" => "right"),
                    "nsk"        => array("caption" => "Neraca Saldo;Kredit", "width" => 6, "justification" => "right"),
                    "ajpd"       => array("caption" => "Penyesuaian;Debet", "width" => 6, "justification" => "right"),
                    "ajpk"       => array("caption" => "Penyesuaian;Kredit", "width" => 6, "justification" => "right"),
                    "nspd"       => array("caption" => "NS Disesuaikan;Debit", "width" => 6, "justification" => "right"),
                    "nspk"       => array("caption" => "NS Disesuaikan;Kredit", "width" => 6, "justification" => "right"),
                    "lrd"        => array("caption" => "Laba Rugi;Debet", "width" => 6, "justification" => "right"),
                    "lrk"        => array("caption" => "Laba Rugi;Kredit", "width" => 6, "justification" => "right"),
                    "nrcd"        => array("caption" => "Neraca;Debet", "width" => 6, "justification" => "right"),
                    "nrck"        => array("caption" => "Neraca;Kredit", "width" => 6, "justification" => "right")
                )));
            $this->bospdf->ezTable($data['footerlr'], "", "",
                array("showLines" => 2, "showHeadings" => 0, "fontSize" => $font, "cols" => array(
                    "keterangan" => array("caption" => "Keterangan", "wrap" => 1,"justification" => "center"),
                    "lrd"        => array("caption" => "Laba Rugi;Debet", "width" => 6, "justification" => "right"),
                    "lrk"        => array("caption" => "Laba Rugi;Kredit", "width" => 6, "justification" => "right"),
                    "nrcd"        => array("caption" => "Neraca;Debet", "width" => 6, "justification" => "right"),
                    "nrck"        => array("caption" => "Neraca;Kredit", "width" => 6, "justification" => "right")
                )));
            $this->bospdf->ezTable($data['footernrc'], "", "",
                array("showLines" => 2, "showHeadings" => 0, "fontSize" => $font, "cols" => array(
                    "keterangan" => array("caption" => "Keterangan", "wrap" => 1,"justification" => "center"),
                    "lrd"        => array("caption" => "Laba Rugi;Debet", "width" => 6, "justification" => "right"),
                    "lrk"        => array("caption" => "Laba Rugi;Kredit", "width" => 6, "justification" => "right"),
                    "nrcd"        => array("caption" => "Neraca;Debet", "width" => 6, "justification" => "right"),
                    "nrck"        => array("caption" => "Neraca;Kredit", "width" => 6, "justification" => "right")
                )));
            //print_r($data) ;
            $this->bospdf->ezStream();
        } else {
            echo ('data kosong');
        }
    }

}
?>
