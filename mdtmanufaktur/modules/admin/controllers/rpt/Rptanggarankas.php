
<?php
class rptanggarankas extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model("rpt/rptanggarankas_m") ;
        $this->load->model("func/perhitungan_m") ;
        $this->bdb 	= $this->rptanggarankas_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptanggarankas_" ;
    }  

    public function index(){ 
        $this->load->view("rpt/rptanggarankas") ; 

    }
	
	
    public function loadgrid(){ 

        $va     = json_decode($this->input->post('request'), true) ; 
        $tglawal 	= $va['tglawal'] ; //date("d-m-Y") ;
        $tglakhir 	= $va['tglakhir'] ;	//date("d-m-Y") ;
        $tglkemarin = date("d-m-Y",strtotime($tglawal)-(24*60*60)) ;

        $vare   = array() ;

        //load rek kas
        $vare[]=array("no"=>"","tgl"=>"","keterangan"=>"<b>:: Saldo Awal Kas / Setara Kas ".$tglawal." ::</b>","penerimaan"=>"","pengeluaran"=>"","jumlah"=>"");
        $dbd = $this->rptanggarankas_m->loadrekkas();
        $n = 0 ;
        $jmlkas = 0 ;
        while($dbr = $this->bdb->getrow($dbd)){
            $n++;
            $saldo = $this->perhitungan_m->getsaldoawal($tglawal,$dbr['kode']);
            $jmlkas += $saldo;
            $vare[]=array("no"=>$n,"tgl"=>"","keterangan"=>$dbr['kode']."-".$dbr['keterangan'],"penerimaan"=>$saldo,"pengeluaran"=>"","jumlah"=>"");
        }
        $vare[]=array("no"=>"","tgl"=>"","keterangan"=>"<b>Total Saldo Awal Kas ".$tglawal."</b>","penerimaan"=>"","pengeluaran"=>"","jumlah"=>$jmlkas);

        //load data anggaran kas
        $arrangarankas = array();
        $vare[]=array("no"=>"","tgl"=>"","keterangan"=>"","penerimaan"=>"","pengeluaran"=>"");
        $dbd = $this->rptanggarankas_m->loaddataanggarankas($va);
        $n = 0 ;
        $jmld = 0 ;
        $jmlk = 0 ;
        $vare[]=array("no"=>"","tgl"=>"","keterangan"=>"<b>:: Anggaran Pengeluaran dan Penerimaan Kas ::</b>","penerimaan"=>"","pengeluaran"=>"");
        while($dbr = $this->bdb->getrow($dbd)){
            $n++;
            $jmld += $dbr['debet'] ;
            $jmlk += $dbr['kredit'] ;
            $time = strtotime($dbr['tgl']);
            $arrangarankas[$time."-".$n]=array("no"=>$n,"tgl"=>date_2d($dbr['tgl']),"keterangan"=>$dbr['keterangan'],
                "penerimaan"=>$dbr['debet'],"pengeluaran"=>$dbr['kredit']);
        }

        //load data bgjatuhtempo
        $dbd = $this->rptanggarankas_m->loadbgjatuhtempo($va);
        
        while($dbr = $this->bdb->getrow($dbd)){
            $n++;
            $jmlk += $dbr['nominal'] ;
            $keterangan = "Jth Tmp BG No ".$dbr['nobgcek']." - ".$dbr['ketbank']." Pemby. Ke ".$dbr['namasupplier'];
            $time = strtotime($dbr['tgljthtmp']);
            $arrangarankas[$time."-".$n]=array("no"=>$n,"tgl"=>date_2d($dbr['tgljthtmp']),"keterangan"=>$keterangan,
                "penerimaan"=>0,"pengeluaran"=>$dbr['nominal']);
        }

        //load data PBjatuhtempo
        $dbd = $this->rptanggarankas_m->loadhtjatuhtempo($va);
        
        while($dbr = $this->bdb->getrow($dbd)){
            $n++;
            $jmlk += $dbr['hutang'] ;
            $keterangan = "JT HD No ".$dbr['faktur']." - ".$dbr['namasupplier'];
            $time = strtotime($dbr['jthtmp']);
            $arrangarankas[$time."-".$n]=array("no"=>$n,"tgl"=>date_2d($dbr['jthtmp']),"keterangan"=>$keterangan,
                "penerimaan"=>0,"pengeluaran"=>$dbr['hutang']);
        }

        //mengurutkan array dari tgl
        ksort($arrangarankas);
        $n = 0 ;
        foreach($arrangarankas as $key => $val){
            $n++;
            $val['no'] = $n;
            $vare[]=$val;
        }

        $jmlanggaran = $jmlk - $jmld;
        $jmlanggaran = $jmlanggaran *-1;
        $vare[]=array("no"=>"","tgl"=>"","keterangan"=>"<b>Total Anggaran Kas </b>","penerimaan"=>$jmld,"pengeluaran"=>$jmlk,"jumlah"=>$jmlanggaran);
        $vare[]=array("no"=>"","tgl"=>"","keterangan"=>"<b>Total</b>","penerimaan"=>"","pengeluaran"=>"","jumlah"=>$jmlkas+$jmlanggaran);
        
        $vare 	= array("total"=>count($vare), "records"=>$vare ) ; 
        echo(json_encode($vare)) ; 
    }

    public function initreport(){
        $n=0;
        $va     = $this->input->post() ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;

        
        $tglawal 	= $va['tglawal'] ; //date("d-m-Y") ;
        $tglakhir 	= $va['tglakhir'] ;
        $tglkemarin = date("d-m-Y",strtotime($tglawal)-(24*60*60)) ;

        $vare   = array() ;


        //load rek kas
        $vare[]=array("no"=>"<mdtlr10>","tgl"=>"","keterangan"=>"<b>:: Saldo Awal Kas / Setara Kas ".$tglawal." ::","penerimaan"=>"","pengeluaran"=>"","jumlah"=>"</b></mdtlr10>");
        $dbd = $this->rptanggarankas_m->loadrekkas();
        $n = 0 ;
        $jmlkas = 0 ;
        while($dbr = $this->bdb->getrow($dbd)){
            $n++;
            $saldo = $this->perhitungan_m->getsaldoawal($tglawal,$dbr['kode']);
            $jmlkas += $saldo;
            $vare[]=array("no"=>$n,"tgl"=>"","keterangan"=>$dbr['kode']."-".$dbr['keterangan'],"penerimaan"=>string_2s($saldo),"pengeluaran"=>"","jumlah"=>"");
        }
        $vare[]=array("no"=>"","tgl"=>"","keterangan"=>"<b>Total Saldo Awal Kas ".$tglawal."","penerimaan"=>"","pengeluaran"=>"","jumlah"=>"<mdtlr01>".string_2s($jmlkas)."</b></mdtlr01>");//"<mdtlr01>".

        //load data anggaran kas
        $arrangarankas = array();
        $vare[]=array("no"=>"","tgl"=>"","keterangan"=>"","penerimaan"=>"","pengeluaran"=>"","jumlah"=>"");
        $dbd = $this->rptanggarankas_m->loaddataanggarankas($va);
        $n = 0 ;
        $jmld = 0 ;
        $jmlk = 0 ;
        $vare[]=array("no"=>"","tgl"=>"","keterangan"=>"<b>:: Anggaran Pengeluaran dan Penerimaan Kas ::</b>",
        "penerimaan"=>"","pengeluaran"=>"","jumlah"=>"");
        while($dbr = $this->bdb->getrow($dbd)){
            $n++;
            $jmld += $dbr['debet'] ;
            $jmlk += $dbr['kredit'] ;
            $time = strtotime($dbr['tgl']);
            $arrangarankas[$time."-".$n]=array("no"=>$n,"tgl"=>date_2d($dbr['tgl']),"keterangan"=>$dbr['keterangan'],
                "penerimaan"=>string_2s($dbr['debet']),"pengeluaran"=>string_2s($dbr['kredit']),"jumlah"=>"");
        }

        //load data bgjatuhtempo
        $dbd = $this->rptanggarankas_m->loadbgjatuhtempo($va);
        
        while($dbr = $this->bdb->getrow($dbd)){
            $n++;
            $jmlk += $dbr['nominal'] ;
            $keterangan = "Jth Tmp BG No ".$dbr['nobgcek']." - ".$dbr['ketbank']." Pemby. Ke ".$dbr['namasupplier'];
            $time = strtotime($dbr['tgljthtmp']);
            $arrangarankas[$time."-".$n]=array("no"=>$n,"tgl"=>date_2d($dbr['tgljthtmp']),"keterangan"=>$keterangan,
                "penerimaan"=>0,"pengeluaran"=>string_2s($dbr['nominal']),"jumlah"=>"");
        }

        //load data PBjatuhtempo
        $dbd = $this->rptanggarankas_m->loadhtjatuhtempo($va);
        
        while($dbr = $this->bdb->getrow($dbd)){
            $n++;
            $jmlk += $dbr['hutang'] ;
            $keterangan = "JT HD No ".$dbr['faktur']." - ".$dbr['namasupplier'];
            $time = strtotime($dbr['jthtmp']);
            $arrangarankas[$time."-".$n]=array("no"=>$n,"tgl"=>date_2d($dbr['jthtmp']),"keterangan"=>$keterangan,
                "penerimaan"=>0,"pengeluaran"=>string_2s($dbr['hutang']),"jumlah"=>"");
        }

        //mengurutkan array dari tgl
        ksort($arrangarankas);
        $n = 0 ;
        foreach($arrangarankas as $key => $val){
            $n++;
            $val['no'] = $n;
            $vare[]=$val;
        }

        $jmlanggaran = $jmlk - $jmld;
        $jmlanggaran = $jmlanggaran * -1;
        $vare[]=array("no"=>"","tgl"=>"","keterangan"=>"<b>Total Anggaran Kas","penerimaan"=>string_2s($jmld),"pengeluaran"=>string_2s($jmlk),"jumlah"=>string_2s($jmlanggaran)."</b>");
        $total = $jmlkas+$jmlanggaran;
        $vare[]=array("no"=>"<mdtlr11>","tgl"=>"","keterangan"=>"<b>Total","penerimaan"=>"","pengeluaran"=>"","jumlah"=>string_2s($total)."</b></mdtlr11>");//<mdtlr11>
        /*foreach($vare as $key => $val){
            $vare[$key]['no'] = "<mdtls11>".$val['no'];
            $vare[$key]['jumlah'] = $val['jumlah']."</mdtls11>";
        }*/
        savesession($this, "rptanggarankas_rpt", json_encode($vare)) ; 
        echo(' bos.rptanggarankas.openreport() ; ') ;
    }

    public function showreport(){
      $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
      $data = getsession($this,"rptanggarankas_rpt") ;      
      $data = json_decode($data,true) ;       
      if(!empty($data)){ 

        $now  = date_2b(date("Y-m-d")) ;
        $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];

        $vttd = array() ;
        $vttd[] = array("1"=>"Mengetahui,","2"=>"","3"=>"","4"=>"","5"=>$kota) ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"(.........................)","2"=>"","3"=>"","4"=>"","5"=>"(.........................)") ;
        $vttd[] = array("1"=>"Manager","2"=>"","3"=>"","4"=>"","5"=>"Bag. Keuangan") ;

        $vttd[] = array("1"=>",","2"=>"","3"=>"Menyetujui,","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"(.........................)","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"Direktur","4"=>"","5"=>"") ;

      	$font = 8 ;
        $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>"",
                        'opt'=>array('export_name'=>'DaftarNeraca_' . getsession($this, "username") ) ) ;
        $this->load->library('bospdf', $o) ;   
        $this->bospdf->ezText("<b>LAPORAN ANGGARAN KAS</b>",$font+4,array("justification"=>"center")) ;
        $this->bospdf->ezText("Periode : ".$va['tglawal']." sd ". $va['tglakhir'],$font+2,array("justification"=>"center")) ;
        
        $this->bospdf->ezText("") ; 
		$this->bospdf->ezTable($data,"","",  
								array("showHeadings"=>"1","showLines"=>"2","fontSize"=>$font,"cols"=> array( 
                                 "no"=>array("caption"=>"No","width"=>5,"justification"=>"left"),
                                 "tgl"=>array("caption"=>"Tgl","width"=>10,"justification"=>"center"),
                                 "keterangan"=>array("caption"=>"Keterangan","wrap"=>0),
			                     "penerimaan"=>array("caption"=>"Penerimaan","width"=>14,"justification"=>"right"),
			                 	 "pengeluaran"=>array("caption"=>"Pengeluaran","width"=>14,"justification"=>"right"),
			                     "jumlah"=>array("caption"=>"Jumlah","width"=>14,"justification"=>"right")))) ;   
        //print_r($data) ;    

        $this->bospdf->ezText("") ;
        $this->bospdf->ezText("") ;
        $this->bospdf->ezTable($vttd,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "1"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                             "2"=>array("justification"=>"right"),
                                             "3"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                             "4"=>array("justification"=>"right"),
                                             "5"=>array("width"=>25,"wrap"=>1,"justification"=>"center"))
                                        )
                                  ) ;

        $this->bospdf->ezStream() ; 
      }else{
         echo('data kosong') ;
      }
   }
}
?>
