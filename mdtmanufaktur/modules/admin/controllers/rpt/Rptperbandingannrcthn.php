
<?php
class Rptperbandingannrcthn extends Bismillah_Controller{
	protected $bdb ;
	public function __construct(){
		parent::__construct() ;
		$this->load->model("rpt/rptperbandingannrcthn_m") ;
        $this->load->model("func/perhitungan_m") ;
		$this->bdb 	= $this->rptperbandingannrcthn_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptperbandingannrcthn_" ;
	}  

	public function index(){ 
		$this->load->view("rpt/rptperbandingannrcthn") ; 

	}   

	public function loadgrid(){ 

	  	$va     = json_decode($this->input->post('request'), true) ; 
        $periode = $va['periode'];
        $arrdata = array();
        for($i=$periode;$i>=$periode-1;$i--){      
            $time = mktime(0,0,0,12,31,$i);//akhir thn
            $tglakhir = date("Y-m-d",$time);
  
            $time2 = mktime(0,0,0,1,1,$i);//awal thn
            $tglawal = date("Y-m-d",$time2);

            $arrlr = $this->perhitungan_m->getlr($tglawal,$tglakhir);//cek laba rugi
  
            $arrdata[] = array("thn"=>$i,"tglawal"=>$tglawal,"tglakhir"=>$tglakhir,"lr"=>$arrlr);
        }
        
	  	$vare   = array() ; 
        
        // AKTIVA
        $totaktiva = array();
        $totaktiva['kode'] = "";
        $totaktiva['keterangan'] = "Total Aktiva";
	    $vdb    = $this->perhitungan_m->loadrekening("1","1.9999") ;
	    while($dbr = $this->bdb->getrow($vdb) ){
			//$vs = $dbr;  
            $vs = array();
            $vs['kode'] = $dbr['kode'];
            $vs['keterangan'] = $dbr['keterangan'];
            foreach($arrdata as $key => $val){
                $vs[$val['thn']] = $this->perhitungan_m->getsaldo($val['tglakhir'],$dbr['kode']) ;
                
                if($dbr['jenis'] == "D"){
                    if(!isset($totaktiva[$val['thn']]))$totaktiva[$val['thn']] = 0 ;
                    $totaktiva[$val['thn']] += $vs[$val['thn']];
                }
                $vs[$val['thn']] = string_2s($vs[$val['thn']]);
            }
			


            //bold text
            if($dbr['jenis'] == "I"){
                foreach($vs as $key => $val){
                    $vs[$key] = "<b>".$val."</b>";
                }
            }
            
			$vare[]		= $vs ;
		} 

        foreach($totaktiva as $key => $val){
            if($key <> "kode" && $key <> "keterangan")$val = string_2s($val);
            $totaktiva[$key] = "<b>".$val."</b>";
        }
	    $vare[] = $totaktiva ;  

        $vare[] = array("kode"=>"","keterangan"=>"") ;

        // PASIVA
        $reklrthnberjalan = $this->bdb->getconfig("reklrthberjalan");
        $reklrblnberjalan = $this->bdb->getconfig("reklrblnberjalan");
        
        $totpasiva = array();
        $totpasiva['kode'] = "";
        $totpasiva['keterangan'] = "Total Pasiva";
	    $vdb    = $this->perhitungan_m->loadrekening("2","3.9999") ;
	    while($dbr = $this->bdb->getrow($vdb) ){
			$vs = $dbr;  
            $vs = array();
            $vs['kode'] = $dbr['kode'];
            $vs['keterangan'] = $dbr['keterangan'];
            foreach($arrdata as $key => $val){
                $vs[$val['thn']] = $this->perhitungan_m->getsaldo($val['tglakhir'],$dbr['kode']) ;
                
                
                if(substr($reklrthnberjalan,0,strlen($reklrthnberjalan)) == substr($dbr['kode'],0,strlen($dbr['kode']))){  
                    $vs[$val['thn']] += $val['lr']['lrstlhpjk']['saldoawal']; 
                }

                if(substr($reklrblnberjalan,0,strlen($vs['kode'])) == substr($vs['kode'],0,strlen($vs['kode']))){
                    $vs[$val['thn']] += $val['lr']['lrstlhpjk']['saldoakhirperiod']; 
                }

                if($dbr['jenis'] == "D"){
                    if(!isset($totpasiva[$val['thn']]))$totpasiva[$val['thn']] = 0 ;
                    $totpasiva[$val['thn']] += $vs[$val['thn']];
                }
                $vs[$val['thn']] = string_2s($vs[$val['thn']]);
            }


            //bold text
                $arrlr = $this->perhitungan_m->getlr($val['tglawal'],$val['tglakhir']);//cek laba rugi
                if($dbr['jenis'] == "I"){
                foreach($vs as $key => $val){
                    $vs[$key] = "<b>".$val."</b>";
                }
            }
            
            unset($vs['jenis']);

			$vare[]		= $vs ;
		} 

	    foreach($totpasiva as $key => $val){
            if($key <> "kode" && $key <> "keterangan")$val = string_2s($val);
            $totpasiva[$key] = "<b>".$val."</b>";
        }
	    $vare[] = $totpasiva ;  

		$vare 	= array("total"=>count($vare), "records"=>$vare ) ; 
        $varpt = $vare['records'] ;
      	echo(json_encode($vare)) ; 
      	savesession($this, "rptperbandingannrcthn_rpt", json_encode($varpt)) ;  
	}

	public function init(){
		savesession($this, "ssrptperbandingannrcthn_id", "") ;    
	}
    
    public function initreport(){
        $n=0;
        $va     = $this->input->post() ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;

	    $periode = $va['periode'];
        $arrdata = array();
        for($i=$periode;$i>=$periode-1;$i--){      
            $time = mktime(0,0,0,12,31,$i);//akhir thn
            $tglakhir = date("Y-m-d",$time);
  
            $time2 = mktime(0,0,0,1,1,$i);//awal thn
            $tglawal = date("Y-m-d",$time2);

            $arrlr = $this->perhitungan_m->getlr($tglawal,$tglakhir);//cek laba rugi
            $arrdata[] = array("thn"=>$i,"tglawal"=>$tglawal,"tglakhir"=>$tglakhir,"lr"=>$arrlr);
        }
        
	  	$vare   = array() ; 
        
        // AKTIVA
        $totaktiva = array();
        $totaktiva['kode'] = "";
        $totaktiva['keterangan'] = "Total Aktiva";
	    $vdb    = $this->perhitungan_m->loadrekening("1","1.9999") ;
	    while($dbr = $this->bdb->getrow($vdb) ){
			//$vs = $dbr;  
            $vs = array();
            $vs['kode'] = $dbr['kode'];
            $vs['keterangan'] = $dbr['keterangan'];
            foreach($arrdata as $key => $val){
                $vs[$val['thn']] = $this->perhitungan_m->getsaldo($val['tglakhir'],$dbr['kode']) ;
                
                if($dbr['jenis'] == "D"){
                    if(!isset($totaktiva[$val['thn']]))$totaktiva[$val['thn']] = 0 ;
                    $totaktiva[$val['thn']] += $vs[$val['thn']];
                }
                $vs[$val['thn']] = string_2s($vs[$val['thn']]);
            }
			


            //bold text
            if($dbr['jenis'] == "I"){
                foreach($vs as $key => $val){
                    $vs[$key] = "<b>".$val."</b>";
                }
            }
            
			$vare[]		= $vs ;
		} 
        $jarak = array();
        foreach($totaktiva as $key => $val){
            if($key <> "kode" && $key <> "keterangan")$val = string_2s($val);
            $totaktiva[$key] = "<b>".$val."</b>";
            $jarak[$key]="";
        }
	    $vare[] = $totaktiva ;  

        $vare[] = $jarak ;

        // PASIVA
        $reklrthnberjalan = $this->bdb->getconfig("reklrthberjalan");
        $reklrblnberjalan = $this->bdb->getconfig("reklrblnberjalan");
        
        $totpasiva = array();
        $totpasiva['kode'] = "";
        $totpasiva['keterangan'] = "Total Pasiva";
	    $vdb    = $this->perhitungan_m->loadrekening("2","3.9999") ;
	    while($dbr = $this->bdb->getrow($vdb) ){
			$vs = $dbr;  
            $vs = array();
            $vs['kode'] = $dbr['kode'];
            $vs['keterangan'] = $dbr['keterangan'];
            foreach($arrdata as $key => $val){
                $vs[$val['thn']] = $this->perhitungan_m->getsaldo($val['tglakhir'],$dbr['kode']) ;
                
                if(substr($reklrthnberjalan,0,strlen($reklrthnberjalan)) == substr($dbr['kode'],0,strlen($dbr['kode']))){  
                    $vs[$val['thn']] +=$val['lr']['lrstlhpjk']['saldoawal']; 
                }

                if(substr($reklrblnberjalan,0,strlen($dbr['kode'])) == substr($dbr['kode'],0,strlen($dbr['kode']))){
                    $vs[$val['thn']] += $val['lr']['lrstlhpjk']['saldoakhirperiod']; 
                }

                if($dbr['jenis'] == "D"){
                    if(!isset($totpasiva[$val['thn']]))$totpasiva[$val['thn']] = 0 ;
                    $totpasiva[$val['thn']] += $vs[$val['thn']];
                }
                $vs[$val['thn']] = string_2s($vs[$val['thn']]);
            }


            //bold text
            if($dbr['jenis'] == "I"){
                foreach($vs as $key => $val){
                    $vs[$key] = "<b>".$val."</b>";
                }
            }
            

			$vare[]		= $vs ;
		} 

	    foreach($totpasiva as $key => $val){
            if($key <> "kode" && $key <> "keterangan")$val = string_2s($val);
            $totpasiva[$key] = "<b>".$val."</b>";
        }
	    $vare[] = $totpasiva ;

		$vare 	= array("total"=>count($vare), "records"=>$vare ) ; 
        $varpt = $vare['records'] ;
      	savesession($this, "rptperbandingannrcthn_rpt", json_encode($varpt)) ; 
        echo(' bos.rptperbandingannrcthn.openreport() ; ') ;
    }

	public function showreport(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $data = getsession($this,"rptperbandingannrcthn_rpt") ;      
        $data = json_decode($data,true) ;     
        
        $periode = $va['periode'];
        $frmtkolom = array();
        $frmtkolom['kode'] = array("caption"=>"Kode","width"=>10);
        $frmtkolom['keterangan'] = array("caption"=>"Keterangan");
        for($i=$periode;$i>=$periode-1;$i--){      
            $frmtkolom[$i] = array("width"=>17,"justification"=>"right");
        }
        //print_r($data);
      if(!empty($data)){ 
      	$font = 8 ;
        $o    = array('paper'=>'A4', 'orientation'=>'portrait', 'export'=>"",
                        'opt'=>array('export_name'=>'DaftarPerbandinganNeraca_' . getsession($this, "username") ) ) ;
        $this->load->library('bospdf', $o) ;   
        $this->bospdf->ezText("<b>LAPORAN PERBANDINGAN NERACA</b>",$font+4,array("justification"=>"center")) ;
        $this->bospdf->ezText("Periode : ".$va['periode'],$font+2,array("justification"=>"center")) ;
		$this->bospdf->ezText("") ; 
		$this->bospdf->ezTable($data,"","",  
								array("fontSize"=>$font,"cols"=>$frmtkolom)) ;   
        //print_r($data) ;    
        $this->bospdf->ezStream() ; 
      }else{
         echo('data kosong') ;
      }
   }

}
?>
