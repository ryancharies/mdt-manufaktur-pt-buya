<?php

class Rpthasilproduksikaru extends Bismillah_Controller{
    protected $bdb ;
    protected $ss ;
    protected $abc ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper('bdate');
        $this->load->model('rpt/rpthasilproduksikaru_m') ;
        $this->load->model('func/func_m') ;
        $this->load->library('escpos');
        $this->bdb = $this->rpthasilproduksikaru_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrpthasilproduksikaru_" ;
    }

    public function index(){
        $d    = array("setdate"=>date_set()) ;
        $this->load->view('rpt/rpthasilproduksikaru', $d) ;
    }

    public function loadgrid_where($bs, $s=''){
        $this->duser();

        $bs['tglawal'] = date_2s($bs['tglawal']);
        $bs['tglakhir'] = date_2s($bs['tglakhir']);

        $this->db->where('t.status', "1");
        $this->db->where('t.tgl >=', $bs['tglawal']);
        $this->db->where('t.tgl <=', $bs['tglakhir']);

        // print_r($bs);

        if($bs['skd_cabang'] !== 'null'){
            $bs['skd_cabang'] = json_decode($bs['skd_cabang'],true);
            $this->db->group_start();
            foreach($bs['skd_cabang'] as $kdc){
                $this->db->or_where('t.cabang',$kdc);
            }
            $this->db->group_end();

        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['cabang'])){
                    $this->db->group_start();
                    foreach($this->aruser['data_var']['cabang'] as $kdc){
                        $this->db->or_where('t.cabang', $kdc);
                    }
                    $this->db->group_end();
                }
            }
        }

        if($s !== ''){
            $this->db->group_start();
                $this->db->or_like(array('t.petugas'=>$s));
            $this->db->group_end();
        }
    }

    public function loadgrid(){
        $va         = json_decode($this->input->post('request'), true) ;
        $limit      = $va['offset'].",".$va['limit'] ;
        $search	    = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search     = $this->db->escape_like_str($search) ;
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $vare  = array() ;

        $this->loadgrid_where($va, $search);
        $f = "if(t.regu is null,t.petugas,k.nama) karuprod,r.keterangan regu,r.karu kodekaru,
            (select ifnull(count(a.id),0) from produksi_total a where a.status = '1' and perbaikan = 'N' and ((a.tgl >= '{$va['tglawal']}' and a.tgl <= '{$va['tglakhir']}') or (a.tglclose >= '{$va['tglawal']}' and a.tglclose <= '{$va['tglakhir']}') or (a.tgl <= '{$va['tglakhir']}' and (a.tglclose = '0000-00-00' or a.tglclose >= '{$va['tglakhir']}'))) and JSON_EXTRACT(a.regu,'$.karu') = r.karu) as prodbaru,
            (select ifnull(count(a.id),0) from produksi_total a where a.status = '1' and perbaikan = 'Y' and ((a.tgl >= '{$va['tglawal']}' and a.tgl <= '{$va['tglakhir']}') or (a.tglclose >= '{$va['tglawal']}' and a.tglclose <= '{$va['tglakhir']}') or (a.tgl <= '{$va['tglakhir']}' and (a.tglclose = '0000-00-00' or a.tglclose >= '{$va['tglakhir']}'))) and JSON_EXTRACT(a.regu,'$.karu')  = r.karu) as prodperbaikan,
            (select ifnull(count(a.id),0) from produksi_total a where a.status = '1' and (a.tgl <= '{$va['tglakhir']}' and (a.tglclose = '0000-00-00' or a.tglclose >= '{$va['tglakhir']}')) and JSON_EXTRACT(a.regu,'$.karu') = r.karu) as prodproses,
            (select ifnull(count(a.id),0) from produksi_total a where a.status = '1' and a.tglclose >= '{$va['tglawal']}' and a.tglclose <= '{$va['tglakhir']}' and JSON_EXTRACT(a.regu,'$.karu') = r.karu) as prodselesai";
        $dbd = $this->db->select($f)
            ->from("produksi_total t")
            ->join("karyawan k","k.kode = JSON_EXTRACT(t.regu,'$.karu')","left")
            ->join("produksi_regu r","r.kode = JSON_EXTRACT(t.regu,'$.kode')","left")
            ->group_by("karuprod")
            ->order_by("karuprod asc")
            ->get();
        $rtot  = $dbd->num_rows();
        if($rtot > 0){
            $this->loadgrid_where($va, $search);

            $f2 = "if(t.regu is null,t.petugas,k.nama) karuprod,r.keterangan regu,r.karu kodekaru,
                (select ifnull(count(a.id),0) from produksi_total a where a.status = '1' and a.perbaikan = 'N' and JSON_EXTRACT(a.regu,'$.karu') = r.karu and ((a.tgl >= '{$va['tglawal']}' and a.tgl <= '{$va['tglakhir']}') or (a.tglclose >= '{$va['tglawal']}' and a.tglclose <= '{$va['tglakhir']}') or (a.tgl <= '{$va['tglakhir']}' and (a.tglclose = '0000-00-00' or a.tglclose >= '{$va['tglakhir']}')))) as prodbaru,
                (select ifnull(count(a.id),0) from produksi_total a where a.status = '1' and a.perbaikan = 'Y' and JSON_EXTRACT(a.regu,'$.karu') = r.karu and ((a.tgl >= '{$va['tglawal']}' and a.tgl <= '{$va['tglakhir']}') or (a.tglclose >= '{$va['tglawal']}' and a.tglclose <= '{$va['tglakhir']}') or (a.tgl <= '{$va['tglakhir']}' and (a.tglclose = '0000-00-00' or a.tglclose >= '{$va['tglakhir']}')))) as prodperbaikan,
                (select ifnull(count(a.id),0) from produksi_total a where a.status = '1' and JSON_EXTRACT(a.regu,'$.karu') = r.karu and (a.tgl <= '{$va['tglakhir']}' and (a.tglclose = '0000-00-00' or a.tglclose >= '{$va['tglakhir']}'))) as prodproses,
                (select ifnull(count(a.id),0) from produksi_total a where a.status = '1' and JSON_EXTRACT(a.regu,'$.karu') = r.karu and a.tglclose >= '{$va['tglawal']}' and a.tglclose <= '{$va['tglakhir']}') as prodselesai";
            $dbd2 = $this->db->select($f2)
                ->from("produksi_total t")
                ->join("karyawan k","k.kode = JSON_EXTRACT(t.regu,'$.karu')","left")
                ->join("produksi_regu r","r.kode = JSON_EXTRACT(t.regu,'$.kode')","left")
                ->group_by("karuprod")
                ->order_by("karuprod asc")
                ->limit($limit)
                ->get();
            foreach($dbd2->result_array() as $dbr){
                $vaset   = $dbr ;
                $vaset['karu'] = $dbr['karuprod'] . "-".$dbr['kodekaru'];
                 $dbr['kodekaru'] = str_replace('"','',$dbr['kodekaru']);
                $vaset['cmdpreview']    = '<button type="button" onClick="bos.rpthasilproduksikaru.cmdpreviewdetail(\''.$dbr['kodekaru'].'\')"
                                        class="btn btn-success btn-grid">Preview Detail</button>';
                $vaset['cmdpreview']    =  html_entity_decode($vaset['cmdpreview']) ;
                                $vare[]     = $vaset ;

            }
        }

        $vare    = array("total"=>$rtot, "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function PreviewDetail(){
        $va    = $this->input->post() ;
        echo('w2ui["bos-form-rpthasilproduksikaru_grid2"].clear();');
        //print_r($va);
        if(!empty($va['karu'])){

            $karu = $this->bdb->getval("nama", "kode = '{$va['karu']}'", "karyawan");

            echo('
                with(bos.rpthasilproduksikaru.obj){
                    find("#karu").val("'.$va['karu'].'") ;
                    find("#namakaru").val("'.$karu.'") ;
                }
            ') ;

            $dbd = $this->rpthasilproduksikaru_m->getdetailproduksikaru($va) ;
            $vare = array();
            $n = 0 ;
            foreach($dbd->result_array() as $dbr){
                $n++;
                $vaset          = $dbr ;
                $vaset['recid'] = $n;
                $vaset['no']    = $n;
                $vare[]         = $vaset ;
            }

            $vare = json_encode($vare);
            echo('
            bos.rpthasilproduksikaru.loadmodalpreview("show") ;
            bos.rpthasilproduksikaru.grid2_reloaddata();
            w2ui["bos-form-rpthasilproduksikaru_grid2"].add('.$vare.');
         ');
        }
    }

    public function initreport(){
        $va      = $this->input->post() ;

        $file   = setfile($this, "rpt", __FILE__ , $va) ;
        savesession($this, $this->ss . "file", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        file_put_contents($file, json_encode(array()) ) ;

        $file    = getsession($this, $this->ss . "file") ;
        $data    = @file_get_contents($file) ;
        $data    = json_decode($data,true) ;
        $db      = $this->bdb->getdetailproduksikaru($va) ;
        $nDebet  = 0 ;
        $nKredit = 0 ;
        $nSaldo  = 0 ;
        $s = 0 ;
        while($dbr = $this->bdb->getrow($db)){
            $no      = ++$s ;
            $data[]  = array("No"=>$no,
                             "Kode"=>$dbr['kode'],
                             "Keterangan"=>$dbr['keterangan'],
                             "Prod. Selesai"=>string_2s($dbr['prodselesai']),
                             "Satuan"=>$dbr['satuan']
                            ) ;
        }
        file_put_contents($file, json_encode($data) ) ;
        echo(' bos.rpthasilproduksikaru.openreport() ; ') ;
    }

    public function initreportTotal(){
        $va         = $this->input->post() ;
        $dtglawal   = date_2s($va['tglawal']);
        $dtglakhir  = date_2s($va['tglakhir']);
        $file       = setfile($this, "rpt_hptotal", __FILE__ , $va) ;
        savesession($this, $this->ss . "file", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        file_put_contents($file, json_encode(array()) ) ;

        $file    = getsession($this, $this->ss . "file") ;
        $data    = @file_get_contents($file) ;
        $data    = json_decode($data,true) ;
        $db      = $this->bdb->getTotalPP($dtglawal,$dtglakhir) ;
        $s       = 0 ;
        while ($dbRow = $this->bdb->getrow($db)) {
            $no      = ++$s ;
            $data[]  = array("No"=>$no,
                             "Faktur"=>$dbRow['faktur'],
                             "Faktur Prod"=>$dbRow['fakturproduksi'],
                             "Tgl"=>date_2d($dbRow['tgl']),
                             "QTY STD"=>string_2s($dbRow['qtystd']),
                             "N STD"=>string_2s($dbRow['std']),
                             "QTY Aktual"=>string_2s($dbRow['qtyaktual']),
                             "N Aktual"=>string_2s($dbRow['aktual'])
                            ) ;
        }
        file_put_contents($file, json_encode($data) ) ;
        echo(' bos.rpthasilproduksikaru.openreporttotal() ; ') ;

    }

    public function showreport(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $file = getsession($this, $this->ss . "file") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        //if(!empty($data)){
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> "" ,"5"=>$kota) ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"Mengetahui,","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"(.........................)","2"=>"","3"=>"(.........................)","4"=>"","5"=>"(.........................)") ;
            $vttd[] = array("1"=>"Manager","2"=>"","3"=>"Ka. Shiff","4"=>"","5"=>"Ka.Sie Produksi") ;



            /*$nTotalSaldo  = 0 ;
            $nNumber = 0 ;
            foreach ($data as $key => $value) {
                $nNumber       = string_2n($value['Jumlah']) ;
                $nTotalSaldo  += $nNumber;
            }
            $nTotalSaldo = string_2s($nTotalSaldo) ;*/

            $total   = array();
           /* $total[] = array("Ket"=>"<b>Total",
                             "Jumlah"=>$nTotalSaldo."</b>",);*/

            $font = 9 ;

            $vhead = array() ;
            $vhead[] = array("1"=>"Karu","2"=>" : ","3"=> $va['namakaru'],"4"=>"","5"=>"Periode","6"=>":","7"=>$va['tglawal'] ." sd ". $va['tglakhir']) ; 

            $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                          'opt'=>array('export_name'=>'hasilproduksikaru') ) ;
            $this->load->library('bospdf', $o) ;
            $this->bospdf->ezText("<b>HASIL PRODUKSI KARU</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vhead,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "1"=>array("width"=>10,"justification"=>"left"),
                                             "2"=>array("width"=>3,"justification"=>"left"),
                                             "3"=>array("justification"=>"left"),
                                             "4"=>array("justification"=>"left"),
                                             "5"=>array("width"=>10,"justification"=>"left"),
                                             "6"=>array("width"=>3,"justification"=>"left"),
                                             "7"=>array("justification"=>"left"))
                                        )
                                  ) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($data,"","",
                                   array("fontSize"=>$font,
                                         "cols"=> array(
                                             "No"           =>array("width"=>5,"justification"=>"right"),
                                             "Kode"         =>array("width"=>12,"justification"=>"center"),
                                             "Keterangan"   =>array("wrap"=>1),
                                             "Prod. Selesai" =>array("width"=>10,"justification"=>"right"),
                                             "Satuan"       =>array("width"=>12,"justification"=>"left")
                                         ))
                                  ) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "2"=>array("wrap"=>1,"justification"=>"center"),
                                          "3"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "4"=>array("wrap"=>1,"justification"=>"center"),
                                          "5"=>array("width"=>25,"wrap"=>1,"justification"=>"center"))
                                          )
                              ) ;
            $this->bospdf->ezStream() ;
        /*}else{
            echo('kosong') ;
        }*/
    }

    public function showreporttotal(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $file = getsession($this, $this->ss . "file") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        if(!empty($data)){
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> "" ,"5"=>$kota) ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"Mengetahui,","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"(.........................)","2"=>"","3"=>"(.........................)","4"=>"","5"=>"(.........................)") ;
            $vttd[] = array("1"=>"Manager","2"=>"","3"=>"Ka. Shiff","4"=>"","5"=>"Ka.Sie Produksi") ;

            $nTotalQtySTD      = 0 ;
            $nTotalSTD      = 0 ;
            $nTotalQtyAktual      = 0 ;
            $nTotalAktual      = 0 ;
            foreach ($data as $key => $value) {
                $nTotalQtySTD += string_2n($value['QTY STD']) ;
                $nTotalSTD += string_2n($value['N STD']) ;
                $nTotalQtyAktual += string_2n($value['QTY Aktual']) ;
                $nTotalAktual += string_2n($value['N Aktual']) ;

            }


            $total   = array();
            $total[] = array("Ket"=>"<b>Total",
                             "QTY STD"=>string_2s($nTotalQtySTD),
                             "N STD"=>string_2s($nTotalSTD),
                             "QTY Aktual"=>string_2s($nTotalQtyAktual),
                             "N Aktual"=>string_2s($nTotalAktual));

            $font = 8 ;
            $o    = array('paper'=>'A4', 'orientation'=>'P', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                          'opt'=>array('export_name'=>'Kartu Stock') ) ;
            $this->load->library('bospdf', $o) ;
            $this->bospdf->ezText("<b>LAPORAN TOTAL PERINTAH PRODUKSI</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("<b>Periode : " .$va['tglawal']. " s/d " . $va['tglakhir'] . "</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($data,"","",
                                   array("fontSize"=>$font,
                                         "cols"=> array(
                                             "No"=>array("width"=>5,"justification"=>"right"),
                                             "Faktur"=>array("wrap"=>1,"justification"=>"center"),
                                             "Faktur Prod"=>array("wrap"=>1,"justification"=>"center"),
                                             "Tgl"=>array("width"=>12,"wrap"=>1,"justification"=>"center"),
                                             "QTY STD"=>array("width"=>12,"justification"=>"right"),
                                             "N STD"=>array("width"=>12,"justification"=>"right"),
                                             "QTY Aktual"=>array("width"=>12,"justification"=>"right"),
                                             "N Aktual"=>array("width"=>12,"justification"=>"right")
                                         ))
                                  ) ;
            $this->bospdf->ezTable($total,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>1,
                                         "cols"=> array(
                                             "Ket"=>array("justification"=>"center"),
                                             "QTY STD"=>array("width"=>12,"justification"=>"right"),
                                             "N STD"=>array("width"=>12,"justification"=>"right"),
                                             "QTY Aktual"=>array("width"=>12,"justification"=>"right"),
                                             "N Aktual"=>array("width"=>12,"justification"=>"right")
                                         )
                                        )
                                  ) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "2"=>array("wrap"=>1,"justification"=>"center"),
                                          "3"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "4"=>array("wrap"=>1,"justification"=>"center"),
                                          "5"=>array("width"=>25,"wrap"=>1,"justification"=>"center"))
                                          )
                              ) ;
            $this->bospdf->ezStream() ;
        }else{
            echo('kosong') ;
        }
    }
    public function cetakdm(){
        $va 	= $this->input->post() ;
        //print_r($va);
		
		$faktur = $va['faktur'] ;
		

        $this->func_m->cetakhasilproduksi($faktur);
    }
}

?>
