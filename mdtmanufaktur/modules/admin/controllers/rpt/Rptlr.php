
<?php
class Rptlr extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model("rpt/rptlr_m") ;
        $this->load->model("func/perhitungan_m") ;
        $this->load->model("func/func_m") ;
        $this->bdb 	= $this->rptlr_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptlr_" ;
    }  

    public function index(){
        $this->load->view("rpt/rptlr") ; 

    }   

    public function loadgrid(){
        $this->duser();

        $va     = json_decode($this->input->post('request'), true) ;
        
        $data = array();
        $penihilan = "T";
        $cabang = array();
        if($va['penihilan'] == "1")$penihilan = "Y";

        //echo "Penihilan -> ".$penihilan;

        if($va['skd_cabang'] !== 'null'){
            $cabang = json_decode($va['skd_cabang'],true);
        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['cabang'])){
                    $cabang = $this->aruser['data_var']['cabang'];
                }
            }else{
                $dbd2 = $this->db->select("kode")->from("cabang")->get();
                
                foreach($dbd2->result_array() as $r2){
                    $cabang[] = $r2['kode'];
                }
            }
        }

        $rrlr = $this->perhitungan_m->getlr($va['tglawal'],$va['tglakhir'],$va['level'],$penihilan,$cabang);

        foreach($rrlr['records'] as $key => $val){
            $saldoakhirperiodinduk = "";
            $arrkd = explode(".",$val['kode']);
            $level = count($arrkd);
            if($val['jenis'] == "I" and $va['level'] > $level){
                $saldoakhirperiodinduk = $val['saldoakhirperiod'];
                $val['saldoakhirperiod'] = "";
            }
            unset($val['saldoawal']);
            unset($val['debet']);
            unset($val['kredit']);
            unset($val['saldoakhir']);
            unset($val['jenis']);

            $data[] = array("kode"=>$val['kode'],"keterangan"=>$val['keterangan'],
                            "saldoakhirperiod"=>$val['saldoakhirperiod'],"saldoakhirperiodinduk"=>$saldoakhirperiodinduk);
        }

        $vare 	= array("total"=>$rrlr['total'],"records"=>$data) ;
        echo(json_encode($vare)) ; 

    }

    public function initreport(){
        $this->duser();
        $n=0;
        $va     = $this->input->post() ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        $data = array();

        $penihilan = "T";
        $cabang = array();
        if(isset($va['penihilan']))$penihilan = "Y";

        //echo "Penihilan -> ".$penihilan;

        if($va['skd_cabang'] !== 'null'){
            $cabang = json_decode($va['skd_cabang'],true);
        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['cabang'])){
                    $cabang = $this->aruser['data_var']['cabang'];
                }
            }else{
                $dbd2 = $this->db->select("kode")->from("cabang")->get();
                
                foreach($dbd2->result_array() as $r2){
                    $cabang[] = $r2['kode'];
                }
            }
        }
        $rrlr = $this->perhitungan_m->getlr($va['tglawal'],$va['tglakhir'],$va['level'],$penihilan,$cabang);

        $n = 0 ;
        foreach($rrlr['records'] as $key => $val){
            $n++;
            $saldoakhirperiodinduk = "";
            $arrkd = explode(".",$val['kode']);
            $level = count($arrkd);
            if($val['jenis'] == "I" and $va['level'] > $level){
                $saldoakhirperiodinduk = $val['saldoakhirperiod']."</mdtlr01>";
                $val['saldoakhirperiod'] = "<mdtlr01>";
            }
            unset($val['saldoawal']);
            unset($val['debet']);
            unset($val['kredit']);
            unset($val['saldoakhir']);
            unset($val['jenis']);
            if($n == 1){
                $val['kode'] = "<mdtlr10>".$val['kode'];
                $saldoakhirperiodinduk = $saldoakhirperiodinduk."</mdtlr10>";
            }
            $data[] = array("kode"=>"<mdtls11>".$val['kode'],"keterangan"=>$val['keterangan'],
                            "saldoakhirperiod"=>$val['saldoakhirperiod'],"saldoakhirperiodinduk"=>$saldoakhirperiodinduk."</mdtls11>");
        }
        savesession($this, "rptlabarugi_rpt", json_encode($data)) ; 
        echo(' bos.rptlr.openreport() ; ') ;
    }

    public function showreport(){
        $this->duser();

        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $data = getsession($this,"rptlabarugi_rpt") ;      
        $data = json_decode($data,true) ;       
        if(!empty($data)){ 
            if(!isset($va['export']))$va['export'] = 0 ;
            $font = 8 ;
            $o    = array('paper'=>'LEGAL', 'orientation'=>'portrait', 'export'=>$va['export'],
                          'opt'=>array('export_name'=>'DaftarLabaRugi_' . getsession($this, "username") ) ) ;
            $this->load->library('bospdf', $o) ;
            if($va['skd_cabang'] !== 'null'){
                $cabang = json_decode($va['skd_cabang'],true);
            }else{
                if($this->aruser['level'] !== '0000'){
                    if(is_array($this->aruser['data_var']['cabang'])){
                        $cabang = $this->aruser['data_var']['cabang'];
                    }
                }else{
                    $dbd2 = $this->db->select("kode")->from("cabang")->get();
                    
                    foreach($dbd2->result_array() as $r2){
                        $cabang[] = $r2['kode'];
                    }
                }
            }

            $arrcab = $this->func_m->GetDataCabang($cabang[0]);
            
            if(count($cabang) == 1){
                $this->bospdf->ezText($arrcab['nama'],$font+4,array("justification"=>"center")) ;
            }else{
                $cabkonsol = "(".implode(",",$cabang).")";
                $this->bospdf->ezText($this->bdb->getconfig("app_company"),$font+4,array("justification"=>"center")) ;
                $this->bospdf->ezText("Konsolidasi : ".$cabkonsol,$font,array("justification"=>"center")) ;
            }

            // $this->bospdf->ezText($arrcab['kode'] ." - ".$arrcab['nama'],$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText($arrcab['alamat'] . " / ". $arrcab['telp'],$font,array("justification"=>"center")) ;

            $this->bospdf->ezText("<b>LABA RUGI</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("Untuk Periode Antara : ".$va['tglawal']." sd ".$va['tglakhir'],$font+2,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ; 
            $this->bospdf->ezTable($data,"","",  
                                   array("showHeadings"=>"","showLines"=>"0","fontSize"=>$font,"cols"=> array( 
                                       "kode"=>array("caption"=>"Tgl","width"=>10),
                                       "keterangan"=>array("caption"=>"Keterangan","wrap"=>1),
                                       "saldoawal"=>array("caption"=>"Saldo Awal","width"=>0,"justification"=>"right"),
                                       "debet"=>array("caption"=>"Debet","width"=>0,"justification"=>"right"),
                                       "kredit"=>array("caption"=>"Kredit","width"=>0,"justification"=>"right"),
                                       "saldoakhirperiod"=>array("caption"=>"Saldo Akhir","width"=>15,"justification"=>"right"),
                                       "saldoakhirperiodinduk"=>array("caption"=>"Saldo Akhir","width"=>15,"justification"=>"right")))) ;   
            //print_r($data) ;    
            $this->bospdf->ezStream() ; 
        }else{
            echo('data kosong') ;
        }
    }

}
?>
