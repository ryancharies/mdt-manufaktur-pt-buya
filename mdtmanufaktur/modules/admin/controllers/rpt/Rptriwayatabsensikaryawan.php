<?php
class Rptriwayatabsensikaryawan extends Bismillah_Controller{
   public function __construct(){
		parent::__construct() ;
      $this->load->model('rpt/rptriwayatabsensikaryawan_m') ;
      $this->load->model('func/perhitunganhrd_m') ;
      $this->load->helper('bdate') ;
      $this->load->helper('bdir') ;
	}

   public function index(){
      $tglmin = $this->rptriwayatabsensikaryawan_m->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
      $data = array();
      $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
      $this->load->view('rpt/rptriwayatabsensikaryawan',$data) ;
   }

   public function loadgrid(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
      $vdb    = $this->rptriwayatabsensikaryawan_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      $tgl = date("Y-m-d");
      
      while( $dbr = $this->rptriwayatabsensikaryawan_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['recid'] = $dbr['id'];
         $vaset['tgl'] = date_2d($vaset['tgl']);
         $golabsensi = $this->perhitunganhrd_m->getgolabsensi($dbr['kode'],$tgl);
         $vaset['golabsensi'] = $golabsensi['keterangan'];
         $vaset['cmdkaryawan']    = '<button type="button" onClick="bos.rptriwayatabsensikaryawan.cmdkaryawan(\''.$dbr['kode'].'\')"
                           class="btn btn-success btn-grid">Rwyt Absensi</button>' ;
         $vaset['cmdkaryawan']	   = html_entity_decode($vaset['cmdkaryawan']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

  

   public function init(){
      savesession($this, "ssgolongan_kode", "") ;
   }

   public function karyawan(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->rptriwayatabsensikaryawan_m->getdata($kode) ;
      if(!empty($data)){
         echo('
            with(bos.rptriwayatabsensikaryawan.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$data['kode'].'") ;
               find("#nama").val("'.$data['nama'].'") ;
               find("#ktp").val("'.$data['ktp'].'") ;
               find("#notelepon").val("'.$data['telepon'].'") ;
               find("#tglmasuk").val("'.date_2s($data['tgl']).'") ;
               find("#alamat").val("'.$data['alamat'].'") ;
            }
            bos.rptriwayatabsensikaryawan.settab(1) ;

         ');

      }
   }
    
   public function seekperiode(){
      $search     = $this->input->get('q');
      $vdb    = $this->rptriwayatabsensikaryawan_m->seekperiode($search) ;
      $dbd    = $vdb['db'] ;
      $vare   = array();
      while( $dbr = $this->rptriwayatabsensikaryawan_m->getrow($dbd) ){
          $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ". $dbr['keterangan']) ;
      }
      $Result = json_encode($vare);
          echo($Result) ;
   }

   public function seekkodeabsensim(){
      $search     = $this->input->get('q');
      $vdb    = $this->rptriwayatabsensikaryawan_m->seekkodeabsensim($search) ;
      $dbd    = $vdb['db'] ;
      $vare   = array();
      while( $dbr = $this->rptriwayatabsensikaryawan_m->getrow($dbd) ){
          $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      $Result = json_encode($vare);
          echo($Result) ;
   }

   public function seekkodeabsensip(){
      $search     = $this->input->get('q');
      $vdb    = $this->rptriwayatabsensikaryawan_m->seekkodeabsensip($search) ;
      $dbd    = $vdb['db'] ;
      $vare   = array();
      while( $dbr = $this->rptriwayatabsensikaryawan_m->getrow($dbd) ){
          $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      $Result = json_encode($vare);
          echo($Result) ;
   }

   public function loadabsensi(){
         $va 	= $this->input->post() ;
         $vdb    = $this->rptriwayatabsensikaryawan_m->getperiode($va) ;
         if( $dbr = $this->rptriwayatabsensikaryawan_m->getrow($vdb['db']) ){
            echo('
               with(bos.rptriwayatabsensikaryawan.obj){
                     find("#tglawal").val("'.date_2d($dbr['tglawal']).'") ;
                     find("#tglakhir").val("'.date_2d($dbr['tglakhir']).'") ;
               }
            ') ;
            
            $array = array();
            $n = 0 ;
            for($tgl = $dbr['tglawal'] ; $tgl <= $dbr['tglakhir'] ; $tgl = date("Y-m-d",strtotime($tgl)+(24*60*60))){
               $n++;
               $jw = $this->perhitunganhrd_m->getjwabsensi($va['nip'],$tgl);
               $hari = date('w', strtotime($tgl));
               $hari = date_day($hari);
               
               $abs = $this->perhitunganhrd_m->getabsensi($va['nip'],$tgl);
               $lembur = $this->perhitunganhrd_m->getlemburdetail($va['nip'],$tgl);
               $array[] = array("recid"=>$n,"no"=>$n,"hari"=>$hari,"tgl"=>date_2d($tgl),"jadwalmasuk"=>$jw['jwmasuk'],"jadwalpulang"=>$jw['jwpulang'],
                           "toleransi"=>$jw['jwtoleransi'],"absmasuk"=>$abs['wktmasuk'],"abspulang"=>$abs['wktpulang'],
                           "kodeabsensim"=>$abs['ketkodeabsenmasuk'],"kodeabsensip"=>$abs['ketkodeabsenpulang'],"terlambat"=>$abs['terlambat'],
                           "plgcepat"=>$abs['plgcepat'],
                           "keterangan"=>"","golabsensi"=>$jw['ketgolongan'],"jadwal"=>$jw['ketjadwal'],
                           "lembur"=>$lembur['totlemburh']." Jam");
               
            }
            $array = json_encode($array);

            echo ('
                w2ui["bos-form-rptriwayatabsensikaryawan_grid2"].add(' . $array . ');
              ');
         }
   }

   public function preview(){
         $va 	= $this->input->get() ;
         $vdb    = $this->rptriwayatabsensikaryawan_m->getperiode($va) ;
         if( $dbr = $this->rptriwayatabsensikaryawan_m->getrow($vdb['db']) ){
            
            $array = array();
            $n = 0 ;
            for($tgl = $dbr['tglawal'] ; $tgl <= $dbr['tglakhir'] ; $tgl = date("Y-m-d",strtotime($tgl)+(24*60*60))){
               $n++;
               $jw = $this->perhitunganhrd_m->getjwabsensi($va['kode'],$tgl);
               $hari = date('w', strtotime($tgl));
               $hari = date_day($hari);
               
               $abs = $this->perhitunganhrd_m->getabsensi($va['kode'],$tgl);
               $lembur = $this->perhitunganhrd_m->getlemburdetail($va['kode'],$tgl);
               $array[] = array("no"=>$n,"hari"=>$hari,"tgl"=>date_2d($tgl),"jadwalmasuk"=>$jw['jwmasuk'],"jadwalpulang"=>$jw['jwpulang'],
                           "toleransi"=>$jw['jwtoleransi'],"absmasuk"=>$abs['wktmasuk'],"abspulang"=>$abs['wktpulang'],
                           "kodeabsensim"=>$abs['ketkodeabsenmasuk'],"kodeabsensip"=>$abs['ketkodeabsenpulang'],"terlambat"=>$abs['terlambat'],
                           "plgcepat"=>$abs['plgcepat'],
                           "keterangan"=>"","golabsensi"=>$jw['ketgolongan'],"jadwal"=>$jw['ketjadwal'],
                           "lembur"=>$lembur['totlemburh']." Jam");
            }
            
            if(!empty($array)){
               $font = 9 ;
            

               $vHead = array() ;
               $vHead[] = array("1"=>"NIP","2"=>":","3"=>$va['kode'],"4"=>"","5"=>"NoTelp","6"=>":","7"=>$va['notelepon']);
               $vHead[] = array("1"=>"Nama","2"=>":","3"=>$va['nama'],"4"=>"","5"=>"Alamat","6"=>":","7"=>$va['alamat']);
         


               $o    = array('paper'=>'A4', 'orientation'=>'L', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                           'opt'=>array('export_name'=>'riwayatabsensi','mtop'=>1) ) ;
               $this->load->library('bospdf', $o) ;
               $this->bospdf->ezSetMargins(1,1,20,20);
               $this->bospdf->ezSetCmMargins(0, 1, 1, 1);
               //$this->bospdf->ezImage("./uploads/HeaderSJDO.jpg",true,'60','190','50');
               //$this->bospdf->ezLogoHeaderPage("./uploads/HeaderSJDO.jpg",'0','65','150','50');
               $this->bospdf->ezHeader("<b>Riwayat Absensi Karyawan</b>",array("justification"=>"center","fontSize"=>$font+4)) ;
               $this->bospdf->ezText("") ;

               //ketika width tidak di set maka akan menyesuaikan dengan lebar kertas

               $this->bospdf->ezTable($vHead,"","",
                                    array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                          "cols"=> array(
                                                "1"=>array("width"=>15,"justification"=>"left"),
                                                "2"=>array("width"=>5,"justification"=>"left"),
                                                "3"=>array("justification"=>"left"),
                                                "4"=>array("width"=>10,"justification"=>"left"),
                                                "5"=>array("width"=>15,"justification"=>"left"),
                                                "6"=>array("width"=>5,"justification"=>"left","wrap"=>1),
                                                "7"=>array("justification"=>"left","wrap"=>1))
                                          )
                                    ) ;

               $this->bospdf->ezText("") ;
               $this->bospdf->ezTable($array,"","",
                                    array("fontSize"=>$font-1,"showLines"=>1,
                                          "cols"=> array(
                                                "no"        =>array("caption"=>"No","width"=>3,"justification"=>"right"),
                                                "nip"       =>array("caption"=>"NIP","width"=>7,"justification"=>"center"),
                                                "nama"      =>array("caption"=>"Nama","wrap"=>1),                                             
                                                "jabatan"   =>array("caption"=>"Jabatan","width"=>10),                                             
                                                "bagian"    =>array("caption"=>"Bagian","width"=>10),                                             
                                                "nopendaftaran" =>array("caption"=>"No Pendaftaran","width"=>10,"justification"=>"center"),                                             
                                                "noasuransi"    =>array("caption"=>"No Asuransi","width"=>10,"justification"=>"center"),                                             
                                                "produk"        =>array("caption"=>"Produk","width"=>10),                                             
                                                "tarifkaryawan"    =>array("caption"=>"Karyawan","width"=>10,"justification"=>"right"),                                             
                                                "tarifperusahaan"  =>array("caption"=>"Perusahaan","width"=>10,"justification"=>"right"),                                             
                                                "total"            =>array("caption"=>"Total","width"=>10,"justification"=>"right"),                                             
                                                ))
                                    ) ;
               $this->bospdf->ezStream() ;
            }

         }else{
            echo('
               alert("Data tidak valid !!!");
            ');
         }
   }
}
?>
