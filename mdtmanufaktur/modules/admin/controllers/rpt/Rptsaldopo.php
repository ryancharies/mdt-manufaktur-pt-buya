<?php

class Rptsaldopo extends Bismillah_Controller{
    protected $bdb ;
    protected $ss ;
    protected $abc ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper('bdate');
        $this->load->model('rpt/rptsaldopo_m') ;
        $this->load->model('func/Perhitungan_m') ;
        $this->load->model('func/func_m') ;
        $this->load->library('escpos');
        $this->bdb = $this->rptsaldopo_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptsaldopo_" ;
    }

    public function index(){
        $d    = array("setdate"=>date_set()) ;
        $this->load->view('rpt/rptsaldopo', $d) ;
    }

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $dbd    = $this->rptsaldopo_m->loadgrid($va) ;
        $n = 0 ;
        while( $dbr = $this->rptsaldopo_m->getrow($dbd) ){            
            if(!isset($vaset[$dbr['faktur']])){
                $n++;
                $vaset[$dbr['faktur']] = array("no"=>$n,"faktur"=>$dbr['faktur'],"fktpr"=>$dbr['fktpr'],
                            "supplier"=>$dbr['supplier'],"tglpo"=>date_2d($dbr['tgl']),"qty"=>0,"nominal"=>0,"cmdPreview"=>"");
            }
            $vaset[$dbr['faktur']]['qty'] += $dbr['saldo'];
            $vaset[$dbr['faktur']]['nominal'] += ($dbr['saldo'] * $dbr['hp']); 
            
        }
        $data = array();
        $totqty = 0 ;
        $totnom = 0 ;
        foreach($vaset as $key => $val){
            $val['cmdPreview']    = '<button type="button" onClick="bos.rptsaldopo.cmdpreviewdetail(\''.$val['faktur'].'\',\''.$va['tglakhir'].'\')"
                                     class="btn btn-success btn-grid">Preview Detail</button>' ;
            $val['cmdPreview']    = html_entity_decode($val['cmdPreview']) ;
            $data[] = $val;
            $totqty += $val['qty'];
            $totnom += $val['nominal'];
        }
        $data[] = array("recid"=>'ZZZZ',"no"=>"","faktur"=>"","fktpr"=>"",
                            "supplier"=>"<b>Total</b>","tglpo"=>"","qty"=>$totqty,"nominal"=>$totnom,
                            "cmdPreview"=>"","w2ui"=>array("summary"=> true));

        $data    = array("total"=>count($data) - 1, "records"=>$data) ;
        echo(json_encode($data)) ;
    }

    public function PreviewDetail(){
        $va    = $this->input->post() ;
        $cFaktur = $va['faktur'] ;
        $tgl = $va['tgl'] ;
        echo('w2ui["bos-form-rptsaldopo_grid2"].clear();');
        $data = $this->rptsaldopo_m->GetDataPerFaktur($cFaktur) ;
        if(!empty($data)){
            echo('
                  with(bos.rptsaldopo.obj){
                     find("#faktur").val("'.$data['faktur'].'") ;
                     find("#fktpr").val("'.$data['fktpr'].'") ;
                     find("#terminhari").val("'.$data['terminhari'].'") ;
                     find("#namabank").val("'.$data['namabank'].'") ;
                     find("#rekening").val("'.$data['rekening'].'") ;
                     find("#atasnamarekening").val("'.$data['atasnamarekening'].'") ;
                     find("#notelepon").val("'.$data['notelepon'].'") ;
                     find("#kontakpersonal").val("'.$data['kontakpersonal'].'") ;
                     find("#supplier").val("'.$data['supplier'].'") ;
                     find("#subtotal").val("'.string_2s($data['total']).'") ;
                     find("#total").val("'.string_2s($data['total']).'") ;
                     find("#tgl").val("'.$data['Tgl'].'") ;
                  }

              ') ;
            $data = $this->rptsaldopo_m->getDetailPO($cFaktur,$tgl) ;
            $vare = array();
            $n = 0 ;
            while($dbr = $this->rptsaldopo_m->getrow($data)){
                $n++;
                $vaset          = $dbr ;
                $vaset['qtysisa'] = $dbr['qtysisa'];
                $vaset['nomsisa'] = $dbr['qtysisa'] * $dbr['hp'];
                $vaset['recid'] = $n;
                $vaset['no']    = $n;
                $vare[]         = $vaset ;
            }

            $vare = json_encode($vare);
            echo('
            bos.rptsaldopo.loadmodalpreview("show") ;
            bos.rptsaldopo.grid2_reloaddata();
            w2ui["bos-form-rptsaldopo_grid2"].add('.$vare.');
         ');
        }
    }

    public function initreport(){
        $va      = $this->input->post() ;
        $cFaktur  = $va['cFaktur'];
        $w    = "p.faktur = '".$cFaktur."'" ;

        $file   = setfile($this, "rpt", __FILE__ , $va) ;
        savesession($this, $this->ss . "file", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        file_put_contents($file, json_encode(array()) ) ;

        $file    = getsession($this, $this->ss . "file") ;
        $data    = @file_get_contents($file) ;
        $data    = json_decode($data,true) ;
        $db      = $this->bdb->getDetailPO($cFaktur,$va['tglakhir']) ;
        $nDebet  = 0 ;
        $nKredit = 0 ;
        $nSaldo  = 0 ;
        $s = 0 ;
        while($dbr = $this->bdb->getrow($db)){
            $no      = ++$s ;
            $saldonom = $dbr['qtysisa'] * $dbr['hp'];
            $data[]  = array("No"=>$no,
                             "Kode"=>$dbr['kode'],
                             "Keterangan"=>$dbr['keterangan'],
                             "Spesifikasi"=>$dbr['spesifikasi'],
                             "Satuan"=>$dbr['satuan'],
                             "Harga"=>string_2s($dbr['harga']),
                             "Qty"=>string_2s($dbr['qty']),                             
                             "Jumlah"=>string_2s($dbr['jumlah']),
                             "Saldo Qty"=>string_2s($dbr['qtysisa']),
                             "Saldo Nominal"=>string_2s($saldonom)
                            ) ;
        }
        file_put_contents($file, json_encode($data) ) ;
        echo(' bos.rptsaldopo.openreport() ; ') ;
    }

    public function initreportTotal(){
        $va         = $this->input->post() ;
     //print_r($va);
        $file       = setfile($this, "rpt_saldopototal", __FILE__ , $va) ;
        savesession($this, $this->ss . "file", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        file_put_contents($file, json_encode(array()) ) ;

        $file    = getsession($this, $this->ss . "file") ;
        $data    = @file_get_contents($file) ;
        $data    = json_decode($data,true) ;
        $db      = $this->bdb->getTotalPO($va) ;
        $n       = 0 ;
        while ($dbr = $this->bdb->getrow($db)) {
           // print_r($dbr);
            if(!isset($vaset[$dbr['faktur']])){
                $n++;
                $data[$dbr['faktur']] = array("no"=>$n,"faktur"=>$dbr['faktur'],"fktpr"=>$dbr['fktpr'],
                            "supplier"=>$dbr['supplier'],"tglpo"=>date_2d($dbr['tgl']),"qty"=>0,"nominal"=>0);
            }
            $data[$dbr['faktur']]['qty'] += $dbr['saldo'];
            $data[$dbr['faktur']]['nominal'] += ($dbr['saldo'] * $dbr['hp']); 
        }
       // print_r($data);
        file_put_contents($file, json_encode($data) ) ;
        echo(' bos.rptsaldopo.openreporttotal() ; ') ;

    }

    public function showreport(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $file = getsession($this, $this->ss . "file") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        if(!empty($data)){
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
            
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> "" ,"5"=>$kota) ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"Mengetahui,","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"(.........................)","2"=>"","3"=>"(.........................)","4"=>"","5"=>"(.........................)") ;
            $vttd[] = array("1"=>"Manager","2"=>"","3"=>"Bag.Pembelian","4"=>"","5"=>"Bag. Gudang") ;


            $totjumlah  = 0 ;
            $totqty  = 0 ;
            $totqtysisa  = 0 ;
            $totnomsisa  = 0 ;
            foreach ($data as $key => $value) {
                $totjumlah  += string_2n($value['Jumlah']) ;
                $totqty  += string_2n($value['Qty']) ;
                $totqtysisa  += string_2n($value['Saldo Qty']) ;
                $totnomsisa  += string_2n($value['Saldo Nominal']) ;
                
            }

            $total   = array();
            $total[] = array("Ket"=>"<b>Total",
                             "Qty"          => string_2s($totqty),
                             "Jumlah"        => string_2s($totjumlah),
                             "Saldo Qty"     => string_2s($totqtysisa),
                             "Saldo Nominal" => string_2s($totnomsisa)."</b>");
                            
            $font = 8 ;
            $exTgl = explode("-",$va['tgl']);
            $dTglFaktur = $exTgl['2'] . "-" . $exTgl['1'] . "-" . $exTgl['0'];

            $vDetail = array() ;
            $vDetail[] = array("1"=>"","2"=>"","3"=>"","4"=>"Faktur","5"=>":","6"=>$va['cFaktur']) ;
            $vDetail[] = array("1"=>"","2"=>"","3"=>"","4"=>"Supplier","5"=>":","6"=>$va['supplier']) ;
            $vDetail[] = array("1"=>"","2"=>"","3"=>"","4"=>"Tanggal","5"=>":","6"=>$dTglFaktur) ;

            $vTitle = array() ;
            $vTitle[] = array("capt"=>" PURCHASE ORDER (PO) ") ;

            

            $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                          'opt'=>array('export_name'=>'Kartu Stock','mtop'=>1) ) ;
            $this->load->library('bospdf', $o) ;
            $this->bospdf->ezLogoHeaderPage("./uploads/HeaderSJDO.jpg",'0','65','150','50');
            $this->bospdf->ezTable($vDetail,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "1"=>array("width"=>10,"justification"=>"left"),
                                             "2"=>array("width"=>3,"justification"=>"left"),
                                             "3"=>array("justification"=>"left"),
                                             "4"=>array("width"=>10,"justification"=>"left"),
                                             "5"=>array("width"=>3,"justification"=>"left"),
                                             "6"=>array("width"=>20,"justification"=>"left"),)
                                        )
                                  ) ;

            $this->bospdf->ezTable($vTitle,"","",
                                   array("fontSize"=>$font+3,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "capt"=>array("justification"=>"center"),)
                                        )
                                  ) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($data,"","",
                                   array("fontSize"=>$font,
                                         "cols"=> array(
                                             "No"            =>array("width"=>3,"justification"=>"right"),
                                             "Kode"  =>array("width"=>12,"justification"=>"center"),
                                             "Keterangan"   =>array("wrap"=>1),
                                             "Spesifikasi"   =>array("wrap"=>1),
                                             "Satuan"       =>array("caption"=>"Sat","width"=>5,"justification"=>"left"),
                                             "Harga"  =>array("width"=>12,"justification"=>"right"),
                                             "Qty"          =>array("width"=>8,"justification"=>"right"),
                                             "Jumlah"        =>array("width"=>12,"justification"=>"right"),
                                             "Saldo Qty"          =>array("width"=>8,"justification"=>"right"),
                                             "Saldo Nominal"        =>array("width"=>12,"justification"=>"right")))
                                  ) ;

            $this->bospdf->ezTable($total,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>1,
                                         "cols"=> array(
                                             "Ket"=>array("justification"=>"center"),
                                             "Qty"          =>array("width"=>8,"justification"=>"right"),
                                             "Jumlah"        =>array("width"=>12,"justification"=>"right"),
                                             "Saldo Qty"          =>array("width"=>8,"justification"=>"right"),
                                             "Saldo Nominal"        =>array("width"=>12,"justification"=>"right")
                                         )
                                        )
                                  ) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "2"=>array("wrap"=>1,"justification"=>"center"),
                                          "3"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "4"=>array("wrap"=>1,"justification"=>"center"),
                                          "5"=>array("width"=>25,"wrap"=>1,"justification"=>"center"))
                                          )
                              ) ;
             
            $this->bospdf->ezStream() ;
        }else{
            echo('kosong') ;
        }
    }

    public function showreporttotal(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $file = getsession($this, $this->ss . "file") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        if(!empty($data)){
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> "" ,"5"=>$kota) ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"Mengetahui,","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"(Manager)","2"=>"","3"=>"(Bag.Pembelian)","4"=>"","5"=>" (Bag. Gudang)") ;

            $ntotqty      = 0 ;
            $ntotnominal      = 0 ;
            $n = 0 ;
            foreach ($data as $key => $value) {
                $n++;
                $ntotqty  += string_2n($value['qty']);
                $ntotnominal  += string_2n($value['nominal']);
                $data[$key]['no'] = $n;
                $data[$key]['qty'] = string_2s($value['qty']);
                $data[$key]['nominal'] = string_2s($value['nominal']);
            }
            


            $total   = array();
            $total[] = array("Ket"=>"<b>Total",
                             "qty"=>string_2s($ntotqty),
                             "nominal"=>string_2s($ntotnominal)."</b>");

            $font = 8 ;
            $o    = array('paper'=>'A4', 'orientation'=>'P', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                          'opt'=>array('export_name'=>'Kartu Stock') ) ;
            $this->load->library('bospdf', $o) ;
            $this->bospdf->ezText("<b>LAPORAN TOTAL PURCHASE ORDER</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("<b>Periode : " .$va['tglakhir'] . "</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($data,"","",
                                   array("fontSize"=>$font,
                                         "cols"=> array(
                                             "no"=>array("caption"=>"No","width"=>4,"justification"=>"right"),
                                             "faktur"=>array("caption"=>"Faktur","width"=>15,"wrap"=>1,"justification"=>"center"),
                                             "fktpr"=>array("caption"=>"Fkt PR ","width"=>15,"wrap"=>1,"justification"=>"center"),
                                             "supplier"=>array("caption"=>"Supplier","wrap"=>1),
                                             "tglpo"=>array("caption"=>"Tgl PO","width"=>10,"wrap"=>1,"justification"=>"center"),
                                             "qty"=>array("caption"=>"S. Qty","width"=>13,"justification"=>"right"),
                                             "nominal"=>array("caption"=>"S. Nominal","width"=>13,"justification"=>"right")
                                         ))
                                  ) ;

            $this->bospdf->ezTable($total,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>1,
                                         "cols"=> array(
                                             "Ket"=>array("justification"=>"center"),
                                             "qty"=>array("caption"=>"S. Qty","width"=>13,"justification"=>"right"),
                                             "nominal"=>array("caption"=>"S. Nominal","width"=>13,"justification"=>"right")
                                         )
                                        )
                                  ) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "2"=>array("wrap"=>1,"justification"=>"center"),
                                          "3"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "4"=>array("wrap"=>1,"justification"=>"center"),
                                          "5"=>array("width"=>25,"wrap"=>1,"justification"=>"center"))
                                 )
                              ) ;
            $this->bospdf->ezStream() ;
        }else{
            echo('kosong') ;
        }
    }
	
	public function cetakdm(){
        $va 	= $this->input->post() ;
        //print_r($va);
		
		$faktur = $va['faktur'] ;
		

        $this->func_m->cetakpo($faktur);
    }

    public function cetaklpbdm(){
        $va 	= $this->input->post() ;
        //print_r($va);
		
		$faktur = $va['faktur'] ;
		

        $this->func_m->cetaklpbpo($faktur);
    }
}

?>
