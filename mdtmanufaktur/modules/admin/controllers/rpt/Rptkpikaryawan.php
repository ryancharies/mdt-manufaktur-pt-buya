<?php
class Rptkpikaryawan extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("rpt/rptkpikaryawan_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->load->model("func/func_m") ;
        
        $this->bdb 	= $this->rptkpikaryawan_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptkpikaryawan2_" ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";

        $this->load->view("rpt/rptkpikaryawan",$data) ;

    } 

   
    public function loadgrid(){
        $va = json_decode($this->input->post('request'), true);
        //print_r($va);
        $vare   = array() ;
        $vdb    = $this->rptkpikaryawan_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;
        
        while( $dbr = $this->rptkpikaryawan_m->getrow($dbd) ){
            $n++;

            //jab bag
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$va['tglakhir']);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['kode'],$va['tglakhir']);
            $dbr['bagian'] = $bagian['keterangan'];
            $dbr['jabatan'] = $jabatan['keterangan'];

            $vaset = $dbr;            
            $vaset['cetak'] = "Tidak";
            $vaset['recid'] = $n;
            $vare[] = $vaset;
            
        }

        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function printkpikaryawan(){
        $va     = $this->input->post() ;
        //print_r($va);
        $vaGrid = json_decode($va['grid1']);
        
        $kpiindikator = array();
        $dkpi = $this->rptkpikaryawan_m->datakpi();
        while($dbr = $this->rptkpikaryawan_m->getrow($dkpi)){
            $kpiindikator[$dbr['kode']] = array("kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan'],"perhitungan"=>$dbr['perhitungan'],"satuan"=>$dbr['satuan']);
        }
        $arrdata = array();
       //print_r($vaGrid);
        foreach($vaGrid as $key => $val){
            //echo($val->cetakslip);
            if($val->cetak == "Ya"){
                //load data karaywan
                $karywn   = $this->perhitunganhrd_m->getdatakaryawan($val->kode,$va['tglakhir']) ;
                //echo($val->kode);
                //$dbd    = $vdb['db'] ;
                $n = 0 ;
                $datakpi = array();
                $arrdata[$val->kode] = array("kode"=>$val->kode,"nama"=>$karywn['nama'],"alamat"=>$karywn['alamat'],"ketjabatan"=>$karywn['ketjabatan'],
                                       "ketbagian"=>$karywn['ketbagian'],"datakpi"=>array());
                
                foreach($kpiindikator as $key => $value){
                    $bobot = $this->perhitunganhrd_m->getbobotkpiindikator($value['kode'],$karywn['kodejabatan'],$karywn['kodebagian'],$va['tglakhir']);
                    if($bobot > 0){
                        $kpi = $this->perhitunganhrd_m->getkpikaryawan($val->kode,$va['tglawal'],$va['tglakhir'],$value['perhitungan']);
                        $target = $kpi['target'] ;
                        $pencapaian = min($kpi['target'],$kpi['pencapaian']); 
                        $nilai = devide($kpi['pencapaian'],$kpi['target']) * $bobot ;
                        $datakpi[$key] = array("kode"=>$value['kode'],"keterangan"=>$value['keterangan'],"satuan"=>$value['satuan'],"target"=>$target,
                                    "bobot"=>$bobot,"pencapaian"=>$pencapaian,"nilai"=>$nilai);
                    }
                }

                $arrdata[$val->kode]['datakpi'] = $datakpi;

            }
        }
        $data = htmlspecialchars(json_encode($arrdata));
        
        echo('
            bos.rptkpikaryawan.openreportkpi("'.$data.'");
        ');
    }

    public function showreporkpikaryawan(){
        $va     = $this->input->get() ;
        $arrdata = json_decode($va['data']);
        $data = getsession($this,"rptkpikaryawan_rpt") ;      
        $data = json_decode($data,true) ;

        $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>0,
                          'opt'=>array('export_name'=>'rptkpikaryawan_' . getsession($this, "username") ) ) ;
        $this->load->library('bospdf', $o) ; 


        $font = 8;
        $row = 0 ;
        foreach($arrdata as $key => $val){
            $row++;
            $header = array();
            $header[] = array("1"=>"NIP","2"=>":","3"=>$val->kode,"4"=>"","5"=>"Jabatan","6"=>":","7"=>$val->ketjabatan);
            $header[] = array("1"=>"Nama","2"=>":","3"=>$val->nama,"4"=>"","5"=>"Bagian","6"=>":","7"=>$val->ketbagian);

            $body = array();
            $n = 0 ;
            foreach($val->datakpi as $key2 => $val2){
                $n++;
                $body[] = array("No"=>$n,"Indikator"=>$val2->keterangan,"Target"=>$val2->target." ".$val2->satuan,
                                "Pencapaian"=>$val2->pencapaian." ".$val2->satuan,"Bobot"=>$val2->bobot." %","Nilai"=>$val2->nilai);
            }

            if($row>1)$this->bospdf->ezNewPage(0) ; 
            //report
            $this->bospdf->ezText("<b>KPI Karyawan</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("Periode : ".$va['tglawal'] . " sd " . $va['tglakhir'],$font+1,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ;

            $this->bospdf->ezTable($header,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("width"=>15,"justification"=>"left"),
                                          "2"=>array("width"=>5,"justification"=>"center"),
                                          "3"=>array("justification"=>"left"),
                                          "4"=>array("width"=>10,"justification"=>"center"),
                                          "5"=>array("width"=>15,"justification"=>"left"),
                                          "6"=>array("width"=>5,"justification"=>"center"),
                                          "7"=>array("justification"=>"left"))
                                 )
                              ) ;
            
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($body,"","",
                                 array("fontSize"=>$font,"showHeadings"=>1,"showLines"=>2,
                                       "cols"=> array(
                                          "No"=>array("width"=>5,"justification"=>"right"),
                                          "Indikator"=>array("justification"=>"left"),
                                          "Target"=>array("width"=>20,"justification"=>"right"),
                                          "Pencapaian"=>array("width"=>20,"justification"=>"right"),
                                          "Bobot"=>array("width"=>10,"justification"=>"center"),
                                          "Nilai"=>array("width"=>20,"justification"=>"right"))
                                 )
                              ) ;
                              
        }
        $this->bospdf->ezStream() ; 

    }
}
?>
