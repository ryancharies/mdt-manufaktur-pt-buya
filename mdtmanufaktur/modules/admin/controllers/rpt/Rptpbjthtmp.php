<?php

class Rptpbjthtmp extends Bismillah_Controller
{
    protected $bdb;
    protected $ss;
    protected $abc;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('bdate');
        $this->load->model('rpt/rptpbjthtmp_m');
        $this->load->model('func/Perhitungan_m');
        $this->bdb = $this->rptpbjthtmp_m;
        $this->ss = "ssrptpbjthtmp_";
    }

    public function index()
    {
        $d = array("setdate" => date_set());
        $this->load->view('rpt/rptpbjthtmp', $d);
    }

    public function loadgrid()
    {
        $va = json_decode($this->input->post('request'), true);
        $vare = array();
        $va['tglakhir'] = date_2s($va['tglAkhir']);
        $va['tglawal'] = date_2s($va['tglAwal']);
        $vdb = $this->rptpbjthtmp_m->loadgrid($va);
        $dbd = $vdb['db'];
        $total = 0;
        $pelunasan = 0;
        $saldo = 0;
        while ($dbr = $this->rptpbjthtmp_m->getrow($dbd)) {
            $vaset = $dbr;
            $key = $dbr['supplier'];
            $key2 = $vaset['jthtmp'] ."-". $dbr['id'];
            //$vaset['pelunasan'] = $vaset['total'] - $vaset['saldo'];
            $total += $vaset['total'];
            //$pelunasan += $vaset['pelunasan'] ;
            // $saldo += $vaset['saldo'];
            $vaset['tgl'] = date_2d($vaset['tgl']);
            $vaset['jthtmp'] = date_2d($vaset['jthtmp']);
            $vaset['jenis'] = "Pembelian";
            // $vaset['cmdPreview']    = '<button type="button" onClick="bos.rptpbjthtmp.cmdpreviewdetail(\''.$dbr['faktur'].'\')"
            //                          class="btn btn-success btn-grid">Detail Pembelian</button>' ;

            // $vaset['cmdPreview']    = html_entity_decode($vaset['cmdPreview']) ;
            $vare[$key][$key2] = $vaset;
        }

        //pembelian aset
        $vdb = $this->rptpbjthtmp_m->loadhutpembelianaset($va);
        $dbd = $vdb['db'];
        
        while ($dbr = $this->rptpbjthtmp_m->getrow($dbd)) {
            $vaset = $dbr;
            $key = $dbr['supplier'];
            $key2 = $vaset['jthtmp'] ."-". $dbr['id'];
            //$vaset['pelunasan'] = $vaset['total'] - $vaset['saldo'];
            $total += $vaset['total'];
            //$pelunasan += $vaset['pelunasan'] ;
            //$saldo += $vaset['saldo'];
            $vaset['tgl'] = date_2d($vaset['tgl']);
            $vaset['jthtmp'] = date_2d($vaset['jthtmp']);
            $vaset['jenis'] = "Aset";
            // $vaset['cmdPreview']    = '<button type="button" onClick="bos.rptpbjthtmp.cmdpreviewdetailaset(\''.$dbr['kode'].'\')"
            // class="btn btn-success btn-grid">Detail Pembelian</button>' ;
            // $vaset['cmdPreview']    = html_entity_decode($vaset['cmdPreview']) ;
            $vare[$key][$key2] = $vaset;
        }

        ksort($vare);
        $data = array();
        $n = 0 ;
        foreach ($vare as $key => $arrval) {
            $data[] = array("faktur" => "", "supplier" => "<b>".$key."</b>", "tgl" => "", "jthtmp" => "",
                "total" => "");
            $subtotal = 0;
            foreach ($arrval as $key2 => $val) {

                $cmdPreview    = '<button type="button" onClick="bos.rptpbjthtmp.cmdpreviewdetail(\''.$val['faktur'].'\')"
                                      class="btn btn-success btn-grid">Detail Pembelian</button>' ;
                if($val['jenis'] == "Aset"){
                    $cmdPreview = '<button type="button" onClick="bos.rptpbjthtmp.cmdpreviewdetailaset(\''.$val['kode'].'\')"
                     class="btn btn-success btn-grid">Detail Pembelian</button>' ;
                }
                $n++;
                $data[] = array("no"=>$n,"faktur" => $val['faktur'], "supplier" => $val['supplier'], "tgl" => $val['tgl'], "jthtmp" => $val['jthtmp'],
                    "total" => $val['total'],"cmdPreview"=>html_entity_decode($cmdPreview));
                $subtotal += $val['total'];

            }
            $data[] = array("faktur" => "", "supplier" => "<b>Sub Total</b>", "tgl" => "", "jthtmp" => "",
                "total" => $subtotal);
                $data[] = array("faktur" => "", "supplier" => "", "tgl" => "", "jthtmp" => "",
                "total" => "");
        }

        $data[] = array("recid" => "zzzz", "faktur" => "", "supplier" => "", "tgl" => "", "jthtmp" => "",
            "total" => $total, "w2ui" => array("summary" => true));

        $vare = array("total" => count($data) - 1, "records" => $data);
        echo (json_encode($vare));
    }

    public function PreviewDetailPembelianaset()
    {
        $va = $this->input->post();
        $kode = $va['kode'];
        $data = $this->rptpbjthtmp_m->getdatapembelianaset($kode);
        if ($dbr = $this->rptpbjthtmp_m->getrow($data)) {
            $golaset[] = array("id" => $dbr['golongan'], "text" => $dbr['golongan'] . " - " . $dbr['ketgolongan']);
            $cabang[] = array("id" => $dbr['cabang'], "text" => $dbr['cabang'] . " - " . $dbr['ketcabang']);
            $vendor[] = array("id" => $dbr['vendor'], "text" => $dbr['vendor'] . " - " . $dbr['ketvendor']);
            $dbr['tglperolehan'] = date_2d($dbr['tglperolehan']);
            echo ('
            with(bos.rptpbjthtmp.obj){
               find("#kodeaset").val("' . $dbr['kode'] . '") ;
			   find("#fakturaset").val("' . $dbr['faktur'] . '") ;
               find("#golasetaset").sval(' . json_encode($golaset) . ') ;
               find("#cabangaset").sval(' . json_encode($cabang) . ') ;
			   find("#vendoraset").sval(' . json_encode($vendor) . ') ;
               find("#tglperolehanaset").val("' . $dbr['tglperolehan'] . '") ;
               find("#hpaset").val("' . string_2s($dbr['hargaperolehan']) . '") ;
               find("#unitaset").val("' . $dbr['unit'] . '") ;
               find("#lamaaset").val("' . $dbr['lama'] . '") ;

               find("#tarifpenyaset").val("' . $dbr['tarifpenyusutan'] . '") ;
               find("#residuaset").val("' . string_2s($dbr['residu']) . '") ;
               find("#keteranganaset").val("' . $dbr['keterangan'] . '").focus() ;

            }
            bos.rptpbjthtmp.setopt("jenisaset","' . $dbr['jenispenyusutan'] . '");
            bos.rptpbjthtmp.loadmodalpreviewaset("show") ;
         ');
        }
    }

    public function PreviewDetailPembelianStock()
    {
        $va = $this->input->post();
        $cFaktur = $va['faktur'];
        echo ('w2ui["bos-form-rptpbjthtmp_grid2"].clear();');
        $data = $this->rptpbjthtmp_m->GetDataPerFaktur($cFaktur);
        if (!empty($data)) {
            echo ('
                  with(bos.rptpbjthtmp.obj){
                     find("#faktur").val("' . $data['faktur'] . '") ;
                     find("#fktpo").val("' . $data['fktpo'] . '") ;
                     find("#supplier").val("' . $data['supplier'] . '") ;
                     find("#subtotal").val("' . string_2s($data['subtotal']) . '") ;
                     find("#diskon").val("' . string_2s($data['diskon']) . '") ;
                     find("#ppn").val("' . string_2s($data['ppn']) . '") ;
                     find("#persppn").val("' . string_2s($data['persppn']) . '") ;
                     find("#total").val("' . string_2s($data['total']) . '") ;
                     find("#tgl").val("' . date_2d($data['Tgl']) . '") ;
                     find("#jthtmp").val("' . date_2d($data['jthtmp']) . '") ;
                  }

              ');
            $data = $this->rptpbjthtmp_m->getDetailPembelian($cFaktur);
            $vare = array();
            $n = 0;
            while ($dbr = $this->rptpbjthtmp_m->getrow($data)) {
                $n++;
                $vaset = $dbr;
                $vaset['recid'] = $n;
                $vaset['no'] = $n;
                $vare[] = $vaset;
            }

            $vare = json_encode($vare);
            echo ('
            bos.rptpbjthtmp.loadmodalpreview("show") ;
            bos.rptpbjthtmp.grid2_reloaddata();
            w2ui["bos-form-rptpbjthtmp_grid2"].add(' . $vare . ');
         ');
        }
    }

    public function initreport()
    {
        $va = $this->input->post();
        $cFaktur = $va['cFaktur'];
        $w = "p.faktur = '" . $cFaktur . "'";

        $file = setfile($this, "rpt", __FILE__, $va);
        savesession($this, $this->ss . "file", $file);
        savesession($this, $this->ss . "va", json_encode($va));
        file_put_contents($file, json_encode(array()));

        $file = getsession($this, $this->ss . "file");
        $data = @file_get_contents($file);
        $data = json_decode($data, true);
        $db = $this->bdb->getDetailPembelian($cFaktur);
        $nDebet = 0;
        $nKredit = 0;
        $nSaldo = 0;
        $s = 0;
        while ($dbr = $this->bdb->getrow($db)) {
            $no = ++$s;
            $data[] = array("#" => $no,
                "Keterangan" => $dbr['keterangan'],
                "HargaSatuan" => number_format($dbr['HargaSatuan']),
                "qty" => number_format($dbr['qty']),
                "satuan" => $dbr['satuan'],
                "Pembelian" => number_format($dbr['pembelian']),
                "Total" => number_format($dbr['total']),
            );
        }
        file_put_contents($file, json_encode($data));
        echo (' bos.rptpbjthtmp.openreport() ; ');
    }

    public function initreportTotal()
    {
        $va = $this->input->post();
        $dTglAkhir = date_2s($va['tglakhir']);
        $dTglAwal = date_2s($va['tglawal']);
        $file = setfile($this, "rpt_belihuttotal", __FILE__, $va);
        savesession($this, $this->ss . "file", $file);
        savesession($this, $this->ss . "va", json_encode($va));
        file_put_contents($file, json_encode(array()));

        $file = getsession($this, $this->ss . "file");
        $data = @file_get_contents($file);
        $data = json_decode($data, true);
        $db = $this->bdb->getTotalPembelian($dTglAwal,$dTglAkhir);
        while ($dbRow = $this->bdb->getrow($db)) {
            $key = $dbRow['supplier'];
            $key2 = $dbRow['jthtmp'] . "-" .$dbRow['id'];
            $data[$key]['header'][0] = array("ket"=>$key);
            $data[$key]['body'][$key2] = array("no" => 0,
                "faktur" => $dbRow['faktur'],
                "tgl" => date_2d($dbRow['tgl']),
                "jthtmp" => date_2d($dbRow['jthtmp']),
                "fktpo" => $dbRow['fktpo'],
                "supplier" => $dbRow['supplier'],
                "total" => string_2s($dbRow['total'])
            );
        }

        $db = $this->bdb->getTotalPembelianaset($dTglAwal,$dTglAkhir);
        while ($dbRow = $this->bdb->getrow($db)) {
            
            $key = $dbRow['supplier'];
            $key2 = $dbRow['jthtmp']."-".$dbRow['id'];
            $data[$key]['header'][0] = array("ket"=>$key);
            $data[$key]['body'][$key2] = array("no" => 0,
                "faktur" => $dbRow['faktur'],
                "tgl" => date_2d($dbRow['tgl']),
                "jthtmp" => date_2d($dbRow['jthtmp']),
                "fktpo" => "",
                "supplier" => $dbRow['supplier'],
                "total" => string_2s($dbRow['total'])
            );
        }

        ksort($data);
        $arrdata = array();
        $n = 0;
        foreach ($data as $key => $val) {
            
            $data[$key]['footer'][0]=array("ket"=>"","total"=>0);
            $subtotal = 0 ;
            foreach($val['body'] as $key2 => $val2){
                $n++;
                $data[$key]['body'][$key2]['no'] = $n;
                $subtotal += string_2n($val2['total']);
            }
            
            $data[$key]['footer'][0]['ket'] = "<b>Sub Total";
            $data[$key]['footer'][0]['total'] = string_2s($subtotal)."</b>";
        }

        //print_r($data);
        file_put_contents($file, json_encode($data));
        echo (' bos.rptpbjthtmp.openreporttotal() ; ');

    }

    public function showreport()
    {
        $va = json_decode(getsession($this, $this->ss . "va", "{}"), true);
        $file = getsession($this, $this->ss . "file");
        $data = @file_get_contents($file);
        $data = json_decode($data, true);
        if (!empty($data)) {
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
            
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> "" ,"5"=>$kota) ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"Mengetahui,","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"(Manager)","2"=>"","3"=>"(Bag.Keuangan)","4"=>"","5"=>"(Bag. Pembelian)") ;

            $nTotalSaldo = 0;
            $nNumber = 0;
            foreach ($data as $key => $value) {
                $nNumber = str_replace(",", "", $value['Total']);
                $nTotalSaldo += $nNumber;
            }
            $nTotalSaldo = number_format($nTotalSaldo);

            $total = array();
            $total[] = array("Ket" => "<b>Total",
                "Jumlah" => $nTotalSaldo,
                "Ket2" => "</b>");

            $font = 8;
            $dTglFaktur = $va['tgl'];

            $vDetail = array();
            $vDetail[] = array("1" => "<b>Faktur</b>", "2" => " : ", "3" => $va['cFaktur'], "4" => "FakturPO", "5" => ":", "6" => $va['fktpo']);
            $vDetail[] = array("1" => "<b>Supplier</b>", "2" => " : ", "3" => $va['supplier'], "4" => "PPn (%)", "5" => ":", "6" => $va['persppn']);
            $vDetail[] = array("1" => "<b>Tanggal</b>", "2" => " : ", "3" => $dTglFaktur, "4" => "Jth Tmp", "5" => ":", "6" => $va['jthtmp']);

            $o = array('paper' => 'A4', 'orientation' => 'p', 'export' => (isset($va['export']) ? $va['export'] : 0),
                'opt' => array('export_name' => 'Kartu Stock'));
            $this->load->library('bospdf', $o);
            $this->bospdf->ezText("<b>DETAIL PEMBELIAN STOCK</b>", $font + 4, array("justification" => "center"));
            $this->bospdf->ezText("");
            $this->bospdf->ezTable($vDetail, "", "",
                array("fontSize" => $font, "showHeadings" => 0, "showLines" => 0,
                    "cols" => array(
                        "1" => array("width" => 10, "justification" => "left"),
                        "2" => array("width" => 3, "justification" => "left"),
                        "3" => array("width" => 15, "justification" => "left"),
                        "4" => array("width" => 10, "justification" => "left"),
                        "5" => array("width" => 10, "justification" => "left"),
                        "6" => array("width" => 50, "justification" => "left")),
                )
            );

            $this->bospdf->ezText("");
            $this->bospdf->ezTable($data, "", "",
                array("fontSize" => $font,
                    "cols" => array(
                        "#" => array("width" => 2, "justification" => "right"),
                        "Keterangan" => array("wrap" => 1),
                        "HargaSatuan" => array("width" => 12, "justification" => "right"),
                        "qty" => array("width" => 6, "justification" => "center"),
                        "satuan" => array("width" => 9, "justification" => "center"),
                        "Pembelian" => array("width" => 12, "justification" => "right"),
                        "Total" => array("width" => 12, "justification" => "right")))
            );
            $this->bospdf->ezTable($total, "", "",
                array("fontSize" => $font, "showHeadings" => 0, "showLines" => 1,
                    "cols" => array(
                        "Ket" => array("justification" => "center"),
                        "Jumlah" => array("width" => 12, "justification" => "right"),
                        "Ket2" => array("width" => 12, "justification" => "center"),
                    ),
                )
            );

            $this->bospdf->ezText("");
            $this->bospdf->ezText("");
            $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "2"=>array("wrap"=>1,"justification"=>"center"),
                                          "3"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "4"=>array("wrap"=>1,"justification"=>"center"),
                                          "5"=>array("width"=>25,"wrap"=>1,"justification"=>"center"))
                                          )
                              ) ;
            $this->bospdf->ezStream();
        } else {
            echo ('kosong');
        }
    }

    public function showreporttotal()
    {
        $va = json_decode(getsession($this, $this->ss . "va", "{}"), true);
        $file = getsession($this, $this->ss . "file");
        $data = @file_get_contents($file);
        $data = json_decode($data, true);
        if (!empty($data)) {
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
            
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> "" ,"5"=>$kota) ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"Mengetahui,","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"(Manager)","2"=>"","3"=>"(Bag.Keuangan)","4"=>"","5"=>"(Bag. Pembelian)") ;

            $tottotal = 0;
            
            foreach ($data as $key => $val) {
                foreach($val['body'] as $key2 => $val2){
                    $tottotal += string_2n($val2['total']);
                }                
            }

            $tottotal = string_2s($tottotal);
            

            $total = array();
            $total[] = array("Ket" => "<b>Total",
                "Total" => $tottotal . "</b>");

            $font = 8;
            $o = array('paper' => 'A4', 'orientation' => 'p', 'export' => (isset($va['export']) ? $va['export'] : 0),
                'opt' => array('export_name' => 'LapPBJthTmp'));
            $this->load->library('bospdf', $o);
            $this->bospdf->ezText("<b>LAPORAN PEMBAYARAN JATUH TEMPO</b>", $font + 4, array("justification" => "center"));
            $this->bospdf->ezText("<b>Jatuh Tempo Tanggal : ".$va['tglawal']." sd " . $va['tglakhir'] . "</b>", $font + 4, array("justification" => "center"));
           // print_r($data);
            foreach($data as $key => $val){
                $this->bospdf->ezText("");
                $this->bospdf->ezTable($val['header'], "", "",
                    array("fontSize" => $font, "showHeadings" => 0, "showLines" => 0,
                        "cols" => array(
                            "ket" => array("justification" => "left")
                        ),
                    )
                );

                $this->bospdf->ezTable($val['body'], "", "",
                    array("fontSize" => $font,
                        "cols" => array(
                            "no" => array("caption" => "No", "width" => 4, "justification" => "right"),
                            "faktur" => array("caption" => "Faktur", "width" => 15, "wrap" => 1, "justification" => "center"),
                            "tgl" => array("caption" => "Tgl", "width" => 12, "wrap" => 1, "justification" => "center"),
                            "jthtmp" => array("caption" => "Jatuh Tempo", "width" => 12, "wrap" => 1, "justification" => "center"),
                            "supplier" => array("caption" => "Supplier", "wrap" => 1, "justification" => "left"),
                            "fktpo" => array("caption" => "PO", "width" => 15, "wrap" => 1, "justification" => "center"),
                            "total" => array("caption" => "Total", "width" => 13, "justification" => "right")
                        ))
                );

                $this->bospdf->ezTable($val['footer'], "", "",
                    array("fontSize" => $font, "showHeadings" => 0, "showLines" => 1,
                        "cols" => array(
                            "ket" => array("justification" => "center"),
                            "total" => array("width" => 13, "justification" => "right")
                        ),
                    )
                );
            }
            
            $this->bospdf->ezTable($total, "", "",
                array("fontSize" => $font, "showHeadings" => 0, "showLines" => 1,
                    "cols" => array(
                        "Ket" => array("justification" => "center"),
                        "Total" => array("width" => 13, "justification" => "right") 
                    ),
                )
            );
            $this->bospdf->ezText("");
            $this->bospdf->ezText("");
            $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "2"=>array("wrap"=>1,"justification"=>"center"),
                                          "3"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "4"=>array("wrap"=>1,"justification"=>"center"),
                                          "5"=>array("width"=>25,"wrap"=>1,"justification"=>"center"))
                                          )
                              ) ;
            $this->bospdf->ezStream();
        } else {
            echo ('kosong');
        }
    }
}
