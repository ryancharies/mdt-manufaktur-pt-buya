<?php

class Rptsaldopinjamankaryawan extends Bismillah_Controller{
    protected $bdb ;
    protected $ss ;
    protected $abc ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper('bdate');
        $this->load->model('rpt/rptsaldopinjamankaryawan_m') ;
        $this->load->model('func/perhitunganhrd_m') ;
        $this->bdb = $this->rptsaldopinjamankaryawan_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptsaldopinjamankaryawan_" ;
    }

    public function index(){
        $d    = array("setdate"=>date_set()) ;
        $this->load->view('rpt/rptsaldopinjamankaryawan', $d) ;
    }

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $va['tgl'] = date_2s($va['tgl']);
        $vdb    = $this->rptsaldopinjamankaryawan_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;
		$totsaldo = 0 ;
        while( $dbr = $this->rptsaldopinjamankaryawan_m->getrow($dbd) ){
            $n++;
            $vaset = $dbr;
            $vaset['tgl'] = date_2d($vaset['tgl']);
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['nip'],$va['tgl']);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['nip'],$va['tgl']);
            $vaset['bagian']  = $bagian['keterangan'];
            $vaset['jabatan']  = $jabatan['keterangan'];
            $vaset['bakidebet'] = $this->perhitunganhrd_m->getsaldopinjamankaryawan($dbr['nopinjaman'],$va['tgl']) ;
            $totsaldo += $vaset['bakidebet'];
			$vaset['no'] = $n;
            $vare[]     = $vaset ;
        }
		
		$vare[] = array("recid"=>'ZZZZ',"nama"=> 'Total',
                        "bakidebet"=>$totsaldo,"w2ui"=>array("summary"=> true));

        $vare    = array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }
	
	public function initreport(){
	  $va     = $this->input->post() ;
      savesession($this, $this->ss . "va", json_encode($va) ) ;
      $vare   = array() ;
      $va['tgl'] = date_2s($va['tgl']);
      $vdb    = $this->rptsaldopinjamankaryawan_m->loaddata($va) ;
      $dbd    = $vdb['db'] ;
	  $n = 0 ;
      while( $dbr = $this->rptsaldopinjamankaryawan_m->getrow($dbd) ){
		$n++;
        
        $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['nip'],$va['tgl']);
        $bagian = $this->perhitunganhrd_m->getbagian($dbr['nip'],$va['tgl']);
        $vaset['bagian']  = $bagian['keterangan'];
        $vaset['jabatan']  = $jabatan['keterangan'];
        $bakidebet = $this->perhitunganhrd_m->getsaldopinjamankaryawan($dbr['nopinjaman'],$va['tgl']) ; 
        
        $vare[]     = array("no"=>$n,"nopinjaman"=>$dbr['nopinjaman'],"tgl"=>date_2d($dbr['tgl']),"nip"=>$dbr['nip'],
                    "nama"=>$dbr['nama'],"jabatan"=>$jabatan['keterangan'],"bagian"=>$bagian['keterangan'],"plafond"=>$dbr['plafond'],
                    "lama"=>$dbr['lama'],"bakidebet"=>$bakidebet) ;
      }
	  
      savesession($this, "rptsaldopinjamankaryawan_rpt", json_encode($vare)) ;
      echo(' bos.rptsaldopinjamankaryawan.openreport() ; ') ;
	}

	public function showreport(){
      $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
      $data = getsession($this,"rptsaldopinjamankaryawan_rpt") ;      
      $data = json_decode($data,true) ;
	  $totbakidebet = 0 ;
      $n = 0 ;
     // print_r($data);
	  foreach($data as $key => $val){
		  $n++;
		    $data[$key]['no'] = $n;
            $totbakidebet += string_2n($val['bakidebet']) ;
            $data[$key]['bakidebet'] = string_2s($val['bakidebet']) ;
            $data[$key]['plafond'] = string_2s($val['plafond']) ;
	  }
	  $total = array();
	  $total[] = array("keterangan"=>"<b>Total","bakidebet"=>string_2s($totbakidebet)."</b>");
      if(!empty($data)){ 
      	$font = 8 ;
        $o    = array('paper'=>'A4', 'orientation'=>'l', 'export'=>"",
                        'opt'=>array('export_name'=>'saldopinjkaryawan_' . getsession($this, "username") ) ) ;
        $this->load->library('bospdf', $o) ;   
        $this->bospdf->ezText("<b>LAPORAN SALDO PINJAMAN KARYAWAN</b>",$font+4,array("justification"=>"center")) ;
        $this->bospdf->ezText("Sampai Tanggal : ". $va['tgl'],$font+2,array("justification"=>"center")) ;
		$this->bospdf->ezText("") ; 
		$this->bospdf->ezTable($data,"","",  
								array("fontSize"=>$font,"cols"=> array( 
			                     "no"=>array("caption"=>"No","width"=>5,"justification"=>"right"),
                                 "nopinjaman"=>array("caption"=>"No Pinjaman","width"=>12,"justification"=>"center"),
                                 "tgl"=>array("caption"=>"Tgl Realisasi","width"=>10,"justification"=>"center"),
                                 "nip"=>array("caption"=>"NIP","width"=>10,"justification"=>"center"),
			                     "nama"=>array("caption"=>"Nama","justification"=>"left"),
			                     "jabatan"=>array("caption"=>"Jabatan","justification"=>"left"),
			                     "bagian"=>array("caption"=>"Bagian","justification"=>"left"),
			                     "plafond"=>array("caption"=>"Plafond","width"=>15,"justification"=>"right"),
			                     "lama"=>array("caption"=>"Lama (Bln)","width"=>8,"justification"=>"right"),
			                     "bakidebet"=>array("caption"=>"Bakidebet","width"=>15,"justification"=>"right")))) ;  
		$this->bospdf->ezTable($total,"","",  
								array("fontSize"=>$font,"showHeadings"=>0,"cols"=> array( 
			                     "keterangan"=>array("caption"=>"Keterangan","wrap"=>1,"justification"=>"center"),
			                     "bakidebet"=>array("caption"=>"bakidebet","width"=>15,"justification"=>"right")))) ;  								 
        //print_r($data) ;    
        $this->bospdf->ezStream() ; 
      }else{
         echo('data kosong') ;
      }
   }
}

?>
