<?php

class Rptkontrakjthtmp extends Bismillah_Controller{
    protected $bdb ;
    protected $ss ;
    protected $abc ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper('bdate');
        $this->load->model('rpt/rptkontrakjthtmp_m') ;
        $this->load->model('func/perhitunganhrd_m') ;
        $this->bdb = $this->rptkontrakjthtmp_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptkontrakjthtmp_" ;
    }

    public function index(){
        $d    = array("setdate"=>date_set()) ;
        $this->load->view('rpt/rptkontrakjthtmp', $d) ;
    }

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->rptkontrakjthtmp_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;
		$totsaldo = 0 ;
        while( $dbr = $this->rptkontrakjthtmp_m->getrow($dbd) ){
            $n++;
            $vaset = $dbr;
            $vaset['tgl'] = date_2d($vaset['tgl']);
            $vaset['tglawal'] = date_2d($vaset['tglawal']);
            $vaset['tglakhir'] = date_2d($vaset['tglakhir']);
            $vaset['lamaperiode'] = $vaset['lama'] ." ". getperiode($vaset['periode']);
			$vaset['no'] = $n;
            $vare[]     = $vaset ;
        }
		
        $vare    = array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }
	
	public function initreport(){
	  $va     = $this->input->post() ;
      savesession($this, $this->ss . "va", json_encode($va) ) ;
      $vare   = array() ;
      $vdb    = $this->rptkontrakjthtmp_m->loaddata($va) ;
      $dbd    = $vdb['db'] ;
	  $n = 0 ;
      while( $dbr = $this->rptkontrakjthtmp_m->getrow($dbd) ){
		$n++;
        
        $dbr['tgl'] = date_2d($dbr['tgl']);
        $dbr['tglawal'] = date_2d($dbr['tglawal']);
        $dbr['tglakhir'] = date_2d($dbr['tglakhir']);
        $dbr['lamaperiode'] = $dbr['lama'] ." ". getperiode($dbr['periode']);
        
        $vare[]     = array("no"=>$n,"noperjanjian"=>$dbr['noperjanjian'],"nomor"=>$dbr['nomor'],"tgl"=>date_2d($dbr['tgl']),
                    "lamaperiode"=>$dbr['lamaperiode'],"tglawal"=>date_2d($dbr['tglawal']),"tglakhir"=>date_2d($dbr['tglakhir']),
                    "nip"=>$dbr['kode'],"nama"=>$dbr['nama'],"bagian"=>$dbr['bagian'],"jabatan"=>$dbr['jabatan']) ;
      }
	  
      savesession($this, "rptkontrakjthtmp_rpt", json_encode($vare)) ;
      echo(' bos.rptkontrakjthtmp.openreport() ; ') ;
	}

	public function showreport(){
      $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
      $data = getsession($this,"rptkontrakjthtmp_rpt") ;      
      $data = json_decode($data,true) ;
	  
      if(!empty($data)){ 
      	$font = 8 ;
        $o    = array('paper'=>'A4', 'orientation'=>'l', 'export'=>"",
                        'opt'=>array('export_name'=>'kontrakkaryawanjthtmp_' . getsession($this, "username") ) ) ;
        $this->load->library('bospdf', $o) ;   
        $this->bospdf->ezText("<b>LAPORAN KONTRAK KARYAWAN JATUH TEMPO</b>",$font+4,array("justification"=>"center")) ;
        $this->bospdf->ezText("Antara Tanggal : ". $va['tglawal'] . " sd " .$va['tglakhir'],$font+2,array("justification"=>"center")) ;
		$this->bospdf->ezText("") ; 
		$this->bospdf->ezTable($data,"","",  
								array("fontSize"=>$font,"cols"=> array( 
			                     "no"=>array("caption"=>"No","width"=>5,"justification"=>"right"),
                                 "noperjanjian"=>array("caption"=>"No Perjanjian","width"=>10,"justification"=>"center"),
                                 "nomor"=>array("caption"=>"Nomor","width"=>8,"justification"=>"center"),
                                 "tgl"=>array("caption"=>"Tgl","width"=>8,"justification"=>"center"),
                                 "lamaperiode"=>array("caption"=>"Lama Periode","width"=>10,"justification"=>"center"),
                                 "tglawal"=>array("caption"=>"Tgl Awal","width"=>8,"justification"=>"center"),
                                 "tglakhir"=>array("caption"=>"Tgl Akhir","width"=>8,"justification"=>"center"),
                                 "nip"=>array("caption"=>"NIP","width"=>8,"justification"=>"center"),
                                 "nama"=>array("caption"=>"Nama","justification"=>"left"),
                                 "ktp"=>array("caption"=>"KTP","width"=>10,"justification"=>"center"),
                                 "alamat"=>array("caption"=>"Alamat","justification"=>"left"),
                                 "telepon"=>array("caption"=>"Telp","width"=>10,"justification"=>"center"),
			                     "bagian"=>array("caption"=>"Bagian","width"=>10,"justification"=>"center"),
			                     "jabatan"=>array("caption"=>"Jabatan","width"=>10,"justification"=>"center")))) ;  							 
        //print_r($data) ;    
        $this->bospdf->ezStream() ; 
      }else{
         echo('data kosong') ;
      }
   }
}

?>
