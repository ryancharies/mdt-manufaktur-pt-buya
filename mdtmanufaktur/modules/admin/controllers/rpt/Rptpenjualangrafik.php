<?php
class rptpenjualangrafik extends Bismillah_Controller{

    protected $bdb ;
    protected $ss ;
    protected $abc ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper('bdate');
        $this->load->model('rpt/rptpenjualangrafik_m') ;
        $this->load->model('func/func_m') ;
        $this->load->model('func/perhitungan_m') ;
        $this->bdb = $this->rptpenjualangrafik_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptpenjualangrafik_" ;
    }

    public function index(){
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set()) ;
        $this->load->view('rpt/rptpenjualangrafik', $d) ;
    }

    public function reloadgrafik(){
        $va     = $this->input->post() ;
        $bulan = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
        

        $data = array();
        $rekpjawal = $this->bdb->getconfig("rekarkpjawal") ;
        $rekpjakhir = $this->bdb->getconfig("rekarkpjakhir") ;
        $thn = $va['thn'];
        $thnmin1 = $thn - 1;
        //tahun ini
        // $tglnow = date("Y-m-d");
        // $dnow = date("w",strtotime($tglnow));
        // $tglawalweek = date("Y-m-d",strtotime($tglnow) - ($dnow*60*60*24));
        $omsetthnini = array();
        
        $omsetthnkemarin = array();
        $jmlx = 0 ;
        $selisih = $va['blnakhir'] - $va['blnawal'];
        $labels = array();
        for($a = 0;$a<=$selisih;$a++){
            $bln = $a + $va['blnawal'];
            $omsetthnini[] = 0 ;
            $omsetthnkemarin[] = 0 ;
            $labels[] = $bulan[$bln-1];
        }
        $labels = json_encode($labels);


        //thn ini
        for($i=0;$i<=$selisih;$i++){
            $bln = $i + $va['blnawal'];
            $time = mktime(0,0,0,$bln,1,$thn);//"01-".$val['bln']."-".$thn;
            $tglawal = date("Y-m-d",$time);
            $tglakhir = date_eom($tglawal);
            $vdb    = $this->perhitungan_m->loadrekening($rekpjawal,$rekpjakhir) ;
            $jumlah = 0;
            $omsetthnini[$i] = 0 ;
            while($dbr = $this->bdb->getrow($vdb) ){                
                $debet = $this->perhitungan_m->getdebet($tglawal,$tglakhir,$dbr['kode']);
                $kredit = $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode']);
                $jumlah = $kredit - $debet;
                $omsetthnini[$i] += $jumlah;
            }
        }
        //minggu kemarin
        // $dnow = $dnow + 7;
        // $tglawalweek1 = date("Y-m-d",strtotime($tglnow) - ($dnow*60*60*24));
        $omsetthnkemarin = array(0,0,0,0,0,0,0,0,0,0,0,0);
        for($i=0;$i<=$selisih;$i++){
            $bln = $i + $va['blnawal'];
            $time = mktime(0,0,0,$bln,1,$thn-1);//"01-".$val['bln']."-".$thn;
            $tglawal = date("Y-m-d",$time);
            $tglakhir = date_eom($tglawal);
            $vdb    = $this->perhitungan_m->loadrekening($rekpjawal,$rekpjakhir) ;
            $jumlah = 0;
            $omsetthnkemarin[$i] = 0 ;
            while($dbr = $this->bdb->getrow($vdb) ){
                //print_r($dbr);
                $debet = $this->perhitungan_m->getdebet($tglawal,$tglakhir,$dbr['kode']);
                $kredit = $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode']);
                $jumlah = $kredit - $debet;
                $omsetthnkemarin[$i] += $jumlah;
            }
            $omsetthnkemarin[$i] = round($omsetthnkemarin[$i]);
        }

        $data[] = array("label"=>$thn,"data"=>$omsetthnini,"backgroundColor"=>"transparent",
                       "borderColor"=>"#FF6384","borderWidth"=>1);
        /*$data[] = array("label"=>$thn-1,"data"=>$omsetthnkemarin,"backgroundColor"=>"transparent",
                       "borderColor"=>"#36A2EB","borderWidth"=>1);
        */
        
        //                $data[] = array("label"=>$thn,"data"=>$omsetthnini,"backgroundColor"=>array("rgba(255, 99, 132, 0)"),
        //                "borderColor"=>array("rgba(255,99,132,1)"),"borderWidth"=>1);
        // $data[] = array("label"=>$thn-1,"data"=>$omsetthnkemarin,"backgroundColor"=>array("rgba(54, 162, 235, 0)"),
        //                "borderColor"=>array("rgba(54, 162, 235, 1)"),"borderWidth"=>1);
        //               print_r($data);
        $data = json_encode($data);    
        echo('
        bos.rptpenjualangrafik.cmdRefresh.html("Refresh");
        bos.rptpenjualangrafik.cmdRefresh.prop("disabled",false);

        var labels = '.$labels.';
        var ctx = document.getElementById("chartgrfkpenjualan").getContext("2d");
        var myChart = new Chart(ctx, {
            type: "line",
            data: {
                labels: labels,                
                datasets: '.$data.'
                
            },
            options: {
                showTooltips: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            stepSize: 500000000,
                            userCallback: function(value, index, values) {
                                // Convert the number to a string and splite the string every 3 charaters from the end
                                value = value.toString();
                                value = value.split(/(?=(?:...)*$)/);
                    
                                // Convert the array to a string and format the output
                                value = value.join(".");
                                return "Rp. " + value;
                            }
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "Omset"
                          }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Bulan"
                          }
                    }]
                },
                title: {
                    display: true,
                    text:"Progress Omset Penjualan",
                    fontSize:20
                },
                showTooltips: true,

                onAnimationComplete: function() {
                    this.showTooltip(this.datasets[0].points, true);
                },          
                tooltips: {
                    mode: "index",
                    podition: "nearest",
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var nominal = tooltipItem.yLabel.toString();
                            nominal = nominal.split(/(?=(?:...)*$)/);
                    
                                // Convert the array to a string and format the output
                            nominal = nominal.join(".");
                            
                            var isi = data.datasets[tooltipItem.datasetIndex].label + " : Rp. " + nominal;
                            return isi ;
                        }
                    }
                },
                animation: {
                    onComplete: function() {
                      const chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                        // ctx.rotate(-Math.PI / 4);
                      ctx.font = Chart.helpers.fontString(
                        10,
                        Chart.defaults.global.defaultFontStyle,
                        Chart.defaults.global.defaultFontFamily
                      );
                      ctx.textAlign = "center";
                      ctx.textBaseline = "bottom";
     
                      this.data.datasets.forEach(function(dataset, i) {
                        const meta = chartInstance.controller.getDatasetMeta(i);
                        
                        meta.data.forEach(function(bar, index) {
                          const data = dataset.data[index];

                          var nominal = data.toString();
                            nominal = nominal.split(/(?=(?:...)*$)/);
                    
                                // Convert the array to a string and format the output
                            nominal = nominal.join(".");

                          ctx.fillStyle = "#000";
                          ctx.fillText("Rp. "+nominal, bar._model.x-20, bar._model.y - 2);
                        });
                      });
                    }
                  }
            }
        });

       ');
    }

}
?>
