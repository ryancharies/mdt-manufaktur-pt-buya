
<?php
class Rpthppenjualan extends Bismillah_Controller{
	protected $bdb ;
	public function __construct(){
		parent::__construct() ;
		$this->load->model("rpt/rpthppenjualan_m") ;
        $this->load->model("func/perhitungan_m") ;
		$this->bdb 	= $this->rpthppenjualan_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrpthppenjualan_" ;
	}  

	public function index(){ 

        $data['rekhppbjawal']= $this->bdb->getconfig("rekhppbjawal") ;
        $data['ketrekhppbjawal'] = $this->bdb->getval("keterangan", "kode = '{$data['rekhppbjawal']}'", "keuangan_rekening");
        $data['rekhppbjakhir']= $this->bdb->getconfig("rekhppbjakhir") ;
        $data['ketrekhppbjakhir'] = $this->bdb->getval("keterangan", "kode = '{$data['rekhppbjakhir']}'", "keuangan_rekening");
        $data['rekhppbdpawal']= $this->bdb->getconfig("rekhppbdpawal") ;
        $data['ketrekhppbdpawal'] = $this->bdb->getval("keterangan", "kode = '{$data['rekhppbdpawal']}'", "keuangan_rekening");
        $data['rekhppbdpakhir']= $this->bdb->getconfig("rekhppbdpakhir") ;
        $data['ketrekhppbdpakhir'] = $this->bdb->getval("keterangan", "kode = '{$data['rekhppbdpakhir']}'", "keuangan_rekening");
        

		$this->load->view("rpt/rpthppenjualan",$data) ; 

	}

    public function saving(){
		$va 	= $this->input->post() ;
        $this->bdb->saveconfig("rekhppbjawal", $va['rekhppbjawal']) ;
        $this->bdb->saveconfig("rekhppbjakhir", $va['rekhppbjakhir']) ;
        $this->bdb->saveconfig("rekhppbdpawal", $va['rekhppbdpawal']) ;
        $this->bdb->saveconfig("rekhppbdpakhir", $va['rekhppbdpakhir']) ;


		echo('bos.rpthppenjualan.settab(0) ') ;
	}

    public function seekrekening(){
        $search     = $this->input->get('q');
        $vdb    = $this->bdb->seekrekening($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ".$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }
    
    public function loadgrid(){ 

	  	$va     = json_decode($this->input->post('request'), true) ; 
	    $tglawal 	= $va['tglawal'] ; //date("d-m-Y") ;
        $tglakhir 	= $va['tglakhir'] ; //date("d-m-Y") ;
	  	$tglkemarin = date("d-m-Y",strtotime($tglawal)-(24*60*60)) ;

        $vare   = array() ;

        //persd barang dalam proses awal
        $vare[] = array("kode"=>"","keterangan"=>"<b>:: Persediaan Barang Jadi ".$tglkemarin."</b>","1"=>"","2"=>"","3"=>"");
        $vdb    = $this->perhitungan_m->loadrekening($this->bdb->getconfig("rekhppbjawal"),$this->bdb->getconfig("rekhppbjakhir")) ;
        $jumlah = 0;
        while($dbr = $this->bdb->getrow($vdb) ){
            $saldoakhir = $this->perhitungan_m->getsaldo($tglkemarin,$dbr['kode']);
            $jumlah += $saldoakhir;
            $vare[] = array("kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan'],"1"=>$saldoakhir,"2"=>"","3"=>"","4"=>""); 
        }
        $vare[] = array("kode"=>"","keterangan"=>"<b>Jumlah Persediaan Barang Jadi ".$tglkemarin."</b>","1"=>"","2"=>$jumlah,"3"=>"","4"=>"");
        
        //reject
        $bpbj = 0 ;
        $selisihopname = 0 ;
        $returjual = 0 ;
        $vdb    = $this->perhitungan_m->loadrekening($this->bdb->getconfig("rekhpppbjawal"),$this->bdb->getconfig("rekhpppbjakhir")) ;
        while($dbr = $this->bdb->getrow($vdb) ){
            $bpbj -= $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode'],"PR");
            $selisihopname += $this->perhitungan_m->getdebet($tglawal,$tglakhir,$dbr['kode'],"PS");
            $selisihopname += $this->perhitungan_m->getdebet($tglawal,$tglakhir,$dbr['kode'],"SP");

            $selisihopname -= $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode'],"PS");
            $selisihopname -= $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode'],"SP");
            $selisihopname -= $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode'],"JR");
            
            $returjual += $this->perhitungan_m->getdebet($tglawal,$tglakhir,$dbr['kode'],"RJ");
           // $sk -= $this->perhitungan_m->getdebet($tglawal,$tglakhir,$dbr['kode'],"SK");

        }
        $vare[] = array("kode"=>"","keterangan"=>"Persd. Barang Jadi Reject ","1"=>"","2"=>$bpbj,"3"=>"","4"=>"");
        $vare[] = array("kode"=>"","keterangan"=>"Retur penjualan ","1"=>"","2"=>$returjual,"3"=>"","4"=>"");
        //$vare[] = array("kode"=>"","keterangan"=>"Selisih Opname Barang Jadi ","1"=>"","2"=>$selisihopname,"3"=>"","4"=>"");
        
        //mengaambil harga pokok produksi
        $jumlahproduksi = 0;
        $vdb    = $this->perhitungan_m->loadrekening($this->bdb->getconfig("rekhppbdpawal"),$this->bdb->getconfig("rekhppbdpakhir")) ;
        while($dbr = $this->bdb->getrow($vdb) ){
            $prod = $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode'],"PD");
            $jumlahproduksi += $prod;
        }
        $vare[] = array("kode"=>"","keterangan"=>"<b>Harga Pokok Produksi</b>","1"=>"","2"=>$jumlahproduksi,"3"=>"","4"=>"");

        //selisih pembulatan produksi
        $selisih = 0 ;
        $vdb    = $this->perhitungan_m->loadrekening('6.10.010.30','6.10.010.30') ;
        while($dbr = $this->bdb->getrow($vdb) ){
            $selisih -= $this->perhitungan_m->getdebet($tglawal,$tglakhir,$dbr['kode'],"PD");
			$selisih += $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode'],"PD");
        }
        $vare[] = array("kode"=>"","keterangan"=>"Selisih Pembulatan Hasil Produksi","1"=>"","2"=>$selisih,"3"=>"","4"=>"");

        $jmlsiapjual = $jumlahproduksi + $jumlah + $bpbj + $returjual + $selisih;// + $selisihopname;
        $vare[] = array("kode"=>"","keterangan"=>"<b>Barang Tersedia Untuk dijual</b>","1"=>"","2"=>"","3"=>$jmlsiapjual);
        $vare[] = array("kode"=>"","keterangan"=>"<b>:: Persediaan Barang Jadi ".$tglakhir."</b>","1"=>"","2"=>"","3"=>"","4"=>"");
        $jumlahsa = 0;
        $vdb    = $this->perhitungan_m->loadrekening($this->bdb->getconfig("rekhppbjawal"),$this->bdb->getconfig("rekhppbjakhir")) ;
        while($dbr = $this->bdb->getrow($vdb) ){
            $saldoakhir = $this->perhitungan_m->getsaldo($tglakhir,$dbr['kode']);
            $jumlahsa += $saldoakhir;
            $vare[] = array("kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan'],"1"=>$saldoakhir,"2"=>"","3"=>"","4"=>""); 
        }
        $vare[] = array("kode"=>"","keterangan"=>"<b>Jumlah Persediaan Barang Jadi ".$tglakhir."</b>","1"=>"","2"=>$jumlahsa,"3"=>"","4"=>"");
        $vare[] = array("kode"=>"","keterangan"=>"","1"=>"","2"=>"","3"=>$jumlahsa);
        $jmlhppproduk = $jmlsiapjual - $jumlahsa;
        $vare[] = array("kode"=>"","keterangan"=>"<b>Harga Pokok Penjualan Produk</b>","1"=>"","2"=>"","3"=>"","4"=>$jmlhppproduk);

        $bbjual = 0 ;
        $vdb    = $this->perhitungan_m->loadrekening($this->bdb->getconfig("rekhppbbpawal"),$this->bdb->getconfig("rekhppbbpakhir")) ;
        while($dbr = $this->bdb->getrow($vdb) ){
            $bbjual += $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode'],"CS") ;
            
        }

        //5.50
        
        $bbjual += $this->perhitungan_m->getdebet($tglawal,$tglakhir,"5.50","SK") ;

        
        //$selisihopname += $this->perhitungan_m->getdebet($tglawal, $tglakhir, "5.50", 'CS', '', "T", true) ;
        //$selisihopname += $this->perhitungan_m->getkredit($tglawal, $tglakhir,"5.50", 'CS', '', "T", true) ;
            
            //$selisihopname = $this->perhitungan_m->getkredit($tglawal, $tglakhir,"5.50", 'RJ', '', "T", false) ;
            $hpppos = 0 ;
            $hpppos += $this->perhitungan_m->getdebet($tglawal,$tglakhir,"5.50","SP") ;
            $hpppos += $this->perhitungan_m->getdebet($tglawal,$tglakhir,"5.50","PS") ;

            $hpppos -= $this->perhitungan_m->getkredit($tglawal,$tglakhir,"5.50","PS") ;
           $hpppos -= $this->perhitungan_m->getkredit($tglawal,$tglakhir,"5.50","SP") ;
           $hpppos -= $this->perhitungan_m->getkredit($tglawal,$tglakhir,"5.50","RJ") ;

       

        //$selisihopname = $selisihopname * -1;
        $bbjual += $selisihopname + $hpppos;
        $vare[] = array("kode"=>"","keterangan"=>"Harga Pokok Penjualan Barang Penolong","1"=>"","2"=>"","3"=>"","4"=>$bbjual);
        // $vare[] = array("kode"=>"","keterangan"=>"HPP K","1"=>"","2"=>"","3"=>"","4"=>$hppk);

        $jmlhpp = $jmlhppproduk + $bbjual;
        $vare[] = array("kode"=>"","keterangan"=>"<b>Harga Pokok Penjualan</b>","1"=>"","2"=>"","3"=>"","4"=>$jmlhpp);

        $vare 	= array("total"=>count($vare), "records"=>$vare ) ; 
      	echo(json_encode($vare)) ; 
    }

    public function initreport(){
        $n=0;
        $va     = $this->input->post() ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;

	    $tglawal 	= $va['tglawal'] ; //date("d-m-Y") ;
        $tglakhir 	= $va['tglakhir'] ; //date("d-m-Y") ;
	  	$tglkemarin = date("d-m-Y",strtotime($tglawal)-(24*60*60)) ;

        $vare   = array() ;

        //persd barang dalam proses awal
        $vare[] = array("kode"=>"<mdtls11><mdtlr10>","keterangan"=>"<b>:: Persediaan Barang Jadi ".$tglkemarin."</b>","1"=>"","2"=>"","3"=>"","4"=>"</mdtlr10></mdtls11>");
        $vdb    = $this->perhitungan_m->loadrekening($this->bdb->getconfig("rekhppbjawal"),$this->bdb->getconfig("rekhppbjakhir")) ;
        $jumlah = 0;

        while($dbr = $this->bdb->getrow($vdb) ){
            $saldoakhir = $this->perhitungan_m->getsaldo($tglkemarin,$dbr['kode']);
            $jumlah += $saldoakhir;
            $vare[] = array("kode"=>"<mdtls11>".$dbr['kode'],"keterangan"=>$dbr['keterangan'],"1"=>"<mdtlr01>".string_2s($saldoakhir)."</mdtlr01>","2"=>"","3"=>"","4"=>"</mdtls11>"); 
        }
        $vare[] = array("kode"=>"<mdtls11>","keterangan"=>"<b>Jumlah Persediaan Barang Jadi ".$tglkemarin,"1"=>"","2"=>string_2s($jumlah),"3"=>"","4"=>"</b></mdtls11>");
        
        //reject
        $bpbj = 0 ;
        $selisihopname = 0 ;
        $returjual = 0 ;
        $vdb    = $this->perhitungan_m->loadrekening($this->bdb->getconfig("rekhpppbjawal"),$this->bdb->getconfig("rekhpppbjakhir")) ;
        while($dbr = $this->bdb->getrow($vdb) ){
            $bpbj -= $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode'],"PR");
            
            $selisihopname += $this->perhitungan_m->getdebet($tglawal,$tglakhir,$dbr['kode'],"PS");
            $selisihopname += $this->perhitungan_m->getdebet($tglawal,$tglakhir,$dbr['kode'],"SP");

            $selisihopname -= $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode'],"PS");
            $selisihopname -= $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode'],"SP");
            $selisihopname -= $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode'],"JR");

            
            $returjual += $this->perhitungan_m->getdebet($tglawal,$tglakhir,$dbr['kode'],"RJ");
        }
        $vare[] = array("kode"=>"<mdtls11>","keterangan"=>"Persd. Barang Jadi Reject ","1"=>"","2"=>string_2s($bpbj),"3"=>"","4"=>"</mdtls11>");
        $vare[] = array("kode"=>"<mdtls11>","keterangan"=>"Retur Penjualan ","1"=>"","2"=>string_2s($returjual),"3"=>"","4"=>"</mdtls11>");
        //$vare[] = array("kode"=>"<mdtls11>","keterangan"=>"Selisih Opname Barang Jadi","1"=>"","2"=>string_2s($selisihopname),"3"=>"","4"=>"</mdtls11>");
        //mengaambil harga pokok produksi
        $jumlahproduksi = 0;
        $vdb    = $this->perhitungan_m->loadrekening($this->bdb->getconfig("rekhppbdpawal"),$this->bdb->getconfig("rekhppbdpakhir")) ;
        while($dbr = $this->bdb->getrow($vdb) ){
            $prod = $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode']);
            $jumlahproduksi += $prod;
        }

        $vare[] = array("kode"=>"<mdtls11>","keterangan"=>"Harga Pokok Produksi","1"=>"","2"=>string_2s($jumlahproduksi),"3"=>"","4"=>"</mdtls11>");

        //selisih pembulatan produksi
        $selisih = 0 ;
        $vdb    = $this->perhitungan_m->loadrekening('6.10.010.30','6.10.010.30') ;
        while($dbr = $this->bdb->getrow($vdb) ){
            $selisih -= $this->perhitungan_m->getdebet($tglawal,$tglakhir,$dbr['kode'],"PD");
			$selisih += $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode'],"PD");
        }
        $vare[] = array("kode"=>"<mdtls11>","keterangan"=>"Selisih Pembulatan Hasil Produksi","1"=>"<mdtlr01>","2"=>string_2s($selisih)."</mdtlr01>","3"=>"","4"=>"</mdtls11>");

        $jmlsiapjual = $jumlahproduksi + $jumlah + $bpbj + $selisih + $returjual;// +$selisihopname;
        $vare[] = array("kode"=>"<mdtls11>","keterangan"=>"<b>Barang Tersedia Untuk dijual","1"=>"","2"=>"","3"=>string_2s($jmlsiapjual),"4"=>"</b></mdtls11>");
        $vare[] = array("kode"=>"<mdtls11>","keterangan"=>"<b>:: Persediaan Barang Jadi ".$tglakhir,"1"=>"","2"=>"","3"=>"","4"=>"</b></mdtls11>");
        $jumlahsa = 0;
        $vdb    = $this->perhitungan_m->loadrekening($this->bdb->getconfig("rekhppbjawal"),$this->bdb->getconfig("rekhppbjakhir")) ;
        while($dbr = $this->bdb->getrow($vdb) ){
            $saldoakhir = $this->perhitungan_m->getsaldo($tglakhir,$dbr['kode']);
            $jumlahsa += $saldoakhir;
            $vare[] = array("kode"=>"<mdtls11>".$dbr['kode'],"keterangan"=>$dbr['keterangan'],"1"=>"<mdtlr01>".string_2s($saldoakhir)."</mdtlr01>","2"=>"","3"=>"","4"=>"</mdtls11>"); 
        }
        $vare[] = array("kode"=>"<mdtls11>","keterangan"=>"<b>Jumlah Persediaan Barang Jadi ".$tglakhir,"1"=>"<mdtlr01>","2"=>string_2s($jumlahsa)."</mdtlr01>","3"=>"","4"=>"</b></mdtls11>");
        $vare[] = array("kode"=>"<mdtls11><b>","keterangan"=>"","1"=>"","2"=>"","3"=>string_2s($jumlahsa)."</b>","4"=>"</mdtls11>");
        $jmlhppproduk = $jmlsiapjual - $jumlahsa;
        $vare[] = array("kode"=>"<mdtls11>","keterangan"=>"Harga Pokok Penjualan Produk","1"=>"","2"=>"<mdtlr01>","3"=>string_2s($jmlhppproduk)."</mdtlr01></b>","4"=>"</mdtls11>");
        
        $bbjual = 0 ;
        $vdb    = $this->perhitungan_m->loadrekening($this->bdb->getconfig("rekhppbbpawal"),$this->bdb->getconfig("rekhppbbpakhir")) ;
        while($dbr = $this->bdb->getrow($vdb) ){
            $bbjual += $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode'],"CS") ;
            $bbjual += $this->perhitungan_m->getkredit($tglawal,$tglakhir,$dbr['kode'],"SK") ;
        }

        $hpppos = 0 ;
            $hpppos += $this->perhitungan_m->getdebet($tglawal,$tglakhir,"5.50","SP") ;
            $hpppos += $this->perhitungan_m->getdebet($tglawal,$tglakhir,"5.50","PS") ;

            $hpppos -= $this->perhitungan_m->getkredit($tglawal,$tglakhir,"5.50","PS") ;
           $hpppos -= $this->perhitungan_m->getkredit($tglawal,$tglakhir,"5.50","SP") ;
           $hpppos -= $this->perhitungan_m->getkredit($tglawal,$tglakhir,"5.50","RJ") ;

        //$selisihopname = $selisihopname * -1;
        $bbjual += $selisihopname + $hpppos;
        $vare[] = array("kode"=>"<mdtls11>","keterangan"=>"Harga Pokok Penjualan Barang Penolong","1"=>"","2"=>"","3"=>"","4"=>string_2s($bbjual)."</mdtls11>");

        

        $jmlhpp = $jmlhppproduk + $bbjual ;
        $vare[] = array("kode"=>"<mdtls11><mdtlr12>","keterangan"=>"<b>Harga Pokok Penjualan","1"=>"","2"=>"","3"=>"","4"=>string_2s($jmlhpp)."</b></mdtlr12></mdtls11>");



      	savesession($this, "rpthppj_rpt", json_encode($vare)) ; 
        echo(' bos.rpthppenjualan.openreport() ; ') ;
    }
                            
    public function showreport(){
      $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
      $data = getsession($this,"rpthppj_rpt") ;      
      $data = json_decode($data,true) ;       
      if(!empty($data)){ 
      	$font = 8 ;
        $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>"",
                        'opt'=>array('export_name'=>'DaftarNeraca_' . getsession($this, "username") ) ) ;
        $this->load->library('bospdf', $o) ;   
        $this->bospdf->ezText("<b>LAPORAN HARGA POKOK PENJUALAN</b>",$font+4,array("justification"=>"center")) ;
        $this->bospdf->ezText("Periode : ".$va['tglawal']." sd ". $va['tglakhir'],$font+2,array("justification"=>"center")) ;
        $this->bospdf->ezText("") ; 
        
		$this->bospdf->ezTable($data,"","",  
								array("showHeadings"=>"","showLines"=>"0","fontSize"=>$font,"cols"=> array( 
			                     "kode"=>array("caption"=>"Tgl","width"=>10),
			                     "keterangan"=>array("caption"=>"Keterangan","wrap"=>1),
			                     "1"=>array("caption"=>"","width"=>14,"justification"=>"right"),
			                 	 "2"=>array("caption"=>"","width"=>14,"justification"=>"right"),
                                 "3"=>array("caption"=>"","width"=>14,"justification"=>"right"),
                                 "4"=>array("caption"=>"","width"=>14,"justification"=>"right")))) ;   
        //print_r($data) ;    
        $this->bospdf->ezStream() ; 
      }else{
         echo('data kosong') ;
      }
   }
}
?>
