
<?php
class Rptperbandinganlrthn extends Bismillah_Controller{
	protected $bdb ;
	public function __construct(){
		parent::__construct() ;
		$this->load->model("rpt/rptperbandinganlrthn_m") ;
        $this->load->model("func/perhitungan_m") ;
		$this->bdb 	= $this->rptperbandinganlrthn_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptperbandinganlrthn_" ;
	}  

	public function index(){ 
		$this->load->view("rpt/rptperbandinganlrthn") ; 

	}   

	public function loadgrid(){ 

	  	$va     = json_decode($this->input->post('request'), true) ; 
        $periode = $va['periode'];
        $vare   = array() ; 
        for($i=$periode;$i>=$periode-1;$i--){      
            $time = mktime(0,0,0,12,31,$i);//akhir thn
            $tglakhir = date("Y-m-d",$time);
  
            $time2 = mktime(0,0,0,1,1,$i);//awal thn
            $tglawal = date("Y-m-d",$time2);
            $arrlr = $this->perhitungan_m->getlr($tglawal,$tglakhir);//cek laba rugi
            
            foreach($arrlr['records'] as $key => $val){                
                if(!isset($vare[$key])){ 
                    $vare[$key]['kode']= $val['kode'];
                    $vare[$key]['keterangan']= $val['keterangan'];
                }
                $vare[$key][$i] = $val['saldoakhir'];                 
            }
        }


        
		$vare 	= array("total"=>count($vare), "records"=>$vare ) ; 
        $varpt = $vare['records'] ;
      	echo(json_encode($vare)) ; 
      	savesession($this, "rptperbandinganlrthn_rpt", json_encode($varpt)) ;  
	}

	public function init(){
		savesession($this, "ssrptperbandinganlrthn_id", "") ;    
	}
    
    public function initreport(){
        $n=0;
        $va     = $this->input->post() ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;

	    $periode = $va['periode'];
        $vare   = array() ; 
        for($i=$periode;$i>=$periode-1;$i--){      
            $time = mktime(0,0,0,12,31,$i);//akhir thn
            $tglakhir = date("Y-m-d",$time);
  
            $time2 = mktime(0,0,0,1,1,$i);//awal thn
            $tglawal = date("Y-m-d",$time2);
            $arrlr = $this->perhitungan_m->getlr($tglawal,$tglakhir);//cek laba rugi
            
            foreach($arrlr['records'] as $key => $val){                
                if(!isset($vare[$key])){ 
                    $vare[$key]['kode']= $val['kode'];
                    $vare[$key]['keterangan']= $val['keterangan'];
                }
                $vare[$key][$i] = $val['saldoakhir'];                 
            }
        }

		$vare 	= array("total"=>count($vare), "records"=>$vare ) ; 
        $varpt = $vare['records'] ;
      	savesession($this, "rptperbandinganlrthn_rpt", json_encode($varpt)) ; 
        echo(' bos.rptperbandinganlrthn.openreport() ; ') ;
    }

	public function showreport(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $data = getsession($this,"rptperbandinganlrthn_rpt") ;      
        $data = json_decode($data,true) ;     
        
        $periode = $va['periode'];
        $frmtkolom = array();
        $frmtkolom['kode'] = array("caption"=>"Kode","width"=>10);
        $frmtkolom['keterangan'] = array("caption"=>"Keterangan");
        for($i=$periode;$i>=$periode-1;$i--){      
            $frmtkolom[$i] = array("width"=>17,"justification"=>"right");
        }
        //print_r($data);
      if(!empty($data)){ 
      	$font = 8 ;
        $o    = array('paper'=>'A4', 'orientation'=>'portrait', 'export'=>"",
                        'opt'=>array('export_name'=>'DaftarPerbandinganNeraca_' . getsession($this, "username") ) ) ;
        $this->load->library('bospdf', $o) ;   
        $this->bospdf->ezText("<b>LAPORAN PERBANDINGAN NERACA</b>",$font+4,array("justification"=>"center")) ;
        $this->bospdf->ezText("Periode : ".$va['periode'],$font+2,array("justification"=>"center")) ;
		$this->bospdf->ezText("") ; 
		$this->bospdf->ezTable($data,"","",  
								array("fontSize"=>$font,"cols"=>$frmtkolom)) ;   
        //print_r($data) ;    
        $this->bospdf->ezStream() ; 
      }else{
         echo('data kosong') ;
      }
   }

}
?>
