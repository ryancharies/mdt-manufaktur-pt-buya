<?php

class Rptpokartu extends Bismillah_Controller{
    protected $bdb ;
    protected $ss ;
    protected $abc ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper('bdate');
        $this->load->model('rpt/Rptpokartu_m') ;
        $this->load->model('func/perhitungan_m') ;
        $this->load->library('m_pdf');
        $this->bdb = $this->Rptpokartu_m ;
        $this->ss  = getsession($this, "username")."-" . "ssRptpokartu_" ;
    }

    public function index(){
        $d    = array("setdate"=>date_set()) ;
        $this->load->view('rpt/rptpokartu', $d) ;
    }

    public function seekfaktur(){
        
        $va 	= $this->input->post() ;
        $faktur = $va['faktur'] ;
        if(trim($faktur) <> ""){
            $data   = $this->Rptpokartu_m->getdatatotalpo($va) ;
            if(!empty($data)){
                echo('
                    with(bos.rptpokartu.obj){
                    find("#faktur").val("'.$data['faktur'].'") ;
                    find("#fktpr").val("'.$data['fktpr'].'") ;
                    find("#tglpo").val("'.date_2d($data['tgl']).'");
                    find("#gudang").val("'.$data['ketgudang'].'") ;
                    find("#supplier").val("'.$data['namasupplier'].'") ;
                    }
                    bos.rptpokartu.grid1_reloadData();
                ') ;    
            }
        }else{
            echo('
                alert("Faktur tidak boleh kosong !!");
            ');
        }
    }
    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $arrdata = array();
        $n = 0 ;
        if(trim($va['faktur']) <> ""){
            //cek po detail
            $dbd    = $this->Rptpokartu_m->loaddetailpo($va['faktur']) ;
        
            while( $dbr = $this->Rptpokartu_m->getrow($dbd) ){
                if(!isset($vare[$dbr['stock']]))$vare[$dbr['stock']] = array("no"=>"","stock"=>$dbr['stock'],"namastock"=>$dbr['namastock'],"satuan"=>$dbr['satuan'],
                                    "qtypo"=>0,"qtypb"=>0,"saldo"=>0);
            
                $vare[$dbr['stock']]['qtypo'] += $dbr['qty'];
            
            }

            //cek pb detail
            $dbd    = $this->Rptpokartu_m->loaddetailpbbypo($va['faktur'],$va['tgl']) ;
        
            while( $dbr = $this->Rptpokartu_m->getrow($dbd) ){
                if(!isset($vare[$dbr['stock']]))$vare[$dbr['stock']] = array("no"=>"","stock"=>$dbr['stock'],"namastock"=>$dbr['namastock'],"satuan"=>$dbr['satuan'],
                                    "qtypo"=>0,"qtypb"=>0,"saldo"=>0);
                $vare[$dbr['stock']]['qtypb'] += $dbr['qty'];
            
            }

            
            $n = 0 ;
            foreach($vare as $key => $val){
                $n++;
                $val['no'] = $n;

                $val['cmddetail']    = '<button type="button" onClick="bos.rptpokartu.cmddetail(\''.$va['faktur'].'\',\''.$val['stock'].'\',\''.$va['tgl'].'\')"
                                     class="btn btn-success btn-grid">Detail</button>' ;
                $val['cmddetail']    = html_entity_decode($val['cmddetail']) ;
                $val['saldo'] = $val['qtypo'] - $val['qtypb'];
                $arrdata[] = $val;
                
            }

            
        }
        //cek pb detail
        $arrdata    = array("total"=>count($arrdata), "records"=>$arrdata ) ;
        echo(json_encode($arrdata)) ;
        
    }

    public function loaddetail(){
        $va    = $this->input->post() ;
        echo('w2ui["bos-form-rptpokartu_grid2"].clear();');
        $vare = array();
        $saldo  = 0;
        $n = 0 ;
        $satuan = "";
        $namastock = "";
        $dbd    = $this->Rptpokartu_m->loadpokartustock($va['faktur'],$va['stock'],$va['tgl']) ;
        while( $dbr = $this->Rptpokartu_m->getrow($dbd) ){
            $n++;
            $saldo += $dbr['debet'] - $dbr['kredit'];
            $vaset   = $dbr ;
            $vaset['no'] = $n;
            $vaset['saldo'] = $saldo;
            $vaset['tgl'] = date_2d($vaset['tgl']);
            $vaset['recid'] = $n;
            $vare[]     = $vaset ;
            $satuan = $dbr['satuan'];
            $namastock = $dbr['namastock'];
        }
        //print_r($vare);
        $vare = json_encode($vare);
        echo('
            with(bos.rptpokartu.obj){
                find("#stock").val("'.$va['stock'].'") ;
                find("#namastock").val("'.$namastock.'") ;
                find("#satuan").val("'.$satuan.'") ;
            }
            bos.rptpokartu.loadmodaldetail("show") ;
            bos.rptpokartu.grid2_reloaddata();
            w2ui["bos-form-rptpokartu_grid2"].add('.$vare.');
        ');
        
    }

    public function initreport(){
        $arr = array();
        $va     = $this->input->post() ;
        $file   = setfile($this, "rpt", __FILE__ , $va) ;
        savesession($this, $this->ss . "file", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
  
        file_put_contents($file, json_encode(array()) ) ;
        $file    = getsession($this, $this->ss . "file") ;
        $data    = @file_get_contents($file) ;
        $data    = json_decode($data,true) ;

        if(trim($va['faktur']) <> ""){
            //cek po detail
            $dbd    = $this->Rptpokartu_m->loaddetailpo($va['faktur'],$va['tgl']) ;
            $n=0;
            while( $dbr = $this->Rptpokartu_m->getrow($dbd) ){
                if(!isset($data[$dbr['stock']]))$data[$dbr['stock']] = array("no"=>++$n,"stock"=>$dbr['stock'],"namastock"=>$dbr['namastock'],"satuan"=>$dbr['satuan'],
                                    "qtypo"=>0,"qtypb"=>0,"saldo"=>0);
            
                $data[$dbr['stock']]['qtypo'] += $dbr['qty'];
            
            }

            //cek pb detail
            $dbd    = $this->Rptpokartu_m->loaddetailpbbypo($va['faktur'],$va['tgl']) ;
        
            while( $dbr = $this->Rptpokartu_m->getrow($dbd) ){
                if(!isset($data[$dbr['stock']]))$data[$dbr['stock']] = array("no"=>++$n,"stock"=>$dbr['stock'],"namastock"=>$dbr['namastock'],"satuan"=>$dbr['satuan'],
                                    "qtypo"=>0,"qtypb"=>0,"saldo"=>0);
                $data[$dbr['stock']]['qtypb'] += $dbr['qty'];
            
            }

            file_put_contents($file, json_encode($data) ) ;
            echo(' bos.rptpokartu.showreport() ; ') ;
         }else{
            echo('alert("Faktur tidak boleh kosong");');
         }
    }

    public function showreport(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $file = getsession($this, $this->ss . "file") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        // echo print_r($data);
  
        if(!empty($data)){
            foreach($data as $key => $val){
                $data[$key]['qtypo'] = string_2s($val['qtypo']);
                $data[$key]['qtypb'] = string_2s($val['qtypb']);
                $data[$key]['saldo'] = string_2s($val['saldo']);
            }

            $font = 8 ;
    
            $vhead = array() ;
            $vhead[] = array("1"=>"Faktur","2"=>":","3"=>$va['faktur'],"4"=>"Sampai Tgl","5"=>":","6"=>$va['tgl']) ;
            $vhead[] = array("1"=>"Tgl PO","2"=>":","3"=>$va['tglpo'],"4"=>"Supplier","5"=>":","6"=>$va['supplier']) ;
            $vhead[] = array("1"=>"Gudang","2"=>":","3"=>$va['gudang'],"4"=>"Fkt. SPP","5"=>":","6"=>$va['fktpr']) ;
  
            $vtitle = array() ;
            $vtitle[] = array("capt"=>"KARTU PURCHASE ORDER") ;
  
  
            $o    = array('paper'=>'A4', 'orientation'=>'P', 'export'=>'0',
                          'opt'=>array('export_name'=>'KARTU PURCHASE ORDER') ) ;
            $this->load->library('bospdf', $o) ;
             
            //ketika width tidak di set maka akan menyesuaikan dengan lebar kertas
            $this->bospdf->ezTable($vtitle,"","",
                 array("fontSize"=>$font+3,"showHeadings"=>0,"showLines"=>0,
                       "cols"=> array(
                          "capt"=>array("justification"=>"center"),)
                 )
              ) ;
  
            $this->bospdf->ezTable($vhead,"","",
                                      array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                            "cols"=> array(
                                               "1"=>array("width"=>10,"justification"=>"left"),
                                               "2"=>array("width"=>3,"justification"=>"left"),
                                               "3"=>array("justification"=>"left"),
                                               "4"=>array("width"=>10,"justification"=>"left"),
                                               "5"=>array("width"=>3,"justification"=>"left"),
                                               "6"=>array("justification"=>"left"),)
                                      )
                                   ) ;
  
           
            $this->bospdf->ezText("") ;
           
            $this->bospdf->ezTable($data,"","",
                                   array("fontSize"=>$font,
                                         "cols"=> array(
                                               "no"         =>array("width"=>5,"justification"=>"right","caption"=>"No"),
                                               "stock"  		=>array("width"=>10,"justification"=>"center","caption"=>"Kode"),
                                               "namastock" =>array("justification"=>"left","caption"=>"Keterangan"),
                                               "satuan"     =>array("width"=>8,"justification"=>"center","caption"=>"Satuan"),
                                               "qtypo"        =>array("width"=>10,"justification"=>"right","caption"=>"Qty PO"),
                                               "qtypb"        =>array("width"=>10,"justification"=>"right","caption"=>"Qty PB"),
                                               "saldo"         =>array("width"=>10,"justification"=>"right","caption"=>"Saldo")))
                                   ) ;
            $this->bospdf->ezStream() ;
        }else{
           echo('kosong') ;
        }
    }

    public function initreportdetil(){
        $arr = array();
        $va     = $this->input->post() ;
        $file   = setfile($this, "rpt", __FILE__ , $va) ;
        savesession($this, $this->ss . "file", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
  
        file_put_contents($file, json_encode(array()) ) ;
        $file    = getsession($this, $this->ss . "file") ;
        $data    = @file_get_contents($file) ;
        $data    = json_decode($data,true) ;

        if(trim($va['faktur']) <> ""){
            //cek po detail
            $saldo = 0 ;
            $dbd    = $this->Rptpokartu_m->loadpokartustock($va['faktur'],$va['stock'],$va['tgl']) ;
            $n=0;
            while( $dbr = $this->Rptpokartu_m->getrow($dbd) ){
                $n++;
                $saldo += $dbr['debet'] - $dbr['kredit'];
                $data[$n]     = array("no"=>$n,"faktur"=>$dbr['faktur'],"tgl"=>date_2d($dbr['tgl']),"keterangan"=>$dbr['keterangan'],
                            "debet"=>string_2s($dbr['debet']),"kredit"=>string_2s($dbr['kredit']),"saldo"=>string_2s($saldo)) ;
            
            }
            file_put_contents($file, json_encode($data) ) ;
            echo(' bos.rptpokartu.showreportdetil() ; ') ;
         }else{
            echo('alert("Faktur tidak boleh kosong");');
         }
    }

    public function showreportdetil(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $file = getsession($this, $this->ss . "file") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        // echo print_r($data);
  
        if(!empty($data)){
     
            $font = 8 ;
    
            $vhead = array() ;
            $vhead[] = array("1"=>"Faktur","2"=>":","3"=>$va['faktur'],"4"=>"Sampai Tgl","5"=>":","6"=>$va['tgl']) ;
            $vhead[] = array("1"=>"Tgl PO","2"=>":","3"=>$va['tglpo'],"4"=>"Supplier","5"=>":","6"=>$va['supplier']) ;
            $vhead[] = array("1"=>"Gudang","2"=>":","3"=>$va['gudang'],"4"=>"Fkt. SPP","5"=>":","6"=>$va['fktpr']) ;
            $vhead[] = array("1"=>"Stock","2"=>":","3"=>$va['stock']." - ".$va['namastock'],"4"=>"Satuan","5"=>":","6"=>$va['satuan']) ;
  
            $vtitle = array() ;
            $vtitle[] = array("capt"=>"KARTU STOCK PURCHASE ORDER") ;
  
  
            $o    = array('paper'=>'A4', 'orientation'=>'P', 'export'=>'0',
                          'opt'=>array('export_name'=>'KARTU STOCK PURCHASE ORDER') ) ;
            $this->load->library('bospdf', $o) ;
             
            //ketika width tidak di set maka akan menyesuaikan dengan lebar kertas
            $this->bospdf->ezTable($vtitle,"","",
                 array("fontSize"=>$font+3,"showHeadings"=>0,"showLines"=>0,
                       "cols"=> array(
                          "capt"=>array("justification"=>"center"),)
                 )
              ) ;
  
            $this->bospdf->ezTable($vhead,"","",
                                      array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                            "cols"=> array(
                                               "1"=>array("width"=>10,"justification"=>"left"),
                                               "2"=>array("width"=>3,"justification"=>"left"),
                                               "3"=>array("justification"=>"left","wrap"=>1),
                                               "4"=>array("width"=>10,"justification"=>"left"),
                                               "5"=>array("width"=>3,"justification"=>"left"),
                                               "6"=>array("justification"=>"left"),)
                                      )
                                   ) ;
  
           
            $this->bospdf->ezText("") ;
           
            $this->bospdf->ezTable($data,"","",
                                   array("fontSize"=>$font,
                                         "cols"=> array(
                                               "no"         =>array("width"=>5,"justification"=>"right","caption"=>"No"),
                                               "faktur"  		=>array("width"=>15,"justification"=>"center","caption"=>"Faktur"),
                                               "tgl"  		=>array("width"=>10,"justification"=>"center","caption"=>"Tgl"),
                                               "keterangan" =>array("justification"=>"left","caption"=>"Keterangan","wrap"=>1),
                                               "debet"        =>array("width"=>10,"justification"=>"right","caption"=>"Debet"),
                                               "kredit"        =>array("width"=>10,"justification"=>"right","caption"=>"Kredit"),
                                               "saldo"         =>array("width"=>10,"justification"=>"right","caption"=>"Saldo")))
                                   ) ;
            $this->bospdf->ezStream() ;
        }else{
           echo('kosong') ;
        }
    }
}

?>
