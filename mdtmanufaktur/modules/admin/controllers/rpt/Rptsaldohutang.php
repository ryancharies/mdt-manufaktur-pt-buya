<?php

class Rptsaldohutang extends Bismillah_Controller{
    protected $bdb ;
    protected $ss ;
    protected $abc ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper('bdate');
        $this->load->model('rpt/rptsaldohutang_m') ;
        $this->load->model('func/perhitungan_m') ;
        $this->bdb = $this->rptsaldohutang_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptsaldohutang_" ;
    }

    public function index(){
        $d    = array("setdate"=>date_set()) ;
        $this->load->view('rpt/rptsaldohutang', $d) ;
    }

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $va['tgl'] = date_2s($va['tgl']);
        $vdb    = $this->rptsaldohutang_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;
		$totsaldo = 0;
        while( $dbr = $this->rptsaldohutang_m->getrow($dbd) ){
            $n++;
            $vaset = $dbr;
            $vaset['saldo'] = 0 ;
            if($va['jenis'] == "H" || $va['jenis'] == ""){
                $vaset['saldo'] += $this->perhitungan_m->GetSaldoAkhirHutang($dbr['kode'],$va['tgl'],"PB","H",true) ; 
                $vaset['saldo'] += $this->perhitungan_m->GetSaldoAkhirHutang($dbr['kode'],$va['tgl'],"KV","H",true) ; 
                $vaset['saldo'] += $this->perhitungan_m->GetSaldoAkhirHutang($dbr['kode'],$va['tgl'],"RB","H",true) ;
            }
            
            if($va['jenis'] == "A" || $va['jenis'] == ""){
                $vaset['saldo'] += $this->perhitungan_m->GetSaldoAkhirHutang($dbr['kode'],$va['tgl'],"AS","H",true) ;
            }

            $vaset['no'] = $n;
            $totsaldo += $vaset['saldo'];
            $vaset['cmddetail']    = '<button type="button" onClick="bos.rptsaldohutang.cmddetail(\''.$dbr['kode'].'\',\''.$va['tgl'].'\')"
                                     class="btn btn-success btn-grid">Detail</button>' ;
            $vaset['cmddetail']    = html_entity_decode($vaset['cmddetail']) ;
            
            $vare[]     = $vaset ;
        }
		$vare[] = array("recid"=>'ZZZZ',"no"=> '', "kode"=> '',"nama"=> '', 'alamat'=>'TOTAL',"saldo"=>$totsaldo,"w2ui"=>array("summary"=> true));

        $vare    = array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }
	
	public function initreport(){
	  $va     = $this->input->post() ;
      savesession($this, $this->ss . "va", json_encode($va) ) ;
      $vare   = array() ;
      $va['tgl'] = date_2s($va['tgl']);
      $vdb    = $this->rptsaldohutang_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
	  $n = 0 ;
      while( $dbr = $this->rptsaldohutang_m->getrow($dbd) ){
		$n++;
        $saldo =  0;
        if($va['jenis'] == "H" || $va['jenis'] == ""){
            $saldo += $this->perhitungan_m->GetSaldoAkhirHutang($dbr['kode'],$va['tgl'],"PB","H",true) ; 
            $saldo += $this->perhitungan_m->GetSaldoAkhirHutang($dbr['kode'],$va['tgl'],"KV","H",true) ; 
            $saldo += $this->perhitungan_m->GetSaldoAkhirHutang($dbr['kode'],$va['tgl'],"RB","H",true) ;
        }
        
        if($va['jenis'] == "A" || $va['jenis'] == ""){
            $saldo += $this->perhitungan_m->GetSaldoAkhirHutang($dbr['kode'],$va['tgl'],"AS","H",true) ;
        }
		 
        
        $vare[]     = array("no"=>$n,"nama"=>$dbr['nama'],"alamat"=>$dbr['alamat'],"saldo"=>string_2s($saldo)) ;
      }
	  
      savesession($this, "rptsaldohutang_rpt", json_encode($vare)) ;
      echo(' bos.rptsaldohutang.openreport() ; ') ;
	}

	public function showreport(){
      $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
      $data = getsession($this,"rptsaldohutang_rpt") ;      
      $data = json_decode($data,true) ;

        $jenis = "";
        if($va['jenis'] == "A") $jenis = "ASET";
        if($va['jenis'] == "H") $jenis = "BAHAN BAKU";

	  $totsaldo = 0 ;
	  $n = 0 ;
	  foreach($data as $key => $val){
		  $n++;
		 $data[$key]['no'] = $n;
		$totsaldo += string_2n($val['saldo']) ;
	  }
	  $total = array();
	  $total[] = array("keterangan"=>"<b>Total","saldo"=>string_2s($totsaldo)."</b>");
      if(!empty($data)){ 
      	$font = 8 ;
        $o    = array('paper'=>'A4', 'orientation'=>'portrait', 'export'=>"",
                        'opt'=>array('export_name'=>'Saldohutang_' . getsession($this, "username") ) ) ;
		$ketsupplier = $this->bdb->getval("nama", "kode = '{$va['supplier']}'", "supplier");
        $this->load->library('bospdf', $o) ;   
        $this->bospdf->ezText("<b>LAPORAN SALDO HUTANG $jenis </b>",$font+4,array("justification"=>"center")) ;
        $this->bospdf->ezText("Sampai Tanggal : ". $va['tgl'],$font+2,array("justification"=>"center")) ;
		$this->bospdf->ezText("") ; 
		$this->bospdf->ezTable($data,"","",  
								array("fontSize"=>$font,"cols"=> array( 
			                     "no"=>array("caption"=>"No","width"=>5,"justification"=>"right"),
			                     "kode"=>array("caption"=>"Kode","width"=>10,"justification"=>"center"),
			                     "nama"=>array("caption"=>"Nama","justification"=>"left"),
			                     "alamat"=>array("caption"=>"Alamat","justification"=>"left"),
			                     "saldo"=>array("caption"=>"Saldo","width"=>15,"justification"=>"right")))) ;  
		$this->bospdf->ezTable($total,"","",  
								array("fontSize"=>$font,"showHeadings"=>0,"cols"=> array( 
			                     "keterangan"=>array("caption"=>"Keterangan","wrap"=>1,"justification"=>"center"),
			                     "saldo"=>array("caption"=>"Saldo","width"=>15,"justification"=>"right")))) ;  								 
        //print_r($data) ;    
        $this->bospdf->ezStream() ; 
      }else{
         echo('data kosong') ;
      }
   }

    public function detailhutang(){
        $va    = $this->input->post() ;
        echo('w2ui["bos-form-rptsaldohutang_grid2"].clear();');
        $data = $this->rptsaldohutang_m->getval("kode,nama,alamat", "kode = '{$va['kode']}'","supplier");
        if(!empty($data)){
            echo('
                with(bos.rptsaldohutang.obj){
                    find("#supplier").val("'.$data['kode'].'") ;
                    find("#namasupplier").val("'.$data['nama'].'") ;
                    find("#alamat").val("'.$data['alamat'].'") ;
                }

            ') ;

            $vare = array();

            $arrhut = $this->perhitungan_m->getsaldohutangdetail($data['kode'],$va['tgl']);
            $n = 0 ;
            $totsisa = 0 ;
            // print_r($arrhut);
            foreach($arrhut as $key => $val){
                $n++;
                $arr = array("recid"=>$n,"faktur"=>$val['faktur'],"tgl"=>date_2d($val['tgl']),"jthtmp"=>date_2d($val['jthtmp']),
                            "fktpo"=>$val['fktpo'],"subtotal"=>$val['subtotal'],"diskon"=>$val['diskon'],
                            "ppn"=>$val['ppn'],"total"=>$val['total'],"pembayaran"=>$val['pembayaran'],"sisa"=>$val['saldo']);
                $vare[] = $arr ;
                $totsisa += $val['saldo'];
            }
            $vare[] = array("recid"=>"ZZZZ","no" => '', "faktur"=> 'Jumlah',"sisa"=> $totsisa,"w2ui"=>array("summary"=>true));

            $vare = json_encode($vare);
            echo('
                bos.rptsaldohutang.loadmodalpreview("show") ;
                bos.rptsaldohutang.grid2_reloaddata();
                w2ui["bos-form-rptsaldohutang_grid2"].add('.$vare.');
            ');
        }
    }


    public function initreportd(){
        $va     = $this->input->post() ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        $vare   = array() ;
        $arrhut = $this->perhitungan_m->getsaldohutangdetail($va['supplier'],$va['tgl']);
        $n = 0 ;
        // print_r($arrhut);
        foreach($arrhut as $key => $val){
            $n++;
            $arr = array("no"=>$n,"faktur"=>$val['faktur'],"tgl"=>date_2d($val['tgl']),"jthtmp"=>date_2d($val['jthtmp']),
                            "fktpo"=>$val['fktpo'],"subtotal"=>string_2s($val['subtotal']),"diskon"=>string_2s($val['diskon']),
                            "ppn"=>string_2s($val['ppn']),"total"=>string_2s($val['total']),"pembayaran"=>string_2s($val['pembayaran']),
                            "sisa"=>string_2s($val['saldo']));
            $vare[] = $arr ;
        
        }
        
        savesession($this, "rptsaldohutangdetail_rpt", json_encode($vare)) ;
        echo(' bos.rptsaldohutang.openreportd() ; ') ;
      }
  
      public function showreportd(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $data = getsession($this,"rptsaldohutangdetail_rpt") ;      
        $data = json_decode($data,true) ;
        $totsaldo = 0 ;
        $n = 0 ;
        foreach($data as $key => $val){
            $n++;
            $data[$key]['no'] = $n;
            $totsaldo += string_2n($val['sisa']) ;
        }
        $total = array();
        $total[] = array("keterangan"=>"<b>Total","sisa"=>string_2s($totsaldo)."</b>");
        if(!empty($data)){ 
            $font = 8 ;
          $o    = array('paper'=>'A4', 'orientation'=>'l', 'export'=>"",
                          'opt'=>array('export_name'=>'saldouhutangdetail_' . getsession($this, "username") ) ) ;
          $ketsupplier = $this->bdb->getval("nama", "kode = '{$va['supplier']}'", "supplier");
          $this->load->library('bospdf', $o) ;   
          $this->bospdf->ezText("<b>LAPORAN DETAIL SALDO HUTANG</b>",$font+4,array("justification"=>"center")) ;
          $this->bospdf->ezText($va['supplier'] . " - " . $va['namasupplier'],$font+2,array("justification"=>"center")) ;
          $this->bospdf->ezText("Sampai Tanggal : ". $va['tgl'],$font+2,array("justification"=>"center")) ;
          $this->bospdf->ezText("") ; 
          $this->bospdf->ezTable($data,"","",  
                                  array("fontSize"=>$font,"cols"=> array( 
                                   "no"=>array("caption"=>"No","width"=>5,"justification"=>"right"),
                                   "faktur"=>array("caption"=>"Faktur","width"=>10,"justification"=>"center"),
                                   "tgl"=>array("caption"=>"Tgl","width"=>8,"justification"=>"center"),
                                   "jthtmp"=>array("caption"=>"JthTmp","width"=>8,"justification"=>"center"),
                                   "fktpo"=>array("caption"=>"Faktur. PO","justification"=>"center"),
                                   "subtotal"=>array("caption"=>"Subtotal","width"=>9,"justification"=>"right"),
                                   "ppn"=>array("caption"=>"PPn","width"=>9,"justification"=>"right"),
                                   "total"=>array("caption"=>"Total","width"=>9,"justification"=>"right"),
                                   "pembayaran"=>array("caption"=>"Pembayaran","width"=>9,"justification"=>"right"),
                                   "sisa"=>array("caption"=>"Sisa","width"=>9,"justification"=>"right")))) ;  
          $this->bospdf->ezTable($total,"","",  
                                  array("fontSize"=>$font,"showHeadings"=>0,"cols"=> array( 
                                   "keterangan"=>array("caption"=>"Keterangan","wrap"=>1,"justification"=>"center"),
                                   "sisa"=>array("caption"=>"Saldo","width"=>9,"justification"=>"right")))) ;  								 
          //print_r($data) ;    
          $this->bospdf->ezStream() ; 
        }else{
           echo('data kosong') ;
        }
     }

}

?>
