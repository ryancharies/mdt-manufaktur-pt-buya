<?php

class Rpthasilproduksi extends Bismillah_Controller{
    protected $bdb ;
    protected $ss ;
    protected $abc ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper('bdate');
        $this->load->model('rpt/rpthasilproduksi_m') ;
        $this->load->model('func/func_m') ;
        $this->load->library('escpos');
        $this->bdb = $this->rpthasilproduksi_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrpthasilproduksi_" ;
    }

    public function index(){
        $d    = array("setdate"=>date_set()) ;
        $this->load->view('rpt/rpthasilproduksi', $d) ;
    }

    public function loadgrid_where($bs, $s=''){
        $this->duser();
        
        $bs['tglawal'] = date_2s($bs['tglawal']);
        $bs['tglakhir'] = date_2s($bs['tglakhir']);

        $this->db->where('h.status', "1");
        $this->db->where('h.tgl >=', $bs['tglawal']);
        $this->db->where('h.tgl <=', $bs['tglakhir']);
        
        // print_r($bs);
        
        if($bs['skd_cabang'] !== 'null'){
            $bs['skd_cabang'] = json_decode($bs['skd_cabang'],true);
            $this->db->group_start();
            foreach($bs['skd_cabang'] as $kdc){
                $this->db->or_where('h.cabang',$kdc);
            }
            $this->db->group_end();
  
        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['cabang'])){
                    $this->db->group_start();
                    foreach($this->aruser['data_var']['cabang'] as $kdc){
                        $this->db->or_where('h.cabang', $kdc);
                    }
                    $this->db->group_end();
                }
            }
        }
  
        if($s !== ''){
            $this->db->group_start();
                $this->db->or_like(array('h.faktur'=>$s,'h.fakturproduksi'=>$s));
            $this->db->group_end();
        }
    }

    public function loadgrid(){
        $va         = json_decode($this->input->post('request'), true) ;
        $limit      = $va['offset'].",".$va['limit'] ;
        $search	    = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search     = $this->db->escape_like_str($search) ;
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $vare  = array() ;
        $vkaru = array() ;
        
        $this->loadgrid_where($va, $search);
        $f = "count(h.id) jml";
        $dbd = $this->db->select($f)
            ->from("produksi_hasil h")
            ->join("produksi_total t","t.faktur = h.fakturproduksi","left")
            ->join("produksi_produk p","p.fakturproduksi = t.faktur","left")
            ->get();
        $rtot  = $dbd->row_array();
        if($rtot['jml'] > 0){
            $this->loadgrid_where($va, $search);
            $f2 = "h.faktur,h.tgl,h.fakturproduksi,h.qty as qtyaktual,(h.qty * h.hp) as aktual,t.hargapokok as std,sum(p.qty) as qtystd,t.petugas,t.regu";
            $dbd = $this->db->select($f2)
                ->from("produksi_hasil h")
                ->join("produksi_total t","t.faktur = h.fakturproduksi","left")
                ->join("produksi_produk p","p.fakturproduksi = t.faktur","left")
                ->group_by('h.faktur')
                ->order_by('h.faktur ASC')
                ->limit($limit)
                ->get();
            foreach($dbd->result_array() as $dbr){  
                $vaset   = $dbr ;
                $vaset['tgl'] = date_2d($vaset['tgl']);

                $vaset['karu'] = $dbr['petugas'];
                if($dbr['regu'] !== null){
                    $regu = json_decode($dbr['regu'],true);
                    $vaset['regu'] = $regu['keterangan'];
                    $vkaru[$regu['karu']] = $this->bdb->getval("nama", "kode = '{$regu['karu']}'", "karyawan",);
                    $vaset['karu'] = $vkaru[$regu['karu']];
                }
                
                $vaset['cmdpreview']    = '<button type="button" onClick="bos.rpthasilproduksi.cmdpreviewdetail(\''.$dbr['faktur'].'\')"
                                        class="btn btn-success btn-grid">Preview Detail</button>' ;
                $vaset['cmdpreview']    = html_entity_decode($vaset['cmdpreview']) ;
                $vare[]     = $vaset ;
            }
        }

        $vare    = array("total"=>$rtot['jml'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function PreviewDetail(){
        $va    = $this->input->post() ;

        $this->db->where('h.faktur', $va['faktur']);
        $f = "h.faktur,h.tgl,h.hp as hargapokok,(h.hp * h.qty) as jumlah,t.btkl,t.bop,h.stock,s.keterangan,t.perbaikan,t.petugas,
            s.satuan,h.qty,h.fakturproduksi,(sum(b.hp * b.qty)/h.qty) bb,p.bop,p.btkl,p.hargapokokperbaikan,p.jumlahperbaikan";
        $dbd = $this->db->select($f)
            ->from("produksi_hasil h")
            ->join("produksi_total t","t.faktur = h.fakturproduksi","left")
            ->join("produksi_produk p","p.fakturproduksi = t.faktur","left")
            ->join("stock s","s.kode = h.stock","left")
            ->join("produksi_bb b","b.fakturproduksi = t.faktur","left")
            ->get();

        $data  = $dbd->row_array();
        if(!empty($data)){
            
            $data['tgl'] = date_2d($data['tgl']);

            $this->db->where('b.status', "1");
            $this->db->where('b.fakturproduksi', $data['fakturproduksi']);
            $f = "s.kode,s.keterangan,s.satuan,b.qty,b.hp,(b.qty * b.hp) as jmlhp";
            $dbd2 = $this->db->select($f)
                ->from("produksi_bb b")
                ->join("stock s","s.kode = b.stock","left")
                ->get();
            $vare = array();
            foreach($dbd2->result_array() as $dbr){  
                $vare[$dbr['kode']]         = $dbr ;
            }

            $data['detil'] = $vare;
        }

        echo(json_encode($data)) ;

    }

    public function initreport(){
        $va      = $this->input->post() ;

        $file   = setfile($this, "rpt", __FILE__ , $va) ;
        savesession($this, $this->ss . "file", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        file_put_contents($file, json_encode(array()) ) ;

        $file    = getsession($this, $this->ss . "file") ;
        $data    = @file_get_contents($file) ;
        $data    = json_decode($data,true) ;
        $s = 0 ;

        $this->db->where('b.fakturproduksi', $va['fakturprod']);
        $this->db->where('b.status', "1");
        $f = "s.kode,s.keterangan,s.satuan,b.qty,b.hp,(b.qty * b.hp) as jmlhp";
        $dbd2 = $this->db->select($f)
            ->from("produksi_bb b")
            ->join("stock s","s.kode = b.stock","left")
            ->get();
        foreach($dbd2->result_array() as $dbr){  
            $no      = ++$s ;
            $data[]  = array("No"=>$no,
                             "Kode"=>$dbr['kode'],
                             "Keterangan"=>$dbr['keterangan'],
                             "Qty"=>string_2s($dbr['qty']),
                             "Satuan"=>$dbr['satuan'],
                             "HP"=>string_2s($dbr['hp']),
                             "JmlHP"=>string_2s($dbr['jmlhp'])
                            ) ;
        }
        file_put_contents($file, json_encode($data) ) ;
        echo(' bos.rpthasilproduksi.openreport() ; ') ;
    }

    public function initreportTotal(){
        $va         = $this->input->post() ;
        $dTglAwal   = date_2s($va['tglawal']);
        $dTglAkhir  = date_2s($va['tglakhir']);
        $file       = setfile($this, "rpt_hptotal", __FILE__ , $va) ;
        savesession($this, $this->ss . "file", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        file_put_contents($file, json_encode(array()) ) ;

        $file    = getsession($this, $this->ss . "file") ;
        $data    = @file_get_contents($file) ;
        $data    = json_decode($data,true) ;

        $s       = 0 ;
        $this->loadgrid_where($va, "");
        $f2 = "h.faktur,h.tgl,h.fakturproduksi,h.qty as qtyaktual,(h.qty * h.hp) as aktual,t.hargapokok as std,sum(p.qty) as qtystd,t.petugas,t.regu";
        $dbd = $this->db->select($f2)
            ->from("produksi_hasil h")
            ->join("produksi_total t","t.faktur = h.fakturproduksi","left")
            ->join("produksi_produk p","p.fakturproduksi = t.faktur","left")
            ->group_by('h.faktur')
            ->order_by('h.faktur ASC')
            ->get();
        foreach($dbd->result_array() as $dbRow){ 
            $data[]  = array("No"=>++$s,
                             "Faktur"=>$dbRow['faktur'],
                             "Faktur Prod"=>$dbRow['fakturproduksi'],
                             "Tgl"=>date_2d($dbRow['tgl']),
                             "QTY STD"=>string_2s($dbRow['qtystd']),
                             "N STD"=>string_2s($dbRow['std']),
                             "QTY Aktual"=>string_2s($dbRow['qtyaktual']),
                             "N Aktual"=>string_2s($dbRow['aktual'])
                            ) ; 
        }

        file_put_contents($file, json_encode($data) ) ;
        echo(' bos.rpthasilproduksi.openreporttotal() ; ') ;

    }

    public function showreport(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $file = getsession($this, $this->ss . "file") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        //if(!empty($data)){
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> $kota ,"5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"Mengetahui,","3"=>"","4"=>"Ka.sie Produksi,","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"(.........................)","3"=>"","4"=>"(.........................)","5"=>"") ;

            $total   = array();

            $font = 8 ;

            $vDetail = array() ;
            $vDetail[] = array("1"=>"Faktur","2"=>" : ","3"=> $va['faktur'],"4"=>"","5"=>"Fkt. Produksi","6"=>" : ","7"=>$va['fakturprod'],"8"=>"","9"=>"Tanggal","10"=>" : ","11"=>date_2d($va['tgl'])) ;    
            $vDetail[] = array("1"=>"Perbaikan","2"=>" : ","3"=> $va['perbaikan'],"4"=>"","5"=>"Karu","6"=>":","7"=>$va['petugas'],"8"=>"","9"=>"","10"=>"","11"=>"") ;    

            $footer = array() ;
            $footer[] = array("1"=>"Produk","2"=>" : ","3"=> $va['namastock'],"4"=>"","5"=>"BB","6"=>" : ","7"=>$va['bb'],"8"=>"","9"=>"Qty","10"=>" : ","11"=>$va['qty'] . " [" . $va['satuan'] . "]") ;
            $footer[] = array("1"=>"HP. Perbaikan","2"=>" : ","3"=>$va['hpperbaikan'],"4"=>"","5"=>"BTKL","6"=>" : ","7"=>$va['btkl'],"8"=>"","9"=>"HP","10"=>" : ","11"=>$va['hp']) ;
            $footer[] = array("1"=>"Jml. Perbaikan","2"=>" : ","3"=> $va['jmlperbaikan'],"4"=>"","5"=>"BOP","6"=>" : ","7"=>$va['bop'],"8"=>"","9"=>"Jml HP","10"=>" : ","11"=>$va['jumlah']) ;


            $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                          'opt'=>array('export_name'=>'Kartu Stock') ) ;
            $this->load->library('bospdf', $o) ;
            $this->bospdf->ezText("<b>HASIL PRODUKSI</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vDetail,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "1"=>array("width"=>10,"justification"=>"left"),
                                             "2"=>array("width"=>3,"justification"=>"left"),
                                             "3"=>array("width"=>15,"justification"=>"left"),
                                             "4"=>array("justification"=>"left"),
                                             "5"=>array("width"=>10,"justification"=>"left"),
                                             "6"=>array("width"=>3,"justification"=>"left"),
                                             "7"=>array("width"=>15,"justification"=>"left"),
                                             "8"=>array("justification"=>"left"),
                                             "9"=>array("width"=>10,"justification"=>"left"),
                                             "10"=>array("width"=>3,"justification"=>"left"),
                                             "11"=>array("width"=>15,"justification"=>"left"))
                                        )
                                  ) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($data,"","",
                                   array("fontSize"=>$font,
                                         "cols"=> array(
                                             "No"           =>array("width"=>5,"justification"=>"right"),
                                             "Kode"         =>array("width"=>12,"justification"=>"center"),
                                             "Keterangan"   =>array("wrap"=>1),
                                             "Qty"          =>array("width"=>10,"justification"=>"right"),
                                             "Satuan"       =>array("width"=>12,"justification"=>"left"),
                                             "HP"           =>array("width"=>12,"justification"=>"right"),
                                             "JmlHP"        =>array("width"=>12,"justification"=>"right")
                                         ))
                                  ) ;
            $this->bospdf->ezTable($total,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>1,
                                         "cols"=> array(
                                             "Ket"=>array("justification"=>"center"),
                                             "Jumlah"=>array("width"=>12,"justification"=>"right"),
                                         )
                                        )
                                  ) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($footer,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "1"=>array("width"=>12,"justification"=>"left"),
                                             "2"=>array("width"=>3,"justification"=>"left"),
                                             "3"=>array("width"=>15,"justification"=>"left"),
                                             "4"=>array("justification"=>"left"),
                                             "5"=>array("width"=>12,"justification"=>"left"),
                                             "6"=>array("width"=>3,"justification"=>"left"),
                                             "7"=>array("width"=>15,"justification"=>"left"),
                                             "8"=>array("justification"=>"left"),
                                             "9"=>array("width"=>12,"justification"=>"left"),
                                             "10"=>array("width"=>3,"justification"=>"left"),
                                             "11"=>array("width"=>15,"justification"=>"left"))
                                        )
                                  ) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vttd,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "1"=>array("justification"=>"right"),
                                             "2"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                             "3"=>array("width"=>40,"wrap"=>1),
                                             "4"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                             "5"=>array("wrap"=>1,"justification"=>"center"))
                                        )
                                  ) ;
            $this->bospdf->ezStream() ;
        /*}else{
            echo('kosong') ;
        }*/
    }

    public function showreporttotal(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $file = getsession($this, $this->ss . "file") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        if(!empty($data)){
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> $kota ,"5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"Mengetahui,","3"=>"","4"=>"Ka.sie Produksi,","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"(.........................)","3"=>"","4"=>"(.........................)","5"=>"") ;

            $nTotalQtySTD      = 0 ;
            $nTotalSTD      = 0 ;
            $nTotalQtyAktual      = 0 ;
            $nTotalAktual      = 0 ;
            foreach ($data as $key => $value) {
                $nTotalQtySTD += string_2n($value['QTY STD']) ;
                $nTotalSTD += string_2n($value['N STD']) ;
                $nTotalQtyAktual += string_2n($value['QTY Aktual']) ;
                $nTotalAktual += string_2n($value['N Aktual']) ;

            }


            $total   = array();
            $total[] = array("Ket"=>"<b>Total",
                             "QTY STD"=>string_2s($nTotalQtySTD),
                             "N STD"=>string_2s($nTotalSTD),
                             "QTY Aktual"=>string_2s($nTotalQtyAktual),
                             "N Aktual"=>string_2s($nTotalAktual));

            $font = 8 ;
            $o    = array('paper'=>'A4', 'orientation'=>'P', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                          'opt'=>array('export_name'=>'Kartu Stock') ) ;
            $this->load->library('bospdf', $o) ;
            $this->bospdf->ezText("<b>LAPORAN TOTAL PERINTAH PRODUKSI</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("<b>Periode : " .$va['tglawal']. " s/d " . $va['tglakhir'] . "</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($data,"","",
                                   array("fontSize"=>$font,
                                         "cols"=> array(
                                             "No"=>array("width"=>5,"justification"=>"right"),
                                             "Faktur"=>array("wrap"=>1,"justification"=>"center"),
                                             "Faktur Prod"=>array("wrap"=>1,"justification"=>"center"),
                                             "Tgl"=>array("width"=>12,"wrap"=>1,"justification"=>"center"),
                                             "QTY STD"=>array("width"=>12,"justification"=>"right"),
                                             "N STD"=>array("width"=>12,"justification"=>"right"),
                                             "QTY Aktual"=>array("width"=>12,"justification"=>"right"),
                                             "N Aktual"=>array("width"=>12,"justification"=>"right")
                                         ))
                                  ) ;
            $this->bospdf->ezTable($total,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>1,
                                         "cols"=> array(
                                             "Ket"=>array("justification"=>"center"),
                                             "QTY STD"=>array("width"=>12,"justification"=>"right"),
                                             "N STD"=>array("width"=>12,"justification"=>"right"),
                                             "QTY Aktual"=>array("width"=>12,"justification"=>"right"),
                                             "N Aktual"=>array("width"=>12,"justification"=>"right")
                                         )
                                        )
                                  ) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vttd,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "1"=>array("justification"=>"right"),
                                             "2"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                             "3"=>array("width"=>40,"wrap"=>1),
                                             "4"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                             "5"=>array("wrap"=>1,"justification"=>"center"))
                                        )
                                  ) ;
            $this->bospdf->ezStream() ;
        }else{
            echo('kosong') ;
        }
    }
    public function cetakdm(){
        $va 	= $this->input->post() ;
        //print_r($va);
		
		$faktur = $va['faktur'] ;
		

        $this->func_m->cetakhasilproduksi($faktur);
    }
}

?>
