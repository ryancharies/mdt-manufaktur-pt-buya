<?php

class Rptagingreportpb extends Bismillah_Controller{
    protected $bdb ;
    protected $ss ;
    protected $abc ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper('bdate');
        $this->load->model('rpt/rptagingreportpb_m') ;
        $this->load->model('func/Perhitungan_m') ;
        $this->bdb = $this->rptagingreportpb_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptagingreportpb_" ;
    }

    public function index(){
        $d    = array("setdate"=>date_set()) ;
        $this->load->view('rpt/rptagingreportpb', $d) ;
    }

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $va['tglakhir'] = date_2s($va['tglAkhir']);
        $vdb    = $this->rptagingreportpb_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $total = 0 ;
        $pelunasan = 0 ;
        $saldo = 0 ;
        while( $dbr = $this->rptagingreportpb_m->getrow($dbd) ){
            $vaset   = $dbr ;
            $key = $dbr['jthtmp']."-".$dbr['faktur'];
            $vaset['pelunasan'] = $vaset['total'] - $vaset['saldo'];
            $total += $vaset['total'] ;
            $pelunasan += $vaset['pelunasan'] ;
            $saldo += $vaset['saldo'];
            $vaset['tgl'] = date_2d($vaset['tgl']);
            $vaset['jthtmp'] = date_2d($vaset['jthtmp']);
            $vaset['cmdPreview']    = '<button type="button" onClick="bos.rptagingreportpb.cmdpreviewdetail(\''.$dbr['faktur'].'\')"
                                     class="btn btn-success btn-grid">Detail Pembelian</button>' ;
                                     
            $vaset['cmdPreview']    = html_entity_decode($vaset['cmdPreview']) ;
            $vare[$key]     = $vaset ;
        }

        //pembelian aset
        $vdb    = $this->rptagingreportpb_m->loadhutpembelianaset($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->rptagingreportpb_m->getrow($dbd) ){
            $vaset   = $dbr ;
            $key = $dbr['jthtmp']."-".$dbr['faktur'];
            $vaset['pelunasan'] = $vaset['total'] - $vaset['saldo'];
            $total += $vaset['total'] ;
            $pelunasan += $vaset['pelunasan'] ;
            $saldo += $vaset['saldo'];
            $vaset['tgl'] = date_2d($vaset['tgl']);
            $vaset['jthtmp'] = date_2d($vaset['jthtmp']);
            $vaset['cmdPreview']    = '<button type="button" onClick="bos.rptagingreportpb.cmdpreviewdetailaset(\''.$dbr['kode'].'\')"
            class="btn btn-success btn-grid">Detail Pembelian</button>' ;
            $vaset['cmdPreview']    = html_entity_decode($vaset['cmdPreview']) ;
            $vare[$key]     = $vaset ;
        }

        ksort($vare);
        $data = array();
        foreach($vare as $key => $val){
            $data[] = $val;
        }

        $data[] = array("recid"=>"zzzz","faktur"=>"","supplier"=>"","tgl"=>"","jthtmp"=>"",
                        "total"=>$total,"pelunasan"=>$pelunasan,"saldo"=>$saldo,"w2ui"=>array("summary"=>true));

        $vare    = array("total"=>count($data)-1, "records"=>$data ) ;
        echo(json_encode($vare)) ;
    }

    public function PreviewDetailPembelianaset(){
        $va 	= $this->input->post() ;
        $kode 	= $va['kode'] ;
        $data = $this->rptagingreportpb_m->getdatapembelianaset($kode) ;
        if( $dbr = $this->rptagingreportpb_m->getrow($data) ){
            $golaset[] = array("id"=>$dbr['golongan'],"text"=>$dbr['golongan'] ." - ". $dbr['ketgolongan']);
            $cabang[] = array("id"=>$dbr['cabang'],"text"=>$dbr['cabang'] ." - ". $dbr['ketcabang']);
			$vendor[] = array("id"=>$dbr['vendor'],"text"=>$dbr['vendor'] ." - ". $dbr['ketvendor']);
            $dbr['tglperolehan'] = date_2d($dbr['tglperolehan']);
            echo('
            with(bos.rptagingreportpb.obj){
               find("#kodeaset").val("'.$dbr['kode'].'") ;
			   find("#fakturaset").val("'.$dbr['faktur'].'") ;
               find("#golasetaset").sval('.json_encode($golaset).') ;
               find("#cabangaset").sval('.json_encode($cabang).') ;
			   find("#vendoraset").sval('.json_encode($vendor).') ;
               find("#tglperolehanaset").val("'.$dbr['tglperolehan'].'") ;
               find("#hpaset").val("'.string_2s($dbr['hargaperolehan']).'") ;
               find("#unitaset").val("'.$dbr['unit'].'") ;
               find("#lamaaset").val("'.$dbr['lama'].'") ;

               find("#tarifpenyaset").val("'.$dbr['tarifpenyusutan'].'") ;
               find("#residuaset").val("'.string_2s($dbr['residu']).'") ;
               find("#keteranganaset").val("'.$dbr['keterangan'].'").focus() ;

            }
            bos.rptagingreportpb.setopt("jenisaset","'.$dbr['jenispenyusutan'].'");
            bos.rptagingreportpb.loadmodalpreviewaset("show") ;
         ') ;
        }
    }

    public function PreviewDetailPembelianStock(){
        $va    = $this->input->post() ;
        $cFaktur = $va['faktur'] ;
        echo('w2ui["bos-form-rptagingreportpb_grid2"].clear();');
        $data = $this->rptagingreportpb_m->GetDataPerFaktur($cFaktur) ;
        if(!empty($data)){
            echo('
                  with(bos.rptagingreportpb.obj){
                     find("#faktur").val("'.$data['faktur'].'") ;
                     find("#fktpo").val("'.$data['fktpo'].'") ;
                     find("#supplier").val("'.$data['supplier'].'") ;
                     find("#subtotal").val("'.string_2s($data['subtotal']).'") ;
                     find("#diskon").val("'.string_2s($data['diskon']).'") ;
                     find("#ppn").val("'.string_2s($data['ppn']).'") ;
                     find("#persppn").val("'.string_2s($data['persppn']).'") ;
                     find("#total").val("'.string_2s($data['total']).'") ;
                     find("#tgl").val("'.date_2d($data['Tgl']).'") ;
                     find("#jthtmp").val("'.date_2d($data['jthtmp']).'") ;
                  }

              ') ;
            $data = $this->rptagingreportpb_m->getDetailPembelian($cFaktur) ;
            $vare = array();
            $n = 0 ;
            while($dbr = $this->rptagingreportpb_m->getrow($data)){
                $n++;
                $vaset          = $dbr ;
                $vaset['recid'] = $n;
                $vaset['no']    = $n;
                $vare[]         = $vaset ;
            }

            $vare = json_encode($vare);
            echo('
            bos.rptagingreportpb.loadmodalpreview("show") ;
            bos.rptagingreportpb.grid2_reloaddata();
            w2ui["bos-form-rptagingreportpb_grid2"].add('.$vare.');
         ');
        }
    }

    public function initreport(){
        $va      = $this->input->post() ;
        $cFaktur  = $va['cFaktur'];
        $w    = "p.faktur = '".$cFaktur."'" ;

        $file   = setfile($this, "rpt", __FILE__ , $va) ;
        savesession($this, $this->ss . "file", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        file_put_contents($file, json_encode(array()) ) ;

        $file    = getsession($this, $this->ss . "file") ;
        $data    = @file_get_contents($file) ;
        $data    = json_decode($data,true) ;
        $db      = $this->bdb->getDetailPembelian($cFaktur) ;
        $nDebet  = 0 ;
        $nKredit = 0 ;
        $nSaldo  = 0 ;
        $s = 0 ;
        while($dbr = $this->bdb->getrow($db)){
            $no      = ++$s ;
            $data[]  = array("#"=>$no,
                             "Keterangan"=>$dbr['keterangan'],
                             "HargaSatuan"=>number_format($dbr['HargaSatuan']),
                             "qty"=>number_format($dbr['qty']),
                             "satuan"=>$dbr['satuan'],
                             "Pembelian"=>number_format($dbr['pembelian']),
                             "Total"=>number_format($dbr['total'])
                            ) ;
        }
        file_put_contents($file, json_encode($data) ) ;
        echo(' bos.rptagingreportpb.openreport() ; ') ;
    }

    public function initreportTotal(){
        $va         = $this->input->post() ;
        $dTglAkhir  = date_2s($va['tglakhir']);
        $file       = setfile($this, "rpt_belihuttotal", __FILE__ , $va) ;
        savesession($this, $this->ss . "file", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        file_put_contents($file, json_encode(array()) ) ;

        $file    = getsession($this, $this->ss . "file") ;
        $data    = @file_get_contents($file) ;
        $data    = json_decode($data,true) ;
        $db      = $this->bdb->getTotalPembelian($dTglAkhir) ;
        $s       = 0 ;
        while ($dbRow = $this->bdb->getrow($db)) {
            $no      = ++$s ;
            $pelunasan = $dbRow['total'] - $dbRow['saldo'];
            $key = $dbRow['jthtmp'] ."-".$dbRow['faktur'];
            $data[$key]  = array("no"=>$no,
                             "faktur"=>$dbRow['faktur'],
                             "tgl"=>date_2d($dbRow['tgl']),
                             "jthtmp"=>date_2d($dbRow['jthtmp']),
                             "fktpo"=>$dbRow['fktpo'],
                             "supplier"=>$dbRow['supplier'],
                             "total"=>string_2s($dbRow['total']),
                             "pelunasan"=>string_2s($pelunasan),
                             "saldo"=>string_2s($dbRow['saldo'])
                            ) ;
        }

        $db      = $this->bdb->getTotalPembelianaset($dTglAkhir) ;
        while ($dbRow = $this->bdb->getrow($db)) {
            $no      = ++$s ;
            $pelunasan = $dbRow['total'] - $dbRow['saldo'];
            $key = $dbRow['jthtmp'] ."-".$dbRow['faktur'];
            $data[$key]  = array("no"=>$no,
                             "faktur"=>$dbRow['faktur'],
                             "tgl"=>date_2d($dbRow['tgl']),
                             "jthtmp"=>date_2d($dbRow['jthtmp']),
                             "fktpo"=>"",
                             "supplier"=>$dbRow['supplier'],
                             "total"=>string_2s($dbRow['total']),
                             "pelunasan"=>string_2s($pelunasan),
                             "saldo"=>string_2s($dbRow['saldo'])
                            ) ;
        }
         
        ksort($data);
        $arrdata = array();
        $n = 0 ;
        foreach($data as $key => $val){
            $n++;
            $val['no'] = $n ;
            $arrdata[] = $val;
        }
        file_put_contents($file, json_encode($arrdata) ) ;
        echo(' bos.rptagingreportpb.openreporttotal() ; ') ;

    }

    public function showreport(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $file = getsession($this, $this->ss . "file") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        if(!empty($data)){
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
            
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> "" ,"5"=>$kota) ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"Mengetahui,","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"(Manager)","2"=>"","3"=>"(Bag.Keuangan)","4"=>"","5"=>"(Bag. Pembelian)") ;



            $nTotalSaldo  = 0 ;
            $nNumber = 0 ;
            foreach ($data as $key => $value) {
                $nNumber       = str_replace(",", "", $value['Total']) ;
                $nTotalSaldo  += $nNumber;
            }
            $nTotalSaldo = number_format($nTotalSaldo) ;

            $total   = array();
            $total[] = array("Ket"=>"<b>Total",
                             "Jumlah"=>$nTotalSaldo,
                             "Ket2"=>"</b>");

            $font = 8 ;
            $dTglFaktur = $va['tgl'];

            $vDetail = array() ;
            $vDetail[] = array("1"=>"<b>Faktur</b>","2"=>" : ","3"=> $va['cFaktur'],"4"=>"FakturPO","5"=>":","6"=>$va['fktpo'], ) ;
            $vDetail[] = array("1"=>"<b>Supplier</b>","2"=>" : ","3"=> $va['supplier'],"4"=>"PPn (%)","5"=>":","6"=>$va['persppn'], ) ;
            $vDetail[] = array("1"=>"<b>Tanggal</b>","2"=>" : ","3"=> $dTglFaktur,"4"=>"Jth Tmp","5"=>":","6"=>$va['jthtmp'], ) ;


            $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                          'opt'=>array('export_name'=>'Kartu Stock') ) ;
            $this->load->library('bospdf', $o) ;
            $this->bospdf->ezText("<b>DETAIL PEMBELIAN STOCK</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vDetail,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "1"=>array("width"=>10,"justification"=>"left"),
                                             "2"=>array("width"=>3,"justification"=>"left"),
                                             "3"=>array("width"=>15,"justification"=>"left"),
                                             "4"=>array("width"=>10,"justification"=>"left"),
                                             "5"=>array("width"=>10,"justification"=>"left"),
                                             "6"=>array("width"=>50,"justification"=>"left"),)
                                        )
                                  ) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($data,"","",
                                   array("fontSize"=>$font,
                                         "cols"=> array(
                                             "#"            =>array("width"=>2,"justification"=>"right"),
                                             "Keterangan"   =>array("wrap"=>1),
                                             "HargaSatuan"  =>array("width"=>12,"justification"=>"right"),
                                             "qty"          =>array("width"=>6,"justification"=>"center"),
                                             "satuan"       =>array("width"=>9,"justification"=>"center"),
                                             "Pembelian"    =>array("width"=>12,"justification"=>"right"),
                                             "Total"        =>array("width"=>12,"justification"=>"right")))
                                  ) ;
            $this->bospdf->ezTable($total,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>1,
                                         "cols"=> array(
                                             "Ket"=>array("justification"=>"center"),
                                             "Jumlah"=>array("width"=>12,"justification"=>"right"),
                                             "Ket2"=>array("width"=>12,"justification"=>"center"),
                                         )
                                        )
                                  ) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "2"=>array("wrap"=>1,"justification"=>"center"),
                                          "3"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "4"=>array("wrap"=>1,"justification"=>"center"),
                                          "5"=>array("width"=>25,"wrap"=>1,"justification"=>"center"))
                                          )
                              ) ;
            $this->bospdf->ezStream() ;
        }else{
            echo('kosong') ;
        }
    }

    public function showreporttotal(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $file = getsession($this, $this->ss . "file") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        if(!empty($data)){
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
            
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> "" ,"5"=>$kota) ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"Mengetahui,","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"(.........................)","2"=>"","3"=>"(.........................)","4"=>"","5"=>"(.........................)") ;
            $vttd[] = array("1"=>"Manager","2"=>"","3"=>"Bag.Keuangan","4"=>"","5"=>"Bag. Pembelian") ;

            $tottotal      = 0 ;
            $totpelunasan      = 0 ;
            $totsaldo      = 0 ;
            foreach ($data as $key => $value){
                $tottotal      += string_2n($value['total']) ;
                $totpelunasan  += string_2n($value['pelunasan']) ;
                $totsaldo      += string_2n($value['saldo']) ;
            }

            $tottotal   = string_2s($tottotal) ;
            $totpelunasan     = string_2s($totpelunasan) ;
            $totsaldo        = string_2s($totsaldo) ;

            $total   = array();
            $total[] = array("Ket"=>"<b>Total",
                             "Total"=>$tottotal,
                             "Pelunasan"=>$totpelunasan,
                             "Saldo"=>$totsaldo."</b>");

            $font = 8 ;
            $o    = array('paper'=>'A4', 'orientation'=>'l', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                          'opt'=>array('export_name'=>'Kartu Stock') ) ;
            $this->load->library('bospdf', $o) ;
            $this->bospdf->ezText("<b>LAPORAN TOTAL HUTANG PEMBELIAN</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("<b>Sampai Tanggal : " .$va['tglakhir'] . "</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($data,"","",
                                   array("fontSize"=>$font,
                                         "cols"=> array(
                                             "no"=>array("caption"=>"No","width"=>4,"justification"=>"right"),
                                             "faktur"=>array("caption"=>"Faktur","width"=>10,"wrap"=>1,"justification"=>"center"),
                                             "tgl"=>array("caption"=>"Tgl","width"=>8,"wrap"=>1,"justification"=>"center"),
                                             "jthtmp"=>array("caption"=>"Jatuh Tempo","width"=>8,"wrap"=>1,"justification"=>"center"),
                                             "supplier"=>array("caption"=>"Supplier","wrap"=>1,"justification"=>"left"),
                                             "fktpo"=>array("caption"=>"PO","width"=>10,"wrap"=>1,"justification"=>"center"),
                                             "total"=>array("caption"=>"Total","width"=>10,"justification"=>"right"),
                                             "pelunasan"=>array("caption"=>"Pelunasan","width"=>10,"justification"=>"right"),
                                             "saldo"=>array("caption"=>"Saldo","width"=>10,"justification"=>"right")
                                         ))
                                  ) ;
            $this->bospdf->ezTable($total,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>1,
                                         "cols"=> array(
                                             "Ket"=>array("justification"=>"center"),
                                             "Total"=>array("width"=>10,"justification"=>"right"),
                                             "Pelunasan"=>array("width"=>10,"justification"=>"right"),
                                             "Saldo"=>array("width"=>10,"justification"=>"right")
                                         )
                                        )
                                  ) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "2"=>array("wrap"=>1,"justification"=>"center"),
                                          "3"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "4"=>array("wrap"=>1,"justification"=>"center"),
                                          "5"=>array("width"=>25,"wrap"=>1,"justification"=>"center"))
                                          )
                              ) ;
            $this->bospdf->ezStream() ;
        }else{
            echo('kosong') ;
        }
    }
}

?>
