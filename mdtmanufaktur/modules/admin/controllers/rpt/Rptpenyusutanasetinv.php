
<?php
class Rptpenyusutanasetinv extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model("rpt/rptpenyusutanasetinv_m") ;
        $this->load->model("func/perhitungan_m") ;
        $this->load->model("func/func_m") ;
        $this->bdb 	= $this->rptpenyusutanasetinv_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptpenyusutan_" ;

    } 

    public function index(){
        $this->load->view("rpt/rptpenyusutanasetinv") ; 

    }   

    public function loadgrid_where($bs, $s){
        $this->duser();

        $arrtgl = explode("-",$bs['periode']);
        $tgl = date('Y-m-d',mktime(0,0,0,$arrtgl[0]+1,0,$arrtgl[1]));

        if($s !== ''){
            $this->db->group_start();
                $this->db->or_like(array('a.kode'=>$s, 'a.keterangan'=>$s));
            $this->db->group_end();
        }

        $this->db->group_start();
        $this->db->or_where('a.tglhabis >', $tgl);
        $this->db->or_where('a.tglhabis','0000-00-00');
        $this->db->group_end();
        
        $this->db->where('a.tglperolehan<=',$tgl);

        
        if($bs['skd_cabang'] !== 'null'){
            $bs['skd_cabang'] = json_decode($bs['skd_cabang'],true);
            $this->db->group_start();
            foreach($bs['skd_cabang'] as $kdc){
                $this->db->or_where('a.cabang',$kdc);
            }
            $this->db->group_end();

        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['cabang'])){
                    $this->db->group_start();
                    foreach($this->aruser['data_var']['cabang'] as $kdc){
                        $this->db->or_where('a.cabang', $kdc);
                    }
                    $this->db->group_end();
                }
            }
        }

        
    }


    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ; 

        // $limit    = $va['offset'].",".$va['limit'] ;
        $search   = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->db->escape_like_str($search) ;  
        savesession($this, $this->ss . "va", json_encode($va) ) ;

        $vare   = array() ;
        $arrtgl = explode("-",$va['periode']);
        $tgl = date('Y-m-d',mktime(0,0,0,$arrtgl[0]+1,0,$arrtgl[1]));
        $n = 0 ;
        $tothp = 0 ;
        $totresidu = 0 ;
        $totpenyawal = 0 ;
        $totpenyblnini = 0 ;
        $totpenyakhir = 0 ;
        $totnilaibuku = 0 ;
        
        $subtothp = 0 ;
        $subtotresidu = 0 ;
        $subtotpenyawal = 0 ;
        $subtotpenyblnini = 0 ;
        $subtotpenyakhir = 0 ;
        $subtotnilaibuku = 0 ;

        $cGol = "";


        $this->loadgrid_where($va, $search);
        $f = "a.kode,a.keterangan,a.golongan,g.keterangan as ketgolongan,a.cabang, c.keterangan as ketcabang,a.lama,a.tglperolehan,a.hargaperolehan,a.unit,
            a.jenispenyusutan,a.tarifpenyusutan,a.residu";
        $dbd = $this->db->select($f)
            ->from("aset a")
            ->join("aset_golongan g","g.kode = a.golongan","left")
            ->join("cabang c","c.kode = a.cabang","left")
            ->order_by('a.golongan asc,a.kode ASC')
            ->get();
       
        foreach($dbd->result_array() as $dbr){
            $vs = $dbr;
            $vs['tglperolehan'] = date_2d($dbr['tglperolehan']);
            $arrpeny = $this->perhitungan_m->getpenyusutan($dbr['kode'],$tgl);
            $vs['hargaperolehan'] = $arrpeny['hargaperolehan'];
            $vs['penyawal'] = $arrpeny['awal'];
            $vs['penyblnini'] = $arrpeny['bulan ini'];
            $vs['penyakhir'] = $arrpeny['akhir'];
            $vs['nilaibuku'] = $vs['hargaperolehan'] - $vs['penyakhir'];
            $lama_bln = $dbr['lama'] * 12;

            $this->db->where('a.kode',$dbr['kode']);
            $this->db->where('a.tgl <',$tgl);
            $this->db->where('a.status',"1");
            $field = "a.*";
            $db2 = $this->db->select($field)->from("aset_perubahan a")
                    ->order_by("a.tgl desc")
                    ->limit("1") 
                    ->get(); 
            $r2  = $db2->row_array();
            if(isset($r2)){
                $d_akhir = json_decode($r2['dataakhir'],true);

                $lama_bln = $d_akhir['lama_bulan_akhir'];
            }
            $vs['lama'] = floor($lama_bln / 12) ." Thn ". $lama_bln % 12 ." Bln";

            $tothp += $vs['hargaperolehan'];
            $totresidu += $vs['residu'];
            $totpenyawal += $vs['penyawal'];
            $totpenyblnini += $vs['penyblnini'];
            $totpenyakhir += $vs['penyakhir'];
            $totnilaibuku += $vs['nilaibuku'];
            
            if($cGol <> $vs['golongan']){
                if($n > 0){
                    $vare[] = array("no" => '', "kode"=> '', "keterangan"=> 'Subtotal',
                                    "cabang"=> '', "golongan"=> '',
                                    "tglperolehan"=> '',"lama"=> '',"hargaperolehan"=> string_2s($subtothp), "unit"=> '',"jenispenyusutan"=> '',
                                    "tarifpenyusutan"=> '',"residu"=>string_2s($subtotresidu),"penyawal"=>string_2s($subtotpenyawal),
                                    "penyblnini"=>string_2s($subtotpenyblnini),"penyakhir"=>string_2s($subtotpenyakhir),
                                    "nilaibuku"=>string_2s($subtotnilaibuku));
                    $vare[] = array("no" => '', "kode"=> '', "keterangan"=> '',
                                "cabang"=> '', "golongan"=> '',"tglperolehan"=> '',"lama"=> '',"hargaperolehan"=> "", "unit"=> '',"jenispenyusutan"=> '',
                                "tarifpenyusutan"=> '',"residu"=>'',"penyawal"=>'',"penyblnini"=>'',
                                "penyakhir"=>'',"nilaibuku"=>'');
                }
                $vare[] = array("no" => '', "kode"=> '', "keterangan"=> '::['.$vs['golongan'].']-'.$vs['ketgolongan'],
                                "cabang"=> '', "golongan"=> '',"tglperolehan"=> '',"lama"=> '',"hargaperolehan"=> "", "unit"=> '',"jenispenyusutan"=> '',
                                "tarifpenyusutan"=> '',"residu"=>'',"penyawal"=>'',"penyblnini"=>'',
                                "penyakhir"=>'',"nilaibuku"=>'');
                $n = 0;
                $subtothp = 0 ;
                $subtotresidu = 0 ;
                $subtotpenyawal = 0 ;
                $subtotpenyblnini = 0 ;
                $subtotpenyakhir = 0 ;
                $subtotnilaibuku = 0 ;
            }
            
            $subtothp += $vs['hargaperolehan'];
            $subtotresidu += $vs['residu'];
            $subtotpenyawal += $vs['penyawal'];
            $subtotpenyblnini += $vs['penyblnini'];
            $subtotpenyakhir += $vs['penyakhir'];
            $subtotnilaibuku += $vs['nilaibuku'];

            $vs['no'] = ++$n ;
            $cGol = $vs['golongan'];                        
            
            $vs['hargaperolehan'] = string_2s($vs['hargaperolehan']);
            $vs['residu'] = string_2s($vs['residu']);
            $vs['penyawal'] = string_2s($vs['penyawal']);
            $vs['penyblnini'] = string_2s($vs['penyblnini']);
            $vs['penyakhir'] = string_2s($vs['penyakhir']);
            $vs['nilaibuku'] = string_2s($vs['nilaibuku']);
            $vare[]		= $vs ;

        }

        
        if($n > 0){
            $vare[] = array("no" => '', "kode"=> '', "keterangan"=> 'Subtotal',
                            "cabang"=> '', "golongan"=> '',
                            "tglperolehan"=> '',"lama"=> '',"hargaperolehan"=> string_2s($subtothp), "unit"=> '',"jenispenyusutan"=> '',
                            "tarifpenyusutan"=> '',"residu"=>string_2s($subtotresidu),"penyawal"=>string_2s($subtotpenyawal),
                            "penyblnini"=>string_2s($subtotpenyblnini),"penyakhir"=>string_2s($subtotpenyakhir),
                            "nilaibuku"=>string_2s($subtotnilaibuku));
        }
        $vare[] = array("no" => '', "kode"=> '', "keterangan"=> 'TOTAL', "cabang"=> '', "golongan"=> '',
                        "tglperolehan"=> '',"lama"=> '',"hargaperolehan"=> string_2s($tothp), "unit"=> '',"jenispenyusutan"=> '',
                        "tarifpenyusutan"=> '',"residu"=>string_2s($totresidu),"penyawal"=>string_2s($totpenyawal),
                        "penyblnini"=>string_2s($totpenyblnini),"penyakhir"=>string_2s($totpenyakhir),
                        "nilaibuku"=>string_2s($totnilaibuku),"w2ui"=>array("summary"=>true));
        $vare 	= array("total"=>count($vare) - 1, "records"=>$vare ) ;
        $varpt = $vare['records'] ;
        echo(json_encode($vare)) ; 
        savesession($this, "rptpenyusutanasetinv_rpt", json_encode($varpt)) ; 
    }

    public function init(){
        savesession($this, "ssrptpenyusutanasetinv_id", "") ;    
    }


    public function showreport(){
        $data = getsession($this,"rptpenyusutanasetinv_rpt") ;      
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;     
        $data = json_decode($data,true) ;      
        if(!empty($data)){  
            $font = 8 ;
            $o    = array('paper'=>'A4', 'orientation'=>'landscape', 'export'=>"0",
                          'opt'=>array('export_name'=>'DaftarJurnal_' . getsession($this, "username") ) ) ;
            $this->load->library('bospdf', $o) ;   


            if($va['skd_cabang'] !== 'null'){
                $cabang = json_decode($va['skd_cabang'],true);
            }else{
                if($this->aruser['level'] !== '0000'){
                    if(is_array($this->aruser['data_var']['cabang'])){
                        $cabang = $this->aruser['data_var']['cabang'];
                    }
                }else{
                    $dbd2 = $this->db->select("kode")->from("cabang")->get();
                    
                    foreach($dbd2->result_array() as $r2){
                        $cabang[] = $r2['kode'];
                    }
                }
            }

            $arrcab = $this->func_m->GetDataCabang($cabang[0]);
            
            if(count($cabang) == 1){
                $this->bospdf->ezText($arrcab['nama'],$font+4,array("justification"=>"center")) ;
            }else{
                $cabkonsol = "(".implode(",",$cabang).")";
                $this->bospdf->ezText($this->bdb->getconfig("app_company"),$font+4,array("justification"=>"center")) ;
                $this->bospdf->ezText("Konsolidasi : ".$cabkonsol,$font,array("justification"=>"center")) ;
            }
            $this->bospdf->ezText($arrcab['alamat'] . " / ". $arrcab['telp'],$font,array("justification"=>"center")) ;
            $this->bospdf->ezText("<b>LAPORAN DAFTAR ASET & PENYUSUTAN</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("<b>Periode: ".$va['periode']."</b>",$font+2,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ; 
            $this->bospdf->ezTable($data,"","",  
                                   array("fontSize"=>$font,"cols"=> array( 
                                       "no"=>array("caption"=>"No","width"=>3,"justification"=>"right"),
                                       "kode"=>array("caption"=>"Kode","width"=>5,"justification"=>"center"),
                                       "keterangan"=>array("caption"=>"Keterangan","wrap"=>1),
                                       "cabang"=>array("caption"=>"Cabang","width"=>3),
                                       "golongan"=>array("caption"=>"Golongan","width"=>3),
                                       "tglperolehan"=>array("caption"=>"Tgl Perolehan","width"=>8,"justification"=>"center"),
                                       "lama"=>array("caption"=>"Lama","width"=>8,"justification"=>"right","wrap"=>1),
                                       "hargaperolehan"=>array("caption"=>"Harga Perolehan","width"=>8,"justification"=>"right"),
                                       "unit"=>array("caption"=>"Unit","width"=>3,"justification"=>"right"),
                                       "jenispenyusutan"=>array("caption"=>"Jenis Peny.","width"=>3,"justification"=>"left"),
                                       "tarifpenyusutan"=>array("caption"=>"Tarif Peny.","width"=>3,"justification"=>"right"),
                                       "residu"=>array("caption"=>"N. Residu","width"=>8,"justification"=>"right"),
                                       "penyawal"=>array("caption"=>"Peny. Awal","width"=>8,"justification"=>"right"),
                                       "penyblnini"=>array("caption"=>"Peny. Bln Ini","width"=>8,"justification"=>"right"),
                                       "penyakhir"=>array("caption"=>"Peny. Akhir","width"=>8,"justification"=>"right"),
                                       "nilaibuku"=>array("caption"=>"N. Buku","width"=>8,"justification"=>"right")))) ;   
            //print_r($data) ;    
            $this->bospdf->ezStream() ; 
        }else{
            echo('data kosong') ;
        }
    }


}
?>
