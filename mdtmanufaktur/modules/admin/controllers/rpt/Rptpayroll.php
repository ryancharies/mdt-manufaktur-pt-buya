<?php
class Rptpayroll extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("rpt/rptpayroll_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->load->model("func/updtransaksi_m") ;
        $this->load->model("func/func_m") ;
        
        $this->bdb 	= $this->rptpayroll_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptpayroll2_" ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";


        $arrkolom = array();
        /*$arrkolom[] = array("field"=> "kode", "caption"=> "NIP", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "nama", "caption"=> "Nama", "size"=> "200px", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "bagian", "caption"=> "Bagian", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "jabatan", "caption"=> "Jabatan", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        

        $kodepinjaman = $this->rptpayroll_m->getconfig("kogapypinjkaryawan");
        $where = "(periode = 'J' or periode = 'H') and status = '1' and perhitungan <> 'K'";
        $dbd = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
        while($dbr = $this->rptpayroll_m->getrow($dbd)){
            $arrkolom[] = array("field"=> "hk_".$dbr['kode'], "caption"=> "HK. ".$dbr['keterangan'], "size"=> "70px", "render"=> "float:2", "sortable"=>false);    
        }
        
        $kodepinjaman = $this->rptpayroll_m->getconfig("kogapypinjkaryawan");
        $where = "(periode = 'J' or periode = 'H') and status = '1' and perhitungan <> 'K'";
        $dbd = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
        while($dbr = $this->rptpayroll_m->getrow($dbd)){
            $arrkolom[] = array("field"=> "uh_".$dbr['kode'], "caption"=> "UH. ".$dbr['keterangan'], "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        }

        $where = "(periode = 'J' or periode = 'H' or periode = 'B') and status = '1' and perhitungan <> 'K'";
        $dbd = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
        while($dbr = $this->rptpayroll_m->getrow($dbd)){
            $arrkolom[] = array("field"=> "ub_".$dbr['kode'], "caption"=> "UB. ".$dbr['keterangan'], "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        }

        $arrkolom[] = array("field"=> "gajikotor", "caption"=> "Upah Kotor", "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        $arrkolom[] = array("field"=> "pinjamankaryawan", "caption"=> "BON", "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        $arrkolom[] = array("field"=> "gajibersih", "caption"=> "Upah Bersih", "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        $arrkolom[] = array("field"=> "cetakslip", "caption"=> "", "size"=> "100px", "sortable"=>false);
        $arrkolom[] = array("field"=> "rekening", "caption"=> "Rekening", "size"=> "100px", "sortable"=>false);
        */
        $data['kolomgrid1'] = json_encode($arrkolom);
        $this->load->view("rpt/rptpayroll",$data) ;

    } 

    public function initgrid1(){

        $va = $this->input->post() ;
        
        //cek status sudah posting atau belum
        $sudahposting = false;
        $cabang = getsession($this, "cabang");
        $dbd2 = $this->rptpayroll_m->select("payroll_posting","faktur","periode = '{$va['periode']}' and cabang = '$cabang' and status = '1'");
        if($this->rptpayroll_m->rows($dbd2) > 0){
            $sudahposting = true;
        }
        
        //$sudahposting = true;
        if($sudahposting){
            $arrkolom = $this->perhitunganhrd_m->getkolomsudahposting($va['periode']);
        }else{
            $arrkolom = $this->perhitunganhrd_m->getkolombelumposting();
        }
        
        
        $arrkolom = json_encode($arrkolom);
        echo('
            bos.rptpayroll.grid1_col = '.$arrkolom.';

            bos.rptpayroll.loadpembayaran();
            bos.rptpayroll.grid1_destroy() ;
            bos.rptpayroll.grid1_loaddata();
            bos.rptpayroll.grid1_load() ;
        ');
    }

    public function loadgrid(){
        $va = json_decode($this->input->post('request'), true);
        //print_r($va);
        $vare   = array() ;
        $vdb    = $this->rptpayroll_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;

        //cek status sudah posting atau belum
        $sudahposting = false;
        $cabang = getsession($this, "cabang");
        $dbd2 = $this->rptpayroll_m->select("payroll_posting","faktur","periode = '{$va['periode']}' and cabang = '$cabang' and status = '1'");
        if($this->rptpayroll_m->rows($dbd2) > 0){
            $sudahposting = true;
        }

        //load data komponen payroll
        $datakomp = array();
        $kodepinjaman = $this->bdb->getconfig("kogapypinjkaryawan");

        if($sudahposting){
            $where = "k.kode <> '$kodepinjaman' and t.status = '1' and t.periode = '{$va['periode']}'";//(k.periode = 'J' or k.periode = 'H') and 
            $join = "left join payroll_komponen k on k.kode = p.payroll left join payroll_posting t on t.faktur = p.faktur";
            $dbd2 = $this->rptpayroll_m->select("payroll_posting_komponen p","k.kode,k.keterangan,k.periode,k.perhitungan,k.potongan",$where,$join,"k.kode");
            while($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
                $datakomp[$dbr2['kode']] = $dbr2;
            }
        }else{  
            $where = "status = '1' and kode <> '$kodepinjaman'";
            $dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
            while($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
                $datakomp[$dbr2['kode']] = $dbr2;
            }
        }

        $total = array();
        $total['recid'] = "ZZZZZ" ;
        $total['nama'] = "Total" ;
        $total['gajibersih'] = 0 ;
        $total['pinjamankaryawan'] = 0 ;
        $total['gajikotor'] = 0 ;
        while( $dbr = $this->rptpayroll_m->getrow($dbd) ){
            

            //jab bag
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$va['tglakhir']);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['kode'],$va['tglakhir']);
            
            $ltampil = true;

            //cek bagian
            if(trim($va['bagian']) <> ""){
                $ltampil = false;
                if(trim($va['bagian']) == $bagian['bagian'])$ltampil = true;
            }

            if($ltampil){
                $n++;
            
                $vaset = $dbr;
                $gajikotor = 0 ;
                $vaset['recid'] = $n;
                $vaset['jabatan'] = $jabatan['keterangan'];
                $vaset['bagian'] = $bagian['keterangan'];
                $vaset['rekening'] = $dbr['rekening'];
                
                // $where = "status = '1' and perhitungan <> 'K'";
                // $dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
                // while($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
                foreach($datakomp as $key => $dbr2){
                    if($sudahposting){
                        $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawanposting($dbr['kode'],$dbr2['kode'],$va['periode']);
                    // print_r($payroll);
                    }else{
                        $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawan($dbr['kode'],$dbr2['kode'],$va['periode']);
                    }
                    

                    if($dbr2['potongan'] == "1"){
                        $payroll['nominal'] *=-1;
                        $payroll['total'] *=-1;
                    }
                    if($dbr2['periode'] == "J" or $dbr2['periode'] == "H"){
                        $vaset["hk_".$dbr2['kode']] = $payroll['jmlskala'];    
                        $vaset["uh_".$dbr2['kode']] = $payroll['nominal'];              
                                    
                    }
                    $vaset["ub_".$dbr2['kode']] = $payroll['total']; 
                    
                    if(!isset($total["ub_".$dbr2['kode']]))$total["ub_".$dbr2['kode']] = 0 ;
                    $total["ub_".$dbr2['kode']] += $vaset["ub_".$dbr2['kode']];
                    
                    $gajikotor += $payroll['total'] ;
                    
                }

                $vaset['gajikotor'] = $gajikotor;

                //cek pinjaman karaywan
                if($sudahposting){
                    $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjamanposting($dbr['kode'],$va['periode']);
                }else{
                    $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjaman($dbr['kode'],$va['periode']);
                }
                
                $vaset['pinjamankaryawan'] = $kasbon['total'] * -1;
                $vaset['gajibersih'] = $gajikotor - $kasbon['total'];
                $vaset['cetakslip'] = "Tidak Cetak Slip";
                $vare[]		= $vaset ;

                $total['gajikotor'] += $vaset['gajikotor'];
                $total['pinjamankaryawan'] += $vaset['pinjamankaryawan'];
                $total['gajibersih'] += $vaset['gajibersih'];
            }
        }
        $total['w2ui']= array("summary"=> true);
        $vare[] = $total;

        $vare = array("total" => $n, "records" => $vare);
        echo (json_encode($vare));
    }

    public function init(){
        savesession($this, "ssrptpayroll_kode", "") ;
    }

    public function seekperiode(){
        $search     = $this->input->get('q');
        $vdb    = $this->rptpayroll_m->seekperiode($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->rptpayroll_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ". $dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
   }

    public function seekbagian(){
        $search     = $this->input->get('q');
        $vdb    = $this->rptpayroll_m->seekbagian($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->rptpayroll_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'],"text"=> $dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

   public function loadperiode(){
        $va 	= $this->input->post() ;
        $vdb    = $this->rptpayroll_m->getperiode($va) ;
        if( $dbr = $this->rptpayroll_m->getrow($vdb['db']) ){
            $status = "-- Belum diposting --";
            $faktur = "";
            $cabang = getsession($this, "cabang");
            $dbd2 = $this->rptpayroll_m->select("payroll_posting","faktur","periode = '{$va['periode']}' and cabang = '$cabang' and status = '1'");
            if($this->rptpayroll_m->rows($dbd2) > 0){
                $status = "Sudah diposting";
                if($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
                    $faktur = $dbr2['faktur'];
                }
            }
            echo('
                with(bos.rptpayroll.obj){
                    find("#tglawal").val("'.date_2d($dbr['tglawal']).'") ;
                    find("#tglakhir").val("'.date_2d($dbr['tglakhir']).'") ;
                    find("#status").val("'.$status.'") ;
                    find("#faktur").val("'.$faktur.'") ;
                }
            ') ;
            
            
             
        }
   }

    public function loadpembayaran(){
        $va 	= $this->input->post() ;
        $cabang = getsession($this, "cabang");
        $faktur = "";
        $total = 0;
        $dbd2 = $this->rptpayroll_m->select("payroll_posting","faktur","periode = '{$va['periode']}' and cabang = '$cabang' and status = '1'");
        if($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
            $faktur = $dbr2['faktur'];

            //loadgrid pembayaran
            $vare = array();
            $dbd = $this->rptpayroll_m->getpembayaran($faktur) ;
            $n = 0 ;
            
            while( $dbr = $this->rptpayroll_m->getrow($dbd) ){
                $n++;
                $vaset   = $dbr ;
                $vaset['recid'] = $n;
                $vaset['no'] = $n;

                $vare[]             = $vaset ;
                $total += $dbr['nominal'];
            }

            
        }
        $vare[] = array("recid"=>"ZZZZ","no"=>"","kode"=>"","ketketerangan"=>"Total","nominal"=>$total,"w2ui"=>array("summary"=>true));

        $vare = json_encode($vare);
            echo('
                w2ui["bos-form-rptpayroll_grid2"].add('.$vare.');
                
           
        ');
    }

    public function initreport(){
        $n=0;
        $va     = $this->input->post() ;
        
        savesession($this, $this->ss . "va", json_encode($va) ) ;

        //print_r($va);
        $vare   = array() ;
        $vdb    = $this->rptpayroll_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;
        
        //cek status sudah posting atau belum
        $sudahposting = false;
        $cabang = getsession($this, "cabang");
        $dbd2 = $this->rptpayroll_m->select("payroll_posting","faktur","periode = '{$va['periode']}' and cabang = '$cabang' and status = '1'");
        if($this->rptpayroll_m->rows($dbd2) > 0){
            $sudahposting = true;
        }

        $kodepinjaman = $this->bdb->getconfig("kogapypinjkaryawan");

        //load data komponen payroll
        $datakomp = array();
        if($sudahposting){
            $where = "p.payroll <> '$kodepinjaman' and t.status = '1' and t.periode = '{$va['periode']}'";//(k.periode = 'J' or k.periode = 'H') and 
            $join = "left join payroll_komponen k on k.kode = p.payroll left join payroll_posting t on t.faktur = p.faktur";
            $dbd2 = $this->rptpayroll_m->select("payroll_posting_komponen p","k.kode,k.keterangan,k.periode,k.perhitungan,k.potongan",$where,$join,"k.kode");
            while($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
                $datakomp[$dbr2['kode']] = $dbr2;
            }
        }else{  
            $where = "status = '1' and kode <> '$kodepinjaman'";
            $dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
            while($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
                $datakomp[$dbr2['kode']] = $dbr2;
            }
        }

        while( $dbr = $this->rptpayroll_m->getrow($dbd) ){
            $n++;

            //jab bag
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$va['tglakhir']);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['kode'],$va['tglakhir']);
            $vaset = array();
            $gajikotor = 0 ;
            $vaset['no'] = $n;
            $vaset['nip'] = $dbr['kode'];
            $vaset['nama'] = $dbr['nama'];            
            $vaset['jabatan'] = $jabatan['keterangan'];
            $vaset['bagian'] = $bagian['keterangan'];
            $arrhk  = array();
            $arruh  = array();
            $arrub  = array();
            
            // $kodepinjaman = $this->rptpayroll_m->getconfig("kogapypinjkaryawan");
            // $where = "status = '1' and perhitungan <> 'K'";
            // $dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
            // while($dbr2 = $this->rptpayroll_m->getrow($dbd2)){

            foreach($datakomp as $key => $dbr2){
                if($sudahposting){
                    $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawanposting($dbr['kode'],$dbr2['kode'],$va['periode']);
                   // print_r($payroll);
                }else{
                    $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawan($dbr['kode'],$dbr2['kode'],$va['periode']);
                }
                

                if($dbr2['potongan'] == "1"){
                    $payroll['nominal'] *=-1;
                    $payroll['total'] *=-1;
                }
                if($dbr2['periode'] == "J" or $dbr2['periode'] == "H"){
                    $arrhk["HK ".$dbr2['keterangan']] = $payroll['jmlskala'];    
                    $arruh["UH ".$dbr2['keterangan']] = $payroll['nominal'];            
                }
                $arrub["UB ".$dbr2['keterangan']] = $payroll['total']; 
                
                $gajikotor += $payroll['total'] ;
                
            }

            //hk
            foreach($arrhk as $key => $val){
                $vaset[$key] = $val;
            }

            //uh
            foreach($arruh as $key => $val){
                $vaset[$key] = $val;
            }

            //ub
            foreach($arrub as $key => $val){
                $vaset[$key] = $val;
            }

            $vaset['gajikotor'] = $gajikotor;

            //cek pinjaman karaywan
            if($sudahposting){
                $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjamanposting($dbr['kode'],$va['periode']);
            }else{
                $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjaman($dbr['kode'],$va['periode']);
            }
            
            $vaset['pinjamankaryawan'] = $kasbon['total'] * -1;
            $vaset['pinjamankaryawan'] = $vaset['pinjamankaryawan'];
            $vaset['gajibersih'] = $gajikotor - $kasbon['total'];
            $vaset['gajibersih'] = $vaset['gajibersih'];
            $vare[]		= $vaset ;   
        }
        savesession($this, "rptpayroll_rpt", json_encode($vare)) ; 
        echo(' bos.rptpayroll.openreport() ; ') ;
    }

    public function showreport(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        //print_r($va);
        $data = getsession($this,"rptpayroll_rpt") ;      
        $data = json_decode($data,true) ;

        $sumbag = array();
        //menyusun total
        $arrtot = array();
        $arrtot['ket'] = "<b>Total";
        foreach($data as $key => $val){
            
            //hitung smmary bagian
            if(!isset($sumbag[$val['bagian']])){
                $sumbag[$val['bagian']] = array("keterangan"=>$val['bagian'],"gajikotor"=>0);
            }
            

            $sumbag[$val['bagian']]['gajikotor'] += string_2n($val['gajikotor']);
            
            //hitung summary semua
            foreach($val as $key2 => $val2){
                $jenis = substr($key2,0,2); 
                
                if($jenis == "HK" || $jenis == "UH" || $jenis == "UB" || $key2 == "gajikotor" || $key2 == "pinjamankaryawan" || $key2 == "gajibersih"){
                    
                    if($jenis == "UB" || $key2 == "gajikotor" || $key2 == "pinjamankaryawan" || $key2 == "gajibersih"){
                        if(!isset($arrtot[$key2]))$arrtot[$key2] = 0 ;
                            $arrtot[$key2] += string_2n($val2);
                    }
                    $data[$key][$key2] = string_2s($val2);
                }
            }
        }

        //number 2 string total
        foreach($arrtot as $key => $val){
            if($key <> "ket")$arrtot[$key] = string_2s($val);
            if($key <> "gajibersih")$arrtot[$key] .= "</b>";
        }
        $total[] = $arrtot;

        //menyusun kolom
        $arrcoltot = array();
        $arrcoltot['ket'] = array("caption"=>"Nama","justification"=>"center");

        $arrcoldata = array();
        $arrcoldata['no'] = array("caption"=>"No","width"=>4,"justification"=>"right");
        $arrcoldata['nip'] = array("caption"=>"NIP","width"=>6,"justification"=>"center");
        $arrcoldata['nama'] = array("caption"=>"Nama","justification"=>"left");
        $arrcoldata['jabatan'] = array("caption"=>"Jabatan","justification"=>"left");
        $arrcoldata['bagian'] = array("caption"=>"Bagian","justification"=>"left");
        $kodepinjaman = $this->rptpayroll_m->getconfig("kogapypinjkaryawan");
        $where = "status = '1' and kode <> '$kodepinjaman'";
        $dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
        while($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
            $arrcoldata["HK ".$dbr2['keterangan']] = array("caption"=>"Hari Kerja;".$dbr2['keterangan'],"width"=>4,"justification"=>"right");    
            $arrcoldata["UH ".$dbr2['keterangan']] = array("caption"=>"Upah Harian;".$dbr2['keterangan'],"width"=>6,"justification"=>"right");          
            $arrcoldata["UB ".$dbr2['keterangan']] = array("caption"=>"Upah Bulan;".$dbr2['keterangan'],"width"=>6,"justification"=>"right");
            
            $arrcoltot["UH ".$dbr2['keterangan']] = array("caption"=>"Upah Harian;".$dbr2['keterangan'],"width"=>6,"justification"=>"right");          
            $arrcoltot["UB ".$dbr2['keterangan']] = array("caption"=>"Upah Bulan;".$dbr2['keterangan'],"width"=>6,"justification"=>"right");
           
        }
        $arrcoldata["gajikotor"] = array("caption"=>"Gaji Kotor","width"=>6,"justification"=>"right");
        $arrcoldata["pinjamankaryawan"] = array("caption"=>"Pinj. Karyawan","width"=>6,"justification"=>"right");
        $arrcoldata["gajibersih"] = array("caption"=>"Gaji ersihB","width"=>6,"justification"=>"right");

        $arrcoltot["gajikotor"] = array("caption"=>"Gaji Kotor","width"=>6,"justification"=>"right");
        $arrcoltot["pinjamankaryawan"] = array("caption"=>"Pinj. Karyawan","width"=>6,"justification"=>"right");
        $arrcoltot["gajibersih"] = array("caption"=>"Gaji Bersih","width"=>6,"justification"=>"right");

        //menghitung total sumbag
        $totsumbag = array();
        $totsumbag[0] = array("keterangan"=>"<b>Total","gajikotor"=>0);
        foreach($sumbag as $key => $val){
            $totsumbag[0]['gajikotor'] += string_2n($val['gajikotor']);
            $sumbag[$key]['gajikotor'] = string_2s($val['gajikotor']);
        }
        $totsumbag[0]['gajikotor'] = string_2s($totsumbag[0]['gajikotor'])."</b>";


        if(!empty($data)){ 
            $font = 6 ;
            if(!isset($va['export']) || $va['export'] == "")$va['export'] = 0 ;
            $o    = array('paper'=>'A4', 'orientation'=>'l', 'export'=>$va['export'],
                          'opt'=>array('export_name'=>'DaftarGaji_' . getsession($this, "username") ) ) ;
            $this->load->library('bospdf', $o) ;   
            $cabang = getsession($this,"cabang");
            $arrcab = $this->func_m->GetDataCabang($cabang);
            $this->bospdf->ezText($arrcab['kode'] ." - ".$arrcab['nama'],$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText($arrcab['alamat'] . " / ". $arrcab['telp'],$font,array("justification"=>"center")) ;
            $this->bospdf->ezText("<b>Daftar Gaji</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("Periode : ".date_2d($va['tglawal']). " sd " .date_2d($va['tglakhir']),$font+2,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ; 
            $this->bospdf->ezTable($data,"","",array("showLines"=>2,"showHeadings"=>"1","fontSize"=>$font,"cols"=> $arrcoldata)) ;   
            $this->bospdf->ezTable($total,"","",array("showLines"=>2,"showHeadings"=>"","fontSize"=>$font,"cols"=> $arrcoltot)) ;
            
            $this->bospdf->ezText("") ; 
            $this->bospdf->ezText("") ; 
            $this->bospdf->ezTable($sumbag,"","",array("showLines"=>2,"showHeadings"=>"1","fontSize"=>$font,"cols"=>array(
                                    "keterangan"=>array("caption"=>"Bagian","width"=>20,"justification"=>"left"),
                                    "gajikotor"=>array("caption"=>"Gaji Kotor","width"=>12,"justification"=>"right")
            ))) ;
            $this->bospdf->ezTable($totsumbag,"","",array("showLines"=>2,"showHeadings"=>"","fontSize"=>$font,"cols"=>array(
                "keterangan"=>array("caption"=>"Bagian","width"=>20,"justification"=>"left"),
                "gajikotor"=>array("caption"=>"Gaji Kotor","width"=>12,"justification"=>"right")
            ))) ;

            //print_r($data) ;    
            $this->bospdf->ezStream() ; 
        }else{
            echo('data kosong') ;
        }
    }

    public function printslip(){
        $this->load->library('m_pdf');
        $va     = $this->input->post() ;
        $jmlkompmax = 60;
        
        $pdfFilePath = "./tmp/".md5("rpslip-".date_now()).".pdf";
        $cDownloadPath = $pdfFilePath ;
        //$this->m_pdf->pdf->AddPage('L');

        //cek status sudah posting atau belum
        $ketperiode = $this->bdb->getval("keterangan", "kode = '{$va['periode']}'", "sys_periode_payroll");
        $sudahposting = false;
        $cabang = getsession($this, "cabang");
        $dbd2 = $this->rptpayroll_m->select("payroll_posting","faktur","periode = '{$va['periode']}' and cabang = '$cabang' and status = '1'");
        if($this->rptpayroll_m->rows($dbd2) > 0){
            $sudahposting = true;
        }
        //print_r($va['grid1']);
        $vaGrid = json_decode($va['grid1']);
       //print_r($vaGrid);
        foreach($vaGrid as $key => $val){
            //echo($val->cetakslip);
            if($val->cetakslip == "Ya"){
                //load data karaywan
                $dbd    = $this->rptpayroll_m->getdatakaryawan($val->kode) ;
                //echo($val->kode);
                //$dbd    = $vdb['db'] ;
                $n = 0 ;
                
                $gajikotor = 0 ;
                while( $dbr = $this->rptpayroll_m->getrow($dbd) ){
                    //print_r($dbr);
                    $arrcab = $this->func_m->GetDataCabang($cabang);
                    $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$va['tglakhir']);
                    
                    $jmlkomp = 0;
                    $html = "";
                    $html .= '<table width = "100%" height = "100%"><tr>';
                    for($cp = 1 ; $cp <=2 ;$cp++){
                        $html .= '<td align="center" height = "100%" width = 50% style = "border:1px solid black;">';
                            
                            $html .= '<h4 align="center">'.$arrcab['nama'].'</h4>';
                            $html .= '<h5 align="center">'.$arrcab['alamat'].'</h5>';
                            $html .= '<h5 align="center">TELP. '.$arrcab['telp'].'</h5>';
                            $html .= '<hr />';
                            $html .= '<h5 align="center">SLIP GAJI</h5>';
                            $html .= '<h5 align="center">'.$ketperiode.'</h5>';
                            $html .= '<table width = "100%" border="0">';
                                $html .= '<tr><td><table width = "100%">';
                                
                                    $html .= '  <tr>
                                                    <td style="font-size:13pxtext-align:center;">Nama/Jabatan</td>
                                                    <td style="font-size:13pxtext-align:center;">'.$dbr['nama'].'</td>
                                                    <td style="font-size:13pxtext-align:center;" colspan = "2">'.$jabatan['keterangan'].'</td>
                                                    <td></td></tr>';
                                    $html .= '  <tr>
                                                    <td style="font-size:13pxtext-align:center;"></td>
                                                    <td style="font-size:13pxtext-align:center;"></td>
                                                    <td style="font-size:13pxtext-align:center;" colspan = "2"></td>
                                                    <td></td></tr>';
                                    $html .= '  <tr>
                                                    <td style="font-size:13pxtext-align:center;"></td>
                                                    <td style="font-size:13pxtext-align:center;"></td>
                                                    <td style="font-size:13pxtext-align:center;" colspan = "2"></td>
                                                    <td></td></tr>';
                                    
                                    //list penambahan
                                    $html .= '  <tr><td style="font-size:10;text-align:left;">::Penambahan::</td><td></td><td></td><td></td><td></td></tr>';
                                    $totpenambahan = 0 ;
                                    $kodepinjaman = $this->rptpayroll_m->getconfig("kogapypinjkaryawan");
                                    $where = "status = '1' and kode <> '$kodepinjaman' and potongan = '0'";
                                    $dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
                                    while($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
                                        if($sudahposting){
                                            $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawanposting($dbr['kode'],$dbr2['kode'],$va['periode']);
                                        // print_r($payroll);
                                        }else{
                                            $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawan($dbr['kode'],$dbr2['kode'],$va['periode']);
                                        }
                                        
                                        $gajikotor += $payroll['total'] ;

                                        if($payroll['total'] > 0){
                                            $html .= '  <tr>
                                                    <td style="font-size:13pxtext-align:left;">'.$dbr2['keterangan'].'</td>
                                                    <td style="font-size:13pxtext-align:center;">'.$payroll['jmlskala'].' x &nbsp; Rp.'.string_2s($payroll['nominal']).'</td>
                                                    <td style="font-size:13pxtext-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:13pxtext-align:right;"> '.string_2s($payroll['total']).'</td>
                                                    <td></td></tr>';
                                            $jmlkomp++;
                                        }
                                        $totpenambahan += $payroll['total'];
                                    }

                                    

                                    


                                    $html .= '  <tr><td></td><td><hr align="right"></td><td><hr align="right"></td><td><hr align="right"></td><td></td></tr>';
                                    //tot penambahan
                                    $html .= '  <tr><td></td><td></td>
                                                    <td style="font-size:13pxtext-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:13pxtext-align:right;">'.string_2s($totpenambahan).'</td>
                                                    <td></td></tr>';


                                    //list potongan
                                    $html .= '  <tr><td style="font-size:10;text-align:left;">::Potongan::</td><td></td><td></td><td></td><td></td></tr>';
                                    $totpotongan = 0 ;
                                    $kodepinjaman = $this->rptpayroll_m->getconfig("kogapypinjkaryawan");
                                    $where = "status = '1' and kode <> '$kodepinjaman' and potongan = '1'";
                                    $dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
                                    while($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
                                        if($sudahposting){
                                            $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawanposting($dbr['kode'],$dbr2['kode'],$va['periode']);
                                        // print_r($payroll);
                                        }else{
                                            $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawan($dbr['kode'],$dbr2['kode'],$va['periode']);
                                        }
                                        
                                        $gajikotor += $payroll['total'] ;

                                        if($payroll['total'] > 0){
                                            $html .= '  <tr>
                                                    <td style="font-size:13pxtext-align:left;">'.$dbr2['keterangan'].'</td>
                                                    <td style="font-size:13pxtext-align:center;">'.$payroll['jmlskala'].' x &nbsp; Rp.'.string_2s($payroll['nominal']).'</td>
                                                    <td style="font-size:13pxtext-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:13pxtext-align:right;"> '.string_2s($payroll['total']).'</td>
                                                    <td></td></tr>';
                                            $jmlkomp++;
                                        }
                                        $totpotongan += $payroll['total'];
                                    }
                                    
                                    //cek pinjaman karaywan
                                    if($sudahposting){
                                        $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjamanposting($dbr['kode'],$va['periode']);
                                    }else{
                                        $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjaman($dbr['kode'],$va['periode']);
                                    }

                                    if($kasbon['total'] > 0){
                                        $html .= '  <tr>
                                                    <td style="font-size:13pxtext-align:left;">Pinjaman Karyawan</td>
                                                    <td style="font-size:13pxtext-align:center;"> 1 x &nbsp; Rp.'.string_2s($kasbon['total']).'</td>
                                                    <td style="font-size:13pxtext-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:13pxtext-align:right;"> '.string_2s($kasbon['total']).'</td>
                                                    <td></td></tr>';
                                        $jmlkomp++;
                                    }
                                    $totpotongan += $kasbon['total'];
                                    
                                    $html .= '  <tr><td></td><td><hr align="right"></td><td><hr align="right"></td><td><hr align="right"></td><td></td></tr>';
                                    //tot pengurangan
                                    
                                    $html .= '  <tr><td></td><td></td>
                                                    <td style="font-size:13pxtext-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:13pxtext-align:right;">'.string_2s($totpotongan).'</td>
                                                    <td></td></tr>';
                                    $html .= '  <tr><td></td><td><hr align="right"></td><td><hr align="right"></td><td><hr align="right"></td><td><hr align="right"></td></tr>';
                                    
                                    $gajibersih = $totpenambahan - $totpotongan;
                                    //tot gaji
                                    $html .= '  <tr><td></td><td></td>
                                                    <td style="font-size:13pxtext-align:left;">&nbsp;=&nbsp;</td>
                                                    <td></td>
                                                    <td style="font-size:13pxtext-align:right;">Rp. '.string_2s($gajibersih).'</td>
                                                    </tr>';
                                
                                $html .= '</table></td></tr>';
                                $html .= '<tr><td><table width = "100%">';
                                    $html .= '  <tr><td></td><td></td></tr>';
                                    
                                    if($jmlkompmax > $jmlkomp){
                                        $selisih = $jmlkompmax - $jmlkomp;
                                        for($a=0;$a<=$selisih;$a++){
                                            $html .= '  <tr><td></td><td></td></tr>';
                                        }
                                    }
                                    //ttd
                                    $html .= '  <tr><td style="font-size:13pxtext-align:center;">Kudus,'.$va['tglakhir'].'</td><td></td></tr>';
                                    $html .= '  <tr><td align="center"></td><td></td></tr>';
                                    $html .= '  <tr><td align="center"></td><td></td></tr>';
                                        
                                    $html .= '  <tr><td style="font-size:13pxtext-align:center;">Bag. Keuangan</td>
                                                    <td style="font-size:13pxtext-align:center;">Karyawan</td></tr>';
                                    for($a=1;$a<=10;$a++){
                                        $html .= '  <tr><td></td><td></td></tr>';
                                    }
                                    
                                    $html .= '  <tr><td style="font-size:13pxtext-align:center;"><u>REFINDA BERLIANA</u></td>
                                                    <td style="font-size:13pxtext-align:center;"><u>'.$dbr['nama'].'</u></td></tr>';

                                $html .= '</table></td></tr>';                  
                            
                            $html .= '</table>';
                        $html .= '</td>';

                    }   
                    $html .= '</tr></table>';

                    $this->m_pdf->pdf->AddPageByArray([
                        'margin-left' => 0,
                        'margin-right' => 0,
                        'margin-top' => 0,
                        'margin-bottom' => 0,
                        'sheet-size' => 'A4-L'
                    ]);

                    $this->m_pdf->pdf->WriteHTML($html);
                    

                    
                }
            }
        }
        $this->m_pdf->pdf->Output(FCPATH.$pdfFilePath,'F');
        echo("
                bos.rptpayroll.showReport('".$pdfFilePath."');
            ");
    }

    public function printslipa5_lama(){
        $this->load->library('m_pdf');
        $va     = $this->input->post() ;
        $jmlkompmax = 50;
        
        $pdfFilePath = "./tmp/".md5("rpslip-".date_now()).".pdf";
        $cDownloadPath = $pdfFilePath ;
        //$this->m_pdf->pdf->AddPage('L');

        //cek status sudah posting atau belum
        $ketperiode = $this->bdb->getval("keterangan", "kode = '{$va['periode']}'", "sys_periode_payroll");
        $sudahposting = false;
        $cabang = getsession($this, "cabang");
        $dbd2 = $this->rptpayroll_m->select("payroll_posting","faktur","periode = '{$va['periode']}' and cabang = '$cabang' and status = '1'");
        if($this->rptpayroll_m->rows($dbd2) > 0){
            $sudahposting = true;
        }
        //print_r($va['grid1']);
        $vaGrid = json_decode($va['grid1']);
       //print_r($vaGrid);
        foreach($vaGrid as $key => $val){
            //echo($val->cetakslip);
            if($val->cetakslip == "Ya"){
                //load data karaywan
                $dbd    = $this->rptpayroll_m->getdatakaryawan($val->kode) ;
                //echo($val->kode);
                //$dbd    = $vdb['db'] ;
                $n = 0 ;
                
                

                while( $dbr = $this->rptpayroll_m->getrow($dbd) ){
                    //print_r($dbr);
                    $arrcab = $this->func_m->GetDataCabang($cabang);
                    $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$va['tglakhir']);
                    $gajikotor = 0 ;
                    $jmlkomp = 0;
                    $html = "";
                    $html .= '<table width = "100%" height = "100%"><tr>';
                    for($cp = 1 ; $cp <=1 ;$cp++){
                        $html .= '<td align="center" height = "100%" width = 50% style = "border:1px solid black;">';
                            
                            $html .= '<h4 align="center">'.$arrcab['nama'].'</h4>';
                            $html .= '<h5 align="center">'.$arrcab['alamat'].'</h5>';
                            $html .= '<h5 align="center">TELP. '.$arrcab['telp'].'</h5>';
                            $html .= '<hr />';
                            $html .= '<h5 align="center">SLIP GAJI</h5>';
                            $html .= '<h5 align="center">'.$ketperiode.'</h5>';
                            $html .= '<table width = "100%" border="0">';
                                $html .= '<tr><td><table width = "100%">';
                                
                                    $html .= '  <tr>
                                                    <td style="font-size:12;text-align:center;">Nama/Jabatan</td>
                                                    <td style="font-size:12;text-align:center;">'.$dbr['nama'].'</td>
                                                    <td style="font-size:12;text-align:center;" colspan = "2">'.$jabatan['keterangan'].'</td>
                                                    <td></td></tr>';
                                    $html .= '  <tr>
                                                    <td style="font-size:12;text-align:center;"></td>
                                                    <td style="font-size:12;text-align:center;"></td>
                                                    <td style="font-size:12;text-align:center;" colspan = "2"></td>
                                                    <td></td></tr>';
                                    $html .= '  <tr>
                                                    <td style="font-size:12;text-align:center;"></td>
                                                    <td style="font-size:12;text-align:center;"></td>
                                                    <td style="font-size:12;text-align:center;" colspan = "2"></td>
                                                    <td></td></tr>';
                                    
                                    //list penambahan
                                    $html .= '  <tr><td style="font-size:10;text-align:left;">::Penambahan::</td><td></td><td></td><td></td><td></td></tr>';
                                    $totpenambahan = 0 ;
                                    $kodepinjaman = $this->rptpayroll_m->getconfig("kogapypinjkaryawan");
                                    // $where = "status = '1' and perhitungan <> 'K' and potongan = '0'";
                                    // $dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
                                    
                                    if($sudahposting){
                                        $where = "p.payroll <> '$kodepinjaman' and t.status = '1' and t.periode = '{$va['periode']}' and k.potongan = '0'";//(k.periode = 'J' or k.periode = 'H') and 
                                        $join = "left join payroll_komponen k on k.kode = p.payroll left join payroll_posting t on t.faktur = p.faktur";
                                        $dbd2 = $this->rptpayroll_m->select("payroll_posting_komponen p","k.kode,k.keterangan,k.periode,k.perhitungan,k.potongan",$where,$join,"k.kode","k.kode asc");
                                        
                                    }else{  
                                        $where = "status = '1' and kode <> '$kodepinjaman' and potongan = '0'";
                                        $dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where,"","","kode asc");

                                    }
                                    
                                    while($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
                                        if($sudahposting){
                                            $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawanposting($dbr['kode'],$dbr2['kode'],$va['periode']);
                                        // print_r($payroll);
                                        }else{
                                            $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawan($dbr['kode'],$dbr2['kode'],$va['periode']);
                                        }
                                        
                                        $gajikotor += $payroll['total'] ;

                                        if($payroll['total'] > 0){
                                            $skala = "";
                                            if($payroll['periode'] <> "B")$skala = $payroll['jmlskala'].' x ';
                                            $html .= '  <tr>
                                                    <td style="font-size:12;text-align:left;">'.$dbr2['keterangan'].'</td>
                                                    <td style="font-size:12;text-align:center;">'.$skala.'Rp.'.string_2s($payroll['nominal']).'</td>
                                                    <td style="font-size:12;text-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:12;text-align:right;"> '.string_2s($payroll['total']).'</td>
                                                    <td></td></tr>';
                                            $jmlkomp++;
                                        }
                                        $totpenambahan += $payroll['total'];
                                    }

                                    




                                    $html .= '  <tr><td></td><td><hr align="right"></td><td><hr align="right"></td><td><hr align="right"></td><td></td></tr>';
                                    //tot penambahan
                                    $html .= '  <tr><td></td><td></td>
                                                    <td style="font-size:12;text-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:12;text-align:right;">'.string_2s($totpenambahan).'</td>
                                                    <td></td></tr>';


                                    //list potongan
                                    $html .= '  <tr><td style="font-size:10;text-align:left;">::Potongan::</td><td></td><td></td><td></td><td></td></tr>';
                                    $totpotongan = 0 ;
                                    $kodepinjaman = $this->rptpayroll_m->getconfig("kogapypinjkaryawan");
                                    //$where = "status = '1' and perhitungan <> 'K' and potongan = '1'";
                                    //$dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
                                    if($sudahposting){
                                        $where = "p.payroll <> '$kodepinjaman' and t.status = '1' and t.periode = '{$va['periode']}' and k.potongan = '1'";//(k.periode = 'J' or k.periode = 'H') and
                                        $join = "left join payroll_komponen k on k.kode = p.payroll left join payroll_posting t on t.faktur = p.faktur";
                                        $dbd2 = $this->rptpayroll_m->select("payroll_posting_komponen p","k.kode,k.keterangan,k.periode,k.perhitungan,k.potongan",$where,$join,"k.kode");
                                        
                                    }else{  
                                        $where = "status = '1' and kode <> '$kodepinjaman' and potongan = '1'";
                                        $dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);

                                    }
                                    while($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
                                        if($sudahposting){
                                            $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawanposting($dbr['kode'],$dbr2['kode'],$va['periode']);
                                        // print_r($payroll);
                                        }else{
                                            $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawan($dbr['kode'],$dbr2['kode'],$va['periode']);
                                        }
                                        
                                        $gajikotor += $payroll['total'] ;

                                        if($payroll['total'] > 0){
                                            $skala = "";
                                            if($payroll['periode'] <> "B")$skala = $payroll['jmlskala'].' x ';
                                            $html .= '  <tr>
                                                    <td style="font-size:12;text-align:left;">'.$dbr2['keterangan'].'</td>
                                                    <td style="font-size:12;text-align:center;">'.$skala.' Rp.'.string_2s($payroll['nominal']).'</td>
                                                    <td style="font-size:12;text-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:12;text-align:right;"> '.string_2s($payroll['total']).'</td>
                                                    <td></td></tr>';
                                            $jmlkomp++;
                                        }
                                        $totpotongan += $payroll['total'];
                                    }
                                    
                                    //cek pinjaman karaywan
                                    if($sudahposting){
                                        $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjamanposting($dbr['kode'],$va['periode']);
                                    }else{
                                        $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjaman($dbr['kode'],$va['periode']);
                                    }

                                    if($kasbon['total'] > 0){
                                        $html .= '  <tr>
                                                    <td style="font-size:12;text-align:left;">Pinjaman Karyawan</td>
                                                    <td style="font-size:12;text-align:center;"> 1 x &nbsp; Rp.'.string_2s($kasbon['total']).'</td>
                                                    <td style="font-size:12;text-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:12;text-align:right;"> '.string_2s($kasbon['total']).'</td>
                                                    <td></td></tr>';
                                        $jmlkomp++;
                                    }
                                    $totpotongan += $kasbon['total'];
                                    
                                    $html .= '  <tr><td></td><td><hr align="right"></td><td><hr align="right"></td><td><hr align="right"></td><td></td></tr>';
                                    //tot pengurangan
                                    
                                    $html .= '  <tr><td></td><td></td>
                                                    <td style="font-size:12;text-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:12;text-align:right;">'.string_2s($totpotongan).'</td>
                                                    <td></td></tr>';
                                    $html .= '  <tr><td></td><td><hr align="right"></td><td><hr align="right"></td><td><hr align="right"></td><td><hr align="right"></td></tr>';
                                    
                                    $gajibersih = $totpenambahan - $totpotongan;
                                    //tot gaji
                                    $html .= '  <tr><td></td><td></td>
                                                    <td style="font-size:12;text-align:left;">&nbsp;=&nbsp;</td>
                                                    <td></td>
                                                    <td style="font-size:12;text-align:right;">Rp. '.string_2s($gajibersih).'</td>
                                                    </tr>';
                                
                                $html .= '</table></td></tr>';
                                $html .= '<tr><td><table width = "100%">';
                                    $html .= '  <tr><td></td><td></td></tr>';
                                    
                                    if($jmlkompmax > $jmlkomp){
                                        $selisih = $jmlkompmax - $jmlkomp;
                                        for($a=0;$a<=$selisih;$a++){
                                            $html .= '  <tr><td></td><td></td></tr>';
                                        }
                                    }
                                    //ttd
                                    $html .= '  <tr><td style="font-size:12;text-align:center;">Kudus,'.$va['tglakhir'].'</td><td></td></tr>';
                                    $html .= '  <tr><td align="center"></td><td></td></tr>';
                                    $html .= '  <tr><td align="center"></td><td></td></tr>';
                                        
                                    $html .= '  <tr><td style="font-size:12;text-align:center;">Bag. Keuangan</td>
                                                    <td style="font-size:12;text-align:center;">Karyawan</td></tr>';
                                    for($a=1;$a<=10;$a++){
                                        $html .= '  <tr><td></td><td></td></tr>';
                                    }
                                    
                                    $html .= '  <tr><td style="font-size:12;text-align:center;"><u>REFINDA BERLIANA</u></td>
                                                    <td style="font-size:12;text-align:center;"><u>'.$dbr['nama'].'</u></td></tr>';

                                $html .= '</table></td></tr>';                  
                            
                            $html .= '</table>';
                        $html .= '</td>';

                    }   
                    $html .= '</tr></table>';

                    $this->m_pdf->pdf->AddPageByArray([
                        'margin-left' => 0,
                        'margin-right' => 0,
                        'margin-top' => 10,
                        'margin-bottom' => 0,
                        'sheet-size' => 'A5'
                    ]);

                    $this->m_pdf->pdf->WriteHTML($html);
                    

                    
                }
            }
        }

        $this->m_pdf->pdf->Output(FCPATH.$pdfFilePath,'F');
        echo("
                bos.rptpayroll.showReport('".$pdfFilePath."');
            ");
    }

    public function printslipa5(){
        $this->load->library('m_pdf');
        $va     = $this->input->post() ;
        $jmlkompmax = 20;
        
        $pdfFilePath = "./tmp/".md5("rpslip-".date_now()).".pdf";
        $cDownloadPath = $pdfFilePath ;
        //$this->m_pdf->pdf->AddPage('L');

        //cek status sudah posting atau belum
        $ketperiode = $this->bdb->getval("keterangan", "kode = '{$va['periode']}'", "sys_periode_payroll");
        $sudahposting = false;
        $cabang = getsession($this, "cabang");
        $dbd2 = $this->rptpayroll_m->select("payroll_posting","faktur","periode = '{$va['periode']}' and cabang = '$cabang' and status = '1'");
        if($this->rptpayroll_m->rows($dbd2) > 0){
            $sudahposting = true;
        }
        //print_r($va['grid1']);
        $vaGrid = json_decode($va['grid1']);
       //print_r($vaGrid);
        foreach($vaGrid as $key => $val){
            //echo($val->cetakslip);
            if($val->cetakslip == "Ya"){
                //load data karaywan
                $dbd    = $this->rptpayroll_m->getdatakaryawan($val->kode) ;
                //echo($val->kode);
                //$dbd    = $vdb['db'] ;
                $n = 0 ;
                
                

                while( $dbr = $this->rptpayroll_m->getrow($dbd) ){
                    //print_r($dbr);
                    $arrcab = $this->func_m->GetDataCabang($cabang);
                    $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$va['tglakhir']);
                    $gajikotor = 0 ;
                    $jmlkomp = 0;
                    $html = "";
                    $html .= '<table width = "600px" height = "100%" style = "border:1px solid black;">';
                    for($cp = 1 ; $cp <=1 ;$cp++){
                        $html .= '<tr><td align="center" height = "100%" width = 100% >';
                            
                            $html .= '<h4 align="center" style="font-size:16px;text-align:center;">'.$arrcab['nama'].'</h4>';
                            $html .= '<h5 align="center" style="font-size:13px;text-align:center;">'.$arrcab['alamat'].'</h5>';
                            $html .= '<h5 align="center" style="font-size:13px;text-align:center;">TELP. '.$arrcab['telp'].'</h5>';
                            $html .= '<hr /><br/>';
                            $html .= '<h5 align="center" style="font-size:14px;text-align:center;">SLIP GAJI</h5>';
                            $html .= '<h5 align="center" style="font-size:14px;text-align:center;">'.$ketperiode.'</h5><br/>';

                        $html .= '</td></tr>';

                        $html .= '<tr><td width = "600px"><table width = "100%">';
                                
                            $html .= '  <tr>
                                <td style="font-size:13px;text-align:center;">Nama/Jabatan</td>
                                <td style="font-size:13px;text-align:center;">'.$dbr['nama'].'</td>
                                <td style="font-size:13px;text-align:center;">'.$jabatan['keterangan'].'</td>
                                </tr>';
                           
                        $html .= '</table></td></tr>'; 
                        $html .= '<tr><td width = "600px"><table width = "100%">';
                                    //list penambahan
                                    $html .= '  <tr><td style="font-size:10;text-align:left;">::Penambahan::</td><td width=150px></td><td width=50px></td><td width=120px></td><td width=120px></td></tr>';
                                    $totpenambahan = 0 ;
                                    $kodepinjaman = $this->rptpayroll_m->getconfig("kogapypinjkaryawan");
                                    // $where = "status = '1' and perhitungan <> 'K' and potongan = '0'";
                                    // $dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
                                    
                                    if($sudahposting){
                                        $where = "p.payroll <> '$kodepinjaman' and t.status = '1' and t.periode = '{$va['periode']}' and k.potongan = '0'";//(k.periode = 'J' or k.periode = 'H') and 
                                        $join = "left join payroll_komponen k on k.kode = p.payroll left join payroll_posting t on t.faktur = p.faktur";
                                        $dbd2 = $this->rptpayroll_m->select("payroll_posting_komponen p","k.kode,k.keterangan,k.periode,k.perhitungan,k.potongan",$where,$join,"k.kode","k.kode asc");
                                        
                                    }else{  
                                        $where = "status = '1' and kode <> '$kodepinjaman' and potongan = '0'";
                                        $dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where,"","","kode asc");

                                    }
                                    
                                    while($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
                                        if($sudahposting){
                                            $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawanposting($dbr['kode'],$dbr2['kode'],$va['periode']);
                                        // print_r($payroll);
                                        }else{
                                            $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawan($dbr['kode'],$dbr2['kode'],$va['periode']);
                                        }
                                        
                                        $gajikotor += $payroll['total'] ;

                                        if($payroll['total'] > 0){
                                            $skala = "";
                                            if($payroll['periode'] <> "B")$skala = $payroll['jmlskala'].' x ';

                                            $html .= '  <tr>
                                                    <td style="font-size:12px;text-align:left;">'.$dbr2['keterangan'].'</td>
                                                    <td style="font-size:12px;text-align:center;">'.$skala.' Rp.'.string_2s($payroll['nominal']).'</td>
                                                    <td style="font-size:12px;text-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:12px;text-align:right;"> '.string_2s($payroll['total']).'</td>
                                                    <td></td></tr>';
                                            $jmlkomp++;
                                        }
                                        $totpenambahan += $payroll['total'];
                                    }

                                    




                                    $html .= '  <tr><td></td><td><hr align="right"></td><td><hr align="right"></td><td><hr align="right"></td><td></td></tr>';
                                    //tot penambahan
                                    $html .= '  <tr><td></td><td></td>
                                                    <td style="font-size:12px;text-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:12px;text-align:right;">'.string_2s($totpenambahan).'</td>
                                                    <td></td></tr>';


                                    //list potongan
                                    $html .= '  <tr><td style="font-size:10;text-align:left;">::Potongan::</td><td></td><td></td><td></td><td></td></tr>';
                                    $totpotongan = 0 ;
                                    $kodepinjaman = $this->rptpayroll_m->getconfig("kogapypinjkaryawan");
                                    //$where = "status = '1' and perhitungan <> 'K' and potongan = '1'";
                                    //$dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
                                    if($sudahposting){
                                        $where = "p.payroll <> '$kodepinjaman' and t.status = '1' and t.periode = '{$va['periode']}' and k.potongan = '1'";//(k.periode = 'J' or k.periode = 'H') and
                                        $join = "left join payroll_komponen k on k.kode = p.payroll left join payroll_posting t on t.faktur = p.faktur";
                                        $dbd2 = $this->rptpayroll_m->select("payroll_posting_komponen p","k.kode,k.keterangan,k.periode,k.perhitungan,k.potongan",$where,$join,"k.kode");
                                        
                                    }else{  
                                        $where = "status = '1' and kode <> '$kodepinjaman' and potongan = '1'";
                                        $dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);

                                    }
                                    while($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
                                        if($sudahposting){
                                            $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawanposting($dbr['kode'],$dbr2['kode'],$va['periode']);
                                        // print_r($payroll);
                                        }else{
                                            $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawan($dbr['kode'],$dbr2['kode'],$va['periode']);
                                        }
                                        
                                        $gajikotor += $payroll['total'] ;

                                        if($payroll['total'] > 0){
                                            $skala = "";
                                            if($payroll['periode'] <> "B")$skala = $payroll['jmlskala'].' x ';
                                            $html .= '  <tr>
                                                    <td style="font-size:12px;text-align:left;">'.$dbr2['keterangan'].'</td>
                                                    <td style="font-size:12px;text-align:center;">'.$skala.' Rp.'.string_2s($payroll['nominal']).'</td>
                                                    <td style="font-size:12px;text-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:12px;text-align:right;"> '.string_2s($payroll['total']).'</td>
                                                    <td></td></tr>';
                                            $jmlkomp++;
                                        }
                                        $totpotongan += $payroll['total'];
                                    }
                                    
                                    //cek pinjaman karaywan
                                    if($sudahposting){
                                        $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjamanposting($dbr['kode'],$va['periode']);
                                    }else{
                                        $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjaman($dbr['kode'],$va['periode']);
                                    }

                                    if($kasbon['total'] > 0){
                                        $html .= '  <tr>
                                                    <td style="font-size:12px;text-align:left;">Pinjaman Karyawan</td>
                                                    <td style="font-size:12px;text-align:center;"> 1 x &nbsp; Rp.'.string_2s($kasbon['total']).'</td>
                                                    <td style="font-size:12px;text-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:12px;text-align:right;"> '.string_2s($kasbon['total']).'</td>
                                                    <td></td></tr>';
                                        $jmlkomp++;
                                    }
                                    $totpotongan += $kasbon['total'];
                                    
                                    $html .= '  <tr><td></td><td><hr align="right"></td><td><hr align="right"></td><td><hr align="right"></td><td></td></tr>';
                                    //tot pengurangan
                                    
                                    $html .= '  <tr><td></td><td></td>
                                                    <td style="font-size:12px;text-align:left;">&nbsp;=&nbsp; Rp.</td>
                                                    <td style="font-size:12px;text-align:right;">'.string_2s($totpotongan).'</td>
                                                    <td></td></tr>';
                                    $html .= '  <tr><td></td><td><hr align="right"></td><td><hr align="right"></td><td><hr align="right"></td><td><hr align="right"></td></tr>';
                                    
                                    $gajibersih = $totpenambahan - $totpotongan;
                                    //tot gaji
                                    $html .= '  <tr><td></td><td></td>
                                                    <td style="font-size:12px;text-align:left;">&nbsp;=&nbsp;</td>
                                                    <td></td>
                                                    <td style="font-size:12px;text-align:right;">Rp. '.string_2s($gajibersih).'</td>
                                                    </tr>';
                                
                            $html .= '</table></td></tr>';
                                $html .= '<tr><td><table width = "100%">';
                                    $html .= '  <tr><td></td><td></td></tr>';
                                    
                                    if($jmlkompmax > $jmlkomp){
                                        $selisih = $jmlkompmax - $jmlkomp;
                                        for($a=0;$a<=$selisih;$a++){
                                            $html .= '  <tr><td style="font-size:12px;text-align:center;">&nbsp;</td><td style="font-size:12px;text-align:center;">&nbsp;</td></tr>';
                                        }
                                    }
                                    //ttd
                                    $html .= '  <tr><td style="font-size:12px;text-align:center;">Kudus,'.$va['tglakhir'].'</td><td></td></tr>';
                                    $html .= '  <tr><td align="center"></td><td></td></tr>';
                                    $html .= '  <tr><td align="center"></td><td></td></tr>';
                                        
                                    $html .= '  <tr><td style="font-size:12px;text-align:center;">Bag. Keuangan</td>
                                                    <td style="font-size:12px;text-align:center;">Karyawan</td></tr>';
                                    for($a=1;$a<=3;$a++){
                                        $html .= '  <tr><td style="font-size:12px;text-align:center;">&nbsp;</td style="font-size:12px;text-align:center;"><td>&nbsp;</td></tr>';
                                    }
                                    
                                    $html .= '  <tr><td style="font-size:12px;text-align:center;"><u>REFINDA BERLIANA</u></td>
                                                    <td style="font-size:12px;text-align:center;"><u>'.$dbr['nama'].'</u></td></tr>';

                                // $html .= '</table></td></tr>';                  
                            
                            // $html .= '</table>';
                        //$html .= '</td>';
                        $html .= '</table></td></tr>';
                        

                    }   
                    $html .= '</table>';

                    $this->m_pdf->pdf->AddPageByArray([
                        'margin-left' => 0,
                        'margin-right' => 0,
                        'margin-top' => 10,
                        'margin-bottom' => 0,
                        'sheet-size' => 'A5'
                    ]);

                    $this->m_pdf->pdf->WriteHTML($html);
                    

                    
                }
            }
        }

        $this->m_pdf->pdf->Output(FCPATH.$pdfFilePath,'F');
        echo("
                bos.rptpayroll.showReport('".$pdfFilePath."');
            ");
    }
    
    public function initreportdaftargaji(){
        $n=0;
        $va     = $this->input->post() ;
        
        savesession($this, $this->ss . "va", json_encode($va) ) ;

        //print_r($va);
        $vare   = array() ;
        $vdb    = $this->rptpayroll_m->loaddaftargaji($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;
        
        //cek status sudah posting atau belum
        $sudahposting = false;
        $cabang = getsession($this, "cabang");
        $dbd2 = $this->rptpayroll_m->select("payroll_posting","faktur","periode = '{$va['periode']}' and cabang = '$cabang' and status = '1'");
        if($this->rptpayroll_m->rows($dbd2) > 0){
            $sudahposting = true;
        }

        while( $dbr = $this->rptpayroll_m->getrow($dbd) ){
            $n++;

            //jab bag
            $vaset = array();
            $gajikotor = 0 ;
            $arrhk  = array();
            $arruh  = array();
            $arrub  = array();
            $kodepinjaman = $this->rptpayroll_m->getconfig("kogapypinjkaryawan");
            $where = "status = '1' and kode <> '$kodepinjaman'";
            $dbd2 = $this->rptpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
            while($dbr2 = $this->rptpayroll_m->getrow($dbd2)){
                if($sudahposting){
                    $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawanposting($dbr['kode'],$dbr2['kode'],$va['periode']);
                   // print_r($payroll);
                }else{
                    $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawan($dbr['kode'],$dbr2['kode'],$va['periode']);
                }
                

                if($dbr2['potongan'] == "1"){
                    $payroll['nominal'] *=-1;
                    $payroll['total'] *=-1;
                }
                if($dbr2['periode'] == "J" or $dbr2['periode'] == "H"){
                    $arrhk["HK ".$dbr2['keterangan']] = $payroll['jmlskala'];    
                    $arruh["UH ".$dbr2['keterangan']] = $payroll['nominal'];            
                }
                $arrub["UB ".$dbr2['keterangan']] = $payroll['total']; 
                
                $gajikotor += $payroll['total'] ;
                
            }

            //hk
            foreach($arrhk as $key => $val){
                $vaset[$key] = $val;
            }

            //uh
            foreach($arruh as $key => $val){
                $vaset[$key] = $val;
            }

            //ub
            foreach($arrub as $key => $val){
                $vaset[$key] = $val;
            }

            $vaset['gajikotor'] = $gajikotor;

            //cek pinjaman karaywan
            if($sudahposting){
                $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjamanposting($dbr['kode'],$va['periode']);
            }else{
                $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjaman($dbr['kode'],$va['periode']);
            }
            
            $vaset['pinjamankaryawan'] = $kasbon['total'] * -1;
            $vaset['pinjamankaryawan'] = $vaset['pinjamankaryawan'];
            $vaset['gajibersih'] = $gajikotor - $kasbon['total'];
            $vaset['gajibersih'] = $vaset['gajibersih'];
            $vare[]		= array("no"=>$n,"nama"=>$dbr['nama'],"rekening"=>$dbr['rekening'],"jumlah"=>$vaset['gajibersih']) ;   
        }
        savesession($this, "rptpayrolldaftargaji_rpt", json_encode($vare)) ; 
        echo(' bos.rptpayroll.openreportdaftargaji() ; ') ;
    }

    public function showreportdaftargaji(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        //print_r($va);
        $data = getsession($this,"rptpayrolldaftargaji_rpt") ;      
        $data = json_decode($data,true) ;
        
        //ttd
        $now  = date_2b($va['tglakhir']) ;
        $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
        $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
        $vttd = array() ;
        $vttd[] = array("1"=>"","2"=>"","3"=> $kota ) ;
        $vttd[] = array("1"=>"Mengetahui,","2"=>"","3"=>"Yang Mengeluarkan") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"") ;
        $vttd[] = array("1"=>"<u>Moh Saufik</u>","2"=>"","3"=>"<u>Refinda Berliana</u>") ;
        $vttd[] = array("1"=>"Manager","2"=>"","3"=>"Bag. Keuangan") ;

        $sumbag = array();
        //menyusun total
        $arrtot = array();
        $arrtot['ket'] = "<b>Total";
        foreach($data as $key => $val){
            
            //hitung summary semua
            foreach($val as $key2 => $val2){
                $jenis = substr($key2,0,2); 
                
                if($key2 == "jumlah"){
                    if(!isset($arrtot[$key2]))$arrtot[$key2] = 0 ;
                    $arrtot[$key2] += string_2n($val2);
                    $data[$key][$key2] = string_2s($val2);
                }
                
            }
        }

        //number 2 string total
        foreach($arrtot as $key => $val){
            if($key == "jumlah")$arrtot[$key] = string_2s($val)."</b>";
        }
        $total[] = $arrtot;

        $ketperiode = $this->bdb->getval("keterangan", "kode = '{$va['periode']}'", "sys_periode_payroll","","","kode asc","1");
        
        
        if(!empty($data)){ 
            $font = 9 ;
            if(!isset($va['export']) || $va['export'] == "")$va['export'] = 0 ;
            $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>$va['export'],
                          'opt'=>array('export_name'=>'DaftarGaji_' . getsession($this, "username") ) ) ;
            $this->load->library('bospdf', $o) ;   
            $cabang = getsession($this,"cabang");
            $arrcab = $this->func_m->GetDataCabang($cabang);
            $this->bospdf->ezText($arrcab['kode'] ." - ".$arrcab['nama'],$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText($arrcab['alamat'] . " / ". $arrcab['telp'],$font,array("justification"=>"center")) ;
            $this->bospdf->ezText("<b>Daftar Gaji</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText($ketperiode,$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText(date_2d($va['tglawal']). " sd " .date_2d($va['tglakhir']),$font+2,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ; 
            
            $this->bospdf->ezTable($data,"","",array("showLines"=>2,"showHeadings"=>"1","fontSize"=>$font,"cols"=>array(
                                    "no"=>array("caption"=>"No","width"=>5,"justification"=>"right"),
                                    "nama"=>array("caption"=>"Nama","justification"=>"left"),
                                    "rekening"=>array("caption"=>"Rekening","width"=>20,"justification"=>"center"),
                                    "jumlah"=>array("caption"=>"Jumlah","width"=>15,"justification"=>"right")
            ))) ;
            $this->bospdf->ezTable($total,"","",array("showLines"=>2,"showHeadings"=>"","fontSize"=>$font,"cols"=>array(
                "ket"=>array("caption"=>"Nama","justification"=>"center"),
                "jumlah"=>array("caption"=>"Jumlah","width"=>15,"justification"=>"right")
            ))) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("width"=>25,"justification"=>"center"),
                                          "2"=>array("wrap"=>1,"justification"=>"center"),
                                          "3"=>array("width"=>25,"justification"=>"center"))
                                 )
                              ) ;

            //print_r($data) ;    
            $this->bospdf->ezStream() ; 
        }else{
            echo('data kosong') ;
        }
    }
}
?>
