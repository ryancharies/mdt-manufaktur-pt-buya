
<?php
class Rptbulanan extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model("rpt/rptbulanan_m") ;
        $this->load->model("func/perhitungan_m") ;
        $this->bdb 	= $this->rptbulanan_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptbulanan_" ;
    }  

    public function index(){ 

        $data['rekarkpjawal']= $this->bdb->getconfig("rekarkpjawal") ;
        $data['ketrekarkpjawal'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkpjawal']}'", "keuangan_rekening");
        $data['rekarkpjakhir']= $this->bdb->getconfig("rekarkpjakhir") ;
        $data['ketrekarkpjakhir'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkpjakhir']}'", "keuangan_rekening");
        $data['rekarkkbawal']= $this->bdb->getconfig("rekarkkbawal") ;
        $data['ketrekarkkbawal'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkkbawal']}'", "keuangan_rekening");
        $data['rekarkkbakhir']= $this->bdb->getconfig("rekarkkbakhir") ;
        $data['ketrekarkkbakhir'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkkbakhir']}'", "keuangan_rekening");
        $data['rekarkpiutangawal']= $this->bdb->getconfig("rekarkpiutangawal") ;
        $data['ketrekarkpiutangawal'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkpiutangawal']}'", "keuangan_rekening");
        $data['rekarkpiutangakhir']= $this->bdb->getconfig("rekarkpiutangakhir") ;
        $data['ketrekarkpiutangakhir'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkpiutangakhir']}'", "keuangan_rekening");
        $data['rekarkpsdawal']= $this->bdb->getconfig("rekarkpsdawal") ;
        $data['ketrekarkpsdawal'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkpsdawal']}'", "keuangan_rekening");
        $data['rekarkpsdakhir']= $this->bdb->getconfig("rekarkpsdakhir") ;
        $data['ketrekarkpsdakhir'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkpsdakhir']}'", "keuangan_rekening");
        $data['rekarkpsktawal']= $this->bdb->getconfig("rekarkpsktawal") ;
        $data['ketrekarkpsktawal'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkpsktawal']}'", "keuangan_rekening");
        $data['rekarkpsktakhir']= $this->bdb->getconfig("rekarkpsktakhir") ;
        $data['ketrekarkpsktakhir'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkpsktakhir']}'", "keuangan_rekening");
        $data['rekarkatawal']= $this->bdb->getconfig("rekarkatawal") ;
        $data['ketrekarkatawal'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkatawal']}'", "keuangan_rekening");
        $data['rekarkatakhir']= $this->bdb->getconfig("rekarkatakhir") ;
        $data['ketrekarkatakhir'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkatakhir']}'", "keuangan_rekening");
        $data['rekarkatwawal']= $this->bdb->getconfig("rekarkatwawal") ;
        $data['ketrekarkatwawal'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkatwawal']}'", "keuangan_rekening");
        $data['rekarkatwakhir']= $this->bdb->getconfig("rekarkatwakhir") ;
        $data['ketrekarkatwakhir'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkatwakhir']}'", "keuangan_rekening");
        $data['rekarkallawal']= $this->bdb->getconfig("rekarkallawal") ;
        $data['ketrekarkallawal'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkallawal']}'", "keuangan_rekening");
        $data['rekarkallakhir']= $this->bdb->getconfig("rekarkallakhir") ;
        $data['ketrekarkallakhir'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkallakhir']}'", "keuangan_rekening");
        $data['rekarkhdawal']= $this->bdb->getconfig("rekarkhdawal") ;
        $data['ketrekarkhdawal'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkhdawal']}'", "keuangan_rekening");
        $data['rekarkhdakhir']= $this->bdb->getconfig("rekarkhdakhir") ;
        $data['ketrekarkhdakhir'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkhdakhir']}'", "keuangan_rekening");
        $data['rekarkhbawal']= $this->bdb->getconfig("rekarkhbawal") ;
        $data['ketrekarkhbawal'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkhbawal']}'", "keuangan_rekening");
        $data['rekarkhbakhir']= $this->bdb->getconfig("rekarkhbakhir") ;
        $data['ketrekarkhbakhir'] = $this->bdb->getval("keterangan", "kode = '{$data['rekarkhbakhir']}'", "keuangan_rekening");


        $this->load->view("rpt/rptbulanan",$data) ; 

    }

    public function saving(){
        $va 	= $this->input->post() ;
        $this->bdb->saveconfig("rekarkpjawal", $va['rekarkpjawal']) ;
        $this->bdb->saveconfig("rekarkpjakhir", $va['rekarkpjakhir']) ;
        $this->bdb->saveconfig("rekarkkbawal", $va['rekarkkbawal']) ;
        $this->bdb->saveconfig("rekarkkbakhir", $va['rekarkkbakhir']) ;
        $this->bdb->saveconfig("rekarkpiutangawal", $va['rekarkpiutangawal']) ;
        $this->bdb->saveconfig("rekarkpiutangakhir", $va['rekarkpiutangakhir']) ;
        $this->bdb->saveconfig("rekarkpsdawal", $va['rekarkpsdawal']) ;
        $this->bdb->saveconfig("rekarkpsdakhir", $va['rekarkpsdakhir']) ;
        $this->bdb->saveconfig("rekarkpsktawal", $va['rekarkpsktawal']) ;
        $this->bdb->saveconfig("rekarkpsktakhir", $va['rekarkpsktakhir']) ;
        $this->bdb->saveconfig("rekarkatawal", $va['rekarkatawal']) ;
        $this->bdb->saveconfig("rekarkatakhir", $va['rekarkatakhir']) ;
        $this->bdb->saveconfig("rekarkatwawal", $va['rekarkatwawal']) ;
        $this->bdb->saveconfig("rekarkatwakhir", $va['rekarkatwakhir']) ;
        $this->bdb->saveconfig("rekarkallawal", $va['rekarkallawal']) ;
        $this->bdb->saveconfig("rekarkallakhir", $va['rekarkallakhir']) ;
        $this->bdb->saveconfig("rekarkhdawal", $va['rekarkhdawal']) ;
        $this->bdb->saveconfig("rekarkhdakhir", $va['rekarkhdakhir']) ;
        $this->bdb->saveconfig("rekarkhbawal", $va['rekarkhbawal']) ;
        $this->bdb->saveconfig("rekarkhbakhir", $va['rekarkhbakhir']) ;

        echo('bos.rptbulanan.settab(0) ') ;
    }

    public function seekrekening(){
        $search     = $this->input->get('q');
        $vdb    = $this->bdb->seekrekening($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ".$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

    public function loadgrid(){ 

        $va     = json_decode($this->input->post('request'), true) ; 
        $arrtw = array("I","II","III","IV");
        $vare   = array() ;
        $arrdata = array();
        $arrperiod = explode("-",$va['periode']);
        $time = mktime(0,0,0,$arrperiod[0]+1,0,$arrperiod[1]);//"01-".$val['bln']."-".$thn;
        $tglakhir = date("d-m-Y",$time);

        $time2 = mktime(0,0,0,$arrperiod[0],1,$arrperiod[1]);//"01-".$val['bln']."-".$thn;
        $tglawal = date("d-m-Y",$time2);
        $arrdata[] = array("periode"=>$va['periode'],"tglawal"=>$tglawal,"tglakhir"=>$tglakhir);
        
        //print_r($arrdata);

        $n = 0 ;

        $arrd = array();
        //laba usaha
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Laba Usaha";
        $arrlr = array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $arrlr = $this->perhitungan_m->getlr($val['tglawal'],$val['tglakhir']);
            $arrd[$val['periode']] =string_2s($arrlr['lrstlhpjk']['saldoakhirperiod']);
            $arrlr[$val['periode']] = $arrlr['lrstlhpjk']['saldoakhirperiod'];
        }
        
        $vare[] = $arrd;
        //print_r($arrd);

        //penjualan
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Penjualan";
        $arrpj = array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkpjawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkpjakhir") ;
            $debet = $this->perhitungan_m->getdebet($val['tglawal'],$val['tglakhir'],$rekening,'',$rekening2);
            $kredit = $this->perhitungan_m->getkredit($val['tglawal'],$val['tglakhir'],$rekening,'',$rekening2);
            $jumlah = $kredit - $debet;
            $arrd[$val['periode']] = string_2s($jumlah);
            $arrpj[$val['periode']] = $jumlah;
        }
        $vare[] = $arrd;

        //aktiva
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Aktiva";
        $arraktiva = array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = "1";
            $rekening2 = "1.9999";
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            //$kredit = $this->perhitungan_m->getkredit($val['tglawal'],$val['tglakhir'],$rekening,'',$rekening2);
            //$jumlah = $debet - $kredit;
            $arrd[$val['periode']] = string_2s($jumlah);
            $arraktiva[$val['periode']] = $jumlah;
        }
        $vare[] = $arrd;

        //space
        $arrd['no'] = "";
        $arrd['keterangan'] = "";
        foreach($arrdata as $key =>$val){
            $arrd[$val['periode']] = "";
        }
        $vare[] = $arrd;

        //saldo
        $arrd['no'] = "";
        $arrd['keterangan'] = "<b>Saldo</b>";
        foreach($arrdata as $key =>$val){

            $arrd[$val['periode']] = "<b>".$val['tglakhir']."</b>";
        }
        $vare[] = $arrd;

        //kas dan bank
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Kas dan Bank";
        $arrkb = array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkkbawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkkbakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$val['periode']] = string_2s($jumlah);
            $arrkb[$val['periode']] = $jumlah;
            $i++;
        }
        $selisih = $t2 - $t1;
        $arrd['pers'] = devide($selisih,$t1) * 100;
        $arrd['pers'] = string_2s($arrd['pers'])."%";
        $vare[] = $arrd;


        //piutang
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Piutang";
        $arrpiut = array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkpiutangawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkpiutangakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$val['periode']] = string_2s($jumlah);
            $arrpiut[$val['periode']] = $jumlah;
            $i++;
        }
        $vare[] = $arrd;

        //persediaan
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Persediaan";
        $arrpersd= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkpsdawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkpsdakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$val['periode']] = string_2s($jumlah);
            $arrpersd[$val['periode']] = $jumlah;
        }
        $vare[] = $arrd;

        //porskot
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Porsekot";
        $arrpskt= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkpsktawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkpsktakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$val['periode']] = string_2s($jumlah);
            $arrpskt[$val['periode']] = $jumlah;
        } 
        $vare[] = $arrd;

        //Jml aktiva lancar
        $arrd['no'] = "";
        $arrd['keterangan'] = "<b><i>Jumlah Aktiva Lancar</i></b>";
        $arrjmlaktivalcr= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $key2 = $val['periode'];
            $jumlah = $arrkb[$key2] + $arrpiut[$key2]  + $arrpersd[$key2] + $arrpskt[$key2];
            $arrd[$key2] = "<b>".string_2s($jumlah)."</b>";
            $arrjmlaktivalcr[$key2] = $jumlah;
        }
        $vare[] = $arrd;

        //space
        $arrd['no'] = "";
        $arrd['keterangan'] = "";
        foreach($arrdata as $key =>$val){
            $arrd[$val['periode']] = "";
        }
        $vare[] = $arrd;

        //Aktiva Tetap
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Aktiva Tetap";
        $arrat= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkatawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkatakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$val['periode']] = string_2s($jumlah);
            $arrat[$val['periode']] = $jumlah;
        }
        $vare[] = $arrd;

        //Aktiva Tidak Berwujud
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Aktiva Tidak Berwujud";
        $arratw= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkatwawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkatwakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$val['periode']] = string_2s($jumlah);
            $arratw[$val['periode']] = $jumlah;
            $i++;
            if($i == 1){
                $t1 = $jumlah;
            }else{
                $t2 = $jumlah;
            }
        }
        $vare[] = $arrd;

        //Aktiva Lain-Lain
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Aktiva Lain-Lain";
        $arrall= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkallawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkallakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$val['periode']] = string_2s($jumlah);
            $arrall[$val['periode']] = $jumlah;
        }
        $vare[] = $arrd;

        //Jml aktiva tetap
        $arrd['no'] = "";
        $arrd['keterangan'] = "<b><i>Jumlah Aktiva Tetap</i></b>";
        $arrjmlaktivat= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $key2 = $val['periode'];
            $jumlah = $arrat[$key2] + $arratw[$key2]  + $arrall[$key2];
            $arrd[$key2] = "<b>".string_2s($jumlah)."</b>";
            $arrjmlaktivat[$key2] = $jumlah;
        }
        $vare[] = $arrd;

        $arrd['no'] = "";
        $arrd['keterangan'] = "<b><i>Total Aktiva</i></b>";
        $arrtotaktiva= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $key2 = $val['periode'];
            $jumlah = $arrjmlaktivalcr[$key2] + $arrjmlaktivat[$key2];
            $arrd[$key2] = "<b>".string_2s($jumlah)."</b>";
            $arrtotaktiva[$key2] = $jumlah;
        }
        $vare[] = $arrd;

        //space
        $arrd['no'] = "";
        $arrd['keterangan'] = "";
        foreach($arrdata as $key =>$val){
            $arrd[$val['periode']] = "";
        }
        $vare[] = $arrd;

        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Hutang Dagang";
        $arrhd= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkhdawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkhdakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$val['periode']] = string_2s($jumlah);
            $arrhd[$val['periode']] = $jumlah;
        }
        $vare[] = $arrd;

        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Hutang Bank/ Jangka Panjang";
        $arrhb= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkhbawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkhbakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$val['periode']] = string_2s($jumlah);
            $arrhb[$val['periode']] = $jumlah;
        }
        $vare[] = $arrd;

        //JML HUTANG
        $arrd['no'] = "";
        $arrd['keterangan'] = "<b><i>Jumlah Hutang</i></b>";
        $arrjmlhut= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $key2 = $val['periode'];
            $jumlah = $arrhd[$key2] + $arrhb[$key2];
            $arrd[$key2] = "<b>".string_2s($jumlah)."</b>";
            $arrjmlhut[$key2] = $jumlah;
        }
       
        $vare[] = $arrd;


        //space
        $arrd['no'] = "";
        $arrd['keterangan'] = "";
        foreach($arrdata as $key =>$val){
            $arrd[$val['periode']] = "";
        }
        $vare[] = $arrd;

        //ark
        $arrd['no'] = "";
        $arrd['keterangan'] = "<b>ANALISA RATIO KEUANGAN</b>";
        foreach($arrdata as $key =>$val){
            $arrd[$val['periode']] = "";
        }
        $vare[] = $arrd;

        //profit margin
        $arrd['no'] = "1";
        $arrd['keterangan'] = "<b>Profit Margin</b>";
        foreach($arrdata as $key =>$val){
            $arrd[$val['periode']] = "";
        }
        $vare[] = $arrd;

        //profit margin
        $arrd['no'] = "";
        $arrd['keterangan'] = "Ratio laba dibandingkan dengan penjualan";
        $arrratio1= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $key2 = $val['periode'];
            $jumlah = devide($arrlr[$key2],$arrpj[$key2]) * 100;
            $arrd[$key2] = string_2s($jumlah)."%";
            $arrratio1[$key2] = $jumlah;
        }
        $vare[] = $arrd;


        //ROA
        $arrd['no'] = "2";
        $arrd['keterangan'] = "<b>Return on Assets</b>";
        foreach($arrdata as $key =>$val){
            $arrd[$val['periode']] = "";
        }
        $vare[] = $arrd;

        //roa
        $arrd['no'] = "";
        $arrd['keterangan'] = "Ratio laba dibanduingkan dengan aktiva";
        $arrratio2= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $key2 = $val['periode'];
            $jumlah = devide($arrlr[$key2],$arraktiva[$key2]) * 100;
            $arrd[$key2] = string_2s($jumlah)."%";
            $arrratio2[$key2] = $jumlah;
        }
        $vare[] = $arrd;

        //curr ratio
        $arrd['no'] = "3";
        $arrd['keterangan'] = "<b>Current Ratio</b>";
        foreach($arrdata as $key =>$val){
            $arrd[$val['periode']] = "";
        }
        $vare[] = $arrd;

        //curr ratio
        $arrd['no'] = "";
        $arrd['keterangan'] = "Aktiva lancar dibanding dengan hutang";
        $arrratio3= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $key2 = $val['periode'];
            $jumlah = devide($arrjmlaktivalcr[$key2],$arrhd[$key2]) * 100;
            $arrd[$key2] = string_2s($jumlah)."%";
            $arrratio3[$key2] = $jumlah;
            $i++;
        }
        $vare[] = $arrd;

        $vare 	= array("total"=>count($vare), "records"=>$vare ) ; 
        echo(json_encode($vare)) ; 
    }

    public function initreport(){
        $n=0;
        $va     = $this->input->post() ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        echo(' bos.rptbulanan.openreport() ; ') ;
    }

    public function showreport(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $arrkolom = array();
        $arrkolom2 = array();
        $arrkolom3 = array();
        $arrkolom['no'] = array("caption"=>"No","width"=>5,"justification"=>"right");
        $arrkolom['keterangan'] = array("caption"=>"Keterangan","wrap"=>1);
        $arrkolom2['keterangan'] = array("caption"=>"Keterangan","wrap"=>1);
        $arrkolom3['no'] = array("caption"=>"No","width"=>5,"justification"=>"right");
        $arrkolom3['keterangan'] = array("caption"=>"Keterangan","wrap"=>1);
        $arrkolom4['no'] = array("caption"=>"No","width"=>5,"justification"=>"right");
        $arrkolom4['keterangan'] = array("caption"=>"Keterangan","wrap"=>1);

        $arrtw = array("I","II","III","IV");
        $vare   = array() ;
        $arrdata = array();
        $arrperiod = explode("-",$va['periode']);
        
        $time = mktime(0,0,0,$arrperiod[0]+1,0,$arrperiod[1]);//"01-".$val['bln']."-".$thn;
        $tglakhir = date("d-m-Y",$time);

        $time2 = mktime(0,0,0,$arrperiod[0],1,$arrperiod[1]);//"01-".$val['bln']."-".$thn;
        $tglawal = date("d-m-Y",$time2);

            $arrkolom[$va['periode']] = array("caption"=>$va['periode'],"width"=>20,"justification"=>"right");
            $arrkolom2[$va['periode']] = array("caption"=>$tglakhir,"width"=>20,"justification"=>"right");
            $arrkolom3[$va['periode']] = array("caption"=>$tglakhir,"width"=>20,"justification"=>"right");
            $arrkolom4[$va['periode']] = array("caption"=>$tglakhir,"width"=>20,"justification"=>"right");
            $arrdata[] = array("periode"=>$va['periode'],"tglawal"=>$tglawal,"tglakhir"=>$tglakhir);
        

        $n = 0 ;

        $arrd = array();
        //laba usaha
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Laba Usaha";
        $arrtwlr = array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $arrlr = $this->perhitungan_m->getlr($val['tglawal'],$val['tglakhir']);
            $arrd[$va['periode']] = string_2s($arrlr['lrstlhpjk']['saldoakhirperiod']);
            $arrlr[$va['periode']] = $arrlr['lrstlhpjk']['saldoakhirperiod'];
        }
        $vare[] = $arrd;

        //penjualan
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Penjualan";
        $arrpj = array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkpjawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkpjakhir") ;
            $debet = $this->perhitungan_m->getdebet($val['tglawal'],$val['tglakhir'],$rekening,'',$rekening2);
            $kredit = $this->perhitungan_m->getkredit($val['tglawal'],$val['tglakhir'],$rekening,'',$rekening2);
            $jumlah = $kredit - $debet;
            $arrd[$va['periode']] = string_2s($jumlah);
            $arrpj[$va['periode']] = $jumlah;
        }
        $vare[] = $arrd;

        //aktiva
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Aktiva";
        $arraktiva = array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = "1";
            $rekening2 = "1.9999";
            /*$debet = $this->perhitungan_m->getdebet($val['tglawal'],$val['tglakhir'],$rekening,'',$rekening2);
            $kredit = $this->perhitungan_m->getkredit($val['tglawal'],$val['tglakhir'],$rekening,'',$rekening2);
            $jumlah = $debet - $kredit;*/
             $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$va['periode']] = string_2s($jumlah);
            $arraktiva[$va['periode']] = $jumlah;
        }
        $vare[] = $arrd;

        $vare2 = array();
        $arrd = array();
        $arrd['keterangan'] = "<b>Saldo";
        foreach($arrdata as $key =>$val){
            $arrd[$va['periode']] = $val['tglakhir']."</b>";
        }
        $vare2[]=$arrd;

        //kas dan bank
        $n++;
        $arrd = array();
        $vare3 = array();
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Kas dan Bank";
        $arrkb = array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkkbawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkkbakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$va['periode']] = string_2s($jumlah);
            $arrkb[$va['periode']] = $jumlah;
        }
        $vare3[] = $arrd;


        //piutang
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Piutang";
        $arrpiut = array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkpiutangawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkpiutangakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$va['periode']] = string_2s($jumlah);
            $arrpiut[$va['periode']] = $jumlah;
        }
        $vare3[] = $arrd;

        //persediaan
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Persediaan";
        $arrpersd= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkpsdawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkpsdakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$va['periode']] = string_2s($jumlah);
            $arrpersd[$va['periode']] = $jumlah;
        }
        $vare3[] = $arrd;

        //porskot
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Porsekot";
        $arrpskt= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkpsktawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkpsktakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$va['periode']] = string_2s($jumlah);
            $arrpskt[$va['periode']] = $jumlah;
        }
        $vare3[] = $arrd;

        //Jml aktiva lancar
        $arrd['no'] = "";
        $arrd['keterangan'] = "<b>Jumlah Aktiva Lancar";
        $arrjmlaktivalcr= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $key2 = $va['periode'];
            $jumlah = $arrkb[$key2] + $arrpiut[$key2]  + $arrpersd[$key2] + $arrpskt[$key2];
            $arrd[$key2] = string_2s($jumlah)."%</b>";
            $arrjmlaktivalcr[$key2] = $jumlah;
        }
        $vare3[] = $arrd;

        //space
        $arrd['no'] = "";
        $arrd['keterangan'] = "";
        $arrd['keterangan'] = "Aktiva Tetap";
        $arrat= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkatawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkatakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$va['periode']] = string_2s($jumlah);
            $arrat[$va['periode']] = $jumlah;
        }
        $vare3[] = $arrd;

        //Aktiva Tidak Berwujud
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Aktiva Tidak Berwujud";
        $arratw= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkatwawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkatwakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$val['periode']] = string_2s($jumlah);
            $arratw[$val['periode']] = $jumlah;
            $i++;
            if($i == 1){
                $t1 = $jumlah;
            }else{
                $t2 = $jumlah;
            }
        }
        $vare[] = $arrd;


        //Aktiva Lain-Lain
        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Aktiva Lain-Lain";
        $arrall= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkallawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkallakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$va['periode']] = string_2s($jumlah);
            $arrall[$va['periode']] = $jumlah;
        }
        $vare3[] = $arrd;

        //Jml aktiva tetap
        $arrd['no'] = "";
        $arrd['keterangan'] = "<b>Jumlah Aktiva Tetap";
        $arrjmlaktivat= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $key2 = $va['periode'];
            $jumlah = $arrat[$key2] + $arratw[$key2]  + $arrall[$key2];
            $arrd[$key2] = string_2s($jumlah)."</b>";
            $arrjmlaktivat[$key2] = $jumlah;
        }
        $vare3[] = $arrd;

        $arrd['no'] = "";
        $arrd['keterangan'] = "<b>Total Aktiva";
        $arrtotaktiva= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $key2 = $va['periode'];
            $jumlah = $arrjmlaktivalcr[$key2] + $arrjmlaktivat[$key2];
            $arrd[$key2] = string_2s($jumlah)."</b>";
            $arrtotaktiva[$key2] = $jumlah;
        }
        $vare3[] = $arrd;

        //space
        $arrd['no'] = "";
        $arrd['keterangan'] = "";
        foreach($arrdata as $key =>$val){
            $arrd[$va['periode']] = "";
        }
        $vare3[] = $arrd;

        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Hutang Dagang";
        $arrhd= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkhdawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkhdakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$va['periode']] = string_2s($jumlah);
            $arrhd[$va['periode']] = $jumlah;
        }
        $vare3[] = $arrd;

        $n++;
        $arrd['no'] = $n;
        $arrd['keterangan'] = "Hutang Bank/ Jangka Panjang";
        $arrhb= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $rekening = $this->bdb->getconfig("rekarkhbawal") ;
            $rekening2 = $this->bdb->getconfig("rekarkhbakhir") ;
            $jumlah = $this->perhitungan_m->getsaldo($val['tglakhir'],$rekening,$rekening2);
            $arrd[$va['periode']] = string_2s($jumlah);
            $arrhb[$va['periode']] = $jumlah;
        }
        $vare3[] = $arrd;

        //JML HUTANG
        $arrd['no'] = "";
        $arrd['keterangan'] = "<b>Jumlah Hutang";
        $arrjmlhut= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $key2 = $va['periode'];
            $jumlah = $arrhd[$key2] + $arrhb[$key2];
            $arrd[$key2] = string_2s($jumlah)."</b>";
            $arrjmlhut[$key2] = $jumlah;
        }
        $vare3[] = $arrd;

        //ark
        $vare4 = array();
        $arrd = array();


        //profit margin
        $arrd['no'] = "1";
        $arrd['keterangan'] = "<b>Profit Margin</b>";
        foreach($arrdata as $key =>$val){
            $arrd[$va['periode']] = "";
        }
        $vare4[] = $arrd;

        //profit margin
        $arrd['no'] = "";
        $arrd['keterangan'] = "Ratio laba dibandingkan dengan penjualan";
        $arrratio1= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $key2 = $va['periode'];
            $jumlah = devide($arrlr[$key2],$arrpj[$key2]) * 100;
            $arrd[$key2] = string_2s($jumlah)."%";
            $arrratio1[$key2] = $jumlah;
        }
        $vare4[] = $arrd;


        //ROA
        $arrd['no'] = "2";
        $arrd['keterangan'] = "<b>Return on Assets</b>";
        foreach($arrdata as $key =>$val){
            $arrd[$va['periode']] = "";
        }
        $vare4[] = $arrd;

        //roa
        $arrd['no'] = "";
        $arrd['keterangan'] = "Ratio laba dibanduingkan dengan aktiva";
        $arrratio2= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $key2 = $va['periode'];
            $jumlah = devide($arrlr[$key2],$arraktiva[$key2]) * 100;
            $arrd[$key2] = string_2s($jumlah)."%</b>";
            $arrratio2[$key2] = $jumlah;
        }
        $vare4[] = $arrd;

        //curr ratio
        $arrd['no'] = "3";
        $arrd['keterangan'] = "<b>Current Ratio</b>";
        foreach($arrdata as $key =>$val){
            $arrd[$va['periode']] = "";
        }
        $vare4[] = $arrd;

        //curr ratio
        $arrd['no'] = "";
        $arrd['keterangan'] = "Aktiva lancar dibanding dengan hutang";
        $arrratio3= array();
        $t2 = 0 ;
        $t1 = 0 ;
        $i =0;
        foreach($arrdata as $key =>$val){
            $key2 = $val['periode'];
            $jumlah = devide($arrjmlaktivalcr[$key2],$arrhd[$key2]) * 100;
            $arrd[$key2] = string_2s($jumlah)."%</b>";
            $arrratio3[$key2] = $jumlah;
        }
        $vare4[] = $arrd;

        $arrkolom['pers'] = array("caption"=>"%","width"=>12,"justification"=>"right");
        $arrkolom2['pers'] = array("caption"=>"%","width"=>12,"justification"=>"right");
        $arrkolom3['pers'] = array("caption"=>"%","width"=>12,"justification"=>"right");
        $arrkolom4['pers'] = array("caption"=>"%","width"=>12,"justification"=>"right");

        if(!empty($vare)){ 
            $font = 10 ;
            $o    = array('paper'=>'A4', 'orientation'=>'portrait', 'export'=>"",
                          'opt'=>array('export_name'=>'DaftarNeraca_' . getsession($this, "username") ) ) ;
            $this->load->library('bospdf', $o) ;   
            $this->bospdf->ezText("<b>ANALISA RATIO KEUANGAN BULANAN</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ; 
            $this->bospdf->ezTable($vare,"","",  
                                   array("showHeadings"=>1,"showLines"=>2,"fontSize"=>$font,"cols"=>$arrkolom)) ;   
            //print_r($data) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vare2,"","",  
                                   array("showHeadings"=>0,"showLines"=>0,"fontSize"=>$font,"cols"=>$arrkolom2)) ;   
            $this->bospdf->ezTable($vare3,"","",  
                                   array("showHeadings"=>0,"showLines"=>2,"fontSize"=>$font,"cols"=>$arrkolom3)) ;   
            
            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("<b>ANALISA RATIO KEUANGAN </b>") ;
            $this->bospdf->ezTable($vare4,"","",  
                                   array("showHeadings"=>0,"showLines"=>0,"fontSize"=>$font,"cols"=>$arrkolom4)) ;   
            $this->bospdf->ezStream() ; 
        }else{
            echo('data kosong') ;
        }
    }
}
?>
