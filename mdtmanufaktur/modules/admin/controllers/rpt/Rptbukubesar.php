<?php
class Rptbukubesar extends Bismillah_Controller{
	protected $bdb ;
	public function __construct(){
		parent::__construct() ;
		$this->load->model("rpt/rptbukubesar_m") ;
		$this->load->model("func/perhitungan_m") ;
		$this->load->model("func/func_m") ;
		$this->bdb 	= $this->rptbukubesar_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptbukubesar_" ;
	}  

	public function index(){
		$this->load->view("rpt/rptbukubesar") ; 

	}  

   public function loadgrid_where($bs, $s=''){
      $this->duser();

      $bs['tglawal'] = date_2s($bs['tglawal']);
      $bs['tglakhir'] = date_2s($bs['tglakhir']);

      $this->db->where('rekening', $bs['rekening']);
      $this->db->where('tgl >=', $bs['tglawal']);
      $this->db->where('tgl <=', $bs['tglakhir']);

      // print_r($bs);

      if($bs['skd_cabang'] !== 'null'){
          $bs['skd_cabang'] = json_decode($bs['skd_cabang'],true);
          $this->db->group_start();
          foreach($bs['skd_cabang'] as $kdc){
              $this->db->or_where('cabang',$kdc);
          }
          $this->db->group_end();

      }else{
          if($this->aruser['level'] !== '0000'){
              if(is_array($this->aruser['data_var']['cabang'])){
                  $this->db->group_start();
                  foreach($this->aruser['data_var']['cabang'] as $kdc){
                      $this->db->or_where('cabang', $kdc);
                  }
                  $this->db->group_end();
              }
          }
      }

      if($bs['skd_faktur'] !== 'null'){
         $bs['skd_faktur'] = json_decode($bs['skd_faktur'],true);
         $this->db->group_start();
         foreach($bs['skd_faktur'] as $kdf){
             $this->db->or_like('faktur',$kdf,'after');
         }
         $this->db->group_end();
     }
   }

	public function loadgrid(){
      $this->duser();

      $va         = json_decode($this->input->post('request'), true) ;
      $search	    = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search     = $this->db->escape_like_str($search) ;
      $va['tglawal'] = date_2s($va['tglawal']);
      $va['tglakhir'] = date_2s($va['tglakhir']);
      $vare  = array() ;
      

      if($va['skd_cabang'] !== 'null'){
         $cabang = json_decode($va['skd_cabang'],true);
      }else{
            if($this->aruser['level'] !== '0000'){
               if(is_array($this->aruser['data_var']['cabang'])){
                  $cabang = $this->aruser['data_var']['cabang'];
               }
            }else{
               $dbd2 = $this->db->select("kode")->from("cabang")->get();
               
               foreach($dbd2->result_array() as $r2){
                  $cabang[] = $r2['kode'];
               }
            }
      }
      
      $saldoawal = $this->perhitungan_m->getsaldoawal($va['tglawal'], $va['rekening'],'', '', $cabang);
      $vare[0] = array("tgl"=>"","faktur"=>"","keterangan"=>"Saldo Awal","debet"=>"","kredit"=>"","total"=> $saldoawal,"username"=>"") ;

      $total = $saldoawal ;
      $totdebet = 0 ;
      $totkredit = 0 ;
      $c = substr($va['rekening'],0,1);

      $this->loadgrid_where($va, $search);
      $f2 = "tgl,faktur,keterangan,debet,kredit,username";
      $dbd2 = $this->db->select($f2)
          ->from("keuangan_bukubesar")
          ->order_by("datetime asc")
          ->get();
      foreach($dbd2->result_array() as $dbr){
         $vs = $dbr;
         $vs['tgl'] = date_2d($vs['tgl']) ;  

         if($va['skd_faktur'] == 'null'){
            if($c == "1" || $c == "5" || $c == "6" || $c == "8" || $c == "9"){
               $total += $vs['debet'] - $vs['kredit'] ;
            }else{
               $total += $vs['kredit'] - $vs['debet'];
            }
            $vs['total'] = $total ;
         }

         $totdebet += $vs['debet'];
         $totkredit += $vs['kredit'];
         
         

         $vare[]		= $vs ;
      }
      $vare[] = array("recid"=>'ZZZZ',"tgl"=> '', "faktur"=> '','keterangan'=>'TOTAL',
                        "debet"=>$totdebet,"kredit"=>$totkredit,"total"=>"","username"=>"","w2ui"=>array("summary"=> true));
      $vare    = array("total"=>count($vare) - 1, "records"=>$vare ) ;

      echo(json_encode($vare)) ;
   }
   
   

   public function initreport(){
      $this->duser();
	   $va     = $this->input->post() ;
      savesession($this, $this->ss . "va", json_encode($va) ) ;
      $vare   = array() ;
      $n = 1 ; 

      if($va['skd_cabang'] !== 'null'){
         $cabang = json_decode($va['skd_cabang'],true);
      }else{
            if($this->aruser['level'] !== '0000'){
               if(is_array($this->aruser['data_var']['cabang'])){
                  $cabang = $this->aruser['data_var']['cabang'];
               }
            }else{
               $dbd2 = $this->db->select("kode")->from("cabang")->get();
               
               foreach($dbd2->result_array() as $r2){
                  $cabang[] = $r2['kode'];
               }
            }
      }
      
      $saldoawal = $this->perhitungan_m->getsaldoawal($va['tglawal'], $va['rekening'],'', '', $cabang);

      $vare[] = array("no"=>$n,"tgl"=>"","faktur"=>"","keterangan"=>"Saldo Awal","debet"=>"","kredit"=>"","total"=> string_2s($saldoawal),"username"=>"") ;
	  
      $total = $saldoawal ;
      $c = substr($va['rekening'],0,1);

      $this->loadgrid_where($va, "");
      $f2 = "tgl,faktur,keterangan,debet,kredit,username";
      $dbd2 = $this->db->select($f2)
          ->from("keuangan_bukubesar")
          ->order_by("datetime asc")
          ->get();
      foreach($dbd2->result_array() as $dbr){
         $vs = $dbr;
         $vs['tgl'] = date_2d($vs['tgl']) ;  
         $vs['no'] = $n++;
         $vs['total'] = 0;
         if($va['skd_faktur'] == 'null'){
            if($c == "1" || $c == "5" || $c == "6" || $c == "8" || $c == "9"){
               $total += $vs['debet'] - $vs['kredit'] ;
            }else{
               $total += $vs['kredit'] - $vs['debet'];
            }
            $vs['total'] = $total ;

         }
         
         $vs['debet'] = string_2s($vs['debet']);
         $vs['kredit'] = string_2s($vs['kredit']);
         $vs['total'] = string_2s($vs['total']);

         $vare[]		= $vs ;
      }

      savesession($this, "rptbukubesar_rpt", json_encode($vare)) ;
      echo(' bos.rptbukubesar.openreport() ; ') ;
	}

	public function showreport(){
      $this->duser();
      $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
      $data = getsession($this,"rptbukubesar_rpt") ;      
      $data = json_decode($data,true) ;    
      if(!empty($data)){ 
         $font = 8 ;
         
         $arrtotal = array();
         $arrtotal[0] = array("keterangan"=>"<b>Total","debet"=>0,"kredit"=>0,"ket2"=>"</b>");
         foreach($data as $key => $val){
            if($val['debet'] <> "") $arrtotal[0]['debet'] += string_2n($val['debet']);
            if($val['kredit'] <> "")$arrtotal[0]['kredit'] += string_2n($val['kredit']);
         }
         $arrtotal[0]['debet'] = string_2s($arrtotal[0]['debet']);
         $arrtotal[0]['kredit'] = string_2s($arrtotal[0]['kredit']);

         $o    = array('paper'=>'A4', 'orientation'=>'portrait', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                           'opt'=>array('export_name'=>'DaftarBukuBesar_' . getsession($this, "username") ) ) ;
         
         if($va['skd_cabang'] !== 'null'){
            $cabang = json_decode($va['skd_cabang'],true);
         }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['cabang'])){
                    $cabang = $this->aruser['data_var']['cabang'];
                }
            }else{
                $dbd2 = $this->db->select("kode")->from("cabang")->get();
                
                foreach($dbd2->result_array() as $r2){
                    $cabang[] = $r2['kode'];
                }
            }
         }
              

         $faktur = ($va['skd_faktur'] !== 'null') ? " Faktur : ".$va['skd_faktur'] : "";
         $this->load->library('bospdf', $o) ;   

         $arrcab = $this->func_m->GetDataCabang($cabang[0]);
            
         if(count($cabang) == 1){
             $this->bospdf->ezText($arrcab['nama'],$font+4,array("justification"=>"center")) ;
         }else{
             $cabkonsol = "(".implode(",",$cabang).")";
             $this->bospdf->ezText($this->bdb->getconfig("app_company"),$font+4,array("justification"=>"center")) ;
             $this->bospdf->ezText("Konsolidasi : ".$cabkonsol,$font,array("justification"=>"center")) ;
         }

         $this->bospdf->ezText($arrcab['alamat'] . " / ". $arrcab['telp'],$font,array("justification"=>"center")) ;
         $this->bospdf->ezText("<b>LAPORAN BUKU BESAR</b>",$font+4,array("justification"=>"center")) ;
         $this->bospdf->ezText("Periode : ". $va['tglawal'] . " sd " . $va['tglakhir'],$font+2,array("justification"=>"center")) ;
         $this->bospdf->ezText("Rekening : ". $va['rekening'] . $faktur,$font+1,array("justification"=>"center")) ;
         $this->bospdf->ezText("") ; 
         $this->bospdf->ezTable($data,"","",  
                           array("fontSize"=>$font,"cols"=> array( 
                                 "no"=>array("caption"=>"No","width"=>5,"justification"=>"right"),
                                 "tgl"=>array("caption"=>"Tgl","width"=>10,"justification"=>"center"),
                                 "faktur"=>array("caption"=>"Faktur","width"=>16,"justification"=>"center"),
                                 "keterangan"=>array("caption"=>"Keterangan","wrap"=>1),
                                 "debet"=>array("caption"=>"Debet","width"=>13,"justification"=>"right"),
                                 "kredit"=>array("caption"=>"Kredit","width"=>13,"justification"=>"right"),
                                 "total"=>array("caption"=>"Saldo","width"=>15,"justification"=>"right"),
                              "username"=>array("caption"=>"Username","width"=>8)))) ;   
         
         $this->bospdf->ezTable($arrtotal,"","",  
                           array("fontSize"=>$font,"showHeadings"=>0,"cols"=> array( 
                                 "keterangan"=>array("caption"=>"Keterangan","justification"=>"center","wrap"=>1),
                                 "debet"=>array("caption"=>"Debet","width"=>13,"justification"=>"right"),
                                 "kredit"=>array("caption"=>"Kredit","width"=>13,"justification"=>"right"),
                                 "ket2"=>array("caption"=>"Username","width"=>23)))) ;   
         //print_r($data) ;    
         $this->bospdf->ezStream() ; 
      }else{
         echo('data kosong') ;
      }
   }

   public function refresh(){
      echo('
         bos.rptbukubesar.grid1_reloaddata() ;
      ');
   }

}
?>
