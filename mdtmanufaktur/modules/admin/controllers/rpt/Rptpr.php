<?php

class Rptpr extends Bismillah_Controller{
   protected $bdb ;
   protected $ss ;
   protected $abc ;
   public function __construct(){
      parent::__construct() ;
      $this->load->helper('bdate');
      $this->load->model('rpt/rptpr_m') ;
      $this->load->model('func/Perhitungan_m') ;
      $this->load->model('func/func_m') ;
      $this->bdb = $this->rptpr_m ;
      $this->ss  = getsession($this, "username")."-" . "ssrptpr_" ;
   }

   public function index(){
      $d    = array("setdate"=>date_set()) ;
      $this->load->view('rpt/rptpr', $d) ;
   }

   public function loadgrid_where($bs, $s){
      $this->duser();

      $this->db->where('t.status', "1");
      $this->db->where('t.tgl >=', $bs['tglawal']);
      $this->db->where('t.tgl <=', $bs['tglakhir']);

      
      if($bs['skd_gudang'] !== 'null'){
          $bs['skd_gudang'] = json_decode($bs['skd_gudang'],true);
          $this->db->group_start();
          foreach($bs['skd_gudang'] as $kdc){
              $this->db->or_where('t.gudang',$kdc);
          }
          $this->db->group_end();

      }else{
          if($this->aruser['level'] !== '0000'){
              if(is_array($this->aruser['data_var']['gudang'])){
                  $this->db->group_start();
                  foreach($this->aruser['data_var']['gudang'] as $kdc){
                      $this->db->or_where('t.gudang', $kdc);
                  }
                  $this->db->group_end();
              }
          }
      }

      if($s !== ''){
          $this->db->group_start();
              $this->db->or_like(array('t.faktur'=>$s, 'k.nama'=>$s));
          $this->db->group_end();
      }
   }

   public function loadgrid(){
      $va       = json_decode($this->input->post('request'), true) ;
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->db->escape_like_str($search) ;
      $va['tglawal'] = date_2s($va['tglawal']);
      $va['tglakhir'] = date_2s($va['tglakhir']);
      $vare               = array() ;
      
      $this->loadgrid_where($va, $search);
      $f = "count(t.id) jml";
      $dbd = $this->db->select($f)
          ->from("pr_total t")
          ->join("gudang s","s.kode = t.gudang","left")
          ->get();
      $rtot  = $dbd->row_array();
      if($rtot['jml'] > 0){
         $this->loadgrid_where($va, $search);
         $f2 = "t.faktur,t.tgl,s.keterangan as gudang";
         $dbd2 = $this->db->select($f2)->from("pr_total t")
             ->join("gudang s","s.kode = t.gudang","left")
             ->limit($limit)
             ->get();
   
         foreach($dbd2->result_array() as $r2){
            $vaset                  = $r2 ;
            $vaset['tgl']           = date_2d($vaset['tgl']);
            $vaset['cmdPreview']    = '<button type="button" onClick="bos.rptpr.cmdpreviewdetail(\''.$r2['faktur'].'\')"
                                    class="btn btn-success btn-grid">Preview Detail</button>' ;
            $vaset['cmdPreview']    = html_entity_decode($vaset['cmdPreview']) ;
            
            $vare[]                 = $vaset ;
         }
      }
      $return 	= array("total"=>$rtot['jml'], "records"=>$vare ) ;
      echo(json_encode($return)) ;
   }

   public function PreviewDetail(){
      $va    = $this->input->post() ;
      $cFaktur = $va['faktur'] ;
      echo('w2ui["bos-form-rptpr_grid2"].clear();');
      $data = $this->rptpr_m->GetDataPerFaktur($cFaktur) ;
      if(!empty($data)){
         echo('
                  with(bos.rptpr.obj){
                     find("#faktur").val("'.$data['faktur'].'") ;
                     find("#gudang").val("'.$data['gudang'].'") ;
                     find("#tgl").val("'.$data['Tgl'].'") ;
                  }

              ') ;
         $data = $this->rptpr_m->getDetailPR($cFaktur) ;
         $vare = array();
         $n = 0 ;
         while($dbr = $this->rptpr_m->getrow($data)){
            $n++;
            $vaset          = $dbr ;
            $vaset['recid'] = $n;
            $vaset['no']    = $n;
            $vare[]         = $vaset ;
         }

         $vare = json_encode($vare);
         echo('
            bos.rptpr.loadmodalpreview("show") ;
            bos.rptpr.grid2_reloaddata();
            w2ui["bos-form-rptpr_grid2"].add('.$vare.');
         ');
      }
   }

   public function initreport(){
      $va      = $this->input->post() ;
      $cFaktur  = $va['cFaktur'];
      $w    = "p.faktur = '".$cFaktur."'" ;

      $file   = setfile($this, "rpt", __FILE__ , $va) ;
      savesession($this, $this->ss . "file", $file ) ;
      savesession($this, $this->ss . "va", json_encode($va) ) ;
      file_put_contents($file, json_encode(array()) ) ;

      $file    = getsession($this, $this->ss . "file") ;
      $data    = @file_get_contents($file) ;
      $data    = json_decode($data,true) ;
      $db      = $this->bdb->getDetailPR($cFaktur) ;
      $nDebet  = 0 ;
      $nKredit = 0 ;
      $nSaldo  = 0 ;
      $s = 0 ;
      while($dbr = $this->bdb->getrow($db)){
         $no      = ++$s ;
         $data[]  = array("#"=>$no,
                          "Kode"=>$dbr['kode'],
                          "Keterangan"=>$dbr['keterangan'],
                          "Qty"=>number_format($dbr['qty']),
                          "Satuan"=>$dbr['satuan']
                       ) ;
      }
      file_put_contents($file, json_encode($data) ) ;
      echo(' bos.rptpr.openreport() ; ') ;
   }

   public function initreportTotal(){
      $this->duser();

      $va         = $this->input->post() ;
      $va['tglawal']   = date_2s($va['tglawal']);
      $va['tglakhir']  = date_2s($va['tglakhir']);
      $file       = setfile($this, "rpt_prtotal", __FILE__ , $va) ;
      savesession($this, $this->ss . "file", $file ) ;
      savesession($this, $this->ss . "va", json_encode($va) ) ;
      file_put_contents($file, json_encode(array()) ) ;

      $file    = getsession($this, $this->ss . "file") ;
      $data    = @file_get_contents($file) ;
      $data    = json_decode($data,true) ;



      $this->loadgrid_where($va, "");
      $f2 = "t.faktur,t.tgl,s.keterangan as gudang,t.cabang";
      $dbd2 = $this->db->select($f2)->from("pr_total t")
          ->join("gudang s","s.kode = t.gudang","left")
          ->get();
          $s=0;
      foreach($dbd2->result_array() as $r2){
         $no      = ++$s ;
         $data[]  = array("#"=>$no,
                          "faktur"=>$r2['faktur'],
                          "tgl"=>$r2['tgl'],
                          "cabang"=>$r2['cabang'],
                          "gudang"=>$r2['gudang']
                        ) ;
      }
      
      file_put_contents($file, json_encode($data) ) ;
      echo(' bos.rptpr.openreporttotal() ; ') ;

   }

   public function showreport(){
      $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
      $file = getsession($this, $this->ss . "file") ;
      $data = @file_get_contents($file) ;
      $data = json_decode($data,true) ;
      if(!empty($data)){
         //tanda tangan
         $now  = date_2b(date("Y-m-d")) ;
         $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
         $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
         $vttd = array() ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> "" ,"5"=>$kota) ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
         $vttd[] = array("1"=>"Disetujui,","2"=>"","3"=>"Disiapkan,","4"=>"","5"=>"Diajukan,") ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
         $vttd[] = array("1"=>"(.........................)","2"=>"","3"=>"(.........................)","4"=>"","5"=>"(.........................)") ;
         $vttd[] = array("1"=>"Manager","2"=>"","3"=>"Bag.Gudang","4"=>"","5"=>"Ka.bag Produksi") ;




         $font = 8 ;
         $exTgl = explode("-",$va['tgl']);
         $dTglFaktur = $exTgl['2'] . "-" . $exTgl['1'] . "-" . $exTgl['0'];

         $vDetail = array() ;
         $vDetail[] = array("1"=>"<b>Faktur</b>","2"=>" : ","3"=> $va['cFaktur'],"4"=>"","5"=>"","6"=>"", ) ;
         $vDetail[] = array("1"=>"<b>Gudang</b>","2"=>" : ","3"=> $va['gudang'],"4"=>"","5"=>"","6"=>"", ) ;
         $vDetail[] = array("1"=>"<b>Tanggal</b>","2"=>" : ","3"=> $dTglFaktur,"4"=>"","5"=>"","6"=>"", ) ;


         $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                        'opt'=>array('export_name'=>'Kartu Stock') ) ;
         $this->load->library('bospdf', $o) ;
         $this->bospdf->ezText("<b>DETAIL SPP</b>",$font+4,array("justification"=>"center")) ;
         $this->bospdf->ezText("") ;
         $this->bospdf->ezTable($vDetail,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("width"=>10,"justification"=>"left"),
                                          "2"=>array("width"=>3,"justification"=>"left"),
                                          "3"=>array("width"=>15,"justification"=>"left"),
                                          "4"=>array("width"=>10,"justification"=>"left"),
                                          "5"=>array("width"=>10,"justification"=>"left"),
                                          "6"=>array("width"=>50,"justification"=>"left"),)
                                 )
                              ) ;

         $this->bospdf->ezText("") ;
         $this->bospdf->ezTable($data,"","",
                                 array("fontSize"=>$font,
                                       "cols"=> array(
                                             "#"            =>array("width"=>2,"justification"=>"right"),
                                             "Kode"  =>array("width"=>12,"justification"=>"center"),
                                             "Keterangan"   =>array("wrap"=>1),
                                             "Qty"          =>array("width"=>6,"justification"=>"right"),
                                             "Satuan"       =>array("width"=>9,"justification"=>"left")))
                                 ) ;


         $this->bospdf->ezText("") ;
         $this->bospdf->ezText("") ;
         $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "2"=>array("wrap"=>1,"justification"=>"center"),
                                          "3"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "4"=>array("wrap"=>1,"justification"=>"center"),
                                          "5"=>array("width"=>25,"wrap"=>1,"justification"=>"center"))
                                 )
                              ) ;
         $this->bospdf->ezStream() ;
      }else{
         echo('kosong') ;
      }
   }

   public function showreporttotal(){
      $this->duser();
      $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
      $file = getsession($this, $this->ss . "file") ;
      $data = @file_get_contents($file) ;
      $data = json_decode($data,true) ;
      if(!empty($data)){
         //tanda tangan
         $now  = date_2b(date("Y-m-d")) ;
         $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
         $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
         $vttd = array() ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> $kota ,"5"=>"") ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"Bag Pembelian,","5"=>"") ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
         $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"(.........................)","5"=>"") ;

         $font = 8 ;
         $o    = array('paper'=>'A4', 'orientation'=>'P', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                        'opt'=>array('export_name'=>'Kartu Stock') ) ;
         $this->load->library('bospdf', $o) ;


         if($va['skd_gudang'] !== 'null'){
            $gudang = json_decode($va['skd_gudang'],true);
        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['gudang'])){
                    $gudang = $this->aruser['data_var']['gudang'];
                }
            }else{
                $dbd2 = $this->db->select("kode")->from("gudang")->get();
                
                foreach($dbd2->result_array() as $r2){
                    $gudang[] = $r2['kode'];
                }
            }
        }



        $arrinf = $this->func_m->getinfogudang($gudang[0]);
        $arrcab = $this->func_m->GetDataCabang($arrinf['cabang']);
        
        if(count($gudang) == 1){
            $this->bospdf->ezText($arrinf['ketcabang'],$font+4,array("justification"=>"center")) ;
        }else{
            $konsol = "(".implode(",",$gudang).")";
            $this->bospdf->ezText($this->bdb->getconfig("app_company"),$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("Konsolidasi Gudang : ".$konsol,$font,array("justification"=>"center")) ;
        }
        $this->bospdf->ezText($arrcab['alamat'] . " / ". $arrcab['telp'],$font,array("justification"=>"center")) ;
         $this->bospdf->ezText("<b>LAPORAN TOTAL SPP<b>",$font+4,array("justification"=>"center")) ;
         $this->bospdf->ezText("<b>Periode : " .$va['tglawal']. " s/d " . $va['tglakhir'] . "</b>",$font+4,array("justification"=>"center")) ;
         $this->bospdf->ezText("") ;
          $this->bospdf->ezTable($data,"","",
                                 array("fontSize"=>$font,
                                       "cols"=> array(
                                             "#"=>array("caption"=>"No","width"=>5,"justification"=>"right"),
                                             "faktur"=>array("caption"=>"Faktur","width"=>15,"wrap"=>1,"justification"=>"center"),
                                             "tgl"=>array("caption"=>"Tgl","width"=>10,"wrap"=>1,"justification"=>"center"),
                                             "cabang"=>array("caption"=>"Cabang","width"=>6,"justification"=>"center")
                                          ))
                                 ) ;

         $this->bospdf->ezText("") ;
         $this->bospdf->ezText("") ;
         $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("justification"=>"right"),
                                          "2"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "3"=>array("width"=>40,"wrap"=>1),
                                          "4"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "5"=>array("wrap"=>1,"justification"=>"center"))
                                 )
                              ) ;
         $this->bospdf->ezStream() ;
      }else{
         echo('kosong') ;
      }
   }
}

?>
