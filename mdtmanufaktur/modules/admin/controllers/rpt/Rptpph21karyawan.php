<?php
class Rptpph21karyawan extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("rpt/rptpph21karyawan_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->load->model("func/func_m") ;
        
        $this->bdb 	= $this->rptpph21karyawan_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptpph21karyawan2_" ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";

        $this->load->view("rpt/rptpph21karyawan",$data) ;

    } 

   
    public function loadgrid(){
        $va = json_decode($this->input->post('request'), true);
        if(!isset($va['thn']))$va['thn'] = "";
        $va['tglawal'] = $va['thn'].'-01-01';
        $va['tglakhir'] = $va['thn'].'-12-31';
        // print_r($va);

        $vare   = array() ;
        $vdb    = $this->rptpph21karyawan_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;
        
        while( $dbr = $this->rptpph21karyawan_m->getrow($dbd) ){
            $n++;

            //jab bag
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$va['tglakhir']);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['kode'],$va['tglakhir']);
            $dbr['bagian'] = $bagian['keterangan'];
            $dbr['jabatan'] = $jabatan['keterangan'];

            $vaset = $dbr;            

            $pph21 = $this->perhitunganhrd_m->getpph21posting($dbr['kode'],$va['tglawal'],$va['tglakhir']);
            $vaset['kodeptkp'] = $pph21['ptkp'];
            $vaset['ptkp'] = $pph21['nilaiptkp'];
            $vaset['penambah'] = $pph21['penambah'];
            $vaset['pengurang'] = $pph21['pengurang'];
            $vaset['neto'] = $pph21['neto'];
            $vaset['pkp'] = $pph21['pkp'];
            $vaset['pph21'] = $pph21['pph21'];
            $vaset['cetak'] = "Tidak";
            $vaset['recid'] = $n;
            $vare[] = $vaset;
            
        }

        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function printpph21karyawan(){
        $va     = $this->input->post() ;

        if(!isset($va['thn']))$va['thn'] = "";
        $va['tglawal'] = $va['thn'].'-01-01';
        $va['tglakhir'] = $va['thn'].'-12-31';

        $vaGrid = json_decode($va['grid1']);
        
       //print_r($vaGrid);
        foreach($vaGrid as $key => $val){
            //echo($val->cetakslip);
            if($val->cetak == "Ya"){
                //load data karaywan
                $karywn   = $this->perhitunganhrd_m->getdatakaryawan($val->kode,$va['tglakhir']) ;
                //echo($val->kode);
                //$dbd    = $vdb['db'] ;
                $n = 0 ;
                $arrdata[$val->kode] = array("kode"=>$val->kode,"nama"=>$karywn['nama'],"alamat"=>$karywn['alamat'],"ketjabatan"=>$karywn['ketjabatan'],
                                       "ketbagian"=>$karywn['ketbagian'],"pph21"=>array());
                
                $pph21 = $this->perhitunganhrd_m->getpph21posting($val->kode,$va['tglawal'],$va['tglakhir']);

                $arrdata[$val->kode]['pph21'] = $pph21;

            }
        }
        $data = htmlspecialchars(json_encode($arrdata));
        
        echo('
            bos.rptpph21karyawan.openreportpph21karyawan("'.$data.'");
        ');
    }

    public function showreporpph21karyawan(){
        $va     = $this->input->get() ;
        $arrdata = json_decode($va['data']);
        $data = getsession($this,"rptpph21karyawan_rpt") ;      
        $data = json_decode($data,true) ;

        $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>0,
                          'opt'=>array('export_name'=>'rptpph21karyawan_' . getsession($this, "username") ) ) ;
        $this->load->library('bospdf', $o) ; 


        $font = 10;
        $row = 0 ;
        foreach($arrdata as $key => $val){
            $row++;
            $header = array();
            $header[] = array("1"=>"NIP","2"=>":","3"=>$val->kode,"4"=>"","5"=>"Jabatan","6"=>":","7"=>$val->ketjabatan);
            $header[] = array("1"=>"Nama","2"=>":","3"=>$val->nama,"4"=>"","5"=>"Bagian","6"=>":","7"=>$val->ketbagian);

            $body = array();
            $n = 0 ;

            //penambahan
            //print_r($val->pph21->detailpenambahan);
            $body[] = array("Keterangan"=>"<b>::Penambah</b>","1"=>"","2"=>"");
            foreach($val->pph21->detailpenambahan as $key2 => $val2){
                $body[] = array("Keterangan"=>$val2->keterangan,"1"=>string_2s($val2->nominal),"2"=>"");
            }
            $body[] = array("Keterangan"=>"<b>Total Penambah","1"=>"","2"=>string_2s($val->pph21->penambah)."</b>");

            $body[] = array("Keterangan"=>"<b>::Pengurang</b>","1"=>"","2"=>"");
            foreach($val->pph21->detailpengurang as $key2 => $val2){
                $body[] = array("Keterangan"=>$val2->keterangan,"1"=>string_2s($val2->nominal),"2"=>"");
            }
            $body[] = array("Keterangan"=>"<mdtlr01><b>Total Pengurang","1"=>"","2"=>string_2s($val->pph21->pengurang)."</b></mdtlr01>");


            $body[] = array("Keterangan"=>"Penghasilan Neto","1"=>"","2"=>string_2s($val->pph21->neto));
            $body[] = array("Keterangan"=>"<mdtlr01>Penghasilan Tidak kena Pajak (PTKP) ".$val->pph21->ptkp,"1"=>"","2"=>"(".string_2s($val->pph21->nilaiptkp).")</mdtlr01>");
            $body[] = array("Keterangan"=>"Penghasilan Kena Pajak (PKP) ","1"=>"","2"=>string_2s($val->pph21->pkp));

            $body[] = array("Keterangan"=>"<b>::PPh21 Terhutang</b>","1"=>"","2"=>"");
            foreach($val->pph21->detailprogressif as $key2 => $val2){
                $body[] = array("Keterangan"=>"PPh 21 Terhutang ".$val2->perstarif ."% x ".string_2s($val2->nilaipkpprog),"1"=>string_2s($val2->pph21),"2"=>"");
            }
            $body[] = array("Keterangan"=>"<mdtlr01><b>Total PPh21 Terhutang","1"=>"","2"=>string_2s($val->pph21->pph21)."</b></mdtlr01>");

            if($row>1)$this->bospdf->ezNewPage(0) ; 
            //report
            $this->bospdf->ezText("<b>PPh 21 Karyawan</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("Tahun : ".$va['thn'],$font+1,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ;

            $this->bospdf->ezTable($header,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("width"=>15,"justification"=>"left"),
                                          "2"=>array("width"=>5,"justification"=>"center"),
                                          "3"=>array("justification"=>"left"),
                                          "4"=>array("width"=>10,"justification"=>"center"),
                                          "5"=>array("width"=>15,"justification"=>"left"),
                                          "6"=>array("width"=>5,"justification"=>"center"),
                                          "7"=>array("justification"=>"left"))
                                 )
                              ) ;
            
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($body,"","",
                                 array("fontSize"=>$font,"showHeadings"=>1,"showLines"=>1,
                                       "cols"=> array(
                                          "Keterangan"=>array("justification"=>"left"),
                                          "1"=>array("width"=>20,"justification"=>"right"),
                                          "2"=>array("width"=>20,"justification"=>"right"))
                                 )
                              ) ;
                              
        }
        $this->bospdf->ezStream() ; 

    }
}
?>
