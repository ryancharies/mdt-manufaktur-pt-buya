<?php

class Rptkartupiutang extends Bismillah_Controller{
    protected $bdb ;
    protected $ss ;
    protected $abc ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper('bdate');
        $this->load->model('rpt/rptkartupiutang_m') ;
        $this->load->model('func/perhitungan_m') ;
        $this->bdb = $this->rptkartupiutang_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrptkartupiutang_" ;
    }

    public function index(){
        $d    = array("setdate"=>date_set()) ;
        $this->load->view('rpt/rptkartupiutang', $d) ;
    }

    public function loadgrid_where($bs, $s=''){
        $this->duser();

        $bs['tglawal'] = date_2s($bs['tglawal']);
        $bs['tglakhir'] = date_2s($bs['tglakhir']);

        $this->db->where('customer',$bs['customer']);
        $this->db->where('tgl >=', $bs['tglawal']);
        $this->db->where('tgl <=', $bs['tglakhir']);
        $this->db->where('jenis',"P");

        
        if(!empty($bs['fkt']))$this->db->where('fkt',$bs['fkt']);
        
        
    }

    public function loadgrid(){        
        $va = json_decode($this->input->post('request'), true);
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->db->escape_like_str($search) ;
        $tglKemarin = date("Y-m-d",strtotime($va['tglawal'])-(60*60*24));
        $vare = array();
        $saldo = $this->perhitungan_m->GetSaldoAkhirPiutang($va['customer'],$tglKemarin,$va['fkt']) ;
        $vare[] = array("no"=>"","faktur"=>"","tgl"=>"","keterangan"=>"Saldo Awal","debet"=>"","kredit"=>"","saldo"=>$saldo);
        $totdebet = 0 ;
        $totkredit = 0 ;

        $this->loadgrid_where($va, $search);
        $f = "count(id) jml";
        $dbdt = $this->db->select($f)
            ->from("piutang_kartu")
            ->get();
        $rtot  = $dbdt->row_array();
        if($rtot['jml'] > 0){
            $this->loadgrid_where($va, $search);
            $f2 = "faktur,tgl,keterangan,debet,kredit,fkt,cabang";
            $dbd = $this->db->select($f2)
                ->from("piutang_kartu")
                ->order_by('tgl ASC,faktur asc,debet desc')
                ->get();
        
            foreach($dbd->result_array() as $dbr){  
                $saldo += $dbr['debet'] - $dbr['kredit'];
                $vaset   = $dbr ;
                $vaset['saldo'] = $saldo;
                $vaset['tgl'] = date_2d($vaset['tgl']);
                $arrdetail = $this->perhitungan_m->getdetailtransaksi($vaset['faktur']);
                if($arrdetail['ketdetail'] !== "")$vaset['keterangan'] = $vaset['keterangan']."Detail :".$arrdetail['ketdetail'];
                $vare[]     = $vaset ;
                $totdebet += $dbr['debet'];
                $totkredit += $dbr['kredit'];
            }
        }

        $rtot['jml']++;
        $vare[] = array("recid"=>'ZZZZ',"no"=> '', "faktur"=> '',"tgl"=> '', 'keterangan'=>'TOTAL',
                        "debet"=>$totdebet,"kredit"=>$totkredit,"saldo"=>"","w2ui"=>array("summary"=> true));

        $vare    = array("total"=>count($vare)-1, "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

	public function initreport(){
        $va     = $this->input->post() ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        $tglKemarin = date("Y-m-d",strtotime($va['tglawal'])-(60*60*24));
        $vare = array();
        $saldo = $this->perhitungan_m->GetSaldoAkhirPiutang($va['customer'],$tglKemarin,$va['fkt']) ;
        $vare[] = array("no"=>1,"faktur"=>"","tgl"=>"","keterangan"=>"Saldo Awal","debet"=>"","kredit"=>"","saldo"=>string_2s($saldo));
        $n = 1;
        $this->loadgrid_where($va);
        $f2 = "faktur,tgl,keterangan,debet,kredit,fkt,cabang";
        $dbd = $this->db->select($f2)
            ->from("piutang_kartu")
            ->order_by('tgl ASC,faktur asc,debet desc')
            ->get();
        foreach($dbd->result_array() as $dbr){  
            $n++;
            $saldo += $dbr['debet'] - $dbr['kredit'];
            $vaset   = $dbr ;
            $vaset['no'] = $n;
            $vaset['saldo'] = string_2s($saldo);
            $vaset['tgl'] = date_2d($vaset['tgl']);
            $vaset['debet'] = string_2s($vaset['debet']);
            $vaset['kredit'] = string_2s($vaset['kredit']);
            $arrdetail = $this->perhitungan_m->getdetailtransaksi($vaset['faktur']);
            if($arrdetail['ketdetail'] !== "")$vaset['keterangan'] = $vaset['keterangan']."\nDetail : \n".$arrdetail['ketdetail'];
            $vare[]     = $vaset ;
        }

        savesession($this, "rptkartupiutang_rpt", json_encode($vare)) ;
        echo(' bos.rptkartupiutang.openreport() ; ') ;
	}

	public function showreport(){
      $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
      $data = getsession($this,"rptkartupiutang_rpt") ;      
      $data = json_decode($data,true) ;
	  $totdebet = 0 ;
      $totkredit = 0 ;
      if (!isset($va['export'])) {
        $va['export'] = 0;
      }
	  foreach($data as $key => $val){
		if($val['debet'] == "")$val['debet'] = 0 ;
		if($val['kredit'] == "")$val['kredit'] = 0 ;
		$totdebet += string_2n($val['debet']) ;
		$totkredit += string_2n($val['kredit']) ;
	  }
	  $total = array();
	  $total[] = array("keterangan"=>"<b>Total","debet"=>string_2s($totdebet),"kredit"=>string_2s($totkredit),"ket2"=>"</b>");
      if(!empty($data)){ 
        //tanda tangan
        $now  = date_2b(date("Y-m-d")) ;
        $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
        $ttd  = json_decode($this->bdb->getconfig("ttd"), true) ;
        $vttd = array() ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> $kota ,"5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"(.........................)","3"=>"","4"=>"(.........................)","5"=>"") ;
        $vttd[] = array("1"=>"","2"=>"Bag. Keuangan","3"=>"","4"=>"Bag. Penjualan","5"=>"") ;

      	$font = 8 ;
        $o    = array('paper'=>'A4', 'orientation'=>'portrait', 'export'=>$va['export'],
                        'opt'=>array('export_name'=>'Kartupiutang_' . getsession($this, "username") ) ) ;
        $ketcutomer = $this->bdb->getval("nama", "kode = '{$va['customer']}'", "customer");
		$this->load->library('bospdf', $o) ;   
        $this->bospdf->ezText("<b>LAPORAN KARTU PIUTANG</b>",$font+4,array("justification"=>"center")) ;
        if(!empty($va['fkt']))$this->bospdf->ezText("Faktur Induk : ". $va['fkt'],$font+2,array("justification"=>"center")) ;
        $this->bospdf->ezText("Periode : ". $va['tglawal'] . " sd " . $va['tglakhir'],$font+2,array("justification"=>"center")) ;
		$this->bospdf->ezText("Customer : ".$va['customer']."-".$ketcutomer,$font+2,array("justification"=>"center")) ;
		$this->bospdf->ezText("") ; 
		$this->bospdf->ezTable($data,"","",  
								array("fontSize"=>$font,"cols"=> array( 
			                     "no"=>array("caption"=>"No","width"=>5,"justification"=>"right"),
			                     "tgl"=>array("caption"=>"Tgl","width"=>10,"justification"=>"center"),
			                     "faktur"=>array("caption"=>"Faktur","width"=>16,"justification"=>"center"),
			                     "keterangan"=>array("caption"=>"Keterangan","wrap"=>1),
			                     "debet"=>array("caption"=>"Debet","width"=>13,"justification"=>"right"),
			                     "kredit"=>array("caption"=>"Kredit","width"=>13,"justification"=>"right"),
			                     "saldo"=>array("caption"=>"Saldo","width"=>15,"justification"=>"right")))) ;  
		$this->bospdf->ezTable($total,"","",  
								array("fontSize"=>$font,"showHeadings"=>0,"cols"=> array( 
			                     "keterangan"=>array("caption"=>"Keterangan","wrap"=>1,"justification"=>"center"),
			                     "debet"=>array("caption"=>"Debet","width"=>13,"justification"=>"right"),
			                     "kredit"=>array("caption"=>"Kredit","width"=>13,"justification"=>"right"),
                                 "ket2"=>array("caption"=>"Saldo","width"=>15,"justification"=>"right")))) ; 
                                 
        $this->bospdf->ezText("") ;
        $this->bospdf->ezText("") ;
        $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("wrap"=>1,"justification"=>"center"),
                                          "2"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "3"=>array("wrap"=>1,"justification"=>"center"),
                                          "4"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "5"=>array("wrap"=>1,"justification"=>"center"))
                                          )
                              ) ;
        //print_r($data) ;    
        $this->bospdf->ezStream() ; 
      }else{
         echo('data kosong') ;
      }
	}
}

?>
