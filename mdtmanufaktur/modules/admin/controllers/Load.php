<?php
class Load extends Bismillah_Controller{
   private $bdb ;
   public function __construct(){
      parent::__construct() ;
      $this->load->model('load_m') ;
      $this->load->model('func/perhitungan_m') ;
      $this->bdb    = $this->load_m ;
   }
   
   public function load_kodefaktur(){
      $vare = array() ;  
      $vare[]    = array("id"=>'AS', "text"=>"(AS) Pembelian aset") ;
      $vare[]    = array("id"=>'PB', "text"=>"(PB) Pembelian stock") ;
      $vare[]    = array("id"=>'RB', "text"=>"(RB) Retur pembelian") ;
      $vare[]    = array("id"=>'AJ', "text"=>"(AJ) Penjualan aset") ;
      $vare[]    = array("id"=>'CS', "text"=>"(CS) Penjualan stock") ;
      $vare[]    = array("id"=>'RJ', "text"=>"(RJ) Retur penjualan") ;
      $vare[]    = array("id"=>'PR', "text"=>"(PR) Perintah produksi") ;
      $vare[]    = array("id"=>'BB', "text"=>"(BB) Proses produksi") ;
      $vare[]    = array("id"=>'PD', "text"=>"(PD) Hasil produksi") ;
      $vare[]    = array("id"=>'PS', "text"=>"(PS) Penyesuaian stock") ;
      $vare[]    = array("id"=>'SP', "text"=>"(SP) Opname stock (posting)") ;
      $vare[]    = array("id"=>'SK', "text"=>"(SK) Stock keluar") ;
      $vare[]    = array("id"=>'BC', "text"=>"(BC) Pencairan BG / CEK") ;
      $vare[]    = array("id"=>'DP', "text"=>"(DP) Mutasi Deposit") ;
      $vare[]    = array("id"=>'MU', "text"=>"(MU) Mutasi uang muka") ;
      $vare[]    = array("id"=>'MP', "text"=>"(MP) Mutasi persekot") ;
      $vare[]    = array("id"=>'MK', "text"=>"(MK) Mutasi kas") ;
      $vare[]    = array("id"=>'MB', "text"=>"(MB) Mutasi bank") ;
      $vare[]    = array("id"=>'PH', "text"=>"(PH) Pelunasan hutang") ;
      $vare[]    = array("id"=>'PP', "text"=>"(PP) Pelunasan piutang") ;
      $vare[]    = array("id"=>'JR', "text"=>"(JR) Jurnal penyesuaian") ;
      $vare[]    = array("id"=>'PA', "text"=>"(PA) Penyusutan aset") ;
      $vare[]    = array("id"=>'RO', "text"=>"(RO) Pencairan pinjaman") ;
      $vare[]    = array("id"=>'UP', "text"=>"(UP) Penggajian / payroll") ;
      $vare[]    = array("id"=>'TH', "text"=>"(TH) Akhir tahun / tutup buku") ;
      echo(json_encode($vare)) ; 
   }
   public function load_rekening(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%') and jenis='D'" ;
      $dbd  = $this->bdb->select("keuangan_rekening", "id, kode ,keterangan", $w, "", "", "kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['kode'] . " - "  . $dbr['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   public function load_bank(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $dbd  = $this->bdb->select("bank", "id, kode ,keterangan", $w, "", "", "kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   public function load_cabang(){
      $this->duser();
      $q    = $this->input->get('q') ;
      $vare = array() ;  

      if($q !== ''){
         $this->db->group_start();
             $this->db->or_like(array('keterangan'=>$this->bdb->escape_like_str($q),'kode'=>$this->bdb->escape_like_str($q)));
         $this->db->group_end();
      }

      if($this->aruser['level'] <> '0000'){
         if(isset($this->aruser['data_var']['cabang'])){
            $this->db->group_start();
            foreach($this->aruser['data_var']['cabang'] as $key => $val){
               $this->db->or_where('kode',$val);
            }
            $this->db->group_end();
         }
         
      }

      $db = $this->db->select("id, kode ,keterangan")->from("cabang")->get();
      foreach($db->result_array() as $r){
         $vare[]    = array("id"=>$r['kode'], "text"=>$r['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   public function load_satuan(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $dbd  = $this->bdb->select("satuan", "id, kode ,keterangan", $w, "", "", "kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   public function load_golstock(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $dbd  = $this->bdb->select("stock_group", "id, kode ,keterangan", $w, "", "", "kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   public function load_klmstock(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $dbd  = $this->bdb->select("stock_kelompok", "id, kode ,keterangan", $w, "", "", "kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   /* Aset */
   public function load_golaset(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $dbd  = $this->bdb->select("aset_golongan", "id, kode ,keterangan", $w, "", "", "kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   public function load_kelaset(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $dbd  = $this->bdb->select("aset_kelompok", "id, kode ,keterangan", $w, "", "", "kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   public function load_aset($cabang){
      $q    = $this->input->get('q') ;
      $q = $this->db->escape_like_str($q);
      $vare = array() ;
      if($cabang <> "")$this->db->where('cabang',$cabang);

      $this->db->group_start();
      $this->db->or_like(array('keterangan'=>$q, 'kode'=>$q));
      $this->db->group_end();
      $this->db->where('fakturpj','');
      $f = "id, kode ,keterangan";
      $dbd = $this->db->select($f)->from("aset")
                ->limit(50)
                ->get();        
      foreach($dbd->result_array() as $dbr){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   /* dati */
   public function load_dati_1(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $dbd  = $this->bdb->select("dati_1", "id, kode ,keterangan", $w, "", "", "kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   public function load_dati_2(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(p.keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' or k.keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' OR k.kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $j = "left join dati_1 p on p.kode = k.dati_1";
      $dbd  = $this->bdb->select("dati_2 k", "k.id, k.kode ,k.keterangan, p.keterangan as dati1", $w, $j, "", "k.kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['keterangan'] ." " .$dbr['dati1']) ;
      }
      echo(json_encode($vare)) ; 
   }

   /* armada */
   public function load_armada(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $dbd  = $this->bdb->select("armada", "id, kode ,keterangan", $w, "", "", "kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['kode'] . " - "  . $dbr['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   /* gudang */
   public function load_gudang($cabang = ''){
      $this->duser();
      $q    = $this->input->get('q') ;
      $vare = array() ;  

      if($q !== ''){
         $this->db->group_start();
             $this->db->or_like(array('keterangan'=>$this->bdb->escape_like_str($q),'kode'=>$this->bdb->escape_like_str($q)));
         $this->db->group_end();
      }

      if($this->aruser['level'] <> '0000'){
         if(isset($this->aruser['data_var']['gudang'])){
            $this->db->group_start();
            foreach($this->aruser['data_var']['gudang'] as $key => $val){
               $this->db->or_where('kode',$val);
            }
            $this->db->group_end();
         }
         
      }

      if($cabang <> '')$this->db->where('cabang',$cabang);

      $db = $this->db->select("id, kode ,keterangan")->from("gudang")->get();
      foreach($db->result_array() as $r){
         $vare[]    = array("id"=>$r['kode'], "text"=>$r['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   /* produksi */
   public function load_produksiregu($cabang=''){
      $this->duser();
      $q    = $this->input->get('q') ;
      $vare = array() ;  

      if($q !== ''){
         $this->db->group_start();
             $this->db->or_like(array('r.keterangan'=>$this->bdb->escape_like_str($q),'k.nama'=>$this->bdb->escape_like_str($q)));
         $this->db->group_end();
      }

      if($cabang == ""){
         if($this->aruser['level'] <> '0000'){
            if(isset($this->aruser['data_var']['cabang'])){
               $this->db->group_start();
               foreach($this->aruser['data_var']['cabang'] as $key => $val){
                  $this->db->or_where('r.cabang',$val);
               }
               $this->db->group_end();
            }         
         }
      }else{
         $this->db->where('r.cabang',$cabang);
      }
      $this->db->where('r.status',"1");

      

      $db = $this->db->select("r.kode,r.keterangan,k.nama as namakaru")
         ->from("produksi_regu r")
         ->join("karyawan k","k.kode = r.karu")
         ->limit(50)
         ->get();
      foreach($db->result_array() as $r){
         $vare[]    = array("id"=>$r['kode'], "text"=>$r['keterangan']." | Karu ".$r['namakaru']) ;
      }
      echo(json_encode($vare)) ; 
   }

   /* supplier */
   public function load_supplier(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(nama LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $dbd  = $this->bdb->select("supplier", "id, kode ,nama", $w, "", "", "kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['nama']) ;
      }
      echo(json_encode($vare)) ; 
   }

   /* customer */
   public function load_customer(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(nama LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $dbd  = $this->bdb->select("customer", "id, kode ,nama", $w, "", "", "kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['nama']) ;
      }
      echo(json_encode($vare)) ; 
   }

   public function load_customer_gol(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $dbd  = $this->bdb->select("customer_golongan", "id, kode ,keterangan", $w, "", "", "kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   /* cabang */
   public function mdl_gr_cabang_where($bs, $s){
      $this->duser();
      if($s !== ''){
          $this->db->group_start();
              $this->db->or_like(array('keterangan'=>$s));
          $this->db->group_end();
      }

      if($this->aruser['level'] <> '0000'){
         // print_r($this->aruser['data_var']);
         if(isset($this->aruser['data_var']['cabang'])){
            $this->db->group_start();
            foreach($this->aruser['data_var']['cabang'] as $key => $val){
               $this->db->or_where('kode',$val);
            }
            $this->db->group_end();
         }
         
      }
   } 

   public function mdl_gr_cabang(){
      // $this->auth();
      $va = json_decode($this->input->post('request'), true);
      $re = array('total'=>0, 'records'=>array());
      $vsat = array(); 

      $bs = isset($va['bsearch']) ? $va['bsearch'] : array();
      $s = isset($va['search']) ? $va['search'][0]['value'] : '';

      $this->mdl_gr_cabang_where($bs, $s);
      $db = $this->db->select("count(id) jml")->from("cabang")->get(); //print_r($this->db->last_query());
      $r  = $db->row_array();
      if(isset($r)){
          $r['jml']   = intval($r['jml']);
          if($r['jml'] > 0){
              $re['total'] = $r['jml'];

              $this->mdl_gr_cabang_where($bs, $s);
              $db = $this->db->select("*")
                                  ->from("cabang")
                                  ->limit($va['limit'], $va['offset'])
                                  ->order_by('kode ASC')
                                  ->get(); 
              foreach($db->result_array() as $r){
                  // $r['pilih'] = '<button type="button" onClick="mdl_cabang.pilih('.$r['kode'].')"
                                 //  class="btn btn-success btn-w2gr w-100">Pilih</button>';
      
                  //append
                  $r['recid'] = $r['kode'];
                  $re['records'][] = $r ;  
              }
          }
      }

      echo(json_encode($re)) ; 
   }

   public function mdl_cabang_seek(){
      $va = $this->input->post() ;
      // $re = array('total'=>0, 'records'=>array());
      $vrtn = array(); 

      if(isset($va['cabang'])){
         $va['cabang'] = json_decode($va['cabang'],true);
         $wgd = array();
         $this->db->group_start();

         foreach($va['cabang'] as $key){
            $vrtn[$key] = "cabang";
            $this->db->or_where('kode',$key);
         }
         $this->db->group_end();

      }
      
      $db = $this->db->select("*")->from("cabang")->get(); //print_r($this->db->last_query());
      $r  = $db->row_array();
      foreach($db->result_array() as $r){
         $vrtn[$r['kode']] = $r;
      }     

      echo(json_encode($vrtn)) ; 
   }

   /* gudang */
   public function mdl_gr_gudang_where($bs, $s){
      $this->duser();
      if($s !== ''){
          $this->db->group_start();
              $this->db->or_like(array('keterangan'=>$s));
          $this->db->group_end();
      }

      if($this->aruser['level'] <> '0000'){
         // print_r($this->aruser['data_var']);
         if(isset($this->aruser['data_var']['gudang'])){
            $this->db->group_start();
            foreach($this->aruser['data_var']['gudang'] as $key => $val){
               $this->db->or_where('kode',$val);
            }
            $this->db->group_end();
         }
         
      }
   } 

   public function mdl_gr_gudang(){
      // $this->auth();
      

      $va = json_decode($this->input->post('request'), true);
      $re = array('total'=>0, 'records'=>array());
      $vsat = array(); 

      $bs = isset($va['bsearch']) ? $va['bsearch'] : array();
      $s = isset($va['search']) ? $va['search'][0]['value'] : '';

      $this->mdl_gr_gudang_where($bs, $s);
      $db = $this->db->select("count(id) jml")->from("gudang")->get(); //print_r($this->db->last_query());
      $r  = $db->row_array();
      if(isset($r)){
          $r['jml']   = intval($r['jml']);
          if($r['jml'] > 0){
              $re['total'] = $r['jml'];

              $this->mdl_gr_gudang_where($bs, $s);
              $db = $this->db->select("*")
                                  ->from("gudang")
                                  ->limit($va['limit'], $va['offset'])
                                  ->order_by('cabang ASC')
                                  ->get(); 
              foreach($db->result_array() as $r){
                  // $r['pilih'] = '<button type="button" onClick="mdl_gudang.pilih('.$r['kode'].')"
                                 //  class="btn btn-success btn-w2gr w-100">Pilih</button>';
      
                  //append
                  $r['recid'] = $r['kode'];
                  $re['records'][] = $r ;  
              }
          }
      }

      echo(json_encode($re)) ; 
   }

   public function mdl_gudang_seek(){
      $va = $this->input->post() ;
      // $re = array('total'=>0, 'records'=>array());
      $vrtn = array(); 

      if(isset($va['gudang'])){
         $va['gudang'] = json_decode($va['gudang'],true);
         $wgd = array();
         $this->db->group_start();

         foreach($va['gudang'] as $key){
            $vrtn[$key] = "gudang";
            $this->db->or_where('kode',$key);
         }
         $this->db->group_end();

      }
      
      $db = $this->db->select("*")->from("gudang")->get(); //print_r($this->db->last_query());
      $r  = $db->row_array();
      foreach($db->result_array() as $r){
         $vrtn[$r['kode']] = $r;
      }     

      echo(json_encode($vrtn)) ; 
   }

   /* barang / stok */
   public function mdl_gr_stock_where($bs, $s){
      if($s !== ''){
         $this->db->group_start();
            $this->db->or_like(array('kode'=>$s,'keterangan'=>$s));
         $this->db->group_end();
      }

      if(!empty($bs)){
         if(isset($bs['tampil'])){

            $this->db->group_start();
               foreach($bs['tampil']as $key => $val){
                  $this->db->or_where('tampil',$val);
               }
            $this->db->group_end();
         }
      }
   } 

   public function mdl_gr_stock(){
      // $this->auth();
      $va = json_decode($this->input->post('request'), true);
      $re = array('total'=>0, 'records'=>array());
      $vsat = array(); 

      

      // echo $orderby;
      $bs = isset($va['bsearch']) ? $va['bsearch'] : array();
      $s = isset($va['search']) ? $va['search'][0]['value'] : '';

      if(isset($bs['tgl']))$bs['tgl'] = date_2s($bs['tgl']);
      $orderby = "keterangan asc";
      if(isset($bs['orderby'])){
         if(!empty($bs['orderby'])) $orderby = $bs['orderby'];
      }
      // print_r($bs);
      $this->mdl_gr_stock_where($bs, $s);
      $db = $this->db->select("count(id) jml")->from("stock")->get(); //print_r($this->db->last_query());
      $r  = $db->row_array();
      if(isset($r)){
         $r['jml']   = intval($r['jml']);
         if($r['jml'] > 0){
            $re['total'] = $r['jml'];
            
            $this->mdl_gr_stock_where($bs, $s);
            $db = $this->db->select("*")
                                 ->from("stock")
                                 ->limit($va['limit'], $va['offset'])
                                 ->order_by($orderby)
                                 ->get(); 
            foreach($db->result_array() as $r){
                  if(isset($bs['customer'])){
                     $r['hj'] = $this->perhitungan_m->gethjstockcustomer($r['kode'], $bs['customer']);
                  }
                  if(isset($bs['gudang']) && isset($bs['tgl'])){
                     $r['saldo'] = 0 ;
                     foreach($bs['gudang'] as $key => $val){
                        $r['saldo'] += $this->perhitungan_m->getsaldoakhirstock($r['kode'],$bs['tgl'],$val); ;
                     }
                  }

                  $r['cmdpilih'] = '<button type="button" onClick="mdl_barang.pilih('.$r['kode'].')"
                                  class="btn btn-success btn-grid">Pilih</button>';
      
                  //append
                  $r['recid'] = $r['kode'];

                  $re['records'][] = $r ;  
            }
         }
      }

      echo(json_encode($re)) ; 
   }

   public function mdl_stock_seek(){
      $va = $this->input->post() ;

      // print_r($va);
      // $re = array('total'=>0, 'records'=>array());
      $vrtn = array(); 
      if(isset($va['tgl']))$va['tgl'] = date_2s($va['tgl']);
      if(isset($va['gudang'])){
         if(!empty($va['gudang']))$va['gudang'] = json_decode($va['gudang']);
      }

      // print_r($va);
      $this->db->where('kode',$va['stock']);
      if(isset($va['tampil'])){
         $va['tampil'] = json_decode($va['tampil']);
          $this->db->group_start();
             foreach($va['tampil']as $key => $val){
                $this->db->or_where('tampil',$val);
             }
          $this->db->group_end();
      }
      
      
      $db = $this->db->select("*")->from("stock")->get(); //print_r($this->db->last_query());
      $r  = $db->row_array();
      foreach($db->result_array() as $r){
         // $r['hj'] = 888;
         if(isset($va['customer'])){
            $r['hj'] = $this->perhitungan_m->gethjstockcustomer($r['kode'], $va['customer']);
         }

         if(isset($va['gudang']) && isset($va['tgl'])){
            if(!empty($va['gudang'])){
               $r['saldo'] = 0 ;
               foreach($va['gudang'] as $key => $val){
                  $r['saldo'] += $this->perhitungan_m->getsaldoakhirstock($r['kode'],$va['tgl'],$val); ;
               }
            }
            
         }

         $vrtn = $r;
      }     

      echo(json_encode($vrtn)) ; 
   }

   /* customer */
   public function mdl_gr_customer_where($bs, $s){
      if($s !== ''){
         $this->db->group_start();
            $this->db->or_like(array('kode'=>$s,'nama'=>$s));
         $this->db->group_end();
      }

      // if(!empty($bs)){
      //    if(isset($bs['tampil'])){

      //       $this->db->group_start();
      //          foreach($bs['tampil']as $key => $val){
      //             $this->db->or_where('tampil',$val);
      //          }
      //       $this->db->group_end();
      //    }
      // }
   } 

   public function mdl_gr_customer(){
      // $this->auth();
      $va = json_decode($this->input->post('request'), true);
      $re = array('total'=>0, 'records'=>array());
      $vsat = array(); 

      

      // echo $orderby;
      $bs = isset($va['bsearch']) ? $va['bsearch'] : array();
      $s = isset($va['search']) ? $va['search'][0]['value'] : '';
      $orderby = "nama asc";
      if(isset($bs['orderby'])){
         if(!empty($bs['orderby'])) $orderby = $bs['orderby'];
      }
      // print_r($bs);
      $this->mdl_gr_customer_where($bs, $s);
      $db = $this->db->select("count(id) jml")->from("customer")->get(); //print_r($this->db->last_query());
      $r  = $db->row_array();
      if(isset($r)){
         $r['jml']   = intval($r['jml']);
         if($r['jml'] > 0){
            $re['total'] = $r['jml'];
            
            $this->mdl_gr_customer_where($bs, $s);
            $db = $this->db->select("*")
                                 ->from("customer")
                                 ->limit($va['limit'], $va['offset'])
                                 ->order_by($orderby)
                                 ->get(); 
            foreach($db->result_array() as $r){
                 

                  $r['cmdpilih'] = '<button type="button" onClick="mdl_customer.pilih('.$r['kode'].')"
                                  class="btn btn-success btn-grid">Pilih</button>';
      
                  //append
                  $r['recid'] = $r['kode'];

                  $re['records'][] = $r ;  
            }
         }
      }

      echo(json_encode($re)) ; 
   }

   public function mdl_customer_seek(){
      $va = $this->input->post() ;

      // print_r($va);
      // $re = array('total'=>0, 'records'=>array());
      $vrtn = array(); 

      // print_r($va);
      $this->db->where('kode',$va['kode']);      
      
      $db = $this->db->select("*")->from("customer")->get(); //print_r($this->db->last_query());
      $r  = $db->row_array();
      foreach($db->result_array() as $r){
         $vrtn = $r;
      }     

      echo(json_encode($vrtn)) ; 
   }

   /* supplier */
   public function mdl_gr_supplier_where($bs, $s){
      if($s !== ''){
         $this->db->group_start();
            $this->db->or_like(array('kode'=>$s,'nama'=>$s));
         $this->db->group_end();
      }

      // if(!empty($bs)){
      //    if(isset($bs['tampil'])){

      //       $this->db->group_start();
      //          foreach($bs['tampil']as $key => $val){
      //             $this->db->or_where('tampil',$val);
      //          }
      //       $this->db->group_end();
      //    }
      // }
   } 

   public function mdl_gr_supplier(){
      // $this->auth();
      $va = json_decode($this->input->post('request'), true);
      $re = array('total'=>0, 'records'=>array());
      $vsat = array(); 

      

      // echo $orderby;
      $bs = isset($va['bsearch']) ? $va['bsearch'] : array();
      $s = isset($va['search']) ? $va['search'][0]['value'] : '';
      $orderby = "nama asc";
      if(isset($bs['orderby'])){
         if(!empty($bs['orderby'])) $orderby = $bs['orderby'];
      }
      // print_r($bs);
      $this->mdl_gr_supplier_where($bs, $s);
      $db = $this->db->select("count(id) jml")->from("supplier")->get(); //print_r($this->db->last_query());
      $r  = $db->row_array();
      if(isset($r)){
         $r['jml']   = intval($r['jml']);
         if($r['jml'] > 0){
            $re['total'] = $r['jml'];
            
            $this->mdl_gr_supplier_where($bs, $s);
            $db = $this->db->select("*")
                                 ->from("supplier")
                                 ->limit($va['limit'], $va['offset'])
                                 ->order_by($orderby)
                                 ->get(); 
            foreach($db->result_array() as $r){
                 

                  $r['cmdpilih'] = '<button type="button" onClick="mdl_supplier.pilih('.$r['kode'].')"
                                  class="btn btn-success btn-grid">Pilih</button>';
      
                  //append
                  $r['recid'] = $r['kode'];

                  $re['records'][] = $r ;  
            }
         }
      }

      echo(json_encode($re)) ; 
   }

   public function mdl_supplier_seek(){
      $va = $this->input->post() ;

      // print_r($va);
      // $re = array('total'=>0, 'records'=>array());
      $vrtn = array(); 

      // print_r($va);
      $this->db->where('kode',$va['kode']);      
      
      $db = $this->db->select("*")->from("supplier")->get(); //print_r($this->db->last_query());
      $r  = $db->row_array();
      foreach($db->result_array() as $r){
         $vrtn = $r;
      }     

      echo(json_encode($vrtn)) ; 
   }

   /*other*/


    
   public function load_export(){
      $vare = array() ;  
      $vare[]    = array("id"=>'0', "text"=>"PDF") ;
      $vare[]    = array("id"=>'1', "text"=>"CSV") ;
      $vare[]    = array("id"=>'2', "text"=>"XLSX") ;
      echo(json_encode($vare)) ; 
   }

   public function load_periode(){
      $vare = array() ;  
      $vare[]    = array("id"=>'J', "text"=>"Jam") ;
      $vare[]    = array("id"=>'H', "text"=>"Hari") ;
      $vare[]    = array("id"=>'B', "text"=>"Bulan") ;
      $vare[]    = array("id"=>'3', "text"=>"Triwulan") ;
      $vare[]    = array("id"=>'S', "text"=>"Semester") ;
      $vare[]    = array("id"=>'T', "text"=>"Tahun") ;
      echo(json_encode($vare)) ; 
   }

   public function load_bulan(){
      $vare = array() ;  
      $vare[]    = array("id"=>'1', "text"=>"Januari") ;
      $vare[]    = array("id"=>'2', "text"=>"Februari") ;
      $vare[]    = array("id"=>'3', "text"=>"Maret") ;
      $vare[]    = array("id"=>'4', "text"=>"April") ;
      $vare[]    = array("id"=>'5', "text"=>"Mei") ;
      $vare[]    = array("id"=>'6', "text"=>"Juni") ;
      $vare[]    = array("id"=>'7', "text"=>"Juli") ;
      $vare[]    = array("id"=>'8', "text"=>"Agustus") ;
      $vare[]    = array("id"=>'9', "text"=>"Sepetember") ;
      $vare[]    = array("id"=>'10', "text"=>"Oktober") ;
      $vare[]    = array("id"=>'11', "text"=>"November") ;
      $vare[]    = array("id"=>'12', "text"=>"Desember") ;
      
      echo(json_encode($vare)) ; 
   }

   public function load_tahun(){
      $vare = array() ;  
      $thnskrg = date("Y");
      $thnawal = 2018;
      $thnakhir = $thnskrg + 5;
      $vare = array() ;
      for($thn = $thnakhir;$thn >= $thnawal;$thn--){
         if($thn == $thnskrg){
            $vare[]    = array("id"=>$thn, "text"=>$thn,"selected"=>true) ;
         }else{
            $vare[]    = array("id"=>$thn, "text"=>$thn) ;
         }
         
      }
      
      echo(json_encode($vare)) ; 
   }

   public function load_tahap(){
      $vare = array() ;  
      $vare[]    = array("id"=>'0', "text"=>"None") ;
      $vare[]    = array("id"=>'1', "text"=>"Tahap 1") ;
      $vare[]    = array("id"=>'2', "text"=>"Tahap 2") ;
      $vare[]    = array("id"=>'3', "text"=>"Tahap 3") ;
      $vare[]    = array("id"=>'4', "text"=>"Tahap 4") ;
      $vare[]    = array("id"=>'5', "text"=>"Tahap 5") ;
      
      echo(json_encode($vare)) ; 
   }

   /***************************************************************************************************** HRD *****************************************************************************************************/
   /* tingkat pendidikan */
   public function load_pendidikan_tingkat(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $dbd  = $this->bdb->select("pendidikan_tingkat", "id, kode ,keterangan", $w, "", "", "kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   /* jabatan */
   public function load_jabatan(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $dbd  = $this->bdb->select("jabatan", "id, kode ,keterangan", $w, "", "", "kode ASC") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }

   /* Periode gaji */
   public function load_periodepayroll(){
      $q    = $this->input->get('q') ;
      $vare = array() ;  
      $w    = "(keterangan LIKE '%". $this->bdb->escape_like_str($q) ."%' OR kode LIKE '%". $this->bdb->escape_like_str($q) ."%')" ;
      $dbd  = $this->bdb->select("sys_periode_payroll", "id, kode ,keterangan", $w, "", "", "kode desc") ;
      while($dbr    = $this->bdb->getrow($dbd)){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      echo(json_encode($vare)) ; 
   }


   /* Karyawan */
   public function load_karyawan($cabang){

      $q    = $this->input->get('q') ;
      $q = $this->db->escape_like_str($q);
      $vare = array() ;
      // if($cabang <> "")$this->db->where('cabang',$cabang);
      $this->db->where('tglkeluar','0000-00-00');
      $this->db->group_start();
      $this->db->or_like(array('kode'=>$q, 'nama'=>$q));
      $this->db->group_end();
      
      $f = "kode,nama";
      $dbd = $this->db->select($f)->from("karyawan")
                ->limit(50)
                ->get();        
      foreach($dbd->result_array() as $dbr){
         $vare[]    = array("id"=>$dbr['kode'], "text"=>$dbr['kode']." - ".$dbr['nama']) ;
      }
      echo(json_encode($vare)) ; 
   }
}
?>
