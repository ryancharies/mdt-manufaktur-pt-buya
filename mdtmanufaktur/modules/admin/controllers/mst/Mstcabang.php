<?php
class Mstcabang extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->model("mst/mstcabang_m") ;
        $this->bdb 	= $this->mstcabang_m ;
    }

    public function index(){
        $this->load->view("mst/mstcabang") ;

    } 

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->bdb->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vs = $dbr;   
            $vs['cmdedit']    = '<button type="button" onClick="bos.mstcabang.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Koreksi</button>' ;
            $vs['cmdedit']	   = html_entity_decode($vs['cmdedit']) ;

            $vs['cmddelete']  = '<button type="button" onClick="bos.mstcabang.cmddelete(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Hapus</button>' ;
            $vs['cmddelete']  = html_entity_decode($vs['cmddelete']) ;

            $vare[]		= $vs ; 
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "ssmstcabang_id", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $id 	= getsession($this, "ssmstcabang_id") ; 

        $this->bdb->saving($va, $id) ;
        echo(' bos.mstcabang.settab(0) ;  ') ;
    }

    public function deleting(){
        $va 	= $this->input->post() ; 
        $kode 	= $va['kode'] ;
        $this->bdb->delete("cabang", "kode = " . $this->bdb->escape($kode)) ;
        echo(' bos.mstcabang.grid1_reload() ; ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $kode 	= $va['kode'] ;
        $data = $this->mstcabang_m->getdata($kode) ;
        if( $dbr = $this->mstcabang_m->getrow($data) ){
            savesession($this, "ssmstcabang_id", $dbr['kode']) ;

            echo('
            with(bos.mstcabang.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$dbr['kode'].'") ;
               find("#keterangan").val("'.$dbr['keterangan'].'").focus() ;
               find("#kota").val("'.$dbr['kota'].'") ;
               find("#alamat").val("'.$dbr['alamat'].'") ;
               find("#telp").val("'.$dbr['telp'].'") ;

            }
            bos.mstcabang.settab(1) ;
         ') ;
        }
    }

}
?>
