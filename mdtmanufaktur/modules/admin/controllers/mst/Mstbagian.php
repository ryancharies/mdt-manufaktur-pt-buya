<?php
class Mstbagian extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->model("mst/mstbagian_m") ;
        $this->bdb 	= $this->mstbagian_m ;
    }

    public function index(){
        $this->load->view("mst/mstbagian") ;

    } 

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->bdb->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vs = $dbr;   
            $vs['cmdedit']    = '<button type="button" onClick="bos.mstbagian.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Koreksi</button>' ;
            $vs['cmdedit']	   = html_entity_decode($vs['cmdedit']) ;

            $vs['cmddelete']  = '<button type="button" onClick="bos.mstbagian.cmddelete(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Hapus</button>' ;
            $vs['cmddelete']  = html_entity_decode($vs['cmddelete']) ;

            $vare[]		= $vs ; 
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "ssmstbagian_id", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $id 	= getsession($this, "ssmstbagian_id") ; 

        $this->bdb->saving($va, $id) ;
        echo(' bos.mstbagian.settab(0) ;  ') ;
    }

    public function deleting(){
        $va 	= $this->input->post() ; 
        $kode 	= $va['kode'] ;
        $this->bdb->delete("bagian", "kode = " . $this->bdb->escape($kode)) ;
        echo(' bos.mstbagian.grid1_reload() ; ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $kode 	= $va['kode'] ;
        $data = $this->mstbagian_m->getdata($kode) ;
        if( $dbr = $this->mstbagian_m->getrow($data) ){
            savesession($this, "ssmstbagian_id", $dbr['kode']) ;
            $ketrekbyasuransi = $this->mstbagian_m->getval("keterangan", "kode = '{$dbr['rekbyasuransi']}'", "keuangan_rekening");

            $rekbyasuransi[] = array("id"=>$dbr['rekbyasuransi'],"text"=>$dbr['rekbyasuransi'] ." - ". $ketrekbyasuransi);

            echo('
                with(bos.mstbagian.obj){
                find(".nav-tabs li:eq(1) a").tab("show") ;
                find("#kode").val("'.$dbr['kode'].'") ;
                find("#keterangan").val("'.$dbr['keterangan'].'").focus() ;
                find("#rekbyasuransi").sval('.json_encode($rekbyasuransi).');

                }
                bos.mstbagian.settab(1) ;
            ') ;
        }
    }

    public function seekrekening(){
        $search     = $this->input->get('q');
        $vdb    = $this->mstbagian_m->seekrekening($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->mstbagian_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ".$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

}
?>
