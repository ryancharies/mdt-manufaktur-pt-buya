<?php
class Mstjwabsensi extends Bismillah_Controller
{
    private $bdb;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('mst/mstjwabsensi_m');
        $this->load->model('func/updtransaksi_m');
        $this->load->helper('bdate');
        $this->bdb = $this->mstjwabsensi_m;
    }

    public function index()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        
        $arrsttd = array();
        $arrsttd[] = array("id"=>"0","text"=>"Masuk");
        $arrsttd[] = array("id"=>"1","text"=>"Libur");

        $d = array("dnow" => date("d-m-Y"), "setdate" => date_set(),"mintgl"=>$tglmin,"statusdetail"=>json_encode($arrsttd));


        $this->load->view('mst/mstjwabsensi', $d);
    }

    public function loadgrid()
    {
        $va = json_decode($this->input->post('request'), true);
        $vare = array();
        $vdb = $this->mstjwabsensi_m->loadgrid($va);
        $dbd = $vdb['db'];
        while ($dbr = $this->mstjwabsensi_m->getrow($dbd)) {
            $vaset = $dbr;
            $vaset['cmdedit'] = '<button type="button" onClick="bos.mstjwabsensi.cmdedit(\'' . $dbr['kode'] . '\')"
                                        class="btn btn-default btn-grid">Edit</button>';
            $vaset['cmddelete'] = '<button type="button" onClick="bos.mstjwabsensi.cmddelete(\'' . $dbr['kode'] . '\')"
                                        class="btn btn-danger btn-grid">Non Aktifkan</button>';

            // $validdel = $this->mstjwabsensi_m->cekvalidasidel($dbr['faktur']);
            // if ($validdel != "") {
            //     $vaset['cmddelete'] = '<button type="button" onClick="alert(\'' . $validdel . '\')"
            //                              class="btn btn-info btn-grid">Info</button>';
            // }

            $vaset['cmdedit'] = html_entity_decode($vaset['cmdedit']);
            $vaset['cmddelete'] = html_entity_decode($vaset['cmddelete']);

            $vare[] = $vaset;
        }
        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function init()
    {
        savesession($this, "ssjwabsensi_kode", "");
    }

    public function saving()
    {
        $va = $this->input->post();
        $kode = getsession($this, "ssjwabsensi_kode");
        //cek valid grid 3
        $error = $this->mstjwabsensi_m->saving($kode, $va);
        

        if ($error == "") {
            
            echo (' bos.mstjwabsensi.settab(0) ;
                     alert("Transaksi berhasil disimpan..");');
        } else {
            echo (' alert("' . $error . '");');
        }
    }

    public function editing()
    {
        $va = $this->input->post();
        $kode = $va['kode'];
        $data = $this->mstjwabsensi_m->getdatatotal($kode);
        if (!empty($data)) {
            savesession($this, "ssjwabsensi_kode", $kode);
            echo ('
            w2ui["bos-form-mstjwabsensi_grid2"].clear();
            with(bos.mstjwabsensi.obj){
                find(".nav-tabs li:eq(1) a").tab("show") ;
                find("#kode").val("' . $data['kode'] . '") ;
                find("#keterangan").val("' . $data['keterangan'] . '") ;
                find("#siklus").val("' . $data['siklus'] . '") ;
                find("#mulailembur").val("' . $data['mulailembur'] . '") ;
                find("#maxlembur").val("' . $data['maxlembur'] . '") ;
                find("#toleransi").val("' . $data['toleransi'] . '") ;
                find("#bukaabsenmasuk").val("' . $data['bukaabsenmasuk'] . '") ;
                find("#tutupabsenmasuk").val("' . $data['tutupabsenmasuk'] . '") ;
                find("#bukaabsenpulang").val("' . $data['bukaabsenpulang'] . '") ;
                find("#tutupabsenpulang").val("' . $data['tutupabsenpulang'] . '") ;
            }
            bos.mstjwabsensi.setopt("sistem","'.$data['sistem'].'");

         ');

            //loadgrid detail
            $vare = array();
            $dbd = $this->mstjwabsensi_m->getdatadetail($kode);
            $n = 0;
            while ($dbr = $this->mstjwabsensi_m->getrow($dbd)) {
                $n++;
                $vaset = $dbr;
                $vaset['recid'] = $n;
                $vaset['keterangan'] = "Hari ke ".$n;
                $vare[] = $vaset;
            }
            $vare = json_encode($vare);
            echo ('
                w2ui["bos-form-mstjwabsensi_grid2"].add(' . $vare . ');
                bos.mstjwabsensi.settab(1) ;
            ');
        }
    }
    public function deleting()
    {

        $va = $this->input->post();
        $error = $this->mstjwabsensi_m->deleting($va['kode']);
        
        if($error == ""){
            echo ('bos.mstjwabsensi.grid1_reloaddata() ; ');
        }else{
            echo('
                alert(" Data tidak bisa dinonaktifkan \\n '.$error.'");
            ');
        }

    }

    public function seekbank()
    {
        $search = $this->input->get('q');
        $vdb = $this->mstjwabsensi_m->seekbank($search);
        $dbd = $vdb['db'];
        $vare = array();
        while ($dbr = $this->mstjwabsensi_m->getrow($dbd)) {
            $vare[] = array("id" => $dbr['kode'], "text" => $dbr['kode'] . " - " . $dbr['keterangan']);
        }
        $Result = json_encode($vare);
        echo ($Result);
    }

    public function seekrekening()
    {
        $search = $this->input->get('q');
        $vdb = $this->mstjwabsensi_m->seekrekening($search);
        $dbd = $vdb['db'];
        $vare = array();
        while ($dbr = $this->mstjwabsensi_m->getrow($dbd)) {
            $vare[] = array("id" => $dbr['kode'], "text" => $dbr['kode'] . " - " . $dbr['keterangan']);
        }
        $Result = json_encode($vare);
        echo ($Result);
    }

    public function seekdiberiterima()
    {
        $search = $this->input->get('q');
        $vdb = $this->mstjwabsensi_m->seekdiberiterima($search);
        $dbd = $vdb['db'];
        $vare = array();
        while ($dbr = $this->mstjwabsensi_m->getrow($dbd)) {
            $vare[] = array("id" => $dbr['kode'], "text" => $dbr['nama']);
        }
        $Result = json_encode($vare);
        echo ($Result);
    }

    public function getkode()
    {
        $va = $this->input->post();
        $kode = $this->mstjwabsensi_m->getkode(false);

        echo ('
        bos.mstjwabsensi.obj.find("#kode").val("' . $kode . '") ;
        ');
    }

    public function printbuktigm()
    {
        //gambar yg diambil di display none dulu di frame, kayaknya perlu untuk pancingan aja

        $va = $this->input->post();
        $dbr = $this->bdb->getdatatotal($va['faktur']);
        if (!empty($dbr)) {

            $html = "";
            $html .= "<table width = 100%>";
            $html .= " <tr><td width = 100%>";
            $html .= "     <table width = 100%>";
            $html .= "         <tr>";
            $html .= "             <td width=65%><img src =\ ./uploads/header.jpg \ width=100%></td>";
            $html .= "             <td >&nbsp;</td>";
            $html .= "             <td width=30%>";
            $html .= "                 <table width=100%>";
            $html .= "                     <tr><td>Kudus " . date_2d($dbr['tgl']) . "</td></tr>";
            $html .= "                     <tr><td>No GM " . $va['faktur'] . "</td></tr>";
            $html .= "                 </table>";
            $html .= "             </td>";
            $html .= "         </tr>";
            $html .= "     </table>";
            $html .= " </td></tr>";
            $html .= " <tr><td width = 100% align=center><h2>BUKTI BG/CEK MASUK</h2></td></tr>";
            $html .= " <tr><td width = 100% rules=all border=1>";
            $html .= "     <table width = 100% align=center>";
            $html .= "         <thead>";
            $html .= "         <tr><th>No</th><th>BG/Cek</th><th>No Rekening</th><th>No BG/Cek</th><th>Jatuh Tempo</th><th>Nominal</th></tr>";
            $html .= "         </thead>";

            //load detail
            $html .= "         <tbody>";
            $dbd = $this->mstjwabsensi_m->getdatadetail($va['faktur']);
            $n = 0;
            while ($dbr2 = $this->mstjwabsensi_m->getrow($dbd)) {
                $n++;
                $html .= "         <tr><td align=center>" . $n . "</td><td>" . $dbr2['bgcek'] . "</td><td>" . $dbr2['norekening'] . "</td><td>" . $dbr2['nobgcek'] . "</td><td align=center>" . date_2d($dbr2['jthtmp']) . "</td><td align=center>" . string_2s($dbr2['nominal']) . "</td></tr>";
            }
            $html .= "         <tr><td colspan=5 align=center>Jumlah</td><td align=center>" . string_2s($dbr['kredit']) . "</td></tr>";
            $html .= "         </tbody>";

            $html .= "     </table>";
            $html .= " </td></tr>";
            $html .= " <tr><td width = 100%>";
            $html .= "     <table width = 100%>";
            $html .= "         <tr><td align = center>Yang Menerima</td><td></td><td align = center>Bagian Keuangan</td></tr>";
            $html .= "         <tr><td height=60px></td><td></td><td></td></tr>";
            $html .= "         <tr><td align = center>...............</td><td></td><td align = center>...............</td></tr>";
            $html .= "     </table>";
            $html .= " </td></tr>";
            $html .= "</table>";
            echo ('

            bos.mstjwabsensi.printbukti("' . $html . '");
            ');
        } else {
            echo ('alert("Data tidak ada !!!");');
        }

    }

    public function printbuktigk()
    {
        //gambar yg diambil di display none dulu di frame, kayaknya perlu untuk pancingan aja

        $va = $this->input->post();
        $dbr = $this->bdb->getdatatotal($va['faktur']);
        if (!empty($dbr)) {

            $html = "";
            $html .= "<table width = 100%>";
            $html .= " <tr><td width = 100%>";
            $html .= "     <table width = 100%>";
            $html .= "         <tr>";
            $html .= "             <td width=65%><img src =\ ./uploads/header.jpg \ width=100%></td>";
            $html .= "             <td >&nbsp;</td>";
            $html .= "             <td width=30%>";
            $html .= "                 <table width=100%>";
            $html .= "                     <tr><td>Kudus " . date_2d($dbr['tgl']) . "</td></tr>";
            $html .= "                     <tr><td>No BK " . $va['faktur'] . "</td></tr>";
            $html .= "                 </table>";
            $html .= "             </td>";
            $html .= "         </tr>";
            $html .= "     </table>";
            $html .= " </td></tr>";
            $html .= " <tr><td width = 100% align=center><h2>BUKTI BG/CEK KELUAR</h2></td></tr>";
            $html .= " <tr><td width = 100% align=left>Dibayarkan kepada : " . $dbr['namasupplier'] . " </td></tr>";
            $html .= " <tr><td width = 100%>";
            $html .= "     <table width = 100% rules=rows border=1 align=center>";
            $html .= "         <thead>";
            $html .= "         <tr><th>No</th><th>Bank</th><th>No Rekening</th><th>No BG/Cek</th><th>Jatuh Tempo</th><th>Nominal</th></tr>";
            $html .= "         </thead>";

            //load detail
            $html .= "         <tbody>";
            $dbd = $this->mstjwabsensi_m->getdatadetail($va['faktur']);
            $n = 0;
            while ($dbr2 = $this->mstjwabsensi_m->getrow($dbd)) {
                $n++;
                $html .= "         <tr align=center><td align=center>" . $n . "</td><td>" . $dbr2['ketbank'] . "</td><td>" . $dbr2['norekening'] . "</td><td>" . $dbr2['nobgcek'] . "</td><td align=center>" . date_2d($dbr2['jthtmp']) . "</td><td align=center>" . string_2s($dbr2['nominal']) . "</td></tr>";
            }
            $html .= "         <tr><td colspan=5 align=right>Jumlah :</td><td align=center>Rp. " . string_2s($dbr['total']) . "</td></tr>";
            $html .= "         </tbody>";

            $html .= "     </table>";
            $html .= " </td></tr>";
            $html .= " <tr><td width = 100% align=left>Guna Pembayaran : " . $dbr['keterangan'] . " </td></tr>";
            $html .= " <tr><td width = 100%>";
            $html .= "     <table width = 100%>";
            $html .= "         <tr><td align = center>Yang Menerima</td><td></td><td align = center>Bagian Keuangan</td></tr>";
            $html .= "         <tr><td height=60px></td><td></td><td></td></tr>";
            $html .= "         <tr><td align = center>...............</td><td></td><td align = center>...............</td></tr>";
            $html .= "     </table>";
            $html .= " </td></tr>";
            $html .= "</table>";
            echo ('

            bos.mstjwabsensi.printbukti("' . $html . '");
            ');
        } else {
            echo ('alert("Data tidak ada !!!");');
        }

    }

}
