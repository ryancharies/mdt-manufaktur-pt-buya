<?php
class Mstpayrollskalaperiode extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("mst/mstpayrollskalaperiode_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->bdb 	= $this->mstpayrollskalaperiode_m ;
    }

    public function index(){
        $arrkolomjam = array();
        $arrkolomhari = array();
        $arrkolombulan = array();

        $arrkolomjam[] = array("field"=> "lama", "caption"=> "Lama (Jam)", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolomhari[] = array("field"=> "lama", "caption"=> "Lama (Hari)", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolombulan[] = array("field"=> "lama", "caption"=> "Lama (Bln)", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        
        $dbd = $this->bdb->select("payroll_komponen","*","status='1' and perhitungan ='P'");
        while( $dbr = $this->bdb->getrow($dbd) ){
            if($dbr['periode'] == "J")$arrkolomjam[] = array("field"=> $dbr['kode'], "caption"=> $dbr['keterangan']."/ ".getperiode($dbr['periode']),
                                    "size"=> "150px", "render"=>"float:2","sortable"=>false,"editable"=>array("type"=>"int"));
            if($dbr['periode'] == "H")$arrkolomhari[] = array("field"=> $dbr['kode'], "caption"=> $dbr['keterangan']."/ ".getperiode($dbr['periode']),
                                    "size"=> "150px", "render"=>"float:2","sortable"=>false,"editable"=>array("type"=>"int"));
            if($dbr['periode'] == "B")$arrkolombulan[] = array("field"=> $dbr['kode'], "caption"=> $dbr['keterangan']."/ ".getperiode($dbr['periode']),
                                    "size"=> "150px", "render"=>"float:2","sortable"=>false,"editable"=>array("type"=>"int"));
        }

        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $data['kolomgridjam'] = json_encode($arrkolomjam);
        $data['kolomgridhari'] = json_encode($arrkolomhari);
        $data['kolomgridbulan'] = json_encode($arrkolombulan);
        $this->load->view("mst/mstpayrollskalaperiode",$data) ;

    } 

   
    public function loadgrid1(){
        $va = $this->input->post();
        // print_r($va);
        $vare = array();
        $n = 0 ;
        $arrn = array();

        $periode = date("Ym",strtotime($va['tgl']));

        $periode2 = $this->perhitunganhrd_m->getperiode($periode);
        $tglawalperiode = date_2s($periode2['tglawal']);
        $tglakhir = date_2s($periode2['tglakhir']);

        if($va['periode'] <> "" ){
            $max = 40;
            if($va['periode'] == "J") $max = 24;
            if($va['periode'] == "H") $max = 31;
            if($va['periode'] == "B") $max = 12;
            for($i = 0 ;$i <= $max ; $i++){
                $n++;
                $data = array();
                $data['recid']  = $n;
                $data['lama']  = $i;

                $dbd2 = $this->bdb->select("payroll_komponen","*","status='1' and perhitungan ='P' and periode= '{$va['periode']}'");
                while( $dbr2 = $this->bdb->getrow($dbd2) ){
                    if($dbr2['resetperiode'] <> '1'){
                        $tglawal = "0000-00-00";
                    }else{
                        $tglawal = $tglawalperiode;
                    }
    
                    $nominal = $this->perhitunganhrd_m->getnilaigajiskalaperiode($dbr2['kode'],$i,$tglawal,$tglakhir);
                    $data[$dbr2['kode']]=$nominal;
                }
                
                $vare[] = $data;
            }
        }   
        $vare = json_encode($vare);

        echo ('
                w2ui["bos-form-mstpayrollskalaperiode_grid1"].add(' . $vare . ');
              ');

    }

    public function init(){
        savesession($this, "ssmstpayrollskalaperiode_kode", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $error = $this->bdb->saving($va) ;
        if($error == ""){
            echo(' bos.mstpayrollskalaperiode.grid1_loaddata() ;  ') ;
        }else{
            echo('
                alert("'.$error.'");
                bos.mstpayrollskalaperiode.grid1_loaddata() ;  
                ') ;
        }
        
    }

}
?>
