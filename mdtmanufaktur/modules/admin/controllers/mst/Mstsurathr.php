<?php

class Mstsurathr extends Bismillah_Controller
{
    public function __construct(){
        parent::__construct() ;
        $this->load->model("mst/mstsurathr_m") ;
        $this->bdb 	= $this->mstsurathr_m ;
    }

    public function index(){
        $this->load->view("mst/mstsurathr") ;
    } 

    public function editing()
    {
        $cKode = $this->input->post('cKode') ;
        $vaData = $this->bdb->GetDataSurat($cKode) ;
        if(isset($vaData)){
            $openFile = $this->BacaFile($vaData['path']) ;
            echo('
                with(bos.mstsurathr.obj){
                    $("#cKode").val("'.$vaData['kode'].'");
                    $("#chKode").val("'.$vaData['kode'].'");
                    $("#cJudul").val("'.$vaData['judul'].'");
                    $("#dTgl").val("'.date_2d($vaData['tgl']).'");
                }
                bos.mstsurathr.setContentJS(\''.json_decode($openFile).'\')
            ');
            
        }
    }

    public function GetKodeSurat()
    {
        $cYear      = date('y') ;
        $cKey  		= $cYear ;
        $n    		= $this->bdb->getincrement($cKey,true,5);
        $cKode    	= $cKey.$n ;
        return $cKode ;
    }

    public function LoadGrid()
    {
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->bdb->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $n      = 0 ;
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vs = $dbr;   
            $vs['No']           = ++$n ;
            $vs['tgl']          = date_2d($dbr['tgl']) ;
            $vs['cmdEdit']      = '<button type="button" onClick="bos.mstsurathr.cmdEdit(\''.$dbr['kode'].'\')"
                                    class="btn btn-success btn-grid">Koreksi</button>' ;
            $vs['cmdEdit']      = html_entity_decode($vs['cmdEdit']) ;

            $vs['cmdDelete']    = '<button type="button" onClick="bos.mstsurathr.cmdDelete(\''.$dbr['kode'].'\')"
                                    class="btn btn-danger btn-grid">Hapus</button>' ;
            $vs['cmdDelete']    = html_entity_decode($vs['cmdDelete']) ;

            $vare[]     = $vs ; 
        }

        $vare   = array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function ValidSaving()
    {
        $va         = $this->input->post() ;
        $lValid     = true ;
        $cKode      = $va['cKode'];
        $chKode     = $va['chKode'];
        $cJudul     = $va['cJudul'];
        $txtDraft   = $va['txtDraft'];
        $msgErr     = "" ;

        if(empty($cKode) && empty($chKode)){
            $va['cKode'] = $this->GetKodeSurat() ;
        }else if(!empty($chKode)){
            $va['cKode'] = $chKode ;
        }


        // validation start
        $cKode = $va['cKode'];
        if(trim(empty($cKode))){
            $lValid = false ;
            $msgErr .= "- Kode Tidak Boleh Kosong! \\n" ;
        }

        if(trim(empty($cJudul))){
            $lValid = false ;
            $msgErr .= "- Judul Tidak Boleh Kosong! \\n" ;
        }

        if(trim(empty($txtDraft))){
            $lValid = false ;
            $msgErr .= "- Draft Tidak Boleh Kosong! \\n" ;
        }
        // validation ends
        
        if($lValid){
            $this->SaveDraft($va);
            $this->CreateContentSurat($va['txtDraft'],$va['cJudul'],$cKode) ;
            echo(' 
                bos.mstsurathr.fin("Data Saved!") ;
                bos.mstsurathr.initForm() ;
                bos.mstsurathr.settab(0) ;
                
            ') ;
        }else{
            echo('
                bos.mstsurathr.fin("'.$msgErr.'") ;  
            ');
        }
    }

    public function SaveDraft($va)
    {
        $this->bdb->SaveDraft($va) ;
    }

    public function CreateContentSurat($cContent,$cFileName,$cKode)
    {
        $cFileName          = str_replace(" ","",$cFileName) ;
        $file_to_write      = $cFileName.'.data';  
        $content_to_write   = $cContent;
        $cDir               = "./masterSurat" ;
        if(!is_dir($cDir)){
            mkdir($cDir,0777);
        }
            
        $file = fopen("./masterSurat/".$file_to_write,"w") or die("Unable to open file!");    
        fwrite($file, $content_to_write); 
        fclose($file);    

        // Save The Path
        $cWhereUpd = "kode = '$cKode'";
        $vaUpdate  = array("path"=>$file_to_write);
        $this->bdb->Update("surat",$vaUpdate,$cWhereUpd);
    }

    function BacaFile($cFileName){
        $cFile      = "./masterSurat/" . $cFileName ;  
        $file       = fopen($cFile,"r") or die("Unable to open file!");    
        $cContent   = fread($file,filesize($cFile));
        return json_encode($cContent) ;
    }

    public function UploadTinyMCE()
    {
        $tempdir = "./uploads/TinyMCEContent";
        if(!is_dir($tempdir)){
            mkdir($tempdir,0777);
        }
        /*
        if (!file_exists($tempdir))
            mkdir($tempdir);*/
        
        reset($_FILES);
        $temp = current($_FILES);
        if(is_uploaded_file($temp['tmp_name'])){
            if(isset($_SERVER['HTTP_ORIGIN'])){
                //if(in_array($_SERVER['HTTP_ORIGIN'])){
                    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
                /*}else{
                    header("HTTP/1.1 403 Origin Denied");
                    return;
                }*/
            }
        
            if(preg_match("/([^\w\s\d\-_~,;:\[\]\(\).])|([\.]{2,})/", $temp['name'])){
                header("HTTP/1.1 400 Invalid file name.");
                return;
            }
        
            if(!in_array(strtolower(pathinfo($temp['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png", "mp4"))){
                header("HTTP/1.1 400 Invalid extension.");
                return;
            }
            $HNow           = date('his');
            $cNamaFile      = md5(rand(0,10000) . $HNow . session_id()) ;
            $acak           = $cNamaFile . ".png";
            $filetowrite    = $tempdir."/" . $acak;
            move_uploaded_file($temp['tmp_name'], $filetowrite);
        
            echo json_encode(array('location' => $filetowrite));
        } else {
            header("HTTP/1.1 500 Server Error");
        }
    }

    public function deleting()
    {
        $cKode = $this->input->post('cKode');
        $this->bdb->deleting($cKode);
        // if($delete){
            echo('
                bos.mstsurathr.fin("Data Deleted!") ;  
                bos.mstsurathr.settab(0) ; 
                bos.mstsurathr.initForm() ;
                bos.mstsurathr.grid1_reloaddata() ;
            ');
        // }
    }
    
}
