<?php
class Mstpayrollskalalamakerja extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("mst/mstpayrollskalalamakerja_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->bdb 	= $this->mstpayrollskalalamakerja_m ;
    }

    public function index(){
        $arrkolom = array();
        $arrkolom[] = array("field"=> "lama", "caption"=> "Lama (Thn)", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        
        $dbd = $this->bdb->select("payroll_komponen","*","status='1' and perhitungan ='L'");
        while( $dbr = $this->bdb->getrow($dbd) ){
            $arrkolom[] = array("field"=> $dbr['kode'], "caption"=> $dbr['keterangan']."/ ".getperiode($dbr['periode']), "size"=> "150px", "render"=>"float:2","sortable"=>false,
                        "editable"=>array("type"=>"int"));
        }

        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $data['kolomgrid1'] = json_encode($arrkolom);
        $this->load->view("mst/mstpayrollskalalamakerja",$data) ;

    } 

   
    public function loadgrid1(){
        $va = $this->input->post();
        $vare = array();
        $n = 0 ;
        $arrn = array();
        $periode = date("Ym",strtotime($va['tgl']));

        $periode2 = $this->perhitunganhrd_m->getperiode($periode);
        $tglawalperiode = date_2s($periode2['tglawal']);
        $tglakhir = date_2s($periode2['tglakhir']);

        for($i = 0 ;$i <= 40 ; $i++){
            $n++;
            $data = array();
            $data['recid']  = $n;
            $data['lama']  = $i;

            
            $dbd2 = $this->bdb->select("payroll_komponen","*","status='1' and perhitungan ='L'");
            while( $dbr2 = $this->bdb->getrow($dbd2) ){
                
                if($dbr2['resetperiode'] <> '1'){
                    $tglawal = "0000-00-00";
                }else{
                    $tglawal = $tglawalperiode;
                }

                $nominal = $this->perhitunganhrd_m->getnilaigajiskalalamakerja($dbr2['kode'],$i,$tglawal,$tglakhir);
                $data[$dbr2['kode']]=$nominal;
            }
            
            $vare[] = $data;
        }
        $vare = json_encode($vare);

        echo ('
                w2ui["bos-form-mstpayrollskalalamakerja_grid1"].add(' . $vare . ');
              ');

    }

    public function init(){
        savesession($this, "ssmstpayrollskalalamakerja_kode", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $error = $this->bdb->saving($va) ;
        if($error == ""){
            echo(' bos.mstpayrollskalalamakerja.grid1_loaddata() ;  ') ;
        }else{
            echo('
                alert("'.$error.'");
                bos.mstpayrollskalalamakerja.grid1_loaddata() ;  
                ') ;
        }
        
    }

  

    public function detail(){
        $va 	= $this->input->post() ;
        echo('w2ui["bos-form-mstpayrollskalalamakerja_grid3"].clear();');
        $vdb = $this->mstpayrollskalalamakerja_m->getdata($va['payroll']) ;
        if( $dbr = $this->mstpayrollskalalamakerja_m->getrow($vdb) ){
            echo('
                  with(bos.mstpayrollskalalamakerja.obj){
                     find("#payroll").val("'.$dbr['keterangan'].'") ;
                  }
              ') ;
            
            $vare = array();
            $n = 0 ;
            $dbd2 = $this->mstpayrollskalalamakerja_m->getdataskala($va['payroll']);
            while( $dbr2 = $this->mstpayrollskalalamakerja_m->getrow($dbd2) ){
                $n++;
                $vare[] = array("recid"=>$n,"lamakerja"=>$dbr2['lamakerja'],"tgl"=>date_2d($dbr2['tgl']),"nominal"=>$dbr2['nominal']);
            }

            $vare = json_encode($vare);
            echo('
              bos.mstpayrollskalalamakerja.loadmodalpreview("show") ;
              w2ui["bos-form-mstpayrollskalalamakerja_grid3"].add('.$vare.');
            ');
        }
    }

}
?>
