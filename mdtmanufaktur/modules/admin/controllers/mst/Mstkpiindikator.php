<?php
class Mstkpiindikator extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("mst/mstkpiindikator_m") ;
        $this->bdb 	= $this->mstkpiindikator_m ;
    }

    public function index(){
        $data = array();
        $this->load->view("mst/mstkpiindikator",$data) ;
    } 

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->bdb->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vs = $dbr;   

            $vs['penilaian'] = "Umum";
            if($dbr['penilaian'] == "1") $vs['penilaian'] = "Khusus";

            $vs['cmdedit']    = '<button type="button" onClick="bos.mstkpiindikator.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Koreksi</button>' ;
            $vs['cmdedit']	   = html_entity_decode($vs['cmdedit']) ;

            $vs['cmddelete']  = '<button type="button" onClick="bos.mstkpiindikator.cmdnonaktifkan(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Non Aktifkan</button>' ;
            if($dbr['status'] == "2")$vs['cmddelete']  = '<button type="button" onClick="bos.mstkpiindikator.cmdaktifkan(\''.$dbr['kode'].'\')"
            class="btn btn-success btn-grid">Aktifkan</button>' ;
            $vs['cmddelete']  = html_entity_decode($vs['cmddelete']) ;

            $vare[]		= $vs ; 
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "ssmstkpiindikator_kode", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $id 	= getsession($this, "ssmstkpiindikator_kode") ; 
        $error = $this->bdb->saving($va, $id) ;
        if($error == ""){
            echo(' bos.mstkpiindikator.settab(0) ;  ') ;
        }else{
            echo('alert("'.$error.'");');
        }
        
    }

    public function nonaktifkan(){
        $va 	= $this->input->post() ; 
        $kode 	= $va['kode'] ;
        $this->bdb->edit("kpi_indikator",array("status"=>"2"), "kode = " . $this->bdb->escape($kode)) ;
        echo(' bos.mstkpiindikator.grid1_reload() ; ') ;
    }

    public function aktifkan(){
        $va 	= $this->input->post() ; 
        $kode 	= $va['kode'] ;
        $this->bdb->edit("kpi_indikator",array("status"=>"1"), "kode = " . $this->bdb->escape($kode)) ;
        echo(' bos.mstkpiindikator.grid1_reload() ; ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $kode 	= $va['kode'] ;
        $data = $this->mstkpiindikator_m->getdata($kode) ;
        if( $dbr = $this->mstkpiindikator_m->getrow($data) ){
            savesession($this, "ssmstkpiindikator_kode", $dbr['kode']) ;
            $periode[] = "";
            if($dbr['periode'] <> "")$periode[] = array("id"=>$dbr['periode'],"text"=>getperiode($dbr['periode']));
            $jabatan[] = array("id"=>$dbr['jabatan'],"text"=>$this->bdb->getval("keterangan", "kode = '{$dbr['jabatan']}'", "jabatan"));

            echo('
            with(bos.mstkpiindikator.obj){
                find(".nav-tabs li:eq(1) a").tab("show") ;
                find("#kode").val("'.$dbr['kode'].'") ;
                find("#keterangan").val("'.$dbr['keterangan'].'").focus() ;
                find("#penilaian").val("'.$dbr['penilaian'].'").trigger("change");
                find("#perhitungan").val("'.$dbr['perhitungan'].'").trigger("change");
                find("#periode").sval('.json_encode($periode).');
                find("#jabatan").sval('.json_encode($jabatan).');
            }
            bos.mstkpiindikator.settab(1) ;
         ') ;
        }
    }

    public function getkode(){
        $n  = $this->mstkpiindikator_m->getkode(FALSE) ;
  
        echo('
          bos.mstkpiindikator.obj.find("#kode").val("'.$n.'") ;
          ') ;
     }

}
?>
