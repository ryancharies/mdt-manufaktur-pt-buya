<?php
class Mstpayrollskalakaryawan extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("mst/mstpayrollskalakaryawan_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->bdb 	= $this->mstpayrollskalakaryawan_m ;
    }

    public function index(){
        $arrkolom = array();
        $arrkolom[] = array("field"=> "kode", "caption"=> "Kode", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "payroll", "caption"=> "Kompoonen", "size"=> "150px", "sortable"=>false,"frozen"=>true);
        
        $dbd = $this->bdb->select("payroll_komponen","*","status='1' and perhitungan ='J'");
        while( $dbr = $this->bdb->getrow($dbd) ){
            $arrkolom[] = array("field"=> $dbr['kode'], "caption"=> $dbr['keterangan']."/ ".getperiode($dbr['periode']), "size"=> "150px", "render"=>"float:2","sortable"=>false,
                        "editable"=>array("type"=>"int"));
        }

        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $data['kolomgrid2'] = json_encode($arrkolom);
        $this->load->view("mst/mstpayrollskalakaryawan",$data) ;

    } 

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->bdb->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vs = $dbr;   
            $vs['cmddetail']    = '<button type="button" onClick="bos.mstpayrollskalakaryawan.cmddetail(\''.$dbr['kode'].'\')"
                           class="btn btn-primary btn-grid"><i class="fa fa-gear"></i></button>' ;
            $vs['cmddetail']	   = html_entity_decode($vs['cmddetail']) ;

            $vare[]		= $vs ; 
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function loadgrid2(){
        $va = $this->input->post();
        $vdb = $this->mstpayrollskalakaryawan_m->loadkomponen($va);
        $dbd = $vdb['db'];
        $vare = array();
        $periode = date("Ym",strtotime($va['tgl']));

        $periode2 = $this->perhitunganhrd_m->getperiode($periode);
        $tglawalperiode = date_2s($periode2['tglawal']);
        $tglakhir = date_2s($periode2['tglakhir']);
        $n = 0 ;
        $arrn = array();
        while ($dbr = $this->mstpayrollskalakaryawan_m->getrow($dbd)) {
            $n++;
            $data = array();
            
            if($dbr['resetperiode'] <> '1'){
                $tglawal = "0000-00-00";
            }else{
                $tglawal = $tglawalperiode;
            }

            $nominal = $this->perhitunganhrd_m->getnilaigajiskalakaryawan($dbr['kode'],$va['nip'],$tglawal,$tglakhir);
            $data = array("recid"=>$n,"kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan'] ,"periode"=>$dbr['periode'],"nominal"=>$nominal);
            
            $vare[] = $data;
        }
        $selectpjct = implode(",",$arrn);
        $vare = json_encode($vare);

        echo ('
                w2ui["bos-form-mstpayrollskalakaryawan_grid2"].add(' . $vare . ');
              ');

    }

    public function init(){
        savesession($this, "ssmstpayrollskalakaryawan_kode", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $error = $this->bdb->saving($va) ;
        if($error == ""){
            echo(' bos.mstpayrollskalakaryawan.grid2_loaddata() ;  ') ;
        }else{
            echo('
                alert("'.$error.'");
                bos.mstpayrollskalakaryawan.grid2_loaddata() ;  
                ') ;
        }
        
    }

    public function detail(){
        $va 	= $this->input->post() ;
        echo('w2ui["bos-form-mstpayrollskalakaryawan_grid2"].clear();');
        $vdb = $this->mstpayrollskalakaryawan_m->getdata($va['payroll']) ;
        if( $dbr = $this->mstpayrollskalakaryawan_m->getrow($vdb) ){
            $tgl = date("d-m-Y");
            $arrjabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$tgl);
            echo('
                  with(bos.mstpayrollskalakaryawan.obj){
                     find("#nip").val("'.$dbr['kode'].'") ;
                     find("#nama").val("'.$dbr['nama'].'") ;
                     find("#alamat").val("'.$dbr['alamat'].'") ;
                     find("#jabatan").val("'.$arrjabatan['keterangan'].'") ;
                  }
              ') ;
            
            echo('
                bos.mstpayrollskalakaryawan.settab(1) ;
            ');
        }
    }

}
?>
