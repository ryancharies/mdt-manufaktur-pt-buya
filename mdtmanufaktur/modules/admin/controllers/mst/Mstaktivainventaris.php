<?php
class Mstaktivainventaris extends Bismillah_Controller
{
    protected $bdb;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("bdate");
        $this->load->model("mst/mstaktivainventaris_m");
        $this->load->model("func/updtransaksi_m");
        $this->load->model("func/func_m");
        $this->bdb = $this->mstaktivainventaris_m;
    }

    public function index()
    {
        // mengambil min tgl transaksi yang aktif
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data = array();
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $this->load->view("mst/mstaktivainventaris",$data);

    }

    public function loadgrid_where($bs, $s){
        $this->duser();

        $this->db->where('tglperolehan >=', $bs['tglawal']);
        $this->db->where('tglperolehan <=', $bs['tglakhir']);

        
        if($bs['skd_cabang'] !== 'null'){
            $bs['skd_cabang'] = json_decode($bs['skd_cabang'],true);
            $this->db->group_start();
            foreach($bs['skd_cabang'] as $kdc){
                $this->db->or_where('cabang',$kdc);
            }
            $this->db->group_end();

        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['cabang'])){
                    $this->db->group_start();
                    foreach($this->aruser['data_var']['cabang'] as $kdc){
                        $this->db->or_where('cabang', $kdc);
                    }
                    $this->db->group_end();
                }
            }
        }

        if($s !== ''){
            $this->db->group_start();
                $this->db->or_like(array('faktur'=>$s, 'keterangan'=>$s));
            $this->db->group_end();
        }
    }

    public function loadgrid()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va = json_decode($this->input->post('request'), true);
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->db->escape_like_str($search) ;
        $vare = array();
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);

        $this->loadgrid_where($va, $search);
        $f = "count(id) jml";
        $dbdt = $this->db->select($f)
            ->from("aset")
            ->get();
        $rtot  = $dbdt->row_array();
        if($rtot['jml'] > 0){

            $this->loadgrid_where($va, $search);
            $f2 = "id,kode,faktur,vendors,vendor,keterangan,golongan,kelompok,cabang,tglperolehan,
                lama,hargaperolehan,unit,jenispenyusutan,tarifpenyusutan,residu";
            $dbd = $this->db->select($f2)->from("aset")
                ->order_by('kode ASC')
                ->limit($limit)
                ->get();
        
            foreach($dbd->result_array() as $dbr){              

                if($dbr['vendor'] == "ZZZ"){
                    $arrven = array();
                    $arrven[$dbr['vendor']] = array();
                    $arrven[$dbr['vendor']]['vendor'] = $dbr['vendor'];
                    $arrven[$dbr['vendor']]['namavendor'] = $this->bdb->getval("nama", "kode = '{$dbr['vendor']}'", "supplier");
                    $arrven[$dbr['vendor']]['hutang'] = $dbr['hargaperolehan'];
                    $this->bdb->edit("aset",array("vendors"=>json_encode($arrven)),"kode = '{$dbr['kode']}'");
                    // print_r($arrven);
                }
                


                $dbr['tglperolehan'] = date_2d($dbr['tglperolehan']);
                $vs = $dbr;
                $vs['cmdedit'] = '<button type="button" onClick="bos.mstaktivainventaris.cmdedit(\'' . $dbr['kode'] . '\')"
                            class="btn btn-default btn-grid">Koreksi</button>';
                $vs['cmdedit'] = html_entity_decode($vs['cmdedit']);

                $vs['cmddelete'] = '<button type="button" onClick="bos.mstaktivainventaris.cmddelete(\'' . $dbr['kode'] . '\',\'' . $dbr['faktur'] . '\')"
                            class="btn btn-danger btn-grid">Hapus</button>';
                $vs['cmddelete'] = html_entity_decode($vs['cmddelete']);
                if($tglmin > date_2s($dbr['tglperolehan'])){
                    $vs['cmdedit'] = "";
                    $vs['cmddelete'] = "";
                }

                $vare[] = $vs;
            }

        }

        
        $vare = array("total" => $rtot['jml'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function init()
    {
        savesession($this, "ssmstaktivainventaris_id", "");
    }

    public function saving()
    {
        $va = $this->input->post();
        $id = getsession($this, "ssmstaktivainventaris_id");

        $this->bdb->saving($va, $id);
        echo (' bos.mstaktivainventaris.settab(0) ;  ');
    }

    public function deleting()
    {
        $va = $this->input->post();
        $kode = $va['kode'];
        $faktur = $va['faktur'];
        $error = "Gagal hapus data, Data tidak valid!!";
        if ($kode != "") {
            $error = "ok";
            $dbd1 = $this->bdb->select("aset", "id", "fakturpj <> '' and kode = '$kode'");
            if ($dbr1 = $this->bdb->getrow($dbd1)) {
                $error = "Data tidak bisa dihapus karena sudah dijual!!";
            } else {
                $dbd = $this->bdb->select("hutang_kartu", "id", "faktur <> '$faktur' and fkt = '$faktur' and fkt <> ''");
                if ($dbr = $this->bdb->getrow($dbd)) {
                    $error = "Data tidak bisa dihapus karena sudah masuk ke proses pelunasan!!";
                } else {

                    $this->bdb->delete("aset", "kode = " . $this->bdb->escape($kode));
                    if ($faktur != "") {
                        $this->bdb->delete("keuangan_bukubesar", "faktur = " . $this->bdb->escape($faktur));
                        $this->bdb->delete("hutang_kartu", "faktur = " . $this->bdb->escape($faktur));
                    }
                }
            }
            if ($error == "ok") {
                echo (' bos.mstaktivainventaris.grid1_reload() ; ');
            } else {
                echo ('
                    alert("' . $error . '");
                ');
            }
        } else {
            echo ('
                    alert("' . $error . '");
                ');
        }

    }

    public function editing()
    {
        $va = $this->input->post();
        $kode = $va['kode'];
        $data = $this->mstaktivainventaris_m->getdata($kode);
        if ($dbr = $this->mstaktivainventaris_m->getrow($data)) {
            savesession($this, "ssmstaktivainventaris_id", $dbr['kode']);
            $golaset[] = array("id" => $dbr['golongan'], "text" => $dbr['golongan'] . " - " . $dbr['ketgolongan']);
            $kelaset[] = array("id" => $dbr['kelompok'], "text" => $dbr['kelompok'] . " - " . $dbr['ketkelompok']);
            $cabang[] = array("id" => $dbr['cabang'], "text" => $dbr['cabang'] . " - " . $dbr['ketcabang']);
            // $vendor[] = array("id" => $dbr['vendor'], "text" => $dbr['vendor'] . " - " . $dbr['ketvendor']);
            $dbr['tglperolehan'] = date_2d($dbr['tglperolehan']);
            echo ('
                with(bos.mstaktivainventaris.obj){
                    find(".nav-tabs li:eq(1) a").tab("show") ;
                    find("#kode").val("' . $dbr['kode'] . '") ;
                    find("#faktur").val("' . $dbr['faktur'] . '") ;
                    find("#golaset").sval(' . json_encode($golaset) . ') ;
                    find("#kelaset").sval(' . json_encode($kelaset) . ') ;
                    find("#cabang").sval(' . json_encode($cabang) . ') ;
                    find("#cabang").attr("readonly", "readonly");
                    
                    find("#tglperolehan").val("' . $dbr['tglperolehan'] . '") ;
                    find("#hp").val("' . string_2s($dbr['hargaperolehan']) . '") ;
                    find("#unit").val("' . $dbr['unit'] . '") ;
                    find("#lama").val("' . $dbr['lama'] . '") ;

                    find("#tarifpeny").val("' . $dbr['tarifpenyusutan'] . '") ;
                    find("#residu").val("' . string_2s($dbr['residu']) . '") ;
                    find("#keterangan").val("' . $dbr['keterangan'] . '").focus() ;

                }

                bos.mstaktivainventaris.setopt("jenis","' . $dbr['jenispenyusutan'] . '");
            ');

            

            //loadgrid detail
            $vare = array();
            $vendors = json_decode($dbr['vendors'],true) ;

            foreach($vendors as $key => $val){
                $vaset['act'] = '<button type="button" onClick="bos.mstaktivainventaris.grid2_deleterow('.$val['vendor'].')"
                                        class="btn btn-danger btn-grid">Delete</button>' ;
                $vaset['act']	= html_entity_decode($vaset['act']) ;
                $vaset['recid'] = $val['vendor'];
                $vaset['vendor'] = $val['vendor'];
                $vaset['namavendor'] = $this->bdb->getval("nama", "kode = '{$val['vendor']}'", "supplier");
                $vaset['hutang'] = $val['hutang'];

                $vare[]             = $vaset ;

            }
            $vare = json_encode($vare);
            echo('
                w2ui["bos-form-mstaktivainventaris_grid2"].add('.$vare.');
                bos.mstaktivainventaris.initdetail();
                bos.mstaktivainventaris.grid2_hitungtotal();
                bos.mstaktivainventaris.settab(1) ;

            ');
        }
    }

    public function getkelaset()
    {
        $va = $this->input->post();
        $this->db->or_where('kode',$va['kode']);
        $db = $this->db->select("*")->from("aset_kelompok")->get(); //print_r($this->db->last_query());
        $r  = $db->row_array();
        echo json_encode($r);
    }

    public function getkode()
    {
        $n = $this->bdb->getkode(false);
        echo ('
        bos.mstaktivainventaris.obj.find("#kode").val("' . $n . '") ;
        ');
    }

    public function getfaktur(){
        $va 	= $this->input->post() ;
        $faktur  = $this->func_m->getfaktur("AS",$va['tgl'],$va['cabang'],FALSE) ;
        echo($faktur);
    }

}
