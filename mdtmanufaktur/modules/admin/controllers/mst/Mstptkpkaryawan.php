<?php
class Mstptkpkaryawan extends Bismillah_Controller{
   public function __construct(){
		parent::__construct() ;
      $this->load->model('mst/mstptkpkaryawan_m') ;
      $this->load->helper('bdate') ;
      $this->load->helper('bdir') ;
	}

   public function index(){
      $tglmin = $this->mstptkpkaryawan_m->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
      $data = array();
      $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
      $this->load->view('mst/mstptkpkaryawan',$data) ;
   }

   public function loadgrid(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
      $vdb    = $this->mstptkpkaryawan_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->mstptkpkaryawan_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['tgl'] = date_2d($vaset['tgl']);
         $vaset['cmdedit']    = '<button type="button" onClick="bos.mstptkpkaryawan.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-success btn-grid">PTKP</button>' ;
         $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function loadgrid2(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
     // print_r($va);
      $vdb    = $this->mstptkpkaryawan_m->loadgrid2($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->mstptkpkaryawan_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['tgl'] = date_2d($vaset['tgl']);
         $vaset['cmddelete']    = '<button type="button" onClick="bos.mstptkpkaryawan.cmddeletemj(\''.$dbr['nip'].'\',\''.$dbr['tgl'].'\')"
                           class="btn btn-danger btn-grid">Delete</button>' ;
         $vaset['cmddelete']	   = html_entity_decode($vaset['cmddelete']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function init(){
      savesession($this, "ssptkpkaryawan_kode", "") ;
   }

   public function saving(){
      $va 	= $this->input->post() ;
      $return = $this->mstptkpkaryawan_m->saving($va) ;
      if($return == "ok"){
         echo('
            alert("Data telah disimpan!!");
            bos.mstptkpkaryawan.grid2_reloaddata() ;
         ');
      }else{
         echo('
            alert("'.$return.'");
         ');
      }
      
   }

   public function editing(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->mstptkpkaryawan_m->getdata($kode) ;
      if(!empty($data)){
         echo('
            with(bos.mstptkpkaryawan.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$data['kode'].'") ;
               find("#nama").val("'.$data['nama'].'") ;
               find("#ktp").val("'.$data['ktp'].'") ;
               find("#notelepon").val("'.$data['telepon'].'") ;
               find("#tglmasuk").val("'.date_2s($data['tgl']).'") ;
               find("#alamat").val("'.$data['alamat'].'") ;
            }
            bos.mstptkpkaryawan.settab(1) ;

         ');

      }
   }

   public function deleting(){
      $va 	= $this->input->post() ;
      $error = $this->mstptkpkaryawan_m->deleting($va) ;
      if($error == "ok"){
         echo(' bos.mstptkpkaryawan.grid2_reloaddata() ; ') ;
      }else{
         echo('alert("'.$error.'");');
      }
   }
    
   public function seekptkp(){
        $search     = $this->input->get('q');
        $vdb    = $this->mstptkpkaryawan_m->seekptkp($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->mstptkpkaryawan_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ". $dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
   }
   
}
?>
