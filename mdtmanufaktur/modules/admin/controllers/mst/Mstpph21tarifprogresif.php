<?php
class Mstpph21tarifprogresif extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("mst/mstpph21tarifprogresif_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->bdb 	= $this->mstpph21tarifprogresif_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $this->load->view("mst/mstpph21tarifprogresif",$data) ;

    } 

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->bdb->loadgrid($va) ; 
        foreach( $vdb as $key => $val){
            $vs = $val;   
            $vs['tgl'] = date_2d($vs['tgl']);
            $vs['cmdedit']    = '<button type="button" onClick="bos.mstpph21tarifprogresif.cmdedit(\''.$val['tgl'].'\')"
                           class="btn btn-primary btn-grid"><i class="fa fa-bars"></i></button>' ;
            $vs['cmdedit']	   = html_entity_decode($vs['cmdedit']) ;
            $vs['npwp']	   = html_entity_decode($vs['npwp']) ;
            $vs['nonnpwp']	   = html_entity_decode($vs['nonnpwp']) ;

            $vare[]		= $vs ; 
        }

        $vare 	= array("total"=>count($vare), "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function loadgrid2(){
        //$va = $this->input->post();
        $va     = json_decode($this->input->post('request'), true) ;
        $vdb = $this->mstpph21tarifprogresif_m->loadgrid2($va);
        $vare = array();

        $max = 20;
        $n = 0 ;
        foreach ($vdb as $key => $val) {
            $n++;
            $data = array();
            $data['recid'] = $n;
            $data['penghasilan']   = $val['penghasilan'];
            $data['perstarif']=$val['perstarif'];
            
            $vare[] = $data;
        }

        $n++;
        for($i = $n ; $n <= 20 ; $n++){
            $vare[] = array("recid"=>$n,"penghasilan"=>"0","perstarif"=>"0");
        }
        //$totdata = count($vare);
        //$vare = json_encode($vare);

        // echo ('
        //         w2ui["bos-form-mstpph21tarifprogresif_grid2"].add(' . $vare . ');
        //       ');

        $vare 	= array("total"=>count($vare), "records"=>$vare ) ;
        echo(json_encode($vare)) ;

    }

    public function loadgrid3(){
        $va     = json_decode($this->input->post('request'), true) ;
       // print_r($va);

        $vdb = $this->mstpph21tarifprogresif_m->loadgrid3($va);
        $vare = array();

        $max = 20;
        $n = 0 ;
        foreach ($vdb as $key => $val) {
            $n++;
            $data = array();
            $data['recid'] = $n;
            $data['penghasilan']   = $val['penghasilan'];
            $data['perstarif']=$val['perstarif'];
            
            $vare[] = $data;
        }

        $n++;
        for($i = $n ; $n <= 20 ; $n++){
            $vare[] = array("recid"=>$n,"penghasilan"=>"0","perstarif"=>"0");
        }
        // $vare = json_encode($vare);

        // echo ('
        //         w2ui["bos-form-mstpph21tarifprogresif_grid3"].add(' . $vare . ');
        //       ');
        
        $vare 	= array("total"=>count($vare), "records"=>$vare ) ;
        echo(json_encode($vare)) ;

    }

    public function init(){
        savesession($this, "ssmstpph21tarifprogresif_kode", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $error = $this->bdb->saving($va) ;
        if($error == ""){
            echo(' bos.mstpph21tarifprogresif.settab(0) ; ') ;
        }else{
            echo('
                alert("'.$error.'");
                ') ;
        }
        
    }

    public function edit(){
        $va 	= $this->input->post() ;
        $va['tgl'] = date_2d($va['tgl']);
        echo('
                  with(bos.mstpph21tarifprogresif.obj){
                     find("#tgl").val("'.$va['tgl'].'") ;
                  }
                
                  bos.mstpph21tarifprogresif.settab(1) ;
              ') ;
        
    }

}
?>
