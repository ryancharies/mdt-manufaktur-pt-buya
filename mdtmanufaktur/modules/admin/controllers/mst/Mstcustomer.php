<?php
class Mstcustomer extends Bismillah_Controller{
   public function __construct(){
		parent::__construct() ;
      $this->load->model('mst/mstcustomer_m') ;
      $this->load->helper('bdate') ;
	}

   public function index(){
      $this->load->view('mst/mstcustomer') ;
   }

   public function loadgrid(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vdati1 = array();
      $vare   = array() ;
      $vdb    = $this->mstcustomer_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->mstcustomer_m->getrow($dbd) ){

         if(!isset($vdati1[$dbr['dati_1']])){
            $vdati1[$dbr['dati_1']] = $this->mstcustomer_m->getval("keterangan", "kode = '{$dbr['dati_1']}'", "dati_1");
         }

         $vaset   = $dbr ;
         $vaset['alamat'] .= " ".$dbr['dati_2'] . " ".$vdati1[$dbr['dati_1']];
         $vaset['cmdedit']    = '<button type="button" onClick="bos.mstcustomer.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Edit</button>' ;
         $vaset['cmddelete']  = '<button type="button" onClick="bos.mstcustomer.cmddelete(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Delete</button>' ;
         $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;
         $vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function init(){
      savesession($this, "sscustomer_kode", "") ;
   }

   public function saving(){
      $va 	= $this->input->post() ;
      $kode 	= getsession($this, "sscustomer_kode") ;
      $this->mstcustomer_m->saving($kode, $va) ;
      echo(' bos.mstcustomer.settab(0) ;  ') ;
   }

   public function editing(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->mstcustomer_m->getdata($kode) ;
      if(!empty($data)){
         savesession($this, "sscustomer_kode", $kode) ;
         $golongan[] = array("id"=>$data['golongan'],"text"=>$data['ketgol']);
         $jenishj[] = array("id"=>$data['jenishj'],"text"=>$data['ketjenishj']);
         $dati_2[] = array("id"=>$data['dati_2'],"text"=>$data['ketdati_2'] ." ". $data['ketdati_1']);
         echo('
            with(bos.mstcustomer.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$data['kode'].'") ;
               find("#nama").val("'.$data['nama'].'").focus() ;
               find("#notelepon").val("'.$data['notelepon'].'") ;
               find("#email").val("'.$data['email'].'") ;
               find("#alamat").val("'.$data['alamat'].'") ;
               find("#golongan").sval('.json_encode($golongan).') ;
               find("#jenishj").sval('.json_encode($jenishj).') ;
               find("#dati_2").sval('.json_encode($dati_2).') ;
            }
            bos.mstcustomer.settab(1) ;
         ') ;

      }
   }

   public function deleting(){
      $va 	= $this->input->post() ;
      $error = $this->mstcustomer_m->deleting($va['kode']) ;
      if($error == "ok"){
         echo(' bos.mstcustomer.grid1_reloaddata() ; ') ;
      }else{
         echo('alert("'.$error.'");');
      }
   }
    
   public function seekgolongan(){
        $search     = $this->input->get('q');
        $vdb    = $this->mstcustomer_m->seekgolongan($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->mstcustomer_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
   }

   public function seekjenishj(){
      $search     = $this->input->get('q');
      $vdb    = $this->mstcustomer_m->seekjenishj($search) ;
      $dbd    = $vdb['db'] ;
      $vare   = array();
      while( $dbr = $this->mstcustomer_m->getrow($dbd) ){
          $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      $Result = json_encode($vare);
      echo($Result) ;
 }
   public function getkode(){
      $n  = $this->mstcustomer_m->getkode(FALSE) ;

      echo('
        bos.mstcustomer.obj.find("#kode").val("'.$n.'") ;
        ') ;
   }
}
?>
