<?php
class Mstsurat extends Bismillah_Controller{
   public function __construct(){
		parent::__construct() ;
      $this->load->model('mst/mstsurat_m') ;
      $this->load->helper('bdate') ;
      $this->load->helper('bdir') ;
	}

   public function index(){
      $tglmin = $this->mstsurat_m->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
      $data = array();
      $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
      $this->load->view('mst/mstsurat',$data) ;
   }

   public function loadgrid(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
      $vdb    = $this->mstsurat_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->mstsurat_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $path = "./uploads/surat/".$dbr['path'];
         $vaset['path'] = '<a href=\''.$path.'\' >Download</a>';
         $vaset['cmdedit']    = '<button type="button" onClick="bos.mstsurat.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Edit</button>' ;
         $vaset['cmddelete']  = '<button type="button" onClick="bos.mstsurat.cmddelete(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Delete</button>' ;
         $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;
         $vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function init(){
      savesession($this, "sssurat_kode", "") ;
   }

   public function saving(){
      
      $va 	= $this->input->post() ;
      //print_r($va);
      $kode 	= getsession($this, "sssurat_kode") ;
      $this->mstsurat_m->saving($kode, $va) ;
      echo(' bos.mstsurat.settab(0) ;  ') ;
   }

   function file_upload($cfg){
      $config	= array("upload_path"=>"tmp/","overwrite"=>true,"allowed_types"=>"*") ;
      $this->load->library('upload', $config);
      if ( ! $this->upload->do_upload(0)){
          echo('
          alert("Hello User : '. $this->upload->display_errors('','') .'") ;
       ') ;
      }else{
         
         $data 	= $this->upload->data() ;
         echo('
            with(bos.mstsurat.obj){
               find("#idl'.$cfg.'").html("") ;
               find("#namafile").val("'.$data['file_name'].'") ;
            }
         ');
      }

   }

   public function editing(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->mstsurat_m->getdata($kode) ;
      if(!empty($data)){
         savesession($this, "sssurat_kode", $kode) ;
         echo('
            with(bos.mstsurat.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$data['kode'].'") ;
               find("#keterangan").val("'.$data['judul'].'").focus() ;
            }
            
         ') ;

         echo('
               bos.mstsurat.settab(1) ;
             ');

      }
   }

   public function deleting(){
      $va 	= $this->input->post() ;
      $error = $this->mstsurat_m->deleting($va['kode']) ;
      if($error == "ok"){
         echo(' bos.mstsurat.grid1_reloaddata() ; ') ;
      }else{
         echo('alert("'.$error.'");');
      }
   }
   
   public function getkode(){
      $n  = $this->mstsurat_m->getkode(FALSE) ;

      echo('
        bos.mstsurat.obj.find("#kode").val("'.$n.'") ;
        ') ;
   }
}
?>
