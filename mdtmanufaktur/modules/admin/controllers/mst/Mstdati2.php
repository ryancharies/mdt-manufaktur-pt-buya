<?php
class Mstdati2 extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->model("mst/mstdati2_m") ;
        $this->bdb 	= $this->mstdati2_m ;
    }

    public function index(){
        $this->load->view("mst/mstdati2") ;

    } 

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->bdb->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vs = $dbr;   
            $vs['cmdedit']    = '<button type="button" onClick="bos.mstdati2.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Koreksi</button>' ;
            $vs['cmdedit']	   = html_entity_decode($vs['cmdedit']) ;

            $vs['cmddelete']  = '<button type="button" onClick="bos.mstdati2.cmddelete(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Hapus</button>' ;
            $vs['cmddelete']  = html_entity_decode($vs['cmddelete']) ;

            $vare[]		= $vs ; 
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "ssmstdati2_id", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $id 	= getsession($this, "ssmstdati2_id") ; 

        $this->bdb->saving($va, $id) ;
        echo(' bos.mstdati2.settab(0) ;  ') ;
    }

    public function deleting(){
        $va 	= $this->input->post() ; 
        $kode 	= $va['kode'] ;
        $this->bdb->delete("dati_1", "kode = " . $this->bdb->escape($kode)) ;
        echo(' bos.mstdati2.grid1_reload() ; ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $kode 	= $va['kode'] ;
        $dati1 = array();
        $data = $this->mstdati2_m->getdata($kode) ;
        if( $dbr = $this->mstdati2_m->getrow($data) ){
            savesession($this, "ssmstdati2_id", $dbr['kode']) ;
            $dati1[]  = array("id"=>$dbr['dati_1'],"text"=>$dbr['ketdati1']);

            echo('
            with(bos.mstdati2.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$dbr['kode'].'") ;
               find("#keterangan").val("'.$dbr['keterangan'].'").focus() ;
               find("#dati_1").sval('.json_encode($dati1).');

            }
            bos.mstdati2.settab(1) ;
         ') ;
        }
    }

    public function getkode(){
        $n  = $this->bdb->getkode(FALSE) ;
  
        echo($n) ;
     }
  

}
?>
