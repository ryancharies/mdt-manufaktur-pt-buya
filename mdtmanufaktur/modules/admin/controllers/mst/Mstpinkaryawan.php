<?php
class Mstpinkaryawan extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("mst/mstpinkaryawan_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->bdb 	= $this->mstpinkaryawan_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $this->load->view("mst/mstpinkaryawan",$data) ;

    } 

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->bdb->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vs = $dbr;   
            $vs['cmddetail']    = '<button type="button" onClick="bos.mstpinkaryawan.cmddetail(\''.$dbr['kode'].'\')"
                           class="btn btn-primary btn-grid"><i class="fa fa-gear"></i></button>' ;
            $vs['cmddetail']	   = html_entity_decode($vs['cmddetail']) ;

            $vare[]		= $vs ; 
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function loadgrid2(){
        $va = $this->input->post();
        $vdb = $this->mstpinkaryawan_m->loadmesin($va);
        $dbd = $vdb['db'];
        $vare = array();
        $n = 0 ;
        $arrn = array();
        while ($dbr = $this->mstpinkaryawan_m->getrow($dbd)) {
            $n++;
            $data = array();
            $data = array("recid"=>$n,"kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan'],"pin"=>$dbr['pin']);
            
            $vare[] = $data;
        }
        $vare = json_encode($vare);

        echo ('
                w2ui["bos-form-mstpinkaryawan_grid2"].add(' . $vare . ');
              ');

    }

    public function init(){
        savesession($this, "ssmstpinkaryawan_kode", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $error = $this->bdb->saving($va) ;
        if($error == ""){
            echo(' bos.mstpinkaryawan.grid2_loaddata() ;  ') ;
        }else{
            echo('
                alert("'.$error.'");
                bos.mstpinkaryawan.grid2_loaddata() ;  
                ') ;
        }
        
    }

    public function detail(){
        $va 	= $this->input->post() ;
        echo('w2ui["bos-form-mstpinkaryawan_grid2"].clear();');
        $vdb = $this->mstpinkaryawan_m->getdata($va['payroll']) ;
        if( $dbr = $this->mstpinkaryawan_m->getrow($vdb) ){
            $tgl = date("d-m-Y");
            $arrjabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$tgl);
            echo('
                  with(bos.mstpinkaryawan.obj){
                     find("#nip").val("'.$dbr['kode'].'") ;
                     find("#nama").val("'.$dbr['nama'].'") ;
                     find("#alamat").val("'.$dbr['alamat'].'") ;
                     find("#jabatan").val("'.$arrjabatan['keterangan'].'") ;
                  }
              ') ;
            
            echo('
                bos.mstpinkaryawan.settab(1) ;
            ');
        }
    }

}
?>
