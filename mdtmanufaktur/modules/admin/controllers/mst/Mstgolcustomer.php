<?php
class Mstgolcustomer extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
		parent::__construct() ;
      $this->load->model('mst/mstgolcustomer_m') ;
      $this->load->helper('bdate') ;
	}

   public function index(){
      $this->load->view('mst/mstgolcustomer') ;
   }

   public function loadgrid(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
      $vdb    = $this->mstgolcustomer_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->mstgolcustomer_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['cmdedit']    = '<button type="button" onClick="bos.mstgolcustomer.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Edit</button>' ;
         $vaset['cmddelete']  = '<button type="button" onClick="bos.mstgolcustomer.cmddelete(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Delete</button>' ;
         $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;
         $vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function init(){
      savesession($this, "ssgolcustomer_kode", "") ;
   }

   public function saving(){
      $va 	= $this->input->post() ;
      $kode 	= getsession($this, "ssgolcustomer_kode") ;
      $this->mstgolcustomer_m->saving($kode, $va) ;
      echo(' bos.mstgolcustomer.init() ; ') ;
   }

   public function editing(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->mstgolcustomer_m->getdata($kode) ;
      if(!empty($data)){
         savesession($this, "ssgolcustomer_kode", $kode) ;
         $rekrj[] = array("id"=>$data['rekrj'],"text"=>$data['rekrj'] ." - ". $this->mstgolcustomer_m->getval("keterangan", "kode = '{$data['rekrj']}'", "keuangan_rekening"));
         $rekpj[] = array("id"=>$data['rekpj'],"text"=>$data['rekpj'] ." - ". $this->mstgolcustomer_m->getval("keterangan", "kode = '{$data['rekpj']}'", "keuangan_rekening"));
         $rekdisc[] = array("id"=>$data['rekdisc'],"text"=>$data['rekdisc'] ." - ". $this->mstgolcustomer_m->getval("keterangan", "kode = '{$data['rekdisc']}'", "keuangan_rekening"));
         echo('
            with(bos.mstgolcustomer.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$data['kode'].'") ;
               find("#rekrj").sval('.json_encode($rekrj).') ;
               find("#rekdisc").sval('.json_encode($rekdisc).') ;
               find("#rekpj").sval('.json_encode($rekpj).') ;
               find("#keterangan").val("'.$data['keterangan'].'").focus() ;
            }
            bos.mstgolcustomer.settab(1) ;
         ') ;
      }
   }

   public function deleting(){
      $va 	= $this->input->post() ;
      $this->mstgolcustomer_m->deleting($va['kode']) ;
      echo(' bos.mstgolcustomer.grid1_reloaddata() ; ') ;
   }

    public function seekrekening(){
        $search     = $this->input->get('q');
        $vdb    = $this->mstgolcustomer_m->seekrekening($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->mstgolcustomer_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ". $dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

}
?>
