<?php
class Mstmesinabsensi extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->model("mst/mstmesinabsensi_m") ;
        $this->bdb 	= $this->mstmesinabsensi_m ;
    }

    public function index(){
        $this->load->view("mst/mstmesinabsensi") ;

    } 

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->bdb->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vs = $dbr;   
            $vs['cmdedit']    = '<button type="button" onClick="bos.mstmesinabsensi.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Koreksi</button>' ;
            $vs['cmdedit']	   = html_entity_decode($vs['cmdedit']) ;

            $vs['cmddelete']  = '<button type="button" onClick="bos.mstmesinabsensi.cmddelete(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Hapus</button>' ;
            $vs['cmddelete']  = html_entity_decode($vs['cmddelete']) ;

            $vare[]		= $vs ; 
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "ssmstmesinabsensi_id", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $id 	= getsession($this, "ssmstmesinabsensi_id") ; 

        $this->bdb->saving($va, $id) ;
        echo(' bos.mstmesinabsensi.settab(0) ;  ') ;
    }

    public function deleting(){
        $va 	= $this->input->post() ; 
        $kode 	= $va['kode'] ;
        $this->bdb->delete("absensi_mesin", "kode = " . $this->bdb->escape($kode)) ;
        echo(' bos.mstmesinabsensi.grid1_reload() ; ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $kode 	= $va['kode'] ;
        $data = $this->mstmesinabsensi_m->getdata($kode) ;
        if( $dbr = $this->mstmesinabsensi_m->getrow($data) ){
            savesession($this, "ssmstmesinabsensi_id", $dbr['kode']) ;
            $cabang[] = array("id"=>$dbr['cabang'],"text"=>$dbr['ketcabang']);

            echo('
            with(bos.mstmesinabsensi.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$dbr['kode'].'") ;
               find("#keterangan").val("'.$dbr['keterangan'].'").focus() ;
               find("#snmesin").val("'.$dbr['snmesin'].'") ;
               find("#ipmesin").val("'.$dbr['ipmesin'].'") ;
               find("#portmesin").val("'.$dbr['portmesin'].'") ;
               find("#macaddrmesin").val("'.$dbr['macaddrmesin'].'") ;
               find("#cabang").sval('.json_encode($cabang).') ;
            }
            bos.mstmesinabsensi.setopt("jenis","'.$dbr['jenis'].'");

            bos.mstmesinabsensi.settab(1) ;
         ') ;
        }
    }

    public function seekcabang(){
        $search     = $this->input->get('q');
        $vdb    = $this->mstmesinabsensi_m->seekcabang($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->mstmesinabsensi_m->getrow($dbd) ){
          $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
     }

}
?>
