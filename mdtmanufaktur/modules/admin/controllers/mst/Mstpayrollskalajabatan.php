<?php
class Mstpayrollskalajabatan extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("mst/mstpayrollskalajabatan_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->bdb 	= $this->mstpayrollskalajabatan_m ;
    }

    public function index(){
        $arrkolom = array();
        $arrkolom[] = array("field"=> "kode", "caption"=> "Kode", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "jabatan", "caption"=> "Jabatan", "size"=> "150px", "sortable"=>false,"frozen"=>true);
        
        $dbd = $this->bdb->select("payroll_komponen","*","status='1' and perhitungan ='J'");
        while( $dbr = $this->bdb->getrow($dbd) ){
            $arrkolom[] = array("field"=> $dbr['kode'], "caption"=> $dbr['keterangan']."/ ".getperiode($dbr['periode']), "size"=> "150px", "render"=>"float:2","sortable"=>false,
                        "editable"=>array("type"=>"int"));
        }

        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $data['kolomgrid2'] = json_encode($arrkolom);
        $this->load->view("mst/mstpayrollskalajabatan",$data) ;

    } 

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->bdb->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vs = $dbr;   
            $vs['cmddetail']    = '<button type="button" onClick="bos.mstpayrollskalajabatan.cmddetail(\''.$dbr['kode'].'\')"
                           class="btn btn-primary btn-grid"><i class="fa fa-bars"></i></button>' ;
            $vs['cmddetail']	   = html_entity_decode($vs['cmddetail']) ;

            $vare[]		= $vs ; 
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function loadgrid2(){
        $va = $this->input->post();
        $vdb = $this->mstpayrollskalajabatan_m->loadjabatan($va);
        $dbd = $vdb['db'];

        $periode = date("Ym",strtotime($va['tgl']));

        $periode2 = $this->perhitunganhrd_m->getperiode($periode);
        $tglawalperiode = date_2s($periode2['tglawal']);
        $tglakhir = date_2s($periode2['tglakhir']);

        $vare = array();
        $n = 0 ;
        $arrn = array();
        while ($dbr = $this->mstpayrollskalajabatan_m->getrow($dbd)) {
            $n++;
            $data = array();
            $data['recid']  = $n;
            $data['kode']   = $dbr['kode'];
            $data['jabatan']=$dbr['keterangan'];

            

            $dbd2 = $this->bdb->select("payroll_komponen","*","status='1' and perhitungan ='J'");
            while( $dbr2 = $this->bdb->getrow($dbd2) ){
                if($dbr2['resetperiode'] <> '1'){
                    $tglawal = "0000-00-00";
                }else{
                    $tglawal = $tglawalperiode;
                }

                $nominal = $this->perhitunganhrd_m->getnilaigajiskalajabatan($dbr2['kode'],$dbr['kode'],$tglawal,$tglakhir);
                $data[$dbr2['kode']]=$nominal;
            }
            
            $vare[] = $data;
        }
        $selectpjct = implode(",",$arrn);
        $vare = json_encode($vare);

        echo ('
                w2ui["bos-form-mstpayrollskalajabatan_grid2"].add(' . $vare . ');
                w2ui["bos-form-mstpayrollskalajabatan_grid2"].select('.$selectpjct.');
              ');

    }

    public function init(){
        savesession($this, "ssmstpayrollskalajabatan_kode", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $error = $this->bdb->saving($va) ;
        if($error == ""){
            echo(' bos.mstpayrollskalajabatan.grid2_loaddata() ;  ') ;
        }else{
            echo('
                alert("'.$error.'");
                bos.mstpayrollskalajabatan.grid2_loaddata() ;  
                ') ;
        }
        
    }

    public function nonaktifkan(){
        $va 	= $this->input->post() ; 
        $kode 	= $va['kode'] ;
        $this->bdb->edit("payroll_komponen",array("status"=>"2"), "kode = " . $this->bdb->escape($kode)) ;
        echo(' bos.mstpayrollskalajabatan.grid1_reload() ; ') ;
    }

    public function aktifkan(){
        $va 	= $this->input->post() ; 
        $kode 	= $va['kode'] ;
        $this->bdb->edit("payroll_komponen",array("status"=>"1"), "kode = " . $this->bdb->escape($kode)) ;
        echo(' bos.mstpayrollskalajabatan.grid1_reload() ; ') ;
    }

    public function detail(){
        $va 	= $this->input->post() ;
        echo('w2ui["bos-form-mstpayrollskalajabatan_grid3"].clear();');
        $vdb = $this->mstpayrollskalajabatan_m->getdata($va['payroll']) ;
        if( $dbr = $this->mstpayrollskalajabatan_m->getrow($vdb) ){
            echo('
                  with(bos.mstpayrollskalajabatan.obj){
                     find("#payroll").val("'.$dbr['keterangan'].'") ;
                  }
              ') ;
            
            $vare = array();
            $n = 0 ;
            $dbd2 = $this->mstpayrollskalajabatan_m->getdataskala($va['payroll']);
            while( $dbr2 = $this->mstpayrollskalajabatan_m->getrow($dbd2) ){
                $n++;
                $vare[] = array("recid"=>$n,"jabatan"=>$dbr2['jabatan'],"tgl"=>date_2d($dbr2['tgl']),"nominal"=>$dbr2['nominal']);
            }

            $vare = json_encode($vare);
            echo('
              bos.mstpayrollskalajabatan.loadmodalpreview("show") ;
              w2ui["bos-form-mstpayrollskalajabatan_grid3"].add('.$vare.');
            ');
        }
    }

}
?>
