<?php
class Mstdati1 extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->model("mst/mstdati1_m") ;
        $this->bdb 	= $this->mstdati1_m ;
    }

    public function index(){
        $this->load->view("mst/mstdati1") ;

    } 

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->bdb->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vs = $dbr;   
            $vs['cmdedit']    = '<button type="button" onClick="bos.mstdati1.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Koreksi</button>' ;
            $vs['cmdedit']	   = html_entity_decode($vs['cmdedit']) ;

            $vs['cmddelete']  = '<button type="button" onClick="bos.mstdati1.cmddelete(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Hapus</button>' ;
            $vs['cmddelete']  = html_entity_decode($vs['cmddelete']) ;

            $vare[]		= $vs ; 
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "ssmstdati1_id", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $id 	= getsession($this, "ssmstdati1_id") ; 

        $this->bdb->saving($va, $id) ;
        echo(' bos.mstdati1.settab(0) ;  ') ;
    }

    public function deleting(){
        $va 	= $this->input->post() ; 
        $kode 	= $va['kode'] ;
        $this->bdb->delete("dati_1", "kode = " . $this->bdb->escape($kode)) ;
        echo(' bos.mstdati1.grid1_reload() ; ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $kode 	= $va['kode'] ;
        $data = $this->mstdati1_m->getdata($kode) ;
        if( $dbr = $this->mstdati1_m->getrow($data) ){
            savesession($this, "ssmstdati1_id", $dbr['kode']) ;

            echo('
            with(bos.mstdati1.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$dbr['kode'].'") ;
               find("#keterangan").val("'.$dbr['keterangan'].'").focus() ;

            }
            bos.mstdati1.settab(1) ;
         ') ;
        }
    }

    public function getkode(){
        $n  = $this->bdb->getkode(FALSE) ;
  
        echo($n) ;
     }
  

}
?>
