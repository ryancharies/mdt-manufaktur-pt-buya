<?php
class Mstuserinfofinger extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('mst/mstuserinfofinger_m') ;
        $this->load->model('func/updtransaksi_m') ;
        $this->load->model('func/func_m') ;
        $this->load->model('func/perhitunganhrd_m') ;
        $this->load->library('escpos');
        $this->load->helper('bdate');
        $this->bdb = $this->mstuserinfofinger_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(),"mintgl"=>$tglmin) ;
        $this->load->view('mst/mstuserinfofinger',$d) ;
    }

    public function loadgrid(){
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare               = array() ;
        $vdb                = $this->mstuserinfofinger_m->loadgrid($va) ;
        $dbd                = $vdb['db'] ;
        $n = 0 ;
        while( $dbr = $this->mstuserinfofinger_m->getrow($dbd) ){
            $n++;
            $vare[] = array("recid"=>$n,"pin"=>$dbr['pin'],"nama"=>$dbr['name'],"password"=>$dbr['password'],"groupfp"=>$dbr['groupfp'],
                        "privilege"=>$dbr['privilege'],"card"=>$dbr['card'],"pin2"=>$dbr['pin2'],"tz1"=>$dbr['tz1'],"tz2"=>$dbr['tz2'],
                        "tz3"=>$dbr['tz3'],"mesin"=>$dbr['ketmesin']);
        }
        $vare 	= array("total"=>count($vare), "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function loadgrid2(){
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare               = array() ;
        $vdb                = $this->mstuserinfofinger_m->loadgrid2($va) ;
        $dbd                = $vdb['db'] ;
        while( $dbr = $this->mstuserinfofinger_m->getrow($dbd) ){
            $vare[] = array("recid"=>$dbr['kode'],"kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan']);
        }
        $vare 	= array("total"=>count($vare), "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function proses(){
        $va     = $this->input->post() ;
        $error      = $this->mstuserinfofinger_m->proses($va) ;
        if($error == "ok"){
            echo(' 
                alert("Permintaan sedang diproses... ");
                ') ;
        }else{
            echo('
                alert("'.$error.'");
                ');
        }
    }

    

    public function seekmesin(){
        $search     = $this->input->get('q');
        $vdb    = $this->mstuserinfofinger_m->seekmesin($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->mstuserinfofinger_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }



    
}
?>
