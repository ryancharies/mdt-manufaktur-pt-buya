<?php
class Mstmaterialproduk extends Bismillah_Controller{
   private $bdb ;
   private $bdbgroupstock ;
   public function __construct(){
		parent::__construct() ;
      $this->load->model('mst/mstmaterialproduk_m') ;
      $this->load->helper('bdate');
      $this->load->library('m_pdf');
      $this->bdb = $this->mstmaterialproduk_m ;
      $this->ss  = getsession($this, "username")."-" . "ssmaterialproduk" ;

	}

   public function index(){
      $this->load->view('mst/mstmaterialproduk') ;
   }

   public function loadgrid(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
      $vdb    = $this->mstmaterialproduk_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->mstmaterialproduk_m->getrow($dbd) ){
         //$dbr['kode'] = $dbr['kode']."<br/>". $dbr['keterangan'];
         $vaset   = $dbr ;
		   $vaset['cmdedit']    = '<button type="button" onClick="bos.mstmaterialproduk.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Edit</button>' ;
         
         $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;

         $vaset['cmddetail']    = '<button type="button" onClick="bos.mstmaterialproduk.cmddetail(\''.$dbr['kode'].'\')"
                           class="btn btn-primary btn-grid">Detail</button>' ;
         
         $vaset['cmddetail']	   = html_entity_decode($vaset['cmddetail']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function init(){
      savesession($this, "ssstock_kode", "") ;
   }

   public function getkode(){
      $n  = $this->bdb->getkode(FALSE) ;

      echo('
        bos.mstmaterialproduk.obj.find("#kode").val("'.$n.'") ;
        ') ;
   }

   public function saving(){
      $va 	  = $this->input->post() ;
      $kode 	= getsession($this, "ssstock_kode") ;
      $this->mstmaterialproduk_m->saving($kode, $va) ;
      echo(' bos.mstmaterialproduk.settab(0) ;  ') ;
   }

   public function editing(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->mstmaterialproduk_m->getdata($kode) ;
      if( $dbr = $this->mstmaterialproduk_m->getrow($data) ){
         savesession($this, "ssstock_kode", $kode) ;
         $satuan[] = array("id"=>$dbr['satuan'],"text"=>$dbr['KetSatuan']);
         $group[]  = array("id"=>$dbr['stock_group'],"text"=>$dbr['KetStockGroup']);
         echo('
            with(bos.mstmaterialproduk.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$dbr['kode'].'") ;
               find("#barcode").val("'.$dbr['barcode'].'") ;
               find("#hargajual").val("'.$dbr['hargajual'].'") ;
               find("#bbb").val("'.$dbr['bbb'].'") ;
               find("#btkl").val("'.$dbr['btkl'].'") ;
			      find("#bop").val("'.$dbr['bop'].'") ;
               find("#group").sval('.json_encode($group).') ;
               find("#satuan").sval('.json_encode($satuan).') ;
               find("#keterangan").val("'.$dbr['keterangan'].'").focus() ;

            }
            bos.mstmaterialproduk.setopt("tampil","'.$dbr['tampil'].'");
            bos.mstmaterialproduk.settab(1) ;
            bos.mstmaterialproduk.hitunghpp();
            
         ') ;
      }
   }
    
    public function loadgrid2(){
      $va     = json_decode($this->input->post('request'), true) ;
      $kode  = $va['kode'];
      $vare   = array() ;
      $vdb    = $this->mstmaterialproduk_m->loadgrid2($kode) ;
      $dbd    = $vdb['db'] ;
      $n      = 0;
      while( $dbr = $this->mstmaterialproduk_m->getrow($dbd) ){
          $n++;
          $vapaket = $this->mstmaterialproduk_m->checkkomponen($dbr['kode'],$kode);
          $ck = $vapaket['status'];
          $vaset   = array("recid"=>$n,"ck"=>$ck,"no"=>$n,"kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan'],
                           "qty"=>$vapaket['qty'],"hp"=>$vapaket['hp'],"satuan"=>$dbr['satuan']) ;
          $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;

   }

   public function loaddetail(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      echo('w2ui["bos-form-mstmaterialproduk_grid3"].clear();');
      $data = $this->mstmaterialproduk_m->getdata($kode) ;
      if( $dbr = $this->mstmaterialproduk_m->getrow($data) ){
         
         $hpp = $dbr['bbb'] + $dbr['btkl']+ $dbr['bop'];
         echo('
            with(bos.mstmaterialproduk.obj){
               find("#d-stock").val("'.$dbr['kode'].'") ;
               find("#d-bbb").val("'.string_2s($dbr['bbb']).'") ;
               find("#d-btkl").val("'.string_2s($dbr['btkl']).'") ;
               find("#d-bop").val("'.string_2s($dbr['bop']).'") ;
               find("#d-hpp").val("'.string_2s($hpp).'") ;
               find("#d-satuan").val("'.$dbr['satuan'].'") ;
               find("#d-namastock").val("'.$dbr['keterangan'].'") ;

            }            
         ') ;

            //loadgrid detail
            $vare = array();
            $dbd2    = $this->mstmaterialproduk_m->loadmaterial($kode) ;
            $n      = 0;
            while( $dbr2 = $this->mstmaterialproduk_m->getrow($dbd2) ){
                $n++;
                $vaset   = $dbr2 ;
                $vaset['recid'] = $n;
                $vaset['no'] = $n;
                $vare[]             = $vaset ;
            }
            $vare = json_encode($vare);
            echo('
                w2ui["bos-form-mstmaterialproduk_grid3"].add('.$vare.');
                bos.mstmaterialproduk.loadmodeldetail("show");
            ');
        }
   }

   public function initreportdetail(){
      $arr = array();
      $va     = $this->input->post() ;
      $file   = setfile($this, "rpt", __FILE__ , $va) ;
      savesession($this, $this->ss . "file", $file ) ;
      savesession($this, $this->ss . "va", json_encode($va) ) ;

      file_put_contents($file, json_encode(array()) ) ;
      $file    = getsession($this, $this->ss . "file") ;
      $data    = @file_get_contents($file) ;
      $data    = json_decode($data,true) ;
      if(trim($va['d-stock']) <> ""){         
         //cek po kartu
         $dbd    = $this->mstmaterialproduk_m->loadmaterial($va['d-stock']) ;
         $n = 0 ;
         while( $dbr = $this->mstmaterialproduk_m->getrow($dbd) ){
             $n++;
             $vaset   = array("no"=>$n,"kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan'],"satuan"=>$dbr['satuan'],"qty"=>string_2s($dbr['qty']),"hp"=>string_2s($dbr['hp']));//,"keterangan"=>$dbr['keterangan'],"satuan"=>$dbr['satuan'],"qty"=>string_2s($dbr['qty']),"hp"=>string_2s($dbr['hp'])
             $data[$n]     = $vaset ;          
         }

         file_put_contents($file, json_encode($data) ) ;
         echo(' bos.mstmaterialproduk.openreportdetil() ; ') ;
      }else{
         echo('alert("Faktur tidak boleh kosong");');
      }
      
  }

   public function showreportdetil(){
      $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
      $file = getsession($this, $this->ss . "file") ;
      $data = @file_get_contents($file) ;
      $data = json_decode($data,true) ;
      // echo print_r($data);

      if(!empty($data)){
   
         $font = 8 ;

         $vhead = array() ;
         $vhead[] = array("1"=>"Produk","2"=>":","3"=>$va['d-stock'],"4"=>"Satuan","5"=>":","6"=>$va['d-satuan']) ;
         $vhead[] = array("1"=>"Nama Produk","2"=>":","3"=>$va['d-namastock'],"4"=>"","5"=>"","6"=>"") ;

         $vfoot = array() ;
         $vfoot[] = array("1"=>"BBB","2"=>":","3"=>$va['d-bbb'],"4"=>"BTKL","5"=>":","6"=>$va['d-btkl']) ;
         $vfoot[] = array("1"=>"BOP","2"=>":","3"=>$va['d-bop'],"4"=>"HPP","5"=>":","6"=>$va['d-hpp']) ;
		 
		   $vtitle = array() ;
         $vtitle[] = array("capt"=>" DETIL MATERIAL PRODUK ") ;


         $o    = array('paper'=>'A4', 'orientation'=>'P', 'export'=>'0',
                        'opt'=>array('export_name'=>'Detil Material Produk') ) ;
         $this->load->library('bospdf', $o) ;
		   
         //ketika width tidak di set maka akan menyesuaikan dengan lebar kertas
         $this->bospdf->ezTable($vtitle,"","",
               array("fontSize"=>$font+3,"showHeadings"=>0,"showLines"=>0,
                     "cols"=> array(
                        "capt"=>array("justification"=>"center"),)
               )
            ) ;

         $this->bospdf->ezTable($vhead,"","",
                                    array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                          "cols"=> array(
                                             "1"=>array("width"=>10,"justification"=>"left"),
                                             "2"=>array("width"=>3,"justification"=>"left"),
                                             "3"=>array("justification"=>"left"),
                                             "4"=>array("width"=>10,"justification"=>"left"),
                                             "5"=>array("width"=>3,"justification"=>"left"),
                                             "6"=>array("justification"=>"left"),)
                                    )
                                 ) ;

         
		   $this->bospdf->ezText("") ;
         //print_r($data);
         $this->bospdf->ezTable($data,"","",
                                 array("fontSize"=>$font,
                                       "cols"=> array(
                                             "no"            =>array("width"=>5,"justification"=>"right","caption"=>"No"),
                                             "kode"  		=>array("width"=>10,"justification"=>"center","caption"=>"Kode"),
                                             "keterangan"   =>array("justification"=>"left","caption"=>"Keterangan"),
                                             "satuan"       =>array("width"=>8,"justification"=>"center","caption"=>"Satuan"),
                                             "qty"          =>array("width"=>8,"justification"=>"right","caption"=>"Qty"),
                                             "hp"       =>array("width"=>8,"justification"=>"right","caption"=>"HP")))
                                 ) ;


         $this->bospdf->ezText("") ;
         $this->bospdf->ezTable($vfoot,"","",
                                    array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                          "cols"=> array(
                                             "1"=>array("width"=>10,"justification"=>"left"),
                                             "2"=>array("width"=>3,"justification"=>"left"),
                                             "3"=>array("justification"=>"left"),
                                             "4"=>array("width"=>10,"justification"=>"left"),
                                             "5"=>array("width"=>3,"justification"=>"left"),
                                             "6"=>array("justification"=>"left"))
                                    )
                                 ) ;
         $this->bospdf->ezStream() ;
      }else{
         echo('kosong') ;
      }
  }
}
?>
