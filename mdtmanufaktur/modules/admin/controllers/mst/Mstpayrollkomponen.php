<?php
class Mstpayrollkomponen extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("mst/mstpayrollkomponen_m") ;
        $this->bdb 	= $this->mstpayrollkomponen_m ;
    }

    public function index(){
        $arrrekening = array();
        $dbd = $this->mstpayrollkomponen_m->select("keuangan_rekening","kode,keterangan","jenis = 'D'");
        while($dbr = $this->mstpayrollkomponen_m->getrow($dbd)){
            $arrrekening[] = array("id"=>$dbr['kode'],"text"=>$dbr['keterangan'] . " | " . $dbr['kode']);
        }
        $data['listrekening'] = json_encode($arrrekening);
        $this->load->view("mst/mstpayrollkomponen",$data) ;

    } 

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->bdb->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vs = $dbr;   
            $ketpot = "Tidak";
            if($vs['potongan'] == "1")$ketpot = "Ya";
            if($vs['potterlambat'] == "1")$ketpot .= ", Terlambat";
            if($vs['potplgcepat'] == "1")$ketpot .= ", Plg Cepat";

            $perhitunganpph21 = "Tidak";
            if($vs['perhitunganpph21'] == "1")$perhitunganpph21 = "Ya";

            $resetperiode = "Tidak";
            if($vs['resetperiode'] == "1")$resetperiode = "Ya";

            $vs['potongan'] = $ketpot;
            $vs['perhitunganpph21'] = $perhitunganpph21;
            $vs['resetperiode'] = $resetperiode;
            $vs['recid'] = $vs['kode'];

            $vs['cmdedit']    = '<button type="button" onClick="bos.mstpayrollkomponen.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Koreksi</button>' ;
            $vs['cmdedit']	   = html_entity_decode($vs['cmdedit']) ;

            $vs['cmddelete']  = '<button type="button" onClick="bos.mstpayrollkomponen.cmdnonaktifkan(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Non Aktifkan</button>' ;
            if($dbr['status'] == "2")$vs['cmddelete']  = '<button type="button" onClick="bos.mstpayrollkomponen.cmdaktifkan(\''.$dbr['kode'].'\')"
            class="btn btn-success btn-grid">Aktifkan</button>' ;
            $vs['cmddelete']  = html_entity_decode($vs['cmddelete']) ;

            $vare[]		= $vs ; 
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function loadgrid2(){
        $va = $this->input->post();
        $vdb = $this->mstpayrollkomponen_m->loadabsensikode($va);
        $dbd = $vdb['db'];
        $vare = array();
        $n = 0 ;
        $arrn = array();
        while ($dbr = $this->mstpayrollkomponen_m->getrow($dbd)) {
            $n++;
            $vare[] = array("recid"=>$n,"kode" => $dbr['kode'],"keterangan" => $dbr['keterangan'],
                "jenis" => $dbr['jenis']);
            if($dbr['absensi'] <> "")$arrn[] = $n;
        }
        $selectpjct = implode(",",$arrn);
        $vare = json_encode($vare);

        echo ('
                w2ui["bos-form-mstpayrollkomponen_grid2"].add(' . $vare . ');
                w2ui["bos-form-mstpayrollkomponen_grid2"].select('.$selectpjct.');
              ');

    }

    public function loadgrid3(){
        $va = $this->input->post();
        $vdb = $this->mstpayrollkomponen_m->loadrekeningbagian($va);
        $dbd = $vdb['db'];
        $vare = array();
        $n = 0 ;
        $arrn = array();
        while ($dbr = $this->mstpayrollkomponen_m->getrow($dbd)) {
            $n++;
            $vare[] = array("recid"=>$n,"kode" => $dbr['kode'],"keterangan" => $dbr['keterangan'],
                "rekening" => $dbr['ketrekening']." | ".$dbr['rekening']);
        }
        $vare = json_encode($vare);

        echo ('
                w2ui["bos-form-mstpayrollkomponen_grid3"].add(' . $vare . ');
              ');

    }

    public function init(){
        savesession($this, "ssmstpayrollkomponen_kode", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $id 	= getsession($this, "ssmstpayrollkomponen_kode") ; 
        $error = $this->bdb->saving($va, $id) ;
        if($error == ""){
            echo(' bos.mstpayrollkomponen.settab(0) ;  ') ;
        }else{
            echo('alert("'.$error.'");');
        }
        
    }

    public function nonaktifkan(){
        $va 	= $this->input->post() ; 
        $kode 	= $va['kode'] ;
        $this->bdb->edit("payroll_komponen",array("status"=>"2"), "kode = " . $this->bdb->escape($kode)) ;
        echo(' bos.mstpayrollkomponen.grid1_reload() ; ') ;
    }

    public function aktifkan(){
        $va 	= $this->input->post() ; 
        $kode 	= $va['kode'] ;
        $this->bdb->edit("payroll_komponen",array("status"=>"1"), "kode = " . $this->bdb->escape($kode)) ;
        echo(' bos.mstpayrollkomponen.grid1_reload() ; ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $kode 	= $va['kode'] ;
        $data = $this->mstpayrollkomponen_m->getdata($kode) ;
        if( $dbr = $this->mstpayrollkomponen_m->getrow($data) ){
            savesession($this, "ssmstpayrollkomponen_kode", $dbr['kode']) ;
            $periode[] = array("id"=>$dbr['periode'],"text"=>getperiode($dbr['periode']));
            $potongan = false;
            if($dbr['potongan'] == "1")$potongan = true;
            $potterlambat = false;
            if($dbr['potterlambat'] == "1")$potterlambat = true;
            $potplgcepat = false;
            if($dbr['potplgcepat'] == "1")$potplgcepat = true;
            $diluarjadwal = false;
            if($dbr['diluarjadwal'] == "1")$diluarjadwal = true;
            $perhitunganpph21 = false;
            if($dbr['perhitunganpph21'] == "1")$perhitunganpph21 = true;
            $resetperiode = false;
            if($dbr['resetperiode'] == "1")$resetperiode = true;
            
            echo('
            with(bos.mstpayrollkomponen.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$dbr['kode'].'") ;
               find("#keterangan").val("'.$dbr['keterangan'].'").focus() ;
               find("#stepskala").val("'.$dbr['stepskala'].'") ;
               find("#periodemulai").val("'.$dbr['periodemulai'].'") ;
               find("#periode").sval('.json_encode($periode).');
               find("#potongan").prop("checked",'.$potongan.');
               find("#potterlambat").prop("checked",'.$potterlambat.');
               find("#potplgcepat").prop("checked",'.$potplgcepat.');
               find("#diluarjadwal").prop("checked",'.$diluarjadwal.');
               find("#perhitunganpph21").prop("checked",'.$perhitunganpph21.');
               find("#resetperiode").prop("checked",'.$resetperiode.');
            }
            bos.mstpayrollkomponen.setopt("perhitungan","'.$dbr['perhitungan'].'");
            bos.mstpayrollkomponen.settab(1) ;
         ') ;
        }
    }
    public function seekrekening(){
        $search     = $this->input->get('q');
        $vdb    = $this->mstpayrollkomponen_m->seekrekening($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->mstpayrollkomponen_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] . " - " .$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

}
?>
