<?php
class Mstasuransi extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->model("mst/mstasuransi_m") ;
        $this->bdb 	= $this->mstasuransi_m ;
    }

    public function index(){
        $this->load->view("mst/mstasuransi") ;

    } 

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->bdb->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vs = $dbr;   
            $vs['cmdedit']    = '<button type="button" onClick="bos.mstasuransi.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Koreksi</button>' ;
            $vs['cmdedit']	   = html_entity_decode($vs['cmdedit']) ;

            $vs['cmddelete']  = '<button type="button" onClick="bos.mstasuransi.cmddelete(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Hapus</button>' ;
            $vs['cmddelete']  = html_entity_decode($vs['cmddelete']) ;

            $vare[]		= $vs ; 
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "ssmstasuransi_id", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $id 	= getsession($this, "ssmstasuransi_id") ; 

        $this->bdb->saving($va, $id) ;
        echo(' bos.mstasuransi.settab(0) ;  ') ;
    }

    public function deleting(){
        $va 	= $this->input->post() ; 
        $kode 	= $va['kode'] ;
        $this->bdb->delete("asuransi", "kode = " . $this->bdb->escape($kode)) ;
        echo(' bos.mstasuransi.grid1_reload() ; ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $kode 	= $va['kode'] ;
        $data = $this->mstasuransi_m->getdata($kode) ;
        if( $dbr = $this->mstasuransi_m->getrow($data) ){
            savesession($this, "ssmstasuransi_id", $dbr['kode']) ;
            $payroll[] = array("id"=>$dbr['payroll'],"text"=>$dbr['payroll'] ." payroll ". $dbr['ketpayroll']);


            echo('
            with(bos.mstasuransi.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$dbr['kode'].'") ;
               find("#payroll").sval('.json_encode($payroll).') ;
               find("#keterangan").val("'.$dbr['keterangan'].'").focus() ;

            }
            bos.mstasuransi.settab(1) ;
         ') ;
        }
    }

    public function seekpayroll(){
        $search     = $this->input->get('q');
        $vdb    = $this->mstasuransi_m->seekpayroll($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->mstasuransi_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ".$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }
}
?>
