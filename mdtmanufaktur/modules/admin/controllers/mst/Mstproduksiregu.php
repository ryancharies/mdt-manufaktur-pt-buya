<?php
class Mstproduksiregu extends Bismillah_Controller
{
    protected $bdb;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("bdate");
        $this->load->model("mst/mstproduksiregu_m");
        $this->load->model("func/func_m");
        $this->load->model("func/perhitunganhrd_m");
        $this->bdb = $this->mstproduksiregu_m;
    }

    public function index()
    {
        // mengambil min tgl transaksi yang aktif
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data = array();
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $this->load->view("mst/mstproduksiregu",$data);

    }

    public function loadgrid_where($bs, $s){
        $this->duser();

        if($bs['skd_cabang'] !== 'null'){
            $bs['skd_cabang'] = json_decode($bs['skd_cabang'],true);
            $this->db->group_start();
            foreach($bs['skd_cabang'] as $kdc){
                $this->db->or_where('r.cabang',$kdc);
            }
            $this->db->group_end();

        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['cabang'])){
                    $this->db->group_start();
                    foreach($this->aruser['data_var']['cabang'] as $kdc){
                        $this->db->or_where('r.cabang', $kdc);
                    }
                    $this->db->group_end();
                }
            }
        }

        if($s !== ''){
            $this->db->group_start();
                $this->db->or_like(array('r.kode'=>$s, 'r.keterangan'=>$s, 'k.nama'=>$s));
            $this->db->group_end();
        }

        $this->db->or_where('r.status',"1");
    }

    public function loadgrid()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va = json_decode($this->input->post('request'), true);
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->db->escape_like_str($search) ;
        $vare = array();

        $this->loadgrid_where($va, $search);
        $f = "count(r.id) jml";
        $dbdt = $this->db->select($f)
            ->from("produksi_regu r")
            ->join("karyawan k","k.kode = r.karu","left")
            ->get();
        $rtot  = $dbdt->row_array();
        if($rtot['jml'] > 0){

            $this->loadgrid_where($va, $search);
            $f2 = "r.kode,r.keterangan,k.nama as karu,r.cabang";
            $dbd = $this->db->select($f2)
                ->from("produksi_regu r")
                ->join("karyawan k","k.kode = r.karu","left")
                ->order_by('kode ASC')
                ->limit($limit)
                ->get();
        
            foreach($dbd->result_array() as $dbr){              

                
                $vs = $dbr;

                $vs['cmdedit'] = '<button type="button" onClick="bos.mstproduksiregu.cmdedit(\'' . $dbr['kode'] . '\')"
                            class="btn btn-default btn-grid">Koreksi</button>';
                $vs['cmdedit'] = html_entity_decode($vs['cmdedit']);

                $vs['cmddelete'] = '<button type="button" onClick="bos.mstproduksiregu.cmddelete(\'' . $dbr['kode'] . '\')"
                            class="btn btn-danger btn-grid">Hapus</button>';
                $vs['cmddelete'] = html_entity_decode($vs['cmddelete']);

                $vare[] = $vs;
            }

        }

        
        $vare = array("total" => $rtot['jml'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function init()
    {
        savesession($this, "ssmstproduksiregu_id", "");
    }

    public function saving()
    {
        $va = $this->input->post();
        $kode = getsession($this, "ssmstproduksiregu_id", "");
        $va['kode'] = $kode !== "" ? $kode : $this->bdb->getkode($va['cabang'],true) ;

        $grid2 = json_decode($va['grid2'],true);
        $anggota = array();
        foreach($grid2 as $key => $val) {
            $anggota[] = $val['nip'];
        }

        $data    = array("kode"=>$va['kode'],"keterangan"=>$va['keterangan'],"cabang"=>$va['cabang'],"karu"=>$va['karu'],"anggota"=>json_encode($anggota),"status"=>'1') ;
        $where   = "kode = " . $this->bdb->escape($va['kode']) ;
        $this->bdb->update("produksi_regu", $data, $where, "") ;
        echo ('ok');
    }

    public function deleting()
    {
        $va = $this->input->post();
        $this->bdb->update("produksi_regu", array("status"=>"2"), "kode = '{$va['kode']}'") ;
        echo("ok");
    }

    public function editing()
    {
        $va = $this->input->post();
        $tgl = date("Y-m-d");
        savesession($this, "ssmstproduksiregu_id", $va['kode']);

        $this->db->where('r.kode',$va['kode']);

        $f = "r.kode,r.keterangan,r.karu,k.nama namakaru,r.cabang,r.anggota,c.keterangan as ketcabang";
        $dbdt = $this->db->select($f)
            ->from("produksi_regu r")
            ->join("karyawan k","k.kode = r.karu","left")
            ->join("cabang c","c.kode = r.cabang","left")
            ->get();
        $r  = $dbdt->row_array();
        
        $vanggota = array();
        $r['anggota'] = json_decode($r['anggota'],true);
        foreach($r['anggota'] as $key){
            $dkry = $this->perhitunganhrd_m->getdatakaryawan($key,$tgl);
            $vanggota[$key] = array("nama"=>$dkry['nama'],"jabatan"=>$dkry['ketjabatan']);
        }
        $r['anggota'] = $vanggota;
        echo(json_encode($r));
    }

    public function seekanggota(){
        $va 	= $this->input->post() ;
        $tgl = date("Y-m-d");
        
        $this->db->where('kode',$va['anggota']);

        $f = "kode,nama";
        $dbdt = $this->db->select($f)
            ->from("karyawan")
            ->get();
        $r  = $dbdt->row_array();
        $jab = $this->perhitunganhrd_m->getjabatan($r['kode'],$tgl);
        $r['jabatan'] = $jab['keterangan'];
        echo(json_encode($r));
        
    }

    public function getkode()
    {
        $va 	= $this->input->post() ;
        $kode = "";
        if($va['cabang'] !== 'null')$kode = $this->bdb->getkode($va['cabang'],false);
        echo($kode);
    }
}
