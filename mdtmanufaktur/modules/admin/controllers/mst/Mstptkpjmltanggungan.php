<?php
class Mstptkpjmltanggungan extends Bismillah_Controller{
   public function __construct(){
		parent::__construct() ;
      $this->load->model('mst/mstptkpjmltanggungan_m') ;
      $this->load->helper('bdate') ;
      $this->load->helper('bdir') ;
	}

   public function index(){
      $tglmin = $this->mstptkpjmltanggungan_m->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
      $data = array();
      $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
      $this->load->view('mst/mstptkpjmltanggungan',$data) ;
   }

   public function loadgrid(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
      $vdb    = $this->mstptkpjmltanggungan_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->mstptkpjmltanggungan_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['cmdedit']    = '<button type="button" onClick="bos.mstptkpjmltanggungan.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-success btn-grid">Jml Tanggungan</button>' ;
         $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function loadgrid2(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
     // print_r($va);
      $vdb    = $this->mstptkpjmltanggungan_m->loadgrid2($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->mstptkpjmltanggungan_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['tgl'] = date_2d($vaset['tgl']);
         $vaset['cmddelete']    = '<button type="button" onClick="bos.mstptkpjmltanggungan.cmddeletejt(\''.$dbr['kode'].'\',\''.$dbr['tgl'].'\')"
                           class="btn btn-danger btn-grid">Delete</button>' ;
         $vaset['cmddelete']	   = html_entity_decode($vaset['cmddelete']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function init(){
      savesession($this, "ssjabatan_kode", "") ;
   }

   public function saving(){
      $va 	= $this->input->post() ;
      $return = $this->mstptkpjmltanggungan_m->saving($va) ;
      if($return == "ok"){
         echo('
            alert("Data telah disimpan!!");
            bos.mstptkpjmltanggungan.grid2_reloaddata() ;
            with(bos.mstptkpjmltanggungan.obj){
               find("#jmltanggungan").val("0") ;
            }
         ');
      }else{
         echo('
            alert("'.$return.'");
         ');
      }
      
   }

   public function editing(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->mstptkpjmltanggungan_m->getdata($kode) ;
      if(!empty($data)){
         echo('
            with(bos.mstptkpjmltanggungan.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$data['kode'].'") ;
               find("#keterangan").val("'.$data['keterangan'].'") ;
            }
            bos.mstptkpjmltanggungan.settab(1) ;

         ');

      }
   }

   public function deleting(){
      $va 	= $this->input->post() ;
      $error = $this->mstptkpjmltanggungan_m->deleting($va) ;
      if($error == "ok"){
         echo(' bos.mstptkpjmltanggungan.grid2_reloaddata() ; ') ;
      }else{
         echo('alert("'.$error.'");');
      }
   }
}
?>
