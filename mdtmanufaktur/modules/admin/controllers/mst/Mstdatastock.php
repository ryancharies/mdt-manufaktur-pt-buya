<?php
class Mstdatastock extends Bismillah_Controller{
   private $bdb ;
   private $bdbgroupstock ;
   public function __construct(){
		parent::__construct() ;
      $this->load->model('mst/mstdatastock_m') ;
      $this->load->helper('bdate');
      $this->bdb = $this->mstdatastock_m ;
	}

   public function index(){
      $this->load->view('mst/mstdatastock') ;
   }

   public function loadgrid(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
      $vdb    = $this->mstdatastock_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->mstdatastock_m->getrow($dbd) ){
         //$dbr['kode'] = $dbr['kode']."<br/>". $dbr['keterangan'];
         $vaset   = $dbr ;
         $vaset['cmdedit']    = '<button type="button" onClick="bos.mstdatastock.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Edit</button>' ;
         $vaset['cmddelete']  = '<button type="button" onClick="bos.mstdatastock.cmddelete(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Delete</button>' ;
         $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;
         $vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function init(){
      savesession($this, "ssstock_kode", "") ;
   }

   public function getkode(){
      $n  = $this->bdb->getkode(FALSE) ;

      echo('
        bos.mstdatastock.obj.find("#kode").val("'.$n.'") ;
        ') ;
   }

   public function saving(){
      $va 	  = $this->input->post() ;
      $kode 	= getsession($this, "ssstock_kode") ;
      $this->mstdatastock_m->saving($kode, $va) ;
      echo(' bos.mstdatastock.settab(0) ;  ') ;
   }

   public function editing(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->mstdatastock_m->getdata($kode) ;
      if( $dbr = $this->mstdatastock_m->getrow($data) ){
         savesession($this, "ssstock_kode", $kode) ;
         $satuan[] = array("id"=>$dbr['satuan'],"text"=>$dbr['KetSatuan']);
         $group[]  = array("id"=>$dbr['stock_group'],"text"=>$dbr['KetStockGroup']);
         $kelompok[]  = array("id"=>$dbr['stock_kelompok'],"text"=>$dbr['ketkelompok']);

         echo('
            with(bos.mstdatastock.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$dbr['kode'].'") ;
               find("#barcode").val("'.$dbr['barcode'].'") ;
               find("#hargajual").val("'.$dbr['hargajual'].'") ;
               find("#group").sval('.json_encode($group).') ;
               find("#satuan").sval('.json_encode($satuan).') ;
               find("#kelompok").sval('.json_encode($kelompok).') ;
               find("#keterangan").val("'.$dbr['keterangan'].'").focus() ;

            }
            bos.mstdatastock.setopt("tampil","'.$dbr['tampil'].'");

            bos.mstdatastock.settab(1) ;
         ') ;
      }
   }

   public function deleting(){
      $va 	= $this->input->post() ;
      $error = $this->mstdatastock_m->deleting($va['kode']) ;
      if($error == "ok"){
         echo(' bos.mstdatastock.grid1_reloaddata() ; ') ;
      }else{
         echo('alert("'.$error.'");');
      }
   }
}
?>
