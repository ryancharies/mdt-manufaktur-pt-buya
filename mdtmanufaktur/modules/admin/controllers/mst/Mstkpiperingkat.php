<?php
class Mstkpiperingkat extends Bismillah_Controller{
   private $bdb ;
   private $bdbgroupstock ;
   public function __construct(){
		parent::__construct() ;
      $this->load->model('mst/mstkpiperingkat_m') ;
      $this->load->helper('bdate');
      $this->bdb = $this->mstkpiperingkat_m ;
	}

   public function index(){
      $this->load->view('mst/mstkpiperingkat') ;
   }

   public function loadgrid_where($bs, $s){
      $this->duser();
      
      if($bs['skd_jabatan'] !== 'null'){
          $bs['skd_jabatan'] = json_decode($bs['skd_jabatan'],true);
          $this->db->group_start();
          foreach($bs['skd_jabatan'] as $kdc){
              $this->db->or_where('p.jabatan',$kdc);
          }
          $this->db->group_end();

      }

      if($s !== ''){
          $this->db->group_start();
              $this->db->or_like(array('p.kode'=>$s,'p.keterangan'=>$s));
          $this->db->group_end();
      }

      $this->db->where('p.status',"1");

  }

   public function loadgrid(){
      $va = json_decode($this->input->post('request'), true);
      $limit    = $va['offset'].",".$va['limit'] ;
      $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
      $search   = $this->db->escape_like_str($search) ;
      $vare = array();
      $this->loadgrid_where($va, $search);
      $f = "count(p.id) jml";
      $dbdt = $this->db->select($f)
          ->from("kpi_peringkat p")
          ->join("jabatan j","j.kode = p.jabatan","left")
          ->get();
      $rtot  = $dbdt->row_array();
      if($rtot['jml'] > 0){

         $this->loadgrid_where($va, $search);
         $f2 = "p.kode,p.keterangan,p.keterangan,p.nilai_min,p.nilai_max,j.keterangan as jabatan,p.peringkat,p.nominal";
         $dbd = $this->db->select($f2)
               ->from("kpi_peringkat p")
               ->join("jabatan j","j.kode = p.jabatan","left")
               ->limit($limit)
               ->get();
      
         foreach($dbd->result_array() as $dbr){ 
            //$dbr['kode'] = $dbr['kode']."<br/>". $dbr['keterangan'];
            $vaset   = $dbr ;
            $vaset['recid']   = $dbr['kode'] ;
            $vaset['cmdedit']    = '<button type="button" onClick="bos.mstkpiperingkat.cmdedit(\''.$dbr['kode'].'\')"
                              class="btn btn-default btn-grid">Edit</button>' ;
            $vaset['cmddelete']  = '<button type="button" onClick="bos.mstkpiperingkat.cmddelete(\''.$dbr['kode'].'\')"
                              class="btn btn-danger btn-grid">Delete</button>' ;
            $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;
            $vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

            $vare[]		= $vaset ;
         }
      }    

      $vare 	= array("total"=>$rtot['jml'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function init(){
      savesession($this, "sskpiper_kode", "") ;
   }

   public function getkode(){
      $n  = $this->bdb->getkode(FALSE) ;

      echo('
        bos.mstkpiperingkat.obj.find("#kode").val("'.$n.'") ;
        ') ;
   }

   public function saving(){
      $va 	  = $this->input->post() ;
      $kode 	= getsession($this, "sskpiper_kode") ;
      $this->mstkpiperingkat_m->saving($kode, $va) ;
      echo(' bos.mstkpiperingkat.settab(0) ;  ') ;
   }

   public function editing(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->mstkpiperingkat_m->getdata($kode) ;
      if( $dbr = $this->mstkpiperingkat_m->getrow($data) ){
         savesession($this, "sskpiper_kode", $kode) ;
         $jabatan[] = array("id"=>$dbr['jabatan'],"text"=>$this->bdb->getval("keterangan", "kode = '{$dbr['jabatan']}'", "jabatan"));

         echo('
            with(bos.mstkpiperingkat.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$dbr['kode'].'") ;
               find("#peringkat").val("'.$dbr['peringkat'].'") ;
               find("#jabatan").sval('.json_encode($jabatan).') ;
               find("#nilai_min").val("'.$dbr['nilai_min'].'") ;
               find("#nilai_max").val("'.$dbr['nilai_max'].'") ;
               find("#nominal").val("'.$dbr['nominal'].'") ;
               find("#keterangan").val("'.$dbr['keterangan'].'").focus() ;

            }

            bos.mstkpiperingkat.settab(1) ;
         ') ;
      }
   }

   public function deleting(){
      $va 	= $this->input->post() ;
      $error = $this->mstkpiperingkat_m->deleting($va['kode']) ;
      if($error == "ok"){
         echo(' bos.mstkpiperingkat.grid1_reloaddata() ; ') ;
      }else{
         echo('alert("'.$error.'");');
      }
   }
}
?>
