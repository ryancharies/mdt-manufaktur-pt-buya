<?php
class Mstkpiindikatorbobot extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("mst/mstkpiindikatorbobot_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->bdb 	= $this->mstkpiindikatorbobot_m ;
    }

    public function index(){
        $arrkolom = array();
        $arrkolom[] = array("field"=> "lama", "caption"=> "Lama (Thn)", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        
        $dbd = $this->bdb->select("payroll_komponen","*","status='1' and periode ='B'");
        while( $dbr = $this->bdb->getrow($dbd) ){
            $arrkolom[] = array("field"=> $dbr['kode'], "caption"=> $dbr['keterangan']." (%)", "size"=> "150px", "render"=>"float:2","sortable"=>false,
                        "editable"=>array("type"=>"int"));
        }

        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $data['kolomgrid1'] = json_encode($arrkolom);
        $this->load->view("mst/mstkpiindikatorbobot",$data) ;

    } 

   
    public function loadgrid1(){
        $va = $this->input->post();
        $vare = array();
        $n = 0 ;
        
        $error = "";
        if(!isset($va['bagian']))$error .= "Bagian masih belum dipilih!! \\n";
        if(!isset($va['jabatan']))$error .= "Jabatan masih belum dipilih!! \\n";

        if($error == ""){
            $dbd2 = $this->bdb->select("kpi_indikator","*","status='1'");
            while( $dbr2 = $this->bdb->getrow($dbd2) ){
                $n++;
                //$nominal = $this->perhitunganhrd_m->getprosentasegajiskalathr($dbr2['kode'],$i,$va['tgl']);
                $bobot = $this->perhitunganhrd_m->getbobotkpiindikator($dbr2['kode'],$va['jabatan'],$va['bagian'],$va['tgl']);
                $data = array("recid"=>$n,"kode"=>$dbr2['kode'],"keterangan"=>$dbr2['keterangan'],"satuan"=>$dbr2['satuan'],"bobot"=>$bobot);
                $vare[]=$data;
            }
            
            $vare = json_encode($vare);

            echo ('
                    w2ui["bos-form-mstkpiindikatorbobot_grid1"].add(' . $vare . ');
                ');
        }else{
            echo('alert("'.$error.'");');
        }
    }

    public function init(){
        savesession($this, "ssmstkpiindikatorbobot_kode", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $error = $this->bdb->saving($va) ;
        if($error == ""){
            echo(' bos.mstkpiindikatorbobot.grid1_loaddata() ;  ') ;
        }else{
            echo('
                alert("'.$error.'");
                bos.mstkpiindikatorbobot.grid1_loaddata() ;  
                ') ;
        }
        
    }

    public function seekjabatan(){
        $search     = $this->input->get('q');
        $vdb    = $this->mstkpiindikatorbobot_m->seekjabatan($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->mstkpiindikatorbobot_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

    public function seekbagian(){
        $search     = $this->input->get('q');
        $vdb    = $this->mstkpiindikatorbobot_m->seekbagian($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->mstkpiindikatorbobot_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }
}
?>
