<?php
class Mstpayrollskalathr extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("mst/mstpayrollskalathr_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->bdb 	= $this->mstpayrollskalathr_m ;
    }

    public function index(){
        $arrkolom = array();
        $arrkolom[] = array("field"=> "lama", "caption"=> "Lama (Thn)", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        
        $dbd = $this->bdb->select("payroll_komponen","*","status='1' and periode ='B'");
        while( $dbr = $this->bdb->getrow($dbd) ){
            $arrkolom[] = array("field"=> $dbr['kode'], "caption"=> $dbr['keterangan']." (%)", "size"=> "150px", "render"=>"float:2","sortable"=>false,
                        "editable"=>array("type"=>"int"));
        }

        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $data['kolomgrid1'] = json_encode($arrkolom);
        $this->load->view("mst/mstpayrollskalathr",$data) ;

    } 

   
    public function loadgrid1(){
        $va = $this->input->post();
        $vare = array();
        $n = 0 ;
        $arrn = array();
        for($i = 0 ;$i <= 40 ; $i++){
            $n++;
            $data = array();
            $data['recid']  = $n;
            $data['lama']  = $i;

            $dbd2 = $this->bdb->select("payroll_komponen","*","status='1' and periode ='B'");
            while( $dbr2 = $this->bdb->getrow($dbd2) ){
                $nominal = $this->perhitunganhrd_m->getprosentasegajiskalathr($dbr2['kode'],$i,$va['tgl']);
                $data[$dbr2['kode']]=$nominal;
            }
            
            $vare[] = $data;
        }
        $vare = json_encode($vare);

        echo ('
                w2ui["bos-form-mstpayrollskalathr_grid1"].add(' . $vare . ');
              ');

    }

    public function init(){
        savesession($this, "ssmstpayrollskalathr_kode", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $error = $this->bdb->saving($va) ;
        if($error == ""){
            echo(' bos.mstpayrollskalathr.grid1_loaddata() ;  ') ;
        }else{
            echo('
                alert("'.$error.'");
                bos.mstpayrollskalathr.grid1_loaddata() ;  
                ') ;
        }
        
    }

}
?>
