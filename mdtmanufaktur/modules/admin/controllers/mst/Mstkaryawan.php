<?php
class Mstkaryawan extends Bismillah_Controller{
   public function __construct(){
		parent::__construct() ;
      $this->load->model('mst/mstkaryawan_m') ;
      $this->load->helper('bdate') ;
      $this->load->helper('bdir') ;
	}

   public function index(){
      $tglmin = $this->mstkaryawan_m->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
      $data = array();
      $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
      $this->load->view('mst/mstkaryawan',$data) ;
   }

   public function loadgrid(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
      $vdb    = $this->mstkaryawan_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->mstkaryawan_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['recid'] = $vaset['kode'];
         $vaset['tgl'] = date_2d($vaset['tgl']);
         $vaset['tgllahir'] = date_2d($vaset['tgllahir']);
         $vaset['cmdedit']    = '<button type="button" onClick="bos.mstkaryawan.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Edit</button>' ;
         $vaset['cmddelete']  = '<button type="button" onClick="bos.mstkaryawan.cmddelete(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Delete</button>' ;
         $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;
         $vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function init(){
      savesession($this, "sskaryawan_kode", "") ;
   }

   public function saving(){
      $va 	= $this->input->post() ;
      $kode 	= getsession($this, "sskaryawan_kode") ;
      $this->mstkaryawan_m->saving($kode, $va) ;
      echo(' bos.mstkaryawan.settab(0) ;  ') ;
   }

   public function editing(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->mstkaryawan_m->getdata($kode) ;
      if(!empty($data)){
         savesession($this, "sskaryawan_kode", $kode) ;
         $bank[] = array("id"=>$data['bank'],"text"=>$data['bank'] ." - ". $this->mstkaryawan_m->getval("keterangan", "kode = '{$data['bank']}'", "bank"));
         $pendidikan_tingkat[] = array("id"=>$data['pendidikan_tingkat'],"text"=>$this->mstkaryawan_m->getval("keterangan", "kode = '{$data['bank']}'", "pendidikan_tingkat"));
         echo('
            with(bos.mstkaryawan.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$data['kode'].'") ;
               find("#nama").val("'.$data['nama'].'").focus() ;
               find("#ktp").val("'.$data['ktp'].'") ;
               find("#notelepon").val("'.$data['telepon'].'") ;
               find("#tgl").val("'.date_2s($data['tgl']).'") ;
               find("#tgllahir").val("'.date_2s($data['tgllahir']).'") ;
               find("#alamat").val("'.$data['alamat'].'") ;
               find("#rekening").val("'.$data['rekening'].'") ;
               find("#anrekening").val("'.$data['anrekening'].'") ;
               find("#bank").sval('.json_encode($bank).') ;
               find("#pendidikan_tingkat").sval('.json_encode($pendidikan_tingkat).') ;
            }
            
         ') ;

         //load foto
         $img = "";
         $kodefoto = "ftkaryawan~".$kode;
         $dbdf = $this->mstkaryawan_m->getfoto($kodefoto) ;
         while( $dbrf = $this->mstkaryawan_m->getrow($dbdf) ){
             $urlfile = $dbrf['urlfile'];
             $checked = "";
             $id = md5($urlfile);
             if($dbrf['fotoutama'] == "1")$checked = "checked";
             $img .= '<div id=\"idfotokaryawan_'.$id .'\" class=\'col-sm-4 \' style=\"margin:15px auto;border:1px black;height:100px;\"><input type=\'checkbox\' id=\'ckfotoutama\' value=\''.$urlfile.'\' '.$checked.'><label for=\'vehicle1\'> Foto Utama</label>&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick = bos.mstkaryawan.deletefoto(\"'.$urlfile.'\"); ><i class=\'fa fa-eraser\'></i> Delete</a><br>';
             $img .= '<img src=\"'.base_url($urlfile . "?time=". time()).'\" class=\"img-responsive\" style=\"margin:0 auto;height:100px\"/>';
             $img .= '</div>';
         }

         echo('
             bos.mstkaryawan.obj.find("#fotobrowse").append("'.$img.'") ;
             bos.mstkaryawan.settab(1) ;
             ');

      }
   }

   public function deleting(){
      $va 	= $this->input->post() ;
      $error = $this->mstkaryawan_m->deleting($va['kode']) ;
      if($error == "ok"){
         echo(' bos.mstkaryawan.grid1_reloaddata() ; ') ;
      }else{
         echo('alert("'.$error.'");');
      }
   }
   
   public function getkode(){
      $n  = $this->mstkaryawan_m->getkode(FALSE) ;

      echo('
        bos.mstkaryawan.obj.find("#kode").val("'.$n.'") ;
        ') ;
   }

   public function saving_image($cfg){
      $username = getsession($this, "username");
      //$file = $username .time();
      $file = md5(rand(0,10000) . time() . session_id());
      $fcfg	= array("upload_path"=>"./tmp/", "allowed_types"=>"jpg|jpeg|png", "overwrite"=>true,
                  "file_name"=> $file ) ;
      $this->load->library('upload', $fcfg) ;

      if ( ! $this->upload->do_upload(0) ){
         echo('
            alert("'. $this->upload->display_errors('','') .'") ;
            bos.mstkaryawan.obj.find("#idl'.$cfg.'").html("") ;
         ') ;
      }else{
         $data 	= $this->upload->data() ;
         $fname 	= $file . $data['file_ext'] ;
         $tname 	= str_replace($data['file_ext'], "", $data['client_name']) ;
         $vimage	= array( $tname => $data['full_path']) ;
         // echo('
         // 	bos.mstdatastock.obj.find("#idl'.$cfg.'2").html("") ;
         // 	bos.mstdatastock.obj.find("#id'.$cfg.'2").html("<img src=\"'.base_url("./tmp/" . $fname . "?time=". time()).'\" class=\"img-responsive\" style=\"margin:0 auto;max-height:200px\"/>") ;
            // ') ;
            $urlfile = "./tmp/" . $fname;
            $id = md5($urlfile);
            $img = '<div id=\"idfotokaryawan_'.$id .'\" class=\'col-sm-4 \' style=\"margin:15px auto;border:1px black;height:100px;\"><input type=\'checkbox\' id=\'ckfotoutama\' value=\''.$urlfile.'\'><label for=\'vehicle1\'> Foto Utama</label>&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick = bos.mstkaryawan.deletefoto(\"'.$urlfile.'\");><i class=\'fa fa-eraser\'></i> Delete</a><br>';
            $img .= '<img src=\"'.base_url($urlfile . "?time=". time()).'\" class=\"img-responsive\" style=\"margin:0 auto;height:100px\"/>';
            $img .= '</div>';
            echo('
            bos.mstkaryawan.obj.find("#idl'.$cfg.'").html("") ;
            bos.mstkaryawan.obj.find("#fotobrowse").append("'.$img.'") ;
         ') ;
      }
  }
  
  public function deletefoto(){
      $va 	= $this->input->post() ;
      if(is_file($va['file']))unlink($va['file']) ;
      $this->mstkaryawan_m->delete("sys_upload","urlfile = '{$va['file']}'") ;
      $id = md5($va['file']);
      echo('
          bos.mstkaryawan.obj.find( "#idfotokaryawan_'.$id.'").remove(); 
          alert("Foto berhasil dihapus...");
      ');
  }
}
?>
