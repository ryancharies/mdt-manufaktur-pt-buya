<?php
class Msthjjenis extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->model("mst/msthjjenis_m") ;
        $this->bdb 	= $this->msthjjenis_m ;
    }

    public function index(){
        $this->load->view("mst/msthjjenis") ;

    } 

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->bdb->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->bdb->getrow($dbd) ){
            $vs = $dbr;   
            $vs['cmdedit']    = '<button type="button" onClick="bos.msthjjenis.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-default btn-grid">Koreksi</button>' ;
            $vs['cmdedit']	   = html_entity_decode($vs['cmdedit']) ;

            $vs['cmddelete']  = '<button type="button" onClick="bos.msthjjenis.cmddelete(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Hapus</button>' ;
            $vs['cmddelete']  = html_entity_decode($vs['cmddelete']) ;

            $vare[]		= $vs ; 
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function loadgrid2(){
        $va = $this->input->post();
        $dbd = $this->msthjjenis_m->loadstockhj($va);
        $vare = array();
        $n = 0 ;
        $arrn = array();
        while ($dbr = $this->msthjjenis_m->getrow($dbd)) {
            $n++;
            $data = array();
            $data['recid']  = $n;
            $data['kode']   = $dbr['kode'];
            $data['keterangan']=$dbr['keterangan'];
            $data['satuan']=$dbr['satuan'];
            $data['hj']=$dbr['hj'];

            
            
            $vare[] = $data;
        }
        $vare = json_encode($vare);

        echo ('
                w2ui["bos-form-msthjjenis_grid2"].add(' . $vare . ');
              ');

    }

    public function init(){
        savesession($this, "ssmsthjjenis_id", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $id 	= getsession($this, "ssmsthjjenis_id") ; 

        $this->bdb->saving($va, $id) ;
        echo(' bos.msthjjenis.settab(0) ;  ') ;
    }

    public function deleting(){
        $va 	= $this->input->post() ; 
        $kode 	= $va['kode'] ;
        $this->bdb->delete("hj_jenis", "kode = " . $this->bdb->escape($kode)) ;
        echo(' bos.msthjjenis.grid1_reload() ; ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $kode 	= $va['kode'] ;
        $data = $this->msthjjenis_m->getdata($kode) ;
        if( $dbr = $this->msthjjenis_m->getrow($data) ){
            savesession($this, "ssmsthjjenis_id", $dbr['kode']) ;

            echo('
            with(bos.msthjjenis.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$dbr['kode'].'") ;
               find("#keterangan").val("'.$dbr['keterangan'].'").focus() ;

            }
            bos.msthjjenis.settab(1) ;
         ') ;
        }
    }

}
?>
