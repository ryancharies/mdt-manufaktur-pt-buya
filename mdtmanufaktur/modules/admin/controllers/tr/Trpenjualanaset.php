<?php
class Trpenjualanaset extends Bismillah_Controller
{
    protected $bdb;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("bdate");
        $this->load->model("tr/trpenjualanaset_m");
        $this->load->model("func/updtransaksi_m");
        $this->bdb = $this->trpenjualanaset_m;
        $this->ss  = getsession($this, "username")."-" . "ssrptpenjualanaset_" ;
    }

    public function index()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $d['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $this->load->view("tr/trpenjualanaset",$d);

    }

    public function loadgrid()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va = json_decode($this->input->post('request'), true);
        $vare = array();
        $vdb = $this->bdb->loadgrid($va);
        $dbd = $vdb['db'];
        while ($dbr = $this->bdb->getrow($dbd)) {
            $dbr['tglperolehan'] = date_2d($dbr['tglperolehan']);
            $vs = $dbr;
            $vs['cmdreport'] = "";
            if ($va['status'] == "1") {
                $vs['cmdact'] = '<button type="button" onClick="bos.trpenjualanaset.cmdjual(\'' . $dbr['kode'] . '\')"
                            class="btn btn-warning btn-grid">Jual Aset</button>';
            } else {
                $vs['cmdact'] = '<button type="button" onClick="bos.trpenjualanaset.cmdbataljual(\'' . $dbr['kode'] . '\',\'' . $dbr['fakturpj'] . '\')"
                            class="btn btn-warning btn-grid">Batal Jual</button>';
                $vs['cmdreport'] = '<button type="button" onClick="bos.trpenjualanaset.cmdreport(\'' . $dbr['kode'] . '\')"
                        class="btn btn-danger btn-grid">Cetak</button>';
                if($tglmin > date_2s($dbr['tglpj'])){
                    $vs['cmdact'] = "";
                }
            }
            $vs['cmdact'] = html_entity_decode($vs['cmdact']);
            $vs['cmdreport'] = html_entity_decode($vs['cmdreport']);

            $vare[] = $vs;
        }
        //print_r($va);

        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function init()
    {
        savesession($this, "sstrpenjualanaset_id", "");
    }

    public function saving()
    {
        $va = $this->input->post();
        $id = getsession($this, "sstrpenjualanaset_id");

        $this->bdb->saving($va, $id);
        echo (' bos.trpenjualanaset.settab(0) ;  ');
    }

    public function deleting()
    {
        $va = $this->input->post();
       // print_r($va);
        $faktur = $va['faktur'];
        
        $error = "Gagal hapus data, Data tidak valid!!";
        if ($faktur != "") {
            $error = "ok";
            $dbd = $this->bdb->select("piutang_kartu", "id","faktur <> '$faktur' and fkt = '$faktur'");
            if ($dbr = $this->bdb->getrow($dbd)) {
                $error = "Data tidak bisa dihapus karena sudah digunakan dalam pelunasan!!";
            } else {
                $this->bdb->delete("keuangan_bukubesar", "faktur = " . $this->bdb->escape($faktur));
                $this->bdb->delete("piutang_kartu", "faktur = " . $this->bdb->escape($faktur));

                $data    = array("fakturpj"=>"","customer"=>"","tglpj"=>"0000-00-00","tglhabis"=>"0000-00-00",
                        "hargajual"=>0,"nilaibukujual"=>0,"usernamejual"=> "","datetimejual"=>"0000-00-00 00:00:00") ;
                $where   = "fakturpj = '$faktur'";
                $this->bdb->edit("aset", $data, $where, "") ;
            }

            if($error == "ok"){
                echo (' bos.trpenjualanaset.grid1_reload() ; ');
            }else{
                echo('alert("'.$error.'");');
            }
        } else {
            echo ('
                    alert("' . $error . '");
                ');
        }
        
    }

    public function jual()
    {
        $va = $this->input->post();
        $kode = $va['kode'];
        $data = $this->trpenjualanaset_m->getdata($kode);
        if ($dbr = $this->trpenjualanaset_m->getrow($data)) {
            savesession($this, "sstrpenjualanaset_id", $dbr['kode']);
            $customer[] = array("id" => $dbr['customer'], "text" => $dbr['namacustomer']);
            $dbr['tglperolehan'] = date_2d($dbr['tglperolehan']);
            if ($dbr['fakturpj'] == "") {
                $dbr['fakturpj'] = $this->bdb->getfaktur(false);
            }
            if($dbr['tglpj'] == "0000-00-00")$dbr['tglpj'] = date("Y-m-d");
            $tgl = date("Y-m-d", nextmonth(strtotime($dbr['tglpj']), -1));
            $arraset = $this->perhitungan_m->getpenyusutan($dbr['kode'],$tgl);
            if($dbr['nilaibukujual'] == 0)$dbr['nilaibukujual'] = $arraset['hargaperolehan'] - $arraset['akhir'];
            echo ('
            with(bos.trpenjualanaset.obj){
                find(".nav-tabs li:eq(1) a").tab("show") ;
                find("#kode").val("' . $dbr['kode'] . '") ;
                find("#fktpb").val("' . $dbr['faktur'] . '") ;
                find("#golaset").val("' . $dbr['ketgolongan'] . '") ;
                find("#cabang").val("' . $dbr['ketcabang'] . '") ;
                find("#vendor").val("' . $dbr['ketvendor'] . '") ;
                find("#tglperolehan").val("' . date_2d($dbr['tglperolehan']) . '") ;
                find("#hp").val("' . string_2s($dbr['hargaperolehan']) . '") ;
                find("#unit").val("' . $dbr['unit'] . '") ;
                find("#lama").val("' . $dbr['lama'] . '") ;

                find("#tarifpeny").val("' . $dbr['tarifpenyusutan'] . '") ;
                find("#residu").val("' . string_2s($dbr['residu']) . '") ;
                find("#keterangan").val("' . $dbr['keterangan'] . '").focus() ;

                find("#fktpj").val("' . $dbr['fakturpj'] . '") ;
                find("#tglpj").val("' . date_2d($dbr['tglpj']) . '") ;
                find("#customer").sval(' . json_encode($customer) . ') ;
                find("#nilaibuku").val("' . string_2s($dbr['nilaibukujual']) . '") ;
                find("#hargajual").val("' . string_2s($dbr['hargajual']) . '") ;

            }
            bos.trpenjualanaset.setopt("jenis","' . $dbr['jenispenyusutan'] . '");
            bos.trpenjualanaset.settab(1) ;
         ');
        }
    }

    public function seekaset()
    {
        $va = $this->input->post();
        //mengambil periode sebelumnya
        $tgl = date("Y-m-d", nextmonth(strtotime($va['tglpj']), -1));
        $arraset = $this->perhitungan_m->getpenyusutan($va['kode'], $tgl);
        $nilaibuku = $arraset['hargaperolehan'] - $arraset['akhir'];
        echo ('
            with(bos.trpenjualanaset.obj){
                find("#nilaibuku").val("' . string_2s($nilaibuku) . '") ;
            }
        ');
    }

    public function seekcustomer()
    {
        $search = $this->input->get('q');
        $vdb = $this->trpenjualanaset_m->seekcustomer($search);
        $dbd = $vdb['db'];
        $vare = array();
        while ($dbr = $this->trpenjualanaset_m->getrow($dbd)) {
            $vare[] = array("id" => $dbr['kode'], "text" => $dbr['nama']);
        }
        $Result = json_encode($vare);
        echo ($Result);
    }

    public function cetak()
    {
        $va = $this->input->post();
        $file       = setfile($this, "rpt_penjualanaset", __FILE__ , $va) ;
        savesession($this, $this->ss . "file", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        file_put_contents($file, json_encode(array()) ) ;

        $file    = getsession($this, $this->ss . "file") ;
        $arr    = @file_get_contents($file);
        $arr    = json_decode($arr,true);

        $va = $this->input->post();
        $kode = $va['kode'];
        $dbd      = $this->trpenjualanaset_m->getdata($kode) ;
        if($dbr = $this->trpenjualanaset_m->getrow($dbd)){
            $arr[] = array("ket"=>"Tgl","tt2"=>":","uraian"=>date_2d($dbr['tglpj']));
            $arr[] = array("ket"=>"Kode","tt2"=>":","uraian"=>$dbr['kode']);
            $arr[] = array("ket"=>"Aset","tt2"=>":","uraian"=>$dbr['keterangan']);
            $arr[] = array("ket"=>"Harga","tt2"=>":","uraian"=>string_2s($dbr['hargajual']));
            $arr[] = array("ket"=>"Pembeli","tt2"=>":","uraian"=>$dbr['namacustomer']);
            
        }

        file_put_contents($file, json_encode($arr) ) ;
        echo(' bos.trpenjualanaset.openreport() ; ') ;
    }

    public function showreport(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $file = getsession($this, $this->ss . "file") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        if(!empty($data)){
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->trpenjualanaset_m->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->trpenjualanaset_m->getconfig("ttd"), true) ;
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> $kota ,"5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"Pembeli,","3"=>"","4"=>"Bag. Keuangan,","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"(.........................)","3"=>"","4"=>"(.........................)","5"=>"") ;
            
            $font = 10;

            $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                          'opt'=>array('export_name'=>'Kartu Stock') ) ;
                          $this->load->library('bospdf', $o) ;
            
            $this->bospdf->ezText("<b>PENJUALAN ASET</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($data,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "ket"=>array("width"=>15,"justification"=>"left"),
                                             "tt2"=>array("width"=>3,"justification"=>"left"),
                                             "uraian"=>array("justification"=>"left"))
                                        )
                                  ) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vttd,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "1"=>array("justification"=>"right"),
                                             "2"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                             "3"=>array("width"=>40,"wrap"=>1),
                                             "4"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                             "5"=>array("wrap"=>1,"justification"=>"center"))
                                        )
                                  ) ;
            $this->bospdf->ezStream() ;
        }else{
            echo('kosong') ;
        }
    }

}
