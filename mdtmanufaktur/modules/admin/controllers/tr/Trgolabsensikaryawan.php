<?php
class Trgolabsensikaryawan extends Bismillah_Controller{
   public function __construct(){
		parent::__construct() ;
      $this->load->model('tr/trgolabsensikaryawan_m') ;
      $this->load->model('func/perhitunganhrd_m') ;
      $this->load->helper('bdate') ;
      $this->load->helper('bdir') ;
	}

   public function index(){
      $tglmin = $this->trgolabsensikaryawan_m->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
      $data = array();
      $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
      $this->load->view('tr/trgolabsensikaryawan',$data) ;
   }

   public function loadgrid(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
      $vdb    = $this->trgolabsensikaryawan_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      $tgl = date("Y-m-d");
      while( $dbr = $this->trgolabsensikaryawan_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['tgl'] = date_2d($vaset['tgl']);
         $golabsensi = $this->perhitunganhrd_m->getgolabsensi($dbr['kode'],$tgl);
         $vaset['golabsensi'] = $golabsensi['keterangan'];
         $vaset['cmdedit']    = '<button type="button" onClick="bos.trgolabsensikaryawan.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-success btn-grid">Gol Absensi</button>' ;
         $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function loadgrid2(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
     // print_r($va);
      $vdb    = $this->trgolabsensikaryawan_m->loadgrid2($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->trgolabsensikaryawan_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['tgl'] = date_2d($vaset['tgl']);
         $vaset['cmddelete']    = '<button type="button" onClick="bos.trgolabsensikaryawan.cmddeletemj(\''.$dbr['nip'].'\',\''.$dbr['tgl'].'\')"
                           class="btn btn-danger btn-grid">Delete</button>' ;
         $vaset['cmddelete']	   = html_entity_decode($vaset['cmddelete']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function init(){
      savesession($this, "ssgolongan_kode", "") ;
   }

   public function saving(){
      $va 	= $this->input->post() ;
      $return = $this->trgolabsensikaryawan_m->saving($va) ;
      if($return == "ok"){
         echo('
            alert("Data telah disimpan!!");
            bos.trgolabsensikaryawan.grid2_reloaddata() ;
         ');
      }else{
         echo('
            alert("'.$return.'");
         ');
      }
      
   }

   public function editing(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->trgolabsensikaryawan_m->getdata($kode) ;
      if(!empty($data)){
         echo('
            with(bos.trgolabsensikaryawan.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$data['kode'].'") ;
               find("#nama").val("'.$data['nama'].'") ;
               find("#ktp").val("'.$data['ktp'].'") ;
               find("#notelepon").val("'.$data['telepon'].'") ;
               find("#tglmasuk").val("'.date_2s($data['tgl']).'") ;
               find("#alamat").val("'.$data['alamat'].'") ;
            }
            bos.trgolabsensikaryawan.settab(1) ;

         ');

      }
   }

   public function deleting(){
      $va 	= $this->input->post() ;
      $error = $this->trgolabsensikaryawan_m->deleting($va) ;
      if($error == "ok"){
         echo(' bos.trgolabsensikaryawan.grid2_reloaddata() ; ') ;
      }else{
         echo('alert("'.$error.'");');
      }
   }
    
   public function seekgolongan(){
        $search     = $this->input->get('q');
        $vdb    = $this->trgolabsensikaryawan_m->seekgolongan($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trgolabsensikaryawan_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ". $dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
   }
   
}
?>
