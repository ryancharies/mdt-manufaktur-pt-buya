<?php
class Trabsensikaryawan extends Bismillah_Controller{
   public function __construct(){
		parent::__construct() ;
      $this->load->model('tr/trabsensikaryawan_m') ;
      $this->load->model('func/perhitunganhrd_m') ;
      $this->load->helper('bdate') ;
      $this->load->helper('bdir') ;
	}

   public function index(){
      $tglmin = $this->trabsensikaryawan_m->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
      $data = array();
      $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
      $this->load->view('tr/trabsensikaryawan',$data) ;
   }

   public function loadgrid(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
      $vdb    = $this->trabsensikaryawan_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      $tgl = date("Y-m-d");
      
      while( $dbr = $this->trabsensikaryawan_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['recid'] = $dbr['id'];
         $vaset['tgl'] = date_2d($vaset['tgl']);
         $golabsensi = $this->perhitunganhrd_m->getgolabsensi($dbr['kode'],$tgl);
         $vaset['golabsensi'] = $golabsensi['keterangan'];
         $vaset['cmdedit']    = '<button type="button" onClick="bos.trabsensikaryawan.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-success btn-grid">Absensi</button>' ;
         $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

  

   public function init(){
      savesession($this, "ssgolongan_kode", "") ;
   }

   public function saving(){
      $va 	= $this->input->post() ;
      // print_r($va);
      $return = $this->trabsensikaryawan_m->saving($va) ;
      if($return == "ok"){
         echo('
            alert("Data telah disimpan!!");
            bos.trabsensikaryawan.loadmodelabsensi("hide");
            bos.trabsensikaryawan.grid2_reloaddata() ;
         ');
      }else{
         echo('
            alert("'.$return.'");
         ');
      }
      
   }

   public function editing(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->trabsensikaryawan_m->getdata($kode) ;
      if(!empty($data)){
         echo('
            with(bos.trabsensikaryawan.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$data['kode'].'") ;
               find("#nama").val("'.$data['nama'].'") ;
               find("#ktp").val("'.$data['ktp'].'") ;
               find("#notelepon").val("'.$data['telepon'].'") ;
               find("#tglmasuk").val("'.date_2s($data['tgl']).'") ;
               find("#alamat").val("'.$data['alamat'].'") ;
            }
            bos.trabsensikaryawan.settab(1) ;

         ');

      }
   }
    
   public function seekperiode(){
      $search     = $this->input->get('q');
      $vdb    = $this->trabsensikaryawan_m->seekperiode($search) ;
      $dbd    = $vdb['db'] ;
      $vare   = array();
      while( $dbr = $this->trabsensikaryawan_m->getrow($dbd) ){
          $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ". $dbr['keterangan']) ;
      }
      $Result = json_encode($vare);
          echo($Result) ;
   }

   public function seekkodeabsensim(){
      $search     = $this->input->get('q');
      $vdb    = $this->trabsensikaryawan_m->seekkodeabsensim($search) ;
      $dbd    = $vdb['db'] ;
      $vare   = array();
      while( $dbr = $this->trabsensikaryawan_m->getrow($dbd) ){
          $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      $Result = json_encode($vare);
          echo($Result) ;
   }

   public function seekkodeabsensip(){
      $search     = $this->input->get('q');
      $vdb    = $this->trabsensikaryawan_m->seekkodeabsensip($search) ;
      $dbd    = $vdb['db'] ;
      $vare   = array();
      while( $dbr = $this->trabsensikaryawan_m->getrow($dbd) ){
          $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      $Result = json_encode($vare);
          echo($Result) ;
   }

   public function loadabsensi(){
         $va 	= $this->input->post() ;
         $vdb    = $this->trabsensikaryawan_m->getperiode($va) ;
         if( $dbr = $this->trabsensikaryawan_m->getrow($vdb['db']) ){
            echo('
               with(bos.trabsensikaryawan.obj){
                     find("#tglawal").val("'.date_2d($dbr['tglawal']).'") ;
                     find("#tglakhir").val("'.date_2d($dbr['tglakhir']).'") ;
               }
            ') ;
            
            $array = array();
            $n = 0 ;
            for($tgl = $dbr['tglawal'] ; $tgl <= $dbr['tglakhir'] ; $tgl = date("Y-m-d",strtotime($tgl)+(24*60*60))){
               $n++;
               $jw = $this->perhitunganhrd_m->getjwabsensi($va['nip'],$tgl);
               $hari = date('w', strtotime($tgl));
               $hari = date_day($hari);
               
               $abs = $this->perhitunganhrd_m->getabsensi($va['nip'],$tgl);
               $btn= '<button type="button" onClick="bos.trabsensikaryawan.cmdabsensi(\''.$va['nip'].'\',\''.$tgl.'\')"
                           class="btn btn-warning btn-grid"><i class="fa fa-edit"></i></button>' ;
               $btn	   = html_entity_decode($btn) ;
               $array[] = array("recid"=>$n,"no"=>$n,"hari"=>$hari,"tgl"=>date_2d($tgl),"jadwalmasuk"=>$jw['jwmasuk'],"jadwalpulang"=>$jw['jwpulang'],
                           "toleransi"=>$jw['jwtoleransi'],"absmasuk"=>$abs['wktmasuk'],"abspulang"=>$abs['wktpulang'],"btn"=>$btn,
                           "kodeabsensim"=>$abs['ketkodeabsenmasuk'],"kodeabsensip"=>$abs['ketkodeabsenpulang'],"terlambat"=>$abs['terlambat'],
                           "plgcepat"=>$abs['plgcepat'],
                           "keterangan"=>"","golabsensi"=>$jw['ketgolongan'],"jadwal"=>$jw['ketjadwal']);
               
            }
            $array = json_encode($array);

            echo ('
                w2ui["bos-form-trabsensikaryawan_grid2"].add(' . $array . ');
              ');
         }
   }
   
   public function absensi(){
      $va 	= $this->input->post() ;
      $abs = $this->perhitunganhrd_m->getabsensi($va['nip'],$va['tgl']);
      $kodeabsensim[] = array("id"=>$abs['kodeabsenmasuk'],"text"=>$abs['ketkodeabsenmasuk']);
      $kodeabsensip[] = array("id"=>$abs['kodeabsenpulang'],"text"=>$abs['ketkodeabsenpulang']);
      //print_r($abs);
      echo('
         with(bos.trabsensikaryawan.obj){
            find("#tglabsensi").val("'.date_2d($va['tgl']).'") ;
            find("#masuk").val("'.$abs['wktmasuk'].'") ;
            find("#pulang").val("'.$abs['wktpulang'].'") ;
            find("#kodeabsensim").sval(' . json_encode($kodeabsensim) . ');
            find("#kodeabsensip").sval(' . json_encode($kodeabsensip) . ');
         }
         bos.trabsensikaryawan.loadmodelabsensi("show");
      ');
   }
}
?>
