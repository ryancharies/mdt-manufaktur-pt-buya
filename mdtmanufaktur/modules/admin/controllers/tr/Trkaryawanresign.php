<?php
class Trkaryawanresign extends Bismillah_Controller{
   public function __construct(){
		parent::__construct() ;
      $this->load->model('tr/trkaryawanresign_m') ;
      $this->load->helper('bdate') ;
      $this->load->helper('bdir') ;
	}

   public function index(){
      $tglmin = $this->trkaryawanresign_m->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
      $data = array();
      $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
      $this->load->view('tr/trkaryawanresign',$data) ;
   }

   public function loadgrid(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
      $vdb    = $this->trkaryawanresign_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->trkaryawanresign_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['tgl'] = date_2d($vaset['tgl']);
         $vaset['tglkeluar'] = date_2d($vaset['tglkeluar']);
         $vaset['cmdedit']    = '<button type="button" onClick="bos.trkaryawanresign.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-success btn-grid">Resign</button>' ;
         if($vaset['tglkeluar'] <> "00-00-0000"){
            $vaset['cmdedit']    = '<button type="button" onClick="bos.trkaryawanresign.cmdbatalresign(\''.$dbr['kode'].'\')"
                           class="btn btn-danger btn-grid">Batal Resign</button>' ;
         }
         $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function init(){
      
   }

   public function saving(){
      $va 	= $this->input->post() ;
      $this->trkaryawanresign_m->saving($va) ;
      echo(' bos.trkaryawanresign.settab(0) ;  ') ;
   }
   

   public function batalresign(){
      $va 	= $this->input->post() ;
      $this->trkaryawanresign_m->edit("karyawan",array("tglkeluar"=>"0000-00-00","ketkeluar"=>""),"kode = '{$va['kode']}'");
      echo(' bos.trkaryawanresign.grid1_reloaddata() ;  ') ;
   }
   public function editing(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->trkaryawanresign_m->getdata($kode) ;
      if(!empty($data)){
         savesession($this, "sskaryawan_kode", $kode) ;
          echo('
            with(bos.trkaryawanresign.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$data['kode'].'") ;
               find("#nama").val("'.$data['nama'].'").focus() ;
               find("#ktp").val("'.$data['ktp'].'") ;
               find("#notelepon").val("'.$data['telepon'].'") ;
               find("#tgl").val("'.date_2s($data['tgl']).'") ;
               find("#alamat").val("'.$data['alamat'].'") ;
               find("#rekening").val("'.$data['rekening'].'") ;
               find("#anrekening").val("'.$data['anrekening'].'") ;

               find("#tglkeluar").val("'.date_2s($data['tglkeluar']).'") ;
               find("#ketkeluar").val("'.$data['ketkeluar'].'") ;

            }
            
         ') ;

         //load foto
         $img = "";
         $kodefoto = "ftkaryawan~".$kode;
         $dbdf = $this->trkaryawanresign_m->getfoto($kodefoto) ;
         while( $dbrf = $this->trkaryawanresign_m->getrow($dbdf) ){
             $urlfile = $dbrf['urlfile'];
             $checked = "";
             $id = md5($urlfile);
             if($dbrf['fotoutama'] == "1")$checked = "checked";
             $img .= '<div id=\"idfotokaryawan_'.$id .'\" class=\'col-sm-4 \' style=\"margin:15px auto;border:1px black;height:100px;\"><br>';
             $img .= '<img src=\"'.base_url($urlfile . "?time=". time()).'\" class=\"img-responsive\" style=\"margin:0 auto;height:100px\"/>';
             $img .= '</div>';
         }

         echo('
             bos.trkaryawanresign.obj.find("#fotobrowse").append("'.$img.'") ;
             bos.trkaryawanresign.settab(1) ;
             ');

      }
   }

   
    
}
?>
