<?php
class Trasuransipembayaran extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('tr/trasuransipembayaran_m') ;
        $this->load->model('func/updtransaksi_m') ;
        $this->load->model('func/func_m') ;
        $this->load->model('func/perhitunganhrd_m') ;
        $this->load->library('escpos');
        $this->load->helper('bdate');
        $this->bdb = $this->trasuransipembayaran_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(),"mintgl"=>$tglmin) ;
        $this->load->view('tr/trasuransipembayaran',$d) ;
    }

    public function loadgrid(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare               = array() ;
        $va['tglawal']      = date_2s($va['tglawal']);
        $va['tglakhir']     = date_2s($va['tglakhir']);
        $vdb                = $this->trasuransipembayaran_m->loadgrid($va) ;
        $dbd                = $vdb['db'] ;
        $tgl = date("Y-m-d");
        while( $dbr = $this->trasuransipembayaran_m->getrow($dbd) ){
            $vaset                  = $dbr ;
            $vaset['tgl']           = date_2d($vaset['tgl']);
            $vaset['cmdedit']       = '<button type="button" onClick="bos.trasuransipembayaran.cmdedit(\''.$dbr['faktur'].'\')"
                                        class="btn btn-default btn-grid">Edit</button>' ;
            $vaset['cmddelete']     = '<button type="button" onClick="bos.trasuransipembayaran.cmddelete(\''.$dbr['faktur'].'\')"
                                         class="btn btn-danger btn-grid">Delete</button>' ;
            $vaset['cmdcetak']    = '<button type="button" onClick="bos.trasuransipembayaran.cmdcetak(\''.$dbr['faktur'].'\')"
                                         class="btn btn-warning btn-grid">Cetak</button>' ;
            // $vaset['cmdcetakdm']    = '<button type="button" onClick="bos.trasuransipembayaran.cmdcetakdm(\''.$dbr['faktur'].'\')"
            //                              class="btn btn-primary btn-grid">Cetak DM</button>' ;
            // $vaset['cmdcetakdm']    = html_entity_decode($vaset['cmdcetakdm']) ;
            $vaset['cmdcetak']    = html_entity_decode($vaset['cmdcetak']) ;
            $vaset['cmdedit']       = html_entity_decode($vaset['cmdedit']) ;
            $vaset['cmddelete']	    = html_entity_decode($vaset['cmddelete']) ;
            if ($tglmin > date_2s($dbr['tgl'])) {
                $vaset['cmdedit'] = "";
                $vaset['cmddelete'] = "";
            }
            $vare[] = $vaset ;
        }
        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "ssasuransipembayaran_faktur", "") ;
    }

    public function saving(){
        $va 	     = $this->input->post() ;
        $faktur 	 = getsession($this, "ssasuransipembayaran_faktur") ;

        $this->trasuransipembayaran_m->saving($faktur, $va) ;
        echo(' bos.trasuransipembayaran.settab(0) ;  ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $faktur = $va['faktur'] ;
        $data   = $this->trasuransipembayaran_m->getdatatotal($faktur) ;
        if(!empty($data)){
            savesession($this, "ssasuransipembayaran_faktur", $faktur) ;
            // $jabatan = $this->perhitunganhrd_m->getjabatan($data['nip'],$data['tgl']);
            // $bagian = $this->perhitunganhrd_m->getbagian($data['nip'],$data['tgl']);
            $asuransi[] = array("id"=>$data['asuransi'],"text"=>$data['ketasuransi']);
            $rekkasbank[] = array("id"=>$data['rekkas'],"text"=>$this->bdb->getval("keterangan", "kode = '{$data['rekkas']}'", "keuangan_rekening","","",""));
            echo('
                w2ui["bos-form-trasuransipembayaran_grid2"].clear();
                with(bos.trasuransipembayaran.obj){
                find(".nav-tabs li:eq(1) a").tab("show") ;
                find("#faktur").val("'.$data['faktur'].'") ;
                
                find("#rekkasbank").sval('.json_encode($rekkasbank).') ;

                
                find("#keterangan").val("'.$data['keterangan'].'") ;
                find("#tgl").val("'.date_2d($data['tgl']).'");
                find("#asuransi").sval('.json_encode($asuransi).') ;

                }

            ') ;

            echo('
                bos.trasuransipembayaran.settab(1) ;
            ');
        }
    }
    public function deleting(){
        $va 	= $this->input->post() ;
        $this->trasuransipembayaran_m->deleting($va['faktur']) ;
        echo('bos.trasuransipembayaran.grid1_reloaddata() ; ') ;

    }

    
    public function seekrekening(){
        $search     = $this->input->get('q');
        $vdb    = $this->trasuransipembayaran_m->seekrekening($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trasuransipembayaran_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] . " - " . $dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

    public function getfaktur(){
        $va 	= $this->input->post() ;
        $faktur  = $this->trasuransipembayaran_m->getfaktur(FALSE) ;

        echo('
        bos.trasuransipembayaran.obj.find("#faktur").val("'.$faktur.'") ;
        ') ;
    }

    public function showreport(){
        $va 	= $this->input->get() ;
        $faktur = $va['faktur'] ;
        $data   = $this->trasuransipembayaran_m->getdatatotal($faktur) ;
        if(!empty($data)){
            $font = 9 ;
            

            $vHead = array() ;
            $vHead[] = array("1"=>"Faktur","2"=>":","3"=>$faktur,"4"=>"","5"=>"Kas/Bank","6"=>":","7"=>$data['rekkas']."-".$data['ketrekkas']);
            $vHead[] = array("1"=>"Tgl","2"=>":","3"=>date_2d($data['tgl']),"4"=>"","5"=>"Keterangan","6"=>":","7"=>$data['keterangan']);
            $vHead[] = array("1"=>"Asuransi","2"=>":","3"=>$data['ketasuransi'],"4"=>"","5"=>"","6"=>"","7"=>"");
        

            //detail
            $dbd = $this->trasuransipembayaran_m->loaddataasuransiedit($va) ;
            $n = 0 ;
            $array = array();
            $totalkaryawan = 0 ; 
            $totalperusahaan = 0 ; 
            $totalasuransi = 0 ; 
            while( $dbr = $this->trasuransipembayaran_m->getrow($dbd)){
                $n++;
                $arrjab = $this->perhitunganhrd_m->getjabatan($dbr['karyawan'],$data['tgl']);
                $arrbag = $this->perhitunganhrd_m->getbagian($dbr['karyawan'],$data['tgl']);
                $total = $dbr['tarifkaryawan'] + $dbr['tarifperusahaan'];
                $array[] = array("no" => $n,"nip"=>$dbr['karyawan'],"nama"=>$dbr['nama'],"jabatan"=>$arrjab['keterangan'],
                                "bagian"=>$arrbag['keterangan'],"nopendaftaran"=>$dbr['nopendaftaran'],
                                "noasuransi"=>$dbr['noasuransi'],"produk"=>$dbr['produk'],
                                "tarifkaryawan"=>string_2s($dbr['tarifkaryawan']),"tarifperusahaan"=>string_2s($dbr['tarifperusahaan']),
                                "total"=>string_2s($total));
                
                $totalkaryawan += $dbr['tarifkaryawan'] ;
                $totalperusahaan += $dbr['tarifperusahaan'] ;
                $totalasuransi += $total ;
            }

            $vtotal[0] = array("keterangan" => "<b>Total","tarifkaryawan"=>string_2s($totalkaryawan),"tarifperusahaan"=>string_2s($totalperusahaan),
                                "total"=>string_2s($totalasuransi)."</b>");

            $now  = date_2b($data['tgl']) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> $kota ,"5"=>"") ;
            //$vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"Dterima,","3"=>"","4"=>"Bag Keuangan,","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"(.......................)","3"=>"","4"=>"(.......................)","5"=>"") ;


            $o    = array('paper'=>'A4', 'orientation'=>'L', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                          'opt'=>array('export_name'=>'asuransipembayaran','mtop'=>1) ) ;
            $this->load->library('bospdf', $o) ;
            $this->bospdf->ezSetMargins(1,1,20,20);
            $this->bospdf->ezSetCmMargins(0, 1, 1, 1);
            //$this->bospdf->ezImage("./uploads/HeaderSJDO.jpg",true,'60','190','50');
            //$this->bospdf->ezLogoHeaderPage("./uploads/HeaderSJDO.jpg",'0','65','150','50');
            $this->bospdf->ezHeader("<b>Pembayaran Asuransi Karyawan</b>",array("justification"=>"center","fontSize"=>$font+4)) ;
            $this->bospdf->ezText("") ;

            //ketika width tidak di set maka akan menyesuaikan dengan lebar kertas

            $this->bospdf->ezTable($vHead,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "1"=>array("width"=>15,"justification"=>"left"),
                                             "2"=>array("width"=>5,"justification"=>"left"),
                                             "3"=>array("justification"=>"left"),
                                             "4"=>array("width"=>10,"justification"=>"left"),
                                             "5"=>array("width"=>15,"justification"=>"left"),
                                             "6"=>array("width"=>5,"justification"=>"left","wrap"=>1),
                                             "7"=>array("justification"=>"left","wrap"=>1))
                                        )
                                  ) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($array,"","",
                                   array("fontSize"=>$font-1,"showLines"=>1,
                                         "cols"=> array(
                                             "no"        =>array("caption"=>"No","width"=>3,"justification"=>"right"),
                                             "nip"       =>array("caption"=>"NIP","width"=>7,"justification"=>"center"),
                                             "nama"      =>array("caption"=>"Nama","wrap"=>1),                                             
                                             "jabatan"   =>array("caption"=>"Jabatan","width"=>10),                                             
                                             "bagian"    =>array("caption"=>"Bagian","width"=>10),                                             
                                             "nopendaftaran" =>array("caption"=>"No Pendaftaran","width"=>10,"justification"=>"center"),                                             
                                             "noasuransi"    =>array("caption"=>"No Asuransi","width"=>10,"justification"=>"center"),                                             
                                             "produk"        =>array("caption"=>"Produk","width"=>10),                                             
                                             "tarifkaryawan"    =>array("caption"=>"Karyawan","width"=>10,"justification"=>"right"),                                             
                                             "tarifperusahaan"  =>array("caption"=>"Perusahaan","width"=>10,"justification"=>"right"),                                             
                                             "total"            =>array("caption"=>"Total","width"=>10,"justification"=>"right"),                                             
                                             ))
                                  ) ;
            $this->bospdf->ezTable($vtotal,"","",
                                   array("fontSize"=>$font-1,"showLines"=>1,"showHeadings"=>0,
                                         "cols"=> array(
                                             "keterangan"      =>array("caption"=>"Keterangan","wrap"=>1,"justification"=>"center"),                                             
                                            "tarifkaryawan"    =>array("caption"=>"Karyawan","width"=>10,"justification"=>"right"),                                             
                                             "tarifperusahaan"  =>array("caption"=>"Perusahaan","width"=>10,"justification"=>"right"),                                             
                                             "total"            =>array("caption"=>"Total","width"=>10,"justification"=>"right"),                                             
                                             ))
                                  ) ;

         $this->bospdf->ezText("") ;
         $this->bospdf->ezText("") ;
         $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("justification"=>"right"),
                                          "2"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "3"=>array("width"=>40,"wrap"=>1),
                                          "4"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "5"=>array("wrap"=>1,"justification"=>"center"))
                                 )
                              ) ;


            $this->bospdf->ezStream() ;
        }else{
            echo("data tidak ada !!!");
        }

    }

    public function seekasuransi(){
        $search     = $this->input->get('q');
        $vdb    = $this->trasuransipembayaran_m->seekasuransi($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trasuransipembayaran_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

    public function loadgrid2(){
        $va = $this->input->post();
        //print_r($va);
        $va['tgl'] = date_2s($va['tgl']);
        $sessfkt = getsession($this, "ssasuransipembayaran_faktur", "");

        $vare = array();
        $n = 0;
        $recid = 0;
        $totkaryawan = 0 ;
        $totperusahaan = 0 ;
        $totasuransi = 0 ;
        if(isset($va['asuransi'])){
            if($sessfkt == ""){
                $dbd = $this->trasuransipembayaran_m->loaddataasuransi($va);
                while ($dbr = $this->trasuransipembayaran_m->getrow($dbd)) {
                    $n++;
                    $recid++;
                    $arrjab = $this->perhitunganhrd_m->getjabatan($dbr['karyawan'],$va['tgl']);
                    $arrbag = $this->perhitunganhrd_m->getbagian($dbr['karyawan'],$va['tgl']);
                    
                    $arrtarif = $this->perhitunganhrd_m->gettarifasuransikaryawan($dbr['nopendaftaran'],$va['tgl']);

                    $total = $arrtarif['karyawan'] + $arrtarif['perusahaan'];
                    $vare[] = array("recid" => $recid,"nip"=>$dbr['karyawan'],"nama"=>$dbr['nama'],"jabatan"=>$arrjab['keterangan'],
                                "bagian"=>$arrbag['keterangan'],"nopendaftaran"=>$dbr['nopendaftaran'],
                                "noasuransi"=>$dbr['noasuransi'],"asuransi"=>$dbr['ketasuransi'],"produk"=>$dbr['produk'],
                                "tarifkaryawan"=>$arrtarif['karyawan'],"tarifperusahaan"=>$arrtarif['perusahaan'],
                                "total"=>$total
                            );
        
                    $totkaryawan += $arrtarif['karyawan'] ;
                    $totperusahaan += $arrtarif['perusahaan'] ;
                    $totasuransi += $total;
                }
            }else{
                $dbd = $this->trasuransipembayaran_m->loaddataasuransiedit($va);
                while ($dbr = $this->trasuransipembayaran_m->getrow($dbd)) {
                    $n++;
                    $recid++;
                    $arrjab = $this->perhitunganhrd_m->getjabatan($dbr['karyawan'],$va['tgl']);
                    $arrbag = $this->perhitunganhrd_m->getbagian($dbr['karyawan'],$va['tgl']);
                    $total = $dbr['tarifkaryawan'] + $dbr['tarifperusahaan'];

                    $vare[] = array("recid" => $recid,"nip"=>$dbr['karyawan'],"nama"=>$dbr['nama'],"jabatan"=>$arrjab['keterangan'],
                                "bagian"=>$arrbag['keterangan'],"nopendaftaran"=>$dbr['nopendaftaran'],
                                "noasuransi"=>$dbr['noasuransi'],"asuransi"=>$dbr['ketasuransi'],"produk"=>$dbr['produk'],
                                "tarifkaryawan"=>$dbr['tarifkaryawan'],"tarifperusahaan"=>$dbr['tarifperusahaan'],
                                "total"=>$total
                            );
        
                    $totkaryawan += $dbr['tarifkaryawan'] ;
                    $totperusahaan += $dbr['tarifperusahaan'] ;
                    $totasuransi += $total;

                }
            }
        }
        

        $vare[] = array("recid" => "ZZZZ","nama"=>"Total ".$va['asuransi'],"tarifkaryawan"=>$totkaryawan,"tarifperusahaan"=>$totperusahaan,
                    "total"=>$totasuransi,"w2ui"=>array("summary"=> true));
        
       

        $vare = json_encode($vare);

        if($va['asuransi'] == "null"){
            echo ('
                w2ui["bos-form-trasuransipembayaran_grid2"].clear();
              ');
        }else{
            echo ('
                w2ui["bos-form-trasuransipembayaran_grid2"].add(' . $vare . ');
              ');
        }
    }
}
?>
