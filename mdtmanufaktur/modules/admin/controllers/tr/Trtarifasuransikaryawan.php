<?php
class Trtarifasuransikaryawan extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('tr/trtarifasuransikaryawan_m') ;
        $this->load->model('func/updtransaksi_m') ;
        $this->load->model('func/func_m') ;
        $this->load->model('func/perhitunganhrd_m') ;
        $this->load->library('escpos');
        $this->load->helper('bdate');
        $this->bdb = $this->trtarifasuransikaryawan_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(),"mintgl"=>$tglmin) ;
        $this->load->view('tr/trtarifasuransikaryawan',$d) ;
    }

    public function loadgrid(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare               = array() ;
        $va['tglakhir']     = date_2s($va['tglakhir']);
        $vdb                = $this->trtarifasuransikaryawan_m->loadgrid($va) ;
        $dbd                = $vdb['db'] ;
        $tgl = $va['tglakhir'];//date("Y-m-d");
        while( $dbr = $this->trtarifasuransikaryawan_m->getrow($dbd) ){
            $vaset                  = $dbr ;
            $vaset['recid'] = $dbr['id']; 
            $vaset['tgl']           = date_2d($vaset['tgl']);
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['nip'],$tgl);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['nip'],$tgl);
            $vaset['bagian']  = $bagian['keterangan'];
            $vaset['jabatan']  = $jabatan['keterangan'];
            $tarifasuransi = $this->perhitunganhrd_m->gettarifasuransikaryawan($dbr['nopendaftaran'],$tgl);
            $vaset['tarifkaryawan'] = $tarifasuransi['karyawan'] ;
            $vaset['tarifperusahaan'] = $tarifasuransi['perusahaan'] ;
            $vare[] = $vaset ;
        }
        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function saving(){
        $va 	     = $this->input->post() ;
        $error = $this->trtarifasuransikaryawan_m->saving($va) ;
        if($error == "ok"){
            echo(' 
                alert("Data berhasil disimpan... ");
                ') ;
        }else{
            echo('
                alert("'.$error.'");
                ');
        }
    }

    

    public function seekasuransi(){
        $search     = $this->input->get('q');
        $vdb    = $this->trtarifasuransikaryawan_m->seekasuransi($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trtarifasuransikaryawan_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }



    
}
?>
