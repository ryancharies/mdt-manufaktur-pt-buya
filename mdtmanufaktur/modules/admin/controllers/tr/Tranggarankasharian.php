<?php
class Tranggarankasharian extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('tr/tranggarankasharian_m') ;
        $this->load->helper('bdate');
		$this->load->model('func/func_m') ;
        $this->bdb = $this->tranggarankasharian_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(),"mintgl"=>$tglmin) ;
        $this->load->view('tr/tranggarankasharian',$d) ;
    }

    public function loadgrid(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare               = array() ;
        $va['tglawal']      = date_2s($va['tglawal']);
        $va['tglakhir']     = date_2s($va['tglakhir']);
        $vdb                = $this->tranggarankasharian_m->loadgrid($va) ;
        $dbd                = $vdb['db'] ;
        while( $dbr = $this->tranggarankasharian_m->getrow($dbd) ){
            $vaset                  = $dbr ;
            $vaset['tgl']           = date_2d($vaset['tgl']);
            $vaset['cmdedit']       = '<button type="button" onClick="bos.tranggarankasharian.cmdedit(\''.$dbr['faktur'].'\')"
                                        class="btn btn-default btn-grid">Edit</button>' ;
            $vaset['cmddelete']     = '<button type="button" onClick="bos.tranggarankasharian.cmddelete(\''.$dbr['faktur'].'\')"
                                        class="btn btn-danger btn-grid">Delete</button>' ;
            
            $vaset['cmdedit']       = html_entity_decode($vaset['cmdedit']) ;
            $vaset['cmddelete']	    = html_entity_decode($vaset['cmddelete']) ;
            if ($tglmin > date_2s($dbr['tgl'])) {
                $vaset['cmdedit'] = "";
                $vaset['cmddelete'] = "";
            }
            $vare[]                 = $vaset ;
        }
        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "ssanggarankasharian_faktur", "") ;
    }

    public function saving(){
        $va 	     = $this->input->post() ;
        $kode 	     = getsession($this, "ssanggarankasharian_faktur") ;
        $va['tgl']   = date_2s($va['tgl']) ;

        $this->tranggarankasharian_m->saving($kode, $va) ;
        echo(' bos.tranggarankasharian.settab(0) ;  ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $faktur = $va['faktur'] ;
        $data   = $this->tranggarankasharian_m->getdatatotal($faktur) ;
        if(!empty($data)){
            savesession($this, "ssanggarankasharian_faktur", $faktur) ;
            $rekkas[] = array("id"=>$data['rekening'],"text"=>$data['rekening']."-".$data['ketrekening']);
           
            echo('
            with(bos.tranggarankasharian.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#faktur").val("'.$data['faktur'].'") ;
               find("#tgl").val("'.date_2d($data['periode']).'");
               find("#rekening").sval('.json_encode($rekkas).');
               find("#keterangan").val("'.$data['keterangan'].'") ;
               find("#jumlah").val("'.string_2s($data['nominal'],2).'") ;

            }
            bos.tranggarankasharian.setopt("jenis","'.$data['jenis'].'");


         ') ;

            
            echo('
                bos.tranggarankasharian.settab(1) ;
            ');
        }
    }
    public function deleting(){
        $va 	= $this->input->post() ;
        $this->tranggarankasharian_m->deleting($va['faktur']) ;
        echo('bos.tranggarankasharian.grid1_reloaddata() ; ') ;

    }

    public function seekrekening(){
        $search     = $this->input->get('q');
        $vdb    = $this->tranggarankasharian_m->seekrekening($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->tranggarankasharian_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] . " - " . $dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

    public function getfaktur(){
        $va 	= $this->input->post() ;
        $faktur  = $this->tranggarankasharian_m->getfaktur(FALSE) ;

        echo('
        bos.tranggarankasharian.obj.find("#faktur").val("'.$faktur.'") ;
        ') ;
    }

}
?>
