<?php
class Trperintahproduksi extends Bismillah_Controller
{
    private $bdb;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('func/updtransaksi_m');
        $this->load->model('func/func_m');
        $this->load->model('tr/trperintahproduksi_m');
        $this->load->helper('bdate');
        $this->bdb = $this->trperintahproduksi_m;
    }

    public function index()
    {
        
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi", "", "", "tgl asc");
        $tglmin = "minimdate = '" . date("m/d/Y", strtotime($tglmin)) . "'";
        $d = array("dnow" => date("d-m-Y"), "setdate" => date_set(), "mintgl" => $tglmin);
        $this->load->view('tr/trperintahproduksi', $d);
    }

    public function loadgrid_where($bs, $s){
        $this->duser();

        $this->db->where('t.tgl >=', $bs['tglawal']);
        $this->db->where('t.tgl <=', $bs['tglakhir']);

        
        if($bs['skd_cabang'] !== 'null'){
            $bs['skd_cabang'] = json_decode($bs['skd_cabang'],true);
            $this->db->group_start();
            foreach($bs['skd_cabang'] as $kdc){
                $this->db->or_where('t.cabang',$kdc);
            }
            $this->db->group_end();

        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['cabang'])){
                    $this->db->group_start();
                    foreach($this->aruser['data_var']['cabang'] as $kdc){
                        $this->db->or_where('t.cabang', $kdc);
                    }
                    $this->db->group_end();
                }
            }
        }

        if($s !== ''){
            $this->db->group_start();
                $this->db->or_like(array('t.faktur'=>$s));
            $this->db->group_end();
        }

        $this->db->where('t.status',"1");

    }

    public function loadgrid()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va = json_decode($this->input->post('request'), true);
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->db->escape_like_str($search) ;
        $vare = array();
        $vkaru = array();
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);

        $this->loadgrid_where($va, $search);
        $f = "count(t.id) jml";
        $dbdt = $this->db->select($f)
            ->from("produksi_total t")
            ->join("cabang c","c.kode = t.cabang","left")
            ->get();
        $rtot  = $dbdt->row_array();
        if($rtot['jml'] > 0){

            $this->loadgrid_where($va, $search);
            $f2 = "t.faktur,t.tgl,t.cabang,t.hargapokok,t.bb,t.bop,t.btkl,t.tglclose,t.regu,t.petugas";
            $dbd = $this->db->select($f2)
                ->from("produksi_total t")
                ->join("cabang c","c.kode = t.cabang","left")
                ->order_by('t.faktur ASC')
                ->limit($limit)
                ->get();
        
            foreach($dbd->result_array() as $dbr){  
                $vaset = $dbr;
                $vaset['tgl'] = date_2d($vaset['tgl']);
                $vaset['karu'] = $dbr['petugas'];
                if($dbr['regu'] !== null){
                    $regu = json_decode($dbr['regu'],true);
                    $vaset['regu'] = $regu['keterangan'];
                    $vkaru[$regu['karu']] = $this->bdb->getval("nama", "kode = '{$regu['karu']}'", "karyawan");
                    $vaset['karu'] = $vkaru[$regu['karu']];

                }
                $vaset['cmdedit'] = "";
                $vaset['cmddelete'] = "";
                if ($dbr['tglclose'] == "0000-00-00") {
                    $vaset['cmdedit'] = '<button type="button" onClick="bos.trperintahproduksi.cmdedit(\'' . $dbr['faktur'] . '\')"
                                            class="btn btn-default btn-grid">Edit</button>';
                    $vaset['cmddelete'] = '<button type="button" onClick="bos.trperintahproduksi.cmddelete(\'' . $dbr['faktur'] . '\')"
                                            class="btn btn-danger btn-grid">Delete</button>';
                    $vaset['cmdedit'] = html_entity_decode($vaset['cmdedit']);
                    $vaset['cmddelete'] = html_entity_decode($vaset['cmddelete']);
                }

                if ($tglmin > date_2s($dbr['tgl'])) {
                    $vaset['cmdedit'] = "";
                    $vaset['cmddelete'] = "";
                }
                $vare[] = $vaset;
            }
        }

        $vare = array("total" => $rtot['jml'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function init()
    {
        savesession($this, "ssperintahproduksi_faktur", "");
    }

    public function saving()
    {
        $va = $this->input->post();
        $va['tgl'] = date_2s($va['tgl']);
        $faktur         = getsession($this, "ssperintahproduksi_faktur", "");
        $va['faktur']   = $faktur !== "" ? $faktur : $this->func_m->getfaktur("PR",$va['tgl'],$va['cabang'],TRUE) ;
        //insert produk
        $data           = array("fakturproduksi"=>$va['faktur'],
                                "stock"=>$va['stock'],
                                "gudangperbaikan"=>$va['gudang'],
                                "qty"=>string_2n($va['qty']),
                                "bb"=>string_2n($va['bb']),
                                "btkl"=>string_2n($va['btkl']),
                                "bop"=>string_2n($va['bop']),
                                "hargapokok"=>string_2n($va['hargapokok']),
                                "jumlah"=>string_2n($va['jumlah'])) ;
        $where          = "fakturproduksi = " . $this->db->escape($va['faktur']) ;
        $this->bdb->update("produksi_produk", $data, $where, "") ;

        $bb = string_2n($va['qty']) * string_2n($va['bb']);
        $btkl = string_2n($va['qty']) * string_2n($va['btkl']);
        $bop = string_2n($va['qty']) * string_2n($va['bop']);
        $jumlah = $bb + $btkl + $bop;


        //insert produksi total
        $regu = array();
        $regu = $this->bdb->getval("kode,keterangan,karu,anggota", "kode = '{$va['regu']}'", "produksi_regu","","","");
        $regu['anggota'] = json_decode($regu['anggota'],true);

        $data           = array("faktur"=>$va['faktur'],
                                "tgl"=>$va['tgl'],
                                "cabang"=>$va['cabang'],
                                "perbaikan"=>$va['perbaikan'],
                                "regu"=>json_encode($regu),
                                "bb"=>$bb,
                                "btkl"=>$btkl,
                                "bop"=>$bop,
                                "hargapokok"=>$jumlah,
                                "status"=>"1",
                                "username"=> getsession($this, "username"),
                                "datetime"=>date("Y-m-d H:i:s")) ;
        $where          = "faktur = " . $this->db->escape($va['faktur']) ;
        $this->bdb->update("produksi_total", $data, $where, "") ;


        //insert detail bb standart
        $vaGrid = json_decode($va['grid2']);
        $this->db->where("faktur",$va['faktur']);
        $this->db->delete("produksi_bb_standart") ;
        foreach($vaGrid as $key => $val){
            $vadetail = array("faktur"=>$va['faktur'],"stock"=>$val->kodebb,"qty"=>$val->qtybb,
                              "hp"=>$val->hpbb,"jmlhp"=>$val->jmlhpbb);
            $this->db->insert("produksi_bb_standart",$vadetail);
        }
        
        $this->updtransaksi_m->updkartustockperintahproduksi($va['faktur']);
        $this->updtransaksi_m->updrekperintahproduksi($va['faktur']);

        echo("ok");
    }

    public function editing()
    {
        $va = $this->input->post();

        $this->db->where("t.faktur",$va['faktur']);
        $f = "t.faktur,t.tgl,t.cabang,c.keterangan as ketcabang,p.stock,p.bb,p.qty,p.btkl,t.petugas,t.regu,
                  p.bop,p.hargapokok,p.jumlah,s.satuan,s.keterangan namastock,t.perbaikan,p.gudangperbaikan,
                  g.keterangan as ketgudang";
        $dbdt = $this->db->select($f)
            ->from("produksi_total t")
            ->join("cabang c","c.kode = t.cabang","left")
            ->join("produksi_produk p","p.fakturproduksi = t.faktur","left")
            ->join("stock s","s.kode = p.stock","left")
            ->join("gudang g","g.kode = p.gudangperbaikan","left")
            ->get();
        $data  = $dbdt->row_array();
        if (!empty($data)) {
            savesession($this, "ssperintahproduksi_faktur", $va['faktur']);
            $data['regu'] = json_decode($data['regu'],true);
            $data['tgl'] = date_2d($data['tgl']);
            if(!empty($data['regu']))$data['regu']['namakaru'] = $this->bdb->getval("nama", "kode = '{$data['regu']['karu']}'", "karyawan","","","");

            //loadgrid detail
            $vare = array();
            $this->db->where("b.faktur",$va['faktur']);
            $f2 = "b.stock as kodebb,b.qty as qtybb,b.hp as hpbb,b.jmlhp as jmlhpbb,s.satuan as satbb,s.keterangan as namabb";
            $dbd2 = $this->db->select($f2)
                ->from("produksi_bb_standart b")
                ->join("stock s","s.kode = b.stock","left")
                ->order_by('b.id ASC')
                ->get();
            foreach($dbd2->result_array() as $dbr2){  
                $vaset = $dbr2;
                $vaset['totqtybb'] = $dbr2['qtybb'] * $data['qty'];
                $vare[$dbr2['kodebb']] = $vaset;
            }
            // $vare = json_encode($vare);
            $data['detail'] = $vare;
        }
        echo(json_encode($data));

    }

    public function deleting()
    {
        $va = $this->input->post();
        $this->trperintahproduksi_m->deleting($va['faktur']);
        echo ('bos.trperintahproduksi.grid1_reloaddata() ; ');

    }


    public function getfaktur(){
        $va 	= $this->input->post() ;
        $faktur  = $this->func_m->getfaktur("PR",$va['tgl'],$va['cabang'],FALSE) ;
        echo($faktur);
    }

    public function hitungproduk()
    {
        $va = $this->input->post();
        $kode = $va['kode'];
        $data = $this->trperintahproduksi_m->getdata($kode);
        if (!empty($data)) {
            echo ('
            with(bos.trperintahproduksi.obj){
               find("#qty").focus() ;
			   find("#btkl").val("' . $data['btkl'] . '");
			   find("#bop").val("' . $data['bop'] . '");
            }
            w2ui["bos-form-trperintahproduksi_grid2"].clear();

         ');

            //loadgrid detail
            $vare = array();
            $dbd = $this->trperintahproduksi_m->autoloadkomponen($data['kode']);
            $n = 0;
            while ($dbr = $this->trperintahproduksi_m->getrow($dbd)) {
                $vaset = $dbr;
                $vaset['recid'] =$dbr['kodebb'];
                $vaset['cmddelete'] = '<button type="button" onClick="bos.trperintahproduksi.grid2_deleterow(' . $vaset['recid'] . ')"
                                        class="btn btn-danger btn-grid">Delete</button>';
                $vaset['cmddelete'] = html_entity_decode($vaset['cmddelete']);
                $vare[] = $vaset;
            }
            $vare = json_encode($vare);
            echo ('
                w2ui["bos-form-trperintahproduksi_grid2"].add(' . $vare . ');
                bos.trperintahproduksi.initdetail();
				bos.trperintahproduksi.hitungbb();
            ');

        }
    }

}
