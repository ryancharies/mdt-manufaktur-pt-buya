<?php
class Trpr extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('tr/trpr_m') ;
        $this->load->model('func/perhitungan_m') ;
        $this->load->model('func/func_m') ;
        $this->load->helper('bdate');
        $this->bdb = $this->trpr_m ;
    }

    public function index(){
        // mengambil min tgl transaksi yang aktif
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";

        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(),"mintgl"=>$tglmin) ;
        $this->load->view('tr/trpr',$d) ;
    }

    public function loadgrid_where($bs, $s){
        $this->duser();

        $this->db->where('t.status', "1");
        $this->db->where('t.tgl >=', $bs['tglawal']);
        $this->db->where('t.tgl <=', $bs['tglakhir']);

        
        if($bs['skd_gudang'] !== 'null'){
            $bs['skd_gudang'] = json_decode($bs['skd_gudang'],true);
            $this->db->group_start();
            foreach($bs['skd_gudang'] as $kdc){
                $this->db->or_where('t.gudang',$kdc);
            }
            $this->db->group_end();

        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['gudang'])){
                    $this->db->group_start();
                    foreach($this->aruser['data_var']['gudang'] as $kdc){
                        $this->db->or_where('t.gudang', $kdc);
                    }
                    $this->db->group_end();
                }
            }
        }

        if($s !== ''){
            $this->db->group_start();
                $this->db->or_like(array('t.faktur'=>$s, 's.keterangan'=>$s));
            $this->db->group_end();
        }
    }

    public function loadgrid(){
        $this->duser();
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va                 = json_decode($this->input->post('request'), true) ;
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->db->escape_like_str($search) ;
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $vare               = array() ;
        

        $this->loadgrid_where($va, $search);
        $f = "count(t.id) jml";
        $dbd = $this->db->select($f)
            ->from("pr_total t")
            ->join("gudang s","s.kode = t.gudang","left")
            ->get();
        $rtot  = $dbd->row_array();
        if($rtot['jml'] > 0){

            $this->loadgrid_where($va, $search);
            $f2 = "t.faktur,t.tgl,s.keterangan as gudang";
            $dbd2 = $this->db->select($f2)->from("pr_total t")
                ->join("gudang s","s.kode = t.gudang","left")
                ->limit($limit)
                ->get();
        
            foreach($dbd2->result_array() as $r2){
                $vaset                  = $r2 ;
                $vaset['tgl']           = date_2d($vaset['tgl']);
                $vaset['cmdedit']       = '<button type="button" onClick="bos.trpr.cmdedit(\''.$r2['faktur'].'\')"
                                            class="btn btn-default btn-grid">Edit</button>' ;
                $vaset['cmddelete']     = '<button type="button" onClick="bos.trpr.cmddelete(\''.$r2['faktur'].'\')"
                                            class="btn btn-danger btn-grid">Delete</button>' ;
                $vaset['cmdedit']       = html_entity_decode($vaset['cmdedit']) ;
                $vaset['cmddelete']	    = html_entity_decode($vaset['cmddelete']) ;
                if($tglmin > date_2s($r2['tgl'])){
                    $vaset['cmdedit'] = "";
                    $vaset['cmddelete'] = "";
                }
                $vare[]                 = $vaset ;
            }
        }
        $return 	= array("total"=>$rtot['jml'], "records"=>$vare ) ;
        echo(json_encode($return)) ;
    }

    public function init(){
        savesession($this, "sspr_faktur", "") ;
    }

    public function saving(){
        $va 	     = $this->input->post() ;
        $faktur 	     = getsession($this, "sspr_faktur") ;
        $va['tgl']   = date_2s($va['tgl']) ;


        $igudang = $this->func_m->getinfogudang($va['gudang']);
        $cabang = $igudang['cabang'];
        $va['faktur']   = $faktur !== "" ? $faktur : $this->func_m->getfaktur("RQ",$va['tgl'],$cabang,true) ;

        

        $data           = array("faktur"=>$va['faktur'],
                                "tgl"=>$va['tgl'],
                                "status"=>"1",
                                "gudang"=>$va['gudang'],
                                "cabang"=> $cabang,
                                "username"=> getsession($this, "username"),
                                "datetime"=>date("Y-m-d H:i:s")) ;
        $where          = "faktur = " . $this->bdb->escape($va['faktur']) ;
        $this->bdb->update("pr_total", $data, $where, "") ;

        //insert detail po
        $vaGrid = json_decode($va['grid2']);
        $this->bdb->delete("pr_detail", "faktur = '{$va['faktur']}'" ) ;
        foreach($vaGrid as $key => $val){
            $cKdStockGrid = $val->stock;
            $dbRKD = $this->perhitungan_m->getdatastock($cKdStockGrid) ;
            $cKodeStock = $dbRKD['kode'];
            
            $vadetail = array("faktur"=>$va['faktur'],
                              "stock"=>$cKodeStock,
                              "qty"=>$val->qty);
            $this->bdb->insert("pr_detail",$vadetail);
        }

        echo("ok") ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $faktur = $va['faktur'] ;

        $this->db->where('t.faktur',$faktur);
        $f = "t.faktur,t.tgl,t.gudang,s.keterangan as ketgudang";
        $dbd = $this->db->select($f)
            ->from("pr_total t")
            ->join("gudang s","s.kode = t.gudang","left")
            ->get();
        $data  = $dbd->row_array();
        if(!empty($data)){
            savesession($this, "sspr_faktur", $va['faktur']) ;

            $data['tgl'] = date_2d($data['tgl']);

            $vare = array();
            $this->db->where('d.faktur',$faktur);
            $f2 = "d.stock,s.keterangan as namastock,d.qty,s.satuan";
            $dbd2 = $this->db->select($f2)->from("pr_detail d")
                ->join("stock s","s.kode = d.stock","left")
                ->get();
        
            foreach($dbd2->result_array() as $dbr){
                $vare[$dbr['stock']] = $dbr ;
            }
            $data['detil'] = $vare;
        }

        echo(json_encode($data));
    }

    public function deleting(){
        $va 	= $this->input->post() ;
        $this->bdb->edit("pr_total",array("status"=>2),"faktur = " . $this->bdb->escape($va['faktur']));
        echo('bos.trpr.grid1_reloaddata() ; ') ;

    }

    public function getfaktur(){
        $va 	= $this->input->post() ;

        $igudang = $this->func_m->getinfogudang($va['gudang']);
        $cabang = $igudang['cabang'];

        $faktur  = $this->func_m->getfaktur("RQ",$va['tgl'],$cabang,FALSE) ;
        echo($faktur);
    }
}
?>
