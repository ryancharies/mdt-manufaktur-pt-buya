<?php
class Trhasilproduksi extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('func/updtransaksi_m') ;
        $this->load->model('func/func_m') ;
        $this->load->model('tr/trhasilproduksi_m') ;
        $this->load->helper('bdate');
        $this->bdb = $this->trhasilproduksi_m ;
    }

    public function index(){
        $gudang = getsession($this, "gudang");
        $ketgudang 	= $gudang . " - " . $this->bdb->getval("keterangan", "kode = '{$gudang}'", "gudang") ;
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(),"gudang"=>$gudang,
                     "ketgudang"=>$ketgudang,"mintgl"=>$tglmin) ;
        $this->load->view('tr/trhasilproduksi',$d) ;
    }

    public function loadgrid_where($bs, $s){
        $this->duser();
        
        if($bs['skd_cabang'] !== 'null'){
            $bs['skd_cabang'] = json_decode($bs['skd_cabang'],true);
            $this->db->group_start();
            foreach($bs['skd_cabang'] as $kdc){
                $this->db->or_where('t.cabang',$kdc);
            }
            $this->db->group_end();

        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['cabang'])){
                    $this->db->group_start();
                    foreach($this->aruser['data_var']['cabang'] as $kdc){
                        $this->db->or_where('t.cabang', $kdc);
                    }
                    $this->db->group_end();
                }
            }
        }

        if($s !== ''){
            $this->db->group_start();
                $this->db->or_like(array('t.faktur'=>$s));
            $this->db->group_end();
        }

        $tgl = date("Y-m-d");

        $this->db->where('t.status',"1");
        $this->db->group_start();
        $this->db->or_where('t.tglclose >=', $tgl);
        $this->db->or_where('t.tglclose',"0000-00-00");
        $this->db->group_end();

    }

    public function loadgrid(){
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare = array();
        $vkaru = array();

        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->db->escape_like_str($search) ;
        $this->loadgrid_where($va, $search);
        $f = "count(t.id) jml";
        $dbdt = $this->db->select($f)
            ->from("produksi_total t")
            ->join("cabang c","c.kode = t.cabang","left")
            ->get();
        $rtot  = $dbdt->row_array();
        if($rtot['jml'] > 0){
            $this->loadgrid_where($va, $search);
            $f2    = "t.faktur,t.tgl,t.cabang,t.regu,t.petugas";
            $dbd = $this->db->select($f2)
                ->from("produksi_total t")
                ->join("cabang c","c.kode = t.cabang","left")
                ->order_by('t.faktur ASC')
                ->limit($limit)
                ->get();
        
            foreach($dbd->result_array() as $dbr){ 
                $vaset                  = $dbr ;
                $vaset['tgl'] = date_2d($vaset['tgl']);
                $vaset['karu'] = $dbr['petugas'];
                if($dbr['regu'] !== null){
                    $regu = json_decode($dbr['regu'],true);
                    $vaset['regu'] = $regu['keterangan'];
                    $vkaru[$regu['karu']] = $this->bdb->getval("nama", "kode = '{$regu['karu']}'", "karyawan");
                    $vaset['karu'] = $vkaru[$regu['karu']];

                }
                
                $vaset['cmdedit']       = '<button type="button" onClick="bos.trhasilproduksi.cmdedit(\''.$dbr['faktur'].'\')"
                                            class="btn btn-success btn-grid">Proses</button>' ;
                $vaset['cmdedit']       = html_entity_decode($vaset['cmdedit']) ;
                $vare[]                 = $vaset ;
            }
        }

        $vare 	= array("total"=>$rtot['jml'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "sshasilproduksi_faktur", "") ;
    }

    public function saving(){
        $va 	     = $this->input->post() ;
        $kode 	     = getsession($this, "sshasilproduksi_faktur") ;
        $va['tgl']   = date_2s($va['tgl']) ;

        $this->trhasilproduksi_m->saving($kode, $va) ;
        echo(' bos.trhasilproduksi.settab(0) ;  ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $faktur = $va['faktur'] ;

        $this->db->where('t.faktur',$faktur);
        $f = "t.faktur,t.tgl,t.cabang,c.keterangan as ketcabang,t.perbaikan,t.petugas,t.regu";
        $dbdt = $this->db->select($f)
            ->from("produksi_total t")
            ->join("cabang c","c.kode = t.cabang","left")
            ->get();
        $data  = $dbdt->row_array();
        if(!empty($data)){
            savesession($this, "ssprosesproduksi_faktur", $faktur) ;
            $data['tgl'] = date_2d($data['tgl']);

            $data['karu'] = $data['petugas'];
            if($data['regu'] !== null){
                $regu = json_decode($data['regu'],true);
                $data['karu'] = $this->bdb->getval("nama", "kode = '{$regu['karu']}'", "karyawan");
            }

            //loadgrid detail
            $vare = array();

            $this->db->where('p.fakturproduksi',$faktur);

            $field = "p.stock,s.keterangan as namastock,p.qty,s.satuan";
            $dbd =$this->db->select($field)
                ->from("produksi_produk p")
                ->join("stock s","s.kode = p.stock","left")
                ->get();
            foreach($dbd->result_array() as $dbr){ 
                $vare[] = $dbr ;
            }
            // $vare = json_encode($vare);
            $data['detil'] = $vare;
            
        }
        echo(json_encode($data));

    }

    public function deleteprod(){
        $va 	= $this->input->post() ;
        $this->trhasilproduksi_m->deleteprod($va) ;
        echo('bos.trhasilproduksi.grid2_reloaddata() ; ') ;

    }

    public function getfaktur(){
        $faktur  = $this->trhasilproduksi_m->getfaktur(FALSE) ;

        echo('
        bos.trhasilproduksi.obj.find("#faktur").val("'.$faktur.'") ;
        ') ;
    }
    
    public function pilihstock(){
        $va 	= $this->input->post() ;
        $kode 	= $va['stock'] ;
        $faktur 	= $va['faktur'] ;
        $data = $this->trhasilproduksi_m->getdataproduksiproduk($kode,$faktur) ;
        if(!empty($data)){
            echo('
            with(bos.trhasilproduksi.obj){
               find("#stock").val("'.$data['kode'].'") ;
               find("#namastock").val("'.$data['keterangan'].'");
               find("#qty").val("'.$data['qty'].'");
               find("#satuan").val("'.$data['satuan'].'");
               bos.trhasilproduksi.loadmodelstock("hide");
            }

         ') ;
        }
    }
    
    public function seekstock(){
        $va 	= $this->input->post() ;
        $kode 	= $va['stock'] ;
        $faktur 	= $va['fakturproduksi'] ;
        $data = $this->trhasilproduksi_m->getdataproduksiproduk($kode,$faktur) ;
        if(!empty($data)){
            echo('
            with(bos.trhasilproduksi.obj){
               find("#stock").val("'.$data['kode'].'") ;
               find("#namastock").val("'.$data['keterangan'].'");
               find("#qty").val("'.$data['qty'].'");
               find("#satuan").val("'.$data['satuan'].'");
            }

         ') ;
        }else{
            echo('
                alert("data tidak ditemukan !!!");
                with(bos.trhasilproduksi.obj){
                    find("#stock").val("") ;
                    find("#stock").focus() ;
                }
            ');
        }
    }
    
    public function loadgrid2(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->trhasilproduksi_m->loadgrid2($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;
        while( $dbr = $this->trhasilproduksi_m->getrow($dbd) ){
            $n++;
            $vaset   = $dbr ;
            $vaset['no'] = $n;
            $vaset['cmddelete']    = '<button type="button" onClick="bos.trhasilproduksi.grid2_deleterow(\''.$dbr['faktur'].'\')"
                           class="btn btn-danger btn-grid">delete</button>' ;
            $vaset['cmddelete']	   = html_entity_decode($vaset['cmddelete']) ;
            $vare[]		= $vaset ;
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }
    
    public function loadgrid3(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->trhasilproduksi_m->loadgrid3($va) ;
        $dbd    = $vdb['db'] ;
       // $va['fakturproduksi'] = "" ;
        while( $dbr = $this->trhasilproduksi_m->getrow($dbd) ){
            $vaset   = $dbr ;
            $vaset['cmdpilih']    = '<button type="button" onClick="bos.trhasilproduksi.cmdpilih(\''.$dbr['kode'].'\',\''.$va['fakturproduksi'].'\')"
                class="btn btn-success btn-grid">Pilih</button>' ;
            $vaset['cmdpilih']	   = html_entity_decode($vaset['cmdpilih']) ;
            $vare[]		= $vaset ;
        }

        $vare 	= array("total"=>count($vare), "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }
    
    public function savingprod(){
        $va 	     = $this->input->post() ;
        $va['tgl']   = date_2s($va['tgl']) ;
        $this->trhasilproduksi_m->savingprod($va) ;
        echo('
            bos.trhasilproduksi.initdetail();
            bos.trhasilproduksi.obj.find("#stock").focus() ;
            bos.trhasilproduksi.grid2_reloaddata() ;
        ') ;
    }
}
?>
