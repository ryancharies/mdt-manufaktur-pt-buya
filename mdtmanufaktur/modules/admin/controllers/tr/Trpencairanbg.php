<?php
class Trpencairanbg extends Bismillah_Controller
{
    protected $bdb;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("bdate");
        $this->load->model("tr/trpencairanbg_m");
        $this->load->model("func/updtransaksi_m");
        $this->load->model('func/func_m') ;
        $this->load->library('escpos');
        $this->bdb = $this->trpencairanbg_m;
        $this->ss  = getsession($this, "username")."-" . "ssrptpenjualanaset_" ;
    }

    public function index()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $d['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $this->load->view("tr/trpencairanbg",$d);

    }

    public function loadgrid()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va = json_decode($this->input->post('request'), true);
        $vare = array();
        $vdb = $this->bdb->loadgrid($va);
        $dbd = $vdb['db'];
        $n = 0 ;
        while ($dbr = $this->bdb->getrow($dbd)) {
            $n++;
            $dbr['tgl'] = date_2d($dbr['tgl']);
            $dbr['tgljthtmp'] = date_2d($dbr['tgljthtmp']);
            $vs = $dbr;
            $vs['status'] = "Belum Cair";
            if($dbr['fakturcair'] <> "")$vs['status'] = "Sudah Cair";
            $vs['no'] = $n;
            $vs['cmdreport'] = "";
            $vs['cmddotmatrix'] = "";
            if ($vs['fakturcair'] == "" and $dbr['status'] == "1") {
                $vs['cmdact'] = '<button type="button" onClick="bos.trpencairanbg.cmdproses(\'' . $dbr['nobgcek'] . '\',\'' . $dbr['faktur'] . '\')"
                            class="btn btn-warning btn-grid">Proses</button>';
            
            }else if($dbr['status'] == "3"){
                $vs['cmdact'] = '<button type="button" onClick="bos.trpencairanbg.cmdopenbg(\'' . $dbr['nobgcek'] . '\',\'' . $dbr['faktur'] . '\')"
                            class="btn btn-success btn-grid">Buka BG</button>';
            } else {
                $vs['cmdact'] = '<button type="button" onClick="bos.trpencairanbg.cmdbatalcair(\'' . $dbr['nobgcek'] . '\',\'' . $dbr['fakturcair'] . '\')"
                            class="btn btn-warning btn-grid">Batal Cair</button>';
                $vs['cmdreport'] = '<button type="button" onClick="bos.trpencairanbg.cmdreport(\'' . $dbr['nobgcek'] . '\',\'' . $dbr['faktur'] . '\')"
                        class="btn btn-danger btn-grid">Cetak PDF</button>';
                $vs['cmddotmatrix'] = '<button type="button" onClick="bos.trpencairanbg.cmddotmatrix(\'' . $dbr['nobgcek'] . '\',\'' . $dbr['faktur'] . '\')"
                        class="btn btn-primary btn-grid">Dot Matrix</button>';
                if($tglmin > date_2s($dbr['tglcair']) && date_2s($dbr['tglcair']) != '0000-00-00'){
                    $vs['cmdact'] = "";
                }
            }
            $vs['cmdact'] = html_entity_decode($vs['cmdact']);
            $vs['cmdreport'] = html_entity_decode($vs['cmdreport']);
            $vs['cmddotmatrix'] = html_entity_decode($vs['cmddotmatrix']);

            $vare[] = $vs;
        }
        //print_r($va);

        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function init()
    {
        savesession($this, "sstrpencairanbg_id", "");
    }

    public function saving()
    {
        $va = $this->input->post();
        $error = "";
        $id = getsession($this, "sstrpencairanbg_id");
        if(date_2s($va['tglkeluar']) > date_2s($va['tglcair'])){
            $error .= "Tgl proses tidak valid tidak boleh sebelum tanggal keluar";
        }
        if($error == ""){
            $this->bdb->saving($va, $id);
            echo (' bos.trpencairanbg.settab(0) ;  ');
        }else{
            echo('
                alert("'.$error.'");
            ');
        }
        
    }

    public function deleting()
    {
        $va = $this->input->post();
       // print_r($va);
        $faktur = $va['faktur'];
        
        $error = "Gagal hapus data, Data tidak valid!!";
        if ($faktur != "") {
            $error = "ok";
            
            $this->bdb->delete("keuangan_bukubesar", "faktur = " . $this->bdb->escape($faktur));
            $data    = array("fakturcair"=>"","tglcair"=>"0000-00-00","nominalcair"=>"0","usernamecair"=>"","keterangan"=>"",
                            "datetimecair"=>"0000-00-00 00:00:00") ;
            $where   = "fakturcair = '$faktur'";
            $this->bdb->edit("bg_list", $data, $where, "") ;

            if($error == "ok"){
                echo (' bos.trpencairanbg.grid1_reload() ; ');
            }else{
                echo('alert("'.$error.'");');
            }
        } else {
            echo ('
                    alert("' . $error . '");
                ');
        }
        
    }

    public function proses()
    {
        $va = $this->input->post();
        $nobgcek = $va['nobgcek'];
        $data = $this->trpencairanbg_m->getdata($nobgcek,$va['faktur']);
        if ($dbr = $this->trpencairanbg_m->getrow($data)) {
            savesession($this, "sstrpencairanbg_id", $dbr['nobgcek']);
            if ($dbr['fakturcair'] == "") {
                $dbr['fakturcair'] = $this->bdb->getfaktur(false);
            }
            if($dbr['tglcair'] == "0000-00-00")$dbr['tglcair'] = date("Y-m-d");
            echo ('
            with(bos.trpencairanbg.obj){
                find("#bank").val("'.$dbr['ketbank'].'") ;
		        find("#faktur").val("'.$dbr['faktur'].'") ;
                find("#kepada").val("'.$dbr['namasupplier'].'") ;
                find("#cabang").val("'.$dbr['cabang'].'") ;
                find("#bgcek").val("'.$dbr['bgcek'].'") ;
		        find("#norekening").val("'.$dbr['norekening'].'") ;
                find("#nobgcek").val("'.$dbr['nobgcek'].'") ;
                find("#tglkeluar").val("'.date_2d($dbr['tgl']).'") ;
                find("#tgljthtmp").val("'.date_2d($dbr['tgljthtmp']).'") ;
                find("#nominal").val("'.string_2s($dbr['nominal']).'") ;
        

                //penjualan
                find("#fakturcair").val("'.$dbr['fakturcair'].'") ;        
                find("#keterangan").val("'.$dbr['keterangan'].'") ;        
                find("#tglcair").val("'.date_2d($dbr['tglcair']).'") ;
                bos.trpencairanbg.setopt("status","'.$dbr['status'].'");
            }
            bos.trpencairanbg.settab(1) ;
         ');
        }
    }

    public function openbg()
    {
        $va = $this->input->post();
        $nobgcek = $va['nobgcek'];

        $error = "Gagal dibuka, Data tidak valid!!";
        if ($nobgcek != "") {
            $error = "ok";
            
            $data    = array("status"=>"1","tglcair"=>"0000-00-00","nominalcair"=>"0","usernamecair"=>"","keterangan"=>"",
                            "datetimecair"=>"0000-00-00 00:00:00") ;
            $where   = "nobgcek = '$nobgcek' and faktur = '{$va['faktur']}'";
            $this->bdb->edit("bg_list", $data, $where, "") ;

            if($error == "ok"){
                echo (' bos.trpencairanbg.grid1_reload() ; ');
            }else{
                echo('alert("'.$error.'");');
            }
        } else {
            echo ('
                    alert("' . $error . '");
                ');
        }
    }

    public function cetak()
    {
        $va = $this->input->post();
        $file       = setfile($this, "rpt_pencairanbg", __FILE__ , $va) ;
        savesession($this, $this->ss . "file", $file ) ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        file_put_contents($file, json_encode(array()) ) ;

        $file    = getsession($this, $this->ss . "file") ;
        $arr    = @file_get_contents($file);
        $arr    = json_decode($arr,true);

        $va = $this->input->post();
        $nobgcek = $va['nobgcek'];
        $dbd      = $this->trpencairanbg_m->getdata($nobgcek) ;
        if($dbr = $this->trpencairanbg_m->getrow($dbd)){
            $arr[] = array("ket"=>"Tgl","tt2"=>":","uraian"=>date_2d($dbr['tglcair']));
            $arr[] = array("ket"=>"No BG/CEK","tt2"=>":","uraian"=>$dbr['nobgcek']);
            $arr[] = array("ket"=>"Bank","tt2"=>":","uraian"=>$dbr['ketbank']);
            $arr[] = array("ket"=>"No Rekening","tt2"=>":","uraian"=>$dbr['norekening']);
            $arr[] = array("ket"=>"Nominal","tt2"=>":","uraian"=>string_2s($dbr['nominal']));
            $arr[] = array("ket"=>"Kepada","tt2"=>":","uraian"=>$dbr['namasupplier']);
            
        }

        file_put_contents($file, json_encode($arr) ) ;
        echo(' bos.trpencairanbg.openreport() ; ') ;
    }

    public function cetakdm()
    {
        $va 	= $this->input->post() ;
        $nobgcek = $va['nobgcek'] ;

        $this->trpencairanbg_m->cetakdm($nobgcek,$va['faktur']);
    }

    public function showreport(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $file = getsession($this, $this->ss . "file") ;
        $data = @file_get_contents($file) ;
        $data = json_decode($data,true) ;
        if(!empty($data)){
            //tanda tangan
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->trpencairanbg_m->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $ttd  = json_decode($this->trpencairanbg_m->getconfig("ttd"), true) ;
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> $kota ,"5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"Disetujui","3"=>"","4"=>"Bag. Keuangan,","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"(.........................)","3"=>"","4"=>"(.........................)","5"=>"") ;
            
            $font = 10;

            $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                          'opt'=>array('export_name'=>'Kartu Stock') ) ;
                          $this->load->library('bospdf', $o) ;
            
            $this->bospdf->ezText("<b>Pencairan BG/Cek</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($data,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "ket"=>array("width"=>15,"justification"=>"left"),
                                             "tt2"=>array("width"=>3,"justification"=>"left"),
                                             "uraian"=>array("justification"=>"left"))
                                        )
                                  ) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vttd,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "1"=>array("justification"=>"right"),
                                             "2"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                             "3"=>array("width"=>40,"wrap"=>1),
                                             "4"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                             "5"=>array("wrap"=>1,"justification"=>"center"))
                                        )
                                  ) ;
            $this->bospdf->ezStream() ;
        }else{
            echo('kosong') ;
        }
    }

}
