<?php
class Trasuransikaryawan extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('tr/trasuransikaryawan_m') ;
        $this->load->model('func/updtransaksi_m') ;
        $this->load->model('func/func_m') ;
        $this->load->model('func/perhitunganhrd_m') ;
        $this->load->library('escpos');
        $this->load->helper('bdate');
        $this->bdb = $this->trasuransikaryawan_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(),"mintgl"=>$tglmin) ;
        $this->load->view('tr/trasuransikaryawan',$d) ;
    }

    public function loadgrid(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare               = array() ;
        $va['tglawal']      = date_2s($va['tglawal']);
        $va['tglakhir']     = date_2s($va['tglakhir']);
        $vdb                = $this->trasuransikaryawan_m->loadgrid($va) ;
        $dbd                = $vdb['db'] ;
        $tgl = date("Y-m-d");
        while( $dbr = $this->trasuransikaryawan_m->getrow($dbd) ){
            $vaset                  = $dbr ;
            $vaset['tgl']           = date_2d($vaset['tgl']);
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['nip'],$tgl);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['nip'],$tgl);
            $vaset['bagian']  = $bagian['keterangan'];
            $vaset['jabatan']  = $jabatan['keterangan'];
            $vaset['cmdedit']       = '<button type="button" onClick="bos.trasuransikaryawan.cmdedit(\''.$dbr['nopendaftaran'].'\')"
                                        class="btn btn-default btn-grid">Edit</button>' ;
            $vaset['cmddelete']     = '<button type="button" onClick="bos.trasuransikaryawan.cmddelete(\''.$dbr['nopendaftaran'].'\')"
                                         class="btn btn-danger btn-grid">Delete</button>' ;
            $vaset['cmdedit']       = html_entity_decode($vaset['cmdedit']) ;
            $vaset['cmddelete']	    = html_entity_decode($vaset['cmddelete']) ;
            if ($tglmin > date_2s($dbr['tgl'])) {
                $vaset['cmdedit'] = "";
                $vaset['cmddelete'] = "";
            }
            $vare[] = $vaset ;
        }
        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "ssasuransikaryawan_nopendaftaran", "") ;
    }

    public function saving(){
        $va 	     = $this->input->post() ;
        $faktur 	 = getsession($this, "ssasuransikaryawan_nopendaftaran") ;
        $error = $this->trasuransikaryawan_m->saving($faktur, $va) ;
        if($error == "ok"){
            echo(' 
                alert("Data berhasil disimpan... ");
                bos.trasuransikaryawan.settab(0) ;
                ') ;
        }else{
            echo('
                alert("'.$error.'");
                ');
        }
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $nopendaftaran = $va['nopendaftaran'] ;
        $data   = $this->trasuransikaryawan_m->getdatatotal($nopendaftaran) ;
        if(!empty($data)){
            savesession($this, "ssasuransikaryawan_nopendaftaran", $nopendaftaran) ;
            $jabatan = $this->perhitunganhrd_m->getjabatan($data['nip'],$data['tgl']);
            $bagian = $this->perhitunganhrd_m->getbagian($data['nip'],$data['tgl']);
            $asuransi[] = array("id"=>$data['asuransi'],"text"=>$data['ketasuransi']);

            echo('
            w2ui["bos-form-trasuransikaryawan_grid2"].clear();
            with(bos.trasuransikaryawan.obj){
                find(".nav-tabs li:eq(1) a").tab("show") ;
                find("#nopendaftaran").val("'.$data['nopendaftaran'].'") ;
                find("#tgl").val("'.date_2d($data['tgl']).'");
                find("#noasuransi").val("'.$data['noasuransi'].'") ;
                find("#asuransi").sval('.json_encode($asuransi).') ;
                find("#produk").val("'.$data['produk'].'") ;
                find("#nip").val("'.$data['nip'].'") ;
                find("#namakaryawan").val("'.$data['nama'].'") ;
                find("#jabatan").val("'.$jabatan['keterangan'].'") ;
                find("#bagian").val("'.$bagian['keterangan'].'") ;               

            }
            bos.trasuransikaryawan.settab(1) ;

         ') ;
        }
    }
    public function deleting(){
        $va 	= $this->input->post() ;
        $error = $this->trasuransikaryawan_m->deleting($va['faktur']) ;
        

        if($error == "ok"){
            echo(' 
                alert("Data berhasil dihapus... ");
                bos.trasuransikaryawan.grid1_reloaddata()  ;
                ') ;
        }else{
            echo('
                alert("'.$error.'");
                ');
        }

    }

    public function loadgrid2()
    {
        $va = json_decode($this->input->post('request'), true);
        $vare = array();
        $vdb = $this->trasuransikaryawan_m->loadgrid2($va);
        $dbd = $vdb['db'];
        $tgl = $va['tgl'];
        while ($dbr = $this->trasuransikaryawan_m->getrow($dbd)) {
            $vaset = $dbr;
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['nip'],$tgl);
            $vaset['jabatan'] = $jabatan['keterangan'];
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['nip'],$tgl);
            $vaset['bagian'] = $bagian['keterangan'];
            
            $vaset['cmdpilih'] = '<button type="button" onClick="bos.trasuransikaryawan.cmdpilih(\'' . $dbr['nip'] . '\')"
                           class="btn btn-success btn-grid">Pilih</button>';
            $vaset['cmdpilih'] = html_entity_decode($vaset['cmdpilih']);
            $vare[] = $vaset;
        }

        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function seekasuransi(){
        $search     = $this->input->get('q');
        $vdb    = $this->trasuransikaryawan_m->seekasuransi($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trasuransikaryawan_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

    public function pilihnip()
    {
        $va = $this->input->post();
        //print_r($va);
        $dbd = $this->trasuransikaryawan_m->getdatakaryawan($va);
        $tgl = $va['tgl'];
        if ($dbr = $this->bdb->getrow($dbd)) {
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$tgl);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['kode'],$tgl);
            echo ('
            with(bos.trasuransikaryawan.obj){
                find("#nip").val("' . $dbr['kode'] . '") ;
                find("#namakaryawan").val("' . $dbr['nama'] . '");
                find("#jabatan").val("' . $jabatan['keterangan'] . '");
                find("#bagian").val("' . $bagian['keterangan'] . '");
                
                bos.trasuransikaryawan.loadmodelnip("hide");
            }
         ');
        }
    }

    public function getnopendaftaran(){
        $va 	= $this->input->post() ;
        $nopendaftaran  = $this->trasuransikaryawan_m->getnopendaftaran(FALSE) ;

        echo('
            bos.trasuransikaryawan.obj.find("#nopendaftaran").val("'.$nopendaftaran.'") ;
        ') ;
    }

    
}
?>
