<?php
class Trpengajuandana extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('tr/trpengajuandana_m') ;
        $this->load->model('func/perhitunganhrd_m') ;
        $this->load->helper('bdate');
		$this->load->model('func/func_m') ;
        $this->load->library('escpos');
        $this->load->library('curl');
        $this->load->library('m_pdf');
        $this->bdb = $this->trpengajuandana_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(),"mintgl"=>$tglmin) ;
        $this->load->view('tr/trpengajuandana',$d) ;
    }

    public function loadgrid(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare               = array() ;
        $va['tglawal']      = date_2s($va['tglawal']);
        $va['tglakhir']     = date_2s($va['tglakhir']);
        $vdb                = $this->trpengajuandana_m->loadgrid($va) ;
        $dbd                = $vdb['db'] ;
        while( $dbr = $this->trpengajuandana_m->getrow($dbd) ){
            $vaset                  = $dbr ;
            $vaset['tgl']           = date_2d($vaset['tgl']);
            $jenis = "";
            if($dbr['jenis'] == "0")$jenis = "Rutin";
            if($dbr['jenis'] == "1")$jenis = "Insidentil";
            $vaset['jenis'] = $jenis;
            $vaset['cmdedit']       = '<button type="button" onClick="bos.trpengajuandana.cmdedit(\''.$dbr['faktur'].'\')"
                                        class="btn btn-default btn-grid">Edit</button>' ;
            $vaset['cmddelete']     = '<button type="button" onClick="bos.trpengajuandana.cmddelete(\''.$dbr['faktur'].'\')"
                                        class="btn btn-danger btn-grid">Delete</button>' ;
            $vaset['cmdcetak']      = '<button type="button" onClick="bos.trpengajuandana.cmdcetak(\''.$dbr['faktur'].'\')"
                                         class="btn btn-warning btn-grid">Cetak</button>' ;
            $vaset['cmdcetak']      = html_entity_decode($vaset['cmdcetak']) ;
			$vaset['cmdcetakdm']    = '<button type="button" onClick="bos.trpengajuandana.cmdcetakdm(\''.$dbr['faktur'].'\')"
                                         class="btn btn-primary btn-grid">Cetak DM</button>' ;
            $vaset['cmdcetakdm']    = html_entity_decode($vaset['cmdcetakdm']) ;
            $vaset['cmdedit']       = html_entity_decode($vaset['cmdedit']) ;
            $vaset['cmddelete']	    = html_entity_decode($vaset['cmddelete']) ;
            $proses = $this->trpengajuandana_m->cekprosesotorisasi($dbr['faktur']);
            if ($tglmin > date_2s($dbr['tgl']) or $proses) {
                $vaset['cmdedit'] = "";
                $vaset['cmddelete'] = "";
            }
            $vare[]                 = $vaset ;
        }
        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "sspengajuandana_faktur", "") ;
    }

    public function saving(){
        $va 	     = $this->input->post() ;
        $kode 	     = getsession($this, "sspengajuandana_faktur") ;
        $va['tgl']   = date_2s($va['tgl']) ;

        $error = $this->trpengajuandana_m->saving($kode, $va) ;
        if($error == "ok"){
            echo('
                alert("Pengajuan Berhasil disimpan... \\n Sedang menunggu persetujuan");
                bos.trpengajuandana.settab(0) ;  
                ') ;
        }else{
            echo('
                alert("'.$error.'"); 
                ') ;
        }
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $faktur = $va['faktur'] ;
        $data   = $this->trpengajuandana_m->getdatatotal($faktur) ;
        if(!empty($data)){
            savesession($this, "sspengajuandana_faktur", $faktur) ;

            echo('
            w2ui["bos-form-trpengajuandana_grid2"].clear();
            with(bos.trpengajuandana.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#faktur").val("'.$data['faktur'].'") ;
               find("#tgl").val("'.date_2d($data['tgl']).'");
               find("#keterangan").val("'.$data['keterangan'].'") ;
               find("#yangmengajukan").val("'.$data['yangmengajukan'].'") ;
               find("#jumlah").val("'.string_2s($data['jumlah'],2).'") ;

            }
            bos.trpengajuandana.setopt("jenis","'.$data['jenis'].'");


         ') ;

            //loadgrid detail
            $vare = array();
            $dbd = $this->trpengajuandana_m->getdatadetail($faktur) ;
            $n = 0 ;
            while( $dbr = $this->trpengajuandana_m->getrow($dbd) ){
                $n++;
                $vaset   = $dbr ;
                $kettahap = "None";
                if($dbr['tahap'] > 0) $kettahap = "Tahap ".$dbr['tahap'];
                $vaset['recid'] = $n;
                $vaset['no'] = $n;
                $vaset['kettahap'] = $kettahap;
                $vaset['cmddelete'] = '<button type="button" onClick="bos.trpengajuandana.grid2_deleterow('.$n.')"
                                        class="btn btn-danger btn-grid">Delete</button>' ;
                $vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

                $vare[]             = $vaset ;
            }
            $vare = json_encode($vare);

            // w2ui["bos-form-trpengajuandana_grid2"].add('.$vare.');
            //     bos.trpengajuandana.initdetail();

            echo('
                w2ui["bos-form-trpengajuandana_grid2"].add('.$vare.');
                bos.trpengajuandana.initdetail();
                bos.trpengajuandana.settab(1) ;
            ');
        }
    }
    public function deleting(){
        $va 	= $this->input->post() ;
        $error = $this->trpengajuandana_m->deleting($va['faktur']) ;
        if($error == "ok"){
            echo('
                alert("Pengajua berhasil dibatalkan!!");
                bos.trpengajuandana.grid1_reloaddata() ; 
            ') ;

        }else{
            echo('
                alert("'.$error.'");
            ') ;
        }

    }

    public function seekotorisasi(){
        $search = $this->input->get('q');
        $vdb    = $this->trpengajuandana_m->seekotorisasi($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trpengajuandana_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['username'], "text"=>$dbr['fullname']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

    public function getdataotorisasi(){
        $va  = $this->input->post() ;
        $ketjabatan = "";
        $dbd = $this->trpengajuandana_m->select("sys_username","nip","username = '{$va['username']}' and nip <> ''");
        if($dbr = $this->trpengajuandana_m->getrow($dbd)){
            $jabatan    = $this->perhitunganhrd_m->getjabatan($dbr['nip'],$va['tgl']);
            $ketjabatan = $jabatan['keterangan'];

        }
        echo('
            bos.trpengajuandana.obj.find("#jabatan").val("'.$ketjabatan.'") ;
        ') ;
    }

    public function getfaktur(){
        $va 	= $this->input->post() ;
        $faktur  = $this->trpengajuandana_m->getfaktur(FALSE) ;

        echo('
            bos.trpengajuandana.obj.find("#faktur").val("'.$faktur.'") ;
        ') ;
    }

    public function cetak(){
        $arr = array();
        $va     = $this->input->post() ;
        if(trim($va['faktur']) <> ""){
            $html = "";
            $username = getsession($this, "username");
            $pdfFilePath = "./tmp/".md5("rpcetakpengajuan-".date_now().$username).".pdf";
            $cDownloadPath = $pdfFilePath ;
            $saldo = 0 ;

            //total
            $html .= "<table style='width:100%;'>";
            $html .= "  <tr><td style='text-align:center;font-size:16;'>PENGAJUAN DANA</td></tr>";
            
            //cek total
            $data    = $this->trpengajuandana_m->getdatatotal($va['faktur']) ;        
            if(!empty($data)){
                $arrcab = $this->func_m->GetDataCabang($data['cabang']);
                $ketjenis = "";
                if($data['jenis'] == "0")$ketjenis = "Rutin";
                if($data['jenis'] == "1")$ketjenis = "Insidentil";
                $html .= "  <tr><td>";
                $html .= "      <table 'width:100%;'>";
                $html .= "          <tr><td style='width:150px;'>Faktur</td><td style='width:3px;'>:</td><td>".$data['faktur']."</td></tr>";
                $html .= "          <tr><td style='width:150px;'>Tgl</td><td style='width:3px;'>:</td><td>".date_2d($data['tgl'])."</td></tr>";
                $html .= "          <tr><td style='width:150px;'>Jumlah</td><td style='width:3px;'>:</td><td>Rp. ".string_2s($data['jumlah'])."</td></tr>";
                $html .= "          <tr><td style='width:150px;'>Terbilang</td><td style='width:3px;'>:</td><td>".terbilang($data['jumlah'])." Rupiah</td></tr>";
                $html .= "          <tr><td style='width:150px;'>Untuk Pembayaran</td><td style='width:3px;'>:</td><td>".$data['keterangan']."</td></tr>";
                $html .= "          <tr><td style='width:150px;'>Jenis</td><td style='width:3px;'>:</td><td>".$ketjenis."</td></tr>";
                $html .= "      </table>";
                $html .= "  </td></tr>"; 
                $html .= "  <tr><td style='text-align:right;'>".$arrcab['kota'].",".date_2d($data['tgl'])."</td></tr>";
                
                //array detail
                $arrdetail = array();
                $arrdetail[] = array("top"=>"Yang Mengajukan,","nama"=>$data['yangmengajukan']);

                $dbd    = $this->trpengajuandana_m->getdatadetail($va['faktur']) ;
                while($dbr = $this->trpengajuandana_m->getrow($dbd)){
                    $arrdetail[] = array("top"=>$dbr['jabatan'].",","nama"=>$dbr['namakaryawan']);
                }

                $html .= "  <tr><td width='100%'>";
                $html .= "      <table style='width:100%;'>";
                $html .= "          <tr>";
                foreach($arrdetail as $key => $val){
                    $html .= "          <td style='text-align:center;'>".$val['top']."</td>";
                }
                $html .= "          </tr>";
                
                for($i=0;$i<=10;$i++){
                    $html .= "          <tr>";
                    foreach($arrdetail as $key => $val){
                        $html .= "          <td style='text-align:center;'></td>";
                    }
                    $html .= "          </tr>";
                }

                $html .= "          <tr>";
                foreach($arrdetail as $key => $val){
                    $html .= "          <td style='text-align:center;'>".$val['nama']."</td>";
                }
                $html .= "          </tr>";
                $html .= "      </table>";
                $html .= "  </td></tr>"; 
            }

            

            $html .="</table>";

            $this->m_pdf->pdf->AddPageByArray([
                'margin-left' => 5,
                'margin-right' => 5,
                'margin-top' => 14,
                'margin-bottom' => 0,
            ]);

            $this->m_pdf->pdf->WriteHTML($html);
            $this->m_pdf->pdf->Output(FCPATH.$pdfFilePath,'F');

            echo("
                bos.trpengajuandana.showReport('".$pdfFilePath."');
            ");
        }else{
            echo('alert("Faktur tidak boleh kosong");');
        }
    }

    public function cetakdm(){
        $arr = array();
        $this->escpos->connect();
        $va     = $this->input->post() ;
        if(trim($va['faktur']) <> ""){
            $data    = $this->trpengajuandana_m->getdatatotal($va['faktur']) ;        
            if(!empty($data)){
                $arrcab = $this->func_m->GetDataCabang($data['cabang']);
                $ketjenis = "";
                if($data['jenis'] == "0")$ketjenis = "Rutin";
                if($data['jenis'] == "1")$ketjenis = "Insidentil";
                $cpl = 80;
                $this->escpos->teks(str_pad($arrcab['nama'],80," ",STR_PAD_RIGHT));
                $this->escpos->teks(str_pad($arrcab['alamat'],80," ",STR_PAD_RIGHT));
                $this->escpos->teks(str_pad("Telp/Fax : ".$arrcab['telp'],80," ",STR_PAD_RIGHT));
                $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
                $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
                $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
                $this->escpos->posisiteks("center");
                //$this->escpos->textsize(4,4);
                $this->escpos->teks("PENGAJUAN DANA");
                $this->escpos->posisiteks("left");
                $this->escpos->teks(str_pad("Faktur",20," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($data['faktur'],57," ",STR_PAD_RIGHT));
                $this->escpos->teks(str_pad("Tanggal",20," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad(date_2d($data['tgl']),57," ",STR_PAD_RIGHT));
                $this->escpos->teks(str_pad("Jumlah",20," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad("Rp.".string_2s($data['jumlah']),57," ",STR_PAD_RIGHT));
                $this->escpos->teks(str_pad("Terbilang",20," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad(terbilang($data['jumlah']),57," ",STR_PAD_RIGHT));
                $this->escpos->teks(str_pad("Utk Pembayaran",20," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($data['keterangan'],57," ",STR_PAD_RIGHT));
                $this->escpos->teks(str_pad("Jenis",20," ",STR_PAD_RIGHT).str_pad(":",3," ",STR_PAD_BOTH).str_pad($ketjenis,57," ",STR_PAD_RIGHT));
                $this->escpos->posisiteks("right");
                $this->escpos->teks($arrcab['kota'].",".date_2d($data['tgl']));
                //array detail
                $arrdetail = array();
                $arrdetail[] = array("top"=>"Yang Mengajukan,","nama"=>$data['yangmengajukan']);

                $dbd    = $this->trpengajuandana_m->getdatadetail($va['faktur']) ;
                while($dbr = $this->trpengajuandana_m->getrow($dbd)){
                    $arrdetail[] = array("top"=>$dbr['jabatan'].",","nama"=>$dbr['namakaryawan']);
                }

                $jmlttd = count($arrdetail);
                $cpldetail = devide($cpl,$jmlttd);
                $cpldetail = intval($cpldetail);
                $isi = "";
                foreach($arrdetail as $key => $val){
                    $isi .= str_pad($val['top'],$cpldetail," ",STR_PAD_BOTH);
                }
                $this->escpos->teks($isi);

                for($i=0;$i<=8;$i++){
                    $this->escpos->teks(str_pad("",$cpl," ",STR_PAD_BOTH));
                }

                $isi = "";
                foreach($arrdetail as $key => $val){
                    $isi .= str_pad($val['nama'],$cpldetail," ",STR_PAD_BOTH);
                }
                $this->escpos->teks($isi);
                
            }

            $this->escpos->cetak(true);
        }else{
            echo('alert("Faktur tidak boleh kosong");');
        }
    }
}
?>
