<?php
class Trperubahanaset extends Bismillah_Controller
{
    protected $bdb;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("bdate");
        $this->load->model("tr/trperubahanaset_m");
        $this->load->model("func/perhitungan_m");
        $this->load->model("func/func_m");
        $this->load->model("func/updtransaksi_m");
        $this->bdb = $this->trperubahanaset_m;
    }

    public function index()
    {
        // mengambil min tgl transaksi yang aktif
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data = array();
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $this->load->view("tr/trperubahanaset",$data);

    }

    public function loadgrid_where($bs, $s){
        $this->duser();


        $this->db->where('p.status', "1");
        $this->db->where('p.tgl >=', $bs['tglawal']);
        $this->db->where('p.tgl <=', $bs['tglakhir']);

        
        if($bs['skd_cabang'] !== 'null'){
            $bs['skd_cabang'] = json_decode($bs['skd_cabang'],true);
            $this->db->group_start();
            foreach($bs['skd_cabang'] as $kdc){
                $this->db->or_where('p.cabang',$kdc);
            }
            $this->db->group_end();

        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['cabang'])){
                    $this->db->group_start();
                    foreach($this->aruser['data_var']['cabang'] as $kdc){
                        $this->db->or_where('p.cabang', $kdc);
                    }
                    $this->db->group_end();
                }
            }
        }

        if($s !== ''){
            $this->db->group_start();
                $this->db->or_like(array('p.faktur'=>$s, 'a.keterangan'=>$s));
            $this->db->group_end();
        }
    }

    public function loadgrid()
    {
        $va = json_decode($this->input->post('request'), true);

        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->db->escape_like_str($search) ;

        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        // print_r($va);
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $vare = array();


        $this->loadgrid_where($va, $search);

        $f = "count(p.id) jml";
        $dbd = $this->db->select($f)
            ->from("aset_perubahan p")
            ->join("aset a","a.kode = p.kode","left")
            ->get();
        $rtot  = $dbd->row_array();
        if($rtot['jml'] > 0){

            $this->loadgrid_where($va, $search);
            $f2 = "p.id,p.kode,p.faktur,p.cabang,p.tgl,a.golongan,p.kelompok,p.dataawal,p.dataakhir,a.keterangan,p.vendors,a.unit";
            $dbd2 = $this->db->select($f2)->from("aset_perubahan p")
                ->join("aset a","a.kode = p.kode","left")
                ->limit($limit)
                ->get();
        
            foreach($dbd2->result_array() as $dbr){
            
                $d_akhir = json_decode($dbr['dataakhir'],true);
                $d_akhir_lama = floor($d_akhir['lama_bulan_akhir'] / 12) ." Tahun " . $d_akhir['lama_bulan_akhir'] % 12 ." Bulan";
                
                $dbr['lama'] = $d_akhir_lama;
                $dbr['hargaperolehan'] = $d_akhir['hargaperolehan_akhir'];
                $dbr['jenispenyusutan'] = $d_akhir['jenispenyusutan'];
                $dbr['tarifpenyusutan'] = $d_akhir['tarifpenyusutan'];
                $dbr['residu'] = $d_akhir['residu'];
    
                $dbr['tgl'] = date_2d($dbr['tgl']);
                $vs = $dbr;
                $vs['cmdedit'] = '<button type="button" onClick="bos.trperubahanaset.cmdedit(\'' . $dbr['faktur'] . '\')"
                               class="btn btn-default btn-grid">Koreksi</button>';
                $vs['cmdedit'] = html_entity_decode($vs['cmdedit']);
    
                $vs['cmddelete'] = '<button type="button" onClick="bos.trperubahanaset.cmddelete(\'' . $dbr['faktur'] . '\')"
                               class="btn btn-danger btn-grid">Hapus</button>';
                $vs['cmddelete'] = html_entity_decode($vs['cmddelete']);
                if($tglmin > date_2s($dbr['tgl'])){
                    $vs['cmdedit'] = "";
                    $vs['cmddelete'] = "";
                }
    
                $vare[] = $vs;
            }
        }
        
        
       

        $vare = array("total" => $rtot['jml'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function init()
    {
        savesession($this, "sstrperubahanaset_id", "");
    }

    public function saving()
    {
        $va = $this->input->post();
        $id = getsession($this, "sstrperubahanaset_id");

        $return = $this->bdb->saving($va, $id);
        echo $return;
    }

    public function deleting()
    {
        $va = $this->input->post();
        $kode = $va['kode'];
        $faktur = $va['faktur'];
        $error = "Gagal hapus data, Data tidak valid!!";
        if ($kode != "") {
            $error = "ok";
            $dbd1 = $this->bdb->select("aset", "id", "fakturpj <> '' and kode = '$kode'");
            if ($dbr1 = $this->bdb->getrow($dbd1)) {
                $error = "Data tidak bisa dihapus karena sudah dijual!!";
            } else {
                $dbd = $this->bdb->select("hutang_kartu", "id", "faktur <> '$faktur' and fkt = '$faktur' and fkt <> ''");
                if ($dbr = $this->bdb->getrow($dbd)) {
                    $error = "Data tidak bisa dihapus karena sudah masuk ke proses pelunasan!!";
                } else {

                    $this->bdb->delete("aset", "kode = " . $this->bdb->escape($kode));
                    if ($faktur != "") {
                        $this->bdb->delete("keuangan_bukubesar", "faktur = " . $this->bdb->escape($faktur));
                        $this->bdb->delete("hutang_kartu", "faktur = " . $this->bdb->escape($faktur));
                    }
                }
            }
            if ($error == "ok") {
                echo (' bos.trperubahanaset.grid1_reload() ; ');
            } else {
                echo ('
                    alert("' . $error . '");
                ');
            }
        } else {
            echo ('
                    alert("' . $error . '");
                ');
        }

    }

    public function editing(){
        $va = $this->input->post();
        $faktur = $va['faktur'];
        $this->db->or_where('p.faktur',$faktur);
        $field = "p.faktur,p.tgl,p.vendors,p.kode,p.kelompok,p.dataawal,p.dataakhir,a.keterangan,p.cabang,c.keterangan ketcabang,
                k.keterangan ketkelompok";
        $db = $this->db->select($field)->from("aset_perubahan p")
            ->join("aset a","a.kode = p.kode","left")
            ->join("aset_kelompok k","k.kode = p.kelompok","left")
            ->join("cabang c","c.kode = p.cabang","left")
            ->get(); //print_r($this->db->last_query());
        $r  = $db->row_array();

        if (!empty($r)) {
            savesession($this, "sstrperubahanaset_id", $r['faktur']);
            $kelaset[] = array("id" => $r['kelompok'], "text" => $r['kelompok'] . " - " . $r['ketkelompok']);
            $aset[] = array("id" => $r['kode'], "text" => $r['keterangan']);
            $cabang[] = array("id" => $r['cabang'], "text" => $r['ketcabang']);
            
            $d_akhir = json_decode($r['dataakhir'],true);
            $r['tgl'] = date_2d($r['tgl']);
            echo ('
                with(bos.trperubahanaset.obj){
                    find(".nav-tabs li:eq(1) a").tab("show") ;
                    find("#kode").sval(' . json_encode($aset) . ');
                    find("#cabang").sval(' . json_encode($cabang) . ');
                    find("#faktur").val("' . $r['faktur'] . '") ;
                    find("#kelaset").sval(' . json_encode($kelaset) . ') ;
                    find("#tgl").val("' . $r['tgl'] . '") ;
                    find("#hp_tambah").val("' . string_2s($d_akhir['hargaperolehan_tambahan']) . '") ;
                    find("#lama").val("' . string_2s($d_akhir['lama_bulan_tambahan']) . '") ;
                    find("#tarifpeny").val("' . $d_akhir['tarifpenyusutan'] . '") ;
                    find("#kode").prop("readonly", true);

                }
                bos.trperubahanaset.setopt("jenis","' . $d_akhir['jenispenyusutan'] . '");

                
                bos.trperubahanaset.dataaset_edit("'.$r['kode'].'","'.$r['tgl'].'");
            ');

            //loadgrid detail
            $vare = array();
            $vendors = json_decode($r['vendors'],true) ;

            foreach($vendors as $key => $val){
                $vaset['act'] = '<button type="button" onClick="bos.trperubahanaset.grid2_deleterow('.$val['vendor'].')"
                                        class="btn btn-danger btn-grid">Delete</button>' ;
                $vaset['act']	= html_entity_decode($vaset['act']) ;
                $vaset['recid'] = $val['vendor'];
                $vaset['vendor'] = $val['vendor'];
                $vaset['namavendor'] = $this->bdb->getval("nama", "kode = '{$val['vendor']}'", "supplier");
                $vaset['hutang'] = $val['hutang'];

                $vare[]             = $vaset ;

            }
            $vare = json_encode($vare);
            echo('
                w2ui["bos-form-trperubahanaset_grid2"].add('.$vare.');
                bos.trperubahanaset.initdetail();
                bos.trperubahanaset.grid2_hitungtotal();
                bos.trperubahanaset.settab(1) ;

            ');
        }
    }

    public function dataaset(){
        $va = $this->input->post();
        $va['tgl'] = date_2s($va['tgl']);
        $return = array("gol"=>"","kel"=>"","lama"=>0,"sisa_lama"=>0,"hp"=>0,"residu"=>0,"nilaibuku"=>0,"penyakhir"=>0,"tarifpeny"=>0,"jenispeny"=>"",
                "golongan"=>"","ketgolongan"=>"","kelompok"=>"","ketkelompok"=>"");
       
        // print_r($r);

        $this->db->where('a.kode',$va['kode']);
        $this->db->where('a.tglperolehan < ',$va['tgl']);
        $this->db->where('a.status',"0");
        $this->db->where('a.fakturhabis',"");
        
        $field = "a.kode,a.lama,a.hargaperolehan,a.residu,a.tarifpenyusutan,a.jenispenyusutan,a.golongan,a.kelompok,g.keterangan ketgolongan,k.keterangan ketkelompok";
        $db = $this->db->select($field)->from("aset a")
            ->join("aset_golongan g","g.kode = a.golongan","left")
            ->join("aset_kelompok k","k.kode = a.kelompok","left")
            ->get(); //print_r($this->db->last_query());
        $r  = $db->row_array();

        if(isset($r)){
            $this->db->where('a.kode',$va['kode']);
            $this->db->where('a.tgl <',$va['tgl']);
            $this->db->where('a.status',"1");
            $field = "a.*,k.keterangan ketkelompok";
            $db2 = $this->db->select($field)->from("aset_perubahan a")
                    ->join("aset_kelompok k","k.kode = a.kelompok","left")
                    ->order_by("a.tgl desc")
                    ->limit("1") //print_r($this->db->last_query());
                    ->get(); //print_r($this->db->last_query());
            $r2  = $db2->row_array();

            if(isset($r2)){
                $return['gol'] = $r['ketgolongan'];
                $return['kel'] = $r2['ketkelompok'];
                $return['golongan'] = $r['golongan'];
                $return['kelompok'] = $r2['kelompok'];

                $d_akhir = json_decode($r2['dataakhir'],true);
                $return['lama'] = $d_akhir['lama_bulan_akhir'];
                $return['residu'] = $d_akhir['residu'];
                $return['hp'] = $d_akhir['hargaperolehan_akhir'];
                $return['tarifpeny'] = $d_akhir['tarifpenyusutan'];
                $return['jenispeny'] = $d_akhir['jenispenyusutan'];

            }else{
                $return['gol'] = $r['ketgolongan'];
                $return['kel'] = $r['ketkelompok'];
                $return['lama'] = $r['lama'] * 12;
                $return['residu'] = $r['residu'];
                $return['hp'] = $r['hargaperolehan'];
                $return['tarifpeny'] = $r['tarifpenyusutan'];
                $return['jenispeny'] = $r['jenispenyusutan'];
                $return['golongan'] = $r['golongan'];
                $return['kelompok'] = $r['kelompok'];
            }
        }

        if(!empty($r['kode'])){
            //tgl ambil akhir bulan lalu, karena transaksi perubahan boleh dilakukan sebelum osting akhir bulan pada bulan berjalan
            $tglbulanlalu = date("Y-m-d",nextmonth(strtotime($va['tgl']),-1));
            $tglbulanlalu = date_eom($tglbulanlalu);
            $peny = $this->perhitungan_m->getpenyusutan($r['kode'],$tglbulanlalu);
            $return['penyakhir'] = $peny['akhir'];
            $return['sisa_lama'] = $return['lama'] - $peny['ke'];
            $return['sisa_lama'] = max($return['sisa_lama'],0);
            $return['nilaibuku'] = $return['hp'] - $peny['akhir'];
        }
        echo json_encode($return);
    }

    public function getkelaset(){
        $va = $this->input->post();
        $this->db->or_where('kode',$va['kode']);
        $db = $this->db->select("*")->from("aset_kelompok")->get(); //print_r($this->db->last_query());
        $r  = $db->row_array();
        echo json_encode($r);
    }

    public function getfaktur()
    {
        $va = $this->input->post();
        $fkt = $this->func_m->getfaktur("AE",$va['tgl'],$va['cabang'],false);
        echo($fkt);
    }

}
