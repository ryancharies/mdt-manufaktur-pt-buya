<?php
class Trreturpembelian extends Bismillah_Controller
{
    private $bdb;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('func/updtransaksi_m');
        $this->load->model('func/func_m');
        $this->load->model('tr/trreturpembelian_m');
        $this->load->helper('bdate');
        $this->bdb = $this->trreturpembelian_m;
    }

    public function index()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d = array("dnow" => date("d-m-Y"), "setdate" => date_set(),"mintgl"=>$tglmin);
        $this->load->view('tr/trreturpembelian', $d);
    }

    public function loadgrid()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va = json_decode($this->input->post('request'), true);
        $vare = array();
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $vdb = $this->trreturpembelian_m->loadgrid($va);
        $dbd = $vdb['db'];
        while ($dbr = $this->trreturpembelian_m->getrow($dbd)) {
            $vaset = $dbr;
            $vaset['tgl'] = date_2d($vaset['tgl']);
            $vaset['cmdedit'] = '<button type="button" onClick="bos.trreturpembelian.cmdedit(\'' . $dbr['faktur'] . '\')"
                           class="btn btn-default btn-grid">Edit</button>';
            $vaset['cmddelete'] = '<button type="button" onClick="bos.trreturpembelian.cmddelete(\'' . $dbr['faktur'] . '\')"
                           class="btn btn-danger btn-grid">Delete</button>';
            $vaset['cmdedit'] = html_entity_decode($vaset['cmdedit']);
            $vaset['cmddelete'] = html_entity_decode($vaset['cmddelete']);
            if($tglmin > date_2s($dbr['tgl'])){//jika status tgl tidak open maka tidak action
                $vaset['cmdedit'] = "";
                $vaset['cmddelete'] = "";
            }
            $vare[] = $vaset;
        }

        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function init()
    {
        savesession($this, "ssreturpembelian_faktur", "");
    }

    public function saving()
    {
        $va = $this->input->post();
        $kode = getsession($this, "ssreturpembelian_faktur");
        $va['tgl'] = date_2s($va['tgl']);
       // $this->trreturpembelian_m->saving($kode, $va);


        $faktur = getsession($this, "ssreturpembelian_faktur", "");
        $va['faktur'] = $faktur !== "" ? $faktur : $this->getfaktur();
        $data = array("faktur" => $va['faktur'], "tgl" => $va['tgl'], "subtotal" => $va['subtotal'], "total" => $va['total'], "status" => "1",
            "gudang" => $va['gudang'], "supplier" => $va['supplier'], "cabang" => getsession($this, "cabang"),
            "username" => getsession($this, "username"), "datetime" => date("Y-m-d H:i:s"));
        $where = "faktur = " . $this->escape($faktur);
        $this->update("pembelian_retur_total", $data, $where, "");

        //insert detail pembelian
        $vaGrid = json_decode($va['grid2']);
        $this->delete("pembelian_retur_detail", "faktur = '{$va['faktur']}'");
        foreach ($vaGrid as $key => $val) {
            $cKdStockGrid = $val->stock;
            $dbRKD = $this->perhitungan_m->getdatastock($cKdStockGrid);
            $cKodeStock = $dbRKD['kode'];
            
            $vadetail = array("faktur" => $va['faktur'],
                "stock" => $cKodeStock,
                "qty" => $val->qty,
                "harga" => $val->harga,
                "jumlah" => $val->jumlah,
                "totalitem" => $val->jumlah);
            $this->insert("pembelian_retur_detail", $vadetail);
        }

        //update kartu stock
        $this->updtransaksi_m->updkartustockreturpembelian($va['faktur']);
        $this->updtransaksi_m->updkartuhutangreturpembelian($va['faktur']);
        $this->updtransaksi_m->updrekreturpembelian($va['faktur']);

        echo (' bos.trreturpembelian.settab(0) ;  ');
    }

    public function editing()
    {
        $va = $this->input->post();
        $faktur = $va['faktur'];
        $data = $this->trreturpembelian_m->getdatatotal($faktur);
        if (!empty($data)) {
            savesession($this, "ssreturpembelian_faktur", $faktur);
            $gudang[] = array("id" => $data['gudang'], "text" => $data['ketgudang']);
            $supplier[] = array("id" => $data['supplier'], "text" => $data['namasupplier']);
            echo ('
            w2ui["bos-form-trreturpembelian_grid2"].clear();
            with(bos.trreturpembelian.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#faktur").val("' . $data['faktur'] . '") ;
               find("#tgl").val("' . date_2d($data['tgl']) . '");
               find("#gudang").sval(' . json_encode($gudang) . ');
               find("#supplier").sval(' . json_encode($supplier) . ');
               find("#subtotal").val("' . $data['subtotal'] . '") ;
               find("#total").val("' . $data['total'] . '") ;
            }

         ');

            //loadgrid detail
            $vare = array();
            $dbd = $this->trreturpembelian_m->getdatadetail($faktur);
            $n = 0;
            while ($dbr = $this->trreturpembelian_m->getrow($dbd)) {
                $n++;
                $vaset = $dbr;
                $vaset['recid'] = $n;
                $vaset['no'] = $n;
                $vaset['cmddelete'] = '<button type="button" onClick="bos.trreturpembelian.grid2_deleterow(' . $n . ')"
                           class="btn btn-danger btn-grid">Delete</button>';
                $vaset['cmddelete'] = html_entity_decode($vaset['cmddelete']);

                $vare[] = $vaset;
            }
            $vare = json_encode($vare);
            echo ('
            w2ui["bos-form-trreturpembelian_grid2"].add(' . $vare . ');
             bos.trreturpembelian.initdetail();
             bos.trreturpembelian.settab(1) ;
          ');
        }
    }

    public function deleting()
    {
        $va = $this->input->post();
        $error = $this->trreturpembelian_m->deleting($va['faktur']);
        if ($error == "ok") {
            echo ('bos.trreturpembelian.grid1_reloaddata() ; ');
        } else {
            echo('alert("'.$error.'");');
        }
        
    }

    public function getfaktur(){
        $va 	= $this->input->post() ;
        $igd = $this->func_m->getinfogudang($va['gudang']);
        $cabang = $igd['cabang'];
        $faktur  = $this->trreturpembelian_m->getfaktur($cabang,$va['tgl'],FALSE) ;
        echo($faktur) ;
    }

    public function loadgrid4(){
        $va = json_decode($this->input->post('request'), true);
        $vare = array();
        $vdb = $this->trreturpembelian_m->loadgrid4($va);
        //print_r($vdb);
        $dbd = $vdb['db'];
        foreach ($dbd as $key => $dbr) {
            $vaset = $dbr;
            $vaset['cmdpilih'] = '<button type="button" onClick="bos.trreturpembelian.cmdpilihpb(\'' . $dbr['faktur'] . '\')"
                           class="btn btn-success btn-grid">Pilih</button>';
            $vaset['cmdpilih'] = html_entity_decode($vaset['cmdpilih']);
            $vare[] = $vaset;
        }

        $vare = array("total" => count($vare), "records" => $vare);
        echo (json_encode($vare));
    }

    public function pilihpb()
    {
        $va = $this->input->post();
        $this->db->where('faktur', $va['fktpb']);

        $f = "t.faktur,t.tgl,t.gudang,t.supplier,g.keterangan as ketgudang,t.subtotal,t.diskon,t.ppn,t.pembulatan,t.jthtmp,
        t.total,s.nama as namasupplier,t.persppn";
        $dbd = $this->db->select($f)
            ->from("pembelian_total t")
            ->join("gudang g","g.kode = t.gudang","left")
            ->join("supplier s","s.kode = t.supplier","left")
            ->get();
        $data  = $dbd->row_array();
        if(!empty($data)){
            $termin = strtotime($data['jthtmp']) - strtotime($data['jthtmp']);
            if($termin > 0) $termin = devide($termin,(60*60*24));
            $data['tgl'] = date_2d($data['tgl']);
            $data['jthtmp'] = date_2d($data['jthtmp']);
            $data['termin'] = $termin;

            //loadgrid detail
            $vare = array();
            $this->db->where('d.faktur',$va['fktpb']);
            $f2 = "d.stock,s.keterangan as namastock,d.harga,d.qty,s.satuan,d.totalitem as jumlah";
            $dbd2 = $this->db->select($f2)->from("pembelian_detail d")
                    ->join("stock s","s.kode = d.stock","left")
                    ->get();
            foreach($dbd2->result_array() as $dbr){
                $vare[$dbr['stock']] = $dbr ;
            }
            $data['detil'] = $vare;
        }
        echo(json_encode($data));

    }
}
