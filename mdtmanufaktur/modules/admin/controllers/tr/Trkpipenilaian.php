<?php
class Trkpipenilaian extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("tr/trkpipenilaian_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->bdb 	= $this->trkpipenilaian_m ;
        $this->ss  = getsession($this, "username")."-" . "ssrpttriwulan_" ;

    }

    public function index(){
        $arrkolom = array();
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $this->load->view("tr/trkpipenilaian",$data) ;

    } 
    

    public function loadgrid(){
        $va     = json_decode($this->input->post('request'), true) ;
        
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $va['cabang'] = getsession($this, "cabang") ;

        $vare   = array() ;
        $arrjab = array();
        $arrperingkat = array();

        $limit    = $va['offset'].','.$va['limit'] ;
        $search	 = isset( $va['search'][0]['value'] ) ? $va['search'][0]['value'] : '' ;
        $search   = $this->db->escape_like_str( $search ) ;
        $where 	 = array() ;

        if ( $search !== '' ){
            $this->db->group_start();
            $this->db->or_like(array('u.nama'=>$search,'u.kode'=>$search));
            $this->db->group_end();
        } 

        $this->db->group_start();
        $this->db->or_where('u.tglkeluar >=',$va['tglawal']);
        $this->db->or_where('u.tglkeluar','0000-00-00');
        $this->db->group_end();

        if($va['jabatan'] <> "")$this->db->where('(select j.jabatan from karyawan_jabatan j ,
        (select nip,max(tgl) as tglmax
        from karyawan_jabatan
        group by nip) max_perubahan
        where j.tgl=max_perubahan.tglmax
        and j.nip=max_perubahan.nip and j.nip = u.kode) =',$va['jabatan']);

        $f = "count(u.id) jml";
        $dbd = $this->db->select($f)->from("karyawan u")
            ->join("kpi_nilai_total t","t.nip = u.kode and t.periode = '{$va['periode']}' and t.cabang = '{$va['cabang']}' and t.status = '1'","left")
            ->get();
        $rtot  = $dbd->row_array();
        if($rtot['jml'] > 0){
            //lihat data peringkat
            //cari peringkat
            
            $f2 = "kode,keterangan,nilai_min,nilai_max,nominal";
            $this->db->where('status',"1");
            $return['peringkat'] = array();
            $dbd2 = $this->db->select($f2)->from("kpi_peringkat")->get();
            foreach($dbd2->result_array() as $r2){
                $arrperingkat[$r2['kode']] = $r2;
            }

            if ( $search !== '' ){
                $this->db->group_start();
                $this->db->or_like(array('u.nama'=>$search,'u.kode'=>$search));
                $this->db->group_end();
            } 
            $this->db->group_start();
            $this->db->or_where('u.tglkeluar >=',$va['tglawal']);
            $this->db->or_where('u.tglkeluar =',"0000-00-00");
            $this->db->group_end();

            if($va['jabatan'] <> "")$this->db->having('jabatan',$va['jabatan']);

            $field = "u.kode,u.nama,u.alamat,u.telepon,t.status,t.faktur,t.posting,t.nilai,t.peringkat,t.nominal, (select j.jabatan from karyawan_jabatan j ,
                (select nip,max(tgl) as tglmax
                from karyawan_jabatan
                group by nip) max_perubahan
                where j.tgl=max_perubahan.tglmax
                and j.nip=max_perubahan.nip and j.nip = u.kode) as jabatan";
            $dbd = $this->db->select($field)->from("karyawan u")
                ->join("kpi_nilai_total t","t.nip = u.kode and t.periode = '{$va['periode']}'  and t.cabang = '{$va['cabang']}' and t.status = '1'","left")
                ->limit($limit)
                ->get();
            foreach($dbd->result_array() as $dbr){
                $vs = $dbr;   
                if(!isset($arrjab[$dbr['jabatan']]))$arrjab[$dbr['jabatan']] = $this->bdb->getval("keterangan", "kode = '{$dbr['jabatan']}'", "jabatan");
                // if(!empty($dbr['peringkat'])){
                //     if(!isset($peringkat[$dbr['peringkat']]))$peringkat[$dbr['peringkat']] = $this->bdb->getval("keterangan", "kode = '{$dbr['peringkat']}'", "kpi_peringkat");
                //     $vs['peringkat'] = $peringkat[$dbr['peringkat']];
                // }
                $vs['recid'] = $dbr['kode'];
                $vs['jabatan'] = $arrjab[$dbr['jabatan']];
                if($dbr['posting'] <> "1"){
                    $vs['cmddetail']    = '<button type="button" onClick="bos.trkpipenilaian.cmddetail(\''.$dbr['kode'].'\')"
                                class="btn btn-primary btn-grid"><i class="fa fa-tasks"></i></button>' ;
                    $vs['cmddetail']	   = html_entity_decode($vs['cmddetail']) ;

                    foreach($arrperingkat as $key => $val){
                        if($val['nilai_min'] <= $dbr['nilai'] && $val['nilai_max'] >= $dbr['nilai']){
                            $vs['peringkat'] = $val['keterangan'];
                            $vs['nominal'] = $val['nominal'];
                            break;
                        }
                    }
                }else{
                    $vs['peringkat'] = $arrperingkat[$dbr['peringkat']]['keterangan'];
                    $vs['nominal'] = $arrperingkat[$dbr['peringkat']]['nominal'];
                }

                if($dbr['status'] == "1"){
                    $vs['cmdcetak']    = '<button type="button" onClick="bos.trkpipenilaian.cmdcetak(\''.$dbr['kode'].'\')"
                                class="btn btn-danger btn-grid"><i class="fa fa-file-pdf-o"></i></button>' ;
                    $vs['cmdcetak']	   = html_entity_decode($vs['cmdcetak']) ;
                }
                
                $vare[]		= $vs ; 
            }

        }

        

        $vare 	= array("total"=>$rtot['jml'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function loadgridpenilaian(){
        $va = $this->input->post();
        $vare = array();
        // print_ar($va);

        $this->db->where('u.kode',$va['nip']);
        $f = "u.kode,u.nama,u.alamat";
        $dbd = $this->db->select($f)->from("karyawan u")->get();
        $r  = $dbd->row_array();
        // print_r($r);
        if(!empty($r)){

            $bln = date("m",strtotime($va['tglakhir']));
            $arrdata = array();

            //ambil data penilaian
            if($va['faktur'] <> 'null'){
                $this->db->where('kpi_penilaian',$va['penilaian']);
                $this->db->where('faktur',$va['faktur']);

                $f2 = "kpi,nilai";
                $dbd2 = $this->db->select($f2)->from("kpi_nilai_detil")
                    ->get();
                foreach($dbd2->result_array() as $r2){
                    $arrdata[$r2['kpi']] = $r2;
                }
            }

            $arrj = $this->perhitunganhrd_m->getjabatan($r['kode'],$va['tglakhir']);

            $this->db->where('status',"1");
            $this->db->where('penilaian',$va['penilaian']);
            if($va['penilaian'] == "1"){
                $this->db->where('jabatan',$arrj['jabatan']);
            }

            $this->db->group_start();
            $this->db->or_where('periode',"B");

            if($bln % 3 == 0)$this->db->or_where('periode',"3");
            if($bln % 6 == 0)$this->db->or_where('periode',"S");
            if($bln == 12)$this->db->or_where('periode',"T");

            $this->db->group_end();
            $f2 = "kode,keterangan,periode,perhitungan";
            $dbd2 = $this->db->select($f2)->from("kpi_indikator")
                ->get();
            foreach($dbd2->result_array() as $r2){
                if($r2['periode'] <> "")$r2['periode'] = getperiode($r2['periode']);
                
                $vare[$r2['kode']] = $r2;
                $vare[$r2['kode']]['nilai'] = 0 ;
                $vare[$r2['kode']]['perhitungan'] = getperhitungankpi($r2['perhitungan']);
                
                if($r2['perhitungan'] == 0){
                    $vare[$r2['kode']]['nilai'] = (isset($arrdata[$r2['kode']])) ? $arrdata[$r2['kode']]['nilai'] : 0;
                }else{
                    $capaian = $this->getpenilaiankpi($va['nip'],$r2['perhitungan'],$va['tglawal'],$va['tglakhir']);
                    $vare[$r2['kode']]['nilai'] = devide($capaian['pers_pecapaian'],10);
                }

                
                
            }
        }

        $vare = json_encode($vare);

        echo($vare);
    }

    public function init(){
        savesession($this, "sstrkpipenilaian_kode", "") ;
    }

    public function getfaktur(){
        $va 	= $this->input->post() ;
        $tgl = date("Y-m-d");
        $va['cabang'] = getsession($this, "cabang") ;
        $faktur  = $this->trkpipenilaian_m->getfaktur($va['cabang'],$tgl,FALSE) ;

        echo($faktur) ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $error = "" ;
        
        if($error == ""){
            $va['cabang'] = getsession($this, "cabang") ;
            $error = "ok";

            //print_r($va['gr2']);
        
            if($va['faktur'] == 'null') $va['faktur'] = $this->bdb->getfaktur($va['cabang'],$va['tglakhir'],true);
            //data detail simpan

            $this->bdb->delete("kpi_nilai_detil", "faktur = '{$va['faktur']}'" ) ;

            //simp penilaian umum
            $jml_umum = 0;
            $kpi_umum = 0;
           // if(!empty($va['gr2'])){
                $vagr2 = json_decode($va['gr2']);
                foreach($vagr2 as $key => $val){
                    $kpi_umum++;
                    $jml_umum += string_2n($val->nilai);
                    $vakpi = $this->bdb->getval("penilaian,periode", "kode = '{$val->kode}'", "kpi_indikator","","","");
                    $data = array("faktur"=>$va['faktur'],"kpi"=>$val->kode,"kpi_penilaian"=>"0","kpi_periode"=>$vakpi['periode'],"nilai"=>string_2n($val->nilai));
                    $this->bdb->insert("kpi_nilai_detil",$data);
                }
           // }
            $nilai_umum = devide($jml_umum,$kpi_umum);

            //simp penilaian khusus
            $jml_khusus = 0;
            $kpi_khusus = 0;
            
            //if(!empty($va['gr3'])){
                $vagr3 = json_decode($va['gr3']);
                foreach($vagr3 as $key => $val){
                    $kpi_khusus++;
                    $jml_khusus += string_2n($val->nilai);
                    $vakpi = $this->bdb->getval("penilaian,periode", "kode = '{$val->kode}'", "kpi_indikator","","","");
                    $data = array("faktur"=>$va['faktur'],"kpi"=>$val->kode,"kpi_penilaian"=>"1","kpi_periode"=>$vakpi['periode'],"nilai"=>string_2n($val->nilai));
                    $this->bdb->insert("kpi_nilai_detil",$data);
                }
            //}
            $nilai_khusus = devide($jml_khusus,$kpi_khusus);

            $nilai = $jml_umum + $jml_khusus;
            $kpi = $kpi_khusus + $kpi_umum;
            $nilai = devide($nilai,$kpi); 

            

            $vaj = $this->perhitunganhrd_m->getjabatan($va['d_nip'],$va['tglakhir']);

            $data = array("faktur"=>$va['faktur'],"cabang"=>$va['cabang'],"nip"=>$va['d_nip'],"periode"=>$va['periode'],"tgl"=>date_2s($va['tglakhir']),
                "jml_umum"=>$jml_umum,"kpi_umum"=>$kpi_umum,"nilai_umum"=>$nilai_umum,
                "jml_khusus"=>$jml_khusus,"kpi_khusus"=>$kpi_khusus,"nilai_khusus"=>$nilai_khusus,
                "nilai"=>$nilai,"jabatan"=>$vaj['jabatan']);
            $this->bdb->update("kpi_nilai_total",$data,"faktur = '{$va['faktur']}'");

        }
        echo($error);
        
    }

    public function posting(){
        $va 	= $this->input->post() ;
        $error = "" ;

        if(!isset($va['periode'])) $error = "Periode tidak valid !!!";

        if($error == ""){
            $va['cabang'] = getsession($this, "cabang") ;

            //cari peringkat
            $arrperingkat = array();
            $f2 = "kode,keterangan,nilai_min,nilai_max,nominal";
            $this->db->where('status',"1");
            $return['peringkat'] = array();
            $dbd2 = $this->db->select($f2)->from("kpi_peringkat")->get();
            foreach($dbd2->result_array() as $r2){
                $arrperingkat[$r2['kode']] = $r2;
            }

            $this->db->where('t.periode',$va['periode']);
            $this->db->where('t.cabang',$va['cabang']);
            $this->db->where('t.status','1');
            $this->db->where('t.posting','0');
            $f = "t.*";
            $dbd = $this->db->select($f)->from("kpi_nilai_total t")
                ->get();
            foreach($dbd->result_array() as $r){
                $peringkat = "";
                $nominal = 0;
                foreach($arrperingkat as $key => $val){
                    if($val['nilai_min'] <= $r['nilai'] && $val['nilai_max'] >= $r['nilai']){
                        $peringkat = $val['kode'];
                        $nominal = $val['nominal'];
                        break;
                    }
                }
                $data = array("posting"=>"1","nominal"=>$nominal,"peringkat"=>$peringkat);
                $this->bdb->edit("kpi_nilai_total",$data,"faktur = '{$r['faktur']}'");
            }


            $error = "ok";
        }
        echo($error);
    }

    public function batalposting(){
        $va 	= $this->input->post() ;
        $error = "" ;
        
        if(!isset($va['periode'])) $error = "Periode tidak valid !!!";

        if($error == ""){
            $va['cabang'] = getsession($this, "cabang") ;

            $this->db->where('periode',$va['periode']);
            $this->db->where('cabang',$va['cabang']);
            $this->db->where('status','1');
            $this->db->where('posting','1');
            
            $this->db->set('peringkat','');
            $this->db->set('posting','0');
            $this->db->set('nominal',0.00);

            $this->db->update("kpi_nilai_total");

            $error = "ok";
        }
        echo($error);
    }

    public function detail(){
        $va 	= $this->input->post() ;
        $return = array();

        $this->db->where('u.kode',$va['kode']);
        $f = "u.kode,u.nama,u.alamat,t.faktur";
        $dbd = $this->db->select($f)->from("karyawan u")
            ->join("kpi_nilai_total t","t.nip = u.kode and t.periode = '{$va['periode']}' and t.status = '1'","left")
            ->get();
        $r  = $dbd->row_array();
        if(!empty($r)){
            $return = $r;
            $arrj = $this->perhitunganhrd_m->getjabatan($r['kode'],$va['tglakhir']);
            $return['jabatan'] = $arrj['keterangan'];
           
            $f2 = "kode,keterangan,nilai_min,nilai_max";
            $this->db->where('status',"1");
            $return['peringkat'] = array();
            $dbd2 = $this->db->select($f2)->from("kpi_peringkat")->get();
            foreach($dbd2->result_array() as $r2){
                $return['peringkat'][$r2['kode']] = $r2;
            }

        }
        echo(json_encode($return));
    }

    public function initreport_detil(){
        $n=0;
        $va     = $this->input->post() ;
        savesession($this, $this->ss . "va", json_encode($va) ) ;
        echo(' bos.trkpipenilaian.cmdcetak_showreport() ; ') ;
    }
    public function showreport_detil(){
        $va   = json_decode(getsession($this, $this->ss . "va", "{}"), true) ;
        $return = array();

        $this->db->where('t.nip',$va['kode']);
        $this->db->where('t.periode',$va['periode']);
        $this->db->where('t.status',"1");
        $f = "t.faktur,t.peringkat,t.nominal,t.posting,t.jabatan,t.jml_umum,t.kpi_umum,t.nilai_umum,t.tgl,
            t.jml_khusus,t.kpi_khusus,t.nilai_khusus,t.nilai,t.nominal,t.nip,u.nama,u.alamat,j.keterangan as ketjabatan,p.keterangan as ketperingkat";
        $dbd = $this->db->select($f)->from("kpi_nilai_total t")
            ->join("karyawan u","u.kode = t.nip","left")
            ->join("jabatan j","j.kode = t.jabatan","left")
            ->join("kpi_peringkat p","p.kode = t.peringkat","left")
            ->get();
        $r  = $dbd->row_array();
        if(!empty($r)){
            $vahead = array();
            $vahead[] = array("1"=>"NIP","2"=>":","3"=>$r['nip'],"4"=>"","5"=>"Tgl Penilaian","6"=>":","7"=>date_2d($r['tgl']));
            $vahead[] = array("1"=>"Nama","2"=>":","3"=>$r['nama'],"4"=>"","5"=>"Jabatan","6"=>":","7"=>$r['ketjabatan']);

            $vakpi0 = array();
            $vakpi1 = array();

            $f2 = "d.kpi,d.kpi_penilaian,d.kpi_periode,d.nilai,i.keterangan";
            $this->db->where('d.kpi_penilaian',"0");
            $this->db->where('d.faktur',$r['faktur']);
            $dbd2 = $this->db->select($f2)->from("kpi_nilai_detil d")
                ->join("kpi_indikator i","i.kode = d.kpi","left")
                ->get();
            foreach($dbd2->result_array() as $r2){
                $vakpi0[] = array("kode"=>$r2['kpi'],"keterangan"=>$r2['keterangan'],"periode"=>getperiode($r2['kpi_periode']),"nilai"=>string_2s($r2['nilai']));
            }
            
            if(!empty($vakpi0)){
                $vakpi0_sub = array();
                $vakpi0_sub[] = array("ket"=>"<b>Jumlah Nilai","nilai"=>string_2s($r['jml_umum'])."</b>"); 
                $vakpi0_sub[] = array("ket"=>"<b>Jumlah KPI","nilai"=>string_2s($r['kpi_umum'])."</b>"); 
                $vakpi0_sub[] = array("ket"=>"<b>Rata-rata Nilai","nilai"=>string_2s($r['nilai_umum'])."</b>"); 
            }   
            

            $f2 = "d.kpi,d.kpi_penilaian,d.kpi_periode,d.nilai,i.keterangan";
            $this->db->where('d.kpi_penilaian',"1");
            $this->db->where('d.faktur',$r['faktur']);
            $dbd2 = $this->db->select($f2)->from("kpi_nilai_detil d")
                ->join("kpi_indikator i","i.kode = d.kpi","left")
                ->get();
            foreach($dbd2->result_array() as $r2){
                $vakpi1[] = array("kode"=>$r2['kpi'],"keterangan"=>$r2['keterangan'],"periode"=>getperiode($r2['kpi_periode']),"nilai"=>string_2s($r2['nilai']));
            }

            if(!empty($vakpi1)){
                $vakpi1_sub = array();
                $vakpi1_sub[] = array("ket"=>"<b>Jumlah Nilai","nilai"=>string_2s($r['jml_khusus'])."</b>"); 
                $vakpi1_sub[] = array("ket"=>"<b>Jumlah KPI","nilai"=>string_2s($r['kpi_khusus'])."</b>"); 
                $vakpi1_sub[] = array("ket"=>"<b>Rata-rata Nilai","nilai"=>string_2s($r['nilai_khusus'])."</b>"); 
            }  

            $font = 10 ;
            $o    = array('paper'=>'A4', 'orientation'=>'portrait',  'export'=>(isset($va['export']) ? $va['export'] : 0),
                          'opt'=>array('export_name'=>'kpi_detil_' . getsession($this, "username") ) ) ;
            $this->load->library('bospdf', $o) ;   
            $this->bospdf->ezText("<b>Penilaian KPI</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ; 
            $this->bospdf->ezTable($vahead,"","",  
                                   array("showHeadings"=>0,"showLines"=>0,"fontSize"=>$font+2,"cols"=>
                                   array("1"=>array("width"=>10),
                                   "2"=>array("width"=>5,"justification"=>"center"),
                                   "3"=>array("justification"=>"left"),
                                   "4"=>array("width"=>10),
                                   "5"=>array("width"=>10),
                                   "6"=>array("width"=>5,"justification"=>"center"),
                                   "7"=>array("justification"=>"left")
                                   ))) ;   
            $this->bospdf->ezText("") ;

            $this->bospdf->ezTable($vakpi0,"","",  
                                array("showHeadings"=>1,"showLines"=>1,"fontSize"=>$font,"cols"=> array(
                                "kode"=>array("caption"=>"Kode","width"=>7,"justification"=>"center"),
                                "keterangan"=>array("caption"=>"Penilaian Umum"),
                                "periode"=>array("caption"=>"Periode","width"=>10,"justification"=>"center"),
                                "nilai"=>array("caption"=>"Nilai","width"=>10,"justification"=>"right")
                                ))) ; 
            $this->bospdf->ezTable($vakpi0_sub,"","",  
                                array("showHeadings"=>0,"showLines"=>2,"fontSize"=>$font,"cols"=> array(
                                "ket"=>array("caption"=>"Penilaian Umum","justification"=>"center"),
                                "nilai"=>array("caption"=>"Nilai","width"=>10,"justification"=>"right")
                                ))) ; 

            $this->bospdf->ezText("") ;
            
            $this->bospdf->ezTable($vakpi1,"","",  
                                array("showHeadings"=>1,"showLines"=>1,"fontSize"=>$font,"cols"=> array(
                                "kode"=>array("caption"=>"Kode","width"=>7,"justification"=>"center"),
                                "keterangan"=>array("caption"=>"Penilaian Khusus"),
                                "periode"=>array("caption"=>"Periode","width"=>10,"justification"=>"center"),
                                "nilai"=>array("caption"=>"Nilai","width"=>10,"justification"=>"right")
                                ))) ; 
            $this->bospdf->ezTable($vakpi1_sub,"","",  
                                array("showHeadings"=>0,"showLines"=>2,"fontSize"=>$font,"cols"=> array(
                                "ket"=>array("caption"=>"Penilaian Umum","justification"=>"center"),
                                "nilai"=>array("caption"=>"Nilai","width"=>10,"justification"=>"right")
                                ))) ; 
            $this->bospdf->ezText("") ;
            
            if($r['posting'] == "0"){
                $r['ketperingkat'] = "Tidak masuk peringkat";
                $r['nominal'] = 0;

                $f2 = "kode,keterangan,nilai_min,nilai_max,nominal";
                $this->db->where('status',"1");
                $dbd2 = $this->db->select($f2)->from("kpi_peringkat")->get();
                foreach($dbd2->result_array() as $r2){
                    if($r2['nilai_min'] <= $r['nilai'] && $r2['nilai_max'] >= $r['nilai']){
                        $r['nominal'] = $r2['nominal'];
                        $r['ketperingkat'] = $r2['keterangan'];
                        break;
                    }
                }
            }
            $this->bospdf->ezText("Nilai KPI :".$r['nilai']." [".$r['ketperingkat']."] Reward Rp. ".string_2s($r['nominal']),$font+7) ;
            // $this->bospdf->ezTable($vare2,"","",  
            //                        array("showHeadings"=>0,"showLines"=>0,"fontSize"=>$font,"cols"=>$arrkolom2)) ;   
            // $this->bospdf->ezTable($vare3,"","",  
            //                        array("showHeadings"=>0,"showLines"=>2,"fontSize"=>$font,"cols"=>$arrkolom3)) ;   
            
            // $this->bospdf->ezText("") ;
            // $this->bospdf->ezText("<b>ANALISA RATIO KEUANGAN </b>") ;
            // $this->bospdf->ezTable($vare4,"","",  
            //                        array("showHeadings"=>0,"showLines"=>0,"fontSize"=>$font,"cols"=>$arrkolom4)) ;   
            $this->bospdf->ezStream() ; 
        }else{
            echo('data kosong') ;
        }
    }

    public function loadperiode(){
        $va 	= $this->input->post() ;
        $cabang = getsession($this, "cabang");
        $return = array();
        $this->db->where('kode',$va['kode']);
        $f = "kode,keterangan,tglawal,tglakhir";
        $dbd = $this->db->select($f)->from("sys_periode_payroll")->get();
        $r  = $dbd->row_array();
        if(!empty($r)){
            $return['status'] = "0";
            $return['tglawal'] = date_2d($r['tglawal']);
            $return['tglakhir'] = date_2d($r['tglakhir']);

            //cek payroll posting
            $this->db->where('periode',$va['kode']);
            $this->db->where('cabang',$cabang);
            $this->db->where('status',"1");
            $dbd2 = $this->db->select("faktur")->from("payroll_posting")->get();
            $r2  = $dbd2->row_array();
            if(!empty($r2)){
                $return['status'] = "1";//payroll sudah posting
            }else{
                //cek kpi posting
                $this->db->where('periode',$va['kode']);
                $this->db->where('cabang',$cabang);
                $this->db->where('status',"1");
                $this->db->where('posting',"1");
                $dbd3 = $this->db->select("faktur")->from("kpi_nilai_total")->get();
                $r3  = $dbd3->row_array();
                if(!empty($r3)){
                    $return['status'] = "2";//kpi sudah posting
                }
            }           

        }

        echo(json_encode($return));
    }

    function getpenilaiankpi($nip,$perhitungan,$tglawal,$tglakhir){
        $return = array("pencapaian"=>0,"target"=>0,"pers_pecapaian"=>0);

        $tglawal = date_2s($tglawal);
        $tglakhir = date_2s($tglakhir);

        switch ($perhitungan) {
            
            case 3://produksi
                $this->db->where('tgl >= ',$tglawal);
                $this->db->where('tgl <= ',$tglakhir);
                $this->db->where('nip',$nip);
                $f = "ifnull(sum(target),0) target,ifnull(sum(pencapaian),0) pencapaian";
                $dbd = $this->db->select($f)->from("produksi_pencapaian")->get();
                $r  = $dbd->row_array();
                if(!empty($r)){
                    $return['pencapaian'] = $r['pencapaian'];
                    $return['target'] = $r['target'];
                }
                
                break;

            case 1:

                $this->db->where('tglfinger >= ',$tglawal);
                $this->db->where('tglfinger <= ',$tglakhir);
                $this->db->where('status',"1");
                $this->db->where('mode',"1");
                $this->db->where('nip',$nip);
                $this->db->where('kodeabsensi',$this->bdb->getconfig("kodeabsmasuk"));
                $f = "ifnull(count(id),0) as jml";
                $dbd = $this->db->select($f)->from("absensi_karyawan")->get();
                $r  = $dbd->row_array();
                if(!empty($r)){
                   $return['target'] = $r['jml'];
                }


                $this->db->where('tglfinger >= ',$tglawal);
                $this->db->where('tglfinger <= ',$tglakhir);
                $this->db->where('status',"1");
                $this->db->where('selisihabsensidtk',0);
                $this->db->where('mode',"1");
                $this->db->where('nip',$nip);
                $this->db->where('kodeabsensi',$this->bdb->getconfig("kodeabsmasuk"));
                $f = "ifnull(count(id),0) as jml";
                $dbd = $this->db->select($f)->from("absensi_karyawan")->get();
                $r  = $dbd->row_array();
                if(!empty($r)){
                   $return['pencapaian'] = $r['jml'];
                }

                
                break;

            case 2:

                $this->db->where('tglfinger >= ',$tglawal);
                $this->db->where('tglfinger <= ',$tglakhir);
                $this->db->where('status',"1");
                $this->db->where('mode',"1");
                $this->db->where('nip',$nip);
                $f = "ifnull(count(id),0) as jml";
                $dbd = $this->db->select($f)->from("absensi_karyawan")->get();
                $r  = $dbd->row_array();
                if(!empty($r)){
                   $return['target'] = $r['jml'];
                }


                $this->db->where('tglfinger >= ',$tglawal);
                $this->db->where('tglfinger <= ',$tglakhir);
                $this->db->where('status',"1");
                $this->db->where('mode',"1");
                $this->db->where('nip',$nip);
                $this->db->where('kodeabsensi',$this->bdb->getconfig("kodeabsmasuk"));
                $f = "ifnull(count(id),0) as jml";
                $dbd = $this->db->select($f)->from("absensi_karyawan")->get();
                $r  = $dbd->row_array();
                if(!empty($r)){
                   $return['pencapaian'] = $r['jml'];
                }

                break;

        }

        $return['pers_pecapaian'] = devide($return['pencapaian'],$return['target']) * 100;

        return $return;
    }

}
?>
