<?php
class Trprosesproduksi extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('func/updtransaksi_m') ;
        $this->load->model('func/func_m') ;
        $this->load->model('tr/trprosesproduksi_m') ;
        $this->load->helper('bdate');
        $this->bdb = $this->trprosesproduksi_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(),"mintgl"=>$tglmin) ;
        $this->load->view('tr/trprosesproduksi',$d) ;
    }

    public function loadgrid_where($bs, $s){
        $this->duser();
        
        if($bs['skd_cabang'] !== 'null'){
            $bs['skd_cabang'] = json_decode($bs['skd_cabang'],true);
            $this->db->group_start();
            foreach($bs['skd_cabang'] as $kdc){
                $this->db->or_where('t.cabang',$kdc);
            }
            $this->db->group_end();

        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['cabang'])){
                    $this->db->group_start();
                    foreach($this->aruser['data_var']['cabang'] as $kdc){
                        $this->db->or_where('t.cabang', $kdc);
                    }
                    $this->db->group_end();
                }
            }
        }

        if($s !== ''){
            $this->db->group_start();
                $this->db->or_like(array('t.faktur'=>$s));
            $this->db->group_end();
        }

        $tgl = date("Y-m-d");

        $this->db->where('t.status',"1");
        $this->db->group_start();
        $this->db->or_where('t.tglclose >=', $tgl);
        $this->db->or_where('t.tglclose',"0000-00-00");
        $this->db->group_end();

    }

    public function loadgrid(){
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare = array();
        $vkaru = array();

        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->db->escape_like_str($search) ;
        $this->loadgrid_where($va, $search);
        $f = "count(t.id) jml";
        $dbdt = $this->db->select($f)
            ->from("produksi_total t")
            ->join("cabang c","c.kode = t.cabang","left")
            ->get();
        $rtot  = $dbdt->row_array();
        if($rtot['jml'] > 0){
            $this->loadgrid_where($va, $search);
            $f2    = "t.faktur,t.tgl,t.cabang,t.regu,t.petugas";
            $dbd = $this->db->select($f2)
                ->from("produksi_total t")
                ->join("cabang c","c.kode = t.cabang","left")
                ->order_by('t.faktur ASC')
                ->limit($limit)
                ->get();
        
            foreach($dbd->result_array() as $dbr){ 
                $vaset                  = $dbr ;
                $vaset['tgl'] = date_2d($vaset['tgl']);
                $vaset['karu'] = $dbr['petugas'];
                if($dbr['regu'] !== null){
                    $regu = json_decode($dbr['regu'],true);
                    $vaset['regu'] = $regu['keterangan'];
                    $vkaru[$regu['karu']] = $this->bdb->getval("nama", "kode = '{$regu['karu']}'", "karyawan");
                    $vaset['karu'] = $vkaru[$regu['karu']];

                }
                
                $vaset['cmdedit']       = '<button type="button" onClick="bos.trprosesproduksi.cmdedit(\''.$dbr['faktur'].'\')"
                                            class="btn btn-success btn-grid">Proses</button>' ;
                $vaset['cmdedit']       = html_entity_decode($vaset['cmdedit']) ;
                $vare[]                 = $vaset ;
            }
        }

        $vare 	= array("total"=>$rtot['jml'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "ssprosesproduksi_faktur", "") ;
    }

    public function saving(){
        $va 	     = $this->input->post() ;
        $kode 	     = getsession($this, "ssprosesproduksi_faktur") ;
        $va['tgl']   = date_2s($va['tgl']) ;

        $this->trprosesproduksi_m->saving($kode, $va) ;
        echo(' bos.trprosesproduksi.settab(0) ;  ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $faktur = $va['faktur'] ;

        $this->db->where('t.faktur',$faktur);
        $f = "t.faktur,t.tgl,t.cabang,c.keterangan as ketcabang,t.perbaikan,t.petugas,t.regu";
        $dbdt = $this->db->select($f)
            ->from("produksi_total t")
            ->join("cabang c","c.kode = t.cabang","left")
            ->get();
        $data  = $dbdt->row_array();
        if(!empty($data)){
            savesession($this, "ssprosesproduksi_faktur", $faktur) ;
            $data['tgl'] = date_2d($data['tgl']);

            $data['karu'] = $data['petugas'];
            if($data['regu'] !== null){
                $regu = json_decode($data['regu'],true);
                $data['karu'] = $this->bdb->getval("nama", "kode = '{$regu['karu']}'", "karyawan");
            }
            //loadgrid detail
            $vare = array();

            $this->db->where('p.fakturproduksi',$faktur);

            $field = "p.stock,s.keterangan as namastock,p.qty,s.satuan";
            $dbd =$this->db->select($field)
                ->from("produksi_produk p")
                ->join("stock s","s.kode = p.stock","left")
                ->get();
            foreach($dbd->result_array() as $dbr){ 
                $vare[] = $dbr ;
            }
            $data['detil'] = $vare;
            
        }
        echo(json_encode($data));

    }

    public function deletebb(){
        $va 	= $this->input->post() ;
        $this->trprosesproduksi_m->deletebb($va['faktur']) ;
        echo('bos.trprosesproduksi.grid2_reloaddata() ; ') ;

    }

    public function getfaktur(){
        $faktur  = $this->trprosesproduksi_m->getfaktur(FALSE) ;

        echo('
        bos.trprosesproduksi.obj.find("#faktur").val("'.$faktur.'") ;
        ') ;
    }
    
    public function loadgrid2(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->trprosesproduksi_m->loadgrid2($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;
        while( $dbr = $this->trprosesproduksi_m->getrow($dbd) ){
            $n++;
            $vaset   = $dbr ;
            $vaset['no'] = $n;
            $vaset['cmddelete']    = '<button type="button" onClick="bos.trprosesproduksi.grid2_deleterow(\''.$dbr['faktur'].'\')"
                           class="btn btn-danger btn-grid">delete</button>' ;
            $vaset['cmddelete']	   = html_entity_decode($vaset['cmddelete']) ;
            $vare[]		= $vaset ;
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }
    
    public function savingbb(){
        $va 	     = $this->input->post() ;
        $va['tgl']   = date_2s($va['tgl']) ;

        $this->trprosesproduksi_m->savingbb($va) ;
        echo('
            bos.trprosesproduksi.initdetail();
            bos.trprosesproduksi.obj.find("#stock").focus() ;
            bos.trprosesproduksi.grid2_reloaddata() ;
        ') ;
    }
    
    public function autoprosesbb(){
        $va 	     = $this->input->post() ;
        $this->trprosesproduksi_m->savingbbautospp($va) ;
        echo('
            bos.trprosesproduksi.initdetail();
            bos.trprosesproduksi.obj.find("#stock").focus() ;
            bos.trprosesproduksi.grid2_reloaddata() ;
        ') ;
    }

}
?>
