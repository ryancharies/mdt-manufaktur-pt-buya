<?php
class Trthr extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("tr/trthr_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->load->model("func/updtransaksi_m") ;
        $this->bdb 	= $this->trthr_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";


        $arrkolom = array();
        $arrkolom[] = array("field"=> "kode", "caption"=> "NIP", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "nama", "caption"=> "Nama", "size"=> "200px", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "bagian", "caption"=> "Bagian", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "jabatan", "caption"=> "Jabatan", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        

        $arrkolom[] = array("field"=> "thr", "caption"=> "THR", "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        $data['kolomgrid1'] = json_encode($arrkolom);
        $this->load->view("tr/trthr",$data) ;

    } 

   
    public function loadgrid(){
        $va = json_decode($this->input->post('request'), true);
        //print_r($va);
        $vare   = array() ;
        $vdb    = $this->trthr_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;
        
        //cek status sudah posting atau belum
        $sudahposting = false;
        $faktur = "";
        $cabang = getsession($this, "cabang");
        $va['tgl'] = date_2s($va['tgl']);
        $dbd2 = $this->trthr_m->select("thr_posting","faktur","tgl = '{$va['tgl']}' and cabang = '$cabang' and status = '1'");
        if($dbr2 = $this->trthr_m->getrow($dbd2)){
            $sudahposting = true;
            $faktur = $dbr2['faktur'];
        }

        $total = array();
        $total['recid'] = "ZZZZZ" ;
        $total['nama'] = "Total" ;
        $total['thr'] = 0 ;
        
        while( $dbr = $this->trthr_m->getrow($dbd) ){
            $n++;

            //jab bag
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$va['tgl']);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['kode'],$va['tgl']);
            
            $vaset = $dbr;
            $gajikotor = 0 ;
            $vaset['jabatan'] = $jabatan['keterangan'];
            $vaset['bagian'] = $bagian['keterangan'];
            if($sudahposting){
                $thr = $this->perhitunganhrd_m->getthrkaryawanposting($dbr['kode'],$va['tgl']);
            }else{
                $thr = $this->perhitunganhrd_m->getthrkaryawan($dbr['kode'],$va['tgl']);
            }

           // print_r($thr);
            $vaset['thr'] = $thr['total'];
            $vare[]		= $vaset ;

            $total['thr'] += $vaset['thr'];
        }
        $total['w2ui']= array("summary"=> true);
        $vare[] = $total;

        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function init(){
        savesession($this, "sstrthr_kode", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $error = $this->bdb->saving($va) ;
        if($error == ""){
            echo(' 
                bos.trthr.init() ;  
                bos.trthr.loadperiode();
                alert("THR berhasil diposting.....");
                ') ;
        }else{
            echo('
                alert("'.$error.'");
                
                ') ;
        }
        
    }

   public function loadperiode(){
        $va 	= $this->input->post() ;
        $status = "Belum diposting";
        $faktur = $this->trthr_m->getfaktur(false);
        $cabang = getsession($this, "cabang");
        $va['tgl'] = date_2s($va['tgl']);
        $dbd2 = $this->trthr_m->select("thr_posting","faktur","tgl = '{$va['tgl']}' and cabang = '$cabang' and status = '1'");
        if($dbr2 = $this->trthr_m->getrow($dbd2)){
            $status = "Sudah diposting";
            $faktur = $dbr2['faktur'];
        }
        
        echo('
            with(bos.trthr.obj){
                find("#status").val("'.$status.'") ;
                find("#faktur").val("'.$faktur.'") ;
            }
        ') ;
            
        if($status == "Sudah diposting"){
            echo('
                with(bos.trthr.obj){
                    find("#cmdposting").attr("disabled", true);
                    find("#cmdbatalposting").attr("disabled", false);
                }
            ') ;                
        }else{
            echo('
                with(bos.trthr.obj){
                    find("#cmdposting").attr("disabled", false);
                    find("#cmdbatalposting").attr("disabled", true);
                }
            ') ;  
        }
   }

   public function seekrekening(){
        $search     = $this->input->get('q');
        $vdb    = $this->trthr_m->seekrekening($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trthr_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] . " - " . $dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

    public function loadpembayaran(){
        $va 	= $this->input->post() ;
        $cabang = getsession($this, "cabang");
        $faktur = "";
        $total = 0;
        $va['tgl'] = date_2s($va['tgl']);
        $dbd2 = $this->trthr_m->select("thr_posting","faktur","tgl = '{$va['tgl']}' and cabang = '$cabang' and status = '1'");
        if($dbr2 = $this->trthr_m->getrow($dbd2)){
            $faktur = $dbr2['faktur'];

            //loadgrid pembayaran
            $vare = array();
            $dbd = $this->trthr_m->getpembayaran($faktur) ;
            $n = 0 ;
            
            while( $dbr = $this->trthr_m->getrow($dbd) ){
                $n++;
                $vaset   = $dbr ;
                $vaset['recid'] = $n;
                $vaset['no'] = $n;
                $vaset['cmddelete'] = '<button type="button" onClick="bos.trthr.grid2_deleterow('.$n.')"
                                        class="btn btn-danger btn-grid">Delete</button>' ;
                $vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

                $vare[]             = $vaset ;
                $total += $dbr['nominal'];
            }

            
        }
        $vare[] = array("recid"=>"ZZZZ","no"=>"","kode"=>"","ketketerangan"=>"Total","nominal"=>$total,"w2ui"=>array("summary"=>true));

        $vare = json_encode($vare);
            echo('
                w2ui["bos-form-trthr_grid2"].add('.$vare.');
                
           
            bos.trthr.initdetail();
        ');
    }

    public function batalposting(){
        $va 	= $this->input->post() ;
        $cabang = getsession($this, "cabang");
        $faktur = "";
        $va['tgl'] = date_2s($va['tgl']);
        $dbd2 = $this->trthr_m->select("thr_posting","faktur","tgl = '{$va['tgl']}' and cabang = '$cabang' and status = '1'");
        if($dbr2 = $this->trthr_m->getrow($dbd2)){
            $faktur = $dbr2['faktur'];

            $this->trthr_m->edit("thr_posting",array("status"=>"2"),"faktur = '$faktur'");
            $this->trthr_m->delete("keuangan_bukubesar","faktur = '$faktur'");
            echo('
                alert("THR berhasil dibatal posting.....");
            ');
        }else{
            echo('
                alert("THR berhasil dibatal posting.....");
            ');
        }

        echo('
            bos.trthr.init() ;  
            
            bos.trthr.loadperiode();

            bos.trthr.loadpembayaran();

            bos.trthr.grid1_destroy() ;
            bos.trthr.grid1_loaddata();
            bos.trthr.grid1_load() ;
        ');
    }
}
?>
