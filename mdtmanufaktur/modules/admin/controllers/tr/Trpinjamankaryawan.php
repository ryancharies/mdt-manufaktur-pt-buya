<?php
class Trpinjamankaryawan extends Bismillah_Controller
{
    private $bdb;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('func/updtransaksi_m');
        $this->load->model('func/perhitunganhrd_m');
        $this->load->model('tr/trpinjamankaryawan_m');
        $this->load->helper('bdate');
        $this->bdb = $this->trpinjamankaryawan_m;
    }

    public function index()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d = array("dnow" => date("d-m-Y"), "setdate" => date_set(),"mintgl"=>$tglmin);
        $this->load->view('tr/trpinjamankaryawan', $d);
    }

    public function loadgrid()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va = json_decode($this->input->post('request'), true);
        $vare = array();
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $tgl = date("Y-m-d");
        $vdb = $this->trpinjamankaryawan_m->loadgrid($va);
        $dbd = $vdb['db'];
        
        while ($dbr = $this->trpinjamankaryawan_m->getrow($dbd)) {
            $vaset = $dbr;
            $recid = $dbr['id'];
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['nip'],$tgl);
            $vaset['jabatan'] = $jabatan['keterangan'];
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['nip'],$tgl);
            $vaset['bagian'] = $bagian['keterangan'];
            $vaset['recid'] = $recid;

            $vaset['tgl'] = date_2d($vaset['tgl']);
            $vaset['tglawalags'] = date_2d($vaset['tglawalags']);
            $vaset['cmdjadwal'] = '<button type="button" onClick="bos.trpinjamankaryawan.cmdjadwal(\'' . $dbr['nopinjaman'] . '\')"
                                        class="btn btn-primary btn-grid">Jadwal</button>';
            $vaset['cmdcetak'] = '<button type="button" onClick="bos.trpinjamankaryawan.cmdcetak(\'' . $dbr['nopinjaman'] . '\')"
                                        class="btn btn-grid">Cetak</button>';
            $vaset['cmdedit'] = '<button type="button" onClick="bos.trpinjamankaryawan.cmdedit(\'' . $dbr['nopinjaman'] . '\')"
                                        class="btn btn-default btn-grid">Edit</button>';
            $vaset['cmddelete'] = '<button type="button" onClick="bos.trpinjamankaryawan.cmddelete(\'' . $dbr['nopinjaman'] . '\')"
                                        class="btn btn-danger btn-grid">Delete</button>';
            
            $vaset['cmdjadwal'] = html_entity_decode($vaset['cmdjadwal']);
            $vaset['cmdcetak'] = html_entity_decode($vaset['cmdcetak']);
            $vaset['cmdedit'] = html_entity_decode($vaset['cmdedit']);
            $vaset['cmddelete'] = html_entity_decode($vaset['cmddelete']);
            if ($tglmin > date_2s($dbr['tgl'])) {
                $vaset['cmdedit'] = "";
                $vaset['cmddelete'] = "";
            }
            $vare[] = $vaset;
        }
        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function init()
    {
        savesession($this, "sspinjamankaryawan_nopinjaman", "");
    }

    public function saving()
    {
        $va = $this->input->post();
        $kode = getsession($this, "sspinjamankaryawan_nopinjaman");

        $this->trpinjamankaryawan_m->saving($kode, $va);
        echo (' bos.trpinjamankaryawan.settab(0) ;  ');
    }

    public function editing()
    {
        $va = $this->input->post();
        $nopinjaman = $va['nopinjaman'];
        $tgl = date("Y-m-d");
        $data = $this->trpinjamankaryawan_m->getdatatotal($nopinjaman);
        if (!empty($data)) {
            savesession($this, "sspinjamankaryawan_nopinjaman", $nopinjaman);
            $bank[] = array("id" => $data['bank'], "text" => $data['ketbank']);
            $jabatan = $this->perhitunganhrd_m->getjabatan($data['nip'],$tgl);
            $bagian = $this->perhitunganhrd_m->getbagian($data['nip'],$tgl);
            echo ('
            with(bos.trpinjamankaryawan.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#nip").val("' . $data['nip'] . '") ;
               find("#namakaryawan").val("' . $data['nama'] . '") ;
               find("#jabatan").val("' . $jabatan['keterangan'] . '") ;
               find("#bagian").val("' . $bagian['keterangan'] . '") ;
               find("#keterangan").val("' . $data['keterangan'] . '") ;

               find("#nopinjaman").val("' . $data['nopinjaman'] . '") ;
               find("#fktrealisasi").val("' . $data['fakturrealisasi'] . '") ;
               find("#plafond").val("' . string_2s($data['plafond']). '") ;
               find("#lama").val("' . string_2s($data['lama']). '") ;
               find("#tglawalags").val("' . date_2d($data['tglawalags']) . '");
               find("#tgl").val("' . date_2d($data['tgl']) . '");
               find("#bank").sval(' . json_encode($bank) . ');
               find("#cmdnip").attr("disabled", true);
               
            }
            bos.trpinjamankaryawan.settab(1) ;
         ');
        }
    }

    public function pilihkaryawan()
    {
        $va = $this->input->post();
        //print_r($va);
        $kode = $va['kode'];
        $data = $this->trpinjamankaryawan_m->getdata($kode);
        $tgl = date("Y-m-d");
        if (!empty($data)) {
            $jabatan = $this->perhitunganhrd_m->getjabatan($data['kode'],$tgl);
            $bagian = $this->perhitunganhrd_m->getbagian($data['kode'],$tgl);
            echo ('
            with(bos.trpinjamankaryawan.obj){
               find("#nip").val("' . $data['kode'] . '") ;
               find("#namakaryawan").val("' . $data['nama'] . '");
               find("#jabatan").val("' . $jabatan['keterangan'] . '");
               find("#bagian").val("' . $bagian['keterangan'] . '");
               bos.trpinjamankaryawan.loadmodelnip("hide");
            }
            bos.trpinjamankaryawan.getnopinjaman();
         ');
        }
    }

    public function deleting()
    {
        $va = $this->input->post();
        $error = $this->trpinjamankaryawan_m->deleting($va['nopinjaman']);
        if($error == ""){
            echo ('bos.trpinjamankaryawan.grid1_reloaddata() ; ');
        }else{
            echo('
                alert("Data tidak bisa di hapus : \\n'.$error.'");
            ');
        }
        

    }

    

    public function getfaktur()
    {
        
    
        $faktur = $this->trpinjamankaryawan_m->getfaktur(false);

        echo ('
        bos.trpinjamankaryawan.obj.find("#fktrealisasi").val("' . $faktur . '") ;
        ');
    }

    public function getnopinjaman()
    {
        $va = $this->input->post();
        $nopinjamnan = $this->trpinjamankaryawan_m->getnopinjaman($va['nip'],false);

        echo ('
        bos.trpinjamankaryawan.obj.find("#nopinjaman").val("' . $nopinjamnan . '") ;
        ');
    }

    public function loadgrid3()
    {
        $va = json_decode($this->input->post('request'), true);
        $vare = array();
        $vdb = $this->trpinjamankaryawan_m->loadgrid3($va);
        $dbd = $vdb['db'];
        $tgl = date("Y-m-d");
        while ($dbr = $this->trpinjamankaryawan_m->getrow($dbd)) {
            $vaset = $dbr;
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$tgl);
            $vaset['jabatan'] = $jabatan['keterangan'];
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['kode'],$tgl);
            $vaset['bagian'] = $bagian['keterangan'];
            
            $vaset['cmdpilih'] = '<button type="button" onClick="bos.trpinjamankaryawan.cmdpilih(\'' . $dbr['kode'] . '\')"
                           class="btn btn-success btn-grid">Pilih</button>';
            $vaset['cmdpilih'] = html_entity_decode($vaset['cmdpilih']);
            $vare[] = $vaset;
        }

        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function seekbank(){
        $search     = $this->input->get('q');
        $vdb    = $this->trpinjamankaryawan_m->seekbank($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trpinjamankaryawan_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

    public function cetkjadwal(){
        $va     = $this->input->get() ;
        $data = $this->trpinjamankaryawan_m->getdatatotal($va['nopinjaman']);
        $vare = array();
        $tgl = date("Y-m-d");
        if (!empty($data)) {
            $jabatan = $this->perhitunganhrd_m->getjabatan($data['nip'],$tgl);
            $bagian = $this->perhitunganhrd_m->getbagian($data['nip'],$tgl);
            $vare['header'] = array();
            $vare['header'][] = array("1"=>"NIP","2"=>":","3"=>$data['nip'],"4"=>"","5"=>"Tgl","6"=>":","7"=>date_2d($data['tgl']));
            $vare['header'][] = array("1"=>"Nama","2"=>":","3"=>$data['nama'],"4"=>"","5"=>"Tgl Awal Angs","6"=>":","7"=>date_2d($data['tglawalags']));
            $vare['header'][] = array("1"=>"Jabatan","2"=>":","3"=>$jabatan['keterangan'],"4"=>"","5"=>"Plafond","6"=>":","7"=>string_2s($data['plafond']));
            $vare['header'][] = array("1"=>"Bagian","2"=>":","3"=>$bagian['keterangan'],"4"=>"","5"=>"Lama","6"=>":","7"=>string_2s($data['lama']) . " Bln");
            $vare['header'][] = array("1"=>"No Pinjaman","2"=>":","3"=>$data['nopinjaman'],"4"=>"","5"=>"Fkt Realisasi","6"=>":","7"=>$data['fakturrealisasi']);
            
            $vare['body'] = array();
            $bakidebet = $data['plafond'];
            $arrjadwal = $this->perhitunganhrd_m->getjadwal($data['tglawalags'],$data['plafond'],$data['lama']);
            foreach($arrjadwal as $key => $val){
                $bakidebet -= $val['agspokok'];
                $vare['body'][] = array("ke"=>$val['ke'],
                                        "jthtmp"=>date_2d($val['jthtmp']),
                                        "keterangan"=>"Angsuran Ke - ".$val['ke'],
                                        "pokok"=>string_2s($val['agspokok']),
                                        "bakidebet"=>string_2s($bakidebet));
            }
        }

        if(!empty($vare)){ 
            $font = 8 ;
            $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>"",
                            'opt'=>array('export_name'=>'JadwalPinjKaryawan_' . getsession($this, "username") ) ) ;
            $this->load->library('bospdf', $o) ;   
            $this->bospdf->ezText("<b>Jadwal Angsuran Pinjaman Karyawan</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ; 

            $this->bospdf->ezTable($vare['header'],"","",  
                                    array("showHeadings"=>"","showLines"=>"0","fontSize"=>$font,"cols"=> array( 
                                     "1"=>array("caption"=>"","width"=>15,"justification"=>"left"),
                                     "2"=>array("caption"=>"","width"=>5,"justification"=>"center"),
                                     "3"=>array("caption"=>"","justification"=>"left"),
                                     "4"=>array("caption"=>"","width"=>10,"justification"=>"left"),
                                     "5"=>array("caption"=>"","width"=>15,"justification"=>"left"),
                                     "6"=>array("caption"=>"","width"=>5,"justification"=>"center"),
                                     "7"=>array("caption"=>"","justification"=>"left")))) ;  
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vare['body'],"","",  
                                    array("showHeadings"=>"1","showLines"=>"2","fontSize"=>$font,"cols"=> array( 
                                     "ke"=>array("caption"=>"Ke","width"=>5,"justification"=>"right"),
                                     "jthtmp"=>array("caption"=>"JthTmp","width"=>10,"justification"=>"center"),
                                     "keterangan"=>array("caption"=>"Keterangan","justification"=>"left"),
                                     "pokok"=>array("caption"=>"Angsuran","width"=>12,"justification"=>"right"),
                                     "bakidebet"=>array("caption"=>"Bakidebet","width"=>15,"justification"=>"right")
                                    ))) ;  
            // $this->bospdf->ezTable($vare,"","",  
            //                         array("showHeadings"=>"","showLines"=>"1","fontSize"=>$font,"cols"=> array( 
            //                          "kode"=>array("caption"=>"Kode","width"=>15,"justification"=>"left"),
            //                          "keterangan"=>array("caption"=>"Keterangan","wrap"=>1),
            //                          "1"=>array("caption"=>"","width"=>15,"justification"=>"right"),
            //                          "2"=>array("caption"=>"","width"=>13,"justification"=>"right"),
            //                          "3"=>array("caption"=>"","width"=>13,"justification"=>"right")))) ;   
            //print_r($data) ;    
            $this->bospdf->ezStream() ; 
          }else{
             echo('data kosong') ;
          }
    }

    public function cetakbukti(){
        $va     = $this->input->get() ;
        $data = $this->trpinjamankaryawan_m->getdatatotal($va['nopinjaman']);
        $vare = array();
        $tgl = date("Y-m-d");
        if (!empty($data)) {
            $jabatan = $this->perhitunganhrd_m->getjabatan($data['nip'],$tgl);
            $bagian = $this->perhitunganhrd_m->getbagian($data['nip'],$tgl);
            $vare['header'] = array();
            $vare['header'][] = array("1"=>"NIP","2"=>":","3"=>$data['nip'],"4"=>"","5"=>"Tgl","6"=>":","7"=>date_2d($data['tgl']));
            $vare['header'][] = array("1"=>"Nama","2"=>":","3"=>$data['nama'],"4"=>"","5"=>"Tgl Awal Angs","6"=>":","7"=>date_2d($data['tglawalags']));
            $vare['header'][] = array("1"=>"Jabatan","2"=>":","3"=>$jabatan['keterangan'],"4"=>"","5"=>"Plafond","6"=>":","7"=>string_2s($data['plafond']));
            $vare['header'][] = array("1"=>"Bagian","2"=>":","3"=>$bagian['keterangan'],"4"=>"","5"=>"Lama","6"=>":","7"=>string_2s($data['lama']) . " Bln");
            $vare['header'][] = array("1"=>"No Pinjaman","2"=>":","3"=>$data['nopinjaman'],"4"=>"","5"=>"Fkt Realisasi","6"=>":","7"=>$data['fakturrealisasi']);
            $vare['header'][] = array("1"=>"Pencairan","2"=>":","3"=>$data['bank'] ." - ". $data['ketbank'],"4"=>"","5"=>"Keterangan","6"=>":","7"=>$data['keterangan']);
            

            $vare['ttd'][] = array("1"=>"Yang Menerima","2"=>"","3"=>"Bagian Keuangan");
            $vare['ttd'][] = array("1"=>"","2"=>"","3"=>"");
            $vare['ttd'][] = array("1"=>"","2"=>"","3"=>"");
            $vare['ttd'][] = array("1"=>"","2"=>"","3"=>"");
            $vare['ttd'][] = array("1"=>"","2"=>"","3"=>"");
            $vare['ttd'][] = array("1"=>"(................................)","2"=>"","3"=>"(................................)");
                        
        }

        if(!empty($vare)){ 
            $font = 8 ;
            $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>"",
                            'opt'=>array('export_name'=>'JadwalPinjKaryawan_' . getsession($this, "username") ) ) ;
            $this->load->library('bospdf', $o) ;   
            $this->bospdf->ezText("<b>Bukti Pencairan Pinjaman Karyawan</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ; 

            $this->bospdf->ezTable($vare['header'],"","",  
                                    array("showHeadings"=>"","showLines"=>"0","fontSize"=>$font,"cols"=> array( 
                                     "1"=>array("caption"=>"","width"=>15,"justification"=>"left"),
                                     "2"=>array("caption"=>"","width"=>5,"justification"=>"center"),
                                     "3"=>array("caption"=>"","justification"=>"left"),
                                     "4"=>array("caption"=>"","width"=>10,"justification"=>"left"),
                                     "5"=>array("caption"=>"","width"=>15,"justification"=>"left"),
                                     "6"=>array("caption"=>"","width"=>5,"justification"=>"center"),
                                     "7"=>array("caption"=>"","justification"=>"left")))) ;  
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vare['ttd'],"","",  
                                    array("showHeadings"=>"","showLines"=>"0","fontSize"=>$font,"cols"=> array( 
                                     "1"=>array("caption"=>"","justification"=>"center"),
                                     "2"=>array("caption"=>"","width"=>10),
                                     "3"=>array("caption"=>"","justification"=>"center")))) ;   
            //print_r($data) ;    
            $this->bospdf->ezStream() ; 
          }else{
             echo('data kosong') ;
          }
    }
}
