<?php
class Trsk extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('tr/trsk_m') ;
        $this->load->model('func/updtransaksi_m') ;
        $this->load->model('func/perhitungan_m') ;
        $this->load->helper('bdate');
        $this->bdb = $this->trsk_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(),"mintgl"=>$tglmin) ;
        $this->load->view('tr/trsk',$d) ;
    }

    public function loadgrid(){
        $jenis = array("1"=>"Prive (Pemakaian Sendiri)","2"=>"Kadaluarrsa","3"=>"Rusak","4"=>"Sample");
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare               = array() ;
        $va['tglawal']      = date_2s($va['tglawal']);
        $va['tglakhir']     = date_2s($va['tglakhir']);
        $vdb                = $this->trsk_m->loadgrid($va) ;
        $dbd                = $vdb['db'] ;
        while( $dbr = $this->trsk_m->getrow($dbd) ){
            $vaset                  = $dbr ;
            $vaset['jenis'] = $jenis[$dbr['jenis']];
            $vaset['tgl']           = date_2d($vaset['tgl']);
            $vaset['cmdedit']       = '<button type="button" onClick="bos.trsk.cmdedit(\''.$dbr['faktur'].'\')"
                                        class="btn btn-default btn-grid">Edit</button>' ;
            $vaset['cmddelete']     = '<button type="button" onClick="bos.trsk.cmddelete(\''.$dbr['faktur'].'\')"
                                        class="btn btn-danger btn-grid">Delete</button>' ;
            $vaset['cmdcetak']     = '<button type="button" onClick="bos.trsk.cmdcetak(\''.$dbr['faktur'].'\')"
                                        class="btn btn-warning btn-grid">Cetak</button>' ;
            $vaset['cmdcetak']       = html_entity_decode($vaset['cmdcetak']) ;
            $vaset['cmdedit']       = html_entity_decode($vaset['cmdedit']) ;
            $vaset['cmddelete']	    = html_entity_decode($vaset['cmddelete']) ;
            if ($tglmin > date_2s($dbr['tgl'])) {
                $vaset['cmdedit'] = "";
                $vaset['cmddelete'] = "";
            }
            $vare[]                 = $vaset ;
        }
        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "sssk_faktur", "") ;
    }

    public function saving(){
        $va 	     = $this->input->post() ;
        $kode 	     = getsession($this, "sssk_faktur") ;
        $va['tgl']   = date_2s($va['tgl']) ;
        
        $this->trsk_m->saving($kode, $va) ;
        echo(' bos.trsk.settab(0) ;  ') ;
    }

    public function editing(){
        $arrjenis = array("1"=>"Prive (Pemakaian Sendiri)","2"=>"Kadaluarsa","3"=>"Rusak","4"=>"Sample");

        $va 	= $this->input->post() ;
        $faktur = $va['faktur'] ;
        $data   = $this->trsk_m->getdatatotal($faktur) ;
        if(!empty($data)){
            savesession($this, "sssk_faktur", $faktur) ;
            $gudang[]   = array("id"=>$data['gudang'],"text"=>$data['ketgudang']);
            $jenis[]   = array("id"=>$data['jenis'],"text"=>$arrjenis[$data['jenis']]);
            echo('
            w2ui["bos-form-trsk_grid2"].clear();
            with(bos.trsk.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#faktur").val("'.$data['faktur'].'") ;
               find("#keterangan").val("'.$data['keterangan'].'") ;
               find("#tgl").val("'.date_2d($data['tgl']).'");
               find("#gudang").sval('.json_encode($gudang).');
               find("#jenis").val('.$data['jenis'].').trigger("change");
            }


         ') ;

            //loadgrid detail
            $vare = array();
            $dbd = $this->trsk_m->getdatadetail($faktur) ;
            $n = 0 ;
            while( $dbr = $this->trsk_m->getrow($dbd) ){
                $n++;
                $vaset   = $dbr ;
                $vaset['recid'] = $n;
                $vaset['no'] = $n;
                $vaset['cmddelete'] = '<button type="button" onClick="bos.trsk.grid2_deleterow('.$n.')"
                                        class="btn btn-danger btn-grid">Delete</button>' ;
                $vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;
                $vare[]             = $vaset ;
            }
            $vare = json_encode($vare);
            echo('
                w2ui["bos-form-trsk_grid2"].add('.$vare.');
                bos.trsk.initdetail();
                bos.trsk.settab(1) ;
            ');
        }
    }

    public function deleting(){
        $va 	= $this->input->post() ;
        $this->trsk_m->deleting($va['faktur']) ;
        echo('bos.trsk.grid1_reloaddata() ; ') ;

    }

    public function getfaktur(){
        $faktur  = $this->trsk_m->getfaktur(FALSE) ;

        echo('
        bos.trsk.obj.find("#faktur").val("'.$faktur.'") ;
        ') ;
    }
    
    public function showreport(){
        $va 	= $this->input->get() ;
        $faktur = $va['faktur'] ;
        $data   = $this->trsk_m->getdatatotal($faktur) ;
        if(!empty($data)){
            $arrjenis = array("1"=>"Prive (Pemakaian Sendiri)","2"=>"Kadaluarsa","3"=>"Rusak","4"=>"Sample");

            $font = 8 ;
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> $kota ,"5"=>"") ;
            //$vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"Dibuat,","3"=>"","4"=>"Mengetahui,","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"(.......................)","3"=>"","4"=>"(.......................)","5"=>"") ;

            $vDetail = array() ;
            $vDetail[] = array("1"=>"Faktur","2"=>":","3"=>$faktur,"4"=>"Gudang","5"=>":","6"=>$data['gudang']."-".$data['ketgudang']);
            $vDetail[] = array("1"=>"Tgl","2"=>":","3"=>date_2d($data['tgl']),"4"=>"Jenis","5"=>":","6"=>$arrjenis[$data['jenis']]);
            $vTitle = array() ;
            $vTitle[] = array("capt"=>" Stock Keluar ") ;

            //detail
            $dbd = $this->trsk_m->getdatadetail($faktur) ;
            $n = 0 ;
            $array = array();

            while( $dbr = $this->trsk_m->getrow($dbd)){ 
                    $n++;
                    $array[] = array("No"=>$n,"Stock"=>$dbr['stock'],"Nama Stock"=>$dbr['namastock'],"Qty"=>string_2s($dbr['qty']),"Satuan"=>$dbr['satuan']);

            }


            $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                          'opt'=>array('export_name'=>'Stock Keluar','mtop'=>1) ) ;
            $this->load->library('bospdf', $o) ;
            //$this->bospdf->ezSetMargins(1,1,20,20);
            //$this->bospdf->ezSetCmMargins(0, 1, 1, 1);
            //$this->bospdf->ezImage("./uploads/HeaderSJDO.jpg",true,'0','190','50');
            // $this->bospdf->ezImage("./uploads/HeaderSJDO.jpg",true,'0','190','50','5','left','',array());
            //$this->bospdf->ezLogoHeaderPage("./uploads/HeaderSJDO.jpg",'0','65','150','50');
            //$this->bospdf->ezHeader("<b>Stock Keluar</b>",array("justification"=>"center","fontSize"=>$font+3)) ;
            //$this->bospdf->ezText("") ;

            //ketika width tidak di set maka akan menyesuaikan dengan lebar kertas

            $this->bospdf->ezTable($vDetail,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "1"=>array("width"=>10,"justification"=>"left"),
                                             "2"=>array("width"=>3,"justification"=>"left"),
                                             "3"=>array("justification"=>"left"),
                                             "4"=>array("width"=>10,"justification"=>"left"),
                                             "5"=>array("width"=>3,"justification"=>"left"),
                                             "6"=>array("width"=>25,"justification"=>"left","wrap"=>1),)
                                        )
                                  ) ;

            $this->bospdf->ezTable($vTitle,"","",
                                   array("fontSize"=>$font+3,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "capt"=>array("justification"=>"center"),)
                                        )
                                  ) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($array,"","",
                                   array("fontSize"=>$font,"showLines"=>1,
                                         "cols"=> array(
                                             "No"           =>array("width"=>5,"justification"=>"right"),
                                             "Stock"  	    =>array("width"=>12,"justification"=>"center"),
                                             "Nama Stock"   =>array("justification"=>"left"),
                                             "Qty" =>array("width"=>15,"justification"=>"right"),
                                         "Satuan"  	    =>array("width"=>10,"justification"=>"left")))
                                  ) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("Ket : ".$data['keterangan']) ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("justification"=>"right"),
                                          "2"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "3"=>array("width"=>40,"wrap"=>1),
                                          "4"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "5"=>array("wrap"=>1,"justification"=>"center"))
                                 )
                              ) ;


            $this->bospdf->ezStream() ;
        }else{
            echo("data tidak ada !!!");
        }

    }
}
?>
