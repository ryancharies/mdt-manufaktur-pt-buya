<?php
class Trpelunasanhutang extends Bismillah_Controller
{
    private $bdb;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('func/updtransaksi_m');
        $this->load->model('tr/trpelunasanhutang_m');
        $this->load->helper('bdate');
        $this->bdb = $this->trpelunasanhutang_m;
    }

    public function index()
    {
        $vdb = $this->trpelunasanhutang_m->seekbankkas("");
        $dbd = $vdb['db'];
        $vare = array();
        while ($dbr = $this->trpelunasanhutang_m->getrow($dbd)) {
            $vare[] = array("id" => $dbr['kode'], "text" => $dbr['keterangan']);
        }
        $datasupp = json_encode($vare);
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d = array("dnow" => date("d-m-Y"), "setdate" => date_set(), "bankkas" => $datasupp,"mintgl"=>$tglmin);
        $this->load->view('tr/trpelunasanhutang', $d);
    }

    public function loadgrid()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va = json_decode($this->input->post('request'), true);
        $vare = array();
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $vdb = $this->trpelunasanhutang_m->loadgrid($va);
        $dbd = $vdb['db'];
        while ($dbr = $this->trpelunasanhutang_m->getrow($dbd)) {
            $vaset = $dbr;

            $vaset['tgl'] = date_2d($vaset['tgl']);
            $vaset['cmdedit'] = '<button type="button" onClick="bos.trpelunasanhutang.cmdedit(\'' . $dbr['faktur'] . '\')"
                                        class="btn btn-default btn-grid">Edit</button>';
                    
            $vaset['cmddelete'] = '<button type="button" onClick="bos.trpelunasanhutang.cmddelete(\'' . $dbr['faktur'] . '\')"
                                        class="btn btn-danger btn-grid">Delete</button>';
            
            $validdel = $this->trpelunasanhutang_m->cekvalidasidel($dbr['faktur']);
            if($validdel <> "") $vaset['cmddelete'] = '<button type="button" onClick="alert(\'' . $validdel . '\')"
            class="btn btn-info btn-grid">Info</button>';

            $vaset['cmdedit'] = html_entity_decode($vaset['cmdedit']);
            $vaset['cmddelete'] = html_entity_decode($vaset['cmddelete']);

            if($tglmin > date_2s($dbr['tgl'])){//jika status tgl tidak open maka tidak action
                $vaset['cmdedit'] = "";
                $vaset['cmddelete'] = "";
            }
            $vare[] = $vaset;
        }
        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function init()
    {
        savesession($this, "sspelunasanhutang_faktur", "");
    }

    public function saving()
    {
        $va = $this->input->post();
        $faktur = getsession($this, "sspelunasanhutang_faktur");
        $va['tgl'] = date_2s($va['tgl']);
        if (!isset($va['kdtrpskt'])) {
            $va['kdtrpskt'] = "";
        }

        if (!isset($va['bankkas'])) {
            $va['bankkas'] = "";
        }

        //cek valid grid 3
        $error = "";
        $grid3 = json_decode($va['grid3']);
        foreach ($grid3 as $key => $val) {
            //print_r($val);
            if ($val->bank != '' or $val->bgcek != '' or $val->rekening != '' or $val->nobg != '' or $val->bgcek != '' or string_2n($val->total) > 0) {
                if (date_2s($val->jthtmp) < date_2s($va['tgl'])) {
                    $error .= "Tgl Jatuh tempo BG tidak boleh lebih kecil dari tgl pelunasan.!! \\n";
                }

                if (string_2n($val->total) <= 0) {
                    $error .= "Nilai BG tidak valid.!! \\n";
                }

                if ($val->bank == '' or $val->bgcek == '' or $val->rekening == '' or $val->nobg == '' or $val->bgcek == '') {
                    $error .= "Data tidak lengkap.!! \\n";
                }

                $nobgcek = $val->nobg;
                if ($error == "") {
                    $dbd = $this->bdb->select("bg_list", "*", "nobgcek='$nobgcek' and faktur <> '$faktur' and status <> '2'");
                    if ($this->bdb->rows($dbd) > 0) {
                        $error .= "No bg sudah pernah digunakan.!! \\n";
                    }
                }
            }
            if ($error != "") {
                break;
            }

        }

        //cek valid data
        if (string_2n($va['tfkas']) > 0 && $va['bankkas'] == "") {
            $error .= "Kas bank belum dilipih.!! \\n";
        }

        //cek valid data
        if (string_2n($va['persekot']) > 0 && $va['kdtrpskt'] == "") {
            $error .= "Kode Transaksi Porsekot belum dilipih.!! \\n";
        }

        if ($error == "") {
            $this->trpelunasanhutang_m->saving($faktur, $va);
            echo (' bos.trpelunasanhutang.settab(0) ;
                     alert("Pelunasan berhasil disimpan..");');
        } else {
            echo (' alert("' . $error . '");');
        }
    }

    public function editing()
    {
        $va = $this->input->post();
        $faktur = $va['faktur'];
        $data = $this->trpelunasanhutang_m->getdatatotal($faktur);
        if (!empty($data)) {
            savesession($this, "sspelunasanhutang_faktur", $faktur);
            $supplier[] = array("id" => $data['supplier'], "text" => $data['namasupplier']);
            $bankkas[] = array("id" => $data['rekkasbank'], "text" => $data['ketrekkasbank']);
            $kdtrpersekot[] = array("id" => $data['kdtrpersekot'], "text" => "[" . $data['dktrpersekot'] . "] " . $data['ketpersekot']);
            echo ('
            w2ui["bos-form-trpelunasanhutang_grid2"].clear();
            w2ui["bos-form-trpelunasanhutang_grid3"].clear();
            with(bos.trpelunasanhutang.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#faktur").val("' . $data['faktur'] . '") ;
               find("#keterangan").val("' . $data['keterangan'] . '") ;
               find("#tgl").val("' . date_2d($data['tgl']) . '");
               find("#bankkas").sval(' . json_encode($bankkas) . ');
               find("#supplier").sval(' . json_encode($supplier) . ');
               find("#kdtrpskt").sval(' . json_encode($kdtrpersekot) . ');
               find("#pembelian").val("' . string_2s($data['pembelian']) . '") ;
               find("#retur").val("' . string_2s($data['retur']) . '") ;
               find("#subtotal").val("' . string_2s($data['subtotal']) . '") ;
               find("#tfkas").val("' . string_2s($data['kasbank']) . '") ;
               find("#diskon").val("' . string_2s($data['diskon']) . '") ;
               find("#pembulatan").val("' . string_2s($data['pembulatan']) . '") ;
               find("#persekot").val("' . string_2s($data['persekot']) . '") ;
               find("#bg").val("' . string_2s($data['bgcek']) . '") ;

            }


         ');

            //loadgrid bg
            $vare = array();
            //initgrid3
            for($i=1;$i<=10;$i++){
                $vare[] = array("recid"=>$i,"no"=>$i,"bank"=>"","bgcek"=>"","rekening"=>"",
                                "nobg"=>"","status"=>"","total"=>0,"jthtmp"=>date("d-m-Y")); 
            }
            
            $dbd = $this->trpelunasanhutang_m->getdatabg($faktur);
            $n = 0;
            
            while ($dbr = $this->trpelunasanhutang_m->getrow($dbd['db'])) {                
                
                $vare[$n]['bank'] = $dbr['bank'];
                $vare[$n]['bgcek'] = $dbr['bgcek'];
                $vare[$n]['rekening'] = $dbr['norekening'];
                $vare[$n]['nobg'] = $dbr['nobgcek'];
                $vare[$n]['total'] = $dbr['nominal'];
                $vare[$n]['jthtmp'] = date_2d($dbr['tgljthtmp']);
                if($dbr['fakturcair'] <> "")$vare[$n]['status']="Cair";
                
                $n++;
            }

            $vare = json_encode($vare);           

            echo ('
            w2ui["bos-form-trpelunasanhutang_grid3"].add(' . $vare . ');
                bos.trpelunasanhutang.settab(1) ;
            ');
        }
    }

    public function deleting()
    {
        $va = $this->input->post();
        $error = $this->trpelunasanhutang_m->deleting($va['faktur']);
        
        if($error == ""){
            echo ('bos.trpelunasanhutang.grid1_reloaddata() ; ');
        }else{
            echo('
                alert(" Pelunasan tidak bisa dihapus \\n '.$error.'");
            ');
        }
    }

    public function seekgudang()
    {
        $search = $this->input->get('q');
        $vdb = $this->trpembelian_m->seekgudang($search);
        $dbd = $vdb['db'];
        $vare = array();
        while ($dbr = $this->trpembelian_m->getrow($dbd)) {
            $vare[] = array("id" => $dbr['kode'], "text" => $dbr['keterangan']);
        }
        $Result = json_encode($vare);
        echo ($Result);
    }

    public function seeksupplier()
    {
        $search = $this->input->get('q');
        $vdb = $this->trpelunasanhutang_m->seeksupplier($search);
        $dbd = $vdb['db'];
        $vare = array();
        while ($dbr = $this->trpelunasanhutang_m->getrow($dbd)) {
            $vare[] = array("id" => $dbr['kode'], "text" => $dbr['nama']);
        }
        $Result = json_encode($vare);
        echo ($Result);
    }

    public function seekbankkas()
    {
        $search = $this->input->get('q');
        $vdb = $this->trpelunasanhutang_m->seekbankkas($search);
        $dbd = $vdb['db'];
        $vare = array();
        while ($dbr = $this->trpelunasanhutang_m->getrow($dbd)) {
            $vare[] = array("id" => $dbr['kode'], "text" => $dbr['keterangan']);
        }
        $Result = json_encode($vare);
        echo ($Result);
    }

    public function seekkodetransaksi()
    {
        $search = $this->input->get('q');
        $vdb = $this->trpelunasanhutang_m->seekkodetransaksi($search);
        $dbd = $vdb['db'];
        $vare = array();
        while ($dbr = $this->trpelunasanhutang_m->getrow($dbd)) {
            $vare[] = array("id" => $dbr['kode'], "text" => "[" . $dbr['dk'] . "] " . $dbr['keterangan']);
        }
        $Result = json_encode($vare);
        echo ($Result);
    }

    public function getfaktur()
    {
        $faktur = $this->trpelunasanhutang_m->getfaktur(false);

        echo ('
        bos.trpelunasanhutang.obj.find("#faktur").val("' . $faktur . '") ;
        ');
    }

    public function getdatastock()
    {
        $va = $this->input->post();
        $cKode = $va['cKodeStock'];
        $dbData = $this->trpembelian_m->getdatastock($cKode);
        if ($dbRow = $this->trpembelian_m->getrow($dbData)) {
            $cKeterangan = $dbRow['Keterangan'];
            $cSatuan = $dbRow['Satuan'];
            echo ('
          bos.trpembelian.obj.find("#satuan").val("' . $cSatuan . '") ;
          bos.trpembelian.obj.find("#namastock").val("' . $cKeterangan . '") ;
        ');
        } else {
            echo ('
          bos.trpembelian.obj.find("#stock").val("") ;
          bos.trpembelian.obj.find("#nomor").focus() ;
          alert("Data Tidak Ditemukan!!");
        ');
        }
    }

    

    public function loadgrid2()
    {
        $va = $this->input->post();
        $va['tgl'] = date_2s($va['tgl']);
        $vdb = $this->trpelunasanhutang_m->loadhutpembelian($va);
        $dbd = $vdb['db'];
        $vare = array();
        $n = 0;
        $recid = 0;
        $vare[] = array("recid" => $recid, "no" => "", "faktur" => ":: Pembelian", "tgl" => "", "total" => "", "sisaawal" => "",
            "pelunasan" => "", "sisa" => "");
        while ($dbr = $this->trpelunasanhutang_m->getrow($dbd)) {
            $n++;
            $recid++;
            $sisa = $dbr['saldo'];
            $pelunasan = $dbr['pelunasan'];
            $sisaawal = $pelunasan + $sisa;
            $vare[] = array("recid" => $recid, "no" => $n, "faktur" => $dbr['faktur'], "tgl" => date_2d($dbr['tgl']), "total" => string_2s($dbr['total']),
                "sisaawal" => string_2s($sisaawal),
                "pelunasan" => string_2s($pelunasan), "sisa" => string_2s($sisa), "jenis" => "Pembelian");
        }

        $vdb = $this->trpelunasanhutang_m->loadhutpembelianaset($va);
        $dbd = $vdb['db'];
        while ($dbr = $this->trpelunasanhutang_m->getrow($dbd)) {
            $n++;
            $recid++;

            $vendors = json_decode($dbr['vendors'],true);
            // print_r($vendors);
            $sisa = $dbr['saldo'];
            $pelunasan = $dbr['pelunasan'];
            $sisaawal = $pelunasan + $sisa;
            $vare[] = array("recid" => $recid, "no" => $n, "faktur" => $dbr['faktur'], "tgl" => date_2d($dbr['tgl']), 
                "total" => string_2s($vendors[$dbr['supplier']]['hutang']),
                "sisaawal" => string_2s($sisaawal),
                "pelunasan" => string_2s($pelunasan), "sisa" => string_2s($sisa), "jenis" => "Pembelian");
        }

        $vdb = $this->trpelunasanhutang_m->loadhutperubahanaset($va);
        $dbd = $vdb['db'];
        while ($dbr = $this->trpelunasanhutang_m->getrow($dbd)) {
            $n++;
            $recid++;

            $vendors = json_decode($dbr['vendors'],true);
            // print_r($vendors);
            $sisa = $dbr['saldo'];
            $pelunasan = $dbr['pelunasan'];
            $sisaawal = $pelunasan + $sisa;
            $vare[] = array("recid" => $recid, "no" => $n, "faktur" => $dbr['faktur'], "tgl" => date_2d($dbr['tgl']), 
                "total" => string_2s($vendors[$dbr['supplier']]['hutang']),
                "sisaawal" => string_2s($sisaawal),
                "pelunasan" => string_2s($pelunasan), "sisa" => string_2s($sisa), "jenis" => "Pembelian");
        }

        $vdb2 = $this->trpelunasanhutang_m->loadhutreturpembelian($va);
        $dbd2 = $vdb2['db'];
        $n = 0;
        $recid++;
        $vare[] = array("recid" => $recid, "no" => "", "faktur" => ":: Retur Pembelian", "tgl" => "", "total" => "", "sisaawal" => "",
            "pelunasan" => "", "sisa" => "");
        while ($dbr = $this->trpelunasanhutang_m->getrow($dbd2)) {
            $n++;
            $recid++;
            $sisa = $dbr['saldo'];
            $pelunasan = $dbr['pelunasan'];
            $sisaawal = $pelunasan + $sisa;
            $vare[] = array("recid" => $recid, "no" => $n, "faktur" => $dbr['faktur'], "tgl" => date_2d($dbr['tgl']),
                "total" => string_2s($dbr['total']), "sisaawal" => string_2s($sisaawal),
                "pelunasan" => string_2s($pelunasan), "sisa" => string_2s($sisa), "jenis" => "Retur Pembelian");
        }

        $vare = json_encode($vare);

        echo ('
                w2ui["bos-form-trpelunasanhutang_grid2"].add(' . $vare . ');
              ');

    }
}
