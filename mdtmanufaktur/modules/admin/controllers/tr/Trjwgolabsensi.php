<?php
class Trjwgolabsensi extends Bismillah_Controller{
   public function __construct(){
		parent::__construct() ;
      $this->load->model('tr/trjwgolabsensi_m') ;
      $this->load->model('func/perhitunganhrd_m') ;
      $this->load->helper('bdate') ;
      $this->load->helper('bdir') ;
	}

   public function index(){
      $tglmin = $this->trjwgolabsensi_m->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
      $data = array();
      $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
      $this->load->view('tr/trjwgolabsensi',$data) ;
   }

   public function loadgrid(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
      $vdb    = $this->trjwgolabsensi_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      $tgl = date("Y-m-d");
      while( $dbr = $this->trjwgolabsensi_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['cmdedit']    = '<button type="button" onClick="bos.trjwgolabsensi.cmdedit(\''.$dbr['kode'].'\')"
                           class="btn btn-success btn-grid">Jadwal</button>' ;
         $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function loadgrid2(){
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
     // print_r($va);
      $vdb    = $this->trjwgolabsensi_m->loadgrid2($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->trjwgolabsensi_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['tgl'] = date_2d($vaset['tgl']);
         $vaset['cmddelete']    = '<button type="button" onClick="bos.trjwgolabsensi.cmddeletemj(\''.$dbr['golongan'].'\',\''.$dbr['tgl'].'\')"
                           class="btn btn-danger btn-grid">Delete</button>' ;
         $vaset['cmddelete']	   = html_entity_decode($vaset['cmddelete']) ;

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function init(){
      savesession($this, "ssgolongan_kode", "") ;
   }

   public function saving(){
      $va 	= $this->input->post() ;
      $return = $this->trjwgolabsensi_m->saving($va) ;
      if($return == "ok"){
         echo('
            alert("Data telah disimpan!!");
            bos.trjwgolabsensi.grid2_reloaddata() ;
         ');
      }else{
         echo('
            alert("'.$return.'");
         ');
      }
      
   }

   public function editing(){
      $va 	= $this->input->post() ;
      $kode 	= $va['kode'] ;
      $data = $this->trjwgolabsensi_m->getdata($kode) ;
      if(!empty($data)){
         echo('
            with(bos.trjwgolabsensi.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$data['kode'].'") ;
               find("#keterangan").val("'.$data['keterangan'].'") ;
            }
            bos.trjwgolabsensi.settab(1) ;

         ');

      }
   }

   public function deleting(){
      $va 	= $this->input->post() ;
      $error = $this->trjwgolabsensi_m->deleting($va) ;
      if($error == "ok"){
         echo(' bos.trjwgolabsensi.grid2_reloaddata() ; ') ;
      }else{
         echo('alert("'.$error.'");');
      }
   }
    
   public function seekjadwal(){
        $search     = $this->input->get('q');
        $vdb    = $this->trjwgolabsensi_m->seekjadwal($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trjwgolabsensi_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
   }
   
}
?>
