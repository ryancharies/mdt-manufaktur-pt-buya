<?php
class Trpo extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('tr/trpo_m') ;
        $this->load->model('func/updtransaksi_m') ;
        $this->load->model('func/perhitungan_m') ;
        $this->load->model('func/func_m') ;
        $this->load->helper('bdate');
        $this->bdb = $this->trpo_m ;
    }

    public function index(){
        // mengambil min tgl transaksi yang aktif
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(),"mintgl"=>$tglmin) ;
        $this->load->view('tr/trpo',$d) ;
    }

    public function loadgrid_where($bs, $s){
        $this->duser();
        $bs['tglawal'] = date_2s($bs['tglawal']);
        $bs['tglakhir'] = date_2s($bs['tglakhir']);

        $this->db->where('t.status', "1");
        $this->db->where('t.tgl >=', $bs['tglawal']);
        $this->db->where('t.tgl <=', $bs['tglakhir']);

        
        if($bs['skd_gudang'] !== 'null'){
            $bs['skd_gudang'] = json_decode($bs['skd_gudang'],true);
            $this->db->group_start();
            foreach($bs['skd_gudang'] as $kdc){
                $this->db->or_where('t.gudang',$kdc);
            }
            $this->db->group_end();

        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['gudang'])){
                    $this->db->group_start();
                    foreach($this->aruser['data_var']['gudang'] as $kdc){
                        $this->db->or_where('t.gudang', $kdc);
                    }
                    $this->db->group_end();
                }
            }
        }

        if($s !== ''){
            $this->db->group_start();
                $this->db->or_like(array('t.faktur'=>$s, 's.nama'=>$s));
            $this->db->group_end();
        }
    }

    public function loadgrid(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");

        $va       = json_decode($this->input->post('request'), true) ;
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	  = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->db->escape_like_str($search) ;
        $vare               = array() ;
        $this->loadgrid_where($va, $search);
        $f = "count(t.id) jml";
        $dbd = $this->db->select($f)
            ->from("po_total t")
            ->join("supplier s","s.kode = t.supplier","left")
            ->join("pembelian_total p","p.fktpo = t.faktur and p.status = '1'","left")
            ->join("gudang g","g.kode = t.gudang","left")
            ->get();
        $rtot  = $dbd->row_array();
        if($rtot['jml'] > 0){
            $this->loadgrid_where($va, $search);
            $f2 = "t.faktur,t.tgl,s.nama as supplier,t.total,t.fktpr,g.keterangan as gudang,p.faktur fktpb";
            $dbd2 = $this->db->select($f2)->from("po_total t")
                ->join("supplier s","s.kode = t.supplier","left")
                ->join("pembelian_total p","p.fktpo = t.faktur and p.status = '1'","left")
                ->join("gudang g","g.kode = t.gudang","left")
                ->limit($limit)
                ->get();
        
            foreach($dbd2->result_array() as $dbr){
                $vaset                  = $dbr ;
                $vaset['tgl']           = date_2d($vaset['tgl']);
                $vaset['cmdedit']       = '<button type="button" onClick="bos.trpo.cmdedit(\''.$dbr['faktur'].'\')"
                                            class="btn btn-default btn-grid">Edit</button>' ;
                $vaset['cmddelete']     = '<button type="button" onClick="bos.trpo.cmddelete(\''.$dbr['faktur'].'\')"
                                            class="btn btn-danger btn-grid">Delete</button>' ;
                $vaset['cmdedit']       = html_entity_decode($vaset['cmdedit']) ;
                $vaset['cmddelete']	    = html_entity_decode($vaset['cmddelete']) ;
                if($dbr['fktpb'] <> '' && $dbr['fktpb'] <> null){//if($tglmin > date_2s($dbr['tgl'])){
                    $vaset['cmdedit'] = "";
                    $vaset['cmddelete'] = "";
                }
                $vare[]                 = $vaset ;
            }
        }

        
        $vare 	= array("total"=>$rtot['jml'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "sspo_faktur", "") ;
    }

    public function saving(){
        $va 	     = $this->input->post() ;
        $va['tgl']   = date_2s($va['tgl']) ;
        $faktur         = getsession($this, "sspo_faktur", "");
        $igd = $this->func_m->getinfogudang($va['gudang']);
        $cabang = $igd['cabang'];
        $va['faktur']   = $faktur !== "" ? $faktur : $this->bdb->getfaktur($cabang,$va['tgl'],true) ;
        
        $data           = array("faktur"=>$va['faktur'],
                                "fktpr"=>$va['fktpr'],
                                "tgl"=>$va['tgl'],
                                "total"=>string_2n($va['total']),
                                "status"=>"1",
                                "gudang"=>$va['gudang'],
                                "supplier"=>$va['supplier'],
                                "cabang"=> $cabang,
                                "username"=> getsession($this, "username"),
                                "datetime"=>date("Y-m-d H:i:s")) ;
        $where          = "faktur = " . $this->bdb->escape($va['faktur']) ;
        $this->bdb->update("po_total", $data, $where, "") ;

        //insert detail po
        $vaGrid = json_decode($va['grid2']);
        $this->bdb->delete("po_detail", "faktur = '{$va['faktur']}'" ) ;
        foreach($vaGrid as $key => $val){
            $cKdStockGrid = $val->stock;
            $dbRKD = $this->perhitungan_m->getdatastock($cKdStockGrid) ;
            // if($dbRKD = $this->getrow($dbKD)){
              $cKodeStock = $dbRKD['kode'];
            // }
            if(!isset($val->spesifikasi))$val->spesifikasi = "";
            if(!isset($val->harga))$val->harga = 0;
            if(!isset($val->jumlah))$val->jumlah = 0;

            $vadetail = array("faktur"=>$va['faktur'],
                              "stock"=>$cKodeStock,
                              "spesifikasi"=>$val->spesifikasi,
                              "qty"=>$val->qty,
                              "harga"=>$val->harga,
                              "jumlah"=>$val->jumlah);
            $this->bdb->insert("po_detail",$vadetail);
        }

        $this->updtransaksi_m->updpokartupurchaseorder($va['faktur']);


        echo('ok') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $faktur = $va['faktur'] ;

        $this->db->where('t.faktur',$faktur);
        $f = "t.faktur,t.tgl,t.gudang,t.supplier,t.fktpr,g.keterangan as ketgudang,t.total,s.nama as namasupplier";
        $dbd = $this->db->select($f)
            ->from("po_total t")
            ->join("gudang g","g.kode = t.gudang","left")
            ->join("supplier s","s.kode = t.supplier","left")
            ->get();
        $data  = $dbd->row_array();
        if(!empty($data)){
            savesession($this, "sspo_faktur", $faktur) ;
            $data['tgl'] = date_2d($data['tgl']);

            //loadgrid detail
            $vare = array();
            $this->db->where('d.faktur',$faktur);
            $f2 = "d.stock,s.keterangan as namastock,d.spesifikasi,d.harga,d.qty,s.satuan,d.jumlah";
            $dbd2 = $this->db->select($f2)->from("po_detail d")
                    ->join("stock s","s.kode = d.stock","left")
                    ->get();
        
            foreach($dbd2->result_array() as $dbr){
                $vare[$dbr['stock']] = $dbr ;
            }
            $data['detil'] = $vare;
        }

        echo(json_encode($data));
    }

    public function deleting(){
        $va 	= $this->input->post() ;

        $this->bdb->edit("po_total",array("status"=>2),"faktur = " . $this->bdb->escape($va['faktur']));
        $this->bdb->delete("po_kartu","faktur = " . $this->bdb->escape($va['faktur']));
        echo("ok") ;

    }

    public function getfaktur(){
        $va 	= $this->input->post() ;
        $igd = $this->func_m->getinfogudang($va['gudang']);
        $cabang = $igd['cabang'];
        $faktur  = $this->trpo_m->getfaktur($cabang,$va['tgl'],FALSE) ;

        echo($faktur) ;
    }
    
    public function pilihpr(){
        $va 	= $this->input->post() ;
        $fktpr 	= $va['fktpr'] ;

        $this->db->where('p.faktur', $fktpr);
        $f = "p.faktur,p.tgl,p.gudang,g.keterangan as ketgudang";
        $dbd = $this->db->select($f)
            ->from("pr_total p")
            ->join("gudang g","g.kode = p.gudang","left")
            ->get();
        $data  = $dbd->row_array();
        if(!empty($data)){
            $vare = array();
            $this->db->where('d.faktur',$fktpr);
            $f2 = "d.stock,s.keterangan as namastock,d.qty,s.satuan";
            $dbd2 = $this->db->select($f2)->from("pr_detail d")
                    ->join("stock s","s.kode = d.stock","left")
                    ->get();
        
            foreach($dbd2->result_array() as $dbr){
                $vare[$dbr['stock']] = $dbr ;
            }
            $data['detil'] = $vare;
        }

        echo(json_encode($data));
    }

    public function loadgrid4_where($bs, $s){
        $this->duser();

        $this->db->where('p.status', "1");
        $this->db->where('b.faktur is null');

        if($s !== ''){
            $this->db->group_start();
            $this->db->or_like(array('p.faktur'=>$s, 'g.keterangan'=>$s));
            $this->db->group_end();
        }
    }
    
    public function loadgrid4(){
        $va       = json_decode($this->input->post('request'), true) ;
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	  = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->db->escape_like_str($search) ;
        $vare               = array() ;

        $this->loadgrid4_where($va, $search);        
        $f = "count(p.id) jml";
        $dbd = $this->db->select($f)
            ->from("pr_total p")
            ->join("gudang g","g.kode = p.gudang","left")
            ->join("po_total b","b.fktpr = p.faktur and b.status = '1'","left")
            ->get();
        $rtot  = $dbd->row_array();
        if($rtot['jml'] > 0){
            $this->loadgrid4_where($va, $search);
            $f2 = "p.faktur,p.tgl,g.keterangan as gudang";
            $dbd2 = $this->db->select($f2)->from("pr_total p")
                ->join("gudang g","g.kode = p.gudang","left")
                ->join("po_total b","b.fktpr = p.faktur and b.status = '1'","left")
                ->order_by("p.faktur asc")
                ->limit($limit)
                ->get();        
            foreach($dbd2->result_array() as $dbr){
                $vaset   = $dbr ;
                $vaset['cmdpilih']    = '<button type="button" onClick="bos.trpo.cmdpilihpr(\''.$dbr['faktur'].'\')"
                            class="btn btn-success btn-grid">Pilih</button>' ;
                $vaset['cmdpilih']	   = html_entity_decode($vaset['cmdpilih']) ;
                $vare[]		= $vaset ;
            }
        }

        $vare 	= array("total"=>$rtot['jml'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }
}
?>
