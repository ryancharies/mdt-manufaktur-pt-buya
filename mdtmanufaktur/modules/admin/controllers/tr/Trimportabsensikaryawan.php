<?php
class Trimportabsensikaryawan extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("tr/trimportabsensikaryawan_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->bdb 	= $this->trimportabsensikaryawan_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $this->load->view("tr/trimportabsensikaryawan",$data) ;

    } 


    public function init(){
        savesession($this, "sstrimportabsensikaryawan_kode", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $error = $this->bdb->saving($va) ;
        if($error == ""){
            echo(' 
                    alert("Data berhasil diimport..");
                    w2ui["bos-form-trimportabsensikaryawan_grid1"].clear(); 
                
                ') ;
        }else{
            echo('
                alert("'.$error.'");
                
                ') ;
        }
        
    }

    public function seekmesin(){
        $search     = $this->input->get('q');
        $vdb    = $this->trimportabsensikaryawan_m->seekmesin($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trimportabsensikaryawan_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ". $dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
   }

    public function seekperiode(){
        $search     = $this->input->get('q');
        $vdb    = $this->trimportabsensikaryawan_m->seekperiode($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trimportabsensikaryawan_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ". $dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
            echo($Result) ;
    }

   public function loadperiode(){
        $va 	= $this->input->post() ;
        $vdb    = $this->trimportabsensikaryawan_m->getperiode($va) ;
        if( $dbr = $this->trimportabsensikaryawan_m->getrow($vdb['db']) ){
            echo('
                with(bos.trimportabsensikaryawan.obj){
                    find("#tglawal").val("'.date_2d($dbr['tglawal']).'") ;
                    find("#tglakhir").val("'.date_2d($dbr['tglakhir']).'") ;
                }
            ') ;
        }
   }

   public function uploadimport($cfg){
        $va = $this->input->post();
       // print_r($va);
        $username = getsession($this, "username");
        //$file = $username .time();
        $file = md5(rand(0,10000) . time() . session_id());
        $fcfg	= array("upload_path"=>"./tmp/", "allowed_types"=>"csv", "overwrite"=>true,
                        "file_name"=> $file ) ;
        $this->load->library('upload', $fcfg) ;

        if ( ! $this->upload->do_upload(0) ){
            echo('
                alert("'. $this->upload->display_errors('','') .'") ;
                bos.trimportabsensikaryawan.obj.find("#idl'.$cfg.'").html("") ;
            ') ;
        }else{
            $data 	= $this->upload->data() ;
            $fname 	= $file . $data['file_ext'] ;
            $tname 	= str_replace($data['file_ext'], "", $data['client_name']) ;
            $vimage	= array( $tname => $data['full_path']) ;
            // echo('
            // 	bos.mstdatastock.obj.find("#idl'.$cfg.'2").html("") ;
            // 	bos.mstdatastock.obj.find("#id'.$cfg.'2").html("<img src=\"'.base_url("./tmp/" . $fname . "?time=". time()).'\" class=\"img-responsive\" style=\"margin:0 auto;max-height:200px\"/>") ;
            // ') ;
            $urlfile = "./tmp/" . $fname;

            // $file = fopen($urlfile,"r");
            // print_r(fgetcsv($file,1000,","));
            // fclose($file);
            $vare = array();
            $row = 0;
            $recid = 0 ;
            if (($handle = fopen($urlfile, "r")) !== FALSE) {
                while (($data = fgetcsv($handle)) !== FALSE) {
                   $row++;
                   
                   if($row > 1 && date_2s($data[5]) <= date_2s($va['tglakhir']) && date_2s($data[5]) >= date_2s($va['tglawal'])){
                        $recid++;

                        $pin = $data[1];
                        $tgl = $data[5];
                        $karyawan = $this->perhitunganhrd_m->getdatakaryawanbypinabsensi($pin,$va['mesin']);
                        
                        $jabatan = $this->perhitunganhrd_m->getjabatan($karyawan['nip'],$tgl);
                        $bagian = $this->perhitunganhrd_m->getbagian($karyawan['nip'],$tgl);

                        $nip = $karyawan['nip'];
                        $nama = $karyawan['nama'];
                        $ketbagian = $bagian['keterangan'];
                        $ketjabatan = $jabatan['keterangan'];
                        
                        //print_r($karyawan);
                        $namamesin = $data[3];
                        $jmasuk = $data[7];
                        $jpulang = $data[8];
                        $masuk = $data[9];
                        $pulang = $data[10];
                        $terlambat = $data[13];
                        $plgcepat = $data[14];
                        $lembur = $data[16];
                        $status = $data[12];
                        $ketstatus = "";
                        if($status == "1"){
                            $ketstatus = "Masuk";
                        }else{
                            $ketstatus = "Absent";
                        }
                        $vare[] = array("recid"=>$recid,"tgl"=>$tgl,"nip"=>$nip,"nama"=>$nama,"bagian"=>$ketbagian,"jabatan"=>$ketjabatan,
                            "pin"=>$pin,"namamesin"=>$namamesin,"masuk"=>$masuk,"jwmasuk"=>$jmasuk,"terlambat"=>$terlambat,"pulang"=>$pulang,
                            "jwpulang"=>$jpulang,"plgcepat"=>$plgcepat,"lembur"=>$lembur,"status"=>$ketstatus);
                   }
                }
                fclose($handle);
            }

            $vare = json_encode($vare);

            echo ('
                w2ui["bos-form-trimportabsensikaryawan_grid1"].add(' . $vare . ');
            
                bos.trimportabsensikaryawan.obj.find("#idl'.$cfg.'").html("") ;
                bos.trimportabsensikaryawan.obj.find("#'.$cfg.'").val("") ;

                bos.trimportabsensikaryawan.grid1_reload();
            ') ;
        }
   }
}
?>
