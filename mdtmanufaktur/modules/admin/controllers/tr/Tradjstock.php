<?php
class Tradjstock extends Bismillah_Controller
{
    private $bdb;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('func/updtransaksi_m');
        $this->load->model('func/perhitungan_m');
        $this->load->model('tr/tradjstock_m');
        $this->load->helper('bdate');
        $this->bdb = $this->tradjstock_m;
    }

    public function index()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d = array("dnow" => date("d-m-Y"), "setdate" => date_set(),"mintgl"=>$tglmin);
        $this->load->view('tr/tradjstock', $d);
    }

    public function loadgrid()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va = json_decode($this->input->post('request'), true);
        $vare = array();
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);
        $vdb = $this->tradjstock_m->loadgrid($va);
        $dbd = $vdb['db'];
        while ($dbr = $this->tradjstock_m->getrow($dbd)) {
            $vaset = $dbr;
            $vaset['tgl'] = date_2d($vaset['tgl']);
            $vaset['cmdedit'] = '<button type="button" onClick="bos.tradjstock.cmdedit(\'' . $dbr['faktur'] . '\')"
                                        class="btn btn-default btn-grid">Edit</button>';
            $vaset['cmddelete'] = '<button type="button" onClick="bos.tradjstock.cmddelete(\'' . $dbr['faktur'] . '\')"
                                        class="btn btn-danger btn-grid">Delete</button>';
            $vaset['cmdedit'] = html_entity_decode($vaset['cmdedit']);
            $vaset['cmddelete'] = html_entity_decode($vaset['cmddelete']);
            if ($tglmin > date_2s($dbr['tgl'])) {
                $vaset['cmdedit'] = "";
                $vaset['cmddelete'] = "";
            }
            $vare[] = $vaset;
        }
        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function init()
    {
        savesession($this, "ssadjstock_faktur", "");
    }

    public function saving()
    {
        $va = $this->input->post();
        $kode = getsession($this, "ssadjstock_faktur");
        $va['tgl'] = date_2s($va['tgl']);

        $this->tradjstock_m->saving($kode, $va);
        echo (' bos.tradjstock.settab(0) ;  ');
    }

    public function editing()
    {
        $va = $this->input->post();
        $faktur = $va['faktur'];
        $data = $this->tradjstock_m->getdatatotal($faktur);
        if (!empty($data)) {
            savesession($this, "ssadjstock_faktur", $faktur);
            $gudang[] = array("id" => $data['gudang'], "text" => $data['ketgudang']);
            echo ('
            with(bos.tradjstock.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#faktur").val("' . $data['faktur'] . '") ;
               find("#tgl").val("' . date_2d($data['tgl']) . '");
               find("#gudang").sval(' . json_encode($gudang) . ');
               find("#satuan").val("' . $data['satuan'] . '") ;
               find("#stock").val("' . $data['stock'] . '") ;
               find("#namastock").val("' . $data['namastock'] . '") ;
               find("#saldosistem").val("' . $data['qtysistem']. '") ;
               find("#saldofisik").val("' . $data['qtyfisik']. '") ;
               find("#selisih").val("' . $data['qtyselisih'] . '") ;
               find("#keterangan").val("' . $data['keterangan'] . '") ;
               find("#stock").attr("readonly", true);
                find("#cmdstock").attr("disabled", true);
            }
            bos.tradjstock.hitungselisih();
            bos.tradjstock.settab(1) ;
         ');
        }
    }

    public function deleting()
    {
        $va = $this->input->post();
        $error = $this->tradjstock_m->deleting($va['faktur']);
        if($error == "ok"){
            echo ('bos.tradjstock.grid1_reloaddata() ; ');
        }else{
            echo('
                alert("'.$error.'");
            ');
        }
        

    }

    public function getfaktur()
    {
        $faktur = $this->tradjstock_m->getfaktur(false);

        echo ('
        bos.tradjstock.obj.find("#faktur").val("' . $faktur . '") ;
        ');
    }
}
