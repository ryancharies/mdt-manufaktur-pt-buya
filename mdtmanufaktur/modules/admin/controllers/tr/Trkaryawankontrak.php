<?php
class Trkaryawankontrak extends Bismillah_Controller{
   public function __construct(){
		parent::__construct() ;
      $this->load->model('tr/trkaryawankontrak_m') ;
      $this->load->model('func/perhitunganhrd_m') ;
      $this->load->helper('bdate') ;
      $this->load->helper('bdir') ;
	}

   public function index(){
      $tglmin = $this->trkaryawankontrak_m->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
      $data = array();
      $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
      $this->load->view('tr/trkaryawankontrak',$data) ;
   }

   public function loadgrid(){
      $tglmin = $this->trkaryawankontrak_m->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
      $va     = json_decode($this->input->post('request'), true) ;
      $vare   = array() ;
      $vdb    = $this->trkaryawankontrak_m->loadgrid($va) ;
      $dbd    = $vdb['db'] ;
      while( $dbr = $this->trkaryawankontrak_m->getrow($dbd) ){
         $vaset   = $dbr ;
         $vaset['tgl'] = date_2d($vaset['tgl']);
         $vaset['tglawal'] = date_2d($vaset['tglawal']);
         $vaset['tglakhir'] = date_2d($vaset['tglakhir']);
         $vaset['lamaperiode'] = $vaset['lama'] ." ". getperiode($vaset['periode']);
         $vaset['cmdcetakpk']    = '<button type="button" onClick="bos.trkaryawankontrak.cmdcetakpk(\''.$dbr['noperjanjian'].'\')"
                           class="btn btn-warning btn-grid">Cetak Kontrak</button>' ;
         $vaset['cmdedit']    = '<button type="button" onClick="bos.trkaryawankontrak.cmdedit(\''.$dbr['noperjanjian'].'\')"
                           class="btn btn-default btn-grid">Edit</button>' ;
         $vaset['cmddelete']    = '<button type="button" onClick="bos.trkaryawankontrak.cmddelete(\''.$dbr['noperjanjian'].'\')"
                           class="btn btn-danger btn-grid">Hapus</button>' ;
         
         $vaset['cmdcetakpk']	   = html_entity_decode($vaset['cmdcetakpk']) ;
         $vaset['cmdedit']	   = html_entity_decode($vaset['cmdedit']) ;
         $vaset['cmddelete']	   = html_entity_decode($vaset['cmddelete']) ;
         
         if ($tglmin > date_2s($dbr['tgl'])) {
            $vaset['cmdedit'] = "";
            $vaset['cmddelete'] = "";
         }

         $vare[]		= $vaset ;
      }

      $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
      echo(json_encode($vare)) ;
   }

   public function init(){
      savesession($this, "sskaryawankontrak_noperjanjian", "") ;
   }

   public function saving(){
      $va 	= $this->input->post() ;
      $error = $this->trkaryawankontrak_m->saving($va) ;
      if($error == "ok"){
         echo(' 
            alert("Data berhasil disimpan!!");
            bos.trkaryawankontrak.settab(0) ;  
            ') ;
      }else{
         echo('alert("'.$error.'");');
      }
      
   }
   
   public function editing(){
      $va 	= $this->input->post() ;
      $noperjanjian = $va['noperjanjian'] ;
      $data = $this->trkaryawankontrak_m->getdata($noperjanjian) ;
      if($dbr = $this->trkaryawankontrak_m->getrow($data)){
         //jab bag
         $tgl  = $dbr['tgl'];

         $bag = $this->perhitunganhrd_m->getbagian($dbr['kode'],$tgl);
         $jab = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$tgl);
         $bagjab = $bag['keterangan']." - ".$jab['keterangan'];
         savesession($this, "sskaryawankontrak_noperjanjian", $noperjanjian) ;
         $periode[] = array("id" => $dbr['periode'], "text" => getperiode($dbr['periode']));
         $jabatan[] = array("id" => $dbr['jabatan'], "text" => $dbr['ketjabatan']);
         $bagian[] = array("id" => $dbr['bagian'], "text" => $dbr['ketbagian']);
         echo('
            with(bos.trkaryawankontrak.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#kode").val("'.$dbr['kode'].'") ;
               find("#nama").val("'.$dbr['nama'].'").focus() ;
               find("#ktp").val("'.$dbr['ktp'].'") ;
               find("#notelepon").val("'.$dbr['telepon'].'") ;
               find("#alamat").val("'.$dbr['alamat'].'") ;
               find("#bagjab").val("'.$bagjab.'") ;
               
               find("#noperjanjian").val("'.$dbr['noperjanjian'].'") ;
               find("#nomor").val("'.$dbr['nomor'].'") ;
               find("#tgl").val("'.date_2d($dbr['tgl']).'") ;
               find("#lama").val("'.$dbr['lama'].'") ;
               find("#periode").sval('.json_encode($periode).') ;
               find("#tglpkawal").val("'.date_2d($dbr['tglawal']).'") ;
               find("#tglpkakhir").val("'.date_2d($dbr['tglakhir']).'") ;
               find("#bagian").sval('.json_encode($bagian).') ;
               find("#jabatan").sval('.json_encode($jabatan).') ;
            }
            
         ') ;

         //load foto
         $img = "";
         $kodefoto = "ftkaryawan~".$dbr['kode'];
         $dbdf = $this->trkaryawankontrak_m->getfoto($kodefoto) ;
         while( $dbrf = $this->trkaryawankontrak_m->getrow($dbdf) ){
             $urlfile = $dbrf['urlfile'];
             $checked = "";
             $id = md5($urlfile);
             if($dbrf['fotoutama'] == "1")$checked = "checked";
             $img .= '<div id=\"idfotokaryawan_'.$id .'\" class=\'col-sm-4 \' style=\"margin:15px auto;border:1px black;height:100px;\"><br>';
             $img .= '<img src=\"'.base_url($urlfile . "?time=". time()).'\" class=\"img-responsive\" style=\"margin:0 auto;height:100px\"/>';
             $img .= '</div>';
         }

         echo('
             bos.trkaryawankontrak.obj.find("#fotobrowse").append("'.$img.'") ;
             bos.trkaryawankontrak.settab(1) ;
             ');

      }
   }

   public function pilihkaryawan(){
      $va = $this->input->post();
      //print_r($va);
      $kode = $va['kode'];
      $tgl = date("Y-m-d");
      $data = $this->perhitunganhrd_m->getdatakaryawan($kode,$tgl);
      
      if (!empty($data)) {

         $bag = $this->perhitunganhrd_m->getbagian($kode,$tgl);
         $jab = $this->perhitunganhrd_m->getjabatan($kode,$tgl);
         $bagjab = $bag['keterangan']." - ".$jab['keterangan'];

         echo ('
            with(bos.trkaryawankontrak.obj){
               find("#kode").val("' . $kode . '") ;
               find("#nama").val("' . $data['nama'] . '");
               find("#ktp").val("' . $data['ktp'] . '");
               find("#notelepon").val("' . $data['telepon'] . '");
               find("#alamat").val("' . $data['alamat'] . '");
               find("#bagjab").val("'.$bagjab.'") ;
               bos.trkaryawankontrak.loadmodelnip("hide");
            }
            //bos.trkaryawankontrak.getnopinjaman();
         ');
      }
   }

   public function loadgrid2(){
      $va = json_decode($this->input->post('request'), true);
      $vare = array();
      $vdb = $this->trkaryawankontrak_m->loadgrid2($va);
      $dbd = $vdb['db'];
      $tgl = date("Y-m-d");
      while ($dbr = $this->trkaryawankontrak_m->getrow($dbd)) {
         $vaset = $dbr;
         $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$tgl);
         $vaset['jabatan'] = $jabatan['keterangan'];
         $bagian = $this->perhitunganhrd_m->getbagian($dbr['kode'],$tgl);
         $vaset['bagian'] = $bagian['keterangan'];
            
         $vaset['cmdpilih'] = '<button type="button" onClick="bos.trkaryawankontrak.cmdpilih(\'' . $dbr['kode'] . '\')"
                           class="btn btn-success btn-grid">Pilih</button>';
         $vaset['cmdpilih'] = html_entity_decode($vaset['cmdpilih']);
         $vare[] = $vaset;
      }

      $vare = array("total" => $vdb['rows'], "records" => $vare);
      echo (json_encode($vare));
    }

    public function getnoperjanjian(){
      $va 	= $this->input->post() ;
      $tgl = $va['tgl'];
      $noperjanjian  = $this->trkaryawankontrak_m->getnoperjanjian($tgl,FALSE) ;

      echo('
         bos.trkaryawankontrak.obj.find("#noperjanjian").val("'.$noperjanjian.'") ;
      ') ;
   }

   public function gettglakhirkontrak(){
      $va 	= $this->input->post() ;
      $datetime = date_2s($va['tglawal'])." 00:00:00";
      $timeawal = strtotime($datetime) - (60*60*24);// di kurangi 1 hari agar lebih pas pada tgl jth tmp
      $datetimeawal = date("Y-m-d H:i:s",$timeawal);

      $lama = $va['lama'];
      $periode = $va['periode'];
      $jthtmp = $this->perhitunganhrd_m->getjthtmpbyperiode($datetimeawal,$lama,$periode);
      $tglakhir = date("d-m-Y",$jthtmp);
      echo('
         bos.trkaryawankontrak.obj.find("#tglpkakhir").val("'.$tglakhir.'") ;
      ') ;
   }

   public function seekjabatan(){
      $search     = $this->input->get('q');
      $vdb    = $this->trkaryawankontrak_m->seekjabatan($search) ;
      $dbd    = $vdb['db'] ;
      $vare   = array();
      while( $dbr = $this->trkaryawankontrak_m->getrow($dbd) ){
          $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      $Result = json_encode($vare);
      echo($Result) ;
   }
    
   public function seekbagian(){
      $search     = $this->input->get('q');
      $vdb    = $this->trkaryawankontrak_m->seekbagian($search) ;
      $dbd    = $vdb['db'] ;
      $vare   = array();
      while( $dbr = $this->trkaryawankontrak_m->getrow($dbd) ){
          $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
      }
      $Result = json_encode($vare);
      echo($Result) ;
   }

   public function seeksurat(){
      $search     = $this->input->get('q');
      $vdb    = $this->trkaryawankontrak_m->seeksurat($search) ;
      $dbd    = $vdb['db'] ;
      $vare   = array();
      while( $dbr = $this->trkaryawankontrak_m->getrow($dbd) ){
          $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['judul']) ;
      }
      $Result = json_encode($vare);
      echo($Result) ;
   }

   public function deleting(){
      $va 	= $this->input->post() ;
      $this->trkaryawankontrak_m->deleting($va['noperjanjian']) ;
      echo('
            alert("Data berhasil dihapus!!");
            bos.trkaryawankontrak.grid1_reloaddata() ;
         ') ;
   }

   public function cetakpk(){
      $va 	= $this->input->post() ;
      echo('
         with(bos.trkaryawankontrak.obj){
            find("#noperjanjianctk").val("'.$va['noperjanjian'].'") ;
         }
         bos.trkaryawankontrak.loadmodelcetakpk(true);
      ');
   }

   public function cetakpktodocx(){
      $va 	= $this->input->post() ;
      $zip = new ZipArchive();
      $file = md5(rand(0,10000) . time() . session_id());
      // Use same filename for "save" and different filename for "save as".
      $inputFilename = './uploads/surat/surat_'.$va['surat'].'.docx';
      $filecetak = './tmp/surat_'.$file.'.docx';
      copy($inputFilename,$filecetak);
      // Some new strings to put in the document.
      $token1 = 'Hello World!';
      $token2 = 'Your mother smelt of elderberries, and your father was a hamster!';

      // Open the Microsoft Word .docx file as if it were a zip file... because it is.
      if ($zip->open($filecetak, ZipArchive::CREATE)!==TRUE) {
         echo "Cannot open $inputFilename :( "; die;
      }

      // Fetch the document.xml file from the word subdirectory in the archive.
      $xml = $zip->getFromName('word/document.xml');

      $arrdata = array();
      $data = $this->trkaryawankontrak_m->getdata($va['noperjanjianctk']) ;
      if($dbr = $this->trkaryawankontrak_m->getrow($data)){
         $bag = $this->perhitunganhrd_m->getbagian($dbr['kode'],$dbr['tgl']);
         $jab = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$dbr['tgl']);

         
         $arrdata['nip'] = $dbr['kode'];
         $arrdata['nama'] = $dbr['nama'];
         $arrdata['ktp'] = $dbr['ktp'];
         $arrdata['notelepon'] = $dbr['telepon'];
         $arrdata['alamat'] = $dbr['alamat'];
         $arrdata['noperjanjian'] = $dbr['noperjanjian'];
         $arrdata['nomor'] = $dbr['nomor'];
         $arrdata['tgl'] = date_2d($dbr['tgl']);
         $arrtgl = date_2b($dbr['tgl']);
         $arrdata['tglfull'] = $arrtgl['day'].", ".$arrtgl['d']." ".$arrtgl['m']." ".$arrtgl['y'];

         $dbr['lama'] = intval($dbr['lama']);
         $arrdata['lama'] = $dbr['lama'];
         $arrdata['lamaterbilang'] = terbilang($dbr['lama']);
         $arrdata['periode'] = $dbr['periode'];
         $arrdata['tglpkawal'] = date_2d($dbr['tglawal']);
         $arrdata['tglpkakhir'] = date_2d($dbr['tglakhir']);
         $arrdata['bagian'] = $bag['keterangan'];
         $arrdata['jabatan'] = $bag['keterangan'];
      }

      // Replace the tokens.
      foreach($arrdata as $key => $val){
         $xml = str_replace('{'.$key.'}', $val, $xml);
      }

      // Write back to the document and close the object
      if(!empty($arrdata)){
         if ($zip->addFromString('word/document.xml', $xml)) { 
            echo('
               var a = document.getElementById("linkdownlaodfile");
               document.getElementById("linkdownlaodfile").href = "'.$filecetak.'" ;
               a.innerHTML = "Download File '. date("d-m-Y H:i:s") .'";         
            ');
         }else{ 
            echo('
               alert("Surat gagal dicetak!!");
            '); 
         }
      }else{
         echo('
               alert("Surat gagal dicetak!!\\n data tidak ditemukan... ");
            '); 
      }     

      $zip->close();
   }
}
?>
