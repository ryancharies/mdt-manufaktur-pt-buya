<?php
class Tragspinjamankaryawan extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('tr/tragspinjamankaryawan_m') ;
        $this->load->model('func/updtransaksi_m') ;
        $this->load->model('func/func_m') ;
        $this->load->model('func/perhitunganhrd_m') ;
        $this->load->library('escpos');
        $this->load->helper('bdate');
        $this->bdb = $this->tragspinjamankaryawan_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(),"mintgl"=>$tglmin) ;
        $this->load->view('tr/tragspinjamankaryawan',$d) ;
    }

    public function loadgrid(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare               = array() ;
        $va['tglawal']      = date_2s($va['tglawal']);
        $va['tglakhir']     = date_2s($va['tglakhir']);
        $vdb                = $this->tragspinjamankaryawan_m->loadgrid($va) ;
        $dbd                = $vdb['db'] ;
        $tgl = date("Y-m-d");
        while( $dbr = $this->tragspinjamankaryawan_m->getrow($dbd) ){
            $vaset                  = $dbr ;
            $vaset['tgl']           = date_2d($vaset['tgl']);
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['nip'],$tgl);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['nip'],$tgl);
            $vaset['bagian']  = $bagian['keterangan'];
            $vaset['jabatan']  = $jabatan['keterangan'];
            $vaset['cmdedit']       = '<button type="button" onClick="bos.tragspinjamankaryawan.cmdedit(\''.$dbr['faktur'].'\')"
                                        class="btn btn-default btn-grid">Edit</button>' ;
            $vaset['cmddelete']     = '<button type="button" onClick="bos.tragspinjamankaryawan.cmddelete(\''.$dbr['faktur'].'\')"
                                         class="btn btn-danger btn-grid">Delete</button>' ;
            $vaset['cmdcetak']    = '<button type="button" onClick="bos.tragspinjamankaryawan.cmdcetak(\''.$dbr['faktur'].'\')"
                                         class="btn btn-warning btn-grid">Cetak</button>' ;
            // $vaset['cmdcetakdm']    = '<button type="button" onClick="bos.tragspinjamankaryawan.cmdcetakdm(\''.$dbr['faktur'].'\')"
            //                              class="btn btn-primary btn-grid">Cetak DM</button>' ;
            // $vaset['cmdcetakdm']    = html_entity_decode($vaset['cmdcetakdm']) ;
            $vaset['cmdcetak']    = html_entity_decode($vaset['cmdcetak']) ;
            $vaset['cmdedit']       = html_entity_decode($vaset['cmdedit']) ;
            $vaset['cmddelete']	    = html_entity_decode($vaset['cmddelete']) ;
            if ($tglmin > date_2s($dbr['tgl'])) {
                $vaset['cmdedit'] = "";
                $vaset['cmddelete'] = "";
            }
            $vare[] = $vaset ;
        }
        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "ssagspinjamankaryawan_faktur", "") ;
    }

    public function saving(){
        $va 	     = $this->input->post() ;
        $faktur 	 = getsession($this, "ssagspinjamankaryawan_faktur") ;

        $this->tragspinjamankaryawan_m->saving($faktur, $va) ;
        echo(' bos.tragspinjamankaryawan.settab(0) ;  ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $faktur = $va['faktur'] ;
        $data   = $this->tragspinjamankaryawan_m->getdatatotal($faktur) ;
        if(!empty($data)){
            savesession($this, "ssagspinjamankaryawan_faktur", $faktur) ;
            $jabatan = $this->perhitunganhrd_m->getjabatan($data['nip'],$data['tgl']);
            $bagian = $this->perhitunganhrd_m->getbagian($data['nip'],$data['tgl']);
            echo('
            w2ui["bos-form-tragspinjamankaryawan_grid2"].clear();
            with(bos.tragspinjamankaryawan.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#nopinjaman").val("'.$data['nopinjaman'].'") ;
               find("#nip").val("'.$data['nip'].'") ;
               find("#namakaryawan").val("'.$data['nama'].'") ;
               find("#jabatan").val("'.$jabatan['keterangan'].'") ;
               find("#bagian").val("'.$bagian['keterangan'].'") ;
               find("#plafond").val("'.string_2s($data['plafond']).'") ;
               find("#lama").val("'.string_2s($data['lama']).'") ;
               find("#bakidebetawal").val("'.string_2s($data['bakidebet']).'") ;
               find("#bakidebetakhir").val("'.string_2s($data['bakidebet'] - $data['pokok']).'") ;

               find("#faktur").val("'.$data['faktur'].'") ;
               find("#keterangan").val("'.$data['keterangan'].'") ;
               find("#tgl").val("'.date_2d($data['tgl']).'");
               find("#pokok").val("'.string_2s($data['pokok'],2).'") ;

            }


         ') ;

            //loadgrid detail
            $vare = array();
            $dbd = $this->tragspinjamankaryawan_m->getdatadetail($faktur) ;
            $n = 0 ;
            while( $dbr = $this->tragspinjamankaryawan_m->getrow($dbd) ){
                $n++;
                $vaset   = $dbr ;
                $vaset['recid'] = $n;
                $vaset['no'] = $n;
                $vaset['cmddelete'] = '<button type="button" onClick="bos.tragspinjamankaryawan.grid2_deleterow('.$n.')"
                                        class="btn btn-danger btn-grid">Delete</button>' ;
                $vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

                $vare[]             = $vaset ;
            }
            $vare = json_encode($vare);
            echo('
                w2ui["bos-form-tragspinjamankaryawan_grid2"].add('.$vare.');
                bos.tragspinjamankaryawan.initdetail();
                bos.tragspinjamankaryawan.settab(1) ;
            ');
        }
    }
    public function deleting(){
        $va 	= $this->input->post() ;
        $this->tragspinjamankaryawan_m->deleting($va['faktur']) ;
        echo('bos.tragspinjamankaryawan.grid1_reloaddata() ; ') ;

    }

    public function loadgrid3()
    {
        $va = json_decode($this->input->post('request'), true);
        $vare = array();
        $vdb = $this->tragspinjamankaryawan_m->loadgrid3($va);
        $dbd = $vdb['db'];
        $tgl = $va['tgl'];
        while ($dbr = $this->tragspinjamankaryawan_m->getrow($dbd)) {
            $vaset = $dbr;
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['nip'],$tgl);
            $vaset['jabatan'] = $jabatan['keterangan'];
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['nip'],$tgl);
            $vaset['bagian'] = $bagian['keterangan'];
            
            $vaset['cmdpilih'] = '<button type="button" onClick="bos.tragspinjamankaryawan.cmdpilih(\'' . $dbr['nopinjaman'] . '\')"
                           class="btn btn-success btn-grid">Pilih</button>';
            $vaset['cmdpilih'] = html_entity_decode($vaset['cmdpilih']);
            $vare[] = $vaset;
        }

        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function seekrekening(){
        $search     = $this->input->get('q');
        $vdb    = $this->tragspinjamankaryawan_m->seekrekening($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->tragspinjamankaryawan_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] . " - " . $dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

    public function pilihnopinjaman()
    {
        $va = $this->input->post();
        //print_r($va);
        $dbd = $this->tragspinjamankaryawan_m->getdatanopinjaman($va);
        $tgl = $va['nopinjaman'];
        if ($dbr = $this->bdb->getrow($dbd)) {
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['nip'],$tgl);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['nip'],$tgl);
            echo ('
            with(bos.tragspinjamankaryawan.obj){
                find("#nopinjaman").val("' . $dbr['nopinjaman'] . '") ;
                find("#nip").val("' . $dbr['nip'] . '") ;
                find("#namakaryawan").val("' . $dbr['nama'] . '");
                find("#jabatan").val("' . $jabatan['keterangan'] . '");
                find("#bagian").val("' . $bagian['keterangan'] . '");
                find("#plafond").val("' . string_2s($dbr['plafond']) . '") ;
                find("#lama").val("' . string_2s($dbr['lama']) . '") ;
                find("#bakidebetawal").val("' . string_2s($dbr['bakidebet']) . '") ;
                find("#tglrealisasi").val("' . date_2d($dbr['tglrealisasi']) . '") ;
                find("#tglawalags").val("' . date_2d($dbr['tglawalags']) . '") ;
                bos.tragspinjamankaryawan.loadmodelnopinjaman("hide");
            }
         ');
        }
    }

    public function getfaktur(){
        $va 	= $this->input->post() ;
        $faktur  = $this->tragspinjamankaryawan_m->getfaktur(FALSE) ;

        echo('
        bos.tragspinjamankaryawan.obj.find("#faktur").val("'.$faktur.'") ;
        ') ;
    }

    public function showreport(){
        $va 	= $this->input->get() ;
        $faktur = $va['faktur'] ;
        $data   = $this->tragspinjamankaryawan_m->getdatatotal($faktur) ;
        if(!empty($data)){
            $font = 10 ;
            $now  = date_2b(date("Y-m-d")) ;
            $kota = $this->bdb->getconfig("kota") . ", " . $now['d'] . ' ' . $now['m'] . ' ' . $now['y'];
            $vttd = array() ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=> $kota ,"5"=>"") ;
            //$vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"Dibuat,","3"=>"","4"=>"Mengetahui,","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"","3"=>"","4"=>"","5"=>"") ;
            $vttd[] = array("1"=>"","2"=>"(.......................)","3"=>"","4"=>"(.......................)","5"=>"") ;


            $jabatan = $this->perhitunganhrd_m->getjabatan($data['nip'],$data['tgl']);
            $bagian = $this->perhitunganhrd_m->getbagian($data['nip'],$data['tgl']);

            $vDetail = array() ;
            $vDetail[] = array("1"=>"No Pinjaman","2"=>":","3"=>$data['nopinjaman'],"4"=>"","5"=>"Faktur","6"=>":","7"=>$data['faktur']);
            $vDetail[] = array("1"=>"NIP","2"=>":","3"=>$data['nip'],"4"=>"","5"=>"Tgl","6"=>":","7"=>date_2d($data['tgl']));
            $vDetail[] = array("1"=>"Nama","2"=>":","3"=>$data['nama'],"4"=>"","5"=>"Pokok","6"=>":","7"=>string_2s($data['pokok']));
            $vDetail[] = array("1"=>"Jabatan","2"=>":","3"=>$jabatan['keterangan'],"4"=>"","5"=>"Keterangan","6"=>":","7"=>$data['keterangan']);
            $vDetail[] = array("1"=>"Bagian","2"=>":","3"=>$bagian['keterangan'],"4"=>"","5"=>"","6"=>"","7"=>"");
            $vTitle[] = array("capt"=>" Angsuran Pinjaman Karyawan ") ;

            //detail
            $dbd = $this->tragspinjamankaryawan_m->getdatadetail($faktur) ;
            $n = 0 ;
            $array = array();

            while( $dbr = $this->tragspinjamankaryawan_m->getrow($dbd)){
                $n++;
                $array[] = array("No"=>$n,"Rekening"=>$dbr['rekening'],"Ket Rekening"=>$dbr['ketrek'],"Jumlah"=>string_2s($dbr['nominal']));
            }


            $o    = array('paper'=>'A4', 'orientation'=>'p', 'export'=>(isset($va['export']) ? $va['export'] : 0 ),
                          'opt'=>array('export_name'=>'agspinjamankaryawan','mtop'=>1) ) ;
            $this->load->library('bospdf', $o) ;
            //$this->bospdf->ezSetMargins(1,1,20,20);
            //$this->bospdf->ezSetCmMargins(0, 1, 1, 1);
            //$this->bospdf->ezImage("./uploads/HeaderSJDO.jpg",true,'60','190','50');
            //$this->bospdf->ezLogoHeaderPage("./uploads/HeaderSJDO.jpg",'0','65','150','50');
            //$this->bospdf->ezHeader("<b>DELIVERY ORDER (DO)</b>",array("justification"=>"center")) ;
            //$this->bospdf->ezText("") ;

            //ketika width tidak di set maka akan menyesuaikan dengan lebar kertas

            $this->bospdf->ezTable($vDetail,"","",
                                   array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "1"=>array("width"=>15,"justification"=>"left"),
                                             "2"=>array("width"=>5,"justification"=>"left"),
                                             "3"=>array("justification"=>"left"),
                                             "4"=>array("width"=>10,"justification"=>"left"),
                                             "5"=>array("width"=>15,"justification"=>"left"),
                                             "6"=>array("width"=>5,"justification"=>"left","wrap"=>1),
                                             "7"=>array("justification"=>"left","wrap"=>1))
                                        )
                                  ) ;

            $this->bospdf->ezTable($vTitle,"","",
                                   array("fontSize"=>$font+3,"showHeadings"=>0,"showLines"=>0,
                                         "cols"=> array(
                                             "capt"=>array("justification"=>"center"),)
                                        )
                                  ) ;

            $this->bospdf->ezText("") ;
            $this->bospdf->ezTable($array,"","",
                                   array("fontSize"=>$font,"showLines"=>1,
                                         "cols"=> array(
                                             "No"            =>array("width"=>5,"justification"=>"right"),
                                             "Rekening"  	=>array("width"=>15,"justification"=>"left"),
                                             "Ket Rekening"   =>array("wrap"=>1),
                                             "Jumlah"          =>array("width"=>12,"justification"=>"right")))
                                  ) ;

         $this->bospdf->ezText("") ;
         $this->bospdf->ezText("") ;
         $this->bospdf->ezTable($vttd,"","",
                                 array("fontSize"=>$font,"showHeadings"=>0,"showLines"=>0,
                                       "cols"=> array(
                                          "1"=>array("justification"=>"right"),
                                          "2"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "3"=>array("width"=>40,"wrap"=>1),
                                          "4"=>array("width"=>25,"wrap"=>1,"justification"=>"center"),
                                          "5"=>array("wrap"=>1,"justification"=>"center"))
                                 )
                              ) ;


            $this->bospdf->ezStream() ;
        }else{
            echo("data tidak ada !!!");
        }

    }

    public function printbuktibm(){
        //gambar yg diambil di display none dulu di frame, kayaknya perlu untuk pancingan aja

        $va 	= $this->input->post() ;
        $dbr    = $this->bdb->getdatatotal($va['faktur']) ;
        if(!empty($dbr)){

            $html   ="";
            $html   .="<table width = 100%>";
            $html   .=" <tr><td width = 100%>";
            $html   .="     <table width = 100%>";
            $html   .="         <tr>";
            $html   .="             <td width=65%><img src =\ ./uploads/header.jpg \ width=100%></td>";
            $html   .="             <td >&nbsp;</td>";
            $html   .="             <td width=30%>";
            $html   .="                 <table width=100%>";
            $html   .="                     <tr><td>Kudus ".date_2d($dbr['tgl'])."</td></tr>";
            $html   .="                     <tr><td>No BM ".$va['faktur']."</td></tr>";
            $html   .="                 </table>";
            $html   .="             </td>";
            $html   .="         </tr>";
            $html   .="     </table>";
            $html   .=" </td></tr>";
            $html   .=" <tr><td width = 100% align=center><h2>BUKTI BANK MASUK</h2></td></tr>";
            $html   .=" <tr><td width = 100%>";
            $html   .="     <table width = 100%>";
            $html   .="         <tr><td width = 150px valign=top>Diterima Dari</td><td valign=top width=5px>:</td><td>".$dbr['diberiterima']."</td></tr>";
            $html   .="         <tr><td width = 150px valign=top>Sejumlah</td><td valign=top width=5px>:</td><td>".string_2n($dbr['debet'])."</td></tr>";
            $html   .="         <tr><td width = 150px valign=top>Terbilang</td><td valign=top width=5px>:</td><td>".terbilang($dbr['debet']) ."&nbsp;rupiah</td></tr>";
            $html   .="         <tr><td width = 150px valign=top>Untuk Pembayaran</td><td valign=top width=5px>:</td><td>".$dbr['keterangan']."</td></tr>";
            $html   .="     </table>";
            $html   .=" </td></tr>";
            $html   .=" <tr><td width = 100%>";
            $html   .="     <table width = 100%>";
            $html   .="         <tr><td align = center>Yang Menerima</td><td></td><td align = center>Bagian Keuangan</td></tr>";
            $html   .="         <tr><td height=60px></td><td></td><td></td></tr>";
            $html   .="         <tr><td align = center>...............</td><td></td><td align = center>...............</td></tr>";
            $html   .="     </table>";
            $html   .=" </td></tr>";
            $html   .="</table>";
            echo('

            bos.tragspinjamankaryawan.printbukti("'.$html.'");
            ');
        }else{
            echo('alert("Data tidak ada !!!");');
        }

    }
}
?>
