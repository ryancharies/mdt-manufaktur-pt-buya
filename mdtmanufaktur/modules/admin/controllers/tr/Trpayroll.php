<?php
class Trpayroll extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("tr/trpayroll_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->load->model("func/updtransaksi_m") ;
        $this->bdb 	= $this->trpayroll_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";

        // $arrlembur = $this->perhitunganhrd_m->getlembur("2018000012","2022-09-21","2022-10-24");
        // print_r($arrlembur);

        $arrkolom = array();/*
        $arrkolom[] = array("field"=> "kode", "caption"=> "NIP", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "nama", "caption"=> "Nama", "size"=> "200px", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "bagian", "caption"=> "Bagian", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);
        $arrkolom[] = array("field"=> "jabatan", "caption"=> "Jabatan", "size"=> "100px", "style"=> "text-align: center", "sortable"=>false,"frozen"=>true);


        //$kodepinjaman = $this->trpayroll_m->getconfig("kogapypinjkaryawan");
        $where = "(periode = 'J' or periode = 'H') and status = '1' and perhitungan <> 'K'";
        $dbd = $this->trpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
        while($dbr = $this->trpayroll_m->getrow($dbd)){
            $arrkolom[] = array("field"=> "hk_".$dbr['kode'], "caption"=> "HK. ".$dbr['keterangan'], "size"=> "70px", "render"=> "float:2", "sortable"=>false);    
        }

       // $kodepinjaman = $this->trpayroll_m->getconfig("kogapypinjkaryawan");
        $where = "(periode = 'J' or periode = 'H') and status = '1' and perhitungan <> 'K'";
        $dbd = $this->trpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
        while($dbr = $this->trpayroll_m->getrow($dbd)){
            $arrkolom[] = array("field"=> "uh_".$dbr['kode'], "caption"=> "UH. ".$dbr['keterangan'], "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        }

        $where = "(periode = 'J' or periode = 'H' or periode = 'B') and status = '1' and perhitungan <> 'K'";
        $dbd = $this->trpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
        while($dbr = $this->trpayroll_m->getrow($dbd)){
            $arrkolom[] = array("field"=> "ub_".$dbr['kode'], "caption"=> "UB. ".$dbr['keterangan'], "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        }

        $arrkolom[] = array("field"=> "gajikotor", "caption"=> "Upah Kotor", "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        $arrkolom[] = array("field"=> "pinjamankaryawan", "caption"=> "BON", "size"=> "100px", "render"=> "float:2", "sortable"=>false);
        $arrkolom[] = array("field"=> "gajibersih", "caption"=> "Upah Bersih", "size"=> "100px", "render"=> "float:2", "sortable"=>false);
*/
        $data['kolomgrid1'] = json_encode($arrkolom);
        $this->load->view("tr/trpayroll",$data) ;

    } 

    public function initgrid1(){

        $va = $this->input->post() ;

        //cek status sudah posting atau belum
        $sudahposting = false;
        $cabang = getsession($this, "cabang");
        $dbd2 = $this->trpayroll_m->select("payroll_posting","faktur","periode = '{$va['periode']}' and cabang = '$cabang' and status = '1'");
        if($this->trpayroll_m->rows($dbd2) > 0){
            $sudahposting = true;
        }

        //$sudahposting = true;
        if($sudahposting){
            $arrkolom = $this->perhitunganhrd_m->getkolomsudahposting($va['periode']);
        }else{
            $arrkolom = $this->perhitunganhrd_m->getkolombelumposting();
        }


        $arrkolom = json_encode($arrkolom);
        echo('
            bos.trpayroll.grid1_col = '.$arrkolom.';

            bos.trpayroll.loadpembayaran();
            bos.trpayroll.grid1_destroy() ;
            bos.trpayroll.grid1_loaddata();
            bos.trpayroll.grid1_load() ;
        ');
    }

    public function loadgrid(){
        $va = json_decode($this->input->post('request'), true);
        //print_r($va);
        $vare   = array() ;
        $vdb    = $this->trpayroll_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;

        //cek status sudah posting atau belum
        $sudahposting = false;
        $cabang = getsession($this, "cabang");
        $dbd2 = $this->trpayroll_m->select("payroll_posting","faktur","periode = '{$va['periode']}' and cabang = '$cabang' and status = '1'");
        if($this->trpayroll_m->rows($dbd2) > 0){
            $sudahposting = true;
        }

        //load data komponen payroll
        $datakomp = array();
        $kodepinjaman = $this->bdb->getconfig("kogapypinjkaryawan");
        if($sudahposting){
            $where = "p.payroll <> '$kodepinjaman' and t.status = '1' and t.periode = '{$va['periode']}'";//(k.periode = 'J' or k.periode = 'H') and 
            $join = "left join payroll_komponen k on k.kode = p.payroll left join payroll_posting t on t.faktur = p.faktur";
            $dbd2 = $this->trpayroll_m->select("payroll_posting_komponen p","k.kode,k.keterangan,k.periode,k.perhitungan,k.potongan",$where,$join,"k.kode");
            while($dbr2 = $this->trpayroll_m->getrow($dbd2)){
                $datakomp[$dbr2['kode']] = $dbr2;
            }
        }else{  
            $where = "status = '1' and kode <> '$kodepinjaman'";
            $dbd2 = $this->trpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
            while($dbr2 = $this->trpayroll_m->getrow($dbd2)){
                $datakomp[$dbr2['kode']] = $dbr2;
            }
        }

        $total = array();
        $total['recid'] = "ZZZZZ" ;
        $total['nama'] = "Total" ;
        $total['gajibersih'] = 0 ;
        $total['pinjamankaryawan'] = 0 ;
        $total['gajikotor'] = 0 ;

        while( $dbr = $this->trpayroll_m->getrow($dbd) ){
            $n++;

            //jab bag
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['kode'],$va['tglakhir']);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['kode'],$va['tglakhir']);

            $vaset = $dbr;
            $gajikotor = 0 ;
            $vaset['recid'] = $dbr['id']; 
            $vaset['jabatan'] = $jabatan['keterangan'];
            $vaset['bagian'] = $bagian['keterangan'];

            // $kodepinjaman = $this->trpayroll_m->getconfig("kogapypinjkaryawan");
            // $where = "status = '1' and perhitungan <> 'K'";
            // $dbd2 = $this->trpayroll_m->select("payroll_komponen","kode,keterangan,periode,perhitungan,potongan",$where);
            // while($dbr2 = $this->trpayroll_m->getrow($dbd2)){
            foreach($datakomp as $key => $dbr2){
                if($sudahposting){
                    $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawanposting($dbr['kode'],$dbr2['kode'],$va['periode']);
                   // print_r($payroll);
                }else{
                    $payroll = $this->perhitunganhrd_m->getpayrollkomponenkaryawan($dbr['kode'],$dbr2['kode'],$va['periode']);
                }
                

                if($dbr2['potongan'] == "1"){
                    $payroll['nominal'] *=-1;
                    $payroll['total'] *=-1;
                }
                if($dbr2['periode'] == "J" or $dbr2['periode'] == "H"){
                    $vaset["hk_".$dbr2['kode']] = $payroll['jmlskala'];    
                    $vaset["uh_".$dbr2['kode']] = $payroll['nominal'];              
                                 
                }
                $vaset["ub_".$dbr2['kode']] = $payroll['total']; 
                
                if(!isset($total["ub_".$dbr2['kode']]))$total["ub_".$dbr2['kode']] = 0 ;
                $total["ub_".$dbr2['kode']] += $vaset["ub_".$dbr2['kode']];
                
                $gajikotor += $payroll['total'] ;
                
            }

            $vaset['gajikotor'] = $gajikotor;

            //cek pinjaman karaywan
            if($sudahposting){
                $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjamanposting($dbr['kode'],$va['periode']);
            }else{
                $kasbon = $this->perhitunganhrd_m->getpayrollpotangsuranpinjaman($dbr['kode'],$va['periode']);
            }
            
            $vaset['pinjamankaryawan'] = $kasbon['total'] * -1;
            $vaset['gajibersih'] = $gajikotor - $kasbon['total'];
            $vare[]		= $vaset ;

            $total['gajikotor'] += $vaset['gajikotor'];
            $total['pinjamankaryawan'] += $vaset['pinjamankaryawan'];
            $total['gajibersih'] += $vaset['gajibersih'];
        }
        $total['w2ui']= array("summary"=> true);
        $vare[] = $total;

        $vare = array("total" => $vdb['rows'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function init(){
        savesession($this, "sstrpayroll_kode", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $error = $this->bdb->saving($va) ;
        if($error == ""){
            echo(' 
                bos.trpayroll.init() ;  
                bos.trpayroll.loadperiode();
                alert("Payroll/ gaji berhasil diposting.....");
                ') ;
        }else{
            echo('
                alert("'.$error.'");
                
                ') ;
        }
        
    }

    public function seekperiode(){
        $search     = $this->input->get('q');
        $vdb    = $this->trpayroll_m->seekperiode($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trpayroll_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ". $dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
   }

   public function loadperiode(){
        $va 	= $this->input->post() ;
        $vdb    = $this->trpayroll_m->getperiode($va) ;
        if( $dbr = $this->trpayroll_m->getrow($vdb['db']) ){
            $status = "-- Belum diposting --";
            $cabang = getsession($this, "cabang");
            $dbd2 = $this->trpayroll_m->select("payroll_posting","faktur","periode = '{$va['periode']}' and cabang = '$cabang' and status = '1'");
            if($this->trpayroll_m->rows($dbd2) > 0){
                $status = "Sudah diposting";
            }
            echo('
                with(bos.trpayroll.obj){
                    find("#tglawal").val("'.date_2d($dbr['tglawal']).'") ;
                    find("#tglakhir").val("'.date_2d($dbr['tglakhir']).'") ;
                    find("#status").val("'.$status.'") ;
                }
            ') ;
            
            if($status == "Sudah diposting"){
                echo('
                    with(bos.trpayroll.obj){
                        find("#cmdposting").attr("disabled", true);
                        find("#cmdbatalposting").attr("disabled", false);
                    }
                ') ;                
            }else{
                echo('
                    with(bos.trpayroll.obj){
                        find("#cmdposting").attr("disabled", false);
                        find("#cmdbatalposting").attr("disabled", true);
                    }
                ') ;  
            }
             
        }
   }

   public function seekrekening(){
        $search     = $this->input->get('q');
        $vdb    = $this->trpayroll_m->seekrekening($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trpayroll_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] . " - " . $dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }

    public function loadpembayaran(){
        $va 	= $this->input->post() ;
        $cabang = getsession($this, "cabang");
        $faktur = "";
        $total = 0;
        $dbd2 = $this->trpayroll_m->select("payroll_posting","faktur","periode = '{$va['periode']}' and cabang = '$cabang' and status = '1'");
        if($dbr2 = $this->trpayroll_m->getrow($dbd2)){
            $faktur = $dbr2['faktur'];

            //loadgrid pembayaran
            $vare = array();
            $dbd = $this->trpayroll_m->getpembayaran($faktur) ;
            $n = 0 ;
            
            while( $dbr = $this->trpayroll_m->getrow($dbd) ){
                $n++;
                $vaset   = $dbr ;
                $vaset['recid'] = $n;
                $vaset['no'] = $n;
                $vaset['cmddelete'] = '<button type="button" onClick="bos.trpayroll.grid2_deleterow('.$n.')"
                                        class="btn btn-danger btn-grid">Delete</button>' ;
                $vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;

                $vare[]             = $vaset ;
                $total += $dbr['nominal'];
            }

            
        }
        $vare[] = array("recid"=>"ZZZZ","no"=>"","kode"=>"","ketketerangan"=>"Total","nominal"=>$total,"w2ui"=>array("summary"=>true));

        $vare = json_encode($vare);
            echo('
                w2ui["bos-form-trpayroll_grid2"].add('.$vare.');
                
           
            bos.trpayroll.initdetail();
        ');
    }

    public function batalposting(){
        $va 	= $this->input->post() ;
        $cabang = getsession($this, "cabang");
        $faktur = "";
        $dbd2 = $this->trpayroll_m->select("payroll_posting","faktur","periode = '{$va['periode']}' and cabang = '$cabang' and status = '1'");
        if($dbr2 = $this->trpayroll_m->getrow($dbd2)){
            $faktur = $dbr2['faktur'];

            $this->trpayroll_m->edit("payroll_posting",array("status"=>"2"),"faktur = '$faktur'");
            $this->trpayroll_m->edit("payroll_pinjaman_karyawan_angsuran",array("status"=>"0","faktur"=>""),"faktur = '$faktur'");
            $this->trpayroll_m->delete("pinjaman_karyawan_kartu","faktur = '$faktur'");
            $this->trpayroll_m->delete("keuangan_bukubesar","faktur = '$faktur'");
            echo('
                alert("Payroll/ gaji berhasil dibatal posting.....");
            ');
        }else{
            echo('
                alert("Payroll/ gaji berhasil dibatal posting.....");
            ');
        }

        echo('
            bos.trpayroll.init() ;  
            
            bos.trpayroll.loadperiode();

            bos.trpayroll.loadpembayaran();

            bos.trpayroll.grid1_destroy() ;
            bos.trpayroll.grid1_loaddata();
            bos.trpayroll.grid1_load() ;
        ');
    }
}
?>
