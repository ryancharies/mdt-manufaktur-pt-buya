<?php

class trbatalproduksiselesai extends Bismillah_Controller
{
    protected $bdb;
    protected $ss;
    protected $abc;
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('bdate');
        $this->load->model('tr/trbatalproduksiselesai_m');
        $this->bdb = $this->trbatalproduksiselesai_m;
        $this->ss = "sstrbatalproduksiselesai_";
    }

    public function index()
    {
        $d = array("setdate" => date_set());
        $this->load->view('tr/trbatalproduksiselesai', $d);
    }

    public function loadgrid_where($bs, $s){
        $this->duser();

        $this->db->where('h.tgl >=', $bs['tglawal']);
        $this->db->where('h.tgl <=', $bs['tglakhir']);

        
        if($bs['skd_cabang'] !== 'null'){
            $bs['skd_cabang'] = json_decode($bs['skd_cabang'],true);
            $this->db->group_start();
            foreach($bs['skd_cabang'] as $kdc){
                $this->db->or_where('h.cabang',$kdc);
            }
            $this->db->group_end();

        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['cabang'])){
                    $this->db->group_start();
                    foreach($this->aruser['data_var']['cabang'] as $kdc){
                        $this->db->or_where('h.cabang', $kdc);
                    }
                    $this->db->group_end();
                }
            }
        }

        if($s !== ''){
            $this->db->group_start();
                $this->db->or_like(array('h.faktur'=>$s,'h.fakturproduksi'=>$s));
            $this->db->group_end();
        }

        $this->db->where('h.status',"1");

    }

    public function loadgrid()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi", "", "", "tgl asc");
        $va = json_decode($this->input->post('request'), true);
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->db->escape_like_str($search) ;
        $vare = array();
        $vkaru = array();
        $va['tglawal'] = date_2s($va['tglawal']);
        $va['tglakhir'] = date_2s($va['tglakhir']);

        $this->loadgrid_where($va, $search);
        $f = "count(h.id) jml";
        $dbdt = $this->db->select($f)
            ->from("produksi_hasil h")
            ->join("cabang c","c.kode = h.cabang","left")
            ->join("produksi_total t","t.faktur = h.fakturproduksi","left")
            ->join("produksi_produk p","p.fakturproduksi = t.faktur","left")
            ->get();
        $rtot  = $dbdt->row_array();
        if($rtot['jml'] > 0){

            $this->loadgrid_where($va, $search);
            $f2 = "h.faktur,h.tgl,h.fakturproduksi,h.qty as qtyaktual,(h.qty * h.hp) as aktual,t.regu,t.petugas,h.cabang,
                    t.hargapokok as std,sum(p.qty) as qtystd";
            $dbd = $this->db->select($f2)
                ->from("produksi_hasil h")
                ->join("cabang c","c.kode = h.cabang","left")
                ->join("produksi_total t","t.faktur = h.fakturproduksi","left")
                ->join("produksi_produk p","p.fakturproduksi = t.faktur","left")
                ->group_by("h.faktur")
                ->limit($limit)
                ->get();
        
            foreach($dbd->result_array() as $dbr){  
                $vaset = $dbr;
                $vaset['tgl'] = date_2d($vaset['tgl']);
                $vaset['karu'] = $dbr['petugas'];
                if($dbr['regu'] !== null){
                    $regu = json_decode($dbr['regu'],true);
                    $vaset['regu'] = $regu['keterangan'];
                    $vkaru[$regu['karu']] = $this->bdb->getval("nama", "kode = '{$regu['karu']}'", "karyawan");
                    $vaset['karu'] = $vkaru[$regu['karu']];
                }
                $vaset['cmdPreview'] = '<button type="button" onClick="bos.trbatalproduksiselesai.cmdpreviewdetail(\'' . $dbr['faktur'] . '\')"
                                         class="btn btn-success btn-grid">Preview Detail</button>';
                $vaset['cmdPreview'] = html_entity_decode($vaset['cmdPreview']);
                $vaset['cmdBatal'] = '<button type="button" onClick="bos.trbatalproduksiselesai.cmdbatal(\'' . $dbr['faktur'] . '\',\'' . $dbr['fakturproduksi'] . '\')"
                                         class="btn btn-danger btn-grid">Batal Selesai</button>';
                $vaset['cmdBatal'] = html_entity_decode($vaset['cmdBatal']);
    
                if($tglmin > date_2s($dbr['tgl'])){
                    $vaset['cmdBatal'] = "";
                }
                $vare[] = $vaset;
            }
    
        }
        
        $vare = array("total" => $rtot['jml'], "records" => $vare);
        echo (json_encode($vare));
    }

    public function PreviewDetail()
    {
        $va = $this->input->post();
        $cFaktur = $va['faktur'];
        echo ('w2ui["bos-form-trbatalproduksiselesai_grid2"].clear();');
        $data = $this->trbatalproduksiselesai_m->GetDataPerFaktur($cFaktur);

        if (!empty($data)) {
            echo ('
                  with(bos.trbatalproduksiselesai.obj){
                     find("#faktur").val("' . $data['faktur'] . '") ;
                     find("#fakturprod").val("' . $data['fakturproduksi'] . '") ;
                     find("#stock").val("' . $data['stock'] . '") ;
                     find("#namastock").val("' . $data['keterangan'] . '") ;
                     find("#satuan").val("' . $data['satuan'] . '") ;
                     find("#qty").val("' . string_2s($data['qty']) . '") ;
                     find("#tgl").val("' . date_2d($data['tgl']) . '") ;
                     find("#bb").val("' . string_2s($data['bb']) . '") ;
                     find("#bop").val("' . string_2s($data['bop']) . '") ;
                     find("#btkl").val("' . string_2s($data['btkl']) . '") ;
                     find("#hp").val("' . string_2s($data['hargapokok']) . '") ;
                     find("#jumlah").val("' . string_2s($data['jumlah']) . '") ;
                  }

              ');

            $data = $this->trbatalproduksiselesai_m->getDetailPH($data['fakturproduksi']);
            $vare = array();
            $n = 0;
            while ($dbr = $this->trbatalproduksiselesai_m->getrow($data)) {
                $n++;
                $vaset = $dbr;
                $vaset['recid'] = $n;
                $vaset['no'] = $n;
                $vare[] = $vaset;
            }

            $vare = json_encode($vare);
            echo ('
            bos.trbatalproduksiselesai.loadmodalpreview("show") ;
            bos.trbatalproduksiselesai.grid2_reloaddata();
            w2ui["bos-form-trbatalproduksiselesai_grid2"].add(' . $vare . ');
         ');
        }
    }

    public function deleting()
    {
        $va = $this->input->post();
        $this->trbatalproduksiselesai_m->batalproduksiselesai($va['faktur'], $va['fakturproduksi']);
        echo ('bos.trbatalproduksiselesai.grid1_reloaddata() ; ');

    }
}
