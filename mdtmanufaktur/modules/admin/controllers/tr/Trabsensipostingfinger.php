<?php
class Trabsensipostingfinger extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('tr/trabsensipostingfinger_m') ;
        $this->load->model('func/updtransaksi_m') ;
        $this->load->model('func/func_m') ;
        $this->load->model('func/perhitunganhrd_m') ;
        $this->load->library('escpos');
        $this->load->helper('bdate');
        $this->bdb = $this->trabsensipostingfinger_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(),"mintgl"=>$tglmin) ;
        $this->load->view('tr/trabsensipostingfinger',$d) ;
    }

    public function loadgrid(){
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare               = array() ;
        $va['tgl']          = date_2s($va['tgl']);
        $vdb                = $this->trabsensipostingfinger_m->loadgrid($va) ;
        $dbd                = $vdb['db'] ;
        $tgl = $va['tgl'];//date("Y-m-d");
        $arrjw = array();
        while( $dbr = $this->trabsensipostingfinger_m->getrow($dbd) ){
            $tgljwabsen = $dbr['tgl'];
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['nip'],$tgl);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['nip'],$tgl);

            if(!isset($arrjw[$dbr['nip']]))$arrjw[$dbr['nip']] = $this->perhitunganhrd_m->getjwabsensi($dbr['nip'],$tgl);

            $absensi = $dbr['datetimeabsensi'];
            $timeabsensi = strtotime($dbr['datetimeabsensi']);

            //cek di jam masuk dulu sesuai jw atau tidak
            $timemasuk = strtotime($va['tgl'] ." ".$arrjw[$dbr['nip']]['jwmasuk']);//;
            $timebukaabsmasuk = $timemasuk - ($arrjw[$dbr['nip']]['bukaabsenmasuk'] * 60); //lihat buka absen masuk dgn jam absen masuk dikurangi menit dibukanya
            $timetutupabsmasuk = $timemasuk + ($arrjw[$dbr['nip']]['tutupabsenmasuk'] * 60); //lihat tutup absen masuk dgn jam absen masuk ditambah menit ditutupnya
            $bukaabsmasuk = date("Y-m-d H:i:s",$timebukaabsmasuk);
            $tutupabsmasuk = date("Y-m-d H:i:s",$timetutupabsmasuk);
            $jwabsmasuk = date("Y-m-d H:i:s",$timemasuk);


            //cek di jam pulang dulu sesuai jw atau tidak
            $timepulang = strtotime($va['tgl'] ." ".$arrjw[$dbr['nip']]['jwpulang']);//;
            $timebukaabspulang = $timepulang - ($arrjw[$dbr['nip']]['bukaabsenpulang'] * 60); //lihat buka absen pulang dgn jam absen pulang dikurangi menit dibukanya
            $timetutupabspulang = $timepulang + ($arrjw[$dbr['nip']]['tutupabsenpulang'] * 60); //lihat tutup absen pulang dgn jam absen pulang ditambah menit ditutupnya
            $bukaabspulang = date("Y-m-d H:i:s",$timebukaabspulang);
            $tutupabspulang = date("Y-m-d H:i:s",$timetutupabspulang);
            $jwabspulang = date("Y-m-d H:i:s",$timepulang);


            //cek apakah masuk jadwal absen masuk
            $jenis = "Tidak terdeteksi";
            $jadwal = "Tidak terdeteksi"; 
            $timeselisih = 0;
            $toleransi = 0 ;
            $ketselisih = "";
            $keterangan = "Tidak terdeteksi";
            if($absensi >= $bukaabsmasuk && $absensi <= $tutupabsmasuk){
                $jenis = "Masuk";
                $jadwal = $jwabsmasuk;
                $timeselisih = $timemasuk - $timeabsensi;
                $ketselisih = "Terlambat";
                $keterangan = "Normal";
                $toleransi = $arrjw[$dbr['nip']]['jwtoleransi'];
            }else if($absensi >= $bukaabspulang && $absensi <= $tutupabspulang){
                $jenis = "Pulang";
                $jadwal = $jwabspulang;
                $timeselisih = $timeabsensi - $timepulang;
                $ketselisih = "Lembur";
                $keterangan = "Normal";

                if($arrjw[$dbr['nip']]['jwmasuk'] > $arrjw[$dbr['nip']]['jwpulang']){
                    $tgljwabsen =  date("Y-m-d",strtotime($dbr['tgl']) - (60*60*24));
                }
            }

            
            //menghitung selisih
            if($timeselisih > 0){
                $selisihjam = floor(devide($timeselisih,3600));
                $selisihmenit = floor(devide($timeselisih - (3600*$selisihjam),60));
                $selisihdetik = $timeselisih - (3600*$selisihjam) - ($selisihmenit * 60);
                

                $keterangan = $ketselisih;
                if($selisihjam > 0)$keterangan .=" ". $selisihjam."jam";
                if($selisihmenit > 0)$keterangan .=" ". $selisihmenit."mnt";
                if($selisihdetik > 0)$keterangan .=" ". $selisihdetik."dtk";
                
            }
            

            $vare[] = array("recid"=>$dbr['id'],"nip"=>$dbr['nip'],"nama"=>$dbr['nama'],"jabatan"=>$jabatan['keterangan'],
                            "bagian"=>$bagian['keterangan'],"tgl"=>date_2d($tgljwabsen),"jenis"=>$jenis,
                            "jadwal"=>$jadwal,"toleransi"=>$toleransi,"absensi"=>$absensi,"selisih"=>$timeselisih,"keterangan"=>$keterangan,
                            "mesin"=>$dbr['mesin'],"ketmesin"=>$dbr['ketmesin'],"cabang"=>$dbr['cabang'],
                            "namacabang"=>$dbr['ketcabang']) ;
        }
        $vare 	= array("total"=>count($vare), "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function posting(){
        $va     = $this->input->post() ;
        $error      = $this->trabsensipostingfinger_m->posting($va) ;
        if($error == "ok"){
            echo(' 
                alert("Data berhasil disimpan... ");
                ') ;
        }else{
            echo('
                alert("'.$error.'");
                ');
        }
    }

    

    public function seekasuransi(){
        $search     = $this->input->get('q');
        $vdb    = $this->trabsensipostingfinger_m->seekasuransi($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trabsensipostingfinger_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
    }



    
}
?>
