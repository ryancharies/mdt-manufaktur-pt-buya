<?php
class Tranggarankas extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('tr/tranggarankas_m') ;
        $this->load->model('func/updtransaksi_m') ;
        $this->load->model('func/perhitungan_m') ;
        $this->load->helper('bdate');
        $this->bdb = $this->tranggarankas_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(),"mintgl"=>$tglmin) ;
        $this->load->view('tr/tranggarankas',$d) ;
    }

    public function loadgrid(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare               = array() ;
        $va['periodeawal']  = date("Y-m-d",mktime(0,0,0,1,1,$va['periodeawal'])); 
        $va['periodeakhir']  = date("Y-m-d",mktime(0,0,0,12,31,$va['periodeakhir']));
        $vdb                = $this->tranggarankas_m->loadgrid($va) ;
        $dbd                = $vdb['db'] ;
        while( $dbr = $this->tranggarankas_m->getrow($dbd) ){
            $vaset                  = $dbr ;
            $vaset['cmdcetak']    = '<button type="button" onClick="bos.tranggarankas.cmdcetak(\''.$dbr['periode'].'\')"
                                         class="btn btn-primary btn-grid">Cetak(Thn)</button>' ;
            $vaset['cmdcetak']    = html_entity_decode($vaset['cmdcetak']) ;

            $vaset['cmdcetak2']    = '<button type="button" onClick="bos.tranggarankas.cmdcetak2(\''.$dbr['periode'].'\')"
                                         class="btn btn-success btn-grid">Cetak(Bln)</button>' ;
            $vaset['cmdcetak2']    = html_entity_decode($vaset['cmdcetak2']) ;
            if ($tglmin > date_2s($dbr['tgl'])) {
                $vaset['cmdedit'] = "";
                $vaset['cmddelete'] = "";
            }
            $vare[]                 = $vaset ;
        }
        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }
    
    public function loadgrid2(){
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare               = array() ;
        $vdb                = $this->tranggarankas_m->loadgrid2($va) ;
        $dbd                = $vdb['db'] ;
        $n = 0 ;
        $cabang = getsession($this,"cabang") ;
        while( $dbr = $this->tranggarankas_m->getrow($dbd) ){
            $n++;
            $dbr['no'] = $n;
            $dbr['recid'] = $n;
            $vaset= $dbr ;
            $total = 0 ;
            for($i=1;$i<=12;$i++){
                $key = "b".$i;
                $periode = date("Y-m",mktime(0,0,0,$i,1,$va['periode']));
                $arrangkas = $this->perhitungan_m->getanggarankasrek($periode,$dbr['kode'],$cabang) ;
                $vaset[$key] = $arrangkas['nominal']; 
                $total += $vaset[$key]; 
            }
            $vaset['total'] = $total;
            $vare[] = $vaset ;
        }
        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
    
    }

    public function saving(){
        $va 	     = $this->input->post() ; 
        
         $this->tranggarankas_m->saving($va) ;
        echo(' bos.tranggarankas.settab(0) ;  ') ;
    }

    public function showreport(){
        $n=0;
        $va     = $this->input->get() ;
        //print_r($va);
        $tglawal 	= date("Y-m-d",mktime(0,0,0,1,1,$va['tahun']));
        $tglakhir 	= date("Y-m-d",mktime(0,0,0,12,31,$va['tahun']));
        $tglkemarin = date("d-m-Y",strtotime($tglawal)-(24*60*60)) ;
        $vare   = array() ;

        $arrdatamutasi = $this->bdb->loadanggarankas($tglawal,$tglakhir);
        $vare   = array() ;
        //OPERASI
        $vare[] = array("kode"=>"","keterangan"=>"<b>ANGGARAN KAS DARI AKTIVITAS OPERASI","1"=>"","2"=>"","3"=>"</b>");
        // $vare[] = array("kode"=>"","keterangan"=>"<b>:: SUMBER DANA","1"=>"","2"=>"","3"=>"","4"=>"</b>");
        // $vdb    = $this->bdb->loadrekeningstt("o") ;
        // $totoprsumberdana = 0 ;
        // while($dbr = $this->bdb->getrow($vdb) ){
        //     $sumberdana = $arrdatamutasi[$dbr['kode']]['sumberdana'];
        //     if($dbr['jenis'] == "I"){
        //         $dbr['kode'] = "<b>".$dbr['kode'];
        //     }else{
        //         $totoprsumberdana += $sumberdana;
        //     }
        //     $vare[] = array("kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan'],"1"=>string_2s($sumberdana),"2"=>"","3"=>"","4"=>"</b>");
        // }
        // $vare[] = array("kode"=>"","keterangan"=>"<b>:: JUMLAH SUMBER DANA</b>","1"=>"","2"=>string_2s($totoprsumberdana),"3"=>"","4"=>"</b>");

        $vare[] = array("kode"=>"","keterangan"=>"<b>:: PENGGUNAAN","1"=>"","2"=>"","3"=>"</b>");
        $vdb    = $this->bdb->loadrekeningstt("o") ;
        $totoprpenggunaan = 0 ;
        while($dbr = $this->bdb->getrow($vdb) ){
            $penggunaan = $arrdatamutasi[$dbr['kode']]['penggunaan'];
            if($dbr['jenis'] == "I"){
                $dbr['kode'] = "<b>".$dbr['kode'];
            }else{
                $totoprpenggunaan += $penggunaan;
            }
            if($penggunaan > 0)$vare[] = array("kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan'],"1"=>string_2s($penggunaan),"2"=>"","3"=>"</b>");
        }
        $vare[] = array("kode"=>"","keterangan"=>"<b>:: JUMLAH PENGGUNAAN","1"=>"","2"=>string_2s($totoprpenggunaan),"3"=>"</b>");
        $jmlaktivitasopr = $totoprpenggunaan;//$totoprsumberdana - $totoprpenggunaan;
        // $vare[] = array("kode"=>"","keterangan"=>"<b>JUMLAH ANGGARAN KAS DARI AKTIVITAS OPERASI","1"=>"","2"=>"","3"=>string_2s($jmlaktivitasopr),"4"=>"</b>");
        $vare[] = array("kode"=>"","keterangan"=>"","1"=>"","2"=>"","3"=>"","4"=>"");

        //investasi
        $vare[] = array("kode"=>"","keterangan"=>"<b>ANGGARAN KAS DARI AKTIVITAS INVESTASI","1"=>"","2"=>"","3"=>"</b>");
        // $vare[] = array("kode"=>"","keterangan"=>"<b>:: SUMBER DANA","1"=>"","2"=>"","3"=>"","4"=>"</b>");
        // $vdb    = $this->bdb->loadrekeningstt("i") ;
        // $totinvessumberdana = 0 ;
        // while($dbr = $this->bdb->getrow($vdb) ){
        //     $sumberdana = $arrdatamutasi[$dbr['kode']]['sumberdana'];
        //     if($dbr['jenis'] == "I"){
        //         $dbr['kode'] = "<b>".$dbr['kode'];
        //     }else{
        //         $totinvessumberdana += $sumberdana;
        //     }
        //     $vare[] = array("kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan'],"1"=>string_2s($sumberdana),"2"=>"","3"=>"","4"=>"</b>");
        // }
        // $vare[] = array("kode"=>"","keterangan"=>"<b>:: JUMLAH SUMBER DANA","1"=>"","2"=>string_2s($totinvessumberdana),"3"=>"","4"=>"</b>");

        $vare[] = array("kode"=>"","keterangan"=>"<b>:: PENGGUNAAN","1"=>"","2"=>"","3"=>"</b>");
        $vdb    = $this->bdb->loadrekeningstt("i") ;
        $totinvespenggunaan = 0 ;
        while($dbr = $this->bdb->getrow($vdb) ){
            $penggunaan = $arrdatamutasi[$dbr['kode']]['penggunaan'];
            if($dbr['jenis'] == "I"){
                $dbr['kode'] = "<b>".$dbr['kode'];
            }else{
                $totinvespenggunaan += $penggunaan;
            }
            if($penggunaan > 0)$vare[] = array("kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan'],"1"=>string_2s($penggunaan),"2"=>"","3"=>"</b>");
        }
        $vare[] = array("kode"=>"","keterangan"=>"<b>:: JUMLAH PENGGUNAAN","1"=>"","2"=>string_2s($totinvespenggunaan),"3"=>"</b>");
        $jmlaktivitasinves = $totinvespenggunaan;//$totinvessumberdana - $totinvespenggunaan;
        // $vare[] = array("kode"=>"","keterangan"=>"<b>JUMLAH ARUS KAS DARI AKTIVITAS INVESTASI","1"=>"","2"=>"","3"=>string_2s($jmlaktivitasinves),"4"=>"</b>");
        $vare[] = array("kode"=>"","keterangan"=>"","1"=>"","2"=>"","3"=>"","4"=>"");
        

        //pembiayaan
        $vare[] = array("kode"=>"","keterangan"=>"<b>ANGGARAN KAS DARI AKTIVITAS PEMBIAYAAN","1"=>"","2"=>"","3"=>"</b>");
        // $vare[] = array("kode"=>"","keterangan"=>"<b>:: SUMBER DANA","1"=>"","2"=>"","3"=>"","4"=>"</b>");
        // $vdb    = $this->bdb->loadrekeningstt("p") ;
        // $totpbysumberdana = 0 ;
        // while($dbr = $this->bdb->getrow($vdb) ){
        //     $sumberdana = $arrdatamutasi[$dbr['kode']]['sumberdana'];
        //     if($dbr['jenis'] == "I"){
        //         $dbr['kode'] = "<b>".$dbr['kode'];
        //     }else{
        //         $totpbysumberdana += $sumberdana;
        //     }
        //     $vare[] = array("kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan'],"1"=>string_2s($sumberdana),"2"=>"","3"=>"","4"=>"</b>");
        // }
        // $vare[] = array("kode"=>"","keterangan"=>"<b>:: JUMLAH SUMBER DANA","1"=>"","2"=>string_2s($totpbysumberdana),"3"=>"","4"=>"</b>");

        $vare[] = array("kode"=>"","keterangan"=>"<b>:: PENGGUNAAN","1"=>"","2"=>"","3"=>"</b>");
        $vdb    = $this->bdb->loadrekeningstt("p") ;
        $totpbypengunaan = 0 ;
        while($dbr = $this->bdb->getrow($vdb) ){
            $penggunaan = $arrdatamutasi[$dbr['kode']]['penggunaan'];
            if($dbr['jenis'] == "I"){
                $dbr['kode'] = "<b>".$dbr['kode'];
            }else{
                $totpbypengunaan += $penggunaan;
            }
            if($penggunaan > 0)$vare[] = array("kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan'],"1"=>string_2s($penggunaan),"2"=>"","3"=>"</b>");
        }
        $vare[] = array("kode"=>"","keterangan"=>"<b>:: JUMLAH PENGGUNAAN","1"=>"","2"=>string_2s($totpbypengunaan),"3"=>"</b>");
        $jmlaktivitaspby = $totpbypengunaan;//$totpbysumberdana - $totpbypengunaan;
        // $vare[] = array("kode"=>"","keterangan"=>"<b>JUMLAH ARUS KAS DARI AKTIVITAS PEMBIAYAAN","1"=>"","2"=>string_2s($jmlaktivitaspby),"3"=>"","4"=>"</b>");
        // $vare[] = array("kode"=>"","keterangan"=>"","1"=>"","2"=>"","3"=>"","4"=>"");
        $totaktivitas =  $jmlaktivitaspby + $jmlaktivitasopr + $jmlaktivitasinves;
        //$vare[] = array("kode"=>"","keterangan"=>"<b>KAS ELEMENT KAS ".$tglakhir,"1"=>"","2"=>"","3"=>"","4"=>string_2s($totaktivitas)."</b>");
        $vare[] = array("kode"=>"","keterangan"=>"<b>TOTAL ANGGARAN KAS ".$va['tahun'],"1"=>"","2"=>"","3"=>string_2s($totaktivitas)."</b>");

        //saldo kas akhir
        // $vare[] = array("kode"=>"","keterangan"=>"","1"=>"","2"=>"","3"=>"","4"=>"");
        // $vdb    = $this->bdb->loadrekeningstt("k","D") ;
        // $vare[] = array("kode"=>"","keterangan"=>"<b>KAS ELEMENT KAS ".$tglakhir,"1"=>"","2"=>"","3"=>"","4"=>"</b>");
        // $totsaldokasakhir = 0 ;
        // while($dbr = $this->bdb->getrow($vdb) ){
        //     $saldo = $this->perhitungan_m->getsaldo($tglakhir,$dbr['kode']);
        //     $totsaldokasakhir += $saldo;
        //     $vare[] = array("kode"=>$dbr['kode'],"keterangan"=>$dbr['keterangan'],"1"=>"","2"=>"","3"=>string_2s($saldo),"4"=>"");
        // }
        // $vare[] = array("kode"=>"","keterangan"=>"<b>JUMLAH KAS ELEMENT KAS","1"=>"","2"=>"","3"=>"","4"=>string_2s($totsaldokasakhir)."</b>");
        
        //to pdf
        if(!empty($vare)){ 
          $font = 10 ;
          $o    = array('paper'=>'A4', 'orientation'=>'l', 'export'=>"",
                          'opt'=>array('export_name'=>'DaftarNeraca_' . getsession($this, "username") ) ) ;
          $this->load->library('bospdf', $o) ;   
          $this->bospdf->ezText("<b>LAPORAN ANGGARAN KAS</b>",$font+4,array("justification"=>"center")) ;
          $this->bospdf->ezText("Periode : ".$va['tahun'],$font+2,array("justification"=>"center")) ;
          $this->bospdf->ezText("") ; 
          $this->bospdf->ezTable($vare,"","",  
                                  array("showHeadings"=>"","showLines"=>"1","fontSize"=>$font,"cols"=> array( 
                                   "kode"=>array("caption"=>"Kode","width"=>15,"justification"=>"left"),
                                   "keterangan"=>array("caption"=>"Keterangan","wrap"=>1),
                                   "1"=>array("caption"=>"","width"=>15,"justification"=>"right"),
                                   "2"=>array("caption"=>"","width"=>13,"justification"=>"right"),
                                   "3"=>array("caption"=>"","width"=>13,"justification"=>"right")))) ;   
          //print_r($data) ;    
          $this->bospdf->ezStream() ; 
        }else{
           echo('data kosong') ;
        }
    }

    public function showreport2(){
        $n=0;
        $cabang = getsession($this,"cabang") ;
        $va     = $this->input->get() ;
        //print_r($va);
        $blnawal = 1;
        $blnakhir = 12;
        $vare   = array() ;
        $arrtotal = array();
        $arrtotal['keterangan'] = "<b>Total";
        $arrj = array("o"=>"OPRASIONAL","i"=>"INVESTASI","p"=>"PENDANAAN");
        foreach($arrj as $key => $val){
            // head foot
            $vare[$key] = array();
            $vare[$key]['header'] = array();
            $vare[$key]['header'][] = array("keterangan"=>"<b>ANGGARAN KAS DARI AKTIVITAS ".$val." </b>");

            $arrfooter = array();
            $arrfooter['keterangan']= "<b>Sub Total";
            $total = 0 ;
            for($i = $blnawal;$i<=$blnakhir;$i++){
                $arrfooter[$i] = 0;
            }
            $arrfooter['total']= 0;

            $vare[$key]['body'] = array();
            $vdb    = $this->bdb->loadrekeningstt($key) ;
            $no = 0;
            while($dbr = $this->bdb->getrow($vdb) ){
                $arrd = array();
                $arrd['no']= "";
                $arrd['kode']= $dbr['kode'];
                $arrd['keterangan']= $dbr['keterangan'];
                $total = 0 ;
                for($i = $blnawal;$i<=$blnakhir;$i++){
                    $periode = date("Y-m",mktime(0,0,0,$i,1,$va['tahun']));
                    $arranggarankas = $this->perhitungan_m->getanggarankasrek($periode,$dbr['kode'],$cabang) ;
                    $arrd[$i] = string_2s($arranggarankas['nominal']);
                    $total += $arranggarankas['nominal'];
                    $arrfooter[$i] += $arranggarankas['nominal'];
                }
                $arrd['total']= string_2s($total);
                if($total > 0 ){
                    $no++;
                    $arrd['no']=$no;
                    $vare[$key]['body'][] = $arrd;
                }
                $arrfooter["total"] += $total;
            }
            for($i = $blnawal;$i<=$blnakhir;$i++){
                if(!isset($arrtotal[$i])) $arrtotal[$i] = 0;
                $arrtotal[$i] += $arrfooter[$i];
                $arrfooter[$i] = string_2s($arrfooter[$i]);
                
            }

            $arrtotal["total"] += $arrfooter["total"];
            $arrfooter["total"] = string_2s($arrfooter["total"])."</b>";
            $vare[$key]['footer'] = array();
            $vare[$key]['footer'][] = $arrfooter;
        }
        $arrtot = array();
        for($i = $blnawal;$i<=$blnakhir;$i++){
            $arrtotal[$i] = string_2s($arrfooter[$i]);    
        }
        $arrtotal["total"] = string_2s($arrtotal["total"])."</b>";
        $arrtot[] = $arrtotal;

        if(!empty($vare)){
            $arrcolbody = array();
            $arrcolfooter = array();

            $arrcolbody['no']= array("caption"=>"No","width"=>2,"justification"=>"right");
            $arrcolbody['kode']= array("caption"=>"Kode","width"=>6,"justification"=>"center");
            $arrcolbody['keterangan']= array("caption"=>"Keterangan","justification"=>"left");
            $arrcolfooter['keterangan']= array("caption"=>"Keterangan","justification"=>"center");
            for($i = $blnawal;$i<=$blnakhir;$i++){
                $caption = date_month($i-1);
                $arrcolbody[$i] = array("caption"=>$caption,"width"=>6,"justification"=>"right");
                $arrcolfooter[$i] = array("caption"=>$caption,"width"=>6 ,"justification"=>"right");
            }
            $arrcolbody['total'] = array("caption"=>"Total","width"=>7,"justification"=>"right");
            $arrcolfooter['total'] = array("caption"=>"Total","width"=>7,"justification"=>"right");

            $font = 7 ;
            $o    = array('paper'=>'letter', 'orientation'=>'l', 'export'=>"",
                            'opt'=>array('export_name'=>'Daftaranggarankas_' . getsession($this, "username") ) ) ;
            $this->load->library('bospdf', $o) ;   
            $this->bospdf->ezText("<b>LAPORAN ANGGARAN KAS</b>",$font+4,array("justification"=>"center")) ;
            $this->bospdf->ezText("Periode : ".$va['tahun'],$font+2,array("justification"=>"center")) ;
            $this->bospdf->ezText("") ; 
            
            foreach($vare as $key => $arrval){
                $this->bospdf->ezText("") ; 
                $this->bospdf->ezTable($arrval['header'],"","",  
                                  array("showHeadings"=>"0","showLines"=>"0","fontSize"=>$font,"cols"=> array(
                                      "keterangan"=>array("justification"=>"left")
                                  ))) ; 
                $this->bospdf->ezTable($arrval['body'],"","",  
                                  array("showHeadings"=>"1","showLines"=>"1","fontSize"=>$font,"cols"=> $arrcolbody)) ; 
                $this->bospdf->ezTable($arrval['footer'],"","",  
                                  array("showHeadings"=>"0","showLines"=>"1","fontSize"=>$font,"cols"=> $arrcolfooter)) ; 
                                  
            }
            $this->bospdf->ezTable($arrtot,"","",  
                                  array("showHeadings"=>"0","showLines"=>"1","fontSize"=>$font,"cols"=> $arrcolfooter)) ;
            
            //print_r($data) ;    
            $this->bospdf->ezStream() ; 
        }else{
           echo('data kosong') ;
        }
    }
    



}
?>
