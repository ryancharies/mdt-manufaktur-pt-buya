<?php
class Trreturpenjualan extends Bismillah_Controller{
    private $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->model('func/updtransaksi_m') ;
        $this->load->model('tr/trreturpenjualan_m') ;
        $this->load->helper('bdate');
        $this->bdb = $this->trreturpenjualan_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi", "", "", "tgl asc");
        $tglmin = "minimdate = '" . date("m/d/Y", strtotime($tglmin)) . "'";
        $d    = array("dnow"=>date("d-m-Y"), "setdate"=>date_set(), "mintgl" => $tglmin) ;
        $this->load->view('tr/trreturpenjualan',$d) ;
    }

    public function loadgrid(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi", "", "", "tgl asc");
        $va                 = json_decode($this->input->post('request'), true) ;
        $vare               = array() ;
        $va['tglawal']      = date_2s($va['tglawal']);
        $va['tglakhir']     = date_2s($va['tglakhir']);
        $vdb                = $this->trreturpenjualan_m->loadgrid($va) ;
        $dbd                = $vdb['db'] ;
        while( $dbr = $this->trreturpenjualan_m->getrow($dbd) ){
            $vaset                  = $dbr ;
            $vaset['tgl']           = date_2d($vaset['tgl']);
            $vaset['cmdedit']       = '<button type="button" onClick="bos.trreturpenjualan.cmdedit(\''.$dbr['faktur'].'\')"
                                        class="btn btn-default btn-grid">Edit</button>' ;
            $vaset['cmddelete']     = '<button type="button" onClick="bos.trreturpenjualan.cmddelete(\''.$dbr['faktur'].'\')"
                                        class="btn btn-danger btn-grid">Delete</button>' ;
            $vaset['cmdedit']       = html_entity_decode($vaset['cmdedit']) ;
            $vaset['cmddelete']	    = html_entity_decode($vaset['cmddelete']) ;
            if($tglmin > date_2s($dbr['tgl'])){
                $vaset['cmdedit'] = "";
                $vaset['cmddelete'] = "";
            }
            $vare[]                 = $vaset ;
        }
        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init(){
        savesession($this, "ssreturpenjualan_faktur", "") ;
    }

    public function saving(){
        $va 	     = $this->input->post() ;
        $kode 	     = getsession($this, "ssreturpenjualan_faktur") ;
        $va['tgl']   = date_2s($va['tgl']) ;

        $this->trreturpenjualan_m->saving($kode, $va) ;
        echo(' bos.trreturpenjualan.settab(0) ;  ') ;
    }

    public function editing(){
        $va 	= $this->input->post() ;
        $faktur = $va['faktur'] ;
        $data   = $this->trreturpenjualan_m->getdatatotal($faktur) ;
        if(!empty($data)){
            savesession($this, "ssreturpenjualan_faktur", $faktur) ;
            $gudang[]   = array("id"=>$data['gudang'],"text"=>$data['ketgudang']);
            $customer[] = array("id"=>$data['customer'],"text"=>$data['namacustomer']);
            echo('
            w2ui["bos-form-trreturpenjualan_grid2"].clear();
            with(bos.trreturpenjualan.obj){
               find(".nav-tabs li:eq(1) a").tab("show") ;
               find("#faktur").val("'.$data['faktur'].'") ;
               find("#fktpj").val("'.$data['fktpj'].'") ;
               find("#tgl").val("'.date_2d($data['tgl']).'");
               find("#gudang").sval('.json_encode($gudang).');
               find("#customer").sval('.json_encode($customer).');
               find("#subtotal").val("'.string_2s($data['subtotal']).'") ;
               find("#persppn").val("'.string_2s($data['persppn']).'") ;
               find("#ppn").val("'.string_2s($data['ppn']).'") ;
               find("#total").val("'.string_2s($data['total']).'") ;
            }


         ') ;

            //loadgrid detail
            $vare = array();
            $dbd = $this->trreturpenjualan_m->getdatadetail($faktur) ;
            $n = 0 ;
            while( $dbr = $this->trreturpenjualan_m->getrow($dbd) ){
                $n++;
                $vaset   = $dbr ;
                $vaset['recid'] = $n;
                $vaset['no'] = $n;
                $vaset['cmddelete'] = '<button type="button" onClick="bos.trreturpenjualan.grid2_deleterow('.$n.')"
                                        class="btn btn-danger btn-grid">Delete</button>' ;
                $vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;
                $vare[]             = $vaset ;
            }
            $vare = json_encode($vare);
            echo('
                w2ui["bos-form-trreturpenjualan_grid2"].add('.$vare.');
                bos.trreturpenjualan.initdetail();
                bos.trreturpenjualan.settab(1) ;
            ');
        }
    }

    public function pilihstock(){
        $va 	= $this->input->post() ;
        
        $data = $this->trreturpenjualan_m->getdata($va) ;
        if(!empty($data)){
            echo('
            with(bos.trreturpenjualan.obj){
               find("#stock").val("'.$data['kode'].'") ;
               find("#namastock").val("'.$data['keterangan'].'");
               find("#harga").val("'.$data['harga'].'");
               find("#harga").focus() ;
               find("#satuan").val("'.$data['satuan'].'");
               bos.trreturpenjualan.loadmodelstock("hide");
            }

         ') ;
        }
    }
    public function deleting(){
        $va 	= $this->input->post() ;
        $error = $this->trreturpenjualan_m->deleting($va['faktur']) ;        
        if ($error == "ok") {
            echo('bos.trreturpenjualan.grid1_reloaddata() ; ') ;
        } else {
            echo('alert("'.$error.'");');
        }

    }

    public function getfaktur(){
        $faktur  = $this->trreturpenjualan_m->getfaktur(FALSE) ;

        echo('
        bos.trreturpenjualan.obj.find("#faktur").val("'.$faktur.'") ;
        ') ;
    }

    public function loadgrid3(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;
        $vdb    = $this->trreturpenjualan_m->loadgrid3($va) ;
        $dbd    = $vdb['db'] ;
        while( $dbr = $this->trreturpenjualan_m->getrow($dbd) ){
            $vaset   = $dbr ;
            $vaset['cmdpilih']    = '<button type="button" onClick="bos.trreturpenjualan.cmdpilih(\''.$dbr['kode'].'\')"
                           class="btn btn-success btn-grid">Pilih</button>' ;
            $vaset['cmdpilih']	   = html_entity_decode($vaset['cmdpilih']) ;
            $vare[]		= $vaset ;
        }

        $vare 	= array("total"=>$vdb['rows'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function getdatastock(){
      $va = $this->input->post();
      $dbData  = $this->trreturpenjualan_m->getdatastock($va) ;
      if($dbRow = $this->trreturpenjualan_m->getrow($dbData)){
        $cKeterangan = $dbRow['keterangan'];
        $cSatuan     = $dbRow['satuan'];
        echo('
          bos.trreturpenjualan.obj.find("#satuan").val("'.$cSatuan.'") ;
          bos.trreturpenjualan.obj.find("#harga").val("'.$dbRow['harga'].'") ;
          bos.trreturpenjualan.obj.find("#namastock").val("'.$cKeterangan.'") ;
        ');
      }else {
        echo('
          bos.trreturpenjualan.obj.find("#stock").val("") ;
          bos.trreturpenjualan.obj.find("#harga").val("") ;
          bos.trreturpenjualan.obj.find("#nomor").focus() ;
          alert("Data Tidak Ditemukan!!");
        ');
      }
    }

    public function pilihpj(){
        $va 	= $this->input->post() ;
        $fkt 	= $va['fktpj'] ;
        $data = $this->trreturpenjualan_m->getdatado($fkt) ;
        if(!empty($data)){
            $customer[]   = array("id"=>$data['customer'],"text"=>$data['namacustomer']);
            echo('
            with(bos.trreturpenjualan.obj){
               find("#fktpj").val("'.$data['faktur'].'") ;
               find("#persppn").val("'.$data['persppn'].'") ;
               find("#customer").sval('.json_encode($customer).');
               bos.trreturpenjualan.loadmodelpj("hide");
            }

         ') ;
           /* //loadgrid detail PO
            $vare = array();
            $dbd = $this->trsj_m->getdatadetailpj($fktdo) ;
            $n = 0 ;
            while( $dbr = $this->trreturpenjualan_m->getrow($dbd) ){
                $n++;
                $vaset   = $dbr ;
                $vaset['recid'] = $n;
                $vaset['no'] = $n;
                $vaset['cmddelete'] = '<button type="button" onClick="bos.trreturpenjualan.grid2_deleterow('.$n.')"
                                        class="btn btn-danger btn-grid">Delete</button>' ;
                $vaset['cmddelete']	= html_entity_decode($vaset['cmddelete']) ;
                $vare[]             = $vaset ;
            }
            $vare = json_encode($vare);
            echo('
                w2ui["bos-form-trsj_grid2"].add('.$vare.');
                bos.trsj.initdetail();
                bos.trsj.hitungsubtotal();
            ');*/
        }
    }
    
    public function loadgrid4(){
        $va     = json_decode($this->input->post('request'), true) ;
        $vare   = array() ;

        $search	 = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        // $search   = $this->dbd->escape($search) ;

        if($search !== ""){
            $vdb    = $this->trreturpenjualan_m->loadgrid4($va) ;
            $dbd    = $vdb['db'] ;
            while( $dbr = $this->trreturpenjualan_m->getrow($dbd) ){
                $vaset   = $dbr ;
                $vaset['cmdpilih']    = '<button type="button" onClick="bos.trreturpenjualan.cmdpilihpj(\''.$dbr['faktur'].'\')"
                            class="btn btn-success btn-grid">Pilih</button>' ;
                $vaset['cmdpilih']	   = html_entity_decode($vaset['cmdpilih']) ;
                $vare[]		= $vaset ;
            }
        }
        

        $vare 	= array("total"=>count($vare), "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }
}
?>
