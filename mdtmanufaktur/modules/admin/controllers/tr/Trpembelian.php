<?php
class Trpembelian extends Bismillah_Controller
{
    private $bdb;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('func/updtransaksi_m');
        $this->load->model('func/perhitungan_m');
        $this->load->model('func/func_m');
        $this->load->model('tr/trpembelian_m');
        $this->load->helper('bdate');
        $this->bdb = $this->trpembelian_m;
    }

    public function index()
    {
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $tglmin = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $d = array("dnow" => date("d-m-Y"), "setdate" => date_set(),"mintgl"=>$tglmin);
        $this->load->view('tr/trpembelian', $d);
    }

    public function loadgrid_where($bs, $s){
        $this->duser();
        $bs['tglawal'] = date_2s($bs['tglawal']);
        $bs['tglakhir'] = date_2s($bs['tglakhir']);

        $this->db->where('t.status', "1");
        $this->db->where('t.tgl >=', $bs['tglawal']);
        $this->db->where('t.tgl <=', $bs['tglakhir']);

        
        if($bs['skd_gudang'] !== 'null'){
            $bs['skd_gudang'] = json_decode($bs['skd_gudang'],true);
            $this->db->group_start();
            foreach($bs['skd_gudang'] as $kdc){
                $this->db->or_where('t.gudang',$kdc);
            }
            $this->db->group_end();

        }else{
            if($this->aruser['level'] !== '0000'){
                if(is_array($this->aruser['data_var']['gudang'])){
                    $this->db->group_start();
                    foreach($this->aruser['data_var']['gudang'] as $kdc){
                        $this->db->or_where('t.gudang', $kdc);
                    }
                    $this->db->group_end();
                }
            }
        }

        if($s !== ''){
            $this->db->group_start();
                $this->db->or_like(array('t.faktur'=>$s, 's.nama'=>$s));
            $this->db->group_end();
        }
    }

    public function loadgrid(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");

        $va       = json_decode($this->input->post('request'), true) ;
        $limit    = $va['offset'].",".$va['limit'] ;
        $search	  = isset($va['search'][0]['value']) ? $va['search'][0]['value'] : "" ;
        $search   = $this->db->escape_like_str($search) ;
        $vare               = array() ;
        $this->loadgrid_where($va, $search);
        $f = "count(t.id) jml";
        $dbd = $this->db->select($f)
            ->from("pembelian_total t")
            ->join("supplier s","s.kode = t.supplier","left")
            ->join("gudang g","g.kode = t.gudang","left")
            ->get();
        $rtot  = $dbd->row_array();
        if($rtot['jml'] > 0){
            $this->loadgrid_where($va, $search);
            $f2 = "t.faktur,t.tgl,s.nama as supplier,t.subtotal,t.diskon,t.pembulatan,t.ppn,t.total,t.fktpo,t.jthtmp";
            $dbd2 = $this->db->select($f2)->from("pembelian_total t")
                ->join("supplier s","s.kode = t.supplier","left")
                ->join("gudang g","g.kode = t.gudang","left")
                ->order_by("t.tgl,t. faktur ASC")
                ->limit($limit)
                ->get();
        
            foreach($dbd2->result_array() as $dbr){
                $vaset = $dbr;
                $vaset['tgl'] = date_2d($vaset['tgl']);
                $vaset['jthtmp'] = date_2d($vaset['jthtmp']);
                $vaset['cmdedit'] = '<button type="button" onClick="bos.trpembelian.cmdedit(\'' . $dbr['faktur'] . '\')"
                                            class="btn btn-default btn-grid">Edit</button>';
                $vaset['cmddelete'] = '<button type="button" onClick="bos.trpembelian.cmddelete(\'' . $dbr['faktur'] . '\')"
                                            class="btn btn-danger btn-grid">Delete</button>';
                $vaset['cmdedit'] = html_entity_decode($vaset['cmdedit']);
                $vaset['cmddelete'] = html_entity_decode($vaset['cmddelete']);
                $pelunasan = $this->trpembelian_m->cekpelunasan($dbr['faktur']);
                if($tglmin > date_2s($dbr['tgl']) || $pelunasan){
                    $vaset['cmdedit'] = "";
                    $vaset['cmddelete'] = "";
                }
                $vare[] = $vaset;
            }
        }

        
        $vare 	= array("total"=>$rtot['jml'], "records"=>$vare ) ;
        echo(json_encode($vare)) ;
    }

    public function init()
    {
        savesession($this, "sspembelian_faktur", "");
    }

    public function saving()
    {
        $va = $this->input->post();
        $kode = getsession($this, "sspembelian_faktur");
        $va['tgl'] = date_2s($va['tgl']);

        $igd = $this->func_m->getinfogudang($va['gudang']);
        $cabang = $igd['cabang'];

        $faktur = getsession($this, "sspembelian_faktur", "");
        $va['faktur'] = $faktur !== "" ? $faktur : $this->bdb->getfaktur($cabang,$va['tgl']);
        $data = array("faktur" => $va['faktur'],
            "tgl" => $va['tgl'],
            "jthtmp" => date_2s($va['jthtmp']),
            "fktpo" => $va['fktpo'],
            "subtotal" => string_2n($va['subtotal']),
            "diskon" => string_2n($va['diskontotal']),
            "pembulatan" => string_2n($va['pembulatantotal']),
            "persppn" => string_2n($va['persppn']),
            "ppn" => string_2n($va['ppntotal']),
            "total" => string_2n($va['total']),
            "hutang" => string_2n($va['total']),
            "status" => "1",
            "gudang" => $va['gudang'],
            "supplier" => $va['supplier'],
            "cabang" => getsession($this, "cabang"),
            "username" => getsession($this, "username"));
        $where = "faktur = " . $this->bdb->escape($faktur);
        $this->bdb->update("pembelian_total", $data, $where, "");

        //insert detail pembelian
        $vaGrid = json_decode($va['grid2']);
        $this->bdb->delete("pembelian_detail", "faktur = '{$va['faktur']}'");
        foreach ($vaGrid as $key => $val) {
            $cKdStockGrid = $val->stock;
            $dbRKD = $this->perhitungan_m->getdatastock($cKdStockGrid);
            $cKodeStock = $dbRKD['kode'];
            
            $vadetail = array("faktur" => $va['faktur'],
                "stock" => $cKodeStock,
                "qty" => $val->qty,
                "harga" => $val->harga,
                "jumlah" => $val->jumlah,
                "totalitem" => $val->jumlah,
                "username" => getsession($this, "username"));
            $this->bdb->insert("pembelian_detail", $vadetail);
        }

        //update kartu stock

        $this->updtransaksi_m->updkartustockpembelian($va['faktur']);
        $this->updtransaksi_m->updkartuhutangpembelian($va['faktur']);
        $this->updtransaksi_m->updrekpembelian($va['faktur']);

       

        echo('ok') ;

    }

    public function editing()
    {
        $va = $this->input->post();
        $faktur = $va['faktur'];

        $data = array();
        $this->db->where('t.faktur',$faktur);
        $f = "t.faktur,t.tgl,t.gudang,t.supplier,g.keterangan as ketgudang,t.subtotal,t.diskon,t.ppn,t.pembulatan,t.jthtmp,
            t.total,s.nama as namasupplier,t.fktpo,t.persppn";
        $dbd = $this->db->select($f)
            ->from("pembelian_total t")
            ->join("gudang g","g.kode = t.gudang","left")
            ->join("supplier s","s.kode = t.supplier","left")
            ->get();
        $data  = $dbd->row_array();
        if(!empty($data)){
            savesession($this, "sspembelian_faktur", $faktur);
            $termin = strtotime($data['jthtmp']) - strtotime($data['jthtmp']);
            if($termin > 0) $termin = devide($termin,(60*60*24));
            $data['tgl'] = date_2d($data['tgl']);
            $data['jthtmp'] = date_2d($data['jthtmp']);
            $data['termin'] = $termin;

            //loadgrid detail
            $vare = array();
            $this->db->where('d.faktur',$faktur);
            $f2 = "d.stock,s.keterangan as namastock,d.harga,d.qty,s.satuan,d.totalitem as jumlah";
            $dbd2 = $this->db->select($f2)->from("pembelian_detail d")
                    ->join("stock s","s.kode = d.stock","left")
                    ->get();
            foreach($dbd2->result_array() as $dbr){
                $vare[$dbr['stock']] = $dbr ;
            }
            $data['detil'] = $vare;
        }
        echo(json_encode($data));
    }


    public function pilihpo()
    {
        $va = $this->input->post();
        $fktpo = $va['fktpo'];
        $data = $this->trpembelian_m->getdatapo($fktpo);
        if (!empty($data)) {
            $gudang[] = array("id" => $data['gudang'], "text" => $data['ketgudang']);
            $supplier[] = array("id" => $data['supplier'], "text" => $data['namasupplier']);
            echo ('
            with(bos.trpembelian.obj){
               find("#fktpo").val("' . $data['faktur'] . '") ;
               find("#gudang").sval(' . json_encode($gudang) . ');
               find("#supplier").sval(' . json_encode($supplier) . ');
               bos.trpembelian.loadmodelpo("hide");
            }

         ');
            //loadgrid detail PO
            $vare = array();
            $dbd = $this->trpembelian_m->getdatadetailpo($fktpo);
            $n = 0;
            while ($dbr = $this->trpembelian_m->getrow($dbd)) {
                $n++;
                $vaset = $dbr;
                $vaset['jumlah'] = $vaset['qty'] * $vaset['harga'];
                $vaset['recid'] = $n;
                $vaset['no'] = $n;
                $vaset['cmddelete'] = '<button type="button" onClick="bos.trpembelian.grid2_deleterow(' . $n . ')"
                                        class="btn btn-danger btn-grid">Delete</button>';
                $vaset['cmddelete'] = html_entity_decode($vaset['cmddelete']);
                $vare[] = $vaset;
            }
            $vare = json_encode($vare);
            echo ('
                w2ui["bos-form-trpembelian_grid2"].add(' . $vare . ');
                bos.trpembelian.initdetail();
                bos.trpembelian.hitungsubtotal();
            ');
        }
    }

    public function ceksupplier()
    {
        
        $va = $this->input->post();
        if ($va['supplier'] != "") {
            $arrsup = $this->trpembelian_m->getdatasupplier($va['supplier']);
            $jthtmp = date("d-m-Y",strtotime($va['tgl'])+($arrsup['terminhari']*60*60*24));
            echo ('
                with(bos.trpembelian.obj){
                    find("#terminhari").val("' . $arrsup['terminhari'] . '") ;
                    find("#jthtmp").val("' . $jthtmp . '") ;
                }

            ');
        }
    }

    public function deleting()
    {
        $va = $this->input->post();
        $faktur = $va['faktur'];

      

        $return = "ok";
        $dbd = $this->bdb->select("hutang_kartu","id","faktur <> '$faktur' and fkt = '$faktur'");
        if ($dbr = $this->bdb->getrow($dbd)) {
            $return = "Data tidak bisa dihapus karena sudah masuk ke proses pelunasan!!";
        }else{
            $this->bdb->delete("keuangan_bukubesar", "faktur = " . $this->bdb->escape($faktur));
            $this->bdb->delete("stock_kartu", "faktur = " . $this->bdb->escape($faktur));
            $this->bdb->delete("po_kartu", "faktur = " . $this->bdb->escape($faktur));
            $this->bdb->delete("hutang_kartu", "faktur = " . $this->bdb->escape($faktur));
            $this->bdb->edit("pembelian_total", array("status" => 2, "fktpo" => ""), "faktur = " . $this->bdb->escape($faktur));
        }
        echo($return);
        

    }

    public function getfaktur(){
        $va 	= $this->input->post() ;
        $igd = $this->func_m->getinfogudang($va['gudang']);
        $cabang = $igd['cabang'];
        $faktur  = $this->trpembelian_m->getfaktur($cabang,$va['tgl'],FALSE) ;
        echo($faktur) ;
    }

    public function loadgrid4()
    {
        $va = json_decode($this->input->post('request'), true);
        $vare = array();
        $vdb = $this->trpembelian_m->loadgrid4($va);
        //print_r($vdb);
        $dbd = $vdb['db'];
        foreach ($dbd as $key => $dbr) {
            $vaset = $dbr;
            $vaset['cmdpilih'] = '<button type="button" onClick="bos.trpembelian.cmdpilihpo(\'' . $dbr['faktur'] . '\')"
                           class="btn btn-success btn-grid">Pilih</button>';
            $vaset['cmdpilih'] = html_entity_decode($vaset['cmdpilih']);
            $vare[] = $vaset;
        }

        $vare = array("total" => count($vare), "records" => $vare);
        echo (json_encode($vare));
    }
}
