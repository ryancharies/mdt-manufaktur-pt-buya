<?php
class Trpotgajipinjamankaryawan extends Bismillah_Controller{
    protected $bdb ;
    public function __construct(){
        parent::__construct() ;
        $this->load->helper("bdate") ;
        $this->load->helper("bcore") ;
        $this->load->model("tr/trpotgajipinjamankaryawan_m") ;
        $this->load->model("func/perhitunganhrd_m") ;
        $this->bdb 	= $this->trpotgajipinjamankaryawan_m ;
    }

    public function index(){
        $tglmin = $this->bdb->getval("tgl", "status = '1'", "tgl_transaksi","","","tgl asc");
        $data['mintgl'] = "minimdate = '".date("m/d/Y",strtotime($tglmin))."'";
        $this->load->view("tr/trpotgajipinjamankaryawan",$data) ;

    } 

   
    public function loadgrid1(){
        $va = $this->input->post();
        $vare   = array() ;
        $vdb    = $this->trpotgajipinjamankaryawan_m->loadgrid($va) ;
        $dbd    = $vdb['db'] ;
        $n = 0 ;
        $editable = true;
        while( $dbr = $this->trpotgajipinjamankaryawan_m->getrow($dbd) ){
            $n++;

            //jab bag
            $jabatan = $this->perhitunganhrd_m->getjabatan($dbr['nip'],$va['tgl']);
            $bagian = $this->perhitunganhrd_m->getbagian($dbr['nip'],$va['tgl']);
            $tunggakan = $this->perhitunganhrd_m->gettunggakanpinjamankaryawan($dbr['nopinjaman'],$dbr['tglrealisasi'],$dbr['tglawalags'],
                            $va['tgl'],$dbr['plafond'],$dbr['lama']);
            $agspinjaman = $this->trpotgajipinjamankaryawan_m->getagspinjaman($dbr['nopinjaman'],$va['periode']);

            $vaset = $dbr;
            $vaset['tunggakan'] = $tunggakan['tpokok'];
            $vaset['tglrealisasi'] = date_2d($vaset['tglrealisasi']); 
            $vaset['tglawalags'] = date_2d($vaset['tglawalags']); 
            $vaset['recid'] = $n;
            $vaset['jabatan'] = $jabatan['keterangan'];
            $vaset['bagian'] = $bagian['keterangan'];
            $vaset['bakidebet'] = $this->perhitunganhrd_m->getsaldopinjamankaryawan($dbr['nopinjaman'],$va['tgl']);
            $vaset['angsuran'] = $agspinjaman['pokok'];
            if($agspinjaman['status'] == 1){
                $vaset['bakidebet'] += $agspinjaman['pokok'];
                $editable = false;
            }

            $vaset['bakidebetakhir'] = $vaset['bakidebet'] - $vaset['angsuran'];

            $vaset['w2ui'] = array("editable"=> $editable);
            $vare[]		= $vaset ;
        }

        
        $vare = json_encode($vare);

        echo ('
                w2ui["bos-form-trpotgajipinjamankaryawan_grid1"].add(' . $vare . ');
              ');

    }

    public function init(){
        savesession($this, "sstrpotgajipinjamankaryawan_kode", "") ;
    }

    public function saving(){
        $va 	= $this->input->post() ;
        $error = $this->bdb->saving($va) ;
        if($error == ""){
            //echo(' bos.trpotgajipinjamankaryawan.grid1_loaddata() ;  ') ;
        }else{
            echo('
                alert("'.$error.'");
                
                ') ;
        }
        
    }

  

    public function detail(){
        $va 	= $this->input->post() ;
        echo('w2ui["bos-form-trpotgajipinjamankaryawan_grid3"].clear();');
        $vdb = $this->trpotgajipinjamankaryawan_m->getdata($va['payroll']) ;
        if( $dbr = $this->trpotgajipinjamankaryawan_m->getrow($vdb) ){
            echo('
                  with(bos.trpotgajipinjamankaryawan.obj){
                     find("#payroll").val("'.$dbr['keterangan'].'") ;
                  }
              ') ;
            
            $vare = array();
            $n = 0 ;
            $dbd2 = $this->trpotgajipinjamankaryawan_m->getdataskala($va['payroll']);
            while( $dbr2 = $this->trpotgajipinjamankaryawan_m->getrow($dbd2) ){
                $n++;
                $vare[] = array("recid"=>$n,"lamakerja"=>$dbr2['lamakerja'],"tgl"=>date_2d($dbr2['tgl']),"nominal"=>$dbr2['nominal']);
            }

            $vare = json_encode($vare);
            echo('
              bos.trpotgajipinjamankaryawan.loadmodalpreview("show") ;
              w2ui["bos-form-trpotgajipinjamankaryawan_grid3"].add('.$vare.');
            ');
        }
    }

    public function seekperiode(){
        $search     = $this->input->get('q');
        $vdb    = $this->trpotgajipinjamankaryawan_m->seekperiode($search) ;
        $dbd    = $vdb['db'] ;
        $vare   = array();
        while( $dbr = $this->trpotgajipinjamankaryawan_m->getrow($dbd) ){
            $vare[] 	= array("id"=>$dbr['kode'], "text"=>$dbr['kode'] ." - ". $dbr['keterangan']) ;
        }
        $Result = json_encode($vare);
        echo($Result) ;
   }

   public function loadperiode(){
        $va 	= $this->input->post() ;
        $vdb    = $this->trpotgajipinjamankaryawan_m->getperiode($va) ;
        if( $dbr = $this->trpotgajipinjamankaryawan_m->getrow($vdb['db']) ){
            echo('
                with(bos.trpotgajipinjamankaryawan.obj){
                    find("#tglawal").val("'.date_2d($dbr['tglawal']).'") ;
                    find("#tglakhir").val("'.date_2d($dbr['tglakhir']).'") ;
                }
            ') ;
        }
   }
}
?>
