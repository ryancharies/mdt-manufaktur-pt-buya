/*cabang*/
var mdl_cabang = {}
mdl_cabang.vargr = {'selection': '', 'grid': '','cabang':''};
mdl_cabang.init = function(bsearch){
    if(w2ui['nm_grcabang'] !== undefined){
        w2ui['nm_grcabang'].destroy(); //hancurkan grid biar bisa dilihat lagi
    }
    
    $("#_grcabang").w2grid({
        name : 'nm_grcabang',
        header: 'Silahkan Pilih Cabang',
        limit : 20 ,
        url : base_url + 'admin/load/mdl_gr_cabang',
        postData: {'bsearch': bsearch} ,
        multiSelect : true,
        show:{
            header : true,
            footer : true,
            toolbar : true,
            toolbarColumns : false
        },
        multiSearch	: false,
        columns: [
            { field: 'kode', caption: 'Kode', size: '100px', sortable: false},
            { field: 'keterangan', caption: 'Nama cabang', size: '350px', sortable: false },
            { field: 'kota', caption: 'Kota', size: '80px', sortable: false }
        ],
        onLoad: function(event) {
            event.onComplete = function () {
                $.each(mdl_cabang.vargr.selection, function(i, v){
                    w2ui['nm_grcabang'].select(v);
                });
                


            }
        },
        onKeydown: function(event) {
            if(event.originalEvent.keyCode == 13){
                eval(w2ui['nm_grcabang'].get(mdl_cabang.recid).pilih_f);
            }
        }, 
        onClick: function(event){
            event.onComplete = function () {
                mdl_cabang.vargr.selection = w2ui['nm_grcabang'].getSelection();
            }            
        },
        onDblClick: function(event){
            mdl_cabang.recid = event.recid;
            eval(w2ui['nm_grcabang'].get(mdl_cabang.recid).pilih_f);
        },
        onRender: function(event){
            // alert("dnrenderf");

            setTimeout(function(){
                $('#grid_nm_grcabang_search_all').focus();
                $('#grid_nm_grcabang_search_all').on('keydown', function(e) {
                    if(e.keyCode === 40) { 
                        $('#grid_nm_grcabang_search_all').blur();
                        $('#grid_nm_grcabang_records tbody tr:eq(2)').click();
                        $('#grid_nm_grcabang_focus').focus();
                    } 
                });
            }, 100);
        }
    });
}

mdl_cabang.pilih = function(kd, kd_cabang){
    if(kd_cabang == undefined) kd_cabang = '';

    mdl_cabang.var = {'kode': kd, 'kd_cabang': kd_cabang};
    mo['mdl_cabang'].modal('hide');
}

mdl_cabang.open = function(_cb, bsearch){
    mdl_cabang.var = {'kode': '', 'kd_cabang': '','cabang':''};
    if(bsearch == undefined) bsearch = {};
    

    this.html = '<div id="_grcabang" style="height:500px; margin-left: -15px; margin-right: -15px;"></div>';
    bjs.modal({
        title: 'Daftar cabang',
        multiselect:true,
        html: mdl_cabang.html,
        size: 400,
        height: 200,
        fshow: function(){
            mdl_cabang.init(bsearch)
        },
        fhide:function(){
            // alert("dddd");
            console.log(mdl_cabang.vargr.selection);
            mdl_cabang.var.cabang = JSON.stringify(mdl_cabang.vargr.selection);
            bjs.ajax('admin/load/mdl_cabang_seek', 'cabang=' + mdl_cabang.var.cabang,'',function(r){
                _cb(r);
            });
        },
        name: 'mdl_cabang'
    });
}

/* gudang */
var mdl_gudang = {}
mdl_gudang.vargr = {'selection': '', 'grid': '','gudang':''};
mdl_gudang.init = function(bsearch){
    if(w2ui['nm_grgudang'] !== undefined){
        w2ui['nm_grgudang'].destroy(); //hancurkan grid biar bisa dilihat lagi
    }
    
    $("#_grgudang").w2grid({
        name : 'nm_grgudang',
        header: 'Silahkan Pilih Gudang',
        limit : 20 ,
        url : base_url + 'admin/load/mdl_gr_gudang',
        postData: {'bsearch': bsearch} ,
        multiSelect : true,
        show:{
            header : true,
            footer : true,
            toolbar : true,
            toolbarColumns : false
        },
        multiSearch	: false,
        columns: [
            { field: 'kode', caption: 'Kode', size: '100px', sortable: false},
            { field: 'keterangan', caption: 'Nama gudang', size: '350px', sortable: false },
            { field: 'cabang', caption: 'Cabang', size: '80px', sortable: false }
        ],
        onLoad: function(event) {
            event.onComplete = function () {
                $.each(mdl_gudang.vargr.selection, function(i, v){
                    w2ui['nm_grgudang'].select(v);
                });
                


            }
        },
        onKeydown: function(event) {
            if(event.originalEvent.keyCode == 13){
                eval(w2ui['nm_grgudang'].get(mdl_gudang.recid).pilih_f);
            }
        }, 
        onClick: function(event){
            event.onComplete = function () {
                mdl_gudang.vargr.selection = w2ui['nm_grgudang'].getSelection();
            }            
        },
        onDblClick: function(event){
            mdl_gudang.recid = event.recid;
            eval(w2ui['nm_grgudang'].get(mdl_gudang.recid).pilih_f);
        },
        onRender: function(event){
            // alert("dnrenderf");

            setTimeout(function(){
                $('#grid_nm_grgudang_search_all').focus();
                $('#grid_nm_grgudang_search_all').on('keydown', function(e) {
                    if(e.keyCode === 40) { 
                        $('#grid_nm_grgudang_search_all').blur();
                        $('#grid_nm_grgudang_records tbody tr:eq(2)').click();
                        $('#grid_nm_grgudang_focus').focus();
                    } 
                });
            }, 100);
        }
    });
}

mdl_gudang.pilih = function(kd, kd_cabang){
    if(kd_cabang == undefined) kd_cabang = '';

    mdl_gudang.var = {'kode': kd, 'kd_cabang': kd_cabang};
    mo['mdl_gudang'].modal('hide');
}

mdl_gudang.open = function(_cb, bsearch){
    mdl_gudang.var = {'kode': '', 'kd_cabang': '','gudang':''};
    if(bsearch == undefined) bsearch = {};
    

    this.html = '<div id="_grgudang" style="height:500px; margin-left: -15px; margin-right: -15px;"></div>';
    bjs.modal({
        title: 'Daftar Gudang',
        multiselect:true,
        html: mdl_gudang.html,
        size: 400,
        height: 200,
        fshow: function(){
            mdl_gudang.init(bsearch)
        },
        fhide:function(){
            // alert("dddd");
            console.log(mdl_gudang.vargr.selection);
            mdl_gudang.var.gudang = JSON.stringify(mdl_gudang.vargr.selection);
            bjs.ajax('admin/load/mdl_gudang_seek', 'gudang=' + mdl_gudang.var.gudang,'',function(r){
                _cb(r);
            });
        },
        name: 'mdl_gudang'
    });
}

/* Barang / stok */
var mdl_barang = {}
mdl_barang.vargr = {'selection': '', 'grid': '','barang':''};
mdl_barang.init = function(bsearch){
    if(w2ui['nm_grbarang'] !== undefined){
        w2ui['nm_grbarang'].destroy(); //hancurkan grid biar bisa dilihat lagi
    }
    
    $("#_grbarang").w2grid({
        name : 'nm_grbarang',
        header: 'Silahkan Pilih Barang',
        limit : 20 ,
        url : base_url + 'admin/load/mdl_gr_stock',
        postData: {'bsearch': bsearch} ,
        multiSelect : true,
        show:{
            header : true,
            footer : true,
            toolbar : true,
            toolbarColumns : false
        },
        multiSearch	: false,
        columns: [
            { field: 'cmdpilih', caption: ' ', size: '80px', sortable: false },
            { field: 'kode', caption: 'Kode', size: '100px', sortable: false},
            { field: 'keterangan', caption: 'Keterangan', size: '200px', sortable: false },
            { field: 'satuan', caption: 'Satuan', size: '100px', sortable: false },
            { field: 'hj', caption: 'Harga Jual', size: '100px',render:'float:2', sortable: false },
            { field: 'saldo', caption: 'Saldo', size: '100px',render:'float:2', sortable: false },
        ],
        onKeydown: function(event) {
            if(event.originalEvent.keyCode == 13){
                eval(w2ui['nm_grbarang'].get(mdl_barang.recid).pilih_f);
            }
        },
        onRender: function(event){
            // alert("dnrenderf");

            setTimeout(function(){
                $('#grid_nm_grbarang_search_all').focus();
                $('#grid_nm_grbarang_search_all').on('keydown', function(e) {
                    if(e.keyCode === 40) { 
                        $('#grid_nm_grbarang_search_all').blur();
                        $('#grid_nm_grbarang_records tbody tr:eq(2)').click();
                        $('#grid_nm_grbarang_focus').focus();
                    } 
                });
            }, 100);
        }
    });
}

mdl_barang.pilih = function(kd){
    console.log(kd);

    mdl_barang.var = {'kode': kd};
    mo['mdl_barang'].modal('hide');
}

mdl_barang.open = function(_cb, bsearch){
    mdl_barang.var = {'kode': '', 'kd_cabang': '','barang':''};
    if(bsearch == undefined) bsearch = {};
    

    this.html = '<div id="_grbarang" style="height:300px; margin-left: -15px; margin-right: -15px;"></div>';
    bjs.modal({
        title: 'Daftar barang',
        multiselect:true,
        html: mdl_barang.html,
        size: 1000,
        height: 200,
        fshow: function(){
            mdl_barang.init(bsearch);
            // grid.hideColumn('recid', 'fname');
            // console.log(bsearch);
            if(bsearch.customer == "" || bsearch.customer == null)w2ui['nm_grbarang'].hideColumn('hj');
            if(bsearch.gudang == "" || bsearch.gudang == null)w2ui['nm_grbarang'].hideColumn('saldo');
        },
        fhide:function(){
            // alert("dddd");
            if(bsearch.customer == "" || bsearch.customer == null) bsearch.customer = '';
            if(bsearch.gudang == "" || bsearch.gudang == null) bsearch.gudang = '';
            if(bsearch.tgl == "" || bsearch.tgl == null) bsearch.tgl = '';
            mdl_barang.datapost = 'stock=' + mdl_barang.var.kode + '&customer=' + bsearch.customer + '&gudang=' + JSON.stringify(bsearch.gudang) + '&tgl=' + bsearch.tgl;
            console.log(mdl_barang.datapost);
            
            bjs.ajax('admin/load/mdl_stock_seek', mdl_barang.datapost,'',function(r){
                // alert("bfigfi");
                console.log(r);
                _cb(r);
            });
        },
        name: 'mdl_barang'
    });
}

/* customer */
var mdl_customer = {}
mdl_customer.vargr = {'selection': '', 'grid': '','customer':''};
mdl_customer.init = function(bsearch){
    if(w2ui['nm_grcustomer'] !== undefined){
        w2ui['nm_grcustomer'].destroy(); //hancurkan grid biar bisa dilihat lagi
    }
    
    $("#_grcustomer").w2grid({
        name : 'nm_grcustomer',
        header: 'Silahkan Pilih customer',
        limit : 20 ,
        url : base_url + 'admin/load/mdl_gr_customer',
        postData: {'bsearch': bsearch} ,
        multiSelect : true,
        show:{
            header : true,
            footer : true,
            toolbar : true,
            toolbarColumns : false
        },
        multiSearch	: false,
        columns: [
            { field: 'cmdpilih', caption: ' ', size: '80px', sortable: false },
            { field: 'kode', caption: 'Kode', size: '100px', sortable: false},
            { field: 'nama', caption: 'Nama', size: '200px', sortable: false },
            { field: 'alamat', caption: 'Alamat', size: '200px', sortable: false }
        ],
        onKeydown: function(event) {
            if(event.originalEvent.keyCode == 13){
                eval(w2ui['nm_grcustomer'].get(mdl_customer.recid).pilih_f);
            }
        }, 
        onRender: function(event){
            // alert("dnrenderf");

            setTimeout(function(){
                $('#grid_nm_grcustomer_search_all').focus();
                $('#grid_nm_grcustomer_search_all').on('keydown', function(e) {
                    if(e.keyCode === 40) { 
                        $('#grid_nm_grcustomer_search_all').blur();
                        $('#grid_nm_grcustomer_records tbody tr:eq(2)').click();
                        $('#grid_nm_grcustomer_focus').focus();
                    } 
                });
            }, 100);
        }
    });
}

mdl_customer.pilih = function(kd){
    console.log(kd);

    mdl_customer.var = {'kode': kd};
    mo['mdl_customer'].modal('hide');
}

mdl_customer.open = function(_cb, bsearch){
    mdl_customer.var = {'kode': ''};
    if(bsearch == undefined) bsearch = {};
    

    this.html = '<div id="_grcustomer" style="height:300px; margin-left: -15px; margin-right: -15px;"></div>';
    bjs.modal({
        title: 'Daftar Customer',
        multiselect:true,
        html: mdl_customer.html,
        size: 1000,
        height: 200,
        fshow: function(){
            mdl_customer.init(bsearch);
        },
        fhide:function(){
            // alert("dddd");
            if(bsearch.customer == "" || bsearch.customer == null) bsearch.customer = '';
            if(bsearch.gudang == "" || bsearch.gudang == null) bsearch.gudang = '';
            if(bsearch.tgl == "" || bsearch.tgl == null) bsearch.tgl = '';
            mdl_customer.datapost = 'kode=' + mdl_customer.var.kode;
            console.log(mdl_customer.datapost);
            
            bjs.ajax('admin/load/mdl_customer_seek', mdl_customer.datapost,'',function(r){
                // alert("bfigfi");
                console.log(r);
                _cb(r);
            });
        },
        name: 'mdl_customer'
    });
}

/* supplier */
var mdl_supplier = {}
mdl_supplier.vargr = {'selection': '', 'grid': '','supplier':''};
mdl_supplier.init = function(bsearch){
    if(w2ui['nm_grsupplier'] !== undefined){
        w2ui['nm_grsupplier'].destroy(); //hancurkan grid biar bisa dilihat lagi
    }
    
    $("#_grsupplier").w2grid({
        name : 'nm_grsupplier',
        header: 'Silahkan Pilih supplier',
        limit : 20 ,
        url : base_url + 'admin/load/mdl_gr_supplier',
        postData: {'bsearch': bsearch} ,
        multiSelect : true,
        show:{
            header : true,
            footer : true,
            toolbar : true,
            toolbarColumns : false
        },
        multiSearch	: false,
        columns: [
            { field: 'cmdpilih', caption: ' ', size: '80px', sortable: false },
            { field: 'kode', caption: 'Kode', size: '100px', sortable: false},
            { field: 'nama', caption: 'Nama', size: '200px', sortable: false },
            { field: 'alamat', caption: 'Alamat', size: '200px', sortable: false }
        ],
        onKeydown: function(event) {
            if(event.originalEvent.keyCode == 13){
                eval(w2ui['nm_grsupplier'].get(mdl_supplier.recid).pilih_f);
            }
        }, 
        onRender: function(event){
            // alert("dnrenderf");

            setTimeout(function(){
                $('#grid_nm_grsupplier_search_all').focus();
                $('#grid_nm_grsupplier_search_all').on('keydown', function(e) {
                    if(e.keyCode === 40) { 
                        $('#grid_nm_grsupplier_search_all').blur();
                        $('#grid_nm_grsupplier_records tbody tr:eq(2)').click();
                        $('#grid_nm_grsupplier_focus').focus();
                    } 
                });
            }, 100);
        }
    });
}

mdl_supplier.pilih = function(kd){
    console.log(kd);

    mdl_supplier.var = {'kode': kd};
    mo['mdl_supplier'].modal('hide');
}

mdl_supplier.open = function(_cb, bsearch){
    mdl_supplier.var = {'kode': ''};
    if(bsearch == undefined) bsearch = {};
    

    this.html = '<div id="_grsupplier" style="height:300px; margin-left: -15px; margin-right: -15px;"></div>';
    bjs.modal({
        title: 'Daftar supplier',
        multiselect:true,
        html: mdl_supplier.html,
        size: 1000,
        height: 200,
        fshow: function(){
            mdl_supplier.init(bsearch);
        },
        fhide:function(){
            // alert("dddd");
            if(bsearch.supplier == "" || bsearch.supplier == null) bsearch.supplier = '';
            if(bsearch.gudang == "" || bsearch.gudang == null) bsearch.gudang = '';
            if(bsearch.tgl == "" || bsearch.tgl == null) bsearch.tgl = '';
            mdl_supplier.datapost = 'kode=' + mdl_supplier.var.kode;
            console.log(mdl_supplier.datapost);
            
            bjs.ajax('admin/load/mdl_supplier_seek', mdl_supplier.datapost,'',function(r){
                // alert("bfigfi");
                console.log(r);
                _cb(r);
            });
        },
        name: 'mdl_supplier'
    });
}